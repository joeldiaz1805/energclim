<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

Route::get('/', function () {
    return view('welcome');
});
*/

//ruta pagina principal /home
Route::get('/home','HomeController@index');

	Route::middleware('UserActive','PwErr')->group(function () {
		Route::middleware('auth')->group(function () {	
		
		Route::resource('/motogenerador','Admin\MotorController');
		Route::get('/movimiento','Admin\MotorController@movimiento');
		

		Route::get('/nivel_combustible','Admin\MotorController@niveles_combustible');
		Route::get('/nivel_combustible/{id}/edit','Admin\MotorController@niveles_combustible_edit');
		Route::put('/nivel_combustible/{id}','Admin\MotorController@niveles_combustible_update');

		Route::get('/niveles_combustible_region','Admin\MotorController@niveles_combustible_region');
		Route::get('/porcentajes_mapa/{tipo}/{id}','Admin\MotorController@porcentajes_mapa');
		Route::get('/combustible_estado/{id}','Admin\MotorController@nivel_estado');
		Route::get('/combustible_region/{id}','Admin\MotorController@nivel_region');

		
		Route::get('/verpdf/{id}','Admin\ActividadesController@download');
		Route::get('/generar_ups/{id}','Admin\ActividadesController@planilla_ups');

		Route::get('/inventario_region','Admin\MotorController@inventario');
		Route::get('/consolidado','Admin\FacturasController@consolidado');
		//ruta inventario aire acondicionado
		Route::resource('/aire_acondicionado','Admin\AAController');
		Route::resource('/climatizacion','Admin\ClimatizacionController');
		//ruta inventario rectificador
		Route::resource('/cuadro_fuerza','Admin\RectificadorController');
		//ruta inventario ups
		Route::resource('/ups','Admin\UpsController');
		Route::resource('/bancob','Admin\BcoBateriaController');
		//ruta registro de actividades de la Coordinación de Seguimiento y Control de Gestión
		Route::get('/opsut.vista','Admin\NodoopsutController@vista');
		Route::resource('/marcas','Admin\MarcasController');
		Route::resource('/proveedores','Admin\ProveedoresController');
		Route::resource('/contratos','Admin\ContratosController');
		Route::resource('/facturas','Admin\FacturasController');
		Route::resource('/actividades','Admin\ActividadesController');
		Route::get('/facturas_suma/{id}','Admin\FacturasController@suma');
		Route::get('/facturas_buscar_proveedor/{id}','Admin\FacturasController@buscar_proveedor');

		Route::resource('/menus','Admin\MenuController');
		Route::get('/orden_menu','Admin\MenuController@orden');
		Route::post('/orden_post','Admin\MenuController@orden_post');

		Route::resource('/submenu','Admin\SubMenuController');
		Route::resource('/sub_submenu','Admin\SubSubMenuController');
		Route::resource('/nodos','Admin\NodoController');
		Route::get('/maps','Admin\NodoController@maps');
		Route::resource('/reqopsut','Admin\RequeOpsutController');



		Route::get('nodos/{id}','Admin\NodoController@show');
		Route::get('equipos_nodos','Admin\NodoController@equipos_nodo');
		Route::get('nodos_cuadroF/{id}','Admin\NodoController@nodos_cuadroF');
		Route::get('nodos_mg/{id}','Admin\NodoController@nodos_mg');
		Route::get('nodos_aa/{id}','Admin\NodoController@nodos_aa');
		Route::get('nodos_bcobb/{id}','Admin\NodoController@nodos_bcobb');
		Route::get('resumen_diario','Admin\NodoController@resumen');
		Route::get('equipos_actualizados','Admin\NodoController@equipos_actualizados');

		Route::resource('Componentes','Admin\ComponentesController');
		

		Route::get('/con_graficas','Admin\SismycController@con_graficas');
		Route::post('/graficas','Admin\SismycController@graficas');


		//Route::resource('/tickets_ex','Admin\SismycController');
		
		Route::get('/consultar_ticket_ex','Admin\SismycController@index');
		Route::get('/tickets_ex','Admin\SismycController@store');
		Route::resource('/control-cambios-ex','Admin\CdcSismycController');
		Route::resource('/disponibilidad-servicio','Admin\DpsSismycController');


	Route::middleware('UsrSecretaria')->group(function () {

		//menu inventario     
		//ruta inventario motogenerador
		Route::resource('/tickets','Admin\TicketController');
		Route::resource('/localidades','Admin\LocalidadesController');
		Route::resource('/estados','Admin\EstadosController');
		Route::resource('/municipios','Admin\MunicipiosController');

		Route::get('/municipios_poredo/{id}','Admin\ParroquiasController@municipios');
		Route::get('/parroquias_pormu/{id}','Admin\ParroquiasController@parroquias');
		Route::resource('/parroquias','Admin\ParroquiasController');
		Route::resource('/regiones','Admin\RegionesController');
		Route::resource('/cuadrillas','Admin\CuadrillaController');

		Route::get('/preventivos','Admin\TicketController@preve');
		Route::get('/correctivos','Admin\TicketController@correctivos');
		Route::get('/inspeccion','Admin\TicketController@inspeccion');
		//Route::resource('/tickets/correctivo','Admin\TiketsCorrController');

		Route::resource('/tickets/correctivo','Admin\TiketsCorrController');
		Route::post('/tickets/documentar','Admin\TicketController@documentar');
		 
		Route::resource('/registro_actividades','Admin\CSCGController');

		//llevame a la vista
		Route::get('/contratista/{id}','Admin\CSCGController@cuadrilla');

		//administrador
		//ruta administracion usuarios, crear modificar
		Route::get('/reset_psw/{id}','Admin\UsuariosController@reset_passw')->middleware('SuperUser');
		

		//acceso modulo personal
		Route::resource('/personal','Admin\PersonalController');

		// rutas de acceso modulo opsut, pendiente reviison de acceso y nivel de acceso
		Route::resource('/opsut','Admin\NodoopsutController');

		//Route::resource('/opsut.nodos','Admin\NodoopsutController');

		//acceso modulo formatos
		Route::resource('/format','Admin\PlantillasController');



		
    

		});
	});
		Route::resource('/usuarios','Admin\UsuariosController');
		Route::get('/mod_usuario/{id}','Admin\UsuariosController@mod_usuario');
		Route::put('/mod_usuario/{id}','Admin\UsuariosController@mod_update');
    
		Route::post('/emails','Admin\loginController@emails');
		Route::get('/password_reset','Admin\loginController@recuperar_password');

// ruta mantenimiento -> registro de tiket -> registro de mantenimiento
		Route::get('/componentes/{id}','Admin\loginController@componentes');
		Route::get('/estado/{id}','Admin\loginController@estado');
		Route::get('/central/{id}','Admin\loginController@central');
		
		Route::get('/fallas/{id}','Admin\loginController@fallas');
		Route::get('/mantenimiento/{id}/{mnt}','Admin\loginController@mantenimiento');
		Route::get('/mantenimiento/{mnt}','Admin\loginController@mantenimiento2');
		Route::get('/tipo_servicio/{id}','Admin\loginController@tipo_servicio');
		Route::get('/tipo_actividad/{id}','Admin\loginController@tipo_actividad');



		Route::get('/equipos_centrales/{id}/{tipo}','Admin\MotorController@equipos');
		Route::get('/equipos_centrales/{id}','Admin\loginController@equipos2');


		Route::get('/download/{tipo}','Admin\TicketController@download');
		Route::get('/download_plan/{tipo}','Admin\TicketController@download_pln');


		Route::resource('/tablero_actividad','Admin\TablaActividadController');
		Route::get('/planillas_ups','Admin\TablaActividadController@planillas_ups');
		Route::get('/planillas_ups-create','Admin\TablaActividadController@planillas_ups_create');
		Route::get('/tablero_actividad/celdas/{id}','Admin\TablaActividadController@create_celdas');
		Route::post('/tablero_actividad/celdas','Admin\TablaActividadController@celdas_post');
		
		Route::get('/masiva','Admin\TablaActividadController@masiva');
		Route::get('/masiva-create','Admin\TablaActividadController@planillas_masiva_create');

		});

//Route::get('/','HomeController@home');

//urls libres de login

Route::resource('/','Admin\loginController');


Route::get('/','Admin\loginController@index')->name('login');
Route::get('/logout','Admin\loginController@logout');
Route::get('/centrales/{id}','Admin\loginController@centrales');

Route::get('/generador_sismyc','Admin\loginController@generador_sismyc');
Route::get('/generador_sismyc_v2','Admin\loginController@generador_sismyc_nuevo');

Route::resource('/pisos','Admin\PisosController');
Route::resource('/salas','Admin\SalasController');
Route::resource('/equipos','Admin\EquiposController');
Route::resource('/fallas_modulo','Admin\FallasController');
Route::resource('/acciones','Admin\AccionesController');

Route::get('/resumen_sismyc','Admin\SismycController@create');

Route::get('/pers/{id}','Admin\loginController@personal');
Route::get('/texto_apertura','Admin\loginController@texto_apertura');
Route::get('/actualizar_centrales','Admin\TicketController@act_central');
Route::get('/consulta_bossprov','Admin\ScrapinController@inic');
Route::post('/consulta_bossprov','Admin\ScrapinController@scraping');
Route::get('/mapa_vector','Admin\loginController@mapa_vector');

