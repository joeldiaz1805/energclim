var simplemaps_countrymap_mapdata={
  main_settings: {
   //General settings
    width: "responsive", //'700' or 'responsive'
    background_color: "#FFFFFF",
    background_transparent: "yes",
    border_color: "#ffffff",
    state_description: "",
    state_color: "#88A4BC",
    state_hover_color: "#3B729F",
    state_url: "",
    border_size: 1.5,
    all_states_inactive: "no",
    all_states_zoomable: "yes",
    
    //Location defaults
    location_description: "Location description",
    location_url: "",
    location_color: "#FF0067",
    location_opacity: 0.8,
    location_hover_opacity: 1,
    location_size: 25,
    location_type: "square",
    location_image_source: "frog.png",
    location_border_color: "#FFFFFF",
    location_border: 2,
    location_hover_border: 2.5,
    all_locations_inactive: "no",
    all_locations_hidden: "no",
    
    //Label defaults
    label_color: "#d5ddec",
    label_hover_color: "#d5ddec",
    label_size: 22,
    label_font: "Arial",
    hide_labels: "no",
    hide_eastern_labels: "no",
   
    //Zoom settings
    zoom: "yes",
    manual_zoom: "yes",
    back_image: "no",
    initial_back: "no",
    initial_zoom: "-1",
    initial_zoom_solo: "no",
    region_opacity: 1,
    region_hover_opacity: 0.6,
    zoom_out_incrementally: "yes",
    zoom_percentage: 0.99,
    zoom_time: 0.5,
    
    //Popup settings
    popup_color: "white",
    popup_opacity: 0.9,
    popup_shadow: 1,
    popup_corners: 5,
    popup_font: "12px/1.5 Verdana, Arial, Helvetica, sans-serif",
    popup_nocss: "no",
    
    //Advanced settings
    div: "map",
    auto_load: "yes",
    url_new_tab: "no",
    images_directory: "default",
    fade_time: 0.1,
    link_text: "View Website",
    popups: "detect",
    state_image_url: "",
    state_image_position: "",
    location_image_url: ""
  },
  state_specific: {
    VEN23: {
      name: "Falcón",
      color: "#00a65a",
      id:15,
      url:'combustible_estado/MTU='
    },
    VEN25: {
      name: "Apure",
      color: "#00c0ef",
      id:10,
      url:'combustible_estado/MTA='
    },
    VEN26: {
      name: "Barinas",
      color: "#00c0ef",
      id:3,
      url:'combustible_estado/Mw=='
    },
    VEN27: {
      name: "Mérida",
      color: "#3c8dbc",
      id:4,
      url:'combustible_estado/NA=='
    },
    VEN28: {
      name: "Táchira",
      color: "#3c8dbc",
      id:1,
      url:'combustible_estado/MQ=='
    },
    VEN29: {
      name: "Trujillo",
      color: "#3c8dbc",
      id:2,
      url:'combustible_estado/Mg=='
    },
    VEN31: {
      name: "Zulia",
      color: "#00a65a",
      id:35,
      url:'combustible_estado/MzU='
    },
    VEN32: {
      name: "Cojedes",
      color: "#00c0ef",
      id:12,
      url:'combustible_estado/MTI='
    },
    VEN33: {
      name: "Carabobo",
      color: "#f39c12",
      url:'combustible_estado/MTE=',
      id:11
    },
    VEN34: {
      name: "Lara",
      color: "#00a65a",
      url:'combustible_estado/MTc=',
      id:17
    },
    VEN35: {
      name: "Portuguesa",
      color: "#00c0ef",
      url:'combustible_estado/MTY=',
      id:16
    },
    VEN36: {
      name: "Yaracuy",
      color: "#f39c12",
      url:'combustible_estado/MTg=',
      id:18
    },
    VEN38: {
      name: "Amazonas",
      color: "#d2d6de",
      url:'combustible_estado/MjI=',
      id:22
    },
    VEN39: {
      name: "Bolívar",
      color: "#d2d6de",
      url:'combustible_estado/MTk=',
      id:19
    },
    VEN40: {
      name: "Anzoátegui",
      color: "#f56954",
      url:'combustible_estado/Mjg=',
      id:28
    },
    VEN41: {
      name: "Aragua",
      color: "#f39c12",
      url:'combustible_estado/MTQ=',
      id:14
    },
    VEN42: {
      name: "Vargas",
      color: "#f39c12",
      url:'combustible_estado/OA==',
      id:8
    },
    VEN43: {
      name: "Distrito Capital",
      color: "#f39c12",
      url:'combustible_estado/Nw==',
      id:7
    },
    VEN44: {
      name: "Dependencias Federales",
      id:0
    },
    VEN45: {
      name: "Guárico",
      color: "#00c0ef",
      url:'combustible_estado/MTM=',
      id:13
    },
    VEN46: {
      name: "Monagas",
      color: "#f56954",
      url:'combustible_estado/MjY=',
      id:26
    },
    VEN47: {
      name: "Miranda",
      color: "#f39c12",
      url:'combustible_estado/OQ==',
      id:9
    },
    VEN48: {
      name: "Nueva Esparta",
      url:'combustible_estado/Mjc=',
      id:27
    },
    VEN49: {
      name: "Sucre",
      color: "#f56954",
      url:'combustible_estado/Mjk=',
      id:29
    },
    VEN65: {
      name: "Delta Amacuro",
      color: "#d2d6de",
      url:'combustible_estado/MzQ=',
      id:34
    }
  },
 
  labels: {
    "0": {
      name: "Las Regiones"
    }
  },
  legend: {
    entries: [
      {
        name: "Los Llanos",
        color: "#f31212",
        type: "",
        shape: "",
        ids: "NAo="
      },
      {
        name: "Centro",
        color: "#f312d7",
        type: "",
        shape: "",
        ids: "Mwo="
      },
      {
        name: "Capital",
        color: "#11c1e7",
        type: "",
        shape: "",
        ids: "Mgo="
      },
      {
        name: "Oriente",
        color: "#9c19f1",
        type: "",
        shape: "",
        ids: "Nwo="
      },
      {
        name: "Occidente",
        color: "#3a7c00",
        type: "",
        shape: "",
        ids: "Ng=="
      },
      {
        name: "Guayana",
        color: "#3e11e7",
        type: "",
        shape: "",
        ids: "NQ=="
      },
      {
        name: "Andes",
        color: "#dcdcdc",
        type: "",
        shape: "",
        ids: "MQ=="
      }
    ]
  },
  regions: {
    "3": {
      states: [
        "VEN41",
        "VEN36",
        "VEN33"
      ],
      name: "Centro",
      color: "#f312c8",
      hover_color: "#f312c8",
      description: "Región  Central",
      zoomable: "0"
    },
    "4": {
      states: [
        "VEN45",
        "VEN32",
        "VEN25",
        "VEN26",
        "VEN35"
      ],
      name: "Los LLanos",
      color: "#f31212",
      hover_color: "#f31212",
      description: "Región Los LLanos",
      zoomable: "0"
    },
    "6": {
      states: [
        "VEN31",
        "VEN23",
        "VEN34"
      ],
      name: "Occidente",
      color: "#3a7c00",
      hover_color: "#3a7c00",
      description: "Región Occidente",
      zoomable: "1"
    },
    "7": {
      states: [
        "VEN40",
        "VEN48",
        "VEN49",
        "VEN46"
      ],
      name: "Oriente",
      color: "#9c19f1",
      description: "Región Oriente"
    },
    "2": {
      states: [
        "VEN47",
        "VEN42",
        "VEN43"
      ],
      name: "Capital",
      color: "#11c1e7",
      description: "Región Capital"
    },
    "5": {
      states: [
        "VEN39",
        "VEN38",
        "VEN65"
      ],
      name: "Guayana",
      color: "#3e11e7",
      description: "Región Guayana"
    },
    "1": {
      states: [
        "VEN29",
        "VEN28",
        "VEN27"
      ],
      name: "Los Andes",
      color: "#dcdcdc",
      description: "Región Los Andes"
    }
   },
  data: {
    data: {
      VEN23: "1",
      VEN25: "2"
    }
  }
};