<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Hash;

class usuarios extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$usuarios = [
    		[
    	    	'nombre' =>'Adriana González',
    	       'email'=>'agonz3@cantv.com.ve',
    	       'clave'=>'Cantv2020',
    	       'rol'=>1
    	],
		[
			'nombre' =>'Antonio Rafael Carrasquero Florez',
			'email'=>	'acarra05@cantv.com.ve',
			'clave'=>	'Cantv2020',
			'rol'=>4
		],
		[
			'nombre' =>'Carlos Manuel Leon Rodriguez',
			'email'=>	'cleon4@cantv.com.ve',
			'clave'=>	'Cantv2020',
			'rol'=>4
		],

		['nombre' =>'Deivinson Gabriel Soto Soto',
			'email'=>	'dsotos01@cantv.com.ve',
			'clave'=>	'Cantv2020',
			'rol'=>4
		],

		[
			'nombre' => 'Erick Pérez',
			'email'=>	'eperez45@cantv.com.ve',
			'clave'=>	'12345678',
			'rol'=>1
		],
		
		[
			'nombre' =>'Gabriel Graffe',	
			'email'=>	'ggraff01@cantv.com.ve',
			'clave'=>	'12345678',
			'rol'=>1
		],

		[
			'nombre' =>'Giuseppe Arbore Blanco',
			'email'=>	'garbor01@cantv.com.ve',
			'clave'=>	'Cantv2020',
			'rol'=>4
		],

		[
			'nombre' =>'joel diaz velasquez',
			'email'=>	'joeldiaz1805@gmail.com',
			'clave'=>	'12345678',
			'rol'=>1
		],

		[
			'nombre' =>'Joens Aranguren',	
			'email'=>	'jarang01@cantv.com.ve',
			'clave'=>	'Cantv2020',
			'rol'=>1
		],

		[
			'nombre' =>'Francisco Masso',	
			'email'=>	'fmasso01@cantv.com.ve',
			'clave'=>	'Cantv2020',
			'rol'=>1
		],

		[
			'nombre' =>'Jose Benjamin Pena Leal',
			'email'=>	'jpenal04@cantv.com.ve',
			'clave'=>	'Cantv2020',
			'rol'=>4
		],

		[
			'nombre' =>'David Daniel Diaz Colina',
			'email'=>	'ddiazc01@cantv.com.ve',
			'clave'=>	'Cantv2020',
			'rol'=>4
		],

		[
			'nombre' =>'Jesus Rafael Medina Castillo',
			'email'=>	'jmedin30@cantv.com.ve',
			'clave'=>	'Cantv2020',
			'rol'=>4
		],

		[
			'nombre' =>'Luis Ramirez',
				'email'=>'lramir13@cantv.com.ve',
				'clave'=>'Cantv2020',
				'rol'=>4
			],

		[
			'nombre' =>'Michelle Ansidey',
				'email'=>'mansid01@cantv.com.ve',
				'clave'=>'12345678',
				'rol'=>1
			],

		[
			'nombre' =>'Milanyela Blanco',
				'email'=>'mblan4@cantv.com.ve',
				'clave'=>'Cantv2020',
				'rol'=>5
			],

		[
			'nombre' =>'Monica Berra Gotto',
				'email'=>'mberra01@cantv.com.ve',
				'clave'=>'Cantv2020',
				'rol'=>4
			],

		[
			'nombre' =>'Ronald Porfirio Gerder Castillo',
				'email'=>'rgerde01@cantv.com.ve',
				'clave'=>'Cantv2020',
				'rol'=>4
			],

		[
			'nombre' =>'Segundo Fernando Reyes Castano',
				'email'=>'sreyes01@cantv.com.ve',
				'clave'=>'Cantv2020',
				'rol'=>4
			],

		[
			'nombre' =>'Simon Subero',
				'email'=>'ssuber@cantv.com.ve',
				'clave'=>'Cantv2020',
				'rol'=>4
			],

		[
			'nombre' =>'Zailú Mejías',
				'email'=>'zmejia01@cantv.com.ve',
				'clave'=>'12345678',
				'rol'=>1
			]
			];


			foreach ($usuarios as $key => $value) {
				//dd($value['rol']);

				DB::table('users')->insert([
		            'name' => $value['nombre'],
		            
		            'email' => $value['email'],
		            'password' =>Hash::make($value['clave']),
		            'rol_id' => $value['rol'],
		            'int_fail' =>1,
		            'active' => 1
		            
		        	]);
			}
    }
}
