<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use App\Models\Models\usuarios;


class alterar_campo extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
        $usuarios = usuarios::get();

        foreach ($usuarios as $key => $value) {
            
            $seudo = usuarios::where('email',$value->email)->first();
            $valores=explode("@", $value->email);
            $seudo->alias= $valores[0];
            $seudo->save();

        }

    	/*
    	Schema::table('localidad', function ($table) {
		   $table->integer('user_id')->change();
		});
        Schema::table('localidad', function (Blueprint $table) {
        	DB::statement("ALTER TABLE localidad ALTER user_id TYPE INTEGER");
    	});
    	*/
    }
}
