<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Hash;


class Generador extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $acciones = [

        	'ADECUACION DE ESPACIOS',
			'BAJO NIVEL',
			'CAMBIO',
			'CARGA',
			'COMPLETACION',
			'CORTE PROGRAMADO',
			'DESINCORPORACION',
			'FALLA',
			'FALSA ALARMA',
			'INHIBIDO',
			'INSPECCION',
			'INSTALACION',
			'INSTALACION Y PRUEBA',
			'LIMPIEZA',
			'MANTENIMIENTO CORRECTIVO',
			'MANTENIMIENTO PREVENTIVO',
			'MOTOR EN SERVICIO',
			'OBSTRUCCION',
			'OTROS(ESPECIFIQUE)',
			'PRUEBA',
			'REPUESTO',
			'SUMINISTRO',
			'SUSTITUCION',
			'TRASEGADO',
			'TRASLADO'

        ];

        $salas = [
        	'BANCO DE BATERIAS',
			'CASETA',
			'CENTRO DE CONTROL',
			'CENTRO DE DESPACHO',
			'CIC',
			'CX',
			'DATA CENTER',
			'DIGITAL',
			'DIGITAL TANDEM',
			'DP',
			'DSLAM',
			'DX',
			'EX',
			'FURGON',
			'LIBRE',
			'MG',
			'MONITOREO',
			'OAC',
			'OFICINA ADMINISTRATIVA',
			'OPSUT',
			'OUTDOOR',
			'PCA',
			'PCM',
			'PRESURIZADORES',
			'RECTIFICADORES',
			'SERVIDORES',
			'SSP',
			'TABLERO',
			'TERCEROS (USUARIOS)',
			'TX',
			'TX INTERNACIONAL',
			'UMA',
			'UMG',
			'UNICA',
			'UPS',
			'VARIAS SALAS',
			'VENEXPAQ',
        ];

        $pisos =[
			'ALMACEN',
			'AZOTEA',
			'FURGON',
			'MEZZANINA',
			'PB',
			'PH',
			'SEMI SOTANO',
			'SOTANO',
			'UNICO',
			'VARIOS PISOS',
			'1',
			'2',
			'3',
			'4',
			'5',
			'6',
			'7',
			'8',
			'9',
			'10',
			'11',
			'12',
			'13',
			'14',
			'15',
			'16',
			'17',
			'18',
			'19',
			'20',
			'21',
			'22'
        ];




        foreach ($acciones as $key => $value) {
				//dd($value);

				DB::table('acciones')->insert([
		            'nombre' => $value,
		            
		        	]);
			}
        foreach ($salas as $key => $value) {
				//dd($value);

				DB::table('salas')->insert([
		            'nombre' => $value,
		            
		        	]);
			}
			foreach ($pisos as $key => $value) {
				//dd($value);

				DB::table('pisos')->insert([
		            'nombre' => $value,
		            
		        	]);
			}
    }
}
