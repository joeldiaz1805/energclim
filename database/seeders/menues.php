<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Hash;

class menues extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu =[
        	'Inicio'=>[
        		'rol_id'=>1,
        		'url'=>'home',
        		'icon'=>'fas fa-home nav-icon',
        		'observacion'=>'Menu Inicio'

        	],
        	'Inventario'=>[
        		'rol_id'=>1,
        		'url'=>'#',
        		'icon'=>'fas fa-home nav-icon',
        		'observacion'=>'Menu Inicio',
        	],
        	'Mantenimiento'=>[
        		'rol_id'=>1,
        		'url'=>'#',
        		'icon'=>'fas fa-home nav-icon',
        		'observacion'=>'Mantenimiento'
        	],
        	'Administracion'=>[
        		'rol_id'=>1,
        		'url'=>'#',
        		'icon'=>'fas fa-home nav-icon',
        		'observacion'=>'Administracion'
        	],
        	'Cerrar Sesion'=>[
				'rol_id'=>1,
        		'url'=>'#',
        		'icon'=>'fas fa-tty',
        		'observacion'=>'Usuarios',
	        ]
        ];
        	
        $Sub_Menu=[
        	'Lenvatamiento en Planta'=>[
	    		'rol_id'=>1,
        		'url'=>'#',
        		'menu_id'=>2,
        		'icon'=>'fas fa-home nav-icon',
        		'observacion'=>'Menu Inicio',
		        	],
		    'Nodos OPSUT'=>[
    			'rol_id'=>1,
        		'url'=>'#',
        		'menu_id'=>2,
        		'icon'=>'fas fa-home nav-icon',
        		'observacion'=>'Menu Inicio',
        	],
	        'Nodos Ngn'=>[
        		'rol_id'=>1,
	        	'url'=>'#',
	        	'menu_id'=>2,
	        	'icon'=>'fas fa-home nav-icon',
	        	'observacion'=>'Menu Inicio',
        		],
        	'Registro de Tickets'=>[
			
				'rol_id'=>1,
        		'url'=>'#',
        		'menu_id'=>3,
        		'icon'=>'fas fa-home nav-icon',
        		'observacion'=>'Registro de Tickets',
        		],
    		'Usuarios'=>[
        				'rol_id'=>1,
		        		'url'=>'#',
		        		'menu_id'=>4,
		        		'icon'=>'fas fa-tty',
		        		'observacion'=>'Usuarios',
        		],
        		'Personal'=>[
        				'rol_id'=>1,
		        		'url'=>'#',
		        		'menu_id'=>4,
		        		'icon'=>'fas fa-user',
		        		'observacion'=>'Personal',
        		],
        		'Formatos y/o Reportes'=>[
        				'rol_id'=>1,
		        		'url'=>'#',
		        		'menu_id'=>4,
		        		'icon'=>'fas fa-archive',
		        		'observacion'=>'Formatos y/o Reportes',
        		],
		       ];



		$Sub_sub_menu=[
			'Motogenerador'=>[
				'rol_id'=>1,
        		'url'=>'motogenerador',
        		'sub_menu_id'=>1,
        		'icon'=>'fas fa-cogs nav-icon',
        		'observacion'=>'MotoGenerador',	
				],
			'Aire Acondicionado'=>[
				'rol_id'=>1,
        		'url'=>'aire_acondicionado',
        		'sub_menu_id'=>1,
        		'icon'=>'fas fa-cubes nav-icon',
        		'observacion'=>'MotoGenerador',	
				],
			'Rectificador'=>[
				'rol_id'=>1,
        		'url'=>'rectificador',
        		'sub_menu_id'=>1,
        		'icon'=>'fas fa-bolt nav-icon',
        		'observacion'=>'Rectificador',	
				],
			'Ups'=>[
				'rol_id'=>1,
        		'url'=>'ups',
        		'sub_menu_id'=>1,
        		'icon'=>'fas fa-battery-full nav-icon',
        		'observacion'=>'Ups',	
				],
			'Estatus'=>[
				'rol_id'=>1,
        		'url'=>'opsut',
        		'sub_menu_id'=>2,
        		'icon'=>'fas fa-chart-bar nav-icon',
        		'observacion'=>'Estatus',	
				],
			'Registro'=>[
				'rol_id'=>1,
        		'url'=>'nodos',
        		'sub_menu_id'=>2,
        		'icon'=>'fas fa-list-alt nav-icon',
        		'observacion'=>'Registro',	
				],
			'Requerimientos'=>[
				'rol_id'=>1,
        		'url'=>'reqopsut',
        		'sub_menu_id'=>2,
        		'icon'=>'fas fa-clipboard-list nav-icon',
        		'observacion'=>'Requerimientos',	
				],
			'Actualizaciones'=>[
				'rol_id'=>1,
        		'url'=>'#',
        		'sub_menu_id'=>2,
        		'icon'=>'fas fa-sync-alt nav-icon',
        		'observacion'=>'Actualizaciones',	
				],
			'Registro de Mantenimiento'=>[
				'rol_id'=>1,
        		'url'=>'#',
        		'sub_menu_id'=>4,
        		'icon'=>'fas fa-paste',
        		'observacion'=>'Registro de Tickets',
				],
			'Tickets'=>[
				'rol_id'=>1,
        		'url'=>'#',
        		'sub_menu_id'=>4,
        		'icon'=>'fas fa-list-alt',
        		'observacion'=>'Tickets',
				],
				

		];

	
		foreach ($menu as $key => $value) {
			
					//dd($menu[$key]['url']);
					DB::table('menu')->insert([
			            'nombre' => $key,
			            'rol_id' => $menu[$key]['rol_id'],
			            'url' => $menu[$key]['url'],
			            'icon' => $menu[$key]['icon'],
			            'observacion'=>$menu[$key]['observacion']
				]);
		
		}
		foreach ($Sub_Menu as $key => $value) {
			
					//dd($menu[$key]['url']);
					DB::table('sub_menu')->insert([
			            'nombre' => $key,
			            'rol_id' => $Sub_Menu[$key]['rol_id'],
			            'url' => $Sub_Menu[$key]['url'],
			            'menu_id' => $Sub_Menu[$key]['menu_id'],
			            'icon' => $Sub_Menu[$key]['icon'],
			            'observacion'=>$Sub_Menu[$key]['observacion']
				]);
			
		}
		foreach ($Sub_sub_menu as $key => $value) {
			
					//dd($menu[$key]['url']);
					DB::table('sub_sub_menu')->insert([
			            'nombre' => $key,
			            'rol_id' => $Sub_sub_menu[$key]['rol_id'],
			            'url' => $Sub_sub_menu[$key]['url'],
			            'sub_menu_id' => $Sub_sub_menu[$key]['sub_menu_id'],
			            'icon' => $Sub_sub_menu[$key]['icon'],
			            'observacion'=>$Sub_sub_menu[$key]['observacion']
				]);
			
		}



    }
}
