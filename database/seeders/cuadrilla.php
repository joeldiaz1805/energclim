<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Hash;

class cuadrilla extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cuadrilla=[

        	'CANTV',
        	'CONTRATISTA',

        ];

        $contratista=['CANTV'=>[
        	'ARMAN II',
			'ARQUIMEC SERVICIOS, C.A',
			'ASOC. COOPERATIVA SERVINTELCON, R.L',
			'CONSTRUCTORA COINVERCA',
			'COOP. DE SERV. PORTUGUESA, R.L',
			'CORPORACION A.Y.R.F, C.A',
			'CORPORACION BAJO CERO, C.A',
			'DAJOCA SERVICIOS & CONSTRUCCIONES, C.A',
			'DESINGEL',
			'DISTRIBUIDORA DE COMBUSTIBLE "EL CARMEN", S.R.L',
			'ENERGIA GLOBAL 63',
			'ENERGIA TACHIRA',
			'FAPACA, C.A',
			'GENERADORES EVENTRUSS 26, C.A',
			'GRUPO SUMIORION, C.A',
			'GRUPO THINKERING, C.A',
			'HG CONSULTORES',
			'INVERSIONES AROKIM,C.A.',
			'INVERSIONES INHOTEP, C.A',
			'INVERSIONES M.D.S. 1983, C.A',
			'IOM TELECOM, C.A.',
			'LA COPELANCITA DE VENEZUELA',
			'MULTISERVICIOS HVM',
			'REPROINCA 21',
			'REVEMACA 2017, C.A',
			'SENATEL, C.A',
			'SERVICIOS ALEM, C.A',
			'SERVICIOS BAUER, C.A',
			'SERVICIOS INDUSTRIALES VOLTA, C.A',
			'SERVICIOS TECNICOS WEKY LOCK',
			'SERVIC-METAL SUCRE',
			'SOMERINCA',
			'THE MINOS GROUR, S.A',
			'VENEZOLANA DE IMPORTACION COMPAÑÍA ANONIMA',
        ],

        'CONTRATISTA'=>[
			'ARMAN II',
			'ARQUIMEC SERVICIOS, C.A',
			'ASOC. COOPERATIVA SERVINTELCON, R.L',
			'CANTV',
			'CONSTRUCTORA COINVERCA',
			'COOP. DE SERV. PORTUGUESA, R.L',
			'CORPORACION A.Y.R.F, C.A',
			'CORPORACION BAJO CERO, C.A',
			'DAJOCA SERVICIOS & CONSTRUCCIONES, C.A',
			'DESINGEL',
			'DISTRIBUIDORA DE COMBUSTIBLE "EL CARMEN", S.R.L',
			'ENERGIA GLOBAL 63',
			'ENERGIA TACHIRA',
			'FAPACA, C.A',
			'GENERADORES EVENTRUSS 26, C.A',
			'GRUPO SUMIORION, C.A',
			'GRUPO THINKERING, C.A',
			'HG CONSULTORES',
			'INVERSIONES AROKIM,C.A.',
			'INVERSIONES INHOTEP, C.A',
			'INVERSIONES M.D.S. 1983, C.A',
			'IOM TELECOM, C.A.',
			'LA COPELANCITA DE VENEZUELA',
			'MULTISERVICIOS HVM',
			'REPROINCA 21',
			'REVEMACA 2017, C.A',
			'SENATEL, C.A',
			'SERVICIOS ALEM, C.A',
			'SERVICIOS BAUER, C.A',
			'SERVICIOS INDUSTRIALES VOLTA, C.A',
			'SERVICIOS TECNICOS WEKY LOCK',
			'SERVIC-METAL SUCRE',
			'SOMERINCA',
			'THE MINOS GROUR, S.A',
			'VENEZOLANA DE IMPORTACION COMPAÑÍA ANONIMA',
        ],


        ];

        foreach ($cuadrilla as $key => $value) {
    	
         DB::table('cuadrilla')->insert([
            'nombre' => $value,
        	]);
    }

	$cont=0;
   foreach ($contratista as $key => $value) {
	$cont+=1;
   		  for ($j=0; $j<sizeof($contratista[$key]) ; $j++) { 

	         DB::table('contratista')->insert([
	            'nombre' => $contratista[$key][$j],
	            'cuadrilla_id' => $cont,
	        	]);
    	}
    }
    }
}
