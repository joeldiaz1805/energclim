<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;


class campos_supervision extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $questions =[
            '1.- Verificación de las alarmas locales y remotas, antes y después de ejecuatr el mantenimiento.',
            '2.- Limpieza exterior del gabinete. ',
            '3.-Limpieza de las rendijas de ventilación y sustitución del filtro de aire de ser necesario. ',
            '4.- Limpieza externa del gabinete de bypass si aplica. ',
            '5.- Revisión de las condiciones de las tarjetas, fusibles, piezas del rectificación e inversión y bypass. ',
            '7.- Realización de inspección general del cuarto de baterías incluyendo condiciones de temperatura y ventilación. Solo para baterías abiertas.',
            '8.- Ejecución del mantenimiento de baterías, aplicando guia de usuario correspondiente, según el tipo de baterías ',
            '9. Ejecución del descargue histórico de operación del equipo',
            '10.- Verificación de sincronización con la red del UPS. ',
            '11.- Verificación del correcto funcionamiento de la pantalla,comandos y leds. ',
            '12.-Realización de ajuste de hora y fecha del equipo de ser necesario. ',
            '13.-Verificación por pantalla de la medición de voltaje de baterías si aplica. ',
            '14. Medición y registro de los parámetros eléctricos en la entrada y la salida del equipo, tales como corrientes, voltajes, potencias y factor de potencia.',
            '15.-Realización de ajuste del torque en los tornillos que integran el (los) banco (s) de baterías, si aplican. DATOS UP'


        ];
       
       foreach ($questions as $key => $value) {
        
            DB::table('Cuestionario_ups')->insert([
                'descripcion' => $value,
                'user_id' => 8,
            ]);

        }
    }
}
