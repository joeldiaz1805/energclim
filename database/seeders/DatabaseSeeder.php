<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        
		//$this->call(Insercion::class);
		$this->call(menues::class);
		//$this->call(opsut::class);
		//$this->call(usuarios::class);
        //$this->call(cuadrilla::class);
		//$this->call(opsutinvnodo::class);

        //$this->call(campos_supervision::class);
        // \App\Models\User::factory(10)->create();
    }
}
