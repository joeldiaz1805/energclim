<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Hash;
use App\Models\Models\estados;
use App\Models\Models\localidades;


class region_occidente extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $estados=[
        	'MARACAIBO',
			'MARACAIBO (EJE GUAJIRA)',
			'SUR DEL LAGO',
			'COSTA ORIENTAL DEL LAGO',
        ];

        $localidades=[
       
        	        'MARACAIBO'=>[
        		        	'COQUIVACOA',
        					'LOS OLIVOS',
        					'PANAMERICANO',
        					'FRONTERAS',
        					'DELICIAS', 
        					'SAN MIGUEL',
        					'SAN FRANCISCO',
        					'INDUSTRIAS',       
        					'SAN FELIPE ',
        					'LA LAGO',
        					'BELLA VISTA III',
        					'URDANETA', 
        					'BELLA VISTA I,II',
        					'CUATRICENTENARIO',
        					'LOS HATICOS ',
        					'EL TREBOL',
        					'LAGOMAR', 
        					'AMPARO',
        					'CAMPO LA CONCEPCION',
        					'LA CAÑADA',
        					'CUATRICENTENARIO EDELCA',
        					'CENTRO OPERATIVO SABANETA',
        					'OAC SANTO ANGEL',
        					'OAC DORAL CENTER',
        					'OAC SAN FRANCISCO',
        					'OAC LOS OLIVOS'],
        	
        			'MARACAIBO (EJE GUAJIRA)'=>[
        					'SAN RAFAEL DEL MOJAN',
        					'CARRASQUERO',
        					'SANTA CRUZ DE MARA',
        					'TAMARE MARA', 
        					'CAMPO MARA ',
        					'SINAMAICA',
        					'PARAGUAIPOA',
        					],
        	
        			'SUR DEL LAGO'=>[
        						'NODO OPSUT EL CRUCE',
        						'NODO OPSUT CAMPO BERNAL',
        						'NODO SAN IGNACIO',
        						'EL GULLAVO',
        						'EL MORALITO',
        						'ENCONTRADOS ZULIA',
        						'PUEBLO NUEVO',
        						'SANTA CRUZ DEL ZULIA',
        						'SAN CARLO DEL ZULIA',
        						'BELLO MONTE',
        						'NODO OPSUT SAN JOSE DE PERIJA',
        						'NODO  OPSUT CASIGUA EL CUBO',
        						'LA VILLA',
        						'MACHIQUES',
        						'LA LOLAS',
        						'SANTA BARBARA',
        					],
        			'COSTA ORIENTAL DEL LAGO'=>[
        					'CABIMAS CABILLAS',
        					'CIUDAD OJEDA',
        					'OJEDA UNION',
        					'BACHAQUERO',
        					'CABIMAS CENTRO',
        					'TAMARE OJEDA',
        					'CABIMAS DELICIAS',
        					'LOS PUERTOS',
        					'EL TABLAZO',
        					'MENEGRANDE',
        					'EL VENADO',
        					'CABIMAS LAURELES',
        					'LAGUNILLAS',
        					'PUNTA GORDA',
        					'OAC OJEDA CENTRO',
        					'NUEVAS DELICIAS',
        					'GALERA DE MISOA',
        					'BEGOTE', 
        				]


	        ];

	    foreach ($localidades as $key => $value) {
	   		$estado = estados::create([
	   			'nombre' => $key,
	   			'region_id' =>6,
	   		]);
   		   	for ($j=0; $j<sizeof($value) ; $j++) { 

	         	localidades::create([
	            'nombre' => $value[$j],
	            'estado_id' => $estado->id,
	        	]);
    		}
    	}

    }
}
