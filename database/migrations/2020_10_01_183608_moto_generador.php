<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MotoGenerador extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('motogenerador', function (Blueprint $table) {
            $table->id();
            $table->integer('motor_id');
            $table->integer('generador_id');
            $table->integer('tipo_id');
            $table->timestamps();
        });
    }
    public function down()
    {
         Schema::dropIfExists('motogenerador');
    }
}
