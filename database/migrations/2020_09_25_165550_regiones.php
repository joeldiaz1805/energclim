<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Regiones extends Migration
{
    /**
     * Run the migrations.
   $table->timestamps();  *
     * @return void
     */
    public function up()
    {
         Schema::create('regiones', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->timestamps();
            
        });
    }
    public function down()
    {
         Schema::dropIfExists('regiones');
    }
}
