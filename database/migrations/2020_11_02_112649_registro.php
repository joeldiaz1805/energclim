<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Registro extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registro', function (Blueprint $table) {
            $table->id();
            $table->string('tipo_actividad')->nullable();
            $table->string('tipo_servicio')->nullable();
            $table->integer('equipo_id')->nullable();
            $table->integer('componente_id')->nullable();
            $table->integer('ubicacion_id')->nullable();
            $table->string('sala')->nullable();
            $table->string('piso')->nullable();
            $table->string('estatus')->nullable();
            $table->string('accion')->nullable();
            $table->string('trabajo')->nullable();
            $table->string('porcentaje')->nullable();
            $table->integer('cuadrilla_id')->nullable();
            $table->date('fecha_inicio')->nullable();
            $table->date('fecha_fin')->nullable();
            $table->string('ticket_cosec')->nullable();
            $table->string('name_informe')->nullable();
            $table->string('observaciones')->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registro');
    }
}
