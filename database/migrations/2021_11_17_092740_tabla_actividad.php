<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaActividad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datos_actividad', function (Blueprint $table) {
            $table->id();
            $table->integer('localidad_id')->nullable();
            $table->integer('usuario_id')->nullable();
            $table->integer('ticket_num')->nullable();
            $table->integer('responsable_id')->nullable();
            $table->integer('supervisor_id')->nullable();
            $table->string('instalacion')->nullable();
            $table->string('sala')->nullable();
            $table->date('fecha_emision')->nullable();
            $table->date('fecha_ejecucion')->nullable();
            $table->string('sistema_dc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
                Schema::dropIfExists('datos_actividad');

    }
}
