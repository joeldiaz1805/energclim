<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpsBB extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('ups_bb', function (Blueprint $table) {
            $table->id();
            $table->integer('ups_id');
            $table->integer('bancob_id');
            $table->integer('ubicacion_id')->nullable();
            $table->integer('tipo_id');
            
            $table->string('operatividad')->nullable();
            $table->string('fecha_instalacion')->nullable();
            $table->string('criticidad')->nullable();
            $table->string('observaciones')->nullable();

            $table->string('garantia')->nullable();
            $table->timestamps();
        });
    }
    public function down()
    {
         Schema::dropIfExists('ups_bb');
    }
}
