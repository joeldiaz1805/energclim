<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MarcaEquipos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create('marca_equipo', function (Blueprint $table) {
            $table->id();
            $table->integer('equipo_id')->nullable();
            $table->string('descripcion')->nullable();
            $table->integer('user_id')->nullable();
            $table->text('observacion')->nullable();
            $table->timestamps();
        });//
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marca_equipo');

        //
    }
}
