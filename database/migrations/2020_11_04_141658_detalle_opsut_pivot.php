<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DetalleOpsutPivot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalleopsutpivot', function (Blueprint $table) {
            $table->id();
            $table->integer('nodoopsut_id');
            $table->integer('equipo_id')->nullable();
            $table->string('kva')->nullable();
            $table->string('tr')->nullable();
            $table->string('amph')->nullable();
            $table->string('watts')->nullable();
            $table->string('detalles_equipo')->nullable();
            $table->string('serial',40)->nullable();
            $table->string('status',10)->nullable();
            $table->text('observacion')->nullable();
            $table->timestamps();
        });
    }

   
    public function down()
    {
        Schema::dropIfExists('detalleopsutpivot');
    }
}
