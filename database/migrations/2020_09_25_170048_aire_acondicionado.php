<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AireAcondicionado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::create('aire_acondicionado', function (Blueprint $table) {
            $table->id();
            $table->string('marca');
            $table->string('modelo');
            $table->string('serial')->nullable();
            $table->string('inventario_cant')->nullable();
            $table->string('tipo_equipo')->nullable();
            $table->string('tipo_correa')->nullable();
            $table->integer('nro_correa')->nullable();
            $table->integer('cant_correa')->nullable();
            $table->string('nro_circuitos')->nullable();
            $table->string('tipo_compresor')->nullable();
            $table->string('tipo_refrigerante')->nullable();
            $table->string('ton_refrig_instalada')->nullable();
            $table->string('ton_refrig_operativa')->nullable();
            $table->string('ton_refrig_faltante')->nullable();
            $table->string('volt_operacion')->nullable();

            $table->string('operatividad')->nullable();
            $table->date('fecha_instalacion')->nullable();
            $table->string('criticidad')->nullable();
            $table->string('garantia')->nullable();
            $table->string('observaciones')->nullable();
            $table->integer('ubicacion_id')->nullable();
            $table->string('tablero_control')->nullable();
            $table->integer('status_id')->nullable();
          $table->integer('porc_falla')->nullable();
            $table->string('user_id')->nullable();
            
           // $table->integer('ubicacion_id')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('aire_acondicionado');
    }
}
