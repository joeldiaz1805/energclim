<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Ubicacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::create('ubicacion', function (Blueprint $table) {
            $table->id();
            $table->integer('central_id');
            $table->string('piso')->nullable();
            $table->string('estructura')->nullable();
            $table->string('criticidad_esp')->nullable();
            $table->integer('responsable_id')->nullable();
            $table->integer('supervisor_id')->nullable();
            $table->string('sala')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ubicacion');
    }
}
