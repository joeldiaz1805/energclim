<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Componente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
         Schema::create('componente', function (Blueprint $table) {
            $table->id();
            $table->string('descripcion');
            $table->integer('equipo_id');
            $table->timestamps();
        });
    }
    public function down()
    {
         Schema::dropIfExists('componente');
    }
}
