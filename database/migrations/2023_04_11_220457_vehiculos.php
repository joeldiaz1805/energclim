<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Vehiculos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('vehiculos', function (Blueprint $table) {
            $table->id();
            $table->string('tipo')->nullable();
            $table->string('marca')->nullable();
            $table->string('modelo')->nullable();
            $table->string('placa')->nullable();
            $table->string('serial_motor')->nullable();
            $table->string('serial_chasis')->nullable();
            $table->string('serial_bateria')->nullable();
            $table->string('num_flota')->nullable();
            $table->string('ano')->nullable();
            $table->string('color')->nullable();
            $table->string('estatus')->nullable();
            $table->string('kilometraje')->nullable();
            $table->string('localidad_id')->nullable();
            $table->string('gerencia_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('vehiculos');
    }
}
