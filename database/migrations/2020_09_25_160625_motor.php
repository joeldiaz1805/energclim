<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Motor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
  public function up()
    {
        Schema::create('motor', function (Blueprint $table) {
            $table->id();
            $table->string('marca');
            $table->string('modelo');
            $table->string('estado_arranque')->nullable();
            $table->string('cap_tanq_aceite')->nullable();
            $table->string('cap_tanq_refrig')->nullable();
            $table->string('comb_tanq_princ')->nullable();
            $table->string('cantidad_filtro')->nullable();
            $table->string('modelo_filtro')->nullable();
            $table->string('cant_bat_arranq')->nullable();
            $table->string('carg_bat')->nullable();
            $table->integer('cant_baterias')->nullable();
            $table->string('fases')->nullable();
            $table->string('amp_bateria')->nullable();
            $table->integer('status_id')->nullable();
            $table->integer('ubicacion_id')->nullable();
            $table->string('inventario_cant')->nullable();
            $table->string('serial')->nullable();
            $table->string('operatividad')->nullable();
            $table->string('fecha_instalacion')->nullable();
            $table->string('criticidad')->nullable();
            $table->string('garantia')->nullable();
            $table->string('observaciones')->nullable();
          $table->integer('porc_falla')->nullable();
            $table->string('user_id')->nullable();
             $table->string('cant_actual')->nullable();
            $table->string('ltrs_hora')->nullable();
             $table->string('aceite_actual')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('motor');
    }
}
