<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Climatizacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('climatizacion', function (Blueprint $table) {
            $table->id();
            $table->integer('region_id')->nullable();
            $table->integer('estado_id')->nullable();
            $table->integer('localidad_id')->nullable();
            $table->string('sala')->nullable();
            $table->string('piso')->nullable();
            $table->string('porcentaje')->nullable();
            $table->float('grados')->nullable();
            $table->string('observacion')->nullable();
            $table->string('user_id')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('climatizacion');
    }
}
