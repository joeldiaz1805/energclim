<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RectBcobb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('rect_bcobb', function (Blueprint $table) {
            $table->id();
            $table->integer('rect_id');
            $table->integer('bancob_id');
            $table->integer('ubicacion_id')->nullable();
            $table->integer('tipo_id');
           
            $table->string('operatividad')->nullable();
            $table->string('fecha_instalacion')->nullable();
            $table->string('criticidad')->nullable();
            $table->string('garantia')->nullable();
            $table->string('observaciones')->nullable();
            $table->timestamps();
        });
    }
    public function down()
    {
         Schema::dropIfExists('rect_bcobb');
    }
}
