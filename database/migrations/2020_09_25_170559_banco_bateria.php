<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BancoBateria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bco_baterias', function (Blueprint $table) {
            $table->id();
            $table->string('marca');
            $table->string('modelo');
            $table->string('serial')->nullable();
            $table->string('inventario_cant')->nullable();
            $table->string('cap_kva')->nullable();
            $table->string('carga_total_bb')->nullable();
            $table->string('elem_oper_bb')->nullable();
            $table->string('elem_inoper_bb')->nullable();
            $table->string('amp_elem')->nullable();
            $table->string('autonomia_respaldo')->nullable();
            $table->string('tablero')->nullable();
            $table->integer('status_id')->nullable();
             $table->string('fecha_instalacion')->nullable();
            $table->string('criticidad')->nullable();
            $table->integer('tipo_id')->nullable();
            $table->string('garantia')->nullable();
            $table->string('observaciones')->nullable();
            $table->string('porc_falla')->nullable();
            $table->string('operatividad')->nullable();
            $table->integer('ubicacion_id')->nullable();
            $table->string('user_id')->nullable();
             $table->string('tipo_rack')->nullable();
            $table->string('tiempo_util')->nullable();
            
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('bco_baterias');
    }
}
