<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CamposSupervisionActividad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('campos_supervision_actividad', function (Blueprint $table) {
            $table->id();
            $table->string('tipo_actividad')->nullable();
            $table->string('supervicion')->nullable();
            $table->integer('planilla_ups_id')->nullable();
            $table->string('personal_cosec')->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
