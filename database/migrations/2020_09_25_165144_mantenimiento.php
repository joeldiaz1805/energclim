<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Mantenimiento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('mantenimiento', function (Blueprint $table) {
            $table->id();
            $table->string('descripcion');
            $table->integer('tipo');
            $table->integer('falla_id');
            $table->timestamps();
        });
    }
    public function down()
    {
         Schema::dropIfExists('mantenimiento');
    }
}
