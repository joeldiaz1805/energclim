<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InvEquipOpsut extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('invequipopsut', function (Blueprint $table) {
            /*campos id equipo_id nombre  descripcion marca   modelo*/

            $table->id();
            $table->integer('id_2');
            $table->integer('equipo_id');
            $table->string('nombre',90)->nullable();
            $table->string('descripcion',120)->nullable();
            $table->string('marca',30)->nullable();
            $table->string('modelo',40)->nullable();
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('invequipopsut');
    }
}
