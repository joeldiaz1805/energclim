<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Comentarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
         Schema::create('comentarios', function (Blueprint $table) {
            $table->id();
            $table->text('descripcion');
            $table->integer('usuario_id');
            $table->integer('ticket_id');
            $table->integer('multimedia_id')->nullable();
            $table->timestamps();
        });
    }
    public function down()
    {
         Schema::dropIfExists('comentarios');
    }
}
