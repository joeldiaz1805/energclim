<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CeldasBb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('celdas_bb', function (Blueprint $table) {
            $table->id();
            $table->integer('planilla_bb_id')->nullable();
            $table->string('densidad')->nullable();
            $table->string('temperatura')->nullable();
            $table->string('tension')->nullable();
            $table->string('observacion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
    {
                Schema::dropIfExists('celdas_bb');

    }
}
