<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RequeOpsut extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requeopsut', function (Blueprint $table) {
            $table->id();
            $table->string('cod_inv_equipo_id',20)->nullable();
            $table->string('requerimiento')->nullable();
            $table->string('stat_req_nod',20)->nullable();
            $table->text('observacion');
            $table->integer('nodoopsut_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requeopsut');
    }
}
