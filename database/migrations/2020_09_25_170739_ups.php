<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Ups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ups', function (Blueprint $table) {
            $table->id();
            $table->string('marca');
            $table->string('modelo');
            $table->string('serial')->nullable();
            $table->string('inventario_cant')->nullable();
            $table->string('cap_kva')->nullable();
            $table->string('carga_conectada')->nullable();
            $table->string('tension_salida')->nullable();
            $table->integer('status_id')->nullable();
            $table->integer('ubicacion_id')->nullable();
          $table->integer('porc_falla')->nullable();
            $table->string('user_id')->nullable();
          

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ups');
    }
}
