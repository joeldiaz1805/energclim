<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Facturas extends Migration
{
    public function up()
    {
        Schema::create('facturas', function (Blueprint $table) {
            $table->id();
            $table->date('fecha')->nullable();
            $table->integer('contrato_id')->nullable();
            $table->integer('localidad_id')->nullable();
            $table->text('trabajo_realizado')->nullable();
            $table->float('monto_factura')->nullable();
            $table->integer('equipo_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('status')->nullable();
            $table->string('nro_factura')->nullable();
            $table->string('gerencia')->nullable();
            $table->string('n_pedido')->nullable();
            $table->string('modo_pago')->nullable();
            $table->date('fecha_recepcion')->nullable();
            $table->date('fecha_pago')->nullable();
            $table->string('monto_factura_usd')->nullable();
            $table->string('prioridad')->nullable();
            $table->string('justificacion')->nullable();


            $table->timestamps();
        });
    }

    
    public function down()
    {
        Schema::dropIfExists('facturas
            ');
    }
}
