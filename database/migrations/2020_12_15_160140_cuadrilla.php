<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Cuadrilla extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuadrilla', function (Blueprint $table) {
            $table->id();
            $table->string('nombre')->nullable();
            $table->integer('contratista_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('personal_id')->nullable();
       

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuadrilla');
    }
}
