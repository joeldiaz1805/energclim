<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NodoOpsut extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nodoopsut', function (Blueprint $table) {
            $table->id();
            $table->string('nombre',30);
            $table->string('codigo',20)->nullable();
            $table->integer('tipo');
            $table->text('ubicacion');
            $table->string('latitud',15)->nullable();
            $table->string('longitud',15)->nullable();
            $table->string('stat_nod',20)->nullable();
            $table->integer('estado_id');
            $table->integer('region_id');
            $table->integer('personal_id');
            $table->text('observacion');
            $table->timestamps();
     });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nodoopsut');
    }
}
