<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PlanillaUps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('planilla_ups', function (Blueprint $table) {
            $table->id();
            $table->string('marca')->nullable();
            $table->string('modelo')->nullable();
            $table->string('voltaje_ac_in')->nullable();
            $table->string('voltaje_ac_out')->nullable();
            $table->string('num_inv')->nullable();
            $table->string('porcentaje_carga')->nullable();
            $table->string('status')->nullable();
            $table->string('sn_ups')->nullable();
            $table->date('fecha_instalacion')->nullable();
            $table->integer('datos_actividad_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
