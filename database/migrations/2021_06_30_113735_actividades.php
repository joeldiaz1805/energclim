<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Actividades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('actividades', function (Blueprint $table) {
            $table->id();
            $table->date('fecha_inicio')->nullable();
            $table->date('fecha_fin')->nullable();
            $table->integer('localidad_id')->nullable();
            $table->string('actividad')->nullable();
            $table->text('descripcion')->nullable();
            $table->string('responsable')->nullable();
            $table->string('prioridad')->nullable();
            $table->string('status')->nullable();
            $table->string('departamento')->nullable();
            $table->string('gerencia')->nullable();
            $table->string('user_id')->nullable();
            $table->timestamps();
        });
    }

    
    public function down()
    {
        Schema::dropIfExists('actividades');
    }
}
