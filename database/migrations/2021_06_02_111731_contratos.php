<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Contratos extends Migration
{
   public function up()
    {
        Schema::create('contratos', function (Blueprint $table) {
            $table->id();
            $table->date('desde')->nullable();
            $table->date('hasta')->nullable();
            $table->string('nro_contrato')->nullable();
            $table->float('monto_asignado')->nullable();
            $table->integer('proveedor_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamps();
        });
    }

    
    public function down()
    {
        Schema::dropIfExists('contratos');
    }
}
