<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('last_name')->nullable();
            $table->string('email')->unique();
            $table->string('cedula')->nullable();
            $table->string('departamento')->nullable();
            $table->string('cargo')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->integer('rol_id')->nullable();
            $table->integer('supervisor_id')->nullable();
            $table->integer('active')->nullable();
            $table->integer('int_fail')->nullable();
            $table->string('password')->nullable();
            $table->integer('avatar')->nullable();
             $table->string('alias')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
//