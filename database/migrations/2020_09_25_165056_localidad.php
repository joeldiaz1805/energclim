<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Localidad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
         Schema::create('localidad', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->integer('estado_id');
            $table->string('tipo')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('status')->nullable();
            $table->string('codigo')->nullable();
            $table->string('latitud')->nullable();
            $table->string('longitud')->nullable();
            $table->string('responsable_id')->nullable();
            $table->string('tipo_opsut')->nullable();
            $table->string('supervisor_id')->nullable();
            $table->string('coordinador_id')->nullable();
            $table->string('anillos')->nullable();
            $table->integer('parroquia_id')->nullable();
             $table->integer('municipio_id')->nullable();
            $table->timestamps();
        });
    }
    public function down()
    {
         Schema::dropIfExists('localidad');
    }
}
