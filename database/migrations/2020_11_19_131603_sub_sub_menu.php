<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SubSubMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
     Schema::create('sub_sub_menu', function (Blueprint $table) {
            $table->id();
            $table->string('nombre',30);
            $table->integer('rol_id');
            $table->integer('sub_menu_id');
            $table->string('url')->nullable();
            $table->string('icon')->nullable();
            $table->text('observacion');
            $table->timestamps();
     });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_sub_menu');
    }
}
