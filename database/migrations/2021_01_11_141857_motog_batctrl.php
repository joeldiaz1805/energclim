<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MotogBatctrl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bateria_control', function (Blueprint $table) {
            $table->id();
            $table->integer('motog_id')->nullable();
            $table->integer('cantidad')->nullable();
            $table->string('amperaje')->nullable();
            $table->string('voltaje')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bateria_control');
    }
}
