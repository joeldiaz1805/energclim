<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Rectificadores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::create('rectificadores', function (Blueprint $table) {
            $table->id();
            $table->string('marca');
            $table->string('modelo');
            $table->string('serial')->nullable();
            $table->string('inventario_cant')->nullable();
            $table->string('num_fases')->nullable();
            $table->string('cant_oper')->nullable();
            $table->string('cant_inoper')->nullable();
            $table->string('cant_total')->nullable();
            $table->string('consumo_amp')->nullable();
            $table->string('carga_total_amp')->nullable();
            $table->string('volt_operacion')->nullable();
            $table->string('tablero')->nullable();
            $table->integer('ubicacion_id')->nullable();
          $table->integer('porc_falla')->nullable();
            $table->string('user_id')->nullable();
          

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('rectificadores');
    }
}
