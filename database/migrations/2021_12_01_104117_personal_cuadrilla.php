<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PersonalCuadrilla extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('personal_cuadrilla', function (Blueprint $table) {
            $table->id();
            $table->integer('cuadrilla_id')->nullable();
            $table->string('personal_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
    {
                Schema::dropIfExists('personal_cuadrilla');

    }
}
