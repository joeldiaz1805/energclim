<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TicketActividadPivot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_actividad_pivot', function (Blueprint $table) {
            $table->id();
            $table->integer('accion_id')->nullable();
            $table->string('ticket_actividad_id')->nullable();
            $table->string('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
                       Schema::dropIfExists('ticket_actividad_pivot');

    }
}
