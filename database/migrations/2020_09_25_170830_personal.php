<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Personal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::create('personal', function (Blueprint $table) {
            $table->id();
            $table->string('nombre',50)->nullable();
            $table->string('apellido',50)->nullable();
            $table->string('cedula',13)->nullable();
            $table->string('cod_p00',21)->nullable();
            $table->integer('region_id')->nullable();
            $table->string('num_oficina',17)->nullable();
            $table->string('num_personal',17)->nullable();
            $table->string('correo',50)->nullable();
            $table->string('cargo',50)->nullable();
            $table->integer('status')->nullable();
            $table->string('FotoPersonal')->nullable();
            $table->integer('estado_id')->nullable();
            $table->integer('supervisor_id')->nullable();
            $table->string('unidad')->nullable();
            $table->string('gerencia')->nullable();

            $table->timestamps();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal');
    }
}
