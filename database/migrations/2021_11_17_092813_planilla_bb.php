<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PlanillaBb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('planilla_bb', function (Blueprint $table) {
            $table->id();
            $table->string('marca')->nullable();
            $table->string('modelo')->nullable();
            $table->string('tipo')->nullable();
            $table->string('v_total')->nullable();
            $table->string('v_carga')->nullable();
            $table->string('num_inv')->nullable();
            $table->string('cap_ah')->nullable();
            $table->string('celdas')->nullable();
            $table->date('fecha_instalacion')->nullable();
            $table->integer('datos_actividad_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
                Schema::dropIfExists('planilla_bb');

    }
}
