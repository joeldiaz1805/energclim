<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Municipios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('municipios', function (Blueprint $table) {
            $table->id();
            $table->integer('estado_id')->nullable();
            $table->string('descripcion')->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamps();
        });
    }

   
     public function down()
    {
                Schema::dropIfExists('municipios');

    }
}
