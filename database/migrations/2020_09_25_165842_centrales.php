<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Centrales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
         Schema::create('centrales', function (Blueprint $table) {
            $table->id();
            $table->integer('localidad_id');
            $table->integer('region_id');
            $table->integer('estado_id');
            $table->string('area')->nullable();
            $table->string('tipo')->nullable();
            $table->timestamps();
        });
    }
    public function down()
    {
         Schema::dropIfExists('centrales');
    }
}
