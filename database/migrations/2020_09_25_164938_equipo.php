<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Equipo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
         Schema::create('equipo', function (Blueprint $table) {
            $table->id();
            $table->string('descripcion');
            $table->integer('localidad_id');
            $table->integer('responsable_id');
            $table->integer('servicio_id')->nullable();
            $table->timestamps();
        });
    }
    public function down()
    {
         Schema::dropIfExists('equipo');
    }
}
