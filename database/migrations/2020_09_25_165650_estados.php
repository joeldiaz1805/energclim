<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Estados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('estados', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->integer('region_id');
            $table->timestamps();
        });
    }
    public function down()
    {
         Schema::dropIfExists('estados');
    }
}
