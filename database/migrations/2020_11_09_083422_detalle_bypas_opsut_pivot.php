<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DetalleBypasOpsutPivot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detallebypasopsut', function (Blueprint $table) {
            $table->id();
            $table->integer('nodoopsut_bypas_id');
            $table->string('nodoopsut_bypas_codigo1',20);
            $table->string('nodoopsut_bypas_codigo2',20);
            $table->string('observacion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detallebypasopsut');
    }
}
