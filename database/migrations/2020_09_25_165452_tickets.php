<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Tickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->string('numero');
            $table->integer('equipo_id')->nullable();
            $table->integer('componente_id')->nullable();
            $table->integer('responsable_id')->nullable();
            $table->integer('usuario_id')->nullable();
            $table->integer('central_id')->nullable();
            $table->integer('tipo_falla')->nullable();
            $table->integer('multimedia_id')->nullable();
            $table->string('tipo_servicio')->nullable();
            $table->string('tipo_mantenimiento')->nullable();
            $table->string('tipo_actividad')->nullable();
            $table->string('telefono')->nullable();
            $table->string('sala')->nullable();
            $table->string('piso')->nullable();
            $table->string('cargo')->nullable();
            $table->string('correo')->nullable();
            $table->string('correo_supervisor')->nullable();
            $table->string('status')->nullable();
            $table->date('fecha_inicio')->nullable();
            $table->date('fecha_fin_falla')->nullable();
            $table->string('hora_inicio')->nullable();
            $table->string('hora_fin_actividad')->nullable();
            $table->timestamps();
        });
    }
    public function down()
    {
         Schema::dropIfExists('tickets');
    }
}
