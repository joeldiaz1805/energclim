<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

 protected $table="users";

     protected $fillable = [
        'name',
        'last_name',
        'rol_id',
        'email',
        'active',
        'password',
    ];
    public function rol(){
        return $this->belongsTo('App\Models\Models\roles', 'rol_id');
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

}
