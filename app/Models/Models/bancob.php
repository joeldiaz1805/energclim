<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class bancob extends Model
{
    use HasFactory;

     protected $table = "bco_baterias";

    protected $fillable=
    ['marca',
	'modelo',
	'serial',
	'inventario_cant',
	'cap_kva',
	'carga_total_bb',
	'elem_oper_bb',
	'elem_inoper_bb',
	'amp_elem',
	'autonomia_respaldo',
	'tablero',
	'status_id',
	'operatividad',
	'fecha_instalacion',
    'criticidad',
    'tipo_id',
	'garantia',
    'observaciones',
    'porc_falla',
	'ubicacion_id',
  'user_id',
  'tipo_rack',
  'tiempo_util',
  'cant_baterias',
  'cant_bat_danadas',
];
	    public static $rule=[
           'bco_baterias.marca'=> 'required',
           'bco_baterias.modelo'=> 'required',
           'bco_baterias.autonomia_respaldo'=> 'required',
           'bco_baterias.operatividad'=> 'required',
           'bco_baterias.elem_oper_bb'=> 'required|numeric',
           'bco_baterias.observaciones'=> 'required|min:1|max:350',

           /*
           'bco_baterias.carga_total_bb'=> 'required',
           'bco_baterias.serial'=> 'required',
           'bco_baterias.inventario_cant'=> 'required',
           'bco_baterias.cap_kva'=> 'required|numeric',
           'bco_baterias.elem_inoper_bb'=> 'required|numeric',
           'bco_baterias.amp_elem'=> 'required',
           'bco_baterias.tablero'=> 'required',
           'bco_baterias.tipo_rack'=> 'required',
           'bco_baterias.tiempo_util'=> 'required|numeric',
           'bco_baterias.porc_falla'=> 'required_if:bco_baterias.operatividad,Falla',
           'bco_baterias.fecha_instalacion'=> 'required',
           'bco_baterias.criticidad'=> 'required',
           'bco_baterias.garantia'=> 'required',

            'ubicacion.piso'              => 'required|min:1',
            'ubicacion.estructura'        => 'required',
            'ubicacion.sala'              => 'required',
*/



            'central.localidad_id'        => 'required',
            'central.region_id'           => 'required',
            'central.estado_id'           => 'required',
        ];

        public function ubicacion(){
        	return $this->belongsTo('App\Models\Models\ubicacion', 'ubicacion_id');
        } 
        public function usuario(){
            return $this->belongsTo('App\Models\Models\usuarios', 'user_id');
        }
	
}
