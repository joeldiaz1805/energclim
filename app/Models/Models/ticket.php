<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ticket extends Model
{
    use HasFactory;


    protected $table = "tickets";

    protected $fillable = [
        'numero',
		'equipo_id',
		'componente_id',
		'responsable_id',
        'central_id',
		'usuario_id',
		'tipo_falla',
		'tipo_servicio',
        'multimedia_id',
		'tipo_actividad',
		'telefono',
		'sala',
		'piso',
		'cargo',
		'correo',
		'correo_supervisor',
        'tipo_mantenimiento',
		'status',
		'fecha_inicio',
        'hora_inicio',
        'hora_fin_actividad',
		'fecha_fin_falla',
    ];


    public function central(){
    	return $this->belongsTo('App\Models\Models\localidades','central_id');
    }
    public function equipo(){
        return $this->belongsTo('App\Models\Models\equipo','equipo_id');
    }
    public function componente(){
    	return $this->belongsTo('App\Models\Models\componente','componente_id');
    }
    public function falla(){
        return $this->belongsTo('App\Models\Models\fallas','tipo_falla');
    }
    public function usuario(){
    	return $this->belongsTo('App\Models\Models\usuarios','usuario_id');
    }
     public function responsable(){
    	return $this->belongsTo('App\Models\Models\personal','responsable_id');
    }
    public function comentarios(){
        return $this->hasMany('App\Models\Models\Comentarios');
    }
     public function multimedia(){
        return $this->belongsTo('App\Models\Models\multimedia','multimedia_id');
    }
    public function actividad(){
        return $this->belongsTo('App\Models\Models\mantenimiento','tipo_actividad');
    }
}
