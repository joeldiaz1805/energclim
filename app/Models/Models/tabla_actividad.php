<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tabla_actividad extends Model
{
    use HasFactory;

    protected $table ="datos_actividad";
    protected $fillable =[
        'ticket_num',
        'responsable_id',
        'localidad_id',
        'supervisor_id',
        'instalacion',
        'sala',
        'fecha_emision',
        'fecha_ejecucion',
        'sistema_dc',
        'usuario_id'
    ];
     public function planilla_bb(){
        return $this->hasMany('App\Models\Models\planilla_bb', 'datos_actividad_id');
    }
     public function responsable(){
        return $this->belongsTo('App\Models\Models\personal', 'responsable_id');
    }
     public function supervisor(){
        return $this->belongsTo('App\Models\Models\personal', 'supervisor_id');
    }
    public function localidad(){
        return $this->belongsTo('App\Models\Models\localidades', 'localidad_id');
    }

    public function usuario(){
        return $this->belongsTo('App\Models\Models\usuarios', 'usuario_id');
    }
}
