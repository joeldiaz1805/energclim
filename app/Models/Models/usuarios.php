<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class usuarios extends Model
{
    use HasFactory;
    protected $table="users";

     protected $fillable = [
        'name',
        'last_name',
        'rol_id',
        'cedula',
        'departamento',
        'cargo',
        'email',
        'region_id',
        'supervisor_id',
        'active',
        'password',
        'alias',
        'avatar'
    ];
    protected $rules=[
        'set_password_certificate' => 'required|regex:/^(?=*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9]).{8,}$/'
    ];

    public function rol(){
        return $this->belongsTo('App\Models\Models\roles', 'rol_id');
    }
    public function region(){
        return $this->belongsTo('App\Models\Models\regiones', 'region_id');
    }
     public function supervisor(){
        return $this->belongsTo('App\Models\Models\usuarios', 'supervisor_id');
    }

    public function permisos(){
        return $this->hasMany('App\Models\Models\permisos','user_id');
    }

    public function localidades(){
        return $this->hasMany('App\Models\Models\localidades', 'user_id');
    }

     public function img(){
        return $this->belongsTo('App\Models\Models\Multimedia', 'avatar');
}
     public static function helper($met){

        switch ($met) {
            case 'GET':
                return 'Ver';
                break;
            case 'POST':
                return 'Guardar';
                break;
            case 'PUT':
                return 'Actualizar';
                break;
            case 'DELETE':
                return 'Eliminar';
                break;
            
            default:
                 return 'NoMethod';
                break;
        }
    }
}
