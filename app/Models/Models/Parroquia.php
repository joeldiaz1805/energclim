<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Parroquia extends Model
{
 
    use HasFactory;
     protected $table ="parroquias";

    protected $fillable = [
        'municipio_id',
        'descripcion',
        'user_id'];


         public function municipio(){
            return $this->belongsTo('App\Models\Models\Municipio', 'municipio_id');
        } 

        public function usuario(){
            return $this->belongsTo('App\Models\Models\usuarios', 'user_id');
        } 
}
