<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class regiones extends Model
{
    use HasFactory;
     protected $table = "regiones";
      

    protected $fillable = [
        'nombre'
    ];

public function regiones(){
        return $this->belongsTo('App\Models\Models\personal', 'region_id');
}
public function nodoopsut(){
        return $this->belongsTo('App\Models\Models\nodoopsut', 'region_id');

 }   
}
