<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class permisos extends Model
{
    use HasFactory;
    protected $table="permisos_rol";

     protected $fillable = [
        'user_id',
        'permiso',
    ];
    public function usuario(){
        return $this->belongsTo('App\Models\Models\user', 'user_id');
    }
   
}
