<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;
class roles extends Model
{
    use HasFactory;
     protected $table="roles";

     protected $fillable = [
        'denominacion',
        'descripcion',
    ];

    public function usuarios(){
    	return $this->belongsTo('App\Models\Models\usuarios');
    }

    
     public function menus()
    {
        return $this->belongsToMany('App\Models\Models\Menu','menu_roles','rol_id');
    }

    public function menusrol()
    {
        return $this->belongsToMany('App\Models\Models\Menu')->where('rol_id',Auth::user()->rol_id);
    }

    public function submenus()
    {
        return $this->belongsToMany('App\Models\Models\SubMenu','submenu_roles','rol_id','submenu_id');
    }
     public function SubSubMenu()
    {
        return $this->belongsToMany('App\Models\Models\SubSubMenu','sub_submenu_roles','rol_id','sub_submenu_id');
    }
}
