<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PersonalCuadrilla extends Model
{
    use HasFactory;

 protected $table="personal_cuadrilla";

     protected $fillable = [
        'cuadrilla_id',
        'personal_id',
    ];
    public function personal(){
        return $this->belongsTo('App\Models\Models\personal', 'personal_id');
    }public function cuadrilla(){
        return $this->belongsTo('App\Models\Models\cuadrilla', 'cuadrilla_id');
    }


}
