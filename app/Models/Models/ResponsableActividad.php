<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ResponsableActividad extends Model
{
    use HasFactory;
    
     protected $table = "responsable_actividad";
    protected $fillable =[
		'responsable_id',
		'actividad_id',
    ];

   
    public function responsable(){
        return $this->belongsTo('App\Models\Models\personal','responsable_id');
    }

     public function actividad(){
        return $this->belongsTo('App\Models\Models\actividades','actividad_id');
    }

}
