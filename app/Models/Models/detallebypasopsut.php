<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class detallebypasopsut extends Model
{
     use HasFactory;
    protected $table ="detalleopsutpivot";

    protected $fillable=[
    		'nodoopsut_bypas_id',
			'nodoopsut_bypas_codigo1',
			'nodoopsut_bypas_codigo2',
			'observacion'
	        ];
            
public function nodoopsut(){
    	return $this->belongsTo('App\Models\Models\nodoopsut','nodoopsut_codigo');
    }

}
