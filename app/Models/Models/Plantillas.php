<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plantillas extends Model
{
    use HasFactory;

    protected $table = "plantillas";

    protected $fillable = [
        'formato_id',
        'descripcion',
        'user_id',
    ];
 
 public static $rule=[
           'descripcion' =>'required|min:5',
           'formato_id'  =>'required|mimes:pdf',
        ];
     public function Multimedia(){
        return $this->belongsTo('App\Models\Models\Multimedia', 'formato_id');
	}
	public function usuario(){
        return $this->belongsTo('App\Models\Models\usuarios','user_id');
    }
}
