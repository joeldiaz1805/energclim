<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class invequipopsut extends Model
{
    use HasFactory;

 protected $table = "invequipopsut";

    protected $fillable = [
	
            'id_2',
            'equipo_id',
            'nombre',
            'descripcion',
            'marca',
            'modelo'
            ];

 public function equipo(){
        return $this->belongsTo('App\Models\Models\equipo', 'equipo_id');
}

public function invequipo(){
        return $this->hasMany('App\Models\Models\detalleopsutpivot', 'inv_equipo_id');

}

public function tipoequipo(){
        return $this->belongsTo('App\Models\Models\detalleopsutpivot','equipo_id','inv_equipo_id');
}

 public function invequipo2(){
        return $this->hasMany('App\Models\Models\requeopsut', 'cod_inv_equipo_id');

}
}
