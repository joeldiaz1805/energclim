<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class registro extends Model
{
    use HasFactory;


 protected $table = "registro";

    protected $fillable = [
        'tipo_servicio',
        'tipo_actividad',
		'equipo_id',
		'componente_id',
		'ubicacion_id',
		'estatus',
		'accion',
		'trabajo',
		'porcentaje',
        'cuadrilla_id',
        'sala',
        'user_id',
        'piso',
		'fecha_inicio',
		'fecha_fin',
		'ticket_cosec',
		'informe',
		'name_informe',
		'observaciones',
    ];

     public static  $rule=array(
            'motor.marca'                => 'required|max: 50',
            'motor.modelo'                => 'required|max: 50',
            'motor.operatividad'          => 'required',
            'generador.cap_kva'           => 'required|integer|min:1',
            'central.localidad_id'        => 'required',
            'central.region_id'           => 'required',
            'central.estado_id'           => 'required',
       

        );


    public function ubicacion(){//ubicacion esta aqui,, de aqui accedemos a;
    	return $this->belongsTo('App\Models\Models\centrales','ubicacion_id');
    	
    }


    public function informe(){//ubicacion esta aqui,, de aqui accedemos a;
        return $this->belongsTo('App\Models\Models\Multimedia','name_informe');
        
    }

    public function equipo(){
    	return $this->belongsTo('App\Models\Models\equipo','equipo_id');
    } 
    public function usuario(){
        return $this->belongsTo('App\Models\Models\usuarios','user_id');
    }
    public function componente(){
    	return $this->belongsTo('App\Models\Models\componente','componente_id');
    }

    public function cuadrilla(){
        return $this->belongsTo('App\Models\Models\cuadrilla','cuadrilla_id');
    }
    
    /*


    */
}
