<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;



class personal extends Model
{
    use HasFactory;
    protected $table = "personal";

    protected $fillable = [
        'nombre',
        'apellido',
        'cedula',
        'cod_p00',
        'supervisor_id',
        'region_id',
        'estado_id',
        'num_oficina',
        'num_personal',
        'correo',
        'cargo',
        'status',
        'FotoPersonal',
        'unidad',
        'gerencia'
    ];
    
    public function regiones(){
        return $this->belongsTo('App\Models\Models\regiones', 'region_id');

    }
    public function supervisor(){
        return $this->belongsTo('App\Models\Models\personal', 'supervisor_id');

    }

    public function estado(){
        return $this->belongsTo('App\Models\Models\estados', 'estado_id');

    }
     public function Multimedia(){
        return $this->belongsTo('App\Models\Models\Multimedia', 'FotoPersonal');
    }

    public function ubicacion(){
        return $this->hasOne('App\Models\Models\ubicacion', 'responsable_id');
    }
}
