<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ups extends Model
{
    use HasFactory;

    protected $table = "ups";

     protected $fillable=
     [
     'marca',
		'modelo',
		'serial',
		'inventario_cant',
		'cap_kva',
		'carga_conectada',
		'tension_salida',
		'status_id',
        'ubicacion_id'
	];
	 public function ubicacion(){
    	return $this->belongsTo('App\Models\Models\ubicacion', 'ubicacion_id');
    }
    public function responsable(){
    	return $this->belongsTo('App\Models\Models\personal', 'responsable_id');
    }
}
