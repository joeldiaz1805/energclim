<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class roles_subsubm extends Model
{
    use HasFactory;
    protected $table="sub_submenu_roles";
     protected $fillable =[
		'rol_id',
		'sub_submenu_id',
    ];

    public function sub_submenu(){
        return $this->hasMany('App\Models\Models\SubSubMenu','sub_submenu_id');
    }
    public function rol(){
        return $this->belongsTo('App\Models\Models\roles','rol_id');
    }
}
