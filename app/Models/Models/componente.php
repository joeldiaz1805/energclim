<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class componente extends Model
{
    use HasFactory;

    protected $table ="componente";

    protected $fillable = [
		'descripcion',
		'equipo_id',
		
    ];

    public function equipo(){
    	return $this->belongsTo('App\Models\Models\equipo','equipo_id');
    }

}
