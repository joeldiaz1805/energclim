<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class climatizacion extends Model
{
    use HasFactory;

    protected $table ="climatizacion";
    protected $fillable = [
       'region_id',
		'estado_id',
		'localidad_id',
		'sala',
		'piso',
		'porcentaje',
		'grados',
		'observacion',
        'user_id',
    ];
     public static $rule=[
            'region_id'         => 'required',
            'estado_id'         => 'required',
            'localidad_id'      => 'required',
            'sala'       		=> 'required',
            'piso'          	=> 'required',
            'porcentaje'        => 'required|numeric',
            'grados'           	=> 'required|numeric|between:0.00,50.99',
            'observacion'       => 'max:255',
        ];

    public function localidad(){
    	return $this->belongsTo('App\Models\Models\localidades', 'localidad_id');
    }
    public function region(){
        return $this->belongsTo('App\Models\Models\regiones', 'region_id');
    }
    public function estados(){
        return $this->belongsTo('App\Models\Models\estados', 'estado_id');
    }
    public function usuario(){
        return $this->belongsTo('App\Models\Models\usuarios', 'user_id');
    }
}
