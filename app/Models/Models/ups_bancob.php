<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ups_bancob extends Model
{
    use HasFactory;

    protected $table = "ups_bb";

     protected $fillable = [
        'ups_id',
        'bancob_id',
        'operatividad',
    		'fecha_instalacion',
    		'criticidad',
        'tipo_id',
    		'garantia',
    		'ubicacion_id',
    		'operatividad',
        'porc_falla',
        'observaciones'
    
	];
     public static $rule=[
           'ups.marca'=> 'required',
           'ups.modelo'=> 'required|min:2',
           'ups.serial'=> 'required|min:2',
           'ups.inventario_cant'=> 'required|min:2',
           'ups.cap_kva'=> 'required|min:2|numeric',
           'ups.carga_conectada'=> 'required|min:2',
           'ups.tension_salida'=> 'required|min:2',

           'bco_baterias.marca'=> 'required',
           'bco_baterias.modelo'=> 'required',
           'bco_baterias.serial'=> 'required',
           'bco_baterias.inventario_cant'=> 'required',
           'bco_baterias.cap_kva'=> 'required|numeric',
           'bco_baterias.carga_total_bb'=> 'required',
           'bco_baterias.elem_oper_bb'=> 'required|numeric',
           'bco_baterias.elem_inoper_bb'=> 'required|numeric',
           'bco_baterias.amp_elem'=> 'required',
           'bco_baterias.autonomia_respaldo'=> 'required',
           'bco_baterias.tablero'=> 'required',
           
           'ups_bcob.operatividad'=> 'required',
           'ups_bcob.fecha_instalacion'=> 'required',
           'ups_bcob.criticidad'=> 'required',
           'ups_bcob.garantia'=> 'required',
           'ups_bcob.observaciones'=> 'required|min:1|max:350',



            'central.region_id'           => 'required',
            'central.estado_id'           => 'required',
            'central.localidad_id'        => 'required',

            'ubicacion.estructura'        => 'required',
            'ubicacion.sala'             => 'required',
            'ubicacion.piso'              => 'required|min:1',
            
            'responsable.nombre'          => 'required|min:2|max:20',
            'responsable.apellido'        => 'required|min:2|max:20',
            'responsable.cargo'           => 'required|min:2|max:20',
            'responsable.cedula'          => 'required|min:7|max:10',
            'responsable.cod_p00'         => 'required|min:3|max:15',
            'responsable.num_oficina'     => 'required|min:11|max:13',
            'responsable.num_personal'    => 'required|min:11|max:13',
            'responsable.correo'          => 'required|email',
        ];

	 public function ups(){
    	return $this->belongsTo('App\Models\Models\ups', 'ups_id');
    }
    public function bancob(){
    	return $this->belongsTo('App\Models\Models\bancob', 'bancob_id');
    }
    public function ubicacion(){
    	return $this->belongsTo('App\Models\Models\ubicacion', 'ubicacion_id');
    }
}
