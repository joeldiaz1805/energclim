<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class acciones extends Model
{
    use HasFactory;
    protected $table = "acciones";

    protected $fillable = ['nombre','actividad','tipo_fluido'];


    public function activ($name){

        return acciones::wherenombre($name)->pluck('actividad');
    }
}
