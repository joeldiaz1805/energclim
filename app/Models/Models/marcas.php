<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class marcas extends Model
{
    use HasFactory;


    protected $table = "marca_equipo";
    protected $fillable = [
    	'equipo_id',
    	'descripcion',
    	'user_id',
    	'observacion',
    ];

    public static $rule=[
            'equipo_id'                => 'required',
            'descripcion'                => 'required|min:2',
            'observacion'                => 'min:2|max:300',
            
        ];
	
	public function usuario(){
        return $this->belongsTo('App\Models\Models\usuarios', 'user_id');
    }
    public function equipo(){
        return $this->belongsTo('App\Models\Models\equipo', 'equipo_id');
    }
    




}
