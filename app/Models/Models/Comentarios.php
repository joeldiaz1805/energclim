<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comentarios extends Model
{
    use HasFactory;

     protected $table = "comentarios";

    protected $fillable = [
        'descripcion', 
        'usuario_id',
        'ticket_id',
        'multimedia_id'
    ];

     public function multimedia(){
        return $this->belongsTo('App\Models\Models\Multimedia','multimedia_id');
    }
    public function tikect(){
        return $this->belongsTo('App\Models\Models\ticket','ticket_id');
    }
    public function usuario(){
        return $this->belongsTo('App\Models\Models\usuarios','usuario_id');
    }
}
