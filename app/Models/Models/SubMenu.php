<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;

class SubMenu extends Model
{
    use HasFactory;
    protected $table = "sub_menu";
    protected $fillable =[
    	'nombre',
		'rol_id',
		'menu_id',
		'url',
		'icon',
		'observacion',
    ];
    public function menu(){
    	return $this->belongsTo('App\Models\Models\Menu','menu_id');
    }

     public function SubSub(){
        return $this->hasMany('App\Models\Models\SubSubMenu')->where('rol_id',Auth::user()->rol_id);
    }

    public function roles()
    {
        return $this->belongsToMany('App\Models\Models\roles','submenu_roles','submenu_id','rol_id')->where('rol_id',Auth::user()->rol_id);
    }
}
