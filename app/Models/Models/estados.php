<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class estados extends Model
{
    use HasFactory;

    protected $table = "estados";

    protected $fillable = [
        'nombre', 
        'region_id'
    ];
     public function region(){
        return $this->belongsTo('App\Models\Models\regiones', 'region_id');
    }

}
