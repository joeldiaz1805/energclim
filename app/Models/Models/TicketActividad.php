<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TicketActividad extends Model
{
    use HasFactory;
    protected $table = "ticket_actividad";
    

    protected $fillable =[


        'nombre',
        'descripcion',
        'user_id'
    ];
}
