<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cuestionario extends Model
{
    use HasFactory;
    protected $table = "Cuestionario_ups";

    protected $fillable= [
        'user_id',
        'descripcion',
    ];

      public function usuario(){
        return $this->belongsTo('App\Models\Models\usuarios','user_id');
    }
}
