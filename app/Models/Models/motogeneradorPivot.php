<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class motogeneradorPivot extends Model
{
    use HasFactory;
     protected $table = "motogenerador";

    protected $fillable = [
        'motor_id',
        'generador_id',
        'tipo_id'
    ];


     public static  $rule=array(
            'motor.marca'                => 'required|max: 50',
            'motor.modelo'                => 'required|max: 50',
            'motor.comb_tanq_princ'        =>'required|integer',
            'motor.cantidad_filtro'        =>'required|numeric',
            'motor.cap_tanq_refrig'        =>'required|numeric',
            'motor.cap_tanq_aceite'        =>'required|numeric',//este modificado
            'motor.cant_bat_arranq'        =>'required|integer',
            
            'motor.operatividad'          => 'required',
            'generador.cap_kva'           => 'required|integer|min:1',
            'central.localidad_id'        => 'required',
            'central.region_id'           => 'required',
            'central.estado_id'           => 'required',
     
        );
         public static  $rule_nivel=array(
            'ltrs_hora'                => 'integer',
            'ltrs_dia'                => 'integer',
            'cap_tanq_aceite'                => 'integer',
            'aceite_actual'                => 'integer',
           'cant_actual'                => 'required|integer|max:7500'
          
     
        );


    public function motor(){
    	return $this->belongsTo('App\Models\Models\motogenerador','motor_id');
    }
    
    public function generador(){
    	return $this->belongsTo('App\Models\Models\generador','generador_id');
    }

    public function tipo(){
        return $this->belongsTo('App\Models\Models\equipo','tipo_id');
    }
    
}
