<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BatteryCtrl extends Model
{
    use HasFactory;
    protected $table = "bateria_control";

    protected $fillable=
    ['motog_id',
	'cantidad',
	'amperaje',
	'voltaje'
	];

	public function motor(){
    	return $this->belongsTo('App\Models\Models\motogenerador','motog_id');
    }
}
