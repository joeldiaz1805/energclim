<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class requeopsut extends Model
{
    use HasFactory;
	 protected $table = "requeopsut";
	    protected $fillable = [
			'cod_inv_equipo_id',
            'requerimiento',
            'stat_req_nod',
            'observacion',
            'nodoopsut_id'
       ];

	public function nodoopsut(){
        return $this->belongsTo('App\Models\Models\nodoopsut', 'nodoopsut_id');

	}
    public function invequipo2(){
        return $this->belongsTo('App\Models\Models\invequipopsut', 'cod_inv_equipo_id');
    }

    public function detalleopsutpivot(){
    return $this->belongsTo('App\Models\Models\detalleopsutpivot', 'nodoopsut_id');

}
    public function invequipopsut(){
    return $this->belongsTo('App\Models\Models\invequipopsut', 'cod_inv_equipo_id');
}
}
