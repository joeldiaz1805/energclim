<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TicketActividadPivot extends Model
{
    use HasFactory;
    protected $table = "ticket_actividad_pivot";
    
    protected $fillable=[

        'accion_id',
        'ticket_actividad_id',
        'user_id'

    ];
}
