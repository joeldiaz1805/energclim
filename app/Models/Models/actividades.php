<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class actividades extends Model
{
    use HasFactory;
      protected $table = "actividades";

    protected $fillable=
    ['fecha_inicio',
	'fecha_fin',
	'localidad_id',
	'actividad',
	'descripcion',
	'responsable',
	'prioridad',
	'status',
	'departamento',
	'gerencia',
	'user_id',
	];

	 public static $rule=[
            'localidad_id'       => 'required',
            'departamento'       => 'required',
            'fecha_inicio'      => 'required|date',
            'actividad'       => 'required|max: 50',
            'prioridad'      => 'required',
            'status'       => 'required',
            'fecha_fin'       => 'required_if:status,1',
            'descripcion'       => 'required',
            'respon'       => 'required',
       ];

	public function usuario(){
    	return $this->belongsTo('App\Models\Models\usuarios','user_id');
    }
    public function localidad(){
    	return $this->belongsTo('App\Models\Models\localidades','localidad_id');
    }
    public function resp(){
        return $this->belongsTo('App\Models\Models\personal','responsable');
    }

     public function responsables()
    {
        return $this->belongsToMany('App\Models\Models\personal','responsable_actividad','actividad_id','responsable_id');
    }
}
