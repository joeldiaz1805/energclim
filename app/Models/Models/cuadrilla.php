<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class cuadrilla extends Model
{
    use HasFactory;
     protected $table = "cuadrilla";
      

    protected $fillable = [
        'nombre',
        'contratista_id',
        'personal_id',
        'user_id'
    ];

    public function contratista(){
    	return $this->belongsTo('App\Models\Models\contratista','contratista_id');
    }

    public function usuario(){
    	return $this->belongsTo('App\Models\Models\usuarios','user_id');
    }
    

 public function cuadrante(){
    	return $this->hasMany('App\Models\Models\PersonalCuadrilla','cuadrilla_id');
    }
    public function personal(){
        return $this->belongsTo('App\Models\Models\personal','personal_id');
    }
}
