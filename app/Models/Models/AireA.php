<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AireA extends Model
{
    use HasFactory;


 protected $table = "aire_acondicionado";

    protected $fillable = [
        'marca',
		'modelo',
		'serial',
		'inventario_cant',
		'tipo_equipo',
        'tipo_correa',
        'nro_correa',
        'cant_correa',
		'nro_circuitos',
		'tipo_compresor',
		'tipo_refrigerante',
        'ton_refrig_instalada',
		'ton_refrig_operativa',
		'ton_refrig_faltante',
		'volt_operacion',
		'operatividad',
        'porc_falla',
		'fecha_instalacion',
		'criticidad',
		'garantia',
		'observaciones',
		'tablero_control',
		'status_id',
		'ubicacion_id',
        'user_id',
		
    ];
     public static $rule=[
            'aireA.marca'                => 'required|max: 50',
            'aireA.modelo'                => 'required|min:2',
             
             
           /* 'aireA.serial'                => 'required|min:6|max: 50',
            'aireA.inventario_cant'       => 'required|min:6|max: 50',
            'aireA.tipo_equipo'           => 'required',
            'aireA.tipo_correa'           => 'required',
            'aireA.nro_correa'           => 'required|numeric',
            'aireA.cant_correa'           => 'required',
            'aireA.nro_circuitos'         => 'required',
            'aireA.ton_refrig_operativa'  => 'required|min:2|max:50',
            'aireA.ton_refrig_instalada'  => 'required|min:2|max:50',
            'aireA.ton_refrig_faltante'   => 'required|min:2|max: 50',
            'aireA.tipo_refrigerante'     => 'required',
            'aireA.volt_operacion'        => 'required',
            'aireA.tablero_control'        => 'required',
            'aireA.criticidad'          => 'required',
            'aireA.porc_falla'          => 'required_if:aireA.operatividad,Falla',
            'aireA.fecha_instalacion'     => 'required',
            'aireA.garantia'              => 'required',
            'aireA.observaciones'         => 'required|min:1|max:350',


            'ubicacion.piso'              => 'required|min:1',
            'ubicacion.estructura'        => 'required',
            'ubicacion.sala'              => 'required',

            'responsable.nombre'          => 'required|min:2|max:20',
            'responsable.apellido'        => 'required|min:2|max:20',
            'responsable.cedula'          => 'required|min:7|max:10',
            'responsable.cargo'           => 'required|min:2|max:20',
            'responsable.cod_p00'         => 'required|min:3|max:15',
            'responsable.num_oficina'     => 'required|min:11|max:13',
            'responsable.num_personal'    => 'required|min:11|max:13',
            'responsable.correo'          => 'required|email',
            */
            'aireA.operatividad'          => 'required',
            'central.localidad_id'        => 'required',
            'central.region_id'           => 'required',
            'central.estado_id'           => 'required',



        ];

      public function ubicacion(){
    	return $this->belongsTo('App\Models\Models\ubicacion', 'ubicacion_id');
    }
    public function usuario(){
        return $this->belongsTo('App\Models\Models\usuarios', 'user_id');
    }
    
    /*


    */
}
