<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class rect_bcobb extends Model
{
    use HasFactory;

     protected $table = "rect_bcobb";

     protected $fillable = [
        'rect_id',
        'bancob_id',
        'operatividad',
        'porc_falla',
	     	'fecha_instalacion',
        'criticidad',
        'tipo_id',
		    'garantia',
        'observaciones',
		    'ubicacion_id'
    
	];
    public static $rule=[
           'rectificador.marca'          => 'required',
           'rectificador.modelo'         => 'required|min:1',
           'rectificador.cant_oper'      => 'required|min:1',
           'rectificador.cant_inoper'    => 'required|min:1',
          /* 'rectificador.serial'         => 'min:2',
           'rectificador.inventario_cant'=> 'min:2',
           'rectificador.cant_total'     => 'min:2',
           'rectificador.num_fases'      => 'min:2',
           'rectificador.consumo_amp'    => 'min:2',
           'rectificador.carga_total_amp'=> 'min:2',
           'rectificador.volt_operacion' => 'min:2',
           'rectificador.tablero'        => 'min:2',
           'rectificador.num_fases'      => '',

           */
          // 'rect_bcobb.operatividad'=> 'required',
           'rect_bcobb.porc_falla'=> 'required_if:rect_bcobb.operatividad,Deficiente',
          // 'rect_bcobb.fecha_instalacion'=> 'required',
          // 'rect_bcobb.criticidad'=> 'required',
          // 'rect_bcobb.garantia'=> 'required',
           'rect_bcobb.observaciones'=> 'required|min:1|max:350',

           // 'ubicacion.piso'              => 'required|min:1',
           // 'ubicacion.estructura'        => 'required',
           // 'ubicacion.sala'              => 'required',

            'central.localidad_id'        => 'required',
           // 'central.region_id'           => 'required',
            //'central.estado_id'           => 'required',
            
           // 'responsable.nombre'          => 'min:2|max:20',
           // 'responsable.apellido'        => 'min:2|max:20',
           // 'responsable.cargo'           => 'min:2|max:20',
           // 'responsable.cedula'          => 'min:7|max:10',
           // 'responsable.cod_p00'         => 'min:3|max:15',
           // 'responsable.num_oficina'     => 'min:11|max:13',
           // 'responsable.num_personal'    => 'min:11|max:13',
            //  'responsable.correo'          => 'email',
        ];
	 public function rectificador(){
    	return $this->belongsTo('App\Models\Models\rectificadores', 'rect_id');
    }
    public function bancob(){
    	return $this->belongsTo('App\Models\Models\bancob', 'bancob_id');
    }
    public function ubicacion(){
    	return $this->belongsTo('App\Models\Models\ubicacion', 'ubicacion_id');
    } 
}
