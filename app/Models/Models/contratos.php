<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class contratos extends Model
{
    use HasFactory;

    protected $table = "contratos";
    protected $fillable =[
    	'desde',
		'hasta',
        'proveedor_id',
		'nro_contrato',
		'monto_asignado',
		'user_id'
    ];
     public static $rule=[
            'desde'                => 'required|date',
            'hasta'                => 'required|date',
            'nro_contrato'         => 'required|numeric',
            'monto_asignado'       => 'required|numeric',
            'proveedor_id'       => 'required',

        ];

    public function usuario(){
        return $this->belongsTo('App\Models\Models\usuarios', 'user_id');
    }
    public function proveedor(){
        return $this->belongsTo('App\Models\Models\proveedores', 'proveedor_id');
    }

    public function facturas(){
        return $this->hasMany('App\Models\Models\facturas','contrato_id');
    }


}
