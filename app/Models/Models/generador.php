<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class generador extends Model
{
    use HasFactory;
     protected $table = "generador";

    
        protected $fillable = [
        'marca', 
        'modelo', 
        'serial',
        'cap_kva',
        'tablero',
        'inventario_cant',
        'status_id'
    ];
}
