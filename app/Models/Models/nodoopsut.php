<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class nodoopsut extends Model
{
    use HasFactory;
     protected $table = "nodoopsut";
    protected $fillable = [
    	 'nombre',
         'codigo',
         'tipo',
         'ubicacion',
         'latitud',
         'longitud',
         'stat_nod',
         'estado_id',
         'region_id',
         'personal_id',
         'observacion'
    ];

      public static  $rule=array(
         'nombre'=> 'required',
         'codigo'=>'required|unique:nodoopsut,codigo',
         'tipo'=>'required',
         'ubicacion'=>'required',
         'latitud',
         'longitud',
         'stat_nod'=>'required',
         'estado_id'=>'required',
         'region_id'=>'required',
         'personal_id',
         'observacion'
               );
           
           
        public function regiones(){
                return $this->belongsTo('App\Models\Models\regiones', 'region_id');
            }
        public function estados(){
                return $this->belongsTo('App\Models\Models\estados', 'estado_id');
            }
        public function personal(){
                return $this->belongsTo('App\Models\Models\personal', 'personal_id');
            }
        public function detalleopsutpivot(){
            return $this->hasMany('App\Models\Models\detalleopsutpivot', 'nodoopsut_id');
        }






}
