<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ubicacion extends Model
{
    use HasFactory;

     protected $table = "ubicacion";

     protected $fillable=
     [
     'central_id',
	 'piso',
	 'estructura',
	 'criticidad_esp',
	 'responsable_id',
     'supervisor_id',
	 'sala',
	];
	 public function central(){
    	return $this->belongsTo('App\Models\Models\centrales', 'central_id');
    }
    public function responsable(){
    	return $this->belongsTo('App\Models\Models\personal', 'responsable_id');
    }
     public function supervisor(){
        return $this->belongsTo('App\Models\Models\personal', 'supervisor_id');
    }

    public function motorgenerador(){
        return $this->hasMany('App\Models\Models\motogenerador');
    }
    public function aire_acondicionado(){
        return $this->hasMany('App\Models\Models\AireA','ubicacion_id');
    }
    public function rectificadores(){
        return $this->hasMany('App\Models\Models\rectificadores');
    }
    public function ups(){
        return $this->hasMany('App\Models\Models\ups');
    }
}
