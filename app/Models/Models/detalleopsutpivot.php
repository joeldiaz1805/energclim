<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class detalleopsutpivot extends Model
{
    use HasFactory;
    protected $table ="detalleopsutpivot";

    protected $fillable=[
    		'nodoopsut_id', 
	        'equipo_id',
            'kva',
            'tr',
            'amph',
            'watts',
            'detalles_equipo',
	        'serial',
            'status',
	        'observacion'
	        ];
            
            public function nodoopsut(){
               	return $this->belongsTo('App\Models\Models\nodoopsut','nodoopsut_id');
            }
            public function equipo(){
                return $this->belongsTo('App\Models\Models\equipo','equipo_id');
            }
           


            // public function motogeneradorPivot(){
            //     	return $this->belongsTo('App\Models\Models\motogeneradorPivot','motogeneradorPivot_id');
            //     }
            // public function rect_bcobb(){
            //     	return $this->belongsTo('App\Models\Models\rect_bcobb','rect_bcobb_id');
            //     }
            // public function AireA(){
            //     	return $this->belongsTo('App\Models\Models\AireA','AireA_id');
            //     }
            // public function invequipo(){
            //         return $this->belongsTo('App\Models\Models\invequipopsut','inv_equipo_id');
            //     }

            // public function invequipoaire(){
            //         return $this->belongsTo('App\Models\Models\invequipopsut','inv_equipo_id');
            //     }





}