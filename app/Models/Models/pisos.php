<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pisos extends Model
{
    use HasFactory;
    protected $table = "pisos";

    protected $fillable = ['nombre'];
}
