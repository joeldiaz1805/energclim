<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class celdas_bb extends Model
{
    use HasFactory;

    protected $table = "celdas_bb";
    protected $fillable = [
            'planilla_bb_id',
            'densidad',
            'temperatura',
            'tension',
            'observacion'
                ];

    public function planilla_bb(){

        return $this->belongsTo('App\Models\Models\planilla_bb', 'planilla_bb_id');

    }
}
