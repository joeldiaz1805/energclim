<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubSubMenu extends Model
{
    use HasFactory;
    protected $table = "sub_sub_menu";
    protected $fillable =[
    	'nombre',
		'rol_id',
		'sub_menu_id',
		'url',
		'icon',
		'observacion',
    ];

    
     public function submenu(){
    	return $this->hasMany('App\Models\Models\SubMenu');
    } 

     public function roles()
    {
        return $this->belongsToMany('App\Models\Models\roles','roles_subsubm','sub_submenu_id','rol_id');
    }

     
    

}
