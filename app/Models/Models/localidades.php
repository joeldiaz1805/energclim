<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class localidades extends Model
{
    use HasFactory;
     protected $table = "localidad";

    protected $fillable = [
        'nombre', 
        'estado_id',
        'tipo',
        'user_id',
        'status',
        'codigo',
        'latitud',
        'longitud',
        'responsable_id',
        'tipo_opsut',
        'supervisor_id',
        'coordinador_id',
        'parroquia_id',
        'municipio_id',
        'anillos'
    ];
    public static $rule=[
           'nombre'=> 'required',
           'estado_id'=> 'required',
           'municipio_id'=>'required',
           'tipo'=> 'required',
           'supervisor.nombre'          => 'required|min:2|max:20',
           'supervisor.apellido'        => 'required|min:2|max:20',
           'supervisor.cedula'          => 'required|min:7|max:10',
           'supervisor.cargo'           => 'required|min:2|max:20',
           'supervisor.cod_p00'         => 'required|min:3|max:15',
           'supervisor.num_oficina'     => 'required|min:11|max:13',
           'supervisor.num_personal'    => 'required|min:11|max:13',
           'supervisor.correo'          => 'required|email',

           'coordinador.nombre'          => 'required|min:2|max:20',
           'coordinador.apellido'        => 'required|min:2|max:20',
           'coordinador.cedula'          => 'required|min:7|max:10',
           'coordinador.cargo'           => 'required|min:2|max:20',
           'coordinador.cod_p00'         => 'required|min:3|max:15',
           'coordinador.num_oficina'     => 'required|min:11|max:13',
           'coordinador.num_personal'    => 'required|min:11|max:13',
           'coordinador.correo'          => 'required|email',
           
        ];



    public function central(){
        return $this->hasMany('App\Models\Models\centrales','localidad_id');
    }
     public function estados(){
        return $this->belongsTo('App\Models\Models\estados', 'estado_id');
    }

    public function coordinador(){
        return $this->belongsTo('App\Models\Models\personal', 'coordinador_id');
    }
     public function supervisor(){
        return $this->belongsTo('App\Models\Models\personal', 'supervisor_id');
    }
    public function usuario(){
        return $this->belongsTo('App\Models\Models\usuarios', 'user_id');
    }
    public function parroquia(){
        return $this->belongsTo('App\Models\Models\Parroquia', 'parroquia_id');
    }
       public function municipio(){
        return $this->belongsTo('App\Models\Models\Municipio', 'municipio_id');
    }
}
