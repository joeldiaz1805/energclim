<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class motogenerador extends Model
{
    use HasFactory;


    protected $table = "motor";

    protected $fillable = [
        'marca', 
        'modelo', 
        'estado_arranque',
        'cap_tanq_aceite',
        'cap_tanq_refrig',
        'comb_tanq_princ',
        'cantidad_filtro',
        'modelo_filtro',
        'cap_bat_arranq',
        'cant_bat_arranq',
        'carg_bat',
        'cant_baterias',
        'fases',
        'operatividad',
        'porc_falla',
        'fecha_instalacion',
        'criticidad',
        'garantia',
        'observaciones',
        'amp_bateria',
        'status_id',
        'ubicacion_id',
        'inventario_cant',
        'serial',
        'cant_actual',
        'ltrs_hora',
        'user_id',
        'aceite_actual'

    ];
    public function ubicacion(){
    	return $this->belongsTo('App\Models\Models\ubicacion', 'ubicacion_id');
    }
    public function generador()
    {
        return $this->belongsToMany('App\Models\Models\generador');
    }

     public function batteryctrl()
    {
        return $this->hasMany('App\Models\Models\BatteryCtrl','motog_id');
    }
    public function usuario(){
        return $this->belongsTo('App\Models\Models\usuarios', 'user_id');
    }

}
