<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class contratista extends Model
{
    use HasFactory;

    protected $table = "contratista";

    protected $fillable = [
        'nombre',
        'cuadrilla_id', 
    ];

    public function cuadrilla(){
        return $this->belongsTo('App\Models\Models\cuadrilla', 'cuadrilla_id');

    }
}
