<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class mantenimiento extends Model
{
    use HasFactory;

    protected $table = "mantenimiento";

    
        protected $fillable = [
        'descripcion', 
        'falla_id', 
        'tipo', 
    ];


     public function falla(){
    	return $this->belongsTo('App\Models\Models\fallas','falla_id');
    }
}
