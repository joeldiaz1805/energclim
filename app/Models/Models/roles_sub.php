<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class roles_sub extends Model
{
    use HasFactory;

     protected $table = "submenu_roles";
    protected $fillable =[
		'rol_id',
		'submenu_id',
    ];

    public function submenu(){
        return $this->hasMany('App\Models\Models\SubMenu','submenu_id');
    }
    public function rol(){
        return $this->belongsTo('App\Models\Models\roles','rol_id');
    }
}
