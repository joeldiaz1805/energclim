<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class menurol extends Model
{
    use HasFactory;
     protected $table = "menu_roles";
    protected $fillable =[
		'rol_id',
		'menu_id',
    ];

    public function menu(){
        return $this->belongsTo('App\Models\Models\Menu');
    }
    public function rol(){
        return $this->belongsTo('App\Models\Models\roles','rol_id');
    }
}
