<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class rectificadores extends Model
{
    use HasFactory;


    protected $table = "rectificadores";

    protected $fillable =[
    	'marca',
		'modelo',
		'serial',
		'inventario_cant',
		'cant_oper',
		'num_fases',
		'cant_inoper',
		'cant_total',
		'consumo_amp',
		'volt_operacion',
		'carga_total_amp',
		'tablero	',
		'ubicacion_id',
		'user_id'
		
    ];
     public function ubicacion(){
    	return $this->belongsTo('App\Models\Models\ubicacion', 'ubicacion_id');
    }
    public function usuario(){
        return $this->belongsTo('App\Models\Models\usuarios', 'user_id');
    }
    
}
