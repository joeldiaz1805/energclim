<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class equipo extends Model
{
    use HasFactory;
    protected $table ="equipo";

    protected $fillable=[
    	'descripcion',
		'localidad_id',
		'responsable_id',
        'servicio_id'
    ];

    public function central(){
    	return $this->belongsTo('App\Models\Models\centrales','localidad_id');
    }
    public function responsable(){
    	return $this->belongsTo('App\Models\Models\personal','responsable_id');
    }
    public function equipo(){
            return $this->hasMany('App\Models\Models\invequipopsut', 'id');
    }
     public function componente(){
        return $this->hasMany('App\Models\Models\componente');
    }
    
}
