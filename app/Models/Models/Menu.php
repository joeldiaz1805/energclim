<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Menu extends Model
{
    use HasFactory;
    protected $table = "menu";
    protected $fillable =[
    	'nombre',
		'url',
		'icon',
        'orden',
        'rol_id',
		'observacion',
    ];

    public function rol(){
        return $this->belongsTo('App\Models\Models\roles');
    }
    public function submenu(){
        return $this->hasMany('App\Models\Models\SubMenu');
    }

  

    public function roles()
    {
        return $this->belongsToMany('App\Models\Models\roles','menu_roles','menu_id','rol_id');
    }

}
