<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    use HasFactory;
    protected $table ="municipios";

    protected $fillable = [
        'estado_id',
    'descripcion',
    'user_id'];

    public function estado(){
            return $this->belongsTo('App\Models\Models\estados', 'estado_id');
        } 
        public function usuario(){
            return $this->belongsTo('App\Models\Models\usuarios', 'user_id');
        } 
}
