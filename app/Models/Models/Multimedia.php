<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Multimedia extends Model
{
    use HasFactory;
    protected $table = 'multimedia';
    protected $fillable = ['archivo','tipo'];

    public function ticket(){
    	return $this->hasMany('App\Models\Models\ticket');
    }
 
}
