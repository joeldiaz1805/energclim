<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class proveedores extends Model
{
    use HasFactory;
    protected $table = 'proveedores';
    protected $fillable = [
    	'nombre',
    	'cod_proveedor',
    	'user_id'

    ];

     public function usuario(){
        return $this->belongsTo('App\Models\Models\usuarios', 'user_id');
    }
     public function contratos(){
        return $this->hasMany('App\Models\Models\contratos', 'proveedor_id');
    }
}
