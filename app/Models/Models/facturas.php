<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class facturas extends Model
{
    use HasFactory;

    protected $table = "facturas";
    protected $fillable = [
    'fecha',
	'contrato_id',
	'localidad_id',
	'trabajo_realizado',
	'monto_factura',
	'equipo_id',
	'nro_factura',
	'user_id',
    'status',
    'gerencia',
    'n_pedido',
    'modo_pago',
    'fecha_pago',
    'prioridad',
    'justificacion',
];
 	
 	public static $rule=[
            'fecha'            => 'required|date',
            'contrato_id'      => 'required',
            'localidad_id'     => 'required',
            'trabajo_realizado'=> 'required',
            'monto_factura'    => 'required|numeric',
            'nro_factura'    => 'required',
            'equipo_id'        => 'required',

        ];

	public function usuario(){
        return $this->belongsTo('App\Models\Models\usuarios', 'user_id');
    }

    public function localidad(){
        return $this->belongsTo('App\Models\Models\localidades', 'localidad_id');
    }
	
	public function contrato(){
	    return $this->belongsTo('App\Models\Models\contratos', 'contrato_id');
	}

	public function equipo(){
    	return $this->belongsTo('App\Models\Models\equipo','equipo_id');
    }
}
