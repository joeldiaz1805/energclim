<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class planilla_bb extends Model
{
    use HasFactory;

        protected $table= "planilla_bb";
        protected $fillable =[
            'marca',
            'modelo',
            'tipo',
            'v_total',
            'v_carga',
            'num_inv',
            'cap_ah',
            'celdas',
           'fecha_instalacion',
            'datos_actividad_id',
        ];

        public function tabla_actividad(){
        return $this->belongsTo('App\Models\Models\tabla_actividad', 'datos_actividad_id');
    }

      public function celdas_bb(){
        return $this->hasMany('App\Models\Models\celdas_bb', 'planilla_bb_id');
    }
}
