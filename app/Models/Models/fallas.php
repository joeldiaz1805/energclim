<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class fallas extends Model
{
    use HasFactory;


    protected $table = "fallas";

    protected $fillable = [
        'descripcion', 
        'componente_id',
        'tipo',
    ];

     public function componente(){
    	return $this->belongsTo('App\Models\Models\componente','componente_id');
    }
}
