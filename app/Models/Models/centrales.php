<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class centrales extends Model
{
    use HasFactory;
    protected $table = "centrales";

     protected $fillable=
     [
     'localidad_id',
     'region_id',
     'estado_id',
	 'area',
	 'tipo',
	];

    public function ubicacion(){
        return $this->hasMany('App\Models\Models\ubicacion','central_id');
    }
     public function localidad(){
        return $this->belongsTo('App\Models\Models\localidades', 'localidad_id');
    }
     public function region(){
        return $this->belongsTo('App\Models\Models\regiones', 'region_id');
    }
     public function estados(){
        return $this->belongsTo('App\Models\Models\estados', 'estado_id');
    }
    
}
