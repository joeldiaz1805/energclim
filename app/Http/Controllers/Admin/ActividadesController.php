<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Models\personal;
use App\Models\Models\regiones;
use App\Models\Models\estados;
use App\Models\Models\localidades;
use App\Models\Models\ResponsableActividad;
use App\Models\Models\actividades;
use App\Models\Models\tabla_actividad;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Models\Multimedia;
use Illuminate\Support\Str;
use JsValidator;
use Session;
use Auth;

class ActividadesController extends Controller
{
    
    public function index()
    {
        $actividades = actividades::get();

        return view('actividades.index',compact('actividades'));
    }
      protected $messages =[
                'required' =>"Campo requerido",
                'min' => "Minimo :min digitos",
                'max' => "Maximo :max digitos",
                'email' => 'Formato email invalido',
                'unique'=> 'Campo ya registrado',
                'required_if' => 'Debe especificar un fecha fin si la actividad tiene status "Realizado"',
                'numeric' => 'Solo valores Numericos',
                'after_or_equal' => 'Fecha fin debe ser mayor o igual a fecha inicio',
            ];

  
    public function create()
    {
      
        $regiones = regiones::get();
        $responsables = personal::orderBy('nombre','asc')->get();
        $data['validator'] = JsValidator::make(actividades::$rule,$this->messages);

        return view('actividades.create',compact('regiones','responsables'),$data);
    }

   
    public function store(Request $request)
    {   
        Validator::make($request->all(),actividades::$rule,$this->messages)->validate();
        $actividad = actividades::create($request->all());
        if ($actividad) {
            foreach ($request->respon as $key => $value) {
                ResponsableActividad::create(['actividad_id'=>$actividad->id,'responsable_id'=>$value]);
            }
            Session::flash('sucess','Actividad guardada con exito');
            return redirect('/actividades');
        }else{

        }


    }
   
    public function create_celdas($id){


        return view('TablaActividad.create_celdas');
    }

        public function download($id)
        {
            $data=tabla_actividad::with('planilla_bb.celdas_bb')->find($id);
            //return $data;
            $pdf = \PDF::loadView('TablaActividad.vistaPdf',compact('data'));
            return $pdf->download('reporte_bancoB.pdf');
        }
        public function planilla_ups($id)
        {
            $data=tabla_actividad::with('planilla_bb.celdas_bb')->find($id);
            //return $data;
            $pdf = \PDF::loadView('TablaActividad.vistaPdfUps',compact('data'));
            return $pdf->download('reporte_bancoB.pdf');
        }
  
    public function show($id)
    {
    }

  
    public function edit($id)
    {
        $actividad = actividades::whereid(decrypt($id))->first();
       
        $regiones = regiones::get();
        $estados = estados::get();
        $localidades = localidades::get();
        $responsables = personal::orderBy('nombre','asc')->get();
        
        $data['validator'] = JsValidator::make(actividades::$rule,$this->messages);
        return view('actividades.edit',compact('regiones','actividad','estados','localidades','responsables'),$data);
    }

 
    public function update(Request $request, $id)
    {
        
        Validator::make($request->all(),actividades::$rule,$this->messages)->validate();
        $activ = actividades::find(decrypt($id));
        $activ->fill($request->all());
        
        $unicos = array_unique($request->respon);
        if ($activ->save()) {
            foreach ($activ->responsables as $key => $value) {
                ResponsableActividad::where('actividad_id',decrypt($id))->delete();
            }
            foreach ($unicos as $key => $value) {
                ResponsableActividad::create(['responsable_id'=>$value,'actividad_id'=>decrypt($id)]);
            }
            Session::flash('sucess','Actividad Actualizada con exito');
           
            return redirect('/actividades');
           
        }
    }

   
    public function destroy($id)
    {
        actividades::find(decrypt($id))->delete();
      
    }
}
