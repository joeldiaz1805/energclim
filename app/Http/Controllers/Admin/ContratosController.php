<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Models\contratos;
use App\Models\Models\proveedores;
use App\Models\Models\personal;
use App\Models\Models\regiones;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Models\Multimedia;
use Illuminate\Support\Str;
use JsValidator;
use Session;
use Auth;


class ContratosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $messages =[
                'required' =>"Campo requerido",
                'min' => "Minimo :min digitos",
                'max' => "Maximo :max digitos",
                'email' => 'Formato email invalido',
                'unique'=> 'Campo ya registrado',
                'required_if' => 'campo requerido si la opción "Falla" esta seleccionada en Operatividad',
                'numeric' => 'Solo valores Numericos'
            ];
    public function index()
    {
       $contratos = contratos::with('facturas')->get();
       //return $contratos;
       return view('contratos.index',compact('contratos'));
    }

   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proveedores = proveedores::get();
        $data['validator'] = JsValidator::make(contratos::$rule,$this->messages);

        return view('contratos.create',compact('proveedores'),$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(),contratos::$rule,$this->messages)->validate();
        if (contratos::create($request->all())) {
            Session::flash('exit',true);
            return redirect('/contratos');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $contratos = contratos::find(decrypt($id));
        $proveedores = proveedores::get();

        $data['validator'] = JsValidator::make(contratos::$rule,$this->messages);

       return view('contratos.edit',compact('contratos','proveedores'),$data);
    }

    
    public function update(Request $request, $id)
    {
        Validator::make($request->all(),contratos::$rule,$this->messages)->validate();
        $contratos= contratos::find(decrypt($id));
        $contratos->fill($request->all());
        if ($contratos->save()) {
            Session::flash('actualizado',true);
            return redirect('/contratos');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
