<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Models\ticket;
use App\Models\Models\regiones;
use App\Models\Models\localidades;
use App\Models\Models\estados;
use App\Models\Models\personal;
use App\Models\Models\ubicacion;
use App\Models\Models\centrales;
use JsValidator;
use Session;

class TiketsCorrController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     protected $rule=[
            'aireA[marca]'                => 'required|max: 50',
            'aireA[modelo]'                => 'required|min:2',
            'aireA[serial]'                => 'required|min:6|max: 50',
            'aireA[inventario_cant]'       => 'required|min:6|max: 50',
            'aireA[tipo_equipo]'           => 'required',
            'aireA[nro_circuitos]'         => 'required',
            'aireA[tipo_compresor]'        => 'required',
            'aireA[tipo_refrigerante]'     => 'required',
            'aireA[ton_refrig_operativa]'  => 'required|min:2|max:50',
            'aireA[ton_refrig_faltante]'   => 'required|min:2|max: 50',
            'aireA[volt_operacion]'        => 'required',
            'aireA[tablero_control]'       => 'required',
            'aireA[operatividad]'          => 'required',
            'aireA[fecha_instalacion]'     => 'required',
            'aireA[criticidad]'            => 'required',
            'aireA[garantia]'              => 'required',
            'aireA[observaciones]'         => 'required|min:1|max:350',

            'central[localidad_id]'        => 'required',
            'central[region_id]'           => 'required',
            'central[estado_id]'           => 'required',
            'ubicacion[piso]'              => 'required|min:1',
            'ubicacion[estructura]'        => 'required',
            'ubicacion[criticidad_esp]'    => 'required',
            'responsable[nombre]'          => 'required|min:2|max:20',
            'responsable[apellido]'        => 'required|min:2|max:20',
            'responsable[cedula]'          => 'required|min:7|max:10',
            'responsable[cod_p00]'         => 'required|min:3|max:15',
            'responsable[num_oficina]'     => 'required|min:11|max:13',
            'responsable[num_personal]'    => 'required|min:11|max:13',
            'responsable[correo]'          => 'required|email',

            'equipo[descripcion]'        => 'required',
            'equipo[localidad_id]'       => 'required',
            'equipo[responsable_id]'     => 'required',


        ];

        protected $messages =[
            'required' =>"Campo requerido",
            'min' => "Minimo :min digitos",
            'max' => "Maximo :max digitos",
            'email' => 'Formato email invalido'
        ];
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */

    public function create()
    {
        //dd('hol');
        $regiones= regiones::get();
        $estados= estados::get();
        $localidades= localidades::get();
        $data['validator'] = JsValidator::make($this->rule,$this->messages);
        return view('ticket.correctivo',compact('regiones','estados','localidades','data'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
