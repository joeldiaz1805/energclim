<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Models\regiones;
use App\Models\Models\localidades;
use App\Models\Models\estados;
use App\Models\Models\centrales;
use Illuminate\Support\Facades\Validator;
use JsValidator;
use Session;
use Auth;

class EstadosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estados = estados::get();
        return view('estados.index',compact('estados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $region= regiones::orderBy('nombre','ASC')->get();
        return view('estados.create',compact('region'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $edo = estados::create($request->all());
        if($edo){
            Session::flash('exito',true);
            return redirect('/estados');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $estado = estados::where('id',$id)->first();
        $region = regiones::get();
        return view('estados.edit',compact('estado','region'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $estado = estados::where('id',$id)->first();
        $estado->fill($request->all());

        if ($estado->save()) {
            Session::flash('actualizado',true);
            return redirect('/estados');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       estados::find($id)->delete();
    }
}
