<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Models\usuarios;
use App\Models\Models\regiones;
use App\Models\Models\localidades;
use App\Models\Models\estados;
use App\Models\Models\personal;
use App\Models\Models\ubicacion;
use App\Models\Models\centrales;
use App\Models\Models\componente;
use App\Models\Models\mantenimiento;
use App\Models\Models\motogeneradorPivot;
use App\Models\Models\fallas;
use App\Models\Models\acciones;
use App\Models\Models\salas;
use App\Models\Models\pisos;
use App\Models\Models\equipo;
use Auth;
use JsValidator;
use Session;
class ComponentesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $componentes = componente::get();
       return view('componentes.index',compact('componentes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $equipos= equipo::get();

        return view('componentes.create',compact('equipos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // dd($request->all());
       foreach ($request->descripcion as $key => $value) {
            componente::create(['descripcion'=>$value,'equipo_id'=>$request->equipo_id]);
       }
       Session::flash('exito',true);
       return redirect('/Componentes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $equipos= equipo::get();

        $componente = componente::find(decrypt($id));
        return view('componentes.edit',compact('componente','equipos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $componente = componente::find(decrypt($id));

        $componente->fill($request->all());

        $componente->save();

        Session::flash('exito',true);
        return redirect('/Componentes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        componente::find($id)->delete();
       
    }
}
