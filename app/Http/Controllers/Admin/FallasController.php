<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Models\fallas;
use Auth;
use JsValidator;
use Session;


class FallasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fallas = fallas::orderBy('descripcion','asc')->get();
        return view('fallas.index',compact('fallas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('fallas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
       foreach ($request->descripcion as $key => $value) {
            fallas::create(['descripcion'=>$value,
                            'componente_id'=> 1,
                            'tipo'=>$request->tipo]);
        }
            
        Session::flash('exito',true);
        return redirect('/fallas_modulo');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $falla = fallas::find(decrypt($id));
        return view('fallas.edit',compact('falla'));    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       //dd($request->all());

        $fallas = fallas::find(decrypt($id));
        $fallas->fill($request->all());
        if ($fallas->save()) {
            Session::flash('actualizado',true);
            return redirect('/fallas_modulo');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        fallas::find($id)->delete();
    }
}
