<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Models\regiones;
use App\Models\Models\localidades;
use App\Models\Models\Municipio;
use App\Models\Models\Parroquia;
use App\Models\Models\estados;
use App\Models\Models\centrales;
use App\Models\Models\personal;
use Illuminate\Support\Facades\Validator;
use JsValidator;
use Session;
use Auth;


class LocalidadesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
      protected $messages =[
            'required' =>"Campo requerido",
            'min' => "Minimo :min digitos",
            'max' => "Maximo :max digitos",
            'email' => 'Formato email invalido',
            'unique'=> 'Campo ya registrado',
            'required_if' => 'campo requerido si la opción "Falla" esta seleccionada en Operatividad'
        ];
    
    public function index(){
       
        $central = localidades::get();
        $request = Request();
        return view('localidades.index',compact('central','request'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $estados = estados::orderBy('nombre','ASC')->get();
        $data['validator'] = JsValidator::make(localidades::$rule,$this->messages);
        return view('localidades.create',compact('estados'),$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        if ($request['supervisor'] && $request['coordinador']) {
            Validator::make($request->all(),localidades::$rule,$this->messages)->validate();
            $verifysuper = personal::where('cod_p00',$request['supervisor']['cod_p00'])->first();
            $verifyCoord = personal::where('cod_p00',$request['coordinador']['cod_p00'])->first();
            if($verifysuper){
                $request['supervisor_id'] =$verifysuper->id;
            }else{
                $pers = personal::create($request['supervisor']);
                $request['supervisor_id'] =$pers->id;
            }
             if($verifyCoord){
                $request['coordinador_id'] =$verifyCoord->id;
            }else{
                $pers = personal::create($request['coordinador']);
                $request['coordinador_id'] =$pers->id;
            }
        }
        $request['user_id'] = Auth::user()->id;
        $localidad = localidades::create($request->all());
        if ($localidad) {
            Session::flash('exito',true);
            return redirect('localidades');
           # code...
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $estados = estados::get();
        $localidad = localidades::where('id',decrypt($id))->first();
         $municipios = Municipio::get();
        $parroquia = Parroquia::get();

        
         $data['validator'] = JsValidator::make(localidades::$rule,$this->messages);
        return view('localidades.edit',compact('localidad','estados','municipios','parroquia'));
    }

    
    public function update(Request $request, $id)
    {
        $localidad = localidades::where('id',decrypt($id))->first();
        if ($request['supervisor'] && $request['coordinador']) {
            $verifysuper = personal::where('cod_p00',$request['supervisor']['cod_p00'])->first();
            $verifyCoord = personal::where('cod_p00',$request['coordinador']['cod_p00'])->first();
            Validator::make($request->all(),localidades::$rule,$this->messages)->validate();
            if($verifysuper){
                $verifysuper->fill($request['supervisor']);
                $verifysuper->save();
                $localidad['supervisor_id'] =$verifysuper->id;
            }else{
                $pers = personal::create($request['supervisor']);
                $localidad['supervisor_id'] =$pers->id;
            }
             if($verifyCoord){
                $verifyCoord->fill($request['coordinador']);
                $verifyCoord->save();
                $localidad['coordinador_id'] =$verifyCoord->id;
            }else{
                $pers = personal::create($request['coordinador']);
                $localidad['coordinador_id'] =$pers->id;
            }
        }
        $localidad->fill($request->all());
        $localidad->user_id = Auth::user()->id; 
        if ($localidad->save()) {
            Session::flash('actualizado',true);
            return redirect('/localidades');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $local = localidades::find($id)->delete();

    }
}
