<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Models\Menu;
use App\Models\Models\menurol;
use App\Models\Models\roles;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Models\Multimedia;
use Illuminate\Support\Str;
use JsValidator;
use Session;
use Auth;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //dd(menurol::with('menu')->whererol_id(Auth::user()->rol_id)->get(), Auth::user()->rol_id);
        $menus = Menu::with('roles')->get();
        //return response($menus);
        return view('menu.index',compact('menus'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles= roles::get();
        return view('menu.create',compact('roles'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // dd($request->all());
        $menu = Menu::create(['nombre'=>$request->nombre,'url'=>$request->url,'icon'=>$request->icon,'observacion'=>$request->nombre]);
        if ($menu) {
            foreach ($request->rol_id as $key => $value) {
                menurol::create(['menu_id'=>$menu->id,'rol_id'=>$value]);
            }
        }
        Session::flash('success','Menu Creado Exitosamente');
        return redirect('/menus');
    }

  
    public function show($id)
    {
        //
    }
      public function orden()
    {
/*
          $menu = Menu::wherehas('roles',function($query){
                     return $query->where('rol_id',Auth::user()->rol_id);
            })->orderBy('orden','asc')->get();

          return response($menu);*/
        $menu = Menu::get();
        return view('menu.orden',compact('menu'));
    }
    public function orden_post(Request $request)
    {

       dd($request->all());
      foreach ($request->orden as $key => $value) {
          $menu=Menu::find($value);
          $menu->orden= $key;
          $menu->save();
      }
      Session::flash('success','Los Menues fueron ordenados con èxito');
      return redirect('/menus');
    }

   
    public function edit($id)
    {
       
        $roles =roles::get();
        $menus = Menu::whereid(decrypt($id))->first();
       
        $idmenus=[];
        foreach ($menus->roles as $key => $value) {
            $idmenus[]=$value->id;
        }


      //  return $idmenus;
      
        return view('menu.edit',compact('roles','menus','idmenus'));
    }

  
    public function update(Request $request, $id)
    {
        $menu = Menu::find(decrypt($id));
        foreach ($request->rol_id as $key => $value) {
            menurol::where('menu_id',decrypt($id))->delete();
        }
        foreach ($request->rol_id as $key => $value) {
         menurol::create(['menu_id'=>decrypt($id),'rol_id'=>$value]);
        }

        unset($request['rol_id']);

        //dd($request->all(),$menu);
        $menu->fill($request->all());
        if ($menu->save()) {
            Session::flash('success','Menu Actualizado con Exito');
            return redirect('/menus');
        }else{
            Session::flash('error','Ocurriò un error para actualizar');
            return redirect('/menus');

        }


    }

    
    public function destroy($id)
    {
        Menu::find(decrypt($id))->delete();
        
    }
}
