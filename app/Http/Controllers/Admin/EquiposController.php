<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Models\usuarios;
use App\Models\Models\regiones;
use App\Models\Models\localidades;
use App\Models\Models\estados;
use App\Models\Models\personal;
use App\Models\Models\ubicacion;
use App\Models\Models\centrales;
use App\Models\Models\componente;
use App\Models\Models\mantenimiento;
use App\Models\Models\motogeneradorPivot;
use App\Models\Models\fallas;
use App\Models\Models\acciones;
use App\Models\Models\salas;
use App\Models\Models\pisos;
use App\Models\Models\equipo;
use Auth;
use JsValidator;
use Session;

class EquiposController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
    {
         $equipos = equipo::orderBy('descripcion','asc')->get();
        return view('equipos.index',compact('equipos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function create()
         {
             return view('equipos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());

        foreach ($request->descripcion as $key => $value) {
            equipo::create(['descripcion'=>$value,
                            'localidad_id'=> 1,
                            'responsable_id'=> 1,
                            'servicio_id'=>$request->servicio_id]);
        }
            # code...
        Session::flash('exito',true);
        return redirect('/equipos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $equipo = equipo::find(decrypt($id));
        return view('equipos.edit',compact('equipo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $equipo = equipo::find(decrypt($id));
        $equipo->fill($request->all());
        if ($equipo->save()) {
            Session::flash('actualizado',true);
            return redirect('/equipos');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        equipo::find($id)->delete();
    }
}
