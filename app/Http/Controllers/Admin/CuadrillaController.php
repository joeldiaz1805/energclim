<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Models\registro;
use App\Models\Models\regiones;
use App\Models\Models\localidades;
use App\Models\Models\estados;
use App\Models\Models\personal;
use App\Models\Models\ubicacion;
use App\Models\Models\centrales;
use App\Models\Models\equipo;
use App\Models\Models\componente;
use App\Models\Models\cuadrilla;
use App\Models\Models\PersonalCuadrilla;
use Illuminate\Support\Facades\Validator;//te falta esta clase. A

use JsValidator;
use Session;
use Auth;

class CuadrillaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   $cuadrillas = cuadrilla::get();
       // return response($cuadrillas);
       return view('cuadrillas.index',compact('cuadrillas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $personal = personal::where('estado_id',13)->get();
       return view('cuadrillas.create',compact('personal'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $cuad = new cuadrilla();
        $cuad->nombre = $request->nombre;
       $cuad->user_id = Auth::user()->id; 
        $cuad->save();

        foreach ($request->personal as $key => $value) {
            PersonalCuadrilla::create(['cuadrilla_id'=>$cuad->id,'personal_id'=>$value]);
        }
        Session::flash('exito',true);
        return redirect('/cuadrillas');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cuadrilla = cuadrilla::find(decrypt($id));
        $personal = personal::where('estado_id',13)->get();
        return view('cuadrillas.edit',compact('cuadrilla','personal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $cuad = cuadrilla::find(decrypt($id));
        $cuad->fill($request->all());
        
        $unicos = array_unique($request->personal);
        if ($cuad->save()) {
            foreach ($cuad->cuadrante as $key => $value) {
                //dd($key,$value);
                PersonalCuadrilla::where('cuadrilla_id',decrypt($id))->delete();
            }
            foreach ($unicos as $key => $value) {
                PersonalCuadrilla::create(['personal_id'=>$value,'cuadrilla_id'=>decrypt($id)]);
            }
            Session::flash('sucess','Cuadrilla Actualizada con exito');
           
            return redirect('/cuadrillas');
           
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        cuadrilla::find($id)->delete();
        PersonalCuadrilla::where('cuadrilla_id',$id)->delete();
    }
}
