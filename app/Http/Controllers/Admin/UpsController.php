<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Models\regiones;
use App\Models\Models\localidades;
use App\Models\Models\estados;
use App\Models\Models\personal;
use App\Models\Models\ubicacion;
use App\Models\Models\centrales;
use App\Models\Models\ups_bancob;
use App\Models\Models\ups;
use App\Models\Models\bancob;
use App\Models\Models\marcas;


use Illuminate\Support\Facades\Validator;
use JsValidator;
use Session;
use Auth;
class UpsController extends Controller
{
 public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
*/
    

    protected $messages =[
            'required' =>"Campo requerido",
            'min' => "Minimo :min digitos",
            'max' => "Maximo :max digitos",
            'email' => 'Formato email invalido',
            'unique'=> 'Campo ya registrado',
            'numeric' => 'Solo valores numèricos'
        ];

    public function index()
    {   
        if (Auth::user()->rol_id == 4) {
            
            $ups = ups_bancob::whereHas('ubicacion',function($query2){
                $query2->whereHas('central',function($query3){
                    return $query3->where('region_id',Auth()->user()->region_id);
                });
            })->get();
             
        }else{
            $ups = ups_bancob::get();
        }
        return view('ups.index',compact('ups'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $regiones= regiones::orderBy('nombre','ASC')->get();
        $estados= estados::orderBy('nombre','ASC')->get();
        $localidades= localidades::orderBy('nombre','ASC')->get();
        $marcas = marcas::whereequipo_id(7)->orderBy('descripcion','ASC')->get();
        $data['validator'] = JsValidator::make(ups_bancob::$rule,$this->messages);
        return view('ups.create',compact('marcas','regiones','estados','localidades'),$data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        Validator::make($request->all(),ups_bancob::$rule,$this->messages)->validate();
        $verifyPers = personal::where('cod_p00',$request['responsable']['cod_p00'])->first();

        $centro =$request['ubicacion'];
        if($verifyPers){
            $centro['responsable_id'] =$verifyPers->id;
        }else{
            $pers = personal::create($request['responsable']);
            $centro['responsable_id'] =$pers->id;
        }

        $centrar = centrales::create($request['central']);
        $centro['central_id'] =$centrar->id;
        $ubi = ubicacion::create($centro);

        $ups = ups::create($request['ups']);
        $bco_baterias = bancob::create($request['bco_baterias']);

        $ups_bcob = $request['ups_bcob'];
        $ups_bcob['ups_id'] =$ups->id;
        $ups_bcob['bancob_id'] =$bco_baterias->id;
        $ups_bcob['tipo_id'] =7;

        $ups_bcob['ubicacion_id'] = $ubi->id;
        $pivot = ups_bancob::create($ups_bcob);
        Session::flash('exito',true);
        return redirect('/ups');
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $ups_bancob = ups_bancob::where('id',$id)->first();
        return view('ups.show',compact('ups_bancob'));

        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        
        $ups_bancob = ups_bancob::where('id',$id)->first();

        $rule = ups_bancob::$rule;

       // $rule['ups.serial'] =$rule['ups.serial'].','.$ups_bancob->ups->id;
        //$rule['bco_baterias.serial'] =$rule['bco_baterias.serial'].','.$ups_bancob->bancob->id;
        $rule['responsable.cedula'] =$rule['responsable.cedula'].','.$ups_bancob->ubicacion->responsable->id;
        $rule['responsable.cod_p00'] = $rule['responsable.cod_p00'].','.$ups_bancob->ubicacion->responsable->id;
       // $rule['responsable.correo'] =$rule['responsable.correo'].','.$ups_bancob->ubicacion->responsable->id;

        $data['validator'] = JsValidator::make($rule,$this->messages);
        $regiones= regiones::orderBy('nombre','ASC')->get();
        $estados= estados::orderBy('nombre','ASC')->get();
        $localidades= localidades::orderBy('nombre','ASC')->get();
        $marcas = marcas::whereequipo_id(7)->orderBy('descripcion','ASC')->get();


        $data['validator'] = JsValidator::make($rule,$this->messages);
        return view('ups.edit',compact('marcas','regiones','estados','localidades','ups_bancob'),$data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $ups_bancob = ups_bancob::where('id',$id)->first();
        $ups = ups::where('id',$ups_bancob->ups_id)->first();
        $bancob = bancob::where('id',$ups_bancob->bancob_id)->first();
        $ubi = ubicacion::where('id',$ups_bancob->ubicacion_id)->first();
        $central = centrales::where('id',$ubi->central_id)->first();
        $responsable = personal::where('id',$ubi->responsable_id)->first();

        $rule = ups_bancob::$rule;
        //$rule['ups.serial'] =$rule['ups.serial'].','.$ups_bancob->ups->id;
        //$rule['bco_baterias.serial'] =$rule['bco_baterias.serial'].','.$ups_bancob->bancob->id;
        $rule['responsable.cedula'] =$rule['responsable.cedula'].','.$ups_bancob->ubicacion->responsable->id;
        $rule['responsable.cod_p00'] = $rule['responsable.cod_p00'].','.$ups_bancob->ubicacion->responsable->id;
        //$rule['responsable.correo'] =$rule['responsable.correo'].','.$ups_bancob->ubicacion->responsable->id;

        Validator::make($request->all(),$rule,$this->messages)->validate();
        $ups_bancob->fill($request['ups_bcob']);
        $bancob->fill($request['bco_baterias']);

        $ups->fill($request['ups']);

        $ubi->fill($request['ubicacion']);

        $central->fill($request['central']);

        $responsable->fill($request['responsable']);

        if($ups->save() &&  $bancob->save() && $ups_bancob->save() && $ubi->save() && $central->save() && $responsable->save()) {

            Session::flash('actualizado',true);
            return redirect('/ups'); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ups_bancob::find($id)->delete();
    }
}
