<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Models\motogenerador;
use App\Models\Models\generador;
use App\Models\Models\regiones;
use App\Models\Models\localidades;
use App\Models\Models\estados;
use App\Models\Models\personal;
use App\Models\Models\ubicacion;
use App\Models\Models\centrales;
use App\Models\Models\marcas;

use JsValidator;
use Session;
use App\Models\Models\motogeneradorPivot;
use App\Models\Models\rect_bcobb;
use App\Models\Models\AireA;
use App\Models\Models\ups_bancob;
use App\Models\Models\bancob;

use App\Models\Models\BatteryCtrl;
use Illuminate\Support\Facades\Validator;
use Auth;


class MotorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
         public function __construct(){
                $this->middleware('auth');
            }
    public function index()
    {
        if (Auth::user()->rol_id == 4) {
            //$motor = motogeneradorPivot::get();
             $motor = motogeneradorPivot::whereHas('motor', function ($query) {
                $query->whereHas('ubicacion',function($query2){
                    $query2->whereHas('central',function($query3){
                        return $query3->where('region_id',Auth()->user()->region_id);

                    });
                });
                 })->get();
             
        }else{
            $motor = motogeneradorPivot::get();

           
           //return response($motor);

        }
        //return response($motor);
        return view('admin.index',compact('motor'));
    }
    public function niveles_combustible(){
        if (Auth::user()->rol_id == 4) {
            //$motor = motogeneradorPivot::get();
             $motor = motogeneradorPivot::whereHas('motor', function ($query) {
                $query->whereHas('ubicacion',function($query2){
                    $query2->whereHas('central',function($query3){
                        return $query3->where('region_id',Auth()->user()->region_id);

                    });
                });
                 })->get();
             
        }else{
            $motor = motogeneradorPivot::get();
           //return response($motor);

        }

        //return response($motor);
        return view('nivelesCombustible.index',compact('motor'));
    }


    public function niveles_combustible_region(){
        
        $nivel_oriente = motogeneradorPivot::with('motor')->whereHas('motor', function ($query) {
                $query->where('cant_actual','!=',null)->whereHas('ubicacion',function($query2){
                    $query2->whereHas('central',function($query3){
                        return $query3->where('region_id',7);

                    });
                });
                 })->get();

        $nivel_andes = motogeneradorPivot::with('motor')->whereHas('motor', function ($query) {
                $query->where('cant_actual','!=',null)->whereHas('ubicacion',function($query2){
                    $query2->whereHas('central',function($query3){
                        return $query3->where('region_id',1);

                    });
                });
                 })->get();

        $nivel_capital = motogeneradorPivot::with('motor')->whereHas('motor', function ($query) {
                $query->where('cant_actual','!=',null)->whereHas('ubicacion',function($query2){
                    $query2->whereHas('central',function($query3){
                        return $query3->where('region_id',2);

                    });
                });
                 })->get();

        $nivel_occidente = motogeneradorPivot::with('motor')->whereHas('motor', function ($query) {
                $query->where('cant_actual','!=',null)->whereHas('ubicacion',function($query2){
                    $query2->whereHas('central',function($query3){
                        return $query3->where('region_id',6);

                    });
                });
                 })->get();
        $nivel_llanos = motogeneradorPivot::with('motor')->whereHas('motor', function ($query) {
                $query->where('cant_actual','!=',null)->whereHas('ubicacion',function($query2){
                    $query2->whereHas('central',function($query3){
                        return $query3->where('region_id',4);

                    });
                });
                 })->get();
        $nivel_centro = motogeneradorPivot::with('motor')->whereHas('motor', function ($query) {
                $query->where('cant_actual','!=',null)->whereHas('ubicacion',function($query2){
                    $query2->whereHas('central',function($query3){
                        return $query3->where('region_id',3);

                    });
                });
                 })->get();

        $nivel_guayana = motogeneradorPivot::with('motor')->whereHas('motor', function ($query) {
                $query->where('cant_actual','!=',null)->whereHas('ubicacion',function($query2){
                    $query2->whereHas('central',function($query3){
                        return $query3->where('region_id',5);

                    });
                });
                 })->get();
        

            
        $oriente = $this->porcentajes($nivel_oriente);
        $andes =  $this->porcentajes($nivel_andes);
         $llanos = $this->porcentajes($nivel_llanos);
         $occidente = $this->porcentajes($nivel_occidente);
         $capital =$this->porcentajes($nivel_capital);
         $centro =$this->porcentajes($nivel_centro);
         $guayana =$this->porcentajes($nivel_guayana);

        return view('nivelesCombustible.regionales',compact('oriente','capital','occidente','llanos','andes','centro','guayana'));
    }


    public function porcentajes_mapa($tipo,$valor){
        $porcentaje=0;
        $id = $valor;
        
        if ($tipo=="region") {
                        
            $regiones = motogeneradorPivot::with('motor')->whereHas('motor', function ($query) use($id) {
                $query->where('cant_actual','!=',null)->where('cant_actual','>',0)->whereHas('ubicacion',function($query2) use($id){
                    $query2->whereHas('central',function($query3) use($id){
                 
                        return $query3->where('region_id',$id);

                    });
                });
                 })->get();
            $acum=0;
            
            $porcentajes = $this->porcentajes($regiones);
        }elseif ($tipo=="state") {
             $estado = motogeneradorPivot::with('motor')->whereHas('motor', function ($query) use($id) {
                $query->where('cant_actual','!=',null)->where('cant_actual','>',0)->whereHas('ubicacion',function($query2) use($id){
                    $query2->whereHas('central',function($query3) use($id){
                 
                        return $query3->where('estado_id',$id);

                    });
                });
                 })->get();
            $porcentajes = $this->porcentajes($estado);

        }

        return response($porcentajes);
    }

    public function nivel_estado($id){
        $faltante=0;
         $motor = motogeneradorPivot::with('motor')->whereHas('motor', function ($query) use($id) {
                $query->where('cant_actual','!=',null)->where('cant_actual','>=',0)->whereHas('ubicacion',function($query2) use($id){
                    $query2->whereHas('central',function($query3) use($id){
                 
                        return $query3->where('estado_id',base64_decode($id));

                    });
                });
                 })->get();
         foreach ($motor as $equipos) {
            $faltante+= ($equipos->motor->comb_tanq_princ - $equipos->motor->cant_actual);
            
         }

         return view('nivelesCombustible.estado',compact('motor','faltante'));
    }

 public function nivel_region($id){
        $faltante=0;
         $motor = motogeneradorPivot::with('motor')->whereHas('motor', function ($query) use($id) {
                $query->where('cant_actual','!=',null)->where('cant_actual','>',0)->whereHas('ubicacion',function($query2) use($id){
                    $query2->whereHas('central',function($query3) use($id){
                 
                        return $query3->where('region_id',base64_decode($id));

                    });
                });
                 })->get();
         foreach ($motor as $equipos) {
            $faltante+= ($equipos->motor->comb_tanq_princ - $equipos->motor->cant_actual);
            
         }

         return view('nivelesCombustible.region',compact('motor','faltante'));
    }




    public function porcentajes($datos){
        $total_comb=0;
        $total_tpc=0;
        $prom_tanque=0;
        $prom_comb=0;
        $porc=0;
         foreach($datos as $cuenta)
            {
                if ($cuenta->motor->comb_tanq_princ > 0  ||  $cuenta->motor->cant_actual > 0)
                    {
                        try{
                            $total_comb += $cuenta->motor->cant_actual;
                            $total_tpc += $cuenta->motor->comb_tanq_princ;
                            }catch(Exception $e){
                                dd($e.' el error' );
                            }

                    }

            }
                    if ($datos->count()== 0) {
                        //dd($total_tpc, $total_comb, $datos->count(),$datos);
                         return 0;
                        // code...
                    }else{
                        
                        $prom_comb= ($total_comb/$datos->count());
                        $prom_tanque= ($total_tpc/$datos->count());
                        if ($prom_tanque ==0) {
                           return $porc = 0;
                            // code...
                        }else{
                            $porc =  ( round(($prom_comb*100) / $prom_tanque) );

                        }
                       
                       return $porc < 1 ? 1 : $porc;
                    }

    }

    public function equipos2($id){
        switch ($id) {
            case 1:
                $resp = motogeneradorPivot::with('motor','generador')->get();
                if (sizeof($resp) != 0) {
                    $ubicacion = $resp;
                }else{
                    return response($resp);
                    $ubicacion['marca'] = "Sin Registro";
                    $ubicacion['serial'] = "000000000";
                }
                break;
            case 2:
                $resp = rect_bcobb::with('rectificador','bancob')->get();
                if (sizeof($resp) != 0) {
                    $ubicacion =$resp;
                }else{
                    return response($resp);
                    $ubicacion['marca'] = "Sin Registro";
                    $ubicacion['serial'] = "000000000";
                }
                
                break;
            case 3:
                $resp = AireA::get();
                if (sizeof($resp) != 0) {
                    $ubicacion =$resp;
                }else{
                    return response($resp);
                    $ubicacion['marca'] = "Sin Registro";
                    $ubicacion['serial'] = "000000000";
                }
                break;
            case 7:
                $resp = ups_bancob::with('ups','bancob','ubicacion')->get();
                if (sizeof($resp) != 0) {
                    $ubicacion =$resp;
                }else{
                    return response($resp);
                    $ubicacion['marca'] = "Sin Registro";
                    $ubicacion['serial'] = "000000000";
                }
                break;
            
            default:
                 $ubicacion = "No disponible";
                break;
        }
        return response($ubicacion);
    }



     public function equipos($id,$tipo)
    {
        $ubicacion;
        $resp;

        // $resp=strtolower($tipo);
        // $variable;
        // $variable = ubicacion::with($resp)->where('id',$id)->get();
        // $ubicacion = $variable[0]->$resp;

        
        switch ($tipo) {
            case 1:
                $resp = ubicacion::with('motorgenerador')->where('id',$id)->get();
                if (sizeof($resp) != 0) {
                    $ubicacion =$resp[0]->motorgenerador;
                }else{
                    return response($resp);
                    $ubicacion['marca'] = "Sin Registro";
                    $ubicacion['serial'] = "000000000";
                }
                break;
            case 2:
                $resp = ubicacion::with('rectificadores')->where('id',$id)->get();
                if (sizeof($resp) != 0) {
                    $ubicacion =$resp[0]->rectificadores;
                }else{
                    return response($resp);
                    $ubicacion['marca'] = "Sin Registro";
                    $ubicacion['serial'] = "000000000";
                }
                
                break;
            case 3:
                $resp = ubicacion::with('aire_acondicionado')->where('id',$id)->get();
                if (sizeof($resp) != 0) {
                    $ubicacion =$resp[0]->aire_acondicionado;
                }else{
                    return response($resp);
                    $ubicacion['marca'] = "Sin Registro";
                    $ubicacion['serial'] = "000000000";
                }
                break;
            case 7:
                $resp = ubicacion::with('ups')->where('id',$id)->get();
                if (sizeof($resp) != 0) {
                    $ubicacion =$resp[0]->ups;
                }else{
                    return response($resp);
                    $ubicacion['marca'] = "Sin Registro";
                    $ubicacion['serial'] = "000000000";
                }
                break;
            
            default:
                 $ubicacion = "No disponible";
                break;
        }

        return response($ubicacion);
    }



    public function inventario()
    {
        $regiones = regiones::orderBy('nombre','ASC')->get();
       
        $mg = motogeneradorPivot::get();
        $rect = rect_bcobb::get();
        $ups = ups_bancob::get();
        $bb = bancob::get();
        $aire = AireA::get();
        

        
        return view('admin.inventario',compact('regiones','mg','rect','aire','ups','bb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

   

 protected $messages =[
            'required' =>"Campo requerido",
            'required_if' =>"Campo requerido",
            'min' => "Minimo :min digitos",
            'max' => "Maximo :max digitos",
            'email' => 'Formato email invalido',
            'numeric' => 'Solos valores numericos',
            'integer' => 'Exprese los valores numericos sin comas ni puntos',
            'unique'=> 'Campo ya registrado',
             'required_if' => 'campo requerido si la opción "Falla" esta seleccionada en Operatividad',
             'max'=>'La cantidad de Combustible supera la Capacidad del Tanque Principal estipulado'
        ];
     
    public function create(Request $request)
    {   
         $parametros = ($request->request->all());
        
        $data['validator'] = JsValidator::make(motogeneradorPivot::$rule,$this->messages);
        $regiones= regiones::orderBy('nombre','ASC')->get();
        $estados= estados::orderBy('nombre','ASC')->get();
        $localidades= localidades::orderBy('nombre','ASC')->get();
        $responsables = personal::get();
        $marcas = marcas::whereequipo_id(1)->orderBy('descripcion','ASC')->get();
        return view('admin.motogenerador',compact('marcas','regiones','estados','localidades','responsables','parametros'),$data);
    }


    public function store(Request $request)
    {
       
        Validator::make($request->all(),motogeneradorPivot::$rule,$this->messages)->validate();
        $verifyPers = personal::where('cod_p00',$request['responsable']['cod_p00'])->first();
        $centro =$request['ubicacion'];
        if($verifyPers){
            $centro['responsable_id'] =$verifyPers->id;
        }else{
            $pers = personal::create($request['responsable']);
            $centro['responsable_id'] =$pers->id;
        }

        $centrar = centrales::create($request['central']);
        $centro['central_id'] =$centrar->id;

        $ubi = ubicacion::create($centro);
        $generador = generador::create($request['generador']);
        

        
        $mtt = $request['motor'];
        $mtt['ubicacion_id'] = $ubi->id;
        $motor = motogenerador::create($mtt);
        $pivot = motogeneradorPivot::create([
                    'motor_id'=>$motor->id,
                    'generador_id'=>$generador->id,
                    'tipo_id'=>1,
        ]);
        if ( $request['bateriaControl'][0] != null) {
        //dd($request['bateriaControl']);
        BatteryCtrl::create([
                    'motog_id'=>$motor->id,
                    'cantidad'=>$request['bateriaControl'][0],
                    'amperaje'=>$request['bateriaControl'][1],
                    'voltaje'=>$request['bateriaControl'][2]
                ]);
    }
        if ($request['bateriaControl2']) {
            BatteryCtrl::create([
                    'motog_id'=>$motor->id,
                    'cantidad'=>$request['bateriaControl2'][0],
                    'amperaje'=>$request['bateriaControl2'][1],
                    'voltaje'=>$request['bateriaControl2'][2]
                ]);
        }
        // BatteryCtrl
        Session::flash('exito',true);
        if ($request->parametro != null) {
            return redirect('/nodos/'.$request->parametro);
            # code...
        }
        return redirect('/motogenerador');
    }


    public function show($id)
    {
        $motogenerador = motogeneradorPivot::where('id',decrypt($id))->first();
        return view('admin.show',compact('motogenerador'));
    }

    public function edit(Request $request, $id)
    {
        $parametros = ($request->request->all());
        $motogenerador = motogeneradorPivot::where('id',decrypt($id))->first();
        $rules = motogeneradorPivot::$rule;
  
        $regiones= regiones::orderBy('nombre','ASC')->get();
        $estados= estados::orderBy('nombre','ASC')->get();
        $localidades= localidades::orderBy('nombre','ASC')->get();
        $marcas = marcas::whereequipo_id(1)->orderBy('descripcion','ASC')->get();

        $data['validator'] = JsValidator::make($rules,$this->messages);
        return view('admin.edit',compact('marcas','motogenerador','regiones','localidades','estados','parametros'),$data);
    }

    public function niveles_combustible_edit($id, Request $request)
    {
        $parametros = ($request->request->all());
        $motogenerador = motogeneradorPivot::where('id',decrypt($id))->first();
        $cantidad_combustible=0;
        if ($motogenerador->motor->comb_tanq_princ != null) {
            $cantidad_combustible= $motogenerador->motor->comb_tanq_princ;
        }
        $rules = [
         'ltrs_hora'                => 'integer',
         'ltrs_dia'                => 'integer',
         'cap_tanq_aceite'                => 'integer',
         'aceite_actual'                => 'integer',
         'cant_actual'                => 'required|integer|max:'.$cantidad_combustible];
     
        $data['validator'] = JsValidator::make($rules,$this->messages);

        return view('nivelesCombustible.edit',compact('motogenerador','parametros'),$data);
    }
    public function niveles_combustible_update(Request $request,$id)
    {
        $motogenerador = motogeneradorPivot::where('id',decrypt($id))->first();
        $motor =motogenerador::where('id',$motogenerador->motor_id)->first();
        $cantidad_combustible=0;
        if ($motor->comb_tanq_princ != null) {
            $cantidad_combustible= $motogenerador->motor->comb_tanq_princ;
        }
        $rules = [
        'ltrs_hora'                => 'integer',
         'ltrs_dia'                => 'integer',
         'cap_tanq_aceite'                => 'integer',
         'aceite_actual'                => 'integer',
         'cant_actual'                  =>'required|integer|max:'.$cantidad_combustible];

        Validator::make($request->all(),$rules,$this->messages)->validate();
    
       
        $motor->fill($request->all());
        if ($motor->save()) {
            # code...
            Session::flash('actualizado',true);
            if($request->state != null){
                return redirect('/combustible_estado/'.base64_encode($request->state));
                
            }
            return redirect('/nivel_combustible');
        }
    }


    public function movimiento(){
        $motor = motogenerador::wheremonth('updated_at','08')->get();

        return view('admin.movimiento',compact('motor'));
    }


   
    public function update(Request $request, $id)
    {  
        $motorpivot = motogeneradorPivot::where('id',decrypt($id))->first();
        $motor = motogenerador::where('id',$motorpivot->motor_id)->first();
        $generador = generador::where('id',$motorpivot->generador_id)->first();
        $ubi = ubicacion::where('id',$motor->ubicacion_id)->first();
        $central = centrales::where('id',$ubi->central_id)->first();

        

        $rules = motogeneradorPivot::$rule;
        Validator::make($request->all(),$rules,$this->messages)->validate();
        //dd($rules);
        foreach ($motor->batteryctrl as $key => $value) {
            $bateriaCtrl = BatteryCtrl::where('id',$value->id)->first();
            $bateriaCtrl->fill($request['bateriaControl'.$value->id]);
            $bateriaCtrl->save();
        }

        foreach ($motor->batteryctrl as $key => $value) {
            // $bateriaCtrl = BatteryCtrl::where('id',$key->id)->first();
            // $bateriaCtrl->fill($request['bateriaControl'.$key->id]);
            // $bateriaCtrl->save();
        }

        $responsable = personal::where('cod_p00',$request['responsable']['cod_p00'])->first();
        //dd($request->all(), $responsable);
        if($responsable){
            $responsable->fill($request['responsable']);
            $ubi['responsable_id'] =$responsable->id;
        }else{
            $pers = personal::create($request['responsable']);
            $ubi['responsable_id'] =$pers->id;
        }
        $motor->fill($request['motor']);
        $generador->fill($request['generador']);
        $ubi->fill($request['ubicacion']);
        $central->fill($request['central']);
       
        if ($motor->save() &&  $generador->save() && $ubi->save() && $central->save() && $responsable->save() ) {
            Session::flash('actualizado',true);
             if ($request->parametro != null) {
               return redirect('/nodos/'.$request->parametro);
           
            }
            return redirect('/motogenerador'); 
        }
    }

   
    public function destroy($id)
    {
        motogeneradorPivot::find($id)->delete();
    }
}
