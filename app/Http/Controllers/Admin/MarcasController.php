<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Models\AireA;
use App\Models\Models\regiones;
use App\Models\Models\localidades;
use App\Models\Models\estados;
use App\Models\Models\personal;
use App\Models\Models\ubicacion;
use App\Models\Models\centrales;
use App\Models\Models\marcas;
use App\Models\Models\equipo;
use Illuminate\Support\Facades\Validator;
use JsValidator;
use Session;
use Auth;

class MarcasController extends Controller
{
    
    public function index()
    {
        $marcas = marcas::get();
        return view('marcas.index',compact('marcas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $equipos = equipo::get();
        return view('marcas.create',compact('equipos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        foreach ($request->nombre as $key => $value) {
            marcas::create(['descripcion'=>$value,'equipo_id'=>$request->equipo_id,'user_id'=>Auth::user()->id]);
        }
            # code...
        Session::flash('exito',true);
        return redirect('/marcas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { // decrypt($id)
        $marcas = marcas::where('id',decrypt($id))->first();
        $equipos = equipo::get();

        return view('marcas.edit',compact('marcas','equipos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $marca = marcas::find(decrypt($id));
        $marca->fill($request->all());
        if ($marca->save()) {
            Session::flash('actualizado',true);
            return redirect('/marcas');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
