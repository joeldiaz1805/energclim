<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Http;
use JsValidator;
use Session;
use Auth;
class DpsSismycController extends Controller
{
    
        protected $rule=[
            'fechaInicio'=>'required|date',
            'fechaFin' => 'required|date|after_or_equal:fechaInicio',
        ];
        protected $messages =[
            'after_or_equal' => 'Fecha menor a la fecha Inicio',
            'required' => 'Campo Requerido',
            'date'=> 'Campo solo de tipo Fecha'
        ];
     public function index()
    {
        $data['validator'] = JsValidator::make($this->rule,$this->messages);
        return view('dps_sismyc.consult',$data);
    }
    
    public function create(Request $request)
    {  
        $response= Http::get('http://sismyc.cantv.com.ve/tickets-dps/'.base64_encode(date('Y-m-d')).'/'.base64_encode(date("Y-m-d", strtotime('tomorrow'))).'?api_token=ZhzJ6FUy23kp18UJFxaAEIfP3psZz94q7ysuCeWzld1VzoKv5VKkKDGk6UIX');
        $res = json_encode($response->json());
        $re  = json_decode($res);
        return view('dps_sismyc.resumen2',compact('re'));
    }
    
    public function store(Request $request)
    {   
        Validator::make($request->all(),$this->rule,$this->messages)->validate();
        $response= Http::get('http://sismyc.cantv.com.ve/tickets-dps/'.base64_encode($request->fechaInicio).'/'.base64_encode($request->fechaFin).'?api_token=ZhzJ6FUy23kp18UJFxaAEIfP3psZz94q7ysuCeWzld1VzoKv5VKkKDGk6UIX');
        $res = json_encode($response->json());
        $re  = json_decode($res);
        return view('dps_sismyc.index',compact('re'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
