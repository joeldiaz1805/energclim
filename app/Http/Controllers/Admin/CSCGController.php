<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Models\registro;
use App\Models\Models\regiones;
use App\Models\Models\localidades;
use App\Models\Models\estados;
use App\Models\Models\personal;
use App\Models\Models\ubicacion;
use App\Models\Models\centrales;
use App\Models\Models\equipo;
use App\Models\Models\componente;
use App\Models\Models\cuadrilla;
use App\Models\Models\contratista;
use Illuminate\Support\Facades\Validator;//te falta esta clase. A
use App\Models\Models\Multimedia;
use Illuminate\Support\Str;


use JsValidator;
use Session;

class CSCGController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $registro = registro::get();
        return view('registro.index', compact('registro'));
    }

    public function tipo_servicio($id)
    {
        return equipo::where('servicio_id', $id)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $rule=[

            'tipo_servicio' => 'required',
            'equipo_id'     => 'required',
            'componente_id' => 'required',
            'sala'          => 'required',
            'piso'          => 'required',
            'estatus'       => 'required',
            'accion'        => 'required',
            //'trabajo'       => 'required',
            'porcentaje'    => 'required',
            'fecha_inicio'  => 'required',
            //'fecha_fin'     => 'required',
            //'name_informe'  => 'required',
            'observaciones' => 'required',
           
            'central.localidad_id'   => 'required',
            'central.region_id'      => 'required',
            'central.estado_id'      => 'required',
            
        ];

    protected $messages =[
            'required' =>"Campo requerido",
            'min' => "Minimo :min digitos",
            'max' => "Maximo :max digitos",
            'email' => 'Formato email invalido',
            'mimes' => 'Formato de archivo invalido, admitido solo jpg,png,pdf',
            'date_format'=> 'Formato de Fecha invalido'
        ];
        
    public function create()
    {
        $regiones= regiones::orderBy('nombre', 'ASC')->get();
        $estados= estados::orderBy('nombre', 'ASC')->get();
        $localidades= localidades::orderBy('nombre', 'ASC')->get();
        
        $cuadrilla= cuadrilla::get();
        $contratista= contratista::get();
        $data['validator'] = JsValidator::make($this->rule, $this->messages);
        return view('registro.create', compact('regiones', 'estados', 'localidades', 'cuadrilla', 'contratista'), $data);
    }

    /*ESTO ES UN METODO */

    public function cuadrilla($id)
    {
        // el $id es que le mandas cuando escoges un select

        $cuadrilla = contratista::where('cuadrilla_id', $id)->get(); // first por qeu es un solo registro que quiero
        return $cuadrilla;  // eso es todo por aqui. tienes que crear la ruta , creo que ya la tienes.

    }
    // *//*


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), $this->rule, $this->messages)->validate();

        $central = centrales::create($request['central']);
        $request['ubicacion_id'] = $central->id;
        registro::create($request->all());

        Session::flash('exito', true);
        return redirect('/registro_actividades');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $registro = registro::where('id', decrypt($id))->first();
        return view('registro.show', compact('registro'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $regiones= regiones::get();
        $estados= estados::get();
        $localidades= localidades::get();
        $registro = registro::where('id', decrypt($id))->first();
        $equipos = equipo::get();
        $componentes = componente::get();
        $cuadrilla = cuadrilla::get();
        $contratista = contratista::get();

        unset($this->rule['observaciones']);
        unset($this->rule['accion']);
        unset($this->rule['fecha_inicio']);
        unset($this->rule['fecha_fin']);
        unset($this->rule['name_informe']);

        $data['validator'] = JsValidator::make($this->rule, $this->messages);
        return view('registro.edit', compact('equipos', 'registro', 'regiones', 'estados', 'localidades', 'componentes', 'cuadrilla', 'contratista'), $data);
    }

    public function archivos1($modelo, $datos)
    {
        //dd($datos);
        $random = Str::random(6);
        $destination = 'img/multimedio/';
        $image = $datos;
        $filename = $random.$image->getClientOriginalName();
        $image->move($destination, $filename);
        $modelo->archivo=$filename;
        $modelo->tipo = $image->getClientOriginalExtension();
        $modelo->save();
        return $modelo;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $registro = registro::where('id', decrypt($id))->first();
        unset($this->rule['fecha_inicio']);
        unset($this->rule['observaciones']);
        unset($this->rule['accion']);
        unset($this->rule['fecha_fin']);
        unset($request['name_informe']);
       
        Validator::make($request->all(), $this->rule, $this->messages)->validate();
        $info;
        $var = $request->all();
        if($request->hasFile('name_informe')) {

            $multi = new Multimedia();
            $info = $this->archivos1($multi, $request->name_informe);
            if ($info) {
                $var['name_informe'] =$info->id;
            }
        }
        $registro->fill($var);
        if($registro->save()) {
            Session::flash('actualizado', true);
            return redirect('/registro_actividades');
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        registro::find($id)->delete();
    }
}
