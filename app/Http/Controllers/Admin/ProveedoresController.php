<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Models\proveedores;
use App\Models\Models\personal;
use App\Models\Models\regiones;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Models\Multimedia;
use Illuminate\Support\Str;
use JsValidator;
use Session;
use Auth;


class ProveedoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proveedores = proveedores::get();
        return view('proveedores.index',compact('proveedores'));
    }

    public function create()
    {
        return view('proveedores.create');
    }

    
    public function store(Request $request)
    {
        for ($i=0; $i< sizeof($request->nombre) ; $i++) { 
            proveedores::create([
                'nombre'=>$request->nombre[$i],
                'cod_proveedor'=>$request->cod_proveedor[$i],
                'user_id'=>Auth::user()->id]);
        }
        
        Session::flash('exito',true);
        return redirect('/proveedores');
    }

   
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        $proveedor = proveedores::where('id',decrypt($id))->first();
        

        return view('proveedores.edit',compact('proveedor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $proveedor = proveedores::find(decrypt($id));
        $proveedor->fill($request->all());
        if ($proveedor->save()) {
            Session::flash('actualizado',true);
            return redirect('/proveedores');
        }
    }

   
    public function destroy($id)
    {
        proveedores::find(decrypt($id))->delete();

    }
}
