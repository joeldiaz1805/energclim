<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Models\personal;
use App\Models\Models\regiones;
use App\Models\Models\estados;
use App\Models\Models\detalleopsutpivot;
use App\Models\Models\requeopsut;
use App\Models\Models\nodoopsut;
use App\Models\Models\invequipopsut;
use App\Models\Models\equipo;
use App\Models\Models\localidades;
use App\Models\Models\motogeneradorPivot;
use App\Models\Models\AireA;
use App\Models\Models\rect_bcobb;
use App\Models\Models\ups_bancob;
use App\Models\Models\bancob;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Models\Multimedia;
use Illuminate\Support\Str;
use JsValidator;
use Session;
use Auth;




class NodoController extends Controller
{
    
    public function index()
    {
        

        $nodoopsut = localidades::where('tipo','OPSUT')->get();
        //return response()->json($nodoopsut);
        return view('nodos.index',compact('nodoopsut'));
    }

     public function nodosMG($id)
    {
        
      // dd($id);
        $motor = motogeneradorPivot::whereHas('motor', function ($query) use($id){
        $query->whereHas('ubicacion',function($query2) use($id){
          $query2->whereHas('central',function($query3) use($id){
              return $query3->where('localidad_id',decrypt($id));
                      });
          });
        })->get();
         $detalleopsutpivot = localidades::where('id',decrypt($id))->first();
        
        return response($motor);
        return view('nodos.equipo_mg',compact('motor'));


    }
      public function nodos_cuadroF($id)
    {
        
        $rectificadores = rect_bcobb::whereHas('ubicacion',function($query2) use($id){
        $query2->whereHas('central',function($query3) use($id){
            return $query3->where('localidad_id',decrypt($id));
        
        });
        })->get();
         $detalleopsutpivot = localidades::where('id',decrypt($id))->first();
        return view('nodos.equipo_cuadrof',compact('rectificadores'));


    }  
     public function nodos_mg($id)
    {
        
      // dd($id);
         $motor = motogeneradorPivot::whereHas('motor', function ($query) use($id){
        $query->whereHas('ubicacion',function($query2) use($id){
          $query2->whereHas('central',function($query3) use($id){
              return $query3->where('localidad_id',decrypt($id));
                      });
          });
        })->get();
         $detalleopsutpivot = localidades::where('id',decrypt($id))->first();
        
        //return response($rectificadores);
        return view('nodos.motog',compact('motor'));


    }
     public function nodos_aa($id)
    {
        
      // dd($id);
        $aa = AireA::whereHas('ubicacion',function($query2) use($id){
        $query2->whereHas('central',function($query3) use($id){
            return $query3->where('localidad_id',decrypt($id));
      
        });

        })->get();

        return view('nodos.equipo_aa',compact('aa'));


    }




     public function nodos_bcobb($id)
    {
        
    

      $bancob = bancob::whereHas('ubicacion',function($query2) use($id){
        $query2->whereHas('central',function($query3) use($id){
            return $query3->where('localidad_id',decrypt($id));
        
        });
        })->get();
         $detalleopsutpivot = localidades::where('id',decrypt($id))->first();
        
        //return response($rectificadores);
        return view('nodos.equipo_bcobb',compact('bancob'));


    }
  public function equipos_actualizados()
    {
        $localidades = localidades::where('tipo','OPSUT')->orderBy('nombre','ASC')->get();
       
        $mg = motogeneradorPivot::get();
        $rect = rect_bcobb::get();
        $ups = ups_bancob::get();
        $bb = bancob::get();
        $aire = AireA::get();
        

        
        return view('nodos.equipo_actualizados',compact('localidades','mg','rect','aire','ups','bb'));
    }
 
    public function store(Request $request)
    {
       //dd($request->all());

       if(detalleopsutpivot::create($request->all())){
        Session::flash('actualizado',true);
        return redirect('/nodos/'.$request->nodoopsut_id);
       }
    }

    public function equipos_nodo(){
        $nodoopsut = nodoopsut::with('estados')->get();
        //return response()->json($nodoopsut);
        return view('nodos.equi_nodos',compact('nodoopsut'));

    }

    
    public function show($id)
    {
        //

        $nodoopsut = localidades::where('id',decrypt($id))->first();
        //dd($id);
        $motor = motogeneradorPivot::whereHas('motor', function ($query) use($id){
        $query->whereHas('ubicacion',function($query2) use($id){
          $query2->whereHas('central',function($query3) use($id){
              return $query3->where('localidad_id',decrypt($id));
                      });
          });
        })->get();

        $aa = AireA::whereHas('ubicacion',function($query2) use($id){
        $query2->whereHas('central',function($query3) use($id){
            return $query3->where('localidad_id',decrypt($id));
      
        });

        })->get();



        $ups1 = ups_bancob::whereHas('ubicacion',function($query2)  use($id){
        $query2->whereHas('central',function($query3)  use($id){
            return $query3->where('localidad_id',decrypt($id));
        
        });
        })->get();

        $rectificadores = rect_bcobb::whereHas('ubicacion',function($query2) use($id){
        $query2->whereHas('central',function($query3) use($id){
            return $query3->where('localidad_id',decrypt($id));
        
        });
        })->get();

        $bancob = bancob::whereHas('ubicacion',function($query2) use($id){
        $query2->whereHas('central',function($query3) use($id){
            return $query3->where('localidad_id',decrypt($id));
        
        });
        })->get();

     //  return response($nodoopsut);
        $requeopsut = requeopsut::where('nodoopsut_id',decrypt($id))->get();
        
        $detalleopsutpivotaire = detalleopsutpivot::where('nodoopsut_id',decrypt($id))->get();
      
       
        return view('nodos.show2',compact('nodoopsut','motor','aa','ups1','rectificadores','bancob'));

    }

    public function resumen(){
        $localidades = localidades::whereDate('updated_at',date('Y-m-d'))->get();
        return view('nodos.resumen',compact('localidades'));
    }

     
    public function edit($id)
    {
     
        $localidad = localidades::where('id',decrypt($id))->first();
        $personal = personal::get();
        $regiones=regiones::get();
        $estados=estados::get();
         return view('nodos.edit',compact('localidad','personal','regiones','estados'));
    }
 
    public function update(Request $request, $id)
        {

          //dd($request->all());
          $anillos = "";
          foreach ($request->anillos as $key => $value) {
              $anillos=$anillos.'-'.$value;
          }
          unset($request['anillos']);
          $request['anillos'] = $anillos;
          $request['user_id'] = Auth::user()->id;
          //dd($request->all());
        $localidad = localidades::where('id',decrypt($id))->first();
        // guardado de las actualizaciones del nodo
         $localidad->fill($request->all());
             if($localidad->save()){

            Session::flash('actualizado',true);
           return redirect('/nodos');
           //  return view('nodos.index',compact('nodoopsut'));

}
        //
    }
    public function maps(){
        $credentials = 'AIzaSyDOhyoDSnsaNH5qRbetRYsVbXGEKUl60Sk';
        $nodos = nodoopsut::get();
     
        return view('nodos.maps',compact('nodos'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

 public function requerimiento($id, $idr)
    {
        
         $nodoopsut = nodoopsut::where('id',$id)->first();
        $requeopsut = requeopsut::where('nodoopsut_id',$id)->where('id', $idr)->with('invequipo2')->get();
        
  //return response ($requeopsut);

         return view('nodos.requerimiento',compact('nodoopsut', 'requeopsut'));



    }

public function act_requerimento(request $request, $nodoopsut_id, $id ){

 //dd($request->all());
        $requeopsut = requeopsut::where('id',$id)->first();
        // guardado de las actualizaciones del nodo
         $requeopsut->fill($request->all());
             if($requeopsut->save()){

            Session::flash('actualizado',true);

    $id = $nodoopsut_id;
//dd($id);
        $nodoopsut = nodoopsut::where('id',$id)->first();
        $requeopsut = requeopsut::where('nodoopsut_id',$id)->get();
        
     $detalleopsutpivotaire = detalleopsutpivot::where('nodoopsut_id',$id)->with('invequipo')->get();
       
 
         return view('nodos.show',compact('nodoopsut', 'requeopsut', 'detalleopsutpivotaire'));



}

}

public function add_requerimiento($id)
    {
        
         $nodoopsut = nodoopsut::where('id',$id)->first();
         
         $detalleopsutpivot = detalleopsutpivot::where('nodoopsut_id',$id)->with('invequipo')->get();
  
        $equipo = equipo::get();

 // return response ($equipo);

         return view('nodos.addrequerimiento',compact('nodoopsut', 'detalleopsutpivot', 'equipo'));



    }

 public function invequipo_equipo($id, $idr){

        $equipo = detalleopsutpivot::where('nodoopsut_id',$idr)->with('invequipo')->get();
        return $equipo;
}


}
