<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Models\Menu;
use App\Models\Models\menurol;
use App\Models\Models\SubMenu;
use App\Models\Models\roles;
use App\Models\Models\roles_sub;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Models\Multimedia;
use Illuminate\Support\Str;
use JsValidator;
use Session;
use Auth;

class SubMenuController extends Controller
{
    
    public function index()
    {   
        /*$roles= roles::with('menusrol')->first();
        return response($roles);
        */

        $submenu = SubMenu::get();
        return view('submenu.index',compact('submenu'));
    }

   
    public function create()
    {
       $menu = Menu::get();
       $roles = roles::get();
        return view('submenu.create',compact('menu','roles'));
    }

    
    public function store(Request $request)
    {   $roles=$request->rol_id;
        $request['rol_id']=1;
        $request['observacion'] ='-';
      //  dd($request->all());
        $submenu=SubMenu::create($request->all());
        foreach ($roles as $key => $value) {
                roles_sub::create(['submenu_id'=>$submenu->id,'rol_id'=>$value]);
            }
        Session::flash('success','Sub Menu Guardado con Èxito');
        return redirect('/submenu');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
       $submenu =SubMenu::find(decrypt($id));
       $menu = Menu::get();
       $roles = roles::get();

       $idmenus=[];
        foreach ($submenu->roles as $key => $value) {
            $idmenus[]=$value->id;
        }
       // return response($idmenus);
       return view('submenu.edit',compact('submenu','menu','roles','idmenus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        //dd($request->all());
        $submenu = SubMenu::find(decrypt($id));
        foreach ($request->roles as $key => $value) {
            roles_sub::where('submenu_id',decrypt($id))->delete();
        }
        foreach ($request->roles as $key => $value) {
         roles_sub::create(['submenu_id'=>decrypt($id),'rol_id'=>$value]);
        }

        $submenu->fill($request->all());
        if ($submenu->save()) {
            Session::flash('success','Sub Actualizado con Èxito');
            return redirect('/submenu');
            # code...
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SubMenu::find(decrypt($id))->delete();
    }
}
