<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Http;
use JsValidator;
use Session;
use Auth;

class SismycController extends Controller
{

        protected $rule=[
            'fechaInicio'=>'required|date',
            'fechaFin' => 'required|date|after_or_equal:fechaInicio',
        ];
        protected $messages =[
            'after_or_equal' => 'Fecha menor a la fecha Inicio',
            'required' => 'Campo Requerido',
            'date'=> 'Campo solo de tipo Fecha'
        ];
    public function index()
    {
        $data['validator'] = JsValidator::make($this->rule,$this->messages);
        return view('sismyc.consult',$data);
    }
    
    public function create(Request $request)
    {  
        $response= Http::get('http://sismyc.cantv.com.ve/tickets-ex/'.base64_encode(date('Y-m-d')).'/'.base64_encode(date("Y-m-d", strtotime('tomorrow'))).'?api_token=ZhzJ6FUy23kp18UJFxaAEIfP3psZz94q7ysuCeWzld1VzoKv5VKkKDGk6UIX');
        $res = json_encode($response->json());
        $re  = json_decode($res);
        return view('sismyc.resumen2',compact('re'));
    }


    public function con_graficas(){

        return view('graficas.consult');
    }

    public function graficas(Request $request){
        $mes = explode(',', $request->meses);
      
        $resp= Http::get('http://sismyc.cantv.com.ve/tickets-ex/'.base64_encode($mes[0]).'/'.base64_encode($mes[1]).'?api_token=ZhzJ6FUy23kp18UJFxaAEIfP3psZz94q7ysuCeWzld1VzoKv5VKkKDGk6UIX');
        $res = json_encode($resp->json());
        $re  = json_decode($res);
        //return view('sismyc.resumen2',compact('re'));
        //dd($resp);
        $valores = [];
        $contdor =0;
        $contdor_afectacion =0;
        $contdor_aba =0;
        $contdor_voz =0;
       $result = array();
       $result2 = array();
       $afectaciones = array();
       $nuevo = array();
       foreach ($re as $key => $value) {
            if ($value->ticket_severity->nombre_severidad == "Disponibilidad de Servicio" && $value->ticket_status->nombre_estado =="Resuelto" ) {
              // dd($re[$key]);
               $nuevo[]=$re[$key];
            }
       }
          // dd($nuevo);
        foreach($nuevo as $t) {
            $repeat=false;
            for($i=0;$i<count($result);$i++)
            {
                if($result[$i]['falla']==$t->ticket_failure->nombre_falla)
                {
                    $result[$i]['items']+=1;
                    $repeat=true;
                    break;
                }
            }
            if($repeat==false)
                $result[] = array('falla' => $t->ticket_failure->nombre_falla, 'items' => 1,
                    'region'=> $t->network_elements[0]->location->state->region->Nombre_region,
                    'impacto_aba'=>$t->impacto_aba,
                    'impacto_voz'=>$t->impacto_voz

            );
        }
         foreach($nuevo as $t) {
            $repeat=false;
            for($i=0;$i<count($result2);$i++)
            {
                if($result2[$i]['falla']==$t->ticket_failure->nombre_falla && $result2[$i]['region']==$t->network_elements[0]->location->state->region->Nombre_region  )
                {
                    $result2[$i]['impacto_voz']+=$t->impacto_aba;
                    $result2[$i]['impacto_voz']+=$t->impacto_voz;
                    $repeat=true;
                    break;
                }
            }
            if($repeat==false)
                $result2[] = array('falla' => $t->ticket_failure->nombre_falla,
                    'region'=> $t->network_elements[0]->location->state->region->Nombre_region,
                    'impacto_aba'=>$t->impacto_aba,
                    'impacto_voz'=>$t->impacto_voz

            );
        }
        
        //dd($result2,'http://sismyc.cantv.com.ve/tickets-ex/'.base64_encode($desde).'/'.base64_encode($hasta).'?api_token=ZhzJ6FUy23kp18UJFxaAEIfP3psZz94q7ysuCeWzld1VzoKv5VKkKDGk6UIX');
        $var = json_encode($result);
        $var2 = json_encode($result2);
        foreach ($result as $key => $value) {
            $contdor += $value['items'] ;
            // code...
        }
         foreach ($result2 as $key => $value) {
            $contdor_afectacion += $value['impacto_voz'] + $value['impacto_aba'] ;
            $contdor_voz += $value['impacto_voz'];
            $contdor_aba +=  $value['impacto_aba'] ;
            // code...
        }

        return view('graficas.graficas',compact('mes','resp','var','var2','result','result2','contdor','contdor_afectacion','contdor_aba','contdor_voz'));
    }
    
    public function store(Request $request)
    {   
        Validator::make($request->all(),$this->rule,$this->messages)->validate();
        $response= Http::get('http://sismyc.cantv.com.ve/tickets-ex/'.base64_encode($request->fechaInicio).'/'.base64_encode($request->fechaFin).'?api_token=ZhzJ6FUy23kp18UJFxaAEIfP3psZz94q7ysuCeWzld1VzoKv5VKkKDGk6UIX');
        $res = json_encode($response->json());
        $re  = json_decode($res);
        return view('sismyc.index',compact('re'));
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {   
        //
    }

    public function destroy($id)
    {
        //
    }
}
