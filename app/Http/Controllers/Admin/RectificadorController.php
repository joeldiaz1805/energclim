<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Models\regiones;
use App\Models\Models\localidades;
use App\Models\Models\estados;
use App\Models\Models\personal;
use App\Models\Models\ubicacion;
use App\Models\Models\centrales;
use App\Models\Models\rectificadores;
use App\Models\Models\rect_bcobb;
use App\Models\Models\bancob;
use App\Models\Models\marcas;

use Illuminate\Support\Facades\Validator;

use JsValidator;
use Session;
use Auth;

class RectificadorController extends Controller
{
 public function __construct(){
        $this->middleware('auth');
    }
   


        protected $messages =[
            'required' =>"Campo requerido",
            'min' => "Minimo :min digitos",
            'max' => "Maximo :max digitos",
            'email' => 'Formato email invalido',
            'unique'=> 'Campo ya registrado',
            'numeric' => 'Solo valores numèricos',
            'required_if' => 'campo requerido si la opción "Falla" esta seleccionada en Operatividad'
        ];
    public function index()
    {
        if (Auth::user()->rol_id == 4) {
            
            $rectificadores = rect_bcobb::whereHas('ubicacion',function($query2){
                $query2->whereHas('central',function($query3){
                    return $query3->where('region_id',Auth()->user()->region_id);
                });
            })->get();
             
        }else{
            
            $rectificadores = rect_bcobb::get();

        }
        
        return view('rectificador.index',compact('rectificadores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $parametros = ($request->request->all());
        $regiones= regiones::orderBy('nombre','ASC')->get();
        $estados= estados::orderBy('nombre','ASC')->get();
        $localidades= localidades::orderBy('nombre','ASC')->get();
           $marcas = marcas::whereequipo_id(2)->orderBy('descripcion','ASC')->get();
           
                
           
        $data['validator'] = JsValidator::make(rect_bcobb::$rule,$this->messages);
        return view('rectificador.create',compact('marcas','regiones','estados','localidades','parametros'),$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());

        Validator::make($request->all(),rect_bcobb::$rule,$this->messages)->validate();
        $verifyPers = personal::where('cod_p00',$request['responsable']['cod_p00'])->first();
        $verifySuper = personal::where('cod_p00',$request['supervisor']['cod_p00'])->first();

        $centro =$request['ubicacion'];
        if($verifyPers){
            $centro['responsable_id'] =$verifyPers->id;
        }else{
            $pers = personal::create($request['responsable']);
            $centro['responsable_id'] =$pers->id;
        }

        if($verifySuper){
            $centro['supervisor_id'] =$verifySuper->id;
        }else{
            $superv = personal::create($request['supervisor']);
            $centro['supervisor_id'] =$superv->id;
        }


        $centrar = centrales::create($request['central']);

        $centro['central_id'] =$centrar->id;
        $ubi = ubicacion::create($centro);
        $rect = rectificadores::create($request['rectificador']);
        //$bco_baterias = bancob::create($request['bco_baterias']);

        $rect_bcob = $request['rect_bcobb'];
        $rect_bcob['rect_id'] =$rect->id;
        $rect_bcob['bancob_id'] =2;
        $rect_bcob['ubicacion_id'] = $ubi->id;
        $rect_bcob['tipo_id'] =2;

        //dd($rect_bcob);
        $pivot = rect_bcobb::create($rect_bcob);
        Session::flash('exito',true);
        if ($request->parametro != null) {
            return redirect('/nodos/'.$request->parametro);
            # code...
        }
        return redirect('/cuadro_fuerza');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rectificador = rect_bcobb::where('id',decrypt($id))->first();
        return view('rectificador.show',compact('rectificador'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {   
        $parametros = $request->request->all();
        //dd($parametros);
        $rect = rect_bcobb::where('id',decrypt($id))->first();
        $rule = rect_bcobb::$rule;
        
       // $rule['rectificador.serial'] =$rule['rectificador.serial'].','.$rect->rectificador->id;
        //$rule['bco_baterias.serial'] =$rule['bco_baterias.serial'].','.$rect->bancob->id;
      

        $regiones= regiones::orderBy('nombre','ASC')->get();
        $estados= estados::orderBy('nombre','ASC')->get();
        $localidades= localidades::orderBy('nombre','ASC')->get();
           $marcas = marcas::whereequipo_id(2)->orderBy('descripcion','ASC')->get();

        $data['validator'] = JsValidator::make($rule,$this->messages);
        return view('rectificador.edit',compact('marcas','regiones','estados','localidades','rect','parametros'),$data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rect = rect_bcobb::where('id',decrypt($id))->first();

        $react = rectificadores::where('id',$rect->rect_id)->first();
        //$bancob = bancob::where('id',$rect->bancob_id)->first();

        $ubi = ubicacion::where('id',$rect->ubicacion_id)->first();
        $central = centrales::where('id',$ubi->central_id)->first();
        $responsable = personal::where('id',$ubi->responsable_id)->first();
        $supervisor = personal::where('id',$ubi->supervisor_id)->first();
        $rules = rect_bcobb::$rule;
        /*
        $rules['responsable.cedula'] =$rules['responsable.cedula'].','.$responsable->id;
        $rules['responsable.cod_p00'] =$rules['responsable.cod_p00'].','.$responsable->id;*/
        Validator::make($request->all(),$rules,$this->messages)->validate();

        $react->fill($request['rectificador']);
        //$bancob->fill($request['bco_baterias']);

        $rect->fill($request['rect_bcobb']);

        $central->fill($request['central']);

        $responsable->fill($request['responsable']);
        $supervisor->fill($request['supervisor']);

        if($rect->save() && $react->save() && $ubi->save() && $central->save() && $responsable->save() && $supervisor->save()) {

            Session::flash('actualizado',true);
             if ($request->parametro != null) {
               return redirect('/nodos/'.$request->parametro);
           
            }
            return redirect('/cuadro_fuerza'); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        rect_bcobb::find($id)->delete();
    }
}
