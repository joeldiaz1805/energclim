<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Models\usuarios;
use App\Models\Models\regiones;
use App\Models\Models\localidades;
use App\Models\Models\estados;
use App\Models\Models\equipo;
use App\Models\Models\personal;
use App\Models\Models\ubicacion;
use App\Models\Models\centrales;
use App\Models\Models\componente;
use App\Models\Models\mantenimiento;
use App\Models\Models\motogeneradorPivot;
use App\Models\Models\fallas;
use App\Models\Models\acciones;
use App\Models\Models\salas;
use App\Models\Models\pisos;
use Auth;
use JsValidator;
use Session;
class PisosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
    {
         $pisos = pisos::get();
        return view('pisos.index',compact('pisos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function create()
         {
             return view('pisos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());

        foreach ($request->nombre as $key => $value) {
            pisos::create(['nombre'=>$value]);
        }
            # code...
        Session::flash('exito',true);
        return redirect('/pisos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $piso = pisos::find(decrypt($id));
        return view('pisos.edit',compact('piso'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $piso = pisos::find(decrypt($id));
        $piso->fill($request->all());
        if ($piso->save()) {
            Session::flash('actualizado',true);
            return redirect('/pisos');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        pisos::find($id)->delete();
    }
}
