<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Models\personal;
use App\Models\Models\regiones;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Models\Multimedia;
use Illuminate\Support\Str;
use JsValidator;
use Session;
use Auth;


class PersonalController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
}
 protected $rule=[


           'nombre'=> 'required',
           'apellido'=> 'required',
           'cedula'=> 'required|unique:personal,cedula',
           'cod_p00'=>'required|unique:personal,cod_p00',
           'region_id'=>'required',
           
           'correo'=>'required|email|unique:personal,correo',
           'cargo'=>'required',
           'status'=>'required',
          
          
           ];
           protected $messages =[
            'required' =>"Campo Requerido",
            'min' => "Minimo :min digitos",
            'max' => "Maximo :max digitos",
            'email' => 'Formato de Correo Invalido',
            'same' => 'No coincide con Password',
            'unique' => 'Ya Esta Registrado, intente con otro'
            ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   


    

    public function index()
    {
        
       $personal = personal::get();
        return view('personal.index',compact('personal'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
       public function archivos1($modelo,$datos){
            //dd($datos);
            $random = Str::random(6);
            $destination = 'img/multimedio/avatar/';
            $image = $datos;
            $filename = $random.$image->getClientOriginalName();
            $image->move($destination, $filename);
            $modelo->archivo=$filename;
            $modelo->tipo = $image->getClientOriginalExtension();
            $modelo->save();
            return $modelo;

        }
    public function create()
    {
   $data['validator'] = JsValidator::make($this->rule,$this->messages);
    $personal = personal::get();
    $regiones= regiones::orderBy('nombre','ASC')->get();   
    return view('personal.create',compact('regiones','personal'),$data);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    // dd($request->all()); 
$var;
 
Validator::make($request->all(),$this->rule,$this->messages)->validate();
 
if($request->hasFile('archivo')){
    $new = new Multimedia();
    $var =$this->archivos1($new,$request->archivo);
    if($var){
        $request['FotoPersonal']=$var->id;
        
    }
}
 //$request['validator'] = JsValidator::make($this->rule,$this->messages);
   
     personal::create($request->all());
    return redirect('/personal');
}
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //



        
        $personal = personal::where('id',$id)->first();
         return view('personal.show',compact('personal'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


        $regiones= regiones::get();   
        $personal = personal::where('id',$id)->first();
        $supervisores = personal::get();

       unset($this->rule['cedula']);
       unset($this->rule['cod_p00']);
       unset($this->rule['correo']);
       $this->rule['cedula']='required|unique:personal,cedula,'.$personal->id;
       $this->rule['cod_p00']='required|unique:personal,cod_p00,'.$personal->id;
       $this->rule['correo']='required|unique:personal,correo,'.$personal->id;

        //Validator::make($request->all(),$this->rule,$this->messages)->validate();
        
        $data['validator'] = JsValidator::make($this->rule,$this->messages);

        return view('personal.edit',compact('personal','regiones','supervisores'),$data);

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
 // dd($request->all());
        $personal = personal::where('id',$id)->first();

        unset($this->rule['cedula']);
       unset($this->rule['cod_p00']);
       unset($this->rule['correo']);
       $this->rule['cedula']='required|unique:personal,cedula,'.$personal->id;
       $this->rule['cod_p00']='required|unique:personal,cod_p00,'.$personal->id;
       $this->rule['correo']='required|unique:personal,correo,'.$personal->id;    

        Validator::make($request->all(),$this->rule,$this->messages)->validate();

       if($request->hasFile('archivo')){
    $new = new Multimedia();
    $var =$this->archivos1($new,$request->archivo);
    if($var){
        $request['FotoPersonal']=$var->id;
        
    }
}


        $personal->fill($request->all());
         if($personal->save()){

            Session::flash('actualizado',true);
            return redirect('/personal');
         }
         }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        personal::find($id)->delete();
    }
}
