<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use resource\views\opsut\nodos;
use App\Models\Models\personal;
use App\Models\Models\regiones;
use App\Models\Models\estados;
use App\Models\Models\detalleopsutpivot;
use App\Models\Models\requeopsut;
use App\Models\Models\nodoopsut;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Models\Multimedia;
use Illuminate\Support\Str;
use JsValidator;
use Session;
use Auth;





class RequeOpsutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
       $requeopsut= requeopsut::get();
        return view("reqopsut.index",compact('requeopsut'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     
   // $data['validator'] = JsValidator::make($this->rule,$this->messages);

    $regiones= regiones::orderBy('nombre','ASC')->get();   
    $estados= estados::where('id', $estados_id)->get();;
    $personal= personal::where('id',$personal_id)->get();;
    $nodoopsut = nodoopsut::where('id',$id)->first()->get();;
    $requeopsut = requeopsut::where($id,'nodoopsut_id')->get();;
    return view('requerimiento',compact('regiones','personal','estados','nodoopsut'));



        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
      


        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
