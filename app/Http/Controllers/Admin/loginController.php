<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Models\usuarios;
use App\Models\Models\regiones;
use App\Models\Models\localidades;
use App\Models\Models\estados;
use App\Models\Models\equipo;
use App\Models\Models\proveedores;

use App\Models\Models\personal;
use App\Models\Models\ubicacion;
use App\Models\Models\centrales;
use App\Models\Models\componente;
use App\Models\Models\mantenimiento;
use App\Models\Models\motogeneradorPivot;
use App\Models\Models\fallas;
use App\Models\Models\acciones;
use App\Models\Models\salas;
use App\Models\Models\pisos;
use Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;

use JsValidator;
use Session;
use Illuminate\Support\Facades\Validator;


class loginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $rules = ['email'=>'required|email|exists:users,email'];
    protected $message =['exists'=>'Email no encontrado'];
    public function index()
    {
        //

         if (Auth::check())
        {
            // Si tenemos sesión activa mostrará la página de inicio
            return redirect('/home');
        }else{
            return view('layouts.login');

        }
        
    }

    public function mapa_vector(){
        return view('mapa_vectorial.test');
    }

    public function generador_sismyc()
    {
        $regiones= regiones::get();
        $contratistas = proveedores::get();
        $estados= estados::get();
        $localidades= localidades::get();
        $responsables = personal::get();
        $acciones = acciones::orderBy('nombre','asc')->get();
        $salas = salas::orderBy('nombre','asc')->get();
        $pisos = pisos::orderBy('nombre','asc')->get();
        $equipos = equipo::orderBy('descripcion','asc')->get();
        return view('ticket.generador_txt',compact('regiones','estados','localidades','responsables','contratistas','salas','pisos','acciones','equipos'));
    
    }
    public function generador_sismyc_nuevo()
    {
        $regiones= regiones::get();
        $contratistas = proveedores::get();
        $estados= estados::get();

        $localidades= localidades::get();
        $responsables = personal::get();
        $acciones = acciones::orderBy('nombre','asc')->get();
        $salas = salas::orderBy('nombre','asc')->get();
        $pisos = pisos::orderBy('nombre','asc')->get();
        $equipos = equipo::orderBy('descripcion','asc')->get();
        return view('ticket.generador_nuevo',compact('regiones','estados','localidades','responsables','contratistas','salas','pisos','acciones','equipos'));
    }

 public function centrales($id){
        if ($id == 0) {
            $central = localidades::with('estados.region')->get()->orderBy('nombre','asc');

            return view('ticket.centrales',compact('central'));
        }else{
            $central = localidades::with('estados.region')->where('id',$id)->get();
        }
        return $central;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

      public function recuperar_password()
    {
        $data['validator'] = JsValidator::make($this->rules,$this->message);

        return view('layouts.email_forgot_password',$data);
    }

     public function emails(Request $request){
       
        Validator::make($request->all(),$this->rules,$this->message)->validate();
        $usuario = usuarios::whereemail($request->email)->first();
        $token = Str::random(64);
        $data=['email'=>$request->email,'name'=>$usuario->name.' '.$usuario->last_name,'token'=>$token];

        $usuario->remember_token = $token;
        $usuario->save();
        Mail::send('emails.confirmation_code', $data, function($message) use ($data) {
        $message->to($data['email'], $data['name'])->subject('Recupera Tu Contraseña');
    });
}

    public function texto_apertura()
    {
        $m=date('m')- 3;
        $date =date('Y').'-'.$m.'-'.date('d');
        $ldate = date('Y-m-d H:i:s');
        $regiones= regiones::get();
        $equipos = equipo::get();
        $estados= estados::get();
        $localidades= localidades::get();
        $componente = componente::get();
        $responsables = personal::get();
          $acciones = acciones::orderBy('nombre','asc')->get();
        $salas = salas::orderBy('nombre','asc')->get();
        $pisos = pisos::orderBy('nombre','asc')->get();
        

    return view('layouts.documentacion',compact('regiones','estados','localidades','responsables','ldate','equipos','componente','acciones','salas','pisos'));

    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            dd($request->all());
            // Authentication passed...
            return redirect()->intended('/home');
        }
    }

    public function logout(){
        Auth::logout();
        return redirect('/');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $credentials = $request->only('email', 'password');
           // dd($credentials);
        //dd(Auth::attempt($credentials));
        $user = usuarios::where('alias',$request->email)->orwhere('email',$request->email)->first();
        if (!$user) {
            return redirect()->back()->with('error_message', 'Usuario no Registrado')->withInput();
            
        }

        if (Auth::check())
        {
            // Si tenemos sesión activa mostrará la página de inicio
            return redirect('/home');
        }
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password]) || Auth::attempt(['alias' => $request->email, 'password' => $request->password]) ){
            if($user->int_fail== 3){
                Session::flash('PwErr',true);
                return redirect()->back()->with('error_message', 'Demasiados intentos fallidos')->withInput();
            }else{
                $user->int_fail =null;
                $user->save();
                return redirect()->intended('/home');
            }
            
        }else{
            if($user->int_fail== 3){
                Session::flash('PwErr',true);
                return redirect()->back()->with('error_message', 'Demasiados intentos fallidos')->withInput();
            }else{
                $user->int_fail +=1;
                $user->save();
                return redirect()->back()->with('error_message', 'Datos errados')->withInput();


            }
            return redirect()->back()->with('error_message', 'Datos errados')->withInput();
        }
       // return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function componentes($id){

        $componentes = componente::where('equipo_id',$id)->get();
        return $componentes;
    }
     public function estado($id){

        $estado = estados::where('region_id',$id)->orderBy('nombre','asc')->get();
        return $estado;
    }
     public function central($id){

        $central = localidades::with('estados.region','municipio')->where('estado_id',$id)->orderBy('nombre','asc')->get();
        return $central;
    }
    public function personal($id){

        $personal = personal::with('estado')->where('estado_id',$id)->get();
        return $personal;
    }
   

    public function fallas($id){

        $fallas = fallas::where('tipo',$id)->orderBy('descripcion','asc')->get();
        return $fallas;
    }
    public function mantenimiento($id,$mnt){

        $mantimiento = mantenimiento::where('falla_id',$id)->where('tipo',$mnt)->get();
        return $mantimiento;
    }
    public function mantenimiento2($mnt){

        $mantimiento = mantenimiento::where('tipo',$mnt)->get();
        return $mantimiento;
    }
    public function tipo_servicio($id)
    {
        return equipo::where('servicio_id',$id)->orderBy('descripcion','asc')->get();
    }

    public function tipo_actividad($id)
    {
        return acciones::where('actividad',$id)->orderBy('nombre','asc')->get();
    }
    
     public function equipos2($id){
        switch ($id) {
            case 1:
                $resp = motogeneradorPivot::with('motor','generador')->get();
                if (sizeof($resp) != 0) {
                    $ubicacion = $resp;
                }else{
                    return response($resp);
                    $ubicacion['marca'] = "Sin Registro";
                    $ubicacion['serial'] = "000000000";
                }
                break;
            case 2:
                $resp = rect_bcobb::with('rectificador','bancob')->get();
                if (sizeof($resp) != 0) {
                    $ubicacion =$resp;
                }else{
                    return response($resp);
                    $ubicacion['marca'] = "Sin Registro";
                    $ubicacion['serial'] = "000000000";
                }
                
                break;
            case 3:
                $resp = AireA::get();
                if (sizeof($resp) != 0) {
                    $ubicacion =$resp;
                }else{
                    return response($resp);
                    $ubicacion['marca'] = "Sin Registro";
                    $ubicacion['serial'] = "000000000";
                }
                break;
            case 7:
                $resp = ups_bancob::with('ups','bancob','ubicacion')->get();
                if (sizeof($resp) != 0) {
                    $ubicacion =$resp;
                }else{
                    return response($resp);
                    $ubicacion['marca'] = "Sin Registro";
                    $ubicacion['serial'] = "000000000";
                }
                break;
            
            default:
                 $ubicacion = "No disponible";
                break;
        }
        return response($ubicacion);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
