<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Models\usuarios;
use App\Models\Models\roles;
use App\Models\Models\regiones;
use App\Models\Models\permisos;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use JsValidator;
use Session;
use Auth;
use Hash;
use Illuminate\Validation\Rule;
class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    }
     protected $rule=[
           'name'=> 'required',
           'cedula'=> 'required|numeric',
           'last_name'=> 'required',
           'email'=> 'required|email|unique:users,email',
           'password'=>'required|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9]).{8,}$/',
           'confirm_password' =>'same:password|required'
           
        ];


    protected $messages =[
            'required' =>"Campo requerido",
            'min' => "Minimo :min digitos",
            'max' => "Maximo :max digitos",
            'email' => 'Formato email invalido',
            'same' => 'No coincide con Password',
            'unique' => 'Email ya se encuentra en uso, intente con otro',
            'regex'=>'Debe Contener al menos 1 Mayuscula, 1 minuscula, nùmeros, caracter especial y minimo 8 digitos'
        ];
    public function index()
    {
        $usuarios = usuarios::get();
        return view('usuarios.index',compact('usuarios'));
    }

    
   


    public function create()
    {
        $roles = roles::get();
        $regiones = regiones::get();

        $this->rule['email'] = 'unique:App\Models\Models\usuarios,email';
        $data['validator'] = JsValidator::make($this->rule,$this->messages);
        return view('usuarios.create',compact('roles','regiones'),$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(),$this->rule,$this->messages)->validate();
        //dd($request->all());
     
        $pass=Hash::make($request->password);
        $request['password'] =$pass;
        unset($request['confirm_password']);
        unset($request['_token']);
        if(usuarios::create($request->all())){

            Session::flash('exito',true);
            return redirect('/usuarios');
        }



        //dd($request->all());
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        //dd($id);
        $usuarios = usuarios::where('id',decrypt($id))->first();
        return view('usuarios.show',compact('usuarios'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuarios = usuarios::where('id',decrypt($id))->first();
        $roles = roles::get();
        $regiones = regiones::get();
        unset($this->rule['email']);
        $this->rule['email'] ='required|email|unique:users,email,'.$usuarios->id;
        $data['validator'] = JsValidator::make($this->rule,$this->messages);
        return view('usuarios.edit',compact('usuarios','roles','regiones'),$data);
    }

    public function mod_usuario($id){
        $usuarios = usuarios::where('id',decrypt($id))->first();
        $roles = roles::get();
        unset($this->rule['email']);
        $rule['password'] ='required|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9]).{8,}$/';
        $rule['confirm_password'] ='required|same:password';
        $data['validator'] = JsValidator::make($rule,$this->messages);
        return view('usuarios.mod_pass',compact('usuarios','roles'),$data);   
    }


    public function mod_update(Request $request, $id)
    {  
        //dd($resquest->all());
        $usuarios = usuarios::where('id',decrypt($id))->first();
        $rule['password'] ='required|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9]).{8,}$/';
        $rule['confirm_password'] ='required|same:password';
        Validator::make($request->all(),$rule,$this->messages)->validate();
        if (Hash::check($request->password_actual, Auth::user()->password)) {
            $pass=Hash::make($request->password);
            $request['password'] =$pass;
            //unset($request['password']);
            unset($request['password_actual']);
            unset($request['confirm_password']);
            unset($request['_token']);
            //dd($request->all());
            $usuarios->fill($request->all());
             if($usuarios->save()){
                Session::flash('actualizado',true);
                return redirect('/mod_usuario/'.encrypt($id));
            }
        }else{
             Session::flash('pass_error',true);
                return redirect('/mod_usuario/'.encrypt($id)); 
        }

    }

  



    public function update(Request $request, $id)
    {   

        $usuarios = usuarios::where('id',decrypt($id))->first();
        unset($this->rule['email']);
        $this->rule['email'] ='required|email|unique:users,email,'.$usuarios->id;
        Validator::make($request->all(),$this->rule,$this->messages)->validate();
        //dd('aqui voy');
        $per = $request->permisos;
        if ($per != null) {
            //$per[]='GET';
           // dd($request->all());

            $p =permisos::where('user_id',decrypt($id))->get();
            foreach ($p as $key => $value) {
            //dd($value);
                $value->delete();
                }
                foreach ($per as $key => $value) {
                    permisos::create(['user_id'=>decrypt($id),
                                        'permiso'=>$value]);
                }

        }

        if($request->password == $usuarios->password){
            unset($request['password']);
            unset($request['confirm_password']);
            unset($request['_token']);

        }else{
            $pass=Hash::make($request->password);
            $request['password'] =$pass;
        }
      
        $usuarios->fill($request->all());
         if($usuarios->save()){
            if (Auth::user()->rol_id != 1 ) {
               Session::flash('actualizado',true);
               return redirect('/usuarios/'.encrypt($id).'/edit');
            }
            Session::flash('actualizado',true);
            return redirect('/usuarios');
        }

    }

    public function reset_passw(Request $request, $id){

        $user = usuarios::where('id',decrypt($id))->first();
        $user->int_fail =null;
        $user->password=Hash::make(12345678);
        if ($user->save()) {
            Session::flash('actualizado',true);
            return redirect('/usuarios');
            # code...
        }
    }

  

    public function emails(){
        $data['email']='cdelga14@cantv.com.ve';
        $data['name']='Correo de PRueba';


         Mail::send('emails.confirmation_code', $data, function($message) use ($data) {
        $message->to($data['email'], $data['name'])->subject('Por favor confirma tu correo');
    });

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user =usuarios::find($id);
        if($user->active == 1){
            $user->active = 2;
        }else{
            $user->active = 1;
        }
            $user->save();
    }
}
