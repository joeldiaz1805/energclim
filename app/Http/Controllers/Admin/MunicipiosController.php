<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Models\Menu;
use App\Models\Models\menurol;
use App\Models\Models\roles;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Models\Multimedia;
use App\Models\Models\Municipio;
use App\Models\Models\estados;
use Illuminate\Support\Str;
use JsValidator;
use Session;
use Auth;

class MunicipiosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $municipios = Municipio::get();
        return view('municipios.index',compact('municipios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $estados = estados::get();
        return view('municipios.create',compact('estados'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        foreach ($request->descripcion as $key => $value) {
            Municipio::create(['descripcion'=>$value,'estado_id'=>$request->estado_id,'user_id'=>Auth::user()->id]);
       }
        Session::flash('sucess','Municipio Registrado Con Exito');
        return redirect('/municipios');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $estado = estados::get();
        $municipio = Municipio::find($id);
        return view('municipios.edit',compact('estado','municipio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $municipio = Municipio::find($id);

       $municipio->fill($request->all());

       if ($municipio->save()) {

            Session::flash('sucess','Municipio Actualizado con Exito');
            return redirect('/municipios');
           // code...
       }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Municipio::find($id)->delete();
    }
}
