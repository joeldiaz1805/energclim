<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Http;
use JsValidator;
use Session;
use Auth;
class CdcSismycController extends Controller
{
        protected $rule=[
            'fechaInicio'=>'required|date',
            'fechaFin' => 'required|date|after_or_equal:fechaInicio',
        ];
        protected $messages =[
            'after_or_equal' => 'Fecha menor a la fecha Inicio',
            'required' => 'Campo Requerido',
            'date'=> 'Campo solo de tipo Fecha'
        ];
    public function index()
    {
        $data['validator'] = JsValidator::make($this->rule,$this->messages);
        return view('cdc_sismyc.consult',$data);
    }
    
    public function create(Request $request)
    {  
        $response= Http::get('http://sismyc.cantv.com.ve/cdc-2-ex/'.base64_encode(date('Y-m-d')).'/'.base64_encode(date("Y-m-d", strtotime('tomorrow'))).'?api_token=ZhzJ6FUy23kp18UJFxaAEIfP3psZz94q7ysuCeWzld1VzoKv5VKkKDGk6UIX');
        $res = json_encode($response->json());
        $re  = json_decode($res);
        return view('cdc_sismyc.resumen2',compact('re'));
    }
    
    public function store(Request $request)
    {   
        Validator::make($request->all(),$this->rule,$this->messages)->validate();
        $response= Http::get('http://sismyc.cantv.com.ve/cdc-2-ex/'.base64_encode($request->fechaInicio).'/'.base64_encode($request->fechaFin).'?api_token=ZhzJ6FUy23kp18UJFxaAEIfP3psZz94q7ysuCeWzld1VzoKv5VKkKDGk6UIX');
        $res = json_encode($response->json());
        $re  = json_decode($res);
        return view('cdc_sismyc.index',compact('re'));
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
