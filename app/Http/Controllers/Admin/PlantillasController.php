<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Models\Plantillas;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use App\Models\Models\Multimedia;

use JsValidator;
use Session;
use Auth;
class PlantillasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd($_SERVER["REMOTE_ADDR"],$_SERVER['HTTP_USER_AGENT']);
        
        
        $plantillas = Plantillas::get();
        return view('planillas.index',compact('plantillas'));
    }

     protected $messages =[
            'required' =>"Campo requerido",
            'min' => "Minimo :min digitos",
            'max' => "Maximo :max digitos",
            
            'mimes' => 'Formato de archivo invalido, admitido solo pdf,docx,excel',
            
        ];
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['validator'] = JsValidator::make(Plantillas::$rule,$this->messages);
        return view('planillas.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //
        Validator::make($request->all(),Plantillas::$rule,$this->messages)->validate();
        $vari = $request->all();
        if ($request->hasFile('formato_id')) {
            $new = new Multimedia();
            $var = $this->archivos1($new,$request->formato_id);
            if ($var) {
                $vari['formato_id'] = $var->id;
            }
        }
        //dd($request->all());
       // 
        Plantillas::create($vari);
        Session::flash('exito',true);
        return redirect('/format');
        
    }

    public function archivos1($modelo,$datos){
        $random = Str::random(6);
        $destination = 'img/multimedio/planillas/';
        $image = $datos;
        $filename = $random.$image->getClientOriginalName();
        $image->move($destination, $filename);
        $modelo->archivo=$filename;
        $modelo->tipo = $image->getClientOriginalExtension();
        $modelo->save();
        return $modelo;

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
