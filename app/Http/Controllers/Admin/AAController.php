<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Models\AireA;
use App\Models\Models\regiones;
use App\Models\Models\localidades;
use App\Models\Models\estados;
use App\Models\Models\personal;
use App\Models\Models\ubicacion;
use App\Models\Models\centrales;
use App\Models\Models\marcas;

use Illuminate\Support\Facades\Validator;
use JsValidator;
use Session;
use Auth;


class AAController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         if (Auth::user()->rol_id == 4) {
            
             $aa = AireA::whereHas('ubicacion',function($query2){
                    $query2->whereHas('central',function($query3){
                        return $query3->where('region_id',Auth()->user()->region_id);

                    });
              
                 })->get();
             
        }else{
            
            $aa = AireA::get();

        }
        return view('aa.index',compact('aa'));
    }

   

        protected $messages =[
            'required' =>"Campo requerido",
            'min' => "Minimo :min digitos",
            'max' => "Maximo :max digitos",
            'email' => 'Formato email invalido',
            'unique'=> 'Campo ya registrado',
            'required_if' => 'campo requerido si la opción "Falla" esta seleccionada en Operatividad',
            'numeric' => 'Solo valores Numericos'
        ];
   

    public function create(Request $request)
    {
        $parametros = ($request->request->all());
        $regiones= regiones::orderBy('nombre','ASC')->get();
        $estados= estados::orderBy('nombre','ASC')->get();
        $localidades= localidades::get();
           $marcas = marcas::whereequipo_id(3)->orderBy('descripcion','ASC')->get();
        $data['validator'] = JsValidator::make(AireA::$rule,$this->messages);
        return view('aa.create',compact('marcas','regiones','estados','localidades','parametros'),$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

        Validator::make($request->all(),AireA::$rule,$this->messages)->validate();
        $verifyPers = personal::where('cod_p00',$request['responsable']['cod_p00'])->first();

        $centro =$request['ubicacion'];
        if($verifyPers){
            $centro['responsable_id'] =$verifyPers->id;
        }else{
            $pers = personal::create($request['responsable']);
            $centro['responsable_id'] =$pers->id;
        }

        $centrar = centrales::create($request['central']);
        $centro['central_id'] =$centrar->id;
        //$centro['responsable_id'] =$pers->id;
        $ubi = ubicacion::create($centro);
        $aire = $request['aireA'];
        $aire['ubicacion_id'] = $ubi->id;
        $aire['tipo_id'] =3;

        $airea= AireA::create($aire);
        Session::flash('exito',true);
        if ($request->parametro != null) {
            return redirect('/nodos/'.$request->parametro);
            # code...
        }
        return redirect('/aire_acondicionado');
        //
    }

    public function show($id)
    {
        $aire  = AireA::where('id',decrypt($id))->first();
        return view('aa.show',compact('aire'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit(Request $request,$id)
    {
        $aire= AireA::where('id',decrypt($id))->first();
      $parametros = ($request->request->all());
        $rule = AireA::$rule;
        /*
        $rule['aireA.serial']= $rule['aireA.serial'].','.$aire->id;
        $rule['responsable.cedula']= $rule['responsable.cedula'].','.$aire->ubicacion->responsable->id;
        $rule['responsable.cod_p00']= $rule['responsable.cod_p00'].','.$aire->ubicacion->responsable->id;
        //$rule['responsable.correo']= $rule['responsable.correo'].','.$aire->ubicacion->responsable->id;
*/
        $regiones= regiones::orderBy('nombre','ASC')->get();
        $estados= estados::orderBy('nombre','ASC')->get();
        $localidades= localidades::orderBy('nombre','ASC')->get();
           $marcas = marcas::whereequipo_id(3)->orderBy('descripcion','ASC')->get();
        
        $data['validator'] = JsValidator::make($rule,$this->messages);
        return view('aa.edit',compact('marcas','regiones','estados','localidades','aire','parametros'),$data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $aire= AireA::where('id',decrypt($id))->first();
        $ubi = ubicacion::where('id',$aire->ubicacion_id)->first();
        $central = centrales::where('id',$ubi->central_id)->first();
        $responsable = personal::where('id',$ubi->responsable_id)->first();
       
        $rule = AireA::$rule;
        /*
        $rule['aireA.serial']= $rule['aireA.serial'].','.$aire->id;
        $rule['responsable.cedula']= $rule['responsable.cedula'].','.$aire->ubicacion->responsable->id;
        $rule['responsable.cod_p00']= $rule['responsable.cod_p00'].','.$aire->ubicacion->responsable->id;
        //$rule['responsable.correo']= $rule['responsable.correo'].','.$aire->ubicacion->responsable->id;
*/
        Validator::make($request->all(),$rule,$this->messages)->validate();

        $aire->fill($request['aireA']);
        $ubi->fill($request['ubicacion']);
        $central->fill($request['central']);
        $responsable->fill($request['responsable']);
        if ($aire->save() && $ubi->save() && $central->save() && $responsable->save() ) {
            Session::flash('actualizado',true);
             if ($request->parametro != null) {
               return redirect('/nodos/'.$request->parametro);
           
            }
            return redirect('/aire_acondicionado'); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         aireA::find($id)->delete();
    }
}
