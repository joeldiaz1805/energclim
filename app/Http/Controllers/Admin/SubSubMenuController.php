<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Models\Menu;
use App\Models\Models\menurol;
use App\Models\Models\SubMenu;
use App\Models\Models\SubSubMenu;
use App\Models\Models\roles;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Models\Multimedia;
use Illuminate\Support\Str;
use JsValidator;
use Session;
use Auth;

class SubSubMenuController extends Controller
{
   
    public function index()
    {
       $sub_sub = SubSubMenu::get();
       return view('sub_submenu.index',compact('sub_sub'));

    }

    
    public function create()
    {
       $sub_menu = SubMenu::get();
       return view('sub_submenu.create',compact('sub_menu'));
    }

    
    public function store(Request $request)
    {
        $request['rol_id']=1;
        $request['observacion']='-';
        SubSubMenu::create($request->all());
        Session::flash('success','Modulo Guardado con exito');
        return redirect('/sub_submenu');
    }

    
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $submenu = SubSubMenu::find(decrypt($id));
       $sub_sub = SubMenu::get();
       return view('sub_submenu.edit',compact('submenu','sub_sub'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sub = SubSubMenu::find(decrypt($id));
        $sub->fill($request->all());
        if ($sub->save()) {
            Session::flash('success','Modulo actualizado con èxito');
            return redirect('/sub_submenu');
            # code...
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
