<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Models\regiones;
use App\Models\Models\localidades;
use App\Models\Models\estados;
use App\Models\Models\personal;
use App\Models\Models\ubicacion;
use App\Models\Models\centrales;
use App\Models\Models\rectificadores;
use App\Models\Models\rect_bcobb;
use App\Models\Models\bancob;
use App\Models\Models\marcas;
use App\Models\Models\salas;
use App\Models\Models\pisos;


use Illuminate\Support\Facades\Validator;

use JsValidator;
use Session;
use Auth;

class BcoBateriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $messages =[
            'required' =>"Campo requerido",
            'min' => "Minimo :min digitos",
            'max' => "Maximo :max digitos",
            'email' => 'Formato email invalido',
            'unique'=> 'Campo ya registrado',
            'numeric' => 'Solo valores numèricos',
            'required_if' => 'campo requerido si la opción "Falla" esta seleccionada en Operatividad'
        ];
    public function index()
    {

        if (Auth::user()->rol_id == 4) {
            
            $bancob = bancob::whereHas('ubicacion',function($query2){
                $query2->whereHas('central',function($query3){
                    return $query3->where('region_id',Auth()->user()->region_id);
                });
            })->get();
             
        }else{
            
            $bancob = bancob::get();

        }
        return view('bancob.index',compact('bancob'));
    }

    
    public function create(Request $request)
    {
        $parametros = ($request->request->all());
        $regiones= regiones::orderBy('nombre','ASC')->get();
        $estados= estados::orderBy('nombre','ASC')->get();
        $localidades= localidades::orderBy('nombre','ASC')->get();
        $marcas = marcas::whereequipo_id(9)->orderBy('descripcion','ASC')->get();
        $salas = salas::get();
        $pisos = pisos::get();
        $data['validator'] = JsValidator::make(bancob::$rule,$this->messages);
        return view('bancob.create',compact('marcas','regiones','estados','localidades','salas','pisos','parametros'),$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(),bancob::$rule,$this->messages)->validate();
       

        $centro =$request['ubicacion'];
        /*
        $verifyPers = personal::where('cod_p00',$request['responsable']['cod_p00'])->first();
        $verifySuper = personal::where('cod_p00',$request['supervisor']['cod_p00'])->first();
        if($verifyPers){
            $centro['responsable_id'] =$verifyPers->id;
        }else{
            $pers = personal::create($request['responsable']);
            $centro['responsable_id'] =$pers->id;
        }

        if($verifySuper){
            $centro['supervisor_id'] =$verifySuper->id;
        }else{
            $superv = personal::create($request['supervisor']);
            $centro['supervisor_id'] =$superv->id;
        }*/
        $centrar = centrales::create($request['central']);

        $centro['central_id'] =$centrar->id;
        $ubi = ubicacion::create($centro);
        $bco_baterias = $request['bco_baterias'];

        
        $bco_baterias['ubicacion_id'] = $ubi->id;
        $bco_baterias['tipo_id'] =2;

        bancob::create($bco_baterias);
        Session::flash('exito',true);
        if ($request->parametro != null) {
            return redirect('/nodos/'.$request->parametro);
            # code...
        }
        return redirect('/bancob');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $parametros = ($request->request->all());
       $bancob = bancob::where('id',decrypt($id))->first();
       $rule = bancob::$rule;
        $regiones= regiones::orderBy('nombre','ASC')->get();
        $estados= estados::orderBy('nombre','ASC')->get();
        $localidades= localidades::orderBy('nombre','ASC')->get();
        $marcas = marcas::whereequipo_id(9)->orderBy('descripcion','ASC')->get();
        $salas = salas::get();
        $pisos = pisos::get();
        $data['validator'] = JsValidator::make($rule,$this->messages);
        return view('bancob.edit',compact('marcas','regiones','estados','localidades','bancob','salas','pisos','parametros'),$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $bancob = bancob::where('id',decrypt($id))->first();
        $ubi = ubicacion::where('id',$bancob->ubicacion_id)->first();
        $central = centrales::where('id',$ubi->central_id)->first();
      
        $rules = bancob::$rule;
        
       // $rules['responsable.cedula'] =$rules['responsable.cedula'].','.$responsable->id;
       //$rules['responsable.cod_p00'] =$rules['responsable.cod_p00'].','.$responsable->id;
        Validator::make($request->all(),$rules,$this->messages)->validate();
        $bancob->fill($request['bco_baterias']);
        $central->fill($request['central']);
        $ubi->fill($request['ubicacion']);

        //$responsable->fill($request['responsable']);
        //$supervisor->fill($request['supervisor']);
        if($bancob->save() && $ubi->save() && $central->save()) {

            Session::flash('actualizado',true);
              if ($request->parametro != null) {
               return redirect('/nodos/'.$request->parametro);
           
            }
            return redirect('/bancob'); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        bancob::find($id)->delete();
    }
}
