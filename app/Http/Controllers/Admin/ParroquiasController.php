<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Models\Menu;
use App\Models\Models\menurol;
use App\Models\Models\roles;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Models\Multimedia;
use App\Models\Models\Municipio;
use App\Models\Models\Parroquia;
use App\Models\Models\estados;
use Illuminate\Support\Str;
use JsValidator;
use Session;
use Auth;
class ParroquiasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $parroquias = Parroquia::get();
        return view('parroquias.index',compact('parroquias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $municipios = Municipio::get();
        $estados = estados::get();
        return view('parroquias.create',compact('municipios','estados'));
    }

    public function municipios($id){

        return Municipio::where('estado_id',$id)->get();
    }
     public function parroquias($id){

        return Parroquia::where('municipio_id',$id)->get();
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         foreach ($request->descripcion as $key => $value) {
            Parroquia::create(['descripcion'=>$value,'municipio_id'=>$request->municipio_id,'user_id'=>Auth::user()->id]);
       }
        Session::flash('sucess','Municipio Registrado Con Exito');
            return redirect('/parroquias');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $municipios = Municipio::get();
        $parroquia = Parroquia::find(decrypt($id));
        $estados = estados::get();
        return view('parroquias.edit',compact('parroquia','municipios','estados'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $parroquia = Parroquia::find(decrypt($id));

       $parroquia->fill($request->all());

       if ($parroquia->save()) {

            Session::flash('sucess','Parroquia Actualizada con Exito');
            return redirect('/parroquias');
           // code...
       }

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
