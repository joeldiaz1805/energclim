<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Models\facturas;
use App\Models\Models\contratos;
use App\Models\Models\proveedores;
use App\Models\Models\personal;
use App\Models\Models\regiones;
use App\Models\Models\estados;
use App\Models\Models\localidades;
use App\Models\Models\equipo;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Models\Multimedia;
use Illuminate\Support\Str;
use JsValidator;
use Session;
use Auth;
class FacturasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     protected $messages =[
                'required' =>"Campo requerido",
                'min' => "Minimo :min digitos",
                'max' => "Maximo :max digitos",
                'email' => 'Formato email invalido',
                'unique'=> 'Campo ya registrado',
                'required_if' => 'campo requerido si la opción "Falla" esta seleccionada en Operatividad',
                'numeric' => 'Solo valores Numericos'
            ];
    public function index()
    {
       $facturas = facturas::get();
       return view('facturas.index',compact('facturas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $contratos = contratos::get();
        $regiones = regiones::get();
        $equipos = equipo::get();
        $data['validator'] = JsValidator::make(facturas::$rule,$this->messages);

        return view('facturas.create',compact('contratos','regiones','equipos'),$data);
    }

    
    public function store(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(),facturas::$rule,$this->messages)->validate();
        if (facturas::create($request->all())) {
            Session::flash('exito',true);
            return redirect('/facturas');
        }
    }

    
    public function show($id)
        
    {
        return facturas::where('contrato_id',2)->get()->sum('monto_factura');
    }

    public function consolidado(){
        $proveedores = proveedores::get();
        return view('facturas.consolidad',compact('proveedores'));
    }

  
    public function edit($id)
    {
        $contratos = contratos::get();
        $regiones = regiones::get();
        $estados = estados::get();
        $localidades = localidades::get();
        $equipos = equipo::get();
        $factura = facturas::find(decrypt($id));
        $data['validator'] = JsValidator::make(facturas::$rule,$this->messages);
        $sumatoria = $this->suma($factura->contrato_id);
        return view('facturas.edit',compact('sumatoria','contratos','regiones','equipos','factura','estados','localidades'),$data);

    }

    public function update(Request $request, $id)
    {
       
        //dd($request->all());
        Validator::make($request->all(),facturas::$rule,$this->messages)->validate();
        $facturas = facturas::find(decrypt($id));
        $facturas->fill($request->all());

        if ($facturas->save()) {
            Session::flash('actualizado',true);
            return redirect('/facturas');
            # code...
        }
    }

    public function suma($id){
        
        $fact =contratos::where('id',$id)->first();
        return $fact->monto_asignado - facturas::where('contrato_id',$id)->get()->sum('monto_factura');
    }

    public function buscar_proveedor($id){
    
       
       $prove = proveedores::with('contratos')->where('cod_proveedor',$id)->first();
       if ($prove) {
           return $prove;
       }else{
        return false;
       }
    
    }

   

    
    public function destroy($id)
    {
        facturas::find(decrypt($id))->delete();
    }
}
