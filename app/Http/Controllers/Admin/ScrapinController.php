<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use DOMDocument;

class ScrapinController extends Controller
{
    

	public function inic(){
		return view('scraping.consulta');
	}

	public function scraping(Request $request){
			//dd($request->all());

		 $url= 'http://bossprov.cantv.net/cgi-bin/adsl/all_getoperinfo_tlf_w.cgi?codarea='.$request->area.'&nume='.$request->numero;

        $html= Http::get($url);

		//$context = stream_context_create(array('http' => array('timeout' => 5)));

		//$html = file_get_contents($url); //Convierte la información de la URL en cadena
		$documento = new DOMDocument();
		libxml_use_internal_errors(TRUE);
		$documento->loadHTML($html);
		$td = $documento->getElementsByTagName('td');
		$th = $documento->getElementsByTagName('th');
		$body = $documento->getElementsByTagName('body');

		$pr = explode(',', $body[0]->nodeValue);
		$pd = [];
		foreach ($pr as $key) {
			$pd[]=explode(':', $key);
		}
		$area =$request->area;
		$numero = $request->numero;
		return view('scraping.result',compact('td','th','body','pr','pd','area','numero'));
	}

}
