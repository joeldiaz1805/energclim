<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Models\marcas;
use App\Models\Models\salas;
use App\Models\Models\pisos;
use App\Models\Models\personal;
use App\Models\Models\tabla_actividad;
use App\Models\Models\planilla_bb;
use App\Models\Models\regiones;
use App\Models\Models\celdas_bb;
use App\Models\Models\Cuestionario;

use JsValidator;
use Session;
use Auth;

class TablaActividadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   $tactividad = tabla_actividad::with('planilla_bb')->get();
        //return $tactividad;
        return view('TablaActividad.index',compact('tactividad'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $marcas = marcas::whereequipo_id(9)->orderBy('descripcion','ASC')->get();
        $salas = salas::get();
        $pisos = pisos::get();
        $personal = personal::orderBy('nombre','ASC')->get();
        $regiones = regiones::get();
        return view('TablaActividad.create',compact('marcas','salas','pisos','personal','regiones'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // dd($request->all());
        $acti = tabla_actividad::create($request->all());

        if ($acti) {
           $bbco1 =$request->bco_baterias;
           $bbco1['datos_actividad_id'] = $acti->id;
            $plaBb = planilla_bb::create($bbco1);

            if ($request->bco_bateria2 && sizeof($request->bco_bateria2) != 0) {

                $bbco2 =$request->bco_bateria2;
                $bbco2['datos_actividad_id'] = $acti->id;

                 $plaBb2 = planilla_bb::create($bbco2);
                // code...
            }
        }
        Session::flash('exito',true);
        return redirect('/tablero_actividad');
        //dd($request->all());
        //
    }

    
    public function create_celdas($id){
        $tablero_id = decrypt($id);
        //dd(tabla_actividad::find(decrypt($id)));
        //return tabla_actividad::with('planilla_bb')->where('id',decrypt($id))->first();
        return view('TablaActividad.celdas',compact('tablero_id'));
    }
 
    public function celdas_post(Request $request){
       //dd($request->all());
        $variable=[];
        $variable2=[];

        foreach ($request->bco_baterias as $key => $value) {
           foreach ($value as $key2 => $value2) {
                if ($value2 =! null) {
                      $variable[$key2] = $value[$key2];
                }

           }
           if (!empty($variable)) {
                $variable['planilla_bb_id']= $request->planilla_bb_id;
               celdas_bb::create($variable);
           }
        }

        foreach ($request->bco_baterias2 as $key => $value) {
           foreach ($value as $key2 => $value2) {
                if (!empty($value2)) {
                      $variable2[$key2] = $value[$key2];
                }

           }
            if (!empty($variable1)) {
                $variable2['planilla_bb_id']= $request->planilla_bb_id;
               celdas_bb::create($variable2);

           }
        }
        return redirect('/tablero_actividad');
    }

    public function show($id)
    {
        //
    }

     public function masiva()
    {
       $tactividad = tabla_actividad::with('planilla_bb')->get();
        //return $tactividad;
        return view('planilla_masiva.index',compact('tactividad'));
    }
      public function planillas_ups()
    {
       $tactividad = tabla_actividad::with('planilla_bb')->get();
        //return $tactividad;
        return view('Planilla_Ups.index',compact('tactividad'));
    }
      public function planillas_ups_create()
    {   $cuestionario = Cuestionario::get();
       
        $marcas = marcas::whereequipo_id(7)->orderBy('descripcion','ASC')->get();
        $salas = salas::get();
        $pisos = pisos::get();
        $personal = personal::orderBy('nombre','ASC')->get();
        $regiones = regiones::get();
        return view('Planilla_Ups.create',compact('cuestionario','marcas','salas','pisos','personal','regiones'));

    }
     public function planillas_masiva_create()
    {
        $marcas = marcas::whereequipo_id(7)->orderBy('descripcion','ASC')->get();
        $salas = salas::get();
        $pisos = pisos::get();
        $personal = personal::orderBy('nombre','ASC')->get();
        $regiones = regiones::get();
        return view('planilla_masiva.create',compact('marcas','salas','pisos','personal','regiones'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
