<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Models\AireA;
use App\Models\Models\regiones;
use App\Models\Models\localidades;
use App\Models\Models\estados;
use App\Models\Models\personal;
use App\Models\Models\ubicacion;
use App\Models\Models\centrales;
use App\Models\Models\climatizacion;
use Illuminate\Support\Facades\Validator;
use App\Models\Models\acciones;
use App\Models\Models\salas;
use App\Models\Models\pisos;
use JsValidator;
use Session;
use Auth;

class ClimatizacionController extends Controller
{
    /**
     * Display a listing of the resource.   
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clima = climatizacion::where('region_id',Auth::user()->region_id)->get();
        if(Auth::user()->rol_id == 1){
             $clima = climatizacion::get();

        }
        return view('climatizacion.index',compact('clima'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $salas = salas::get();
        $pisos = pisos::get();
        $regiones= regiones::orderBy('nombre','ASC')->get();
        $estados= estados::orderBy('nombre','ASC')->get();
        
        
        if (Auth::user()->region_id) {
            $estados = estados::where('region_id',Auth::user()->region_id)->get();
        }
        $data['validator'] = JsValidator::make(climatizacion::$rule,$this->messages);
        return view('climatizacion.create',compact('regiones','pisos','salas','estados'),$data);
    }

    protected $messages =[
            'required' =>"Campo requerido",
            'min' => "Minimo :min digitos",
            'max' => "Maximo :max digitos",
            'email' => 'Formato email invalido',
            'unique'=> 'Campo ya registrado',
            'required_if' => 'campo requerido si la opción "Falla" esta seleccionada en Operatividad',
            'between' => "Valores entre 0°C - 50°C"
        ];


    public function store(Request $request)
    {
        Validator::make($request->all(),climatizacion::$rule,$this->messages)->validate();
       // dd($request->all());
        if (climatizacion::create($request->all())) {
           Session::flash('exito',true);
           return redirect('/climatizacion');
        }
    }

    
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $clima = climatizacion::where('id',decrypt($id))->first();
        $salas = salas::get();
        $pisos = pisos::get();
        $regiones= regiones::orderBy('nombre','ASC')->get();
        $estados= estados::orderBy('nombre','ASC')->get();
        $localidades= localidades::orderBy('nombre','ASC')->get();
        
        return view('climatizacion.edit',compact('clima','salas','pisos','regiones','estados','localidades'));
    }

    
    public function update(Request $request, $id)
    {
        //var_dump($id);
        $clima = climatizacion::where('id',decrypt($id))->first();
        $clima->fill($request->all());
        if ($clima->save()) {
            Session::flash('actualizado',true);
            return redirect('/climatizacion');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        climatizacion::find($id)->delete();
    }
}
