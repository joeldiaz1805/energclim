<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
 
use App\Models\Models\personal;
use App\Models\Models\regiones;
use App\Models\Models\restados;
use App\Models\Models\detalleopsutpivot;
use App\Models\Models\requeopsut;
use App\Models\Models\nodoopsut;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Models\Multimedia;
use Illuminate\Support\Str;
use JsValidator;
use Session;
use Auth;




class DetalleOpsutController extends Controller
{
     public function __construct(){
        $this->middleware('auth');
    }
     protected $messages =[
            'required' =>"Campo Requerido",
            'min' => "Minimo :min digitos",
            'max' => "Maximo :max digitos",
            'email' => 'Formato de Correo Invalido',
            'same' => 'No coincide con Password',
            'unique' => 'Ya Esta Registrado, intente con otro'
            ];
    public function index()
    {
      
        $nodoopsut = nodoopsut::get();
        $motogeneradorPivot= motogeneradorPivot::get();
        $rect_bcobb = rect_bcobb::get();
        $AireA = AireA::get();
       // return view('opsut.detnodo',compact('nodoopsut','motogeneradorPivot','rect_bcobb','AireA'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
  $nodoopsut = nodoopsut::get();
        $motogeneradorPivot= motogeneradorPivot::get();
        $rect_bcobb = rect_bcobb::get();
        $AireA = AireA::get();
return view('invopsut.create',compact('nodoopsut','motogeneradorPivot','rect_bcobb'));




    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $equipo = detalleopsutpivot::where('id',$id)->first();
        return view('opsut.edit',compact('equipo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
