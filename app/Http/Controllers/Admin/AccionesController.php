<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Models\usuarios;
use App\Models\Models\regiones;
use App\Models\Models\localidades;
use App\Models\Models\estados;
use App\Models\Models\equipo;
use App\Models\Models\personal;
use App\Models\Models\ubicacion;
use App\Models\Models\centrales;
use App\Models\Models\componente;
use App\Models\Models\mantenimiento;
use App\Models\Models\motogeneradorPivot;
use App\Models\Models\fallas;
use App\Models\Models\acciones;
use App\Models\Models\salas;
use App\Models\Models\pisos;
use App\Models\Models\permisos;
use Auth;
use JsValidator;
use Session;
class AccionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     

         $acciones = acciones::orderBy('nombre','asc')->get();
        return view('acciones.index',compact('acciones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function create()
         {
             return view('acciones.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        foreach ($request->actividad as $key => $value) {
        //dd($request->all(),$key,$value);
            acciones::create(['nombre'=>$request->nombre,
                                'actividad'=>$value,
                                'tipo_fluido'=>$request->tipo_fluido
                            ]);
        }
            # code...
        Session::flash('exito',true);
        return redirect('/acciones');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $accion = acciones::find(decrypt($id));
        return view('acciones.edit',compact('accion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $accion = acciones::find(decrypt($id));
        $accion->fill($request->all());
        if ($accion->save()) {
            Session::flash('actualizado',true);
            return redirect('/acciones');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        acciones::find($id)->delete();
    }
}
