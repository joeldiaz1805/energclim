<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Models\ticket;
use App\Models\Models\ups_bancob;
use App\Models\Models\rect_bcobb;
use App\Models\Models\motogeneradorPivot;
use App\Models\Models\Menu;
use App\Models\Models\roles;
use App\Models\Models\AireA;
use App\Models\Models\bancob;
use App\Http\Controllers\Admin\MotorController;
use Illuminate\Support\Facades\Http;

use Auth;
use Session;

class HomeController extends Controller
{
    

     public function __construct(){
                $this->middleware('auth');
            }


    public function index()
    {  

       // $menu = roles::with('menus.submenu.SubSub')->find(Auth::user()->rol_id);
        //return response(Request->method());
        //$tickets = ticket::get();
        $prevCount = ticket::where('tipo_mantenimiento','Planificacion')->count();
        $corrCount = ticket::where('tipo_mantenimiento','Proyecto')->count();
        $insp = ticket::where('tipo_mantenimiento','Inspeccion')->count();
        $act = [
                'Enero'=>0,
                'Febrero'=>0,
                'Marzo'=>0,
                'Abril'=>0,
                'Mayo'=>0,
                'Junio'=>0,
                'Julio'=>0,
                'Agosto'=>0,
                'Septiembre'=>0,
                'Octubre'=>0,
                'Noviembre'=>0,
                'Diciembre'=>0];
        $arrres = [
                'Enero'=>0,
                'Febrero'=>0,
                'Marzo'=>0,
                'Abril'=>0,
                'Mayo'=>0,
                'Junio'=>0,
                'Julio'=>0,
                'Agosto'=>0,
                'Septiembre'=>0,
                'Octubre'=>0,
                'Noviembre'=>0,
                'Diciembre'=>0];

        $cont =0;
        $ah = Auth::check();
       

        foreach ($act as $key => $value) {
            $cont+=1;
 
            
            $act[$key] =ticket::wherestatus('En Proceso')->whereYear('created_at',date('Y'))->whereMonth('created_at',$cont)->get()->count();
            $actcount=ticket::wherestatus('En Proceso')->whereYear('created_at',date('Y'))->get()->count();
           }
        foreach ($arrres as $key => $value) {

            //$arr[$key] =ticket::whereMonth('created_at',$cont)->whereYear('created_at',date('Y'))->get()->count();


             $arrres[$key] =ticket::wherestatus('Resuelto')->whereYear('created_at',date('Y'))->whereMonth('created_at',$cont)->get()->count();
             $rescount=ticket::wherestatus('Resuelto')->whereYear('created_at',date('Y',$cont))->get()->count();
        }
       // dd($arr);

        if (Auth::user()->rol_id == 4) {
           
             $motoG = motogeneradorPivot::whereHas('motor', function ($query) {
                $query->whereHas('ubicacion',function($query2){
                    $query2->whereHas('central',function($query3){
                        return $query3->where('region_id',Auth()->user()->region_id);

                    });
                });
                 })->get()->count();
            $bancob = bancob::whereHas('ubicacion',function($query2){
                $query2->whereHas('central',function($query3){
                    return $query3->where('region_id',Auth()->user()->region_id);
                });
            })->get()->count();
           
            $cuadroF = rect_bcobb::whereHas('ubicacion',function($query2){
                $query2->whereHas('central',function($query3){
                    return $query3->where('region_id',Auth()->user()->region_id);
                });
            })->get()->count();

            $AireA =AireA::whereHas('ubicacion',function($query2){
                    $query2->whereHas('central',function($query3){
                        return $query3->where('region_id',Auth()->user()->region_id);

                    });
              
                 })->get()->count();
             $ups = ups_bancob::whereHas('ubicacion',function($query2){
                $query2->whereHas('central',function($query3){
                    return $query3->where('region_id',Auth()->user()->region_id);
                });
            })->get()->count();

            $mto = motogeneradorPivot::get();


        }else{
            $mto = motogeneradorPivot::get();
            $cuadroF = rect_bcobb::get()->count(); 
            $ups = ups_bancob::get()->count();
            $AireA =AireA::get()->count();
            $bancob =bancob::get()->count();

            $motoG = $mto->count();

        }
            
       

        $totalEquipos =  $AireA + $motoG + $cuadroF +$ups + $bancob;


        $resp= Http::get('http://sismyc.cantv.com.ve/tickets-ex/'.base64_encode(date("Y-m-01")).'/'.base64_encode(date("Y-m-d")).'?api_token=ZhzJ6FUy23kp18UJFxaAEIfP3psZz94q7ysuCeWzld1VzoKv5VKkKDGk6UIX');
        $res = json_encode($resp->json());
        $re  = json_decode($res);
        //return view('sismyc.resumen2',compact('re'));
        //dd($resp);
        $valores = [];
        $contdor =0;
        $contdor_afectacion =0;
        $contdor_aba =0;
        $contdor_voz =0;
       $result = array();
       $result2 = array();
       $afectaciones = array();
       $nuevo = array();
       foreach ($re as $key => $value) {
            if ($value->ticket_severity->nombre_severidad == "Disponibilidad de Servicio" && $value->ticket_status->nombre_estado =="Resuelto" ) {
              // dd($re[$key]);
               $nuevo[]=$re[$key];
            }
       }
          // dd($nuevo);
        foreach($nuevo as $t) {
            $repeat=false;
            for($i=0;$i<count($result);$i++)
            {
                if($result[$i]['falla']==$t->ticket_failure->nombre_falla)
                {
                    $result[$i]['items']+=1;
                    $repeat=true;
                    break;
                }
            }
            if($repeat==false)
                $result[] = array('falla' => $t->ticket_failure->nombre_falla, 'items' => 1,
                    'region'=> $t->network_elements[0]->location->state->region->Nombre_region,
                    'impacto_aba'=>$t->impacto_aba,
                    'impacto_voz'=>$t->impacto_voz

            );
        }
         foreach($nuevo as $t) {
            $repeat=false;
            for($i=0;$i<count($result2);$i++)
            {
                if($result2[$i]['falla']==$t->ticket_failure->nombre_falla && $result2[$i]['region']==$t->network_elements[0]->location->state->region->Nombre_region  )
                {
                    $result2[$i]['impacto_voz']+=$t->impacto_aba;
                    $result2[$i]['impacto_voz']+=$t->impacto_voz;
                    $repeat=true;
                    break;
                }
            }
            if($repeat==false)
                $result2[] = array('falla' => $t->ticket_failure->nombre_falla,
                    'region'=> $t->network_elements[0]->location->state->region->Nombre_region,
                    'impacto_aba'=>$t->impacto_aba,
                    'impacto_voz'=>$t->impacto_voz

            );
        }
        
        //dd($result2,'http://sismyc.cantv.com.ve/tickets-ex/'.base64_encode($desde).'/'.base64_encode($hasta).'?api_token=ZhzJ6FUy23kp18UJFxaAEIfP3psZz94q7ysuCeWzld1VzoKv5VKkKDGk6UIX');
        $var = json_encode($result);
        $var2 = json_encode($result2);
        foreach ($result as $key => $value) {
            $contdor += $value['items'] ;
            // code...
        }
         foreach ($result2 as $key => $value) {
            $contdor_afectacion += $value['impacto_voz'] + $value['impacto_aba'] ;
            $contdor_voz += $value['impacto_voz'];
            $contdor_aba +=  $value['impacto_aba'] ;
            // code...
        }

        $combu =0;
        $cant =0;
        $ltrsFaltantes = 0;
        foreach ($mto as $key) {
            
            if ($key->motor->operatividad == 'Operativo' && $key->motor->comb_tanq_princ >0 && $key->motor->cant_actual >0){
                $combu += $key->motor->comb_tanq_princ;
                try{

                $cant += $key->motor->cant_actual;
            }catch(Exception $e){
                echo $e;
            }
        }

        }
        $ltrsFaltantes = $combu - $cant;

        $porcentajeFaltante =( round(($cant*100) / $combu) );
        



        return view('layouts.home',compact('porcentajeFaltante','ltrsFaltantes','resp','var','var2','result','result2','contdor','contdor_afectacion','contdor_aba','contdor_voz','motoG','cuadroF','AireA','ups','ah','insp','corrCount','prevCount','totalEquipos','act','arrres','rescount','actcount','bancob'));
       
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
