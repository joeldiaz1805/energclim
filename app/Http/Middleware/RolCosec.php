<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class RolCosec
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
       if (Auth::check() && Auth::user()->rol_id != 6 ) {
            Session::flash('superUser',true);
            return redirect()->back()->withErrors('superUser');
        }else{
            return $next($request);
        }
    }
}
