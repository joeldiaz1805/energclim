<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;

use Illuminate\Http\Request;

class PwErr
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        //  if (Auth::check() && Auth::user()->int_fail == 3) {
        //     Auth::logout();
        //     Session::flash('PwErr',true);
        //     return redirect('/')->with('error_message', 'Demasiados intentos fallidos');

        //     //return redirect('/');
        // }
        return $next($request);
    }
}
