<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;
use Session;

class UsrAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    ////Super usuario tipo 1, 2, 3 y 4 
    //permite la visualizacion de rutas solo para el admin y el superusuario
    public function handle(Request $request, Closure $next)
    {
        if (Auth::check() && Auth::user()->rol_id == 4 || Auth::user()->rol_id == 3 ) {
            Session::flash('superUser',true);
            return redirect()->back()->withErrors('superUser');
        }else{
            return $next($request);
        }

    }
}
