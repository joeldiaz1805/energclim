<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

use Auth;
use Session;

class Administrador
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    // 1 Super Usuario  2 Administrador 3 Monitor
    public function handle(Request $request, Closure $next)
    {
        if (Auth::check() && Auth::user()->rol_id == 2 || Auth::user()->rol_id == 1) {
            return $next($request);
            # code...
        }else{
            Session::flash('superUser',true);
            return redirect()->back()->withErrors('superUser');
        }
    }
}
