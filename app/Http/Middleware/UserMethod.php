<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Models\usuarios;

use Auth;
use Session;

class UserMethod
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {


        if (Auth::check()) {
            # code...
            $per = usuarios::where('id',Auth::user()->id)->first();
            
            $lol=[];
            if ($per->permisos != null) {
                foreach ($per->permisos as $key => $value){
                   $lol[]= $value->permiso;
                    
                }
            }
            if($lol== null){
                //dd($lol);
                return $next($request);     

            }
            if(in_array($request->method(),$lol) ) {
                return $next($request);     
                   
            }else{
                   
                    Session::flash('permision',true);
                     return redirect()->back()->withErrors('No tienes permisos para ejecutar la acción ');
                }
                
         
        }else{
            return $next($request);
        }
        
    }
}
