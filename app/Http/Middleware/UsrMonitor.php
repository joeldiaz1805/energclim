<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;
use Session;

class UsrMonitor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    ///Super usuario tipo 1, 2, 3 y 4 
    //solo permite el acceso al monitor nivel 4
    public function handle(Request $request, Closure $next)
    {
        if (Auth::check() && Auth::user()->rol_id == 4) {
            return $next($request);
            # code...
        }else{
            Session::flash('superUser',true);
            return redirect()->back()->withErrors('superUser');
        }

    }
}
