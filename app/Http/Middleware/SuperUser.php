<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;
use Session;

class SuperUser
{
   
    public function handle(Request $request, Closure $next)
    {
        if (Auth::check() && Auth::user()->rol_id != 1) {
            
            Session::flash('superUser',true);
            return redirect()->back()->withErrors('superUser');
            # code...
        }else{
            return $next($request);
        }

    }
}
