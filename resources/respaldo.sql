--
-- PostgreSQL database dump
--

-- Dumped from database version 11.9 (Debian 11.9-0+deb10u1)
-- Dumped by pg_dump version 11.9 (Debian 11.9-0+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: adminpack; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION adminpack; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: acciones; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.acciones (
    id bigint NOT NULL,
    nombre character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.acciones OWNER TO postgres;

--
-- Name: acciones_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.acciones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.acciones_id_seq OWNER TO postgres;

--
-- Name: acciones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.acciones_id_seq OWNED BY public.acciones.id;


--
-- Name: aire_acondicionado; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.aire_acondicionado (
    id bigint NOT NULL,
    marca character varying(255) NOT NULL,
    modelo character varying(255) NOT NULL,
    serial character varying(255),
    inventario_cant character varying(255),
    tipo_id integer,
    nro_circuitos character varying(255),
    tipo_compresor character varying(255),
    tipo_refrigerante character varying(255),
    ton_refrig_operativa character varying(255),
    ton_refrig_faltante character varying(255),
    volt_operacion character varying(255),
    operatividad character varying(255),
    fecha_instalacion date,
    criticidad character varying(255),
    garantia character varying(255),
    observaciones character varying(255),
    ubicacion_id integer,
    tablero_control character varying(255),
    status_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    ton_refrig_instalada character varying,
    tipo_correa character varying,
    nro_correa integer,
    cant_correa integer,
    tipo_equipo integer
);


ALTER TABLE public.aire_acondicionado OWNER TO postgres;

--
-- Name: aire_acondicionado_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.aire_acondicionado_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.aire_acondicionado_id_seq OWNER TO postgres;

--
-- Name: aire_acondicionado_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.aire_acondicionado_id_seq OWNED BY public.aire_acondicionado.id;


--
-- Name: bateria_control; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.bateria_control (
    id bigint NOT NULL,
    motog_id integer,
    cantidad integer,
    amperaje character varying(255),
    voltaje character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.bateria_control OWNER TO postgres;

--
-- Name: bateria_control_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.bateria_control_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bateria_control_id_seq OWNER TO postgres;

--
-- Name: bateria_control_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.bateria_control_id_seq OWNED BY public.bateria_control.id;


--
-- Name: bco_baterias; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.bco_baterias (
    id bigint NOT NULL,
    marca character varying(255) NOT NULL,
    modelo character varying(255) NOT NULL,
    serial character varying(255),
    inventario_cant character varying(255),
    cap_kva character varying(255),
    carga_total_bb character varying(255),
    elem_oper_bb character varying(255),
    elem_inoper_bb character varying(255),
    amp_elem character varying(255),
    autonomia_respaldo character varying(255),
    tablero character varying(255),
    status_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.bco_baterias OWNER TO postgres;

--
-- Name: bco_baterias_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.bco_baterias_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bco_baterias_id_seq OWNER TO postgres;

--
-- Name: bco_baterias_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.bco_baterias_id_seq OWNED BY public.bco_baterias.id;


--
-- Name: centrales; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.centrales (
    id bigint NOT NULL,
    localidad_id integer NOT NULL,
    region_id integer NOT NULL,
    estado_id integer NOT NULL,
    area character varying(255),
    tipo character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.centrales OWNER TO postgres;

--
-- Name: centrales_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.centrales_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.centrales_id_seq OWNER TO postgres;

--
-- Name: centrales_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.centrales_id_seq OWNED BY public.centrales.id;


--
-- Name: comentarios; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.comentarios (
    id bigint NOT NULL,
    descripcion text NOT NULL,
    usuario_id integer NOT NULL,
    ticket_id integer NOT NULL,
    multimedia_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.comentarios OWNER TO postgres;

--
-- Name: comentarios_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.comentarios_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.comentarios_id_seq OWNER TO postgres;

--
-- Name: comentarios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.comentarios_id_seq OWNED BY public.comentarios.id;


--
-- Name: componente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.componente (
    id bigint NOT NULL,
    descripcion character varying(255) NOT NULL,
    equipo_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.componente OWNER TO postgres;

--
-- Name: componente_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.componente_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.componente_id_seq OWNER TO postgres;

--
-- Name: componente_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.componente_id_seq OWNED BY public.componente.id;


--
-- Name: contratista; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.contratista (
    id bigint NOT NULL,
    nombre character varying(255),
    cuadrilla_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.contratista OWNER TO postgres;

--
-- Name: contratista_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.contratista_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contratista_id_seq OWNER TO postgres;

--
-- Name: contratista_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.contratista_id_seq OWNED BY public.contratista.id;


--
-- Name: cuadrilla; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cuadrilla (
    id bigint NOT NULL,
    nombre character varying(255),
    contratista_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.cuadrilla OWNER TO postgres;

--
-- Name: cuadrilla_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cuadrilla_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cuadrilla_id_seq OWNER TO postgres;

--
-- Name: cuadrilla_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cuadrilla_id_seq OWNED BY public.cuadrilla.id;


--
-- Name: detallebypasopsut; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.detallebypasopsut (
    id bigint NOT NULL,
    nodoopsut_bypas_id integer NOT NULL,
    nodoopsut_bypas_codigo1 character varying(20) NOT NULL,
    nodoopsut_bypas_codigo2 character varying(20) NOT NULL,
    observacion character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.detallebypasopsut OWNER TO postgres;

--
-- Name: detallebypasopsut_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.detallebypasopsut_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.detallebypasopsut_id_seq OWNER TO postgres;

--
-- Name: detallebypasopsut_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.detallebypasopsut_id_seq OWNED BY public.detallebypasopsut.id;


--
-- Name: detalleopsutpivot; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.detalleopsutpivot (
    id bigint NOT NULL,
    nodoopsut_id integer NOT NULL,
    equipo_id integer,
    kva character varying(255),
    tr character varying(255),
    amph character varying(255),
    watts character varying(255),
    detalles_equipo character varying(255),
    serial character varying(40),
    status character varying(10),
    observacion text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.detalleopsutpivot OWNER TO postgres;

--
-- Name: detalleopsutpivot_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.detalleopsutpivot_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.detalleopsutpivot_id_seq OWNER TO postgres;

--
-- Name: detalleopsutpivot_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.detalleopsutpivot_id_seq OWNED BY public.detalleopsutpivot.id;


--
-- Name: equipo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.equipo (
    id bigint NOT NULL,
    descripcion character varying(255) NOT NULL,
    localidad_id integer NOT NULL,
    responsable_id integer NOT NULL,
    servicio_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.equipo OWNER TO postgres;

--
-- Name: equipo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.equipo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.equipo_id_seq OWNER TO postgres;

--
-- Name: equipo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.equipo_id_seq OWNED BY public.equipo.id;


--
-- Name: estados; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.estados (
    id bigint NOT NULL,
    nombre character varying(255) NOT NULL,
    region_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.estados OWNER TO postgres;

--
-- Name: estados_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.estados_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.estados_id_seq OWNER TO postgres;

--
-- Name: estados_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.estados_id_seq OWNED BY public.estados.id;


--
-- Name: failed_jobs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.failed_jobs (
    id bigint NOT NULL,
    uuid character varying(255) NOT NULL,
    connection text NOT NULL,
    queue text NOT NULL,
    payload text NOT NULL,
    exception text NOT NULL,
    failed_at timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.failed_jobs OWNER TO postgres;

--
-- Name: failed_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.failed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.failed_jobs_id_seq OWNER TO postgres;

--
-- Name: failed_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.failed_jobs_id_seq OWNED BY public.failed_jobs.id;


--
-- Name: fallas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fallas (
    id bigint NOT NULL,
    descripcion character varying(255) NOT NULL,
    tipo character varying(255) NOT NULL,
    componente_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.fallas OWNER TO postgres;

--
-- Name: fallas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.fallas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fallas_id_seq OWNER TO postgres;

--
-- Name: fallas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.fallas_id_seq OWNED BY public.fallas.id;


--
-- Name: generador; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.generador (
    id bigint NOT NULL,
    marca character varying(255),
    modelo character varying(255),
    serial character varying(255),
    inventario_cant character varying(255),
    cap_kva integer,
    tablero character varying(255),
    status_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.generador OWNER TO postgres;

--
-- Name: generador_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.generador_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.generador_id_seq OWNER TO postgres;

--
-- Name: generador_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.generador_id_seq OWNED BY public.generador.id;


--
-- Name: invequipopsut; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.invequipopsut (
    id bigint NOT NULL,
    id_2 integer NOT NULL,
    equipo_id integer NOT NULL,
    nombre character varying(90),
    descripcion character varying(120),
    marca character varying(30),
    modelo character varying(40),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.invequipopsut OWNER TO postgres;

--
-- Name: invequipopsut_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.invequipopsut_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.invequipopsut_id_seq OWNER TO postgres;

--
-- Name: invequipopsut_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.invequipopsut_id_seq OWNED BY public.invequipopsut.id;


--
-- Name: localidad; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.localidad (
    id bigint NOT NULL,
    nombre character varying(255) NOT NULL,
    estado_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    tipo character varying(255)
);


ALTER TABLE public.localidad OWNER TO postgres;

--
-- Name: localidad_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.localidad_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.localidad_id_seq OWNER TO postgres;

--
-- Name: localidad_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.localidad_id_seq OWNED BY public.localidad.id;


--
-- Name: mantenimiento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.mantenimiento (
    id bigint NOT NULL,
    descripcion character varying(255) NOT NULL,
    tipo integer NOT NULL,
    falla_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.mantenimiento OWNER TO postgres;

--
-- Name: mantenimiento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.mantenimiento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mantenimiento_id_seq OWNER TO postgres;

--
-- Name: mantenimiento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.mantenimiento_id_seq OWNED BY public.mantenimiento.id;


--
-- Name: menu; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.menu (
    id bigint NOT NULL,
    nombre character varying(30) NOT NULL,
    rol_id integer,
    url character varying(255),
    icon character varying(255),
    observacion text NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.menu OWNER TO postgres;

--
-- Name: menu_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.menu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.menu_id_seq OWNER TO postgres;

--
-- Name: menu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.menu_id_seq OWNED BY public.menu.id;


--
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- Name: motogenerador; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.motogenerador (
    id bigint NOT NULL,
    motor_id integer NOT NULL,
    generador_id integer NOT NULL,
    tipo_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.motogenerador OWNER TO postgres;

--
-- Name: motogenerador_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.motogenerador_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.motogenerador_id_seq OWNER TO postgres;

--
-- Name: motogenerador_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.motogenerador_id_seq OWNED BY public.motogenerador.id;


--
-- Name: motor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.motor (
    id bigint NOT NULL,
    marca character varying(255) NOT NULL,
    modelo character varying(255) NOT NULL,
    estado_arranque character varying(255),
    cap_tanq_aceite character varying(255),
    cap_tanq_refrig character varying(255),
    comb_tanq_princ character varying(255),
    cantidad_filtro character varying(255),
    modelo_filtro character varying(255),
    cant_bat_arranq character varying(255),
    carg_bat character varying(255),
    cant_baterias integer,
    fases character varying(255),
    amp_bateria character varying(255),
    status_id integer,
    ubicacion_id integer,
    inventario_cant character varying(255),
    serial character varying(255),
    operatividad character varying(255),
    fecha_instalacion character varying(255),
    criticidad character varying(255),
    garantia character varying(255),
    observaciones character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.motor OWNER TO postgres;

--
-- Name: motor_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.motor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.motor_id_seq OWNER TO postgres;

--
-- Name: motor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.motor_id_seq OWNED BY public.motor.id;


--
-- Name: multimedia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.multimedia (
    id bigint NOT NULL,
    archivo character varying(255),
    tipo character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.multimedia OWNER TO postgres;

--
-- Name: multimedia_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.multimedia_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.multimedia_id_seq OWNER TO postgres;

--
-- Name: multimedia_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.multimedia_id_seq OWNED BY public.multimedia.id;


--
-- Name: nodoopsut; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.nodoopsut (
    id bigint NOT NULL,
    nombre character varying(30) NOT NULL,
    codigo character varying(20),
    tipo integer NOT NULL,
    ubicacion text NOT NULL,
    latitud character varying(15),
    longitud character varying(15),
    stat_nod character varying(20),
    estado_id integer NOT NULL,
    region_id integer NOT NULL,
    personal_id integer NOT NULL,
    observacion text NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.nodoopsut OWNER TO postgres;

--
-- Name: nodoopsut_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.nodoopsut_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nodoopsut_id_seq OWNER TO postgres;

--
-- Name: nodoopsut_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.nodoopsut_id_seq OWNED BY public.nodoopsut.id;


--
-- Name: password_resets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE public.password_resets OWNER TO postgres;

--
-- Name: personal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.personal (
    id bigint NOT NULL,
    nombre character varying(50),
    apellido character varying(50),
    cedula character varying(13),
    cod_p00 character varying(21),
    region_id integer,
    num_oficina character varying(17),
    num_personal character varying(17),
    correo character varying(50),
    cargo character varying(50),
    status integer,
    "FotoPersonal" character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    estado_id integer
);


ALTER TABLE public.personal OWNER TO postgres;

--
-- Name: personal_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.personal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.personal_id_seq OWNER TO postgres;

--
-- Name: personal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.personal_id_seq OWNED BY public.personal.id;


--
-- Name: pisos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pisos (
    id bigint NOT NULL,
    nombre character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.pisos OWNER TO postgres;

--
-- Name: pisos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pisos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pisos_id_seq OWNER TO postgres;

--
-- Name: pisos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pisos_id_seq OWNED BY public.pisos.id;


--
-- Name: plantillas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.plantillas (
    id bigint NOT NULL,
    formato_id character varying(255),
    descripcion character varying(255),
    user_id character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.plantillas OWNER TO postgres;

--
-- Name: plantillas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.plantillas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.plantillas_id_seq OWNER TO postgres;

--
-- Name: plantillas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.plantillas_id_seq OWNED BY public.plantillas.id;


--
-- Name: rect_bcobb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rect_bcobb (
    id bigint NOT NULL,
    rect_id integer NOT NULL,
    bancob_id integer NOT NULL,
    ubicacion_id integer,
    tipo_id integer NOT NULL,
    operatividad character varying(255),
    fecha_instalacion character varying(255),
    criticidad character varying(255),
    garantia character varying(255),
    observaciones character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.rect_bcobb OWNER TO postgres;

--
-- Name: rect_bcobb_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rect_bcobb_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rect_bcobb_id_seq OWNER TO postgres;

--
-- Name: rect_bcobb_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rect_bcobb_id_seq OWNED BY public.rect_bcobb.id;


--
-- Name: rectificadores; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rectificadores (
    id bigint NOT NULL,
    marca character varying(255) NOT NULL,
    modelo character varying(255) NOT NULL,
    serial character varying(255),
    inventario_cant character varying(255),
    num_fases character varying(255),
    cant_oper character varying(255),
    cant_inoper character varying(255),
    cant_total character varying(255),
    consumo_amp character varying(255),
    carga_total_amp character varying(255),
    volt_operacion character varying(255),
    tablero character varying(255),
    status_id integer,
    ubicacion_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.rectificadores OWNER TO postgres;

--
-- Name: rectificadores_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rectificadores_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rectificadores_id_seq OWNER TO postgres;

--
-- Name: rectificadores_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rectificadores_id_seq OWNED BY public.rectificadores.id;


--
-- Name: regiones; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.regiones (
    id bigint NOT NULL,
    nombre character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.regiones OWNER TO postgres;

--
-- Name: regiones_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.regiones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.regiones_id_seq OWNER TO postgres;

--
-- Name: regiones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.regiones_id_seq OWNED BY public.regiones.id;


--
-- Name: registro; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.registro (
    id bigint NOT NULL,
    tipo_actividad character varying(255),
    tipo_servicio character varying(255),
    equipo_id integer,
    componente_id integer,
    ubicacion_id integer,
    sala character varying(255),
    piso character varying(255),
    estatus character varying(255),
    accion character varying(255),
    trabajo character varying(255),
    porcentaje character varying(255),
    cuadrilla_id integer,
    fecha_inicio date,
    fecha_fin date,
    ticket_cosec character varying(255),
    name_informe character varying(255),
    observaciones character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.registro OWNER TO postgres;

--
-- Name: registro_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.registro_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.registro_id_seq OWNER TO postgres;

--
-- Name: registro_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.registro_id_seq OWNED BY public.registro.id;


--
-- Name: requeopsut; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.requeopsut (
    id bigint NOT NULL,
    cod_inv_equipo_id character varying(20),
    requerimiento character varying(255),
    stat_req_nod character varying(20),
    observacion text NOT NULL,
    nodoopsut_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.requeopsut OWNER TO postgres;

--
-- Name: requeopsut_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.requeopsut_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.requeopsut_id_seq OWNER TO postgres;

--
-- Name: requeopsut_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.requeopsut_id_seq OWNED BY public.requeopsut.id;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.roles (
    id bigint NOT NULL,
    denominacion character varying(255) NOT NULL,
    descripcion character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.roles OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- Name: salas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.salas (
    id bigint NOT NULL,
    nombre character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.salas OWNER TO postgres;

--
-- Name: salas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.salas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.salas_id_seq OWNER TO postgres;

--
-- Name: salas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.salas_id_seq OWNED BY public.salas.id;


--
-- Name: status; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.status (
    id bigint NOT NULL,
    status character varying(255) NOT NULL,
    ticket_id integer NOT NULL,
    fecha_instalacion date,
    observaciones text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.status OWNER TO postgres;

--
-- Name: status_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.status_id_seq OWNER TO postgres;

--
-- Name: status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.status_id_seq OWNED BY public.status.id;


--
-- Name: sub_menu; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sub_menu (
    id bigint NOT NULL,
    nombre character varying(30) NOT NULL,
    rol_id integer NOT NULL,
    menu_id integer NOT NULL,
    url character varying(255),
    icon character varying(255),
    observacion text NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.sub_menu OWNER TO postgres;

--
-- Name: sub_menu_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sub_menu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sub_menu_id_seq OWNER TO postgres;

--
-- Name: sub_menu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sub_menu_id_seq OWNED BY public.sub_menu.id;


--
-- Name: sub_sub_menu; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sub_sub_menu (
    id bigint NOT NULL,
    nombre character varying(30) NOT NULL,
    rol_id integer NOT NULL,
    sub_menu_id integer NOT NULL,
    url character varying(255),
    icon character varying(255),
    observacion text NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.sub_sub_menu OWNER TO postgres;

--
-- Name: sub_sub_menu_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sub_sub_menu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sub_sub_menu_id_seq OWNER TO postgres;

--
-- Name: sub_sub_menu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sub_sub_menu_id_seq OWNED BY public.sub_sub_menu.id;


--
-- Name: tickets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tickets (
    id bigint NOT NULL,
    numero character varying(255) NOT NULL,
    equipo_id integer,
    componente_id integer,
    responsable_id integer,
    usuario_id integer,
    central_id integer,
    tipo_falla integer,
    multimedia_id integer,
    tipo_servicio character varying(255),
    tipo_mantenimiento character varying(255),
    tipo_actividad character varying(255),
    telefono character varying(255),
    sala character varying(255),
    piso character varying(255),
    cargo character varying(255),
    correo character varying(255),
    correo_supervisor character varying(255),
    status character varying(255),
    fecha_inicio date,
    fecha_fin_falla date,
    hora_inicio character varying(255),
    hora_fin_actividad character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.tickets OWNER TO postgres;

--
-- Name: tickets_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tickets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tickets_id_seq OWNER TO postgres;

--
-- Name: tickets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tickets_id_seq OWNED BY public.tickets.id;


--
-- Name: ubicacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ubicacion (
    id bigint NOT NULL,
    central_id integer NOT NULL,
    piso character varying(255),
    estructura character varying(255),
    criticidad_esp character varying(255),
    responsable_id integer,
    supervisor_id integer,
    sala character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.ubicacion OWNER TO postgres;

--
-- Name: ubicacion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ubicacion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ubicacion_id_seq OWNER TO postgres;

--
-- Name: ubicacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ubicacion_id_seq OWNED BY public.ubicacion.id;


--
-- Name: ups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ups (
    id bigint NOT NULL,
    marca character varying(255) NOT NULL,
    modelo character varying(255) NOT NULL,
    serial character varying(255),
    inventario_cant character varying(255),
    cap_kva character varying(255),
    carga_conectada character varying(255),
    tension_salida character varying(255),
    status_id integer,
    ubicacion_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.ups OWNER TO postgres;

--
-- Name: ups_bb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ups_bb (
    id bigint NOT NULL,
    ups_id integer NOT NULL,
    bancob_id integer NOT NULL,
    ubicacion_id integer,
    tipo_id integer NOT NULL,
    operatividad character varying(255),
    fecha_instalacion character varying(255),
    criticidad character varying(255),
    observaciones character varying(255),
    garantia character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.ups_bb OWNER TO postgres;

--
-- Name: ups_bb_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ups_bb_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ups_bb_id_seq OWNER TO postgres;

--
-- Name: ups_bb_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ups_bb_id_seq OWNED BY public.ups_bb.id;


--
-- Name: ups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ups_id_seq OWNER TO postgres;

--
-- Name: ups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ups_id_seq OWNED BY public.ups.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    last_name character varying(255),
    email character varying(255) NOT NULL,
    cedula character varying(255),
    departamento character varying(255),
    cargo character varying(255),
    email_verified_at timestamp(0) without time zone,
    rol_id integer,
    active integer,
    int_fail integer,
    password character varying(255),
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    region_id integer
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: acciones id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.acciones ALTER COLUMN id SET DEFAULT nextval('public.acciones_id_seq'::regclass);


--
-- Name: aire_acondicionado id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aire_acondicionado ALTER COLUMN id SET DEFAULT nextval('public.aire_acondicionado_id_seq'::regclass);


--
-- Name: bateria_control id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bateria_control ALTER COLUMN id SET DEFAULT nextval('public.bateria_control_id_seq'::regclass);


--
-- Name: bco_baterias id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bco_baterias ALTER COLUMN id SET DEFAULT nextval('public.bco_baterias_id_seq'::regclass);


--
-- Name: centrales id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.centrales ALTER COLUMN id SET DEFAULT nextval('public.centrales_id_seq'::regclass);


--
-- Name: comentarios id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comentarios ALTER COLUMN id SET DEFAULT nextval('public.comentarios_id_seq'::regclass);


--
-- Name: componente id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.componente ALTER COLUMN id SET DEFAULT nextval('public.componente_id_seq'::regclass);


--
-- Name: contratista id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contratista ALTER COLUMN id SET DEFAULT nextval('public.contratista_id_seq'::regclass);


--
-- Name: cuadrilla id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cuadrilla ALTER COLUMN id SET DEFAULT nextval('public.cuadrilla_id_seq'::regclass);


--
-- Name: detallebypasopsut id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detallebypasopsut ALTER COLUMN id SET DEFAULT nextval('public.detallebypasopsut_id_seq'::regclass);


--
-- Name: detalleopsutpivot id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalleopsutpivot ALTER COLUMN id SET DEFAULT nextval('public.detalleopsutpivot_id_seq'::regclass);


--
-- Name: equipo id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.equipo ALTER COLUMN id SET DEFAULT nextval('public.equipo_id_seq'::regclass);


--
-- Name: estados id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estados ALTER COLUMN id SET DEFAULT nextval('public.estados_id_seq'::regclass);


--
-- Name: failed_jobs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.failed_jobs ALTER COLUMN id SET DEFAULT nextval('public.failed_jobs_id_seq'::regclass);


--
-- Name: fallas id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fallas ALTER COLUMN id SET DEFAULT nextval('public.fallas_id_seq'::regclass);


--
-- Name: generador id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.generador ALTER COLUMN id SET DEFAULT nextval('public.generador_id_seq'::regclass);


--
-- Name: invequipopsut id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.invequipopsut ALTER COLUMN id SET DEFAULT nextval('public.invequipopsut_id_seq'::regclass);


--
-- Name: localidad id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.localidad ALTER COLUMN id SET DEFAULT nextval('public.localidad_id_seq'::regclass);


--
-- Name: mantenimiento id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mantenimiento ALTER COLUMN id SET DEFAULT nextval('public.mantenimiento_id_seq'::regclass);


--
-- Name: menu id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.menu ALTER COLUMN id SET DEFAULT nextval('public.menu_id_seq'::regclass);


--
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- Name: motogenerador id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.motogenerador ALTER COLUMN id SET DEFAULT nextval('public.motogenerador_id_seq'::regclass);


--
-- Name: motor id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.motor ALTER COLUMN id SET DEFAULT nextval('public.motor_id_seq'::regclass);


--
-- Name: multimedia id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.multimedia ALTER COLUMN id SET DEFAULT nextval('public.multimedia_id_seq'::regclass);


--
-- Name: nodoopsut id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nodoopsut ALTER COLUMN id SET DEFAULT nextval('public.nodoopsut_id_seq'::regclass);


--
-- Name: personal id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.personal ALTER COLUMN id SET DEFAULT nextval('public.personal_id_seq'::regclass);


--
-- Name: pisos id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pisos ALTER COLUMN id SET DEFAULT nextval('public.pisos_id_seq'::regclass);


--
-- Name: plantillas id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.plantillas ALTER COLUMN id SET DEFAULT nextval('public.plantillas_id_seq'::regclass);


--
-- Name: rect_bcobb id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rect_bcobb ALTER COLUMN id SET DEFAULT nextval('public.rect_bcobb_id_seq'::regclass);


--
-- Name: rectificadores id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rectificadores ALTER COLUMN id SET DEFAULT nextval('public.rectificadores_id_seq'::regclass);


--
-- Name: regiones id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regiones ALTER COLUMN id SET DEFAULT nextval('public.regiones_id_seq'::regclass);


--
-- Name: registro id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.registro ALTER COLUMN id SET DEFAULT nextval('public.registro_id_seq'::regclass);


--
-- Name: requeopsut id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.requeopsut ALTER COLUMN id SET DEFAULT nextval('public.requeopsut_id_seq'::regclass);


--
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- Name: salas id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.salas ALTER COLUMN id SET DEFAULT nextval('public.salas_id_seq'::regclass);


--
-- Name: status id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.status ALTER COLUMN id SET DEFAULT nextval('public.status_id_seq'::regclass);


--
-- Name: sub_menu id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sub_menu ALTER COLUMN id SET DEFAULT nextval('public.sub_menu_id_seq'::regclass);


--
-- Name: sub_sub_menu id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sub_sub_menu ALTER COLUMN id SET DEFAULT nextval('public.sub_sub_menu_id_seq'::regclass);


--
-- Name: tickets id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tickets ALTER COLUMN id SET DEFAULT nextval('public.tickets_id_seq'::regclass);


--
-- Name: ubicacion id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ubicacion ALTER COLUMN id SET DEFAULT nextval('public.ubicacion_id_seq'::regclass);


--
-- Name: ups id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ups ALTER COLUMN id SET DEFAULT nextval('public.ups_id_seq'::regclass);


--
-- Name: ups_bb id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ups_bb ALTER COLUMN id SET DEFAULT nextval('public.ups_bb_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: acciones; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.acciones (id, nombre, created_at, updated_at) FROM stdin;
1	ADECUACION DE ESPACIOS	\N	\N
2	BAJO NIVEL	\N	\N
3	CAMBIO	\N	\N
4	CARGA	\N	\N
5	COMPLETACION	\N	\N
6	CORTE PROGRAMADO	\N	\N
7	DESINCORPORACION	\N	\N
8	FALLA	\N	\N
9	FALSA ALARMA	\N	\N
10	INHIBIDO	\N	\N
11	INSPECCION	\N	\N
12	INSTALACION	\N	\N
13	INSTALACION Y PRUEBA	\N	\N
14	LIMPIEZA	\N	\N
15	MANTENIMIENTO CORRECTIVO	\N	\N
16	MANTENIMIENTO PREVENTIVO	\N	\N
17	MOTOR EN SERVICIO	\N	\N
18	OBSTRUCCION	\N	\N
19	OTROS(ESPECIFIQUE)	\N	\N
20	PRUEBA	\N	\N
21	REPUESTO	\N	\N
22	SUMINISTRO	\N	\N
23	SUSTITUCION	\N	\N
24	TRASEGADO	\N	\N
25	TRASLADO	\N	\N
26	FALLA DE RED PUBLICA	2021-03-18 11:26:49	2021-03-18 11:26:49
\.


--
-- Data for Name: aire_acondicionado; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.aire_acondicionado (id, marca, modelo, serial, inventario_cant, tipo_id, nro_circuitos, tipo_compresor, tipo_refrigerante, ton_refrig_operativa, ton_refrig_faltante, volt_operacion, operatividad, fecha_instalacion, criticidad, garantia, observaciones, ubicacion_id, tablero_control, status_id, created_at, updated_at, ton_refrig_instalada, tipo_correa, nro_correa, cant_correa, tipo_equipo) FROM stdin;
19	Carrier	40RR-024-020	0591F47755	5055897	\N	Bifásico	Semi-Hermético	R22	20	00	208/230/3/60	Operativo	2010-01-04	Alta	no	CONDENSADORA OPERATIVA 100%	80	Si	\N	2021-02-04 12:59:42	2021-02-11 09:50:48	20	B	40	2	3
5	Carrier	39NXH21	no posee	3118083	\N	Monofásico	\N	R22	00	100	460/3/60	Operativo	2008-12-08	Alta	no	.	57	Si	\N	2021-01-27 12:32:14	2021-02-01 11:31:51	100	B56	56	2	2
4	Carrier	39NXH21	no posee	3118083	\N	Monofásico	\N	R22	30	70	460/3/60	Operativo	2008-12-08	Alta	no	TRABAJA EN CONDICIONES NORMALES DE OPERACION	56	Si	\N	2021-01-27 12:24:57	2021-02-01 11:32:48	100	B56	56	2	2
9	Carrier	39NXH32	no posee	3118095	\N	Monofásico	\N	R22	30	70	460/3/60	Operativo	2008-12-08	Alta	no	OPERATIVO	66	Si	\N	2021-02-01 10:13:24	2021-02-01 11:33:22	100	B56	56	2	2
8	Carrier	39NXH32	no posee	3118083	\N	Monofásico	\N	R22	00	100	460/3/60	Inoperativo	2011-12-28	Alta	no	INOPERATIVO	65	Si	\N	2021-02-01 10:08:37	2021-02-01 11:34:14	100	B56	56	2	2
10	Carrier	39NA32	no posee	3118093	\N	Trifásico	\N	R22	30	70	460/3/60	Operativo	2008-12-08	Alta	no	OPERATIVO	71	si	\N	2021-02-01 11:53:30	2021-02-01 11:53:30	100	B56	56	2	2
6	Carrier	40RM012-B611GC	5105X15995	3135874	\N	Monofásico	Semi-Hermético	R22	10	00	208/230/3/60	Operativo	2000-01-01	Alta	no	EQUIPO OPERATIVO	62	Si	\N	2021-01-28 10:56:28	2021-02-03 11:11:27	10	A40	1	1	3
20	Carrier	38AKS024-510	4399F58304	3135867	\N	Bifásico	Semi-Hermético	R22	20	00	208/230/3/60	Operativo	2010-01-04	Alta	no	EQUIPO CONDENSADOR OPERATIVO 100%	81	si	\N	2021-02-04 13:03:16	2021-02-04 13:03:16	20	B	40	2	3
12	Traine	TSC060E3E0A0000	101211149L	4016407	\N	Monofásico	Hermético	R22	05	00	208/230/3/60	Operativo	2005-01-01	Alta	no	EQUIPO OPERATIVO	73	Si	\N	2021-02-03 11:22:23	2021-02-03 11:35:41	05	A	26	1	4
21	Chigo	AR18MVSHCWKNED	no posee	NO POSEE	\N	Monofásico	Hermético	R22	1.5	00	208/230/3/60	Operativo	2015-09-02	Alta	no	Equipo operativo 100%, amerita A/A de mayor capacidad para la sala	82	no	\N	2021-02-09 08:57:44	2021-02-09 08:57:44	1.5	no aplica	0	0	4
22	Chigo	AR18MVSHCWKNED	no posee	NO POSEE	\N	Monofásico	Hermético	R22	1.5	00	208/230/3/60	Operativo	2015-09-02	Alta	no	Equipo operativo, sala amerita A/A de mayor capacidad	83	no	\N	2021-02-09 09:00:39	2021-02-09 09:00:39	1.5	no aplica	0	0	4
7	Marvair	ADP60ACC15NR-2000 CI	DJ7879	3133081	\N	Monofásico	Hermético	410A	05	00	208/230/3/60	Operativo	2021-01-27	Alta	no	EQUIPO OPERATIVO	63	Si	\N	2021-01-28 11:50:50	2021-02-03 11:48:39	05	N/A	0	0	4
11	Marvair	50ZP-060-311	0101G40218	4016406	\N	Monofásico	Hermético	R22	05	00	208/230/3/60	Operativo	2005-01-01	Alta	no	EQUIPO OPERATIVO	72	Si	\N	2021-02-03 11:18:15	2021-02-03 11:49:07	05	N/A	0	0	4
13	Carrier	ZZ-AC/PU-060	300705944018C030100084	NO POSEE	\N	Monofásico	Hermético	R22	05	00	208/230/3/60	Operativo	2020-11-04	Alta	si	EQUIPO OPERATIVO	74	si	\N	2021-02-03 13:49:57	2021-02-03 13:49:57	05	S/I	0	0	4
14	Carrier	ZZ-AC/PU-060	300705944019C030100089	NO APLICA	\N	Monofásico	Hermético	R22	05	00	208/230/3/60	Operativo	2020-11-04	Alta	si	EQUIPO OPERATIVO	75	si	\N	2021-02-03 13:56:21	2021-02-03 13:56:21	05	no aplica	0	0	4
15	Carrier	40RM-034-B600	3398F62827	3135855	\N	Monofásico	Semi-Hermético	R22	10	10	460/3/60	Operativo	2010-04-01	Alta	no	EQUIPO EVAPORADOR  AMERITA COMPRESOR DE 10TR PARA SU OPERATIVIDAD 100%	76	Si	\N	2021-02-04 12:08:14	2021-02-04 12:20:21	20	B	42	2	3
18	Carrier	38AE-016-500	1292781897	3135871	\N	Bifásico	Semi-Hermético	R22	15	00	208/230/3/60	Operativo	2010-01-04	Alta	no	EQUIPO CONDENSADOR OPERATIVO	79	si	\N	2021-02-04 12:38:58	2021-02-04 12:38:58	15	B	40	2	3
23	Chigo	AR18MVSHCWKNED	no posee	NO POSEE	\N	Monofásico	Hermético	R22	1.5	00	208/230/3/60	Operativo	2015-09-02	Alta	no	Equipo operativo, Amerita A/A de mayor capacidad	84	no	\N	2021-02-09 09:02:43	2021-02-09 09:02:43	1.5	no aplica	0	0	4
24	Chigo	AR18MVSHCWKNED	no posee	NO POSEE	\N	Monofásico	Hermético	R22	1.5	00	208/230/3/60	Operativo	2015-09-02	Alta	no	Equipo operativo, Amerita A/A de mayor capacidad	85	no	\N	2021-02-09 09:04:24	2021-02-09 09:04:24	1.5	no aplica	0	0	4
25	Chigo	AR18MVSHCWKNED	no posee	NO POSEE	\N	Monofásico	Hermético	R22	1.5	00	208/230/3/60	Operativo	2015-09-02	Alta	no	Equipo operativo 100%, Amerita A/A de mayor capacidad	86	no	\N	2021-02-09 09:25:51	2021-02-09 09:25:51	1.5	no aplica	0	0	4
26	Chigo	AR18MVSHCWKNED	no posee	NO POSEE	\N	Monofásico	Hermético	R22	1.5	00	208/230/3/60	Operativo	2015-09-02	Alta	no	Equipo operativo 100%, Amerita A/A de mayor capacidad	87	no	\N	2021-02-09 09:28:57	2021-02-09 09:28:57	1.5	no aplica	0	0	4
27	Chigo	AR18MVSHCWKNED	no posee	NO POSEE	\N	Monofásico	Hermético	R22	1.5	00	208/230/3/60	Operativo	2015-09-02	Alta	no	Equipo operativo 100%, Amerita equipo de A/A de mayor capacidad	88	no	\N	2021-02-09 09:40:06	2021-02-09 09:40:06	1.5	no aplica	0	0	4
28	Chigo	AR18MVSHCWKNED	no posee	NO POSEE	\N	Monofásico	Hermético	R22	1.5	00	208/230/3/60	Operativo	2015-09-02	Alta	no	Equipo operativo 100%, Amerita A/A de mayor capacidad	89	no	\N	2021-02-09 09:42:05	2021-02-09 09:42:05	1.5	no aplica	0	0	4
29	Chigo	AR18MVSHCWKNED	no posee	NO POSEE	\N	Monofásico	Hermético	R22	1.5	00	208/230/3/60	Operativo	2015-09-02	Alta	no	Equipo operativo 100%, Amerita equipo de mayor capacidad	90	no	\N	2021-02-09 09:43:46	2021-02-09 09:43:46	1.5	no aplica	0	0	4
31	Carrier	38AK-028600	3793F66118	3135946	\N	Bifásico	Semi-Hermético	R22	00	20	208/230/3/60	Vandalizado	2010-04-02	Alta	no	EQUIPO AMERITA 02 COMPRESORES Y TODOS LOS MATERIALES PARA SU INSTALACION	92	si	\N	2021-02-09 11:12:48	2021-02-09 11:12:48	20	B	40	2	3
32	Carrier	FB4BNF060000AAAA	3104A72225	3135966	\N	Monofásico	Hermético	R22	05	00	208/230/3/60	Operativo	2015-02-09	Alta	no	EQUIPO FUNCIONAL 100%	93	no	\N	2021-02-09 11:55:03	2021-02-09 11:55:03	05	no aplica	0	0	3
33	Carrier	38CKC060570	1905E40526	3135947	\N	Monofásico	Hermético	R22	05	00	208/230/3/60	Operativo	2015-02-09	Alta	si	Condensador operativo, tiene garantia de 1 año	94	si	\N	2021-02-09 11:58:05	2021-02-09 11:58:05	05	no aplica	0	0	3
30	Carrier	69CA0901AVA14-DX	733210090	3135944	\N	Bifásico	Semi-Hermético	R22	00	20	208/230/3/60	Operativo	2015-09-02	Alta	no	Equipo amerita 02 motoventiladores	91	Si	\N	2021-02-09 10:42:20	2021-02-11 09:42:49	20	B	40	2	3
17	Carrier	40RR-016-5	1592F94802	3135851	\N	Bifásico	Semi-Hermético	R22	15	00	460/3/60	Operativo	2010-04-01	Alta	no	EQUIPO EVAPORADOR  OPERATIVO 100%	78	Si	\N	2021-02-04 12:24:14	2021-02-11 10:15:51	15	B	42	2	3
36	Marvair	ADP60ACC15NR-2000CI	CJ59627	3135207	\N	Monofásico	Hermético	R22	03	00	208/230/3/60	Operativo	2010-12-03	Baja	no	OPERATIVO	161	si	\N	2021-03-18 10:52:39	2021-03-18 10:52:39	03	NA	0	0	4
37	Carrier	40RMM-024-B511GC	4508032791	5056034	\N	Monofásico	Semi-Hermético	R22	00	00	208/230/3/60	Inoperativo	2010-03-18	Alta	no	EQUIPO EVAPORADOR  OPERATIVO , CONDENSADOR AMERITA COMPRESOR SEMI HERMETICO DE 20TR	162	si	\N	2021-03-18 11:01:34	2021-03-18 11:01:34	20	B	42	2	3
16	Carrier	38AH034610AA	0499F94702	3135864	\N	Trifásico	Semi-Hermético	R22	10	10	460/3/60	Operativo	2010-04-01	Alta	no	EQUIPO CONDENSADO OPERATIVO CON SUS MOTOVENTILADORES	77	Si	\N	2021-02-04 12:20:00	2021-03-18 12:57:05	10	B	42	2	3
38	Carrier	38AK024-521	2708G20076	5056035	\N	Trifásico	Semi-Hermético	R22	20	00	208/230/3/60	Operativo	2008-03-18	Alta	si	En este caso solo la garantia se refiere al compresor , debido a que es un compresor reparado	167	si	\N	2021-03-18 12:32:05	2021-03-18 12:32:05	20	B	40	2	3
39	Liebert	VS070ARCO3E1703A	COGC8E0016	5055909	\N	Trifásico	Hermético	R22	20	00	208/230/3/60	Operativo	2007-03-18	Alta	no	Equipo operativo 100%	169	si	\N	2021-03-18 13:09:52	2021-03-18 13:09:52	20	B	34	2	1
40	Trane	TCC024F100B6	5344YE52H	502493	\N	Monofásico	Hermético	R22	03	00	208/230/3/60	Operativo	2006-06-01	Alta	no	EQUIPO OPERATIVO	170	no	\N	2021-03-18 13:25:12	2021-03-18 13:25:12	03	0	0	0	4
41	Trane	TCC024F100B5	5201LW82H	502494	\N	Monofásico	Hermético	R22	03	00	208/230/3/60	Operativo	2006-06-01	Alta	no	LA UNIDAD CONDENSADORA ESTA DETERIORADA POR VIDA UTIL	171	no	\N	2021-03-18 13:40:35	2021-03-18 13:40:35	03	0	0	0	4
42	Carrier	38AKS024-510	4399F58304	3135867	\N	Trifásico	Hermético	R22	20	00	208/230/3/60	Operativo	2005-12-05	Alta	no	Equipo operativo al 100%.	172	si	\N	2021-03-18 13:46:33	2021-03-18 13:46:33	20	B	34	2	3
43	Carrier	40RM-016-B600GC	2195F49359	5055907	\N	Trifásico	Hermético	R22	15	00	208/230/3/60	Operativo	2007-02-05	Alta	no	Equipo evaporadora operativo al 100%	173	si	\N	2021-03-18 13:55:13	2021-03-18 13:55:13	15	B	34	2	3
44	Carrier	38KSO16-500	4595F77991	3135865	\N	Trifásico	Hermético	R22	15	00	208/230/3/60	Operativo	2007-12-06	Alta	no	Equipo condensadora operativo al 100%.	174	si	\N	2021-03-18 13:59:26	2021-03-18 13:59:26	15	B	34	2	3
45	Liebert	BF067A-CSM	225886-006	5055991	\N	Trifásico	Hermético	R22	05	00	208/230/3/60	Operativo	2004-02-05	Alta	no	Equipo evaporadora operativa al 100%.	175	si	\N	2021-03-18 14:09:42	2021-03-18 14:09:42	05	B	34	2	1
46	Liebert	CSL-083LY	98090181	3135869	\N	Trifásico	Hermético	R22	05	00	208/230/3/60	Operativo	2005-12-06	Alta	no	Equipo  condensadora  operativo al 100%	176	si	\N	2021-03-18 14:14:30	2021-03-18 14:14:30	05	B	34	5	1
47	Liebert	BF067A-CSM	225886-007	5055992	\N	Trifásico	Hermético	R22	05	00	208/230/3/60	Operativo	2006-08-05	Alta	no	Operativo evaporadora al 100%	177	si	\N	2021-03-18 14:24:04	2021-03-18 14:24:04	05	A	36	2	1
48	Carrier	40RR016	34K800887	no posee	\N	Trifásico	\N	R22	00	15	208/230/3/60	Inoperativo	1993-01-01	Media	no	Requiere suministro de 2 compresores de 7.5 Tr. Valvula de expansión, 02 motorventiladores para la condensadora.	178	no	\N	2021-03-19 11:16:21	2021-03-19 11:16:21	15	A45	45	2	2
49	Carrier	adfsd232	no posee	SIN INFORMACION	\N	Monofásico	Hermético	R22	12	00	208/230/3/60	Operativo	2000-01-01	Alta	no	OPERATIVO	179	no	\N	2021-03-19 11:36:49	2021-03-19 11:36:49	12	B52	2	2	3
50	Carrier	50ES-A36---30TP	0513C16824	no posee	\N	Bifásico	Hermético	410A	05	00	208/230/3/60	Operativo	2010-01-01	Media	no	Sin observaciones	186	si	\N	2021-03-19 15:54:19	2021-03-19 15:54:19	05	00	0	0	4
51	Carrier	50ES-A36---30TP	4912C11061	no posee	\N	Bifásico	Hermético	410A	00	05	208/230/3/60	Inoperativo	2010-01-01	Media	no	Requiere suministro e instalación de motor ventilador de la condensadora y motor de la unidad evaporadora.	187	si	\N	2021-03-19 16:02:26	2021-03-19 16:02:26	05	00	0	0	4
52	Carrier	Climar PHC-24-41	97CE109-0002	no posee	\N	Bifásico	Hermético	R22	00	00	208/230/3/60	Vandalizado	2010-01-01	Baja	no	Equipo vandalizado	188	si	\N	2021-03-19 16:08:56	2021-03-19 16:08:56	05	00	0	0	4
53	Liebert	CSL-083LY	98090182	3135868	\N	Trifásico	Semi-Hermético	R22	05	00	208/230/3/60	Operativo	2008-12-03	Alta	no	Equipo condensadora operativo al 100%.	189	si	\N	2021-03-19 16:31:08	2021-03-19 16:31:08	05	A	36	2	1
54	Airflow	CCX20A4	026FX044	3135356	\N	Trifásico	Semi-Hermético	R22	20	00	208/230/3/60	Operativo	2008-02-01	Alta	no	Equipo operativo al 100%	192	si	\N	2021-03-22 13:01:28	2021-03-22 13:01:28	20	A	59	2	3
55	Carrier	50TFF012511	4306G30734	3135367	\N	Trifásico	Semi-Hermético	R22	10	00	208/230/3/60	Operativo	2001-12-06	Alta	no	Equipo operativo	194	si	\N	2021-03-22 13:34:41	2021-03-22 13:34:41	10	A	49	2	4
56	SubZero	ZZ-AC/PU-060	3407051880391120100027	000000	\N	Trifásico	Hermético	410A	05	00	208/230/3/60	Operativo	2020-06-16	Baja	si	AA 5TR NUEVO	197	no	\N	2021-03-23 10:33:17	2021-03-23 10:33:17	05	NO APLICA	0	0	4
57	SubZero	ZZ-AC/PU-060	340705944018C030100049	00000001	\N	Trifásico	Hermético	410A	05	00	208/230/3/60	Operativo	2020-06-16	Baja	si	AA 5TR NUEVO	199	no	\N	2021-03-23 10:38:03	2021-03-23 10:38:03	05	NO APLICA	0	0	4
58	SubZero	ZZ-AC/PU-060	3407051880391120100063	00000001	\N	Trifásico	Hermético	410A	05	00	208/230/3/60	Operativo	2020-04-01	Baja	si	AA 5TR NUEVO	200	no	\N	2021-03-23 10:40:03	2021-03-23 10:40:03	05	NO APLICA	0	0	4
59	SubZero	ZZ-AC/PU-060	3407051880391120100055	00000001	\N	Trifásico	Hermético	410A	05	00	208/230/3/60	Operativo	2020-04-05	Baja	si	AA 5TR NUEVO	201	no	\N	2021-03-23 10:42:12	2021-03-23 10:42:12	05	NO APLICA	0	0	4
60	SubZero	ZZ-AC/PU-060	C704052410117822400028	00000001	\N	Trifásico	Hermético	410A	05	00	208/230/3/60	Operativo	2020-06-16	Baja	si	AA  5TR NUEVO	202	no	\N	2021-03-23 10:43:56	2021-03-23 10:43:56	05	NO APLICA	0	0	4
61	SubZero	ZZ-AC/PU-060	340705944018C030100051	00000001	\N	Trifásico	Hermético	410A	05	00	208/230/3/60	Operativo	2020-07-14	Baja	si	AA 5 TR NUEVO	203	no	\N	2021-03-23 10:46:18	2021-03-23 10:46:18	05	NO APLICA	0	0	4
62	SubZero	ZZ-AC/PU-060	340705944018C030100041	00000001	\N	Trifásico	Hermético	410A	05	00	208/230/3/60	Operativo	2020-08-24	Baja	si	AA 5TR NUEVO	204	no	\N	2021-03-23 10:48:13	2021-03-23 10:48:13	05	NO APLICA	0	0	4
63	SubZero	ZZ-AC/PU-060	3407051880391120100004	00000001	\N	Trifásico	Hermético	410A	05	00	208/230/3/60	Operativo	2020-07-03	Baja	si	AA 5TR NUEVO	205	no	\N	2021-03-23 10:50:11	2021-03-23 10:50:11	05	NO APLICA	0	0	4
64	SubZero	ZZ-AC/PU-060	704052410117822400030	00000001	\N	Trifásico	Hermético	410A	05	00	208/230/3/60	Operativo	2020-07-03	Baja	si	AA 5TR NUEVO	206	no	\N	2021-03-23 10:51:59	2021-03-23 10:51:59	05	NO APLICA	0	0	4
65	Star Way	ZZ-AC/PU-060	3407051880391120100037	00000001	\N	Trifásico	Hermético	410A	05	00	208/230/3/60	Operativo	2020-04-01	Baja	si	AA 5TR NUEVO	207	no	\N	2021-03-23 10:54:26	2021-03-23 10:54:26	05	NO APLICA	0	0	4
66	SubZero	ZZ-AC/PU-060	3407051880391120100037	00000001	\N	Trifásico	Hermético	410A	05	00	208/230/3/60	Operativo	2020-05-21	Baja	si	AA 5TR NUEVO	208	no	\N	2021-03-23 10:57:02	2021-03-23 10:57:02	05	NO APLICA	0	0	4
67	SubZero	ZZ-AC/PU-060	3407051880391120100062	00000001	\N	Trifásico	Hermético	410A	05	00	208/230/3/60	Operativo	2020-03-26	Baja	si	AA 5TR NUEVO	209	no	\N	2021-03-23 10:59:57	2021-03-23 10:59:57	05	NO APLICA	0	0	4
\.


--
-- Data for Name: bateria_control; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.bateria_control (id, motog_id, cantidad, amperaje, voltaje, created_at, updated_at) FROM stdin;
1	5	3	231	12v	2021-01-12 13:36:01	2021-01-12 13:36:01
2	12	2	1000	12v	2021-01-20 10:06:43	2021-01-20 10:06:43
3	13	2	1000	12v	2021-01-20 10:17:56	2021-01-20 10:17:56
4	14	2	1000	12v	2021-01-20 10:19:20	2021-01-20 10:19:20
5	15	2	70	12v	2021-01-20 10:36:09	2021-01-20 10:36:09
6	16	1	SIN INFORMACION	12v	2021-01-20 10:47:07	2021-01-20 10:47:07
7	17	1	SIN INFORMACION	12v	2021-01-20 10:53:45	2021-01-20 10:53:45
8	18	1	SIN INFORMACION	12v	2021-01-20 11:04:50	2021-01-20 11:04:50
9	19	1	SIN INFORMACION	12v	2021-01-20 11:10:49	2021-01-20 11:10:49
10	20	1	SIN INFORMACION	12v	2021-01-20 12:33:10	2021-01-20 12:33:10
11	21	1	SIN INFORMACION	12v	2021-01-20 12:33:11	2021-01-20 12:33:11
12	22	1	SIN INFORMACION	12v	2021-01-20 12:33:11	2021-01-20 12:33:11
13	23	2	850	12v	2021-01-20 12:54:48	2021-01-20 12:54:48
14	24	2	70	12v	2021-01-22 10:55:37	2021-01-22 10:55:37
15	28	2	70	12v	2021-01-22 11:02:37	2021-01-22 11:02:37
16	33	2	110	12v	2021-01-22 11:57:27	2021-01-22 11:57:27
17	34	2	110	12v	2021-01-22 12:12:55	2021-01-22 12:12:55
18	37	\N	110	12v	2021-01-22 12:40:15	2021-01-22 12:40:15
19	39	2	110	12v	2021-01-22 12:52:34	2021-01-22 12:52:34
20	41	2	70	12v	2021-01-26 12:20:18	2021-01-26 12:20:18
22	45	2	150	12v	2021-01-28 08:13:50	2021-01-28 08:13:50
23	46	2	150	12v	2021-01-28 08:16:35	2021-01-28 08:16:35
24	47	1	24	24v	2021-01-29 13:16:31	2021-01-29 13:16:31
25	48	2	2250	12v	2021-02-01 11:19:31	2021-02-01 11:19:31
26	49	2	155	12v	2021-02-01 11:27:45	2021-02-01 11:27:45
27	50	2	70	12v	2021-02-01 11:38:05	2021-02-01 11:38:05
28	51	2	2250	12v	2021-02-01 11:47:16	2021-02-01 11:47:16
21	44	1	150	12v	2021-01-28 08:13:17	2021-02-03 12:38:33
29	52	2	150	12v	2021-02-10 12:53:37	2021-02-10 12:53:37
30	53	2	150	12v	2021-02-10 13:05:29	2021-02-10 13:05:29
31	54	2	150	12v	2021-02-10 13:08:49	2021-02-10 13:08:49
32	55	2	\N	12v	2021-02-10 14:19:07	2021-02-10 14:19:07
33	57	1	1	12v	2021-02-11 10:00:25	2021-02-11 10:02:25
34	99	2	150	12v	2021-03-18 10:37:17	2021-03-18 10:37:17
35	107	2	250	24v	2021-03-22 13:01:56	2021-03-22 13:01:56
\.


--
-- Data for Name: bco_baterias; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.bco_baterias (id, marca, modelo, serial, inventario_cant, cap_kva, carga_total_bb, elem_oper_bb, elem_inoper_bb, amp_elem, autonomia_respaldo, tablero, status_id, created_at, updated_at) FROM stdin;
1	CHARLES	wer123	no posee	sdfa	25156	asdf	123	123	1231	123	si	\N	2021-01-15 14:03:39	2021-01-15 14:03:39
2	SUNLIGHT	S/I	no posee	S/I	0	2160	24	0	0	6	no	\N	2021-01-28 08:26:34	2021-01-28 08:26:34
3	EVOLUTION	generic	no posee	309788687	200	200	20	20	40	2hrs	si	\N	2021-03-23 10:12:42	2021-03-23 10:12:42
\.


--
-- Data for Name: centrales; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.centrales (id, localidad_id, region_id, estado_id, area, tipo, created_at, updated_at) FROM stdin;
1	756	2	7	\N	\N	2021-01-12 12:29:07	2021-01-12 12:29:07
2	756	2	7	\N	\N	2021-01-12 12:35:29	2021-01-12 12:35:29
3	756	2	7	\N	\N	2021-01-12 12:35:38	2021-01-12 12:35:38
4	756	2	7	\N	\N	2021-01-12 12:53:01	2021-01-12 12:53:01
5	422	2	5	\N	\N	2021-01-12 13:36:01	2021-01-12 13:36:01
6	756	2	7	\N	\N	2021-01-12 13:42:58	2021-01-12 13:42:58
7	756	2	7	\N	\N	2021-01-12 14:25:41	2021-01-12 14:25:41
8	756	2	7	\N	\N	2021-01-12 14:35:46	2021-01-12 14:35:46
9	756	2	7	\N	\N	2021-01-12 15:46:05	2021-01-12 15:46:05
10	742	2	7	\N	\N	2021-01-13 13:38:03	2021-01-13 13:38:03
11	743	2	7	\N	\N	2021-01-13 14:09:13	2021-01-13 14:09:13
12	142	1	2	\N	\N	2021-01-13 18:28:45	2021-01-13 18:28:45
13	561	2	6	\N	\N	2021-01-15 14:03:39	2021-01-15 14:03:39
15	1849	5	19	\N	\N	2021-01-20 10:17:56	2021-01-20 10:17:56
16	1849	5	19	\N	\N	2021-01-20 10:19:20	2021-01-20 10:19:20
17	1935	5	20	\N	\N	2021-01-20 10:36:09	2021-01-20 10:36:09
18	1928	5	20	\N	\N	2021-01-20 10:47:07	2021-01-20 10:47:07
19	1929	5	20	\N	\N	2021-01-20 10:53:44	2021-01-20 10:53:44
20	1955	5	20	\N	\N	2021-01-20 11:04:50	2021-01-20 11:04:50
21	1938	5	20	\N	\N	2021-01-20 11:10:49	2021-01-20 11:10:49
22	1911	5	20	\N	\N	2021-01-20 12:33:10	2021-01-20 12:33:10
23	1911	5	20	\N	\N	2021-01-20 12:33:11	2021-01-20 12:33:11
24	1911	5	20	\N	\N	2021-01-20 12:33:11	2021-01-20 12:33:11
25	1963	5	20	\N	\N	2021-01-20 12:54:48	2021-01-20 12:54:48
26	1501	4	16	\N	\N	2021-01-22 08:52:07	2021-01-22 08:52:07
27	1805	5	19	\N	\N	2021-01-22 10:55:37	2021-01-22 10:55:37
28	230	1	2	\N	\N	2021-01-22 11:01:31	2021-01-22 11:01:31
29	230	1	2	\N	\N	2021-01-22 11:01:49	2021-01-22 11:01:49
30	230	1	2	\N	\N	2021-01-22 11:01:58	2021-01-22 11:01:58
31	1887	5	19	\N	\N	2021-01-22 11:02:37	2021-01-22 11:02:37
32	231	1	2	\N	\N	2021-01-22 11:08:48	2021-01-22 11:08:48
33	155	1	2	\N	\N	2021-01-22 11:15:10	2021-01-22 11:15:10
34	205	1	2	\N	\N	2021-01-22 11:22:35	2021-01-22 11:22:35
35	187	1	2	\N	\N	2021-01-22 11:28:27	2021-01-22 11:28:27
36	210	1	2	\N	\N	2021-01-22 11:38:13	2021-01-22 11:38:13
37	210	1	2	\N	\N	2021-01-22 11:42:37	2021-01-22 11:42:37
38	1856	5	19	\N	\N	2021-01-22 11:57:27	2021-01-22 11:57:27
39	1831	5	19	\N	\N	2021-01-22 12:09:50	2021-01-22 12:09:50
40	1831	5	19	\N	\N	2021-01-22 12:10:57	2021-01-22 12:10:57
41	1831	5	19	\N	\N	2021-01-22 12:12:55	2021-01-22 12:12:55
42	1872	5	19	\N	\N	2021-01-22 12:38:45	2021-01-22 12:38:45
43	1872	5	19	\N	\N	2021-01-22 12:39:19	2021-01-22 12:39:19
44	1872	5	19	\N	\N	2021-01-22 12:40:15	2021-01-22 12:40:15
45	1814	5	19	\N	\N	2021-01-22 12:52:00	2021-01-22 12:52:00
46	1814	5	19	\N	\N	2021-01-22 12:52:34	2021-01-22 12:52:34
47	1888	5	19	\N	\N	2021-01-26 10:51:18	2021-01-26 10:51:18
48	1906	5	20	\N	\N	2021-01-26 12:20:18	2021-01-26 12:20:18
49	2590	6	30	\N	\N	2021-01-26 12:36:01	2021-01-26 12:36:01
50	2590	6	30	\N	\N	2021-01-26 12:48:57	2021-01-26 12:48:57
51	2615	6	31	\N	\N	2021-01-26 13:02:55	2021-01-26 13:02:55
52	2590	6	30	\N	\N	2021-01-26 13:06:20	2021-01-26 13:06:20
53	2624	6	32	\N	\N	2021-01-26 13:10:39	2021-01-26 13:10:39
14	1868	5	19	\N	\N	2021-01-20 10:06:43	2021-01-26 15:01:53
54	756	2	7	\N	\N	2021-01-27 11:57:28	2021-01-27 11:57:28
55	756	2	7	\N	\N	2021-01-27 11:57:43	2021-01-27 11:57:43
56	1501	4	16	\N	\N	2021-01-27 12:24:57	2021-01-27 12:24:57
57	1501	4	16	\N	\N	2021-01-27 12:32:14	2021-01-27 12:32:14
58	2589	6	30	\N	\N	2021-01-28 08:13:17	2021-01-28 08:13:17
59	2589	6	30	\N	\N	2021-01-28 08:13:50	2021-01-28 08:13:50
60	2589	6	30	\N	\N	2021-01-28 08:16:35	2021-01-28 08:16:35
61	2589	6	30	\N	\N	2021-01-28 08:26:34	2021-01-28 08:26:34
62	2598	6	30	\N	\N	2021-01-28 10:56:28	2021-01-28 10:56:28
63	2590	6	30	\N	\N	2021-01-28 11:50:50	2021-01-28 11:50:50
64	2588	6	30	\N	\N	2021-01-29 13:16:31	2021-01-29 13:16:31
65	1501	4	16	\N	\N	2021-02-01 10:08:37	2021-02-01 10:08:37
66	1501	4	16	\N	\N	2021-02-01 10:13:24	2021-02-01 10:13:24
67	1501	4	16	\N	\N	2021-02-01 11:19:31	2021-02-01 11:19:31
68	2609	6	30	\N	\N	2021-02-01 11:27:45	2021-02-01 11:27:45
69	1934	5	20	\N	\N	2021-02-01 11:38:05	2021-02-01 11:38:05
70	1501	4	16	\N	\N	2021-02-01 11:47:16	2021-02-01 11:47:16
71	1501	4	16	\N	\N	2021-02-01 11:53:30	2021-02-01 11:53:30
73	2590	6	30	\N	\N	2021-02-03 11:22:23	2021-02-03 11:22:23
72	2590	6	30	\N	\N	2021-02-03 11:18:15	2021-02-03 11:34:59
74	2589	6	30	\N	\N	2021-02-03 13:49:57	2021-02-03 13:49:57
75	2589	6	30	\N	\N	2021-02-03 13:56:21	2021-02-03 13:56:21
76	2598	6	30	\N	\N	2021-02-04 12:08:14	2021-02-04 12:08:14
77	2598	6	30	\N	\N	2021-02-04 12:20:00	2021-02-04 12:20:00
78	2598	6	30	\N	\N	2021-02-04 12:24:14	2021-02-04 12:24:14
79	2598	6	30	\N	\N	2021-02-04 12:38:58	2021-02-04 12:38:58
80	2598	6	30	\N	\N	2021-02-04 12:59:42	2021-02-04 12:59:42
81	2598	6	30	\N	\N	2021-02-04 13:03:16	2021-02-04 13:03:16
82	2589	6	30	\N	\N	2021-02-09 08:57:44	2021-02-09 08:57:44
83	2589	6	30	\N	\N	2021-02-09 09:00:39	2021-02-09 09:00:39
84	2589	6	30	\N	\N	2021-02-09 09:02:43	2021-02-09 09:02:43
85	2589	6	30	\N	\N	2021-02-09 09:04:24	2021-02-09 09:04:24
86	2589	6	30	\N	\N	2021-02-09 09:25:51	2021-02-09 09:25:51
87	2589	6	30	\N	\N	2021-02-09 09:28:57	2021-02-09 09:28:57
88	2589	6	30	\N	\N	2021-02-09 09:40:06	2021-02-09 09:40:06
89	2589	6	30	\N	\N	2021-02-09 09:42:05	2021-02-09 09:42:05
90	2589	6	30	\N	\N	2021-02-09 09:43:46	2021-02-09 09:43:46
91	2589	6	30	\N	\N	2021-02-09 10:42:20	2021-02-09 10:42:20
92	2589	6	30	\N	\N	2021-02-09 11:12:48	2021-02-09 11:12:48
93	2589	6	30	\N	\N	2021-02-09 11:55:03	2021-02-09 11:55:03
94	2589	6	30	\N	\N	2021-02-09 11:58:05	2021-02-09 11:58:05
95	2609	6	30	\N	\N	2021-02-10 12:53:37	2021-02-10 12:53:37
96	2591	6	30	\N	\N	2021-02-10 13:05:29	2021-02-10 13:05:29
97	2591	6	30	\N	\N	2021-02-10 13:08:49	2021-02-10 13:08:49
98	2598	6	30	\N	\N	2021-02-10 14:19:07	2021-02-10 14:19:07
99	2660	5	22	\N	\N	2021-02-10 16:45:48	2021-02-10 16:45:48
100	2381	7	28	\N	\N	2021-02-11 10:00:25	2021-02-11 10:00:25
101	2382	7	28	\N	\N	2021-02-11 10:53:39	2021-02-11 10:53:39
102	2382	7	28	\N	\N	2021-02-11 11:05:14	2021-02-11 11:05:14
103	2427	7	28	\N	\N	2021-02-11 14:43:02	2021-02-11 14:43:02
104	2427	7	28	\N	\N	2021-02-11 14:43:11	2021-02-11 14:43:11
105	2428	7	28	\N	\N	2021-02-11 14:44:20	2021-02-11 14:44:20
106	2427	7	28	\N	\N	2021-02-12 08:12:48	2021-02-12 08:12:48
107	2429	7	28	\N	\N	2021-02-12 08:37:34	2021-02-12 08:37:34
108	2427	7	28	\N	\N	2021-02-12 09:09:05	2021-02-12 09:09:05
109	2464	7	28	\N	\N	2021-02-12 09:09:50	2021-02-12 09:09:50
110	2464	7	28	\N	\N	2021-02-12 09:19:29	2021-02-12 09:19:29
111	2464	7	28	\N	\N	2021-02-12 09:21:01	2021-02-12 09:21:01
112	2464	7	28	\N	\N	2021-02-12 10:02:13	2021-02-12 10:02:13
113	2464	7	28	\N	\N	2021-02-12 10:02:37	2021-02-12 10:02:37
114	1197	3	13	\N	\N	2021-02-12 10:11:54	2021-02-12 10:11:54
115	2464	7	28	\N	\N	2021-02-12 10:20:33	2021-02-12 10:20:33
116	2471	7	28	\N	\N	2021-02-12 10:36:16	2021-02-12 10:36:16
117	2379	7	28	\N	\N	2021-02-12 11:06:16	2021-02-12 11:06:16
118	2427	7	28	\N	\N	2021-02-12 11:15:56	2021-02-12 11:15:56
119	2464	7	28	\N	\N	2021-02-12 11:17:41	2021-02-12 11:17:41
120	2464	7	28	\N	\N	2021-02-12 11:20:32	2021-02-12 11:20:32
121	2402	7	28	\N	\N	2021-02-12 12:10:24	2021-02-12 12:10:24
122	2419	7	28	\N	\N	2021-02-12 15:40:17	2021-02-12 15:40:17
123	2451	7	28	\N	\N	2021-02-17 08:26:21	2021-02-17 08:26:21
124	2407	7	28	\N	\N	2021-02-17 15:27:02	2021-02-17 15:27:02
125	2408	7	28	\N	\N	2021-02-17 15:40:27	2021-02-17 15:40:27
126	2432	7	28	\N	\N	2021-02-17 15:53:12	2021-02-17 15:54:21
127	240	1	3	\N	\N	2021-02-23 11:35:31	2021-02-23 11:35:31
128	240	1	3	\N	\N	2021-02-23 11:36:39	2021-02-23 11:36:39
129	240	1	3	\N	\N	2021-02-23 11:38:28	2021-02-23 11:38:28
130	241	1	3	\N	\N	2021-02-23 12:06:52	2021-02-23 12:06:52
131	238	1	3	\N	\N	2021-02-23 12:22:06	2021-02-23 12:22:06
132	206	1	2	\N	\N	2021-02-23 14:06:13	2021-02-23 14:06:13
133	206	1	2	\N	\N	2021-02-23 14:10:14	2021-02-23 14:10:14
134	206	1	2	\N	\N	2021-02-23 14:17:51	2021-02-23 14:17:51
135	210	1	2	\N	\N	2021-02-23 14:22:26	2021-02-23 14:22:26
136	210	1	2	\N	\N	2021-02-23 14:27:11	2021-02-23 14:27:11
137	210	1	2	\N	\N	2021-02-23 14:27:54	2021-02-23 14:27:54
138	210	1	2	\N	\N	2021-02-23 14:32:24	2021-02-23 14:32:24
139	171	1	2	\N	\N	2021-02-23 14:53:10	2021-02-23 14:53:10
140	224	1	2	\N	\N	2021-02-24 14:01:47	2021-02-24 14:01:47
141	152	1	2	\N	\N	2021-02-24 14:38:20	2021-02-24 14:38:20
142	197	1	2	\N	\N	2021-02-24 14:43:59	2021-02-24 14:43:59
143	227	1	2	\N	\N	2021-02-25 13:53:30	2021-02-25 13:53:30
144	160	1	2	\N	\N	2021-02-25 14:14:40	2021-02-25 14:14:40
145	184	1	2	\N	\N	2021-02-25 14:18:03	2021-02-25 14:18:03
146	167	1	2	\N	\N	2021-02-25 14:23:59	2021-02-25 14:23:59
147	157	1	2	\N	\N	2021-02-25 14:32:19	2021-02-25 14:32:19
148	2614	6	31	\N	\N	2021-02-26 12:44:00	2021-02-26 12:44:00
149	371	1	4	\N	\N	2021-02-26 13:11:58	2021-02-26 13:11:58
150	371	1	4	\N	\N	2021-02-26 13:18:17	2021-02-26 13:18:17
151	2614	6	31	\N	\N	2021-02-26 13:23:59	2021-02-26 13:23:59
152	2614	6	31	\N	\N	2021-02-26 13:24:35	2021-02-26 13:24:35
153	371	1	4	\N	\N	2021-03-01 12:32:41	2021-03-01 12:32:41
154	371	1	4	\N	\N	2021-03-01 12:43:41	2021-03-01 12:43:41
155	371	1	4	\N	\N	2021-03-01 13:11:38	2021-03-01 13:11:38
156	372	1	4	\N	\N	2021-03-01 13:25:53	2021-03-01 13:25:53
157	3024	6	30	\N	\N	2021-03-16 14:35:31	2021-03-16 14:35:31
158	3024	6	30	\N	\N	2021-03-16 14:43:19	2021-03-16 14:43:19
159	2396	7	28	\N	\N	2021-03-17 10:35:38	2021-03-17 10:35:38
160	2600	6	30	\N	\N	2021-03-18 10:37:17	2021-03-18 10:37:17
161	2603	6	30	\N	\N	2021-03-18 10:52:39	2021-03-18 10:52:39
162	3024	6	30	\N	\N	2021-03-18 11:01:34	2021-03-18 11:01:34
163	2598	6	30	\N	\N	2021-03-18 11:20:08	2021-03-18 11:20:08
164	2598	6	30	\N	\N	2021-03-18 11:30:09	2021-03-18 11:30:09
165	2598	6	30	\N	\N	2021-03-18 11:56:57	2021-03-18 11:56:57
166	2598	6	30	\N	\N	2021-03-18 12:10:34	2021-03-18 12:10:34
167	3024	6	30	\N	\N	2021-03-18 12:32:05	2021-03-18 12:32:05
168	2598	6	30	\N	\N	2021-03-18 12:37:57	2021-03-18 12:37:57
169	2598	6	30	\N	\N	2021-03-18 13:09:52	2021-03-18 13:09:52
170	3047	7	26	\N	\N	2021-03-18 13:25:12	2021-03-18 13:25:12
171	3047	7	26	\N	\N	2021-03-18 13:40:35	2021-03-18 13:40:35
172	2598	6	30	\N	\N	2021-03-18 13:46:33	2021-03-18 13:46:33
173	2598	6	30	\N	\N	2021-03-18 13:55:13	2021-03-18 13:55:13
174	2598	6	30	\N	\N	2021-03-18 13:59:26	2021-03-18 13:59:26
175	2598	6	30	\N	\N	2021-03-18 14:09:42	2021-03-18 14:09:42
176	2598	6	30	\N	\N	2021-03-18 14:14:30	2021-03-18 14:14:30
177	2598	6	30	\N	\N	2021-03-18 14:24:04	2021-03-18 14:24:04
178	2379	7	28	\N	\N	2021-03-19 11:16:21	2021-03-19 11:16:21
179	2593	6	30	\N	\N	2021-03-19 11:36:49	2021-03-19 11:36:49
180	2489	7	28	\N	\N	2021-03-19 12:25:16	2021-03-19 12:25:16
181	2402	7	28	\N	\N	2021-03-19 15:44:47	2021-03-19 15:44:47
182	2402	7	28	\N	\N	2021-03-19 15:45:06	2021-03-19 15:45:06
183	2402	7	28	\N	\N	2021-03-19 15:45:28	2021-03-19 15:45:28
184	2402	7	28	\N	\N	2021-03-19 15:51:55	2021-03-19 15:51:55
185	2402	7	28	\N	\N	2021-03-19 15:53:05	2021-03-19 15:53:05
186	2402	7	28	\N	\N	2021-03-19 15:54:19	2021-03-19 15:54:19
187	2402	7	28	\N	\N	2021-03-19 16:02:26	2021-03-19 16:02:26
188	2402	7	28	\N	\N	2021-03-19 16:08:56	2021-03-19 16:08:56
189	2598	6	30	\N	\N	2021-03-19 16:31:08	2021-03-19 16:31:08
190	1171	3	13	\N	\N	2021-03-22 11:56:47	2021-03-22 11:56:47
191	1284	3	13	\N	\N	2021-03-22 12:59:23	2021-03-22 12:59:23
192	2594	6	30	\N	\N	2021-03-22 13:01:28	2021-03-22 13:01:28
193	1283	3	13	\N	\N	2021-03-22 13:01:56	2021-03-22 13:01:56
194	2594	6	30	\N	\N	2021-03-22 13:34:41	2021-03-22 13:34:41
195	2600	6	30	\N	\N	2021-03-22 17:29:29	2021-03-22 17:29:29
196	1283	3	13	\N	\N	2021-03-23 10:12:42	2021-03-23 10:12:42
197	2967	5	19	\N	\N	2021-03-23 10:33:17	2021-03-23 10:33:17
198	2967	5	19	\N	\N	2021-03-23 10:36:08	2021-03-23 10:36:08
199	2967	5	19	\N	\N	2021-03-23 10:38:03	2021-03-23 10:38:03
200	1872	5	19	\N	\N	2021-03-23 10:40:03	2021-03-23 10:40:03
201	1814	5	19	\N	\N	2021-03-23 10:42:12	2021-03-23 10:42:12
202	1824	5	19	\N	\N	2021-03-23 10:43:56	2021-03-23 10:43:56
203	2971	5	19	\N	\N	2021-03-23 10:46:18	2021-03-23 10:46:18
204	1887	5	19	\N	\N	2021-03-23 10:48:13	2021-03-23 10:48:13
205	1856	5	19	\N	\N	2021-03-23 10:50:11	2021-03-23 10:50:11
206	1856	5	19	\N	\N	2021-03-23 10:51:59	2021-03-23 10:51:59
207	1963	5	20	\N	\N	2021-03-23 10:54:26	2021-03-23 10:54:26
208	1934	5	20	\N	\N	2021-03-23 10:57:02	2021-03-23 10:57:02
209	1951	5	22	\N	\N	2021-03-23 10:59:57	2021-03-23 10:59:57
\.


--
-- Data for Name: comentarios; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.comentarios (id, descripcion, usuario_id, ticket_id, multimedia_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: componente; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.componente (id, descripcion, equipo_id, created_at, updated_at) FROM stdin;
1	BATERIA DE ARRANQUE	1	\N	\N
2	TANQUE PRINCIPAL DE COMBUSTIBLE	1	\N	\N
3	TANQUE DIARIO DE COMBUSTIBLE	1	\N	\N
4	TANQUE DE ACEITE	1	\N	\N
5	TANQUE DE REFRIGERANTE	1	\N	\N
6	MANGUERAS DE ALTA PRESIÓN	1	\N	\N
7	TABLERO DE CONTROL	1	\N	\N
8	RADIADOR	1	\N	\N
9	TURBO	1	\N	\N
10	GENERADOR	1	\N	\N
11	TARJETA REGULADORA DE VOLTAJE (AVR)	1	\N	\N
12	RECTIFICADOR	2	\N	\N
13	SUPERVISOR DE VOLTAJE	2	\N	\N
14	BANCO DE BATERIAS	2	\N	\N
15	COMPRESOR	3	\N	\N
16	MOTOR VENTILADOR	3	\N	\N
17	CONDENSADOR	3	\N	\N
18	EVAPORADOR	3	\N	\N
19	CAPACITOR	3	\N	\N
20	DUCTOS	3	\N	\N
21	BLOWER	3	\N	\N
22	TURBINA	3	\N	\N
23	MOTOR ELECTRICO	3	\N	\N
24	DUCTOS	4	\N	\N
25	FILTRO	4	\N	\N
26	VENTILADOR	4	\N	\N
27	CONTROLES	4	\N	\N
28	INTERCAMBIADOR DE CALOR UMA	4	\N	\N
29	HUMIDIFICADOR	4	\N	\N
30	SERPENTIN	4	\N	\N
31	MOTOR ELECTRICO	4	\N	\N
32	CHUMACERA	4	\N	\N
33	BOMBA DE AGUA	5	\N	\N
34	MOTOR ELECTRICO	5	\N	\N
35	COMPRESOR	5	\N	\N
36	MICROPROCESADOR	5	\N	\N
37	INTERCAMBIADOR DE CALOR	5	\N	\N
38	VENTILADOR	6	\N	\N
39	TUBERIA DE AGUA	6	\N	\N
40	TABLERO DE CONTROL	7	\N	\N
41	BANCO DE BATERIAS	7	\N	\N
\.


--
-- Data for Name: contratista; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.contratista (id, nombre, cuadrilla_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: cuadrilla; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cuadrilla (id, nombre, contratista_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: detallebypasopsut; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.detallebypasopsut (id, nodoopsut_bypas_id, nodoopsut_bypas_codigo1, nodoopsut_bypas_codigo2, observacion, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: detalleopsutpivot; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.detalleopsutpivot (id, nodoopsut_id, equipo_id, kva, tr, amph, watts, detalles_equipo, serial, status, observacion, created_at, updated_at) FROM stdin;
1	13	1	25	\N	\N	\N	\N	\N	1	operativo	2021-01-12 15:01:40	2021-01-12 15:01:40
2	11	1	50	\N	\N	\N	\N	\N	1	REQUIERE SUMINISTRO DE COMBUSTIBLE	2021-02-09 07:57:49	2021-02-09 07:57:49
3	11	9	\N	\N	250	\N	\N	\N	3	EL BANCO DE BATERÍA ESTA DESGASTADO REQUIERE CAMBIO	2021-02-09 08:02:31	2021-02-09 08:02:31
4	11	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 08:03:53	2021-02-09 08:03:53
5	11	3	\N	5	\N	\N	\N	\N	6	REQUIERE COMPRESOR	2021-02-09 08:04:50	2021-02-09 08:04:50
6	11	3	\N	5	\N	\N	\N	\N	6	REQUIERE COMPRESOR	2021-02-09 08:05:08	2021-02-09 08:05:08
7	11	9	\N	\N	250	\N	\N	\N	3	NO TIENEN RESPALDO, ESTÁN DESGASTADAS	2021-02-09 08:06:08	2021-02-09 08:06:08
8	11	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 08:08:28	2021-02-09 08:08:28
9	11	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 08:08:39	2021-02-09 08:08:39
10	11	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 08:08:49	2021-02-09 08:08:49
11	17	1	250	\N	\N	\N	\N	\N	1	SE REQUIERE DE ARRANQUE AUTOMATICO	2021-02-09 08:17:16	2021-02-09 08:17:16
12	17	9	\N	\N	200	\N	\N	\N	1	MODELO BSB DB12 - 200 DE 24 CELDAS	2021-02-09 08:19:22	2021-02-09 08:19:22
13	17	2	\N	\N	\N	\N	\N	\N	1	SR 300A/-48V 2700-6 07/891	2021-02-09 08:22:08	2021-02-09 08:22:08
14	17	2	\N	\N	\N	\N	\N	\N	1	SR 300A/-48V 2700-6 07/891	2021-02-09 08:23:57	2021-02-09 08:23:57
15	17	2	\N	\N	\N	\N	\N	\N	1	SR 300A/-48V 2700-6 07/891	2021-02-09 08:24:09	2021-02-09 08:24:09
16	17	2	\N	\N	\N	\N	\N	\N	1	SR 300A/-48V 2700-6 07/891	2021-02-09 08:24:19	2021-02-09 08:24:19
17	61	1	25	\N	\N	\N	\N	\N	2	FALTA BATERIA DE ARRANQUE Y ATS DAÑADO	2021-02-09 09:02:11	2021-02-09 09:02:11
18	61	9	\N	\N	200	\N	\N	\N	2	ABULTAMIENTO EN LAS BATERÍAS, SIN RESPALDO.	2021-02-09 09:03:07	2021-02-09 09:03:07
19	61	2	\N	\N	\N	\N	\N	\N	1	RECTIFICADORES DELTA ESR-48/56A F	2021-02-09 09:06:30	2021-02-09 09:06:30
20	61	2	\N	\N	\N	\N	\N	\N	1	DELTA ESR-48/56A F	2021-02-09 09:06:45	2021-02-09 09:06:45
21	61	2	\N	\N	\N	\N	\N	\N	1	DELTA ESR-48/56A F	2021-02-09 09:06:56	2021-02-09 09:06:56
22	61	3	\N	5	\N	\N	\N	\N	1	A/A COMPACTO DE 5TR	2021-02-09 09:07:45	2021-02-09 09:07:45
23	61	3	\N	5TR	\N	\N	\N	\N	1	A/A COMPACTO DE 5TR	2021-02-09 09:07:59	2021-02-09 09:07:59
24	53	1	50	\N	\N	\N	\N	\N	2	PROBLEMA DE ENCENDIDO. REQUIERE DE BATERÍA DE ARRANQUE	2021-02-09 09:21:12	2021-02-09 09:21:12
25	53	9	\N	\N	155	\N	\N	\N	2	BANCO DE BATERÍA ABULTADAS, SIN RESPALDO	2021-02-09 09:23:00	2021-02-09 09:23:00
26	53	9	\N	\N	155	\N	\N	\N	2	BANCO DE BATERÍA ABULTADAS, SIN RESPALDO	2021-02-09 09:23:39	2021-02-09 09:23:39
27	53	9	\N	\N	155	\N	\N	\N	2	BANCO DE BATERÍA ABULTADAS, SIN RESPALDO	2021-02-09 09:23:51	2021-02-09 09:23:51
28	53	9	\N	\N	155	\N	\N	\N	2	BANCO DE BATERÍA ABULTADAS, SIN RESPALDO	2021-02-09 09:24:00	2021-02-09 09:24:00
29	53	9	\N	\N	155	\N	\N	\N	2	BANCO DE BATERÍA ABULTADAS, SIN RESPALDO	2021-02-09 09:24:10	2021-02-09 09:24:10
30	53	9	\N	\N	155	\N	\N	\N	2	BANCO DE BATERÍA ABULTADAS, SIN RESPALDO	2021-02-09 09:24:20	2021-02-09 09:24:20
31	53	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 09:25:05	2021-02-09 09:25:05
32	53	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 09:25:15	2021-02-09 09:25:15
33	53	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 09:25:23	2021-02-09 09:25:23
34	53	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 09:25:31	2021-02-09 09:25:31
35	53	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 09:25:41	2021-02-09 09:25:41
36	53	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 09:25:49	2021-02-09 09:25:49
37	53	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 09:25:57	2021-02-09 09:25:57
38	53	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 09:26:05	2021-02-09 09:26:05
39	53	3	\N	5TR	\N	\N	\N	\N	2	REQUIERE CONTACTOR	2021-02-09 09:26:44	2021-02-09 09:26:44
40	166	1	25kVA	\N	\N	\N	\N	\N	2	SE REQUIERE ARRANQUE, ALTERNADOR Y BATERÍA, CAMBIO DE ACEITE Y FILTROS.	2021-02-09 09:28:21	2021-02-09 09:28:21
41	166	9	\N	\N	200	\N	\N	\N	4	BATERÍAS HURTADAS, REQUIERE DE INSTALACIÓN DE NUEVOS BANCOS DE BATERIA	2021-02-09 09:29:19	2021-02-09 09:29:19
42	166	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 09:29:50	2021-02-09 09:29:50
43	166	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 09:29:59	2021-02-09 09:29:59
44	166	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 09:29:59	2021-02-09 09:29:59
45	166	3	\N	5TR	\N	\N	\N	\N	2	SE REQUIERE INSTALACIÓN DOS AA 5TR	2021-02-09 09:30:42	2021-02-09 09:30:42
46	166	3	\N	5TR	\N	\N	\N	\N	2	SE REQUIERE INSTALACIÓN DOS AA 5TR	2021-02-09 09:30:52	2021-02-09 09:30:52
47	179	1	25KVA	\N	\N	\N	\N	\N	2	SE REQUIERE ARRANQUE, ALTERNADOR Y BATERÍA, CAMBIO DE ACEITE Y FILTROS, CAMBIO DE ATS	2021-02-09 09:32:54	2021-02-09 09:32:54
48	179	9	\N	\N	200	\N	\N	\N	4	SE REQUIERE NUEVOS BANCO DE BATERIA	2021-02-09 09:33:25	2021-02-09 09:33:25
49	179	9	\N	\N	200	\N	\N	\N	1	SE REQUIERE DE NUEVO BANCO DE BATERIA	2021-02-09 09:33:42	2021-02-09 09:33:42
50	179	9	\N	\N	200	\N	\N	\N	2	SE REQUIERE BANCO DE BATERIA	2021-02-09 09:34:05	2021-02-09 09:34:05
51	14	9	\N	\N	200	\N	\N	\N	4	Los dos banco de baterías fueron hurtado y notificado en el ticket # 107425.	2021-02-09 09:50:15	2021-02-09 09:50:15
52	14	1	25	\N	\N	\N	\N	\N	4	El motogenerador fue hurtado y notificado en el ticket #107425.	2021-02-09 09:53:30	2021-02-09 09:53:30
53	179	2	\N	\N	\N	\N	\N	\N	3	\N	2021-02-09 09:55:49	2021-02-09 09:55:49
54	179	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 09:56:00	2021-02-09 09:56:00
55	179	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 09:56:09	2021-02-09 09:56:09
56	179	3	\N	5TR	\N	\N	\N	\N	2	SE REQUIERE LA INSTALACIÓN DE 2 A/A DE 5TR	2021-02-09 09:56:47	2021-02-09 09:56:47
57	192	1	25KVA	\N	\N	\N	\N	\N	2	SE REQUIERE ARRANQUE, ALTERNADOR Y BATERÍA, CAMBIO DE ACEITE Y FILTROS, CAMBIO DE ATS	2021-02-09 09:59:01	2021-02-09 09:59:01
58	14	2	\N	\N	\N	\N	\N	\N	1	Tiene tres rectificadores operativos y uno fuera de servicio. Requiere la sustitución o reparación de un rectificador DELTA DPR 1600B-48.	2021-02-09 09:59:05	2021-02-09 09:59:05
59	192	9	\N	\N	200	\N	\N	\N	4	BATERÍAS HURTADAS	2021-02-09 09:59:40	2021-02-09 09:59:40
60	192	2	\N	\N	\N	\N	\N	\N	3	\N	2021-02-09 10:00:04	2021-02-09 10:00:04
61	192	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 10:00:14	2021-02-09 10:00:14
62	192	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 10:00:22	2021-02-09 10:00:22
63	192	3	\N	5TR	\N	\N	\N	\N	1	\N	2021-02-09 10:00:40	2021-02-09 10:00:40
64	192	3	\N	5TR	\N	\N	\N	\N	1	\N	2021-02-09 10:00:49	2021-02-09 10:00:49
65	21	1	25KVA	\N	\N	\N	\N	\N	1	SE REQUIERE ATS Y BATERÍA DE ARRANQUE	2021-02-09 10:01:48	2021-02-09 10:01:48
66	21	1	25KVA	\N	\N	\N	\N	\N	2	SE REQUIERE ATS Y BATERÍA DE ARRANQUE	2021-02-09 10:02:15	2021-02-09 10:02:15
67	21	9	\N	\N	200	\N	\N	\N	2	ABULTAMIENTO EN LAS BATERÍAS	2021-02-09 10:03:12	2021-02-09 10:03:12
68	21	9	\N	\N	200	\N	\N	\N	2	ABULTAMIENTO EN LAS BATERÍAS	2021-02-09 10:03:55	2021-02-09 10:03:55
69	14	3	\N	3	\N	\N	\N	\N	2	El equipo se encuentra fuera de servicio.	2021-02-09 10:08:12	2021-02-09 10:08:12
70	14	3	\N	3	\N	\N	\N	\N	2	El equipo se encuentra fuera de servicio.	2021-02-09 10:08:49	2021-02-09 10:08:49
71	21	3	\N	5TR	\N	\N	\N	\N	2	\N	2021-02-09 10:09:08	2021-02-09 10:09:08
72	21	3	\N	5TR	\N	\N	\N	\N	2	\N	2021-02-09 10:09:19	2021-02-09 10:09:19
73	21	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 10:10:09	2021-02-09 10:10:09
74	21	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 10:10:20	2021-02-09 10:10:20
75	149	1	450	\N	\N	\N	\N	\N	1	\N	2021-02-09 10:12:31	2021-02-09 10:12:31
76	149	9	\N	\N	1500	\N	\N	\N	1	\N	2021-02-09 10:12:47	2021-02-09 10:12:47
77	149	9	\N	\N	1500	\N	\N	\N	1	\N	2021-02-09 10:12:56	2021-02-09 10:12:56
78	149	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 10:13:05	2021-02-09 10:13:05
79	149	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 10:13:22	2021-02-09 10:13:22
80	149	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 10:14:03	2021-02-09 10:14:03
81	149	3	\N	5TR	\N	\N	\N	\N	1	\N	2021-02-09 10:14:36	2021-02-09 10:14:36
82	149	3	\N	5TR	\N	\N	\N	\N	1	\N	2021-02-09 10:14:47	2021-02-09 10:14:47
83	35	1	76	\N	\N	\N	\N	\N	1	El motogenerador se encuentra operativo.	2021-02-09 10:14:57	2021-02-09 10:14:57
84	35	3	\N	5	\N	\N	\N	\N	1	Se encuentra un equipo operativo y un equipo fuera de servicio.	2021-02-09 10:17:55	2021-02-09 10:17:55
85	151	1	75	\N	\N	\N	\N	\N	2	ATS DAÑADO	2021-02-09 10:19:32	2021-02-09 10:19:32
86	151	9	\N	\N	1100	\N	\N	\N	1	\N	2021-02-09 10:20:00	2021-02-09 10:20:00
87	35	9	\N	\N	155	\N	\N	\N	1	Tienen poco tiempo de respaldo.	2021-02-09 10:20:35	2021-02-09 10:20:35
88	151	2	\N	\N	\N	\N	\N	\N	1	LORAIN DE 200AMP	2021-02-09 10:20:44	2021-02-09 10:20:44
89	151	2	\N	\N	\N	\N	\N	\N	1	LORAIN 200	2021-02-09 10:21:15	2021-02-09 10:21:15
90	151	2	\N	\N	\N	\N	\N	\N	1	LORAIN 200	2021-02-09 10:21:30	2021-02-09 10:21:30
91	35	2	\N	\N	\N	\N	\N	\N	1	Los rectificadores se encuentran operativos.	2021-02-09 10:22:22	2021-02-09 10:22:22
92	151	3	\N	5TR	\N	\N	\N	\N	1	\N	2021-02-09 10:23:00	2021-02-09 10:23:00
93	35	3	\N	5	\N	\N	\N	\N	1	\N	2021-02-09 10:25:03	2021-02-09 10:25:03
94	35	9	\N	\N	155	\N	\N	\N	1	Tienen poco tiempo de respaldo.	2021-02-09 10:32:29	2021-02-09 10:32:29
95	35	2	\N	\N	\N	\N	\N	\N	2	Se encuentra fuera de servicio.	2021-02-09 10:34:11	2021-02-09 10:34:11
96	35	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 10:34:34	2021-02-09 10:34:34
97	35	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 10:35:10	2021-02-09 10:35:10
98	35	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 10:35:31	2021-02-09 10:35:31
99	35	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 10:35:49	2021-02-09 10:35:49
100	35	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 10:36:07	2021-02-09 10:36:07
101	14	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 10:45:22	2021-02-09 10:45:22
102	14	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 10:45:36	2021-02-09 10:45:36
103	14	2	\N	\N	\N	\N	\N	\N	2	\N	2021-02-09 10:45:51	2021-02-09 10:45:51
104	14	9	\N	\N	200	\N	\N	\N	4	El banco de bateria fue huratado, notificado en el ticket # 107425.	2021-02-09 10:48:20	2021-02-09 10:48:20
105	55	1	25	\N	\N	\N	\N	\N	2	El motogenerador se encuentra fuera de servicio.	2021-02-09 10:51:28	2021-02-09 10:51:28
106	55	9	\N	\N	250	\N	\N	\N	3	El banco de batería se encuentra dañado.	2021-02-09 10:53:19	2021-02-09 10:53:19
107	55	9	\N	\N	250	\N	\N	\N	3	El banco de baterias se encuentra dañada.	2021-02-09 10:54:17	2021-02-09 10:54:17
108	55	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 10:55:35	2021-02-09 10:55:35
109	55	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 10:55:51	2021-02-09 10:55:51
110	55	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 10:56:44	2021-02-09 10:56:44
111	55	2	\N	\N	\N	\N	\N	\N	2	Tiene un rectificador inoperativo.	2021-02-09 10:57:50	2021-02-09 10:57:50
112	55	3	\N	5	\N	\N	\N	\N	1	\N	2021-02-09 10:59:08	2021-02-09 10:59:08
113	55	3	\N	5	\N	\N	\N	\N	1	\N	2021-02-09 10:59:47	2021-02-09 10:59:47
114	67	1	25	\N	\N	\N	\N	\N	3	Se encuentra dañada por hurto y vandalismo, el motogenerador tiene condiciones que puede ser recuperable.	2021-02-09 11:11:20	2021-02-09 11:11:20
115	67	3	\N	5	\N	\N	\N	\N	8	El equipo fue vandalizado.	2021-02-09 11:12:59	2021-02-09 11:12:59
116	67	3	\N	5	\N	\N	\N	\N	8	El equipo fue vandalizado.	2021-02-09 11:14:01	2021-02-09 11:14:01
117	67	9	\N	\N	200	\N	\N	\N	3	\N	2021-02-09 11:15:28	2021-02-09 11:15:28
118	67	9	\N	\N	200	\N	\N	\N	3	\N	2021-02-09 11:15:56	2021-02-09 11:15:56
119	67	9	\N	\N	200	\N	\N	\N	3	\N	2021-02-09 11:16:23	2021-02-09 11:16:23
120	67	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 11:17:09	2021-02-09 11:17:09
121	67	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 11:17:21	2021-02-09 11:17:21
122	67	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 11:17:37	2021-02-09 11:17:37
123	67	2	\N	\N	\N	\N	\N	\N	2	El rectificador se encuentra inoperativo.	2021-02-09 11:19:37	2021-02-09 11:19:37
124	96	1	25	\N	\N	\N	\N	\N	6	\N	2021-02-09 11:24:11	2021-02-09 11:24:11
125	96	3	\N	3	\N	\N	\N	\N	6	\N	2021-02-09 11:35:44	2021-02-09 11:35:44
126	96	3	\N	3	\N	\N	\N	\N	6	\N	2021-02-09 11:35:58	2021-02-09 11:35:58
127	96	9	\N	\N	200	\N	\N	\N	6	\N	2021-02-09 11:40:08	2021-02-09 11:40:08
128	96	9	\N	\N	200	\N	\N	\N	6	\N	2021-02-09 11:40:30	2021-02-09 11:40:30
129	96	2	\N	\N	\N	\N	\N	\N	6	\N	2021-02-09 11:46:35	2021-02-09 11:46:35
130	96	2	\N	\N	\N	\N	\N	\N	6	\N	2021-02-09 11:46:50	2021-02-09 11:46:50
131	96	2	\N	\N	\N	\N	\N	\N	5	\N	2021-02-09 11:47:20	2021-02-09 11:47:20
132	96	2	\N	\N	\N	\N	\N	\N	6	\N	2021-02-09 11:47:39	2021-02-09 11:47:39
133	84	9	\N	\N	2200	\N	\N	\N	1	\N	2021-02-09 11:59:41	2021-02-09 11:59:41
134	117	2	\N	\N	\N	\N	\N	\N	2	\N	2021-02-09 13:40:06	2021-02-09 13:40:06
135	117	2	\N	\N	\N	\N	\N	\N	2	\N	2021-02-09 13:40:35	2021-02-09 13:40:35
136	117	2	\N	\N	\N	\N	\N	\N	2	\N	2021-02-09 13:41:04	2021-02-09 13:41:04
137	117	2	\N	\N	\N	\N	\N	\N	2	\N	2021-02-09 13:42:15	2021-02-09 13:42:15
138	117	2	\N	\N	\N	\N	\N	\N	2	\N	2021-02-09 13:42:35	2021-02-09 13:42:35
139	117	2	\N	\N	\N	\N	\N	\N	2	\N	2021-02-09 13:42:53	2021-02-09 13:42:53
140	117	3	\N	5	\N	\N	\N	\N	4	\N	2021-02-09 13:44:19	2021-02-09 13:44:19
141	117	1	50	\N	\N	\N	\N	\N	5	\N	2021-02-09 13:51:08	2021-02-09 13:51:08
142	123	3	\N	3	\N	\N	\N	\N	6	\N	2021-02-09 13:57:18	2021-02-09 13:57:18
143	123	3	\N	3	\N	\N	\N	\N	6	\N	2021-02-09 13:57:40	2021-02-09 13:57:40
144	123	2	\N	\N	\N	\N	\N	\N	2	\N	2021-02-09 14:21:16	2021-02-09 14:21:16
145	123	2	\N	\N	\N	\N	\N	\N	6	\N	2021-02-09 14:21:44	2021-02-09 14:21:44
146	123	2	\N	\N	\N	\N	\N	\N	6	\N	2021-02-09 14:22:01	2021-02-09 14:22:01
147	123	2	\N	\N	\N	\N	\N	\N	6	\N	2021-02-09 14:22:17	2021-02-09 14:22:17
148	123	9	\N	\N	200	\N	\N	\N	6	\N	2021-02-09 14:22:58	2021-02-09 14:22:58
149	123	9	\N	\N	200	\N	\N	\N	6	\N	2021-02-09 14:23:16	2021-02-09 14:23:16
150	123	9	\N	\N	200	\N	\N	\N	6	\N	2021-02-09 14:23:56	2021-02-09 14:23:56
151	123	1	25	\N	\N	\N	\N	\N	6	Este nodo se encuentra bypasseado.	2021-02-09 14:25:11	2021-02-09 14:25:11
152	130	1	25	\N	\N	\N	\N	\N	2	El motogenerador se encuentra fuera de servicio.	2021-02-09 14:30:39	2021-02-09 14:30:39
153	130	3	\N	3	\N	\N	\N	\N	3	El equipo se encuentra fuera de servicio por daños vandálicos. El equipo presenta  condiciones recuperables.	2021-02-09 14:43:28	2021-02-09 14:43:28
154	130	3	\N	3	\N	\N	\N	\N	3	El equipo se encuentra fuera de servicio por daños vandálicos. El equipo presenta  condiciones recuperables.	2021-02-09 14:44:02	2021-02-09 14:44:02
155	130	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 14:45:32	2021-02-09 14:45:32
156	130	2	\N	\N	\N	\N	\N	\N	2	\N	2021-02-09 14:45:46	2021-02-09 14:45:46
157	130	2	\N	\N	\N	\N	\N	\N	2	\N	2021-02-09 14:46:21	2021-02-09 14:46:21
158	130	9	\N	\N	200	\N	\N	\N	4	\N	2021-02-09 14:47:50	2021-02-09 14:47:50
159	130	9	\N	\N	200	\N	\N	\N	4	\N	2021-02-09 14:48:37	2021-02-09 14:48:37
160	130	2	\N	\N	\N	\N	\N	\N	2	\N	2021-02-09 14:49:21	2021-02-09 14:49:21
161	142	1	50	\N	\N	\N	\N	\N	5	\N	2021-02-09 14:54:39	2021-02-09 14:54:39
162	142	3	\N	3	\N	\N	\N	\N	4	\N	2021-02-09 14:56:04	2021-02-09 14:56:04
163	142	3	\N	3	\N	\N	\N	\N	4	\N	2021-02-09 14:56:41	2021-02-09 14:56:41
164	142	9	\N	\N	250	\N	\N	\N	4	\N	2021-02-09 14:57:30	2021-02-09 14:57:30
165	142	9	\N	\N	250	\N	\N	\N	4	\N	2021-02-09 14:57:54	2021-02-09 14:57:54
166	142	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 14:58:45	2021-02-09 14:58:45
167	142	2	\N	\N	\N	\N	\N	\N	2	\N	2021-02-09 14:59:02	2021-02-09 14:59:02
168	142	2	\N	\N	\N	\N	\N	\N	2	\N	2021-02-09 14:59:21	2021-02-09 14:59:21
169	142	2	\N	\N	\N	\N	\N	\N	2	\N	2021-02-09 14:59:48	2021-02-09 14:59:48
170	161	1	25	\N	\N	\N	\N	\N	3	El motogenerador está dañado por actos vandalicos, este se encuentra en condiciones recuperables.	2021-02-09 15:06:35	2021-02-09 15:06:35
171	161	3	\N	3	\N	\N	\N	\N	3	El equipo se encuentra fuera de servicio por daños vandálicos, esté presenta  condiciones recuperables.	2021-02-09 15:08:21	2021-02-09 15:08:21
172	161	3	\N	3	\N	\N	\N	\N	3	El equipo se encuentra fuera de servicio por daños vandálicos, esté presenta  condiciones recuperables.	2021-02-09 15:09:38	2021-02-09 15:09:38
173	161	9	\N	\N	200	\N	\N	\N	4	\N	2021-02-09 15:11:19	2021-02-09 15:11:19
174	161	9	\N	\N	200	\N	\N	\N	4	\N	2021-02-09 15:12:14	2021-02-09 15:12:14
175	161	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 15:21:52	2021-02-09 15:21:52
176	161	2	\N	\N	\N	\N	\N	\N	1	\N	2021-02-09 15:22:17	2021-02-09 15:22:17
177	161	2	\N	\N	\N	\N	\N	\N	2	\N	2021-02-09 15:22:38	2021-02-09 15:22:38
178	161	2	\N	\N	\N	\N	\N	\N	2	\N	2021-02-09 15:22:56	2021-02-09 15:22:56
179	190	1	25	\N	\N	\N	\N	\N	6	\N	2021-02-09 15:27:40	2021-02-09 15:27:40
180	190	3	\N	3	\N	\N	\N	\N	6	\N	2021-02-09 15:29:06	2021-02-09 15:29:06
181	190	3	\N	3	\N	\N	\N	\N	6	\N	2021-02-09 15:29:21	2021-02-09 15:29:21
182	197	1	25	\N	\N	\N	\N	\N	5	\N	2021-02-10 08:53:10	2021-02-10 08:53:10
183	197	9	\N	\N	200	\N	\N	\N	4	\N	2021-02-10 08:53:39	2021-02-10 08:53:39
184	197	9	\N	\N	200	\N	\N	\N	4	\N	2021-02-10 08:54:01	2021-02-10 08:54:01
185	197	2	\N	\N	\N	\N	\N	\N	6	\N	2021-02-10 08:54:59	2021-02-10 08:54:59
186	197	2	\N	\N	\N	\N	\N	\N	6	\N	2021-02-10 08:55:15	2021-02-10 08:55:15
187	197	2	\N	\N	\N	\N	\N	\N	6	\N	2021-02-10 08:55:33	2021-02-10 08:55:33
188	197	2	\N	\N	\N	\N	\N	\N	6	\N	2021-02-10 08:55:51	2021-02-10 08:55:51
189	197	3	\N	5	\N	\N	\N	\N	5	\N	2021-02-10 08:58:24	2021-02-10 08:58:24
190	197	3	\N	5	\N	\N	\N	\N	5	\N	2021-02-10 08:58:40	2021-02-10 08:58:40
191	7	3	\N	3	\N	\N	\N	\N	5	La Unidad Condensadora fue vandalizada.	2021-02-10 09:03:29	2021-02-10 09:03:29
192	7	3	\N	3	\N	\N	\N	\N	5	La Unidad Condensadora fue vandalizada.	2021-02-10 09:03:46	2021-02-10 09:03:46
193	53	1	58	\N	\N	\N	\N	\N	1	\N	2021-03-09 14:26:32	2021-03-09 14:26:32
\.


--
-- Data for Name: equipo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.equipo (id, descripcion, localidad_id, responsable_id, servicio_id, created_at, updated_at) FROM stdin;
1	MOTORGENERADOR	1	1	1	\N	\N
2	CUADRO DE FUERZA	1	1	1	\N	\N
4	UMA	1	1	2	\N	\N
5	CHILLER	1	1	2	\N	\N
7	UPS	1	1	1	\N	\N
9	BANCO DE BATERIAS	1	1	1	\N	\N
3	AIRE ACONDICIONADO	1	1	2	\N	2021-03-18 12:59:31
11	CARGADOR DE BATERIAS	1	1	\N	2021-03-18 13:05:05	2021-03-18 13:05:05
12	DESCARGA DE BATERIAS	1	1	\N	2021-03-18 13:05:05	2021-03-18 13:05:05
13	DESTILACION DE AGUA	1	1	\N	2021-03-18 13:05:05	2021-03-18 13:05:05
14	EXTRACTOR	1	1	\N	2021-03-18 13:05:05	2021-03-18 13:05:05
15	INVERSOR	1	1	\N	2021-03-18 13:05:05	2021-03-18 13:05:05
16	LVD	1	1	\N	2021-03-18 13:05:05	2021-03-18 13:05:05
17	MOTOR EN SERVICIO	1	1	\N	2021-03-18 13:05:05	2021-03-18 13:05:05
18	OPTO22	1	1	\N	2021-03-18 13:05:05	2021-03-18 13:05:05
19	RECTIFICADOR	1	1	\N	2021-03-18 13:05:05	2021-03-18 13:05:05
20	SISTEMA DE PUESTA A TIERRA(SPT)	1	1	\N	2021-03-18 13:05:05	2021-03-18 13:05:05
21	TABLERO PRINCIPAL(AC)	1	1	\N	2021-03-18 13:05:05	2021-03-18 13:05:05
22	TRANSFORMADOR	1	1	\N	2021-03-18 13:05:05	2021-03-18 13:05:05
23	VARIAS(A/A)	1	1	\N	2021-03-18 13:05:05	2021-03-18 13:05:05
24	VARIOS(EX)	1	1	\N	2021-03-18 13:05:05	2021-03-18 13:05:05
\.


--
-- Data for Name: estados; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.estados (id, nombre, region_id, created_at, updated_at) FROM stdin;
1	TACHIRA	1	\N	\N
2	TRUJILLO	1	\N	\N
3	BARINAS	1	\N	\N
4	MERIDA	1	\N	\N
5	CAPITAL OESTE	2	\N	\N
6	CAPITAL ESTE	2	\N	\N
7	CAPITAL CENTRO	2	\N	\N
8	LA GUAIRA	2	\N	\N
9	MIRANDA	2	\N	\N
10	APURE	3	\N	\N
11	CARABOBO	3	\N	\N
12	COJEDES	3	\N	\N
13	GUARICO	3	\N	\N
14	ARAGUA	3	\N	\N
15	FALCON	4	\N	\N
16	PORTUGUESA	4	\N	\N
17	LARA	4	\N	\N
18	YARACUY	4	\N	\N
19	BOLIVAR ESTE	5	\N	\N
20	BOLIVAR OESTE	5	\N	\N
21	BOLIVAR	5	\N	\N
22	AMAZONAS	5	\N	\N
26	MONAGAS	7	\N	\N
27	NUEVA ESPARTA	7	\N	\N
28	ANZOATEGUI	7	\N	\N
29	SUCRE	7	\N	\N
32	SUR DEL LAGO	6	2021-01-26 10:03:22	2021-01-26 10:03:22
33	COSTA ORIENTAL DEL LAGO	6	2021-01-26 10:03:22	2021-01-26 10:03:22
30	ZULIA/MARACAIBO	6	2021-01-26 10:03:22	2021-03-03 14:19:22
31	ZULIA/MARACAIBO (EJE GUAJIRA)	6	2021-01-26 10:03:22	2021-03-03 14:19:45
34	DELTA AMACURO	5	2021-02-04 12:26:46	2021-03-03 14:27:17
\.


--
-- Data for Name: failed_jobs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.failed_jobs (id, uuid, connection, queue, payload, exception, failed_at) FROM stdin;
\.


--
-- Data for Name: fallas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.fallas (id, descripcion, tipo, componente_id, created_at, updated_at) FROM stdin;
1	BATERIA DE ARRANQUE DAÑADA	BATERIA DE ARRANQUE DAÑADA	1	\N	\N
2	CELDA DANADA	CELDA DANADA	1	\N	\N
3	ALTO VOLTAJE	ALTO VOLTAJE	1	\N	\N
4	BAJO VOLTAJE	BAJO VOLTAJE	1	\N	\N
5	TIEMPO DE VIDA UTIL	TIEMPO DE VIDA UTIL	1	\N	\N
6	DERRAME DE ACIDO	DERRAME DE ACIDO	1	\N	\N
7	BORNES DAÑADOS	BORNES DAÑADOS	1	\N	\N
8	BAJO NIVEL DE ELECTROLITOS	BAJO NIVEL DE ELECTROLITOS	1	\N	\N
9	BAJO NIVEL DE COMBUSTIBLE	BAJO NIVEL DE COMBUSTIBLE	2	\N	\N
10	FLOTANTE DAÑADO	FLOTANTE DAÑADO	2	\N	\N
11	OBSTRUCCION DE FILTRO	OBSTRUCCION DE FILTRO	2	\N	\N
12	BAJO NIVEL DE COMBUSTIBLE	BAJO NIVEL DE COMBUSTIBLE	3	\N	\N
13	FLOTANTE DANADA	FLOTANTE DANADA	3	\N	\N
14	OBSTRUCCION DE FILTRO	OBSTRUCCION DE FILTRO	3	\N	\N
15	OBSTRUCCION DE FILTRO	OBSTRUCCION DE FILTRO	4	\N	\N
16	BAJO NIVEL DE ACEITE	BAJO NIVEL DE ACEITE	4	\N	\N
17	DETERIORO EN MANGUERAS	DETERIORO EN MANGUERAS	5	\N	\N
18	ATS DANADA	ATS DANADA	6	\N	\N
19	RADIADOR DANADA	RADIADOR DANADA	7	\N	\N
20	OBSTRUCCION	OBSTRUCCION	8	\N	\N
21	ELECTRICA	ELECTRICA	9	\N	\N
22	MECANICA	MECANICA	9	\N	\N
23	ESTATOR	ESTATOR	9	\N	\N
24	ROTOR	ROTOR	9	\N	\N
25	EXITATRIS	EXITATRIS	9	\N	\N
26	ELECTRICA	ELECTRICA	10	\N	\N
27	RECTIFICADOR DAÑADO	RECTIFICADOR DAÑADO	11	\N	\N
28	TARJETA CONTROLADORA DANADA	TARJETA CONTROLADORA DANADA	11	\N	\N
29	ELECTRICA	ELECTRICA	12	\N	\N
30	CELDAS DANADAS	CELDAS DANADAS	13	\N	\N
31	DERRAME DE ACIDO	DERRAME DE ACIDO	13	\N	\N
32	FUSIBLE	FUSIBLE	13	\N	\N
33	BAJO NIVEL DE ELECTROLITOS	BAJO NIVEL DE ELECTROLITOS	13	\N	\N
34	MECANICA	MECANICA	14	\N	\N
35	ELECTRICA	ELECTRICA	14	\N	\N
36	ELECTRICA	ELECTRICA	15	\N	\N
37	MECANICA	MECANICA	15	\N	\N
38	CORREA DANADA	CORREA DANADA	16	\N	\N
39	FUGA DE REFRIGERANTE	FUGA DE REFRIGERANTE	16	\N	\N
40	CORREA DANADA	CORREA DANADA	17	\N	\N
41	FUGA DE REFRIGERANTE	FUGA DE REFRIGERANTE	17	\N	\N
42	OBSTRUCCION DE FILTRO DE AIRE	OBSTRUCCION DE FILTRO DE AIRE	17	\N	\N
43	ELECTRICA	ELECTRICA	18	\N	\N
44	OBSTRUCCION	OBSTRUCCION	19	\N	\N
45	OBSTRUCCION DE DISIPADOR DE AIRE	OBSTRUCCION DE DISIPADOR DE AIRE	19	\N	\N
46	OBSTRUCCION O QUEMADO	OBSTRUCCION O QUEMADO	20	\N	\N
47	OBSTRUCCION	OBSTRUCCION	21	\N	\N
48	ELECTRICA	ELECTRICA	22	\N	\N
49	OBSTRUCCION	OBSTRUCCION	23	\N	\N
50	MECANICA	MECANICA	24	\N	\N
51	ELECTRICA	ELECTRICA	24	\N	\N
52	ELECTRICA	ELECTRICA	25	\N	\N
53	OBSTRUCCION DE TUBERIA	OBSTRUCCION DE TUBERIA	26	\N	\N
54	OBSTRUCCION DE FILTRO	OBSTRUCCION DE FILTRO	26	\N	\N
55	ELECTRICA	ELECTRICA	26	\N	\N
56	MECANICA	MECANICA	26	\N	\N
57	ELECTRICA	ELECTRICA	27	\N	\N
58	MECANICA	MECANICA	27	\N	\N
59	OBSTRUCCION	OBSTRUCCION	28	\N	\N
60	FALLA DE CHUMACERA	FALLA DE CHUMACERA	29	\N	\N
61	BAJA PRESION	BAJA PRESION	30	\N	\N
62	MECANICA	MECANICA	30	\N	\N
63	FUGA	FUGA	30	\N	\N
64	ELECTRICA	ELECTRICA	31	\N	\N
65	OBSTRUCCION DE TUBERIA	OBSTRUCCION DE TUBERIA	32	\N	\N
\.


--
-- Data for Name: generador; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.generador (id, marca, modelo, serial, inventario_cant, cap_kva, tablero, status_id, created_at, updated_at) FROM stdin;
2	Cummins/Stanford	P1734C	X08L470328	5033325	1875	No	\N	2021-01-12 12:35:29	2021-01-12 12:35:29
3	Cummins/Stanford	P1734C	X08L470328	5033325	1875	No	\N	2021-01-12 12:35:38	2021-01-12 12:35:38
1	Cummins/Stanford	P1734C	X08L470328	5033325	1875	Si	\N	2021-01-12 12:29:07	2021-01-12 12:46:14
4	Cummins/Stanford	P1734C	X08L470326	5033393	1875	No	\N	2021-01-12 12:53:01	2021-01-12 12:53:01
5	Cummins	2121321	no posee	213123	123	Si	\N	2021-01-12 13:36:01	2021-01-12 13:36:01
6	Cummins/Stanford	HCI634J	X08H340943	5033389	1250	No	\N	2021-01-12 13:42:58	2021-01-12 13:42:58
8	ONAN	HCI634J1	X07L486522	5061763	1300	Si	\N	2021-01-12 14:35:46	2021-01-12 14:40:24
65	STAMFORD	UCI274E	no posee	506770	165	Si	\N	2021-02-12 08:12:48	2021-02-12 08:12:48
9	Cummins	HCI634J	X10D160936	5033385	1250	Si	\N	2021-01-12 15:46:05	2021-01-12 15:46:05
7	ONAN	300.ODFM-17R/9930M	D830658138	3108962	375	Si	\N	2021-01-12 14:25:41	2021-01-12 15:59:07
10	Kohler Detroit	800ROZD4	751661	3125563	1019	Si	\N	2021-01-13 13:38:03	2021-01-13 13:38:03
11	Seleccione una marca	LSA491S4	166631	6579306	750	Si	\N	2021-01-13 14:09:13	2021-01-13 14:09:13
12	LEROY SOMER	LSA 472 M736/4	23010711	3126694	625	No	\N	2021-01-20 10:06:43	2021-01-20 10:06:43
13	STAMFORD	UC1224E14	065705	6411463	65	No	\N	2021-01-20 10:17:56	2021-01-20 10:17:56
14	STAMFORD	UC1224E14	065705	6411463	65	No	\N	2021-01-20 10:19:20	2021-01-20 10:19:20
15	STAMFORD	UC1224E13	no posee	507108	150	Si	\N	2021-01-20 10:36:09	2021-01-20 10:36:09
16	Seleccione una marca	SIN INFORMACION	no posee	5033489	25	No	\N	2021-01-20 10:47:07	2021-01-20 10:47:07
17	Seleccione una marca	SIN INFORMACION	no posee	5033466	25	No	\N	2021-01-20 10:53:44	2021-01-20 10:53:44
18	Seleccione una marca	SIN INFORMACION	no posee	SIN INFORMACION	25	No	\N	2021-01-20 11:04:50	2021-01-20 11:04:50
19	Seleccione una marca	SIN INFORMACION	no posee	SIN INFORMACION	25	No	\N	2021-01-20 11:10:49	2021-01-20 11:10:49
20	MARATHON	SIN INFORMACION	316NSL1601	5060380	50	No	\N	2021-01-20 12:33:10	2021-01-20 12:33:10
21	MARATHON	SIN INFORMACION	316NSL1601	5060380	50	No	\N	2021-01-20 12:33:11	2021-01-20 12:33:11
22	MARATHON	SIN INFORMACION	316NSL1601	5060380	50	No	\N	2021-01-20 12:33:11	2021-01-20 12:33:11
23	Seleccione una marca	SIN INFORMACION	2500DFM17R-25421M	507297	151	No	\N	2021-01-20 12:54:48	2021-01-20 12:54:48
24	STAMFORD	SC534C	P9466/1	506694	337	No	\N	2021-01-22 10:55:37	2021-01-22 10:55:37
26	Seleccione una marca	200BDFD	L780382490	3142531	250	Si	\N	2021-01-22 11:01:49	2021-01-22 11:01:49
27	Seleccione una marca	200BDFD	L780382490	3142531	250	Si	\N	2021-01-22 11:01:58	2021-01-22 11:01:58
28	STAMFORD	HC434E1	W5423/5	6411502	255	Si	\N	2021-01-22 11:02:37	2021-01-22 11:02:37
29	Seleccione una marca	SR4	8LF00838	3142573	400	Si	\N	2021-01-22 11:08:48	2021-01-22 11:08:48
30	Seleccione una marca	180-ROZ581	331473	3142686	225	Si	\N	2021-01-22 11:15:10	2021-01-22 11:15:10
31	Seleccione una marca	80REOZJB	2037841	5048928	100	Si	\N	2021-01-22 11:22:35	2021-01-22 11:22:35
32	STAMFORD	sc434e	p3400/1	3142615	255	Si	\N	2021-01-22 11:28:27	2021-01-22 11:28:27
33	STAMFORD	HC434E1	P3401/6	6412054	189	No	\N	2021-01-22 11:57:27	2021-01-22 11:57:27
34	STAMFORD	SC534C	P1764/4	6411625	337	No	\N	2021-01-22 12:09:50	2021-01-22 12:09:50
35	STAMFORD	SC534C	P1764/4	6411625	337	No	\N	2021-01-22 12:10:57	2021-01-22 12:10:57
36	STAMFORD	SC534C	P1764/4	6411625	337	No	\N	2021-01-22 12:12:55	2021-01-22 12:12:55
37	STAMFORD	HC434E1	18039/3	6581167	189	No	\N	2021-01-22 12:38:45	2021-01-22 12:38:45
38	STAMFORD	HC434E1	18039/3	6581167	189	No	\N	2021-01-22 12:39:19	2021-01-22 12:39:19
39	STAMFORD	HC434E1	18039/3	6581167	189	No	\N	2021-01-22 12:40:15	2021-01-22 12:40:15
40	STAMFORD	UC1274E1	C085830/05	6411818	165	No	\N	2021-01-22 12:52:00	2021-01-22 12:52:00
41	STAMFORD	UC1274E1	C085830/05	6411818	165	No	\N	2021-01-22 12:52:34	2021-01-22 12:52:34
42	STAMFORD	UC1274E1	C085940/04	507431	165	No	\N	2021-01-26 10:51:18	2021-01-26 10:51:18
43	STAMFORD	SC534C	HC434E	6411983	255	Si	\N	2021-01-26 12:20:18	2021-01-26 12:20:18
44	STAMFORD	P1734C	X08L470328	5033325	1875	No	\N	2021-01-27 11:57:28	2021-01-27 11:57:28
45	STAMFORD	P1734C	X08L470328	5033325	1875	No	\N	2021-01-27 11:57:43	2021-01-27 11:57:43
46	STAMFORD	S5024078	no posee	3135935	189	Si	\N	2021-01-28 08:13:17	2021-01-28 08:13:17
47	STAMFORD	S5024078	no posee	3135935	189	Si	\N	2021-01-28 08:13:50	2021-01-28 08:13:50
48	STAMFORD	S5024078	no posee	3135935	189	Si	\N	2021-01-28 08:16:35	2021-01-28 08:16:35
49	LEROY SOMER	47.2VS3 J 6/4	220758/7	000505616	500	Si	\N	2021-01-29 13:16:31	2021-01-29 13:16:31
50	STAMFORD	B.5.500/99	L7006/2	3118003	255	No	\N	2021-02-01 11:19:31	2021-02-01 11:19:31
51	STAMFORD	NT-855-G1	23204398	3135041	189	Si	\N	2021-02-01 11:27:45	2021-02-01 11:27:45
52	STAMFORD	SC534C	SC434E	507202	255	No	\N	2021-02-01 11:38:05	2021-02-01 11:38:05
53	LEROY SOMER	SDMO	EJ084470/02	3118004	315	Si	\N	2021-02-01 11:47:16	2021-02-01 11:47:16
54	STAMFORD	HC434D	P3401/1	3135041	189	Si	\N	2021-02-10 12:53:37	2021-02-10 12:53:37
55	LEROY SOMER	LSA40M5	324186/26	MV00958718	23	No	\N	2021-02-10 13:05:29	2021-02-10 13:05:29
56	LEROY SOMER	LSA40M5	324186/26	MV00958718	23	No	\N	2021-02-10 13:08:49	2021-02-10 13:08:49
57	STAMFORD	M08A30066302	S300663-02	0005053609	500	Si	\N	2021-02-10 14:19:07	2021-02-10 14:19:07
58	LEROY SOMER	J-80	no posee	5047837	100	No	\N	2021-02-10 16:45:48	2021-02-10 16:45:48
59	LEROY SOMER	46.2VL12J6/4	166631	3126376	375	Si	\N	2021-02-11 10:00:25	2021-02-11 10:00:25
60	Seleccione una marca	KOHLER 5M4027	0746033	3126016	450	Si	\N	2021-02-11 10:53:39	2021-02-11 10:53:39
61	LEROY SOMER	47.2M7 C 6/4	247573/1	3126013	630	Si	\N	2021-02-11 11:05:14	2021-02-11 11:05:14
62	LEROY SOMER	LSA47 44C 6/4	247510/2	6580690	500	Si	\N	2021-02-11 14:43:02	2021-02-11 14:43:02
63	LEROY SOMER	LSA47 44C 6/4	247510/2	6580690	500	Si	\N	2021-02-11 14:43:11	2021-02-11 14:43:11
64	LEROY SOMER	LSA47 44C 6/4	247510/2	6580690	500	Si	\N	2021-02-11 14:44:20	2021-02-11 14:44:20
66	STAMFORD	UCI274E	no posee	506770	165	Si	\N	2021-02-12 08:37:34	2021-02-12 08:37:34
67	STAMFORD	UCDI224C1	no posee	6292472	50	Si	\N	2021-02-12 09:09:05	2021-02-12 09:09:05
68	STAMFORD	UCDI224C1	no posee	6292472	50	Si	\N	2021-02-12 09:09:50	2021-02-12 09:09:50
69	STAMFORD	UCDI224C1	no posee	6292472	50	Si	\N	2021-02-12 09:19:29	2021-02-12 09:19:29
70	STAMFORD	UCDI224C1	no posee	6292472	50	Si	\N	2021-02-12 09:21:01	2021-02-12 09:21:01
71	STAMFORD	UCDI224C1	no posee	6292472	50	Si	\N	2021-02-12 10:02:13	2021-02-12 10:02:13
72	STAMFORD	UCDI224C1	no posee	6292472	50	Si	\N	2021-02-12 10:02:37	2021-02-12 10:02:37
73	STAMFORD	UCDI224C1	no posee	6292472	50	Si	\N	2021-02-12 10:20:33	2021-02-12 10:20:33
74	Seleccione una marca	KHOLER / 4P5	no posee	506699	50	Si	\N	2021-02-12 10:36:16	2021-02-12 10:36:16
75	Seleccione una marca	FARADAY / FD4LS1-4	K164400	5059407	400	No	\N	2021-02-12 11:06:16	2021-02-12 11:06:16
76	STAMFORD	UCDI224C1	no posee	6292472	50	Si	\N	2021-02-12 11:15:56	2021-02-12 11:15:56
77	STAMFORD	UCDI224C1	no posee	6292472	50	Si	\N	2021-02-12 11:17:41	2021-02-12 11:17:41
78	STAMFORD	UCDI224C1	no posee	6292472	50	Si	\N	2021-02-12 11:20:32	2021-02-12 11:20:32
79	Seleccione una marca	DOMOSA / SLB224F	no posee	6580866	76	Si	\N	2021-02-12 12:10:24	2021-02-12 12:10:24
80	Seleccione una marca	ONSITY ENERGY / 40BJF6D3	no posee	6292467	50	Si	\N	2021-02-12 15:40:17	2021-02-12 15:40:17
81	Seleccione una marca	DOMOSA / SLB224F	no posee	506739	76	Si	\N	2021-02-17 08:26:21	2021-02-17 08:26:21
82	STAMFORD	SLG224E-8093301	M08E31010002	5059295	76	Si	\N	2021-02-17 15:27:02	2021-02-17 15:27:02
83	STAMFORD	UC1224E	C0026440/04	1216116	65	No	\N	2021-02-17 15:40:27	2021-02-17 15:40:27
84	STAMFORD	UCD1224D1L788D	M05J086637	506797	65	Si	\N	2021-02-17 15:53:12	2021-02-17 15:53:12
85	LEROY SOMER	LSA 43 2S 15 C 6/4	277745 34	2027890	45	Si	\N	2021-02-23 11:36:39	2021-02-23 11:36:39
86	LEROY SOMER	LSA 43 2S 15 C 6 4	277745 34	2027890	45	Si	\N	2021-02-23 11:38:28	2021-02-23 11:38:28
87	LEROY SOMER	LSA 46.2.L6	289909/1	211324	312	Si	\N	2021-02-23 12:06:52	2021-02-23 12:06:52
88	STAMFORD	UCI224F1	X14H343457	64000394	75	Si	\N	2021-02-23 12:22:06	2021-02-23 12:22:06
89	LEROY SOMER	SIN INFORMACION	SIN INFORMACION	SIN INFORMACION	50	Si	\N	2021-02-23 14:06:13	2021-02-23 14:06:13
90	STAMFORD	SIN INFORMACION	no posee	SIN INFORMACION	50	Si	\N	2021-02-23 14:10:14	2021-02-23 14:10:14
91	MECC ALTER	ECO32-3S/4	1205163	5049543	50	Si	\N	2021-02-23 14:17:51	2021-02-23 14:17:51
92	ECC	BRF225	20971/4	3142915	52	Si	\N	2021-02-23 14:27:54	2021-02-23 14:27:54
93	ECC	BRF225	20971/3	31422912	52	Si	\N	2021-02-23 14:32:24	2021-02-23 14:32:24
94	ECC	BRF225	2097172	525048997	52	Si	\N	2021-02-23 14:53:10	2021-02-23 14:53:10
25	AEG	200BDFD	L780382490	3142531	250	Si	\N	2021-01-22 11:01:31	2021-02-23 14:55:21
95	STAMFORD	UC-274C	CO90162/01	3142796	125	Si	\N	2021-02-24 14:01:47	2021-02-24 14:01:47
96	STAMFORD	98132701	92744103	5049164	175	Si	\N	2021-02-24 14:38:20	2021-02-24 14:38:20
97	OLYMPIAN	60ROZ1	678701	5049429	78	Si	\N	2021-02-24 14:43:59	2021-02-24 14:43:59
98	LEROY SOMER	40VS2 C 8/4	SIN INFORMACION	SIN INFORMACION	14	Si	\N	2021-02-25 13:53:30	2021-02-25 13:53:30
99	Seleccione una marca	SIN INFORMACION	SIN INFORMACION	SIN INFORMACION	50	Si	\N	2021-02-25 14:14:40	2021-02-25 14:14:40
100	Seleccione una marca	SIN INFORMACION	SIN INFORMACION	SIN INFORMACION	50	Si	\N	2021-02-25 14:18:03	2021-02-25 14:18:03
101	Seleccione una marca	SIN INFORMACION	SIN INFORMACION	SIN INFORMACION	25	Si	\N	2021-02-25 14:23:59	2021-02-25 14:23:59
102	Seleccione una marca	SIN INFORMACION	SIN INFORMACION	SIN INFORMACION	25	Si	\N	2021-02-25 14:32:19	2021-02-25 14:32:19
104	LEROY SOMER	20R0ZJ	0681473	3138035	33	No	\N	2021-02-26 13:18:17	2021-02-26 13:18:17
105	LEROY SOMER	20R0ZJ	0681473	3138035	33	Si	\N	2021-03-01 12:32:41	2021-03-01 12:32:41
106	LEROY SOMER	20R0ZJ	0681473	3138035	33	Si	\N	2021-03-01 12:43:41	2021-03-01 12:43:41
107	LEROY SOMER	200RE0ZJB	2039899	3138106	250	Si	\N	2021-03-01 13:11:38	2021-03-01 13:11:38
103	AEG	20R0ZJ	0681473	3138035	33	Si	\N	2021-02-26 13:11:58	2021-03-01 13:13:11
108	AEG	DKVH318/04	73-463755	206457	240	Si	\N	2021-03-01 13:25:53	2021-03-01 13:25:53
109	LEROY SOMER	3406	no posee	sin informacion	400	No	\N	2021-03-18 10:37:17	2021-03-18 10:37:17
110	STAMFORD	HC43461	0028222/04	3135807	250	Si	\N	2021-03-18 11:20:08	2021-03-18 11:20:08
111	STAMFORD	HC434C1	0028222/04	3135007	250	Si	\N	2021-03-18 11:30:09	2021-03-18 11:30:09
112	STAMFORD	HC434C1	C028222/04	3135007	250	Si	\N	2021-03-18 11:56:57	2021-03-18 11:56:57
113	STAMFORD	HC434C1	0028222/04	3135007	250	No	\N	2021-03-18 12:10:34	2021-03-18 12:10:34
114	STAMFORD	hc434c1	0028222/04	3135007	250	Si	\N	2021-03-18 12:37:57	2021-03-18 12:37:57
115	CATERPILLAR	generic	no posee	355577	34	No	\N	2021-03-22 11:56:47	2021-03-22 11:56:47
116	CATERPILLAR	generic	no posee	3055999	250	Si	\N	2021-03-22 12:59:23	2021-03-22 12:59:23
117	CATERPILLAR	generic	no posee	3055999	25	No	\N	2021-03-22 13:01:56	2021-03-22 13:01:56
118	CATERPILLAR	5006	no posee	000000	400	No	\N	2021-03-22 17:29:29	2021-03-22 17:29:29
\.


--
-- Data for Name: invequipopsut; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.invequipopsut (id, id_2, equipo_id, nombre, descripcion, marca, modelo, created_at, updated_at) FROM stdin;
1	1	3	A/A COMPACT MARVAIR 5TON AVPA60	AIRE ACONDICIONADO COMPACT MARVAIR 5TON	MARVAIR	COMPACT I AVPA60A-CA	\N	\N
2	2	3	A/A COMPACT MARVAIR 3TON AVPA36	AIRE ACONDICIONADO COMPACT MARVAIR 3TON	MARVAIR	COMPACT I AVPA36A-CA	\N	\N
3	3	3	A/A SPLIT GENERICO 2TON	SPLIT GENERICO 2TON	GENERICO	AA-SPLIT-GEN-2TON	\N	\N
4	4	3	A/A COMPACTO GENERICO 10TON	A/A COMPACTO GENERICO 10TON	GENERICO	AA-COMPACT-GEN-10TON	\N	\N
5	5	3	A/A SPLIT CARRIER 3TON FXADNF	 AIRE ACONDICIONADO TIPO SPLIT CARRIER 3TON	CARRIER	FXADNF037000	\N	\N
6	6	3	A//A COMPACTO CARRIER 50ES-A-36	AIRE ACONDICIONADO COMPACTO CARRIER 3TON 50ES-A-36	CARRIER	50ES-A-36-30	\N	\N
7	7	3	A/A COMPACTO CARRIER 50ES-A-60	AIRE ACONDICIONADO COMPACTO CARRIER 5TON 50ES-A-60	CARRIER	50ES-A-60-30	\N	\N
8	8	3	A/A MANEJADOR DE AIRE CARRIER 10TON 40RUAA12	MANEJADORA DE AIRE CARRIER SOLO FRÍO R-410A 10 TON 220-440/3/60 	CARRIER	40RUAA12A2A6-0A0A0	\N	\N
9	9	3	LONIX SENSOR TEMPERATURA/HUMEDAD LX-RHT	SENSOR DE TEMPERATURA Y HUMEDAD LANIX LX-RHT-RAP-P-LCD	LANIX	LX-RHT-RAP-P-LCD	\N	\N
10	10	2	CUADRO DE FUERZA DELTA-05-891-SR	CUADRO DE FUERZA DELTA INPUT 17.6KVA3Ø~208VAC 50~60HZ, OUTPUT 48VDC	DELTA	IN-30KVA~208VAC 50~60HZ, OUT-48VDC	\N	\N
11	11	2	INVERSOR DELTA ES148N202AB A	INVERSOR DELTA ES148N202AB A  IN2.4/48Vdc, OUT2.4/1Ø/120V 	DELTA	ES148N202AB A 	\N	\N
12	12	9	BATERIA BSB POWER 12V~200AH-T22	BATERIA BSB POWER DB12V-200AH T22	BSB POWER	DB12V-200AH T22	\N	\N
13	13	9	BATERIA BSB POWER 12V~250AH-T22	BATERIA BSB POWER DB12V-250AH T22	BSB POWER	DB12V-250AH T22	\N	\N
14	14	9	BATERIA BSB POWER LSE2-1000AH	BATERIA BSB POWER LSE2-1000AH	BSB POWER	LSE2-1000AH T22	\N	\N
15	15	9	BATERIA BSB POWER LSE2-1500AH	BATERIA BSB POWER LSE2-1500AH	BSB POWER	LSE2-1500AH T22	\N	\N
16	16	9	BATERIA BSB POWER LSE2-2000AH	BATERIA BSB POWER LSE2-2000AH	BSB POWER	LSE2-2000AH T22	\N	\N
17	17	9	BATERIA DUNCAN STX 2VDC 2200AH	BATERIA ESTACIONARIA DUNCAN STX 2VDC 2200AH	DUNCAN	D-STX2200AH~2VDC	\N	\N
18	18	9	BATERIA SEC CELLYTE 12FTA155	BATERIA ESTACIONARIA SEC CELLYTE 12VDC~155AH	SEC	SEC-CELLYTE-12FTA155-12VDC~155AH	\N	\N
19	19	2	RECTIFICADOR DE POTENCIA DELTA DPR 1600W-48	RECTIFICADOR DE POTENCIA DELTA DPR-1600, IN-88~310VRMS 50~60HZ, OUT-40~59.5VDC 1600WAT	DELTA	DPR 1600C-48 / ESR-48/56	\N	\N
20	20	2	RECTIFICADOR DE POTENCIA EATON APR48-3G 48V 1800W	RECTIFICADOR DE POTENCIA EATON APR48-3G 48VDC 1800W	EATON	APR48-3G 1800W	\N	\N
21	21	2	RECTIFICADOR DE POTENCIA DELTA DPR 2700W-48	RECTIFICADOR DE POTENCIA DELTA DPR-2700, IN-88~310VRMS 50~60HZ, OUT-40~59.5VDC 2700WAT	DELTA	DPR 2700C-48 / ESR-48/56	\N	\N
22	22	2	PDB DELTA 700AMP 600VAC	BLOQUE DISTRIBUIDOR DE POTENCIA DELTA 700AMP 600VAC	DELTA	PDB-DELTA-700AMP-600VAC	\N	\N
23	23	2	PDB DELTA 700AMP 500VAC	BLOQUE DISTRIBUIDOR DE POTENCIA DELTA 700AMP 500VAC	DELTA	PDB-DELTA-700AMP-500VAC	\N	\N
24	24	7	UPS DELTA 3KVA T-R O/L	UPS DELTA 3KVA (2.7KW) 10INC TORRE-RACK ON LINE	DELTA	SERIE RT~3KVA O-L	\N	\N
25	25	10	BMS LONIX CONTROLLER	UNIDAD CONTROLADORA BUILD MANAGER SISTEM LONIX	LONIX	LONIXCONTROLLER	\N	\N
26	26	8	TRANSFORMADOR  GENERICO CA 15 KVA	TRANSFORMADOR CORRIENTE ALTERNA 15KVA	GENERICO	TRANS-GEN-CA-15KVA	\N	\N
27	27	8	TRANSFORMADOR  GENERICO CA 37,5 KVA	TRANSFORMADOR CORRIENTE ALTERNA 37,5KVA	GENERICO	TRANS-GEN-CA-37.5KVA	\N	\N
28	28	8	TRANSFORMADOR  GENERICO CA 45 KVA	TRANSFORMADOR CORRIENTE ALTERNA 45 KVA	GENERICO	TRANS-GEN-CA-45 KVA	\N	\N
29	29	8	TRANSFORMADOR  GENERICO SECO CA 45 KVA	TRANSFORMADOR  CORRIENTE ALTERNA GENERICO SECO 45 KVA	GENERICO	TRANS-GEN-SECO-CA-45KVA	\N	\N
30	30	8	TRANSFORMADOR  GENERICO CA 112,5KVA	TRANSFORMADOR  CORRIENTE ALTERNA GENERICO 112,5KVA	GENERICO	TRANS-GEN-CA-112.5KVA	\N	\N
31	31	1	ATS GENERICO 3FASES	SISTEMA AUTOMATICO DE TRANSFERENCIA GENERICO 3 FASES	GENERICO	ATS-GEN-3FAS	\N	\N
32	32	1	MOTOGENERADOR GENERICO 25KVA 3FASES	MOTOGENERADOR GENERICO 25KVA 3FASES	GENERICO	MOT-GENE-25KVA-3FAS	\N	\N
33	33	1	MOTOGENERADOR GENERICO 40KVA 3FASES	MOTOGENERADOR GENERICO 40KVA 3FASES	GENERICO	MOT-GENE-40KVA-3FAS	\N	\N
34	34	1	MOTOGENERADOR GENERICO 45KVA 3FASES	MOTOGENERADOR GENERICO 45KVA 3FASES	GENERICO	MOT-GENE-45KVA-3FAS	\N	\N
35	35	1	MOTOGENERADOR GENERICO 50KVA 3FASES	MOTOGENERADOR GENERICO 50KVA 3FASES	GENERICO	MOT-GENE-50KVA-3FAS	\N	\N
36	36	1	MOTOGENERADOR GENERICO 75KVA 3FASES	MOTOGENERADOR GENERICO 75KVA 3FASES	GENERICO	MOT-GENE-75KVA-3FAS	\N	\N
37	37	1	TANQUE DE COMBUSTIBLE 1400LTS OIL	TANQUE DE COMBUSTIBLE 1400 LITROS OIL	GENERICO	TAN-COMB-OIL-1400LTS	\N	\N
38	38	1	TANQUE DE COMBUSTIBLE 2100LTS OIL	TANQUE DE COMBUSTIBLE 2100 LITROS OIL	GENERICO	TAN-COMB-OIL-2100LTS	\N	\N
39	39	1	TANQUE DE COMBUSTIBLE 6000LTS OIL	TANQUE DE COMBUSTIBLE 6000 LITROS OIL	GENERICO	TAN-COMB-OIL-6000LTS	\N	\N
40	40	8	TABLERO DE CONTROL GEN NHB 30CTOS 3F	TABLERO DE CONTROL GENERICO TIPO NHB 30 CIRCUITOS 3 FASES	GENERICO	TAB-CONT-GEN-NHB-30CTOS-3F	\N	\N
41	41	8	TABLERO DE CONTROL GEN NHB 24CTOS 3F	TABLERO DE CONTROL GENERICO TIPO NHB 24 CIRCUITOS 3 FASES	GENERICO	TAB-CONT-GEN-NHB-24CTOS-3F	\N	\N
42	42	8	TABLERO DE CONTROL GEN NHB 18CTOS 3F	TABLERO DE CONTROL GENERICO TIPO NHB 18 CIRCUITOS 3 FASES	GENERICO	TAB-CONT-GEN-NHB-18CTOS-3F	\N	\N
43	43	8	TABLERO DE CONTROL GEN NHB 12CTOS 3F	TABLERO DE CONTROL GENERICO TIPO NHB 12 CIRCUITOS 3 FASES	GENERICO	TAB-CONT-GEN-NHB-12CTOS-3F	\N	\N
44	44	8	TABLERO DE CONTROL GEN NHB 8CTOS 3F	TABLERO DE CONTROL GENERICO TIPO NHB 8 CIRCUITOS 3 FASES	GENERICO	TAB-CONT-GEN-NHB-8CTOS-3F	\N	\N
45	45	8	TABLERO DE CONTROL GEN NHB 6CTOS 3F	TABLERO DE CONTROL GENERICO TIPO NHB 6 CIRCUITOS 3 FASES	GENERICO	TAB-CONT-GEN-NHB-6CTOS-3F	\N	\N
46	46	8	SUPRESOR DE PICOS GENERICO 3FASES	SUPRESOR DE PICOS ELECTRICOS TRIFASICO	GENERICO	SUP-PIC-GEN-3F	\N	\N
47	47	8	POSTE ALUMBRADO ELECTRICO	POSTE ALUMBRADO ELECTRICO 9MTS	GENERICO	POST-ALUM-ELECT-9MTS	\N	\N
48	48	8	BREAKER TRIFASICO GENENERICO 20 AMP	BREAKER TRIFASICO GENENERICO 20 AMP	GENERICO	BRECK-GEN-3F-20AMP	\N	\N
49	49	8	BREAKER TRIFASICO GENENERICO 30 AMP	BREAKER TRIFASICO GENENERICO 30 AMP	GENERICO	BRECK-GEN-3F-30AMP	\N	\N
50	50	8	BREAKER TRIFASICO GENENERICO 40 AMP	BREAKER TRIFASICO GENENERICO 40 AMP	GENERICO	BRECK-GEN-3F-40AMP	\N	\N
51	51	8	BREAKER TRIFASICO GENENERICO 50 AMP	BREAKER TRIFASICO GENENERICO 50 AMP	GENERICO	BRECK-GEN-3F-50AMP	\N	\N
52	52	8	BREAKER TRIFASICO GENENERICO 60 AMP	BREAKER TRIFASICO GENENERICO 60 AMP	GENERICO	BRECK-GEN-3F-60AMP	\N	\N
53	53	8	BREAKER TRIFASICO GENENERICO 70 AMP	BREAKER TRIFASICO GENENERICO 70 AMP	GENERICO	BRECK-GEN-3F-70AMP	\N	\N
54	54	8	BREAKER TRIFASICO GENENERICO 80 AMP	BREAKER TRIFASICO GENENERICO 80 AMP	GENERICO	BRECK-GEN-3F-80AMP	\N	\N
55	55	8	BREAKER TRIFASICO GENENERICO 90 AMP	BREAKER TRIFASICO GENENERICO 90 AMP	GENERICO	BRECK-GEN-3F-90AMP	\N	\N
56	56	8	BREAKER TRIFASICO GENENERICO 100 AMP	BREAKER TRIFASICO GENENERICO 100 AMP	GENERICO	BRECK-GEN-3F-100AMP	\N	\N
57	57	8	BREAKER TRIFASICO GENENERICO 125 AMP	BREAKER TRIFASICO GENENERICO 125 AMP	GENERICO	BRECK-GEN-3F-125AMP	\N	\N
58	58	8	BREAKER TRIFASICO GENENERICO 150 AMP	BREAKER TRIFASICO GENENERICO 150 AMP	GENERICO	BRECK-GEN-3F-150AMP	\N	\N
59	59	8	BREAKER TRIFASICO GENENERICO 200 AMP	BREAKER TRIFASICO GENENERICO 200 AMP	GENERICO	BRECK-GEN-3F-200AMP	\N	\N
60	60	8	BREAKER TRIFASICO GENENERICO 250 AMP	BREAKER TRIFASICO GENENERICO 250 AMP	GENERICO	BRECK-GEN-3F-250AMP	\N	\N
61	61	9	BANCO DE BATERIA GENERICO 12V 200AH	BANCO DE BATERIA GENERICO 12VOLTIOS 200AMPER~HORA	GENERICO	BAN-BAT-GEN-12V-200AH	\N	\N
62	62	1	MOTOGENERADOR GENERICO 30KVA 3FASES	MOTOGENERADOR GENERICO 30KVA 3FASES	GENERICO	MOT-GENE-30KVA-3FAS	\N	\N
63	63	9	BANCO DE BATERIA GENERICO 12V 150AH	BANCO DE BATERIA GENERICO 12VOLTIOS 150AMPER~HORA	GENERICO	BAN-BAT-GEN-12V-150AH	\N	\N
64	64	9	BANCO DE BATERIA GENERICO 12V 250AH	BANCO DE BATERIA GENERICO 12VOLTIOS 250AMPER~HORA	GENERICO	BAN-BAT-GEN-12V-250AH	\N	\N
65	65	9	BANCO DE BATERIAS GENERICO 	BANCO DE BATERIA GENERICO 12VOLTIOS 	GENERICO	BAN-BAT-GEN-12V 	\N	\N
66	66	2	RECTIFICADOR DE POTENCIA GENERICO	RECTIFICADOR DE POTENCIA GENERICO	GENERICO	RECT-POT-GEN-ND	\N	\N
67	67	2	CUADRO DE FUERZA GENERICO	CUADRO DE FUERZA GENERICO	GENERICO	CUAD-FUERZ-GEN-ND	\N	\N
68	68	1	MOTOGENERADOR GENERICO KVA=ND FASES=ND	MOTOGENERADOR GENERICO 	GENERICO	MOT-GENE-NDKVA-NDFAS	\N	\N
\.


--
-- Data for Name: localidad; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.localidad (id, nombre, estado_id, created_at, updated_at, tipo) FROM stdin;
7	BANCO LARGO	1	\N	2021-03-03 15:59:05	CENTRAL CRITICA
39	EL CANTON	1	\N	2021-03-03 15:27:43	NODO OUTDOOR
49	EL PIÑAL	1	\N	2021-03-03 15:30:27	CENTRAL CRITICA
63	LA FLORIDA	1	\N	2021-03-03 15:32:55	CENTRAL
65	LA FUNDACION	1	\N	2021-03-03 15:34:45	CENTRAL
66	LA GRITA I	1	\N	2021-03-03 15:35:56	CENTRAL
75	LAS DELICIAS	1	\N	2021-03-03 15:38:37	CENTRAL
81	LAS PALMERAS	1	\N	2021-03-03 15:39:27	CENTRAL
96	PALO GORDO	1	\N	2021-03-16 14:14:00	URL
101	PREGONERO	1	\N	2021-03-03 15:41:19	CENTRAL
103	PUEBLO NUEVO I	1	\N	2021-03-03 15:42:25	CENTRAL
118	SAN PABLO	1	\N	2021-03-03 15:49:26	CENTRAL
122	SANTA ANA	1	\N	2021-03-03 15:50:09	CENTRAL
26	CM NEGRO PRIMERO TACHIRA	1	\N	2021-03-03 15:51:13	DLC
110	SAN ANTONIO	1	\N	2021-03-03 15:54:12	SUPERAULA
139	ZONA BAJA	1	\N	2021-03-16 12:18:50	NODO OUTDOOR
6	BAILADORES II	1	\N	2021-02-11 09:28:48	CENTRAL
13	BOROTA	1	\N	2021-02-11 09:28:49	NODO OUTDOOR
20	CAPACHO	1	\N	2021-02-11 09:28:49	CENTRAL
19	CAPACHO I	1	\N	2021-02-11 09:28:49	CENTRAL
22	CERRO EL TAMBO	1	\N	2021-02-11 09:28:49	CENTRAL
23	CERRO GALLINERO	1	\N	2021-02-11 09:28:49	CENTRAL
31	CONCORDIA I	1	\N	2021-02-11 09:28:49	CENTRAL
32	CORDERO I	1	\N	2021-02-11 09:28:49	CENTRAL
34	CUTUFI	1	\N	2021-02-11 09:28:49	CENTRAL
35	EL AMPARO I	1	\N	2021-02-11 09:28:49	CENTRAL
38	EL CANTON II	1	\N	2021-02-11 09:28:49	CENTRAL
40	EL COBRE	1	\N	2021-02-11 09:28:49	NODO OUTDOOR
43	EL NULA I	1	\N	2021-02-11 09:28:49	CENTRAL
44	EL NULA II	1	\N	2021-02-11 09:28:49	CENTRAL
53	EL ZUMBADOR	1	\N	2021-02-11 09:28:49	CENTRAL
127	TERMINAL PLAN DE RUBIO	1	\N	2021-02-11 09:28:50	CENTRAL
60	HATO LA VIRGEN	1	\N	2021-02-11 09:28:50	CENTRAL
76	LAS DELICIAS	1	\N	2021-02-11 09:28:50	NODO OUTDOOR
86	LOS HATICOS	1	\N	2021-02-11 09:28:50	CENTRAL
89	MICHELENA	1	\N	2021-02-11 09:28:50	CENTRAL
94	PALMIRA	1	\N	2021-02-11 09:28:50	CENTRAL
105	QUENIQUEA	1	\N	2021-02-11 09:28:50	NODO OUTDOOR
108	RIO FRIO	1	\N	2021-02-11 09:28:51	CENTRAL
109	RUBIO	1	\N	2021-02-11 09:28:51	SUPERAULA
114	SAN JOAQUIN DE NAVAY	1	\N	2021-02-11 09:28:51	CENTRAL
115	SAN JOSE DE BOLIVAR	1	\N	2021-02-11 09:28:51	NODO OUTDOOR
126	TARIBA I	1	\N	2021-02-11 09:28:51	CENTRAL
130	UMUQUENA	1	\N	2021-02-11 09:28:51	NODO OUTDOOR
131	UREÑA	1	\N	2021-02-11 09:28:51	CENTRAL
140	ZORCA SAN ISIDRO	1	\N	2021-02-11 09:28:51	CENTRAL
64	LA FRIA	1	\N	2021-02-11 09:28:51	OPSUT
72	LA PEDRERA	1	\N	2021-02-11 09:28:52	OPSUT
117	SAN JUAN DE COLON	1	\N	2021-02-11 09:28:52	OPSUT
133	Y01 C PARAMILLO CALLE 3 CANEYES	1	\N	2021-02-11 09:28:52	DLC
135	Y02 C CONCORDIA EL PALMAR	1	\N	2021-02-11 09:28:52	DLC
136	Y02 C PARAMILLO URL COPA DE ORO	1	\N	2021-02-11 09:28:52	DLC
137	Y03 C CONCORDIA RINCON DE LA VEGA	1	\N	2021-02-11 09:28:52	DLC
138	Y04 C CONCORDIA SAN LORENZO	1	\N	2021-02-11 09:28:52	DLC
90	MOVISTAR CC EL ESTE (NO CANTV)	1	\N	2021-02-11 09:28:52	NO CANTV
119	SAN PEDRO DEL RIO (NO CANTV)	1	\N	2021-02-11 09:28:52	NO CANTV
125	SIBERIA (NO CANTV)	1	\N	2021-02-11 09:28:52	NO CANTV
91	NODO EL CANTON PB	1	\N	2021-02-11 09:28:52	NODO OUTDOOR
3	AGUAS CALIENTES	1	\N	2021-02-11 09:28:53	NODO OUTDOOR
5	AURORA	1	\N	2021-02-11 09:28:53	NODO OUTDOOR
8	BARRANCAS TACHIRA I	1	\N	2021-02-11 09:28:53	NODO OUTDOOR
10	BARRANCAS TACHIRA III	1	\N	2021-02-11 09:28:53	NODO OUTDOOR
11	BOCA DE GRITA	1	\N	2021-02-11 09:28:53	NODO OUTDOOR
12	BOLIVARIANO	1	\N	2021-02-11 09:28:53	NODO OUTDOOR
16	CANEYES I	1	\N	2021-02-11 09:28:53	NODO OUTDOOR
18	CAPACHITO	1	\N	2021-02-11 09:28:53	NODO OUTDOOR
25	CIELO AZUL	1	\N	2021-02-11 09:28:53	NODO OUTDOOR
29	COLONCITO I	1	\N	2021-02-11 09:28:53	NODO OUTDOOR
30	COLONCITO II	1	\N	2021-02-11 09:28:53	NODO OUTDOOR
37	EL AMPARO	1	\N	2021-02-11 09:28:54	NODO OUTDOOR
45	EL NULA	1	\N	2021-02-11 09:28:54	NODO OUTDOOR
46	EL PALOTAL	1	\N	2021-02-11 09:28:54	NODO OUTDOOR
48	EL PIÑAL II	1	\N	2021-02-11 09:28:54	NODO OUTDOOR
51	EL VALLE I	1	\N	2021-02-11 09:28:54	NODO OUTDOOR
54	FRANCISCO DE MIRANDA	1	\N	2021-02-11 09:28:54	NODO OUTDOOR
55	GUACAS	1	\N	2021-02-11 09:28:54	NODO OUTDOOR
57	GUASDUALITO II	1	\N	2021-02-11 09:28:54	NODO OUTDOOR
62	LA ESMERALDA	1	\N	2021-02-11 09:28:54	NODO OUTDOOR
67	LA GRITA II	1	\N	2021-02-11 09:28:54	NODO OUTDOOR
69	LA GRITA IV	1	\N	2021-02-11 09:28:54	NODO OUTDOOR
70	LA LAJA	1	\N	2021-02-11 09:28:54	NODO OUTDOOR
71	LA MACHIRI	1	\N	2021-02-11 09:28:54	NODO OUTDOOR
78	LAS MESAS I	1	\N	2021-02-11 09:28:55	NODO OUTDOOR
80	LAS MESAS III	1	\N	2021-02-11 09:28:55	NODO OUTDOOR
82	LLANO JORGE	1	\N	2021-02-11 09:28:55	NODO OUTDOOR
84	LOMAS BAJAS	1	\N	2021-02-11 09:28:55	NODO OUTDOOR
85	LOMAS BLANCAS	1	\N	2021-02-11 09:28:55	NODO OUTDOOR
92	OROPE	1	\N	2021-02-11 09:28:55	NODO OUTDOOR
93	PALMAR DE LA COPE	1	\N	2021-02-11 09:28:55	NODO OUTDOOR
97	PAN DE AZUCAR	1	\N	2021-02-11 09:28:55	NODO OUTDOOR
98	PARAMILLOS	1	\N	2021-02-11 09:28:55	NODO OUTDOOR
104	PUEBLO VIEJO	1	\N	2021-02-11 09:28:55	NODO OUTDOOR
120	SAN PEDRO DEL RIO	1	\N	2021-02-11 09:28:56	NODO OUTDOOR
129	TUCAPE MEDIO	1	\N	2021-02-11 09:28:56	NODO OUTDOOR
121	SAN RAFAEL DEL PIÑAL	1	\N	2021-02-11 09:28:57	OPSUT
28	COLON	1	\N	2021-02-11 09:28:57	SUPERAULA
277	MIRI	3	\N	2021-03-03 15:03:10	CENTRAL
260	EL COROZO	3	\N	2021-03-03 15:11:51	NODO OUTDOOR
217	SANTA APOLONIA	2	\N	2021-03-04 09:55:06	CENTRAL
215	SANTA ANA	2	\N	2021-03-04 10:02:19	CENTRAL
185	LAS LOMAS	2	\N	2021-03-04 10:20:12	NODO OUTDOOR
226	URB EL PRADO (PAMPANITO)	2	\N	2021-03-04 10:26:20	OPSUT
2661	CARRIZALES	9	2021-02-25 11:46:18	2021-03-23 09:18:34	CENTRAL
145	ALMACEN NODAL VALERA (ZONA INDUSTRIAL)	2	\N	2021-02-11 09:28:48	CENTRAL
238	ALTO BARINAS	3	\N	2021-02-11 09:28:48	CENTRAL
146	ALTOS SAN ANTONIO BATATAL	2	\N	2021-02-11 09:28:48	CENTRAL
240	BARINAS I	3	\N	2021-02-11 09:28:48	CENTRAL
242	BARINITAS	3	\N	2021-02-11 09:28:48	CENTRAL
150	BETIJOQUE	2	\N	2021-02-11 09:28:49	CENTRAL
151	BOBURES	2	\N	2021-02-11 09:28:49	NODO OUTDOOR
244	BRUZUAL I	3	\N	2021-02-11 09:28:49	NODO OUTDOOR
245	BRUZUAL II	3	\N	2021-02-11 09:28:49	CENTRAL
153	BURBUSAY	2	\N	2021-02-11 09:28:49	OPSUT
248	CALDERAS	3	\N	2021-02-11 09:28:49	NODO OUTDOOR
154	CARACHE	2	\N	2021-02-11 09:28:49	NODO OUTDOOR
155	CARVAJAL	2	\N	2021-02-11 09:28:49	CENTRAL
156	CERRO SAN PEDRO	2	\N	2021-02-11 09:28:49	CENTRAL
157	CHACHOPO	2	\N	2021-02-11 09:28:49	OPSUT
251	CHAMETA	3	\N	2021-02-11 09:28:49	NODO OUTDOOR
252	CIUDAD BOLIVIA	3	\N	2021-02-11 09:28:49	NODO OUTDOOR
254	CODAZZI	3	\N	2021-02-11 09:28:49	CENTRAL
159	CUICAS	2	\N	2021-02-11 09:28:49	NODO OUTDOOR
161	EL BATEY	2	\N	2021-02-11 09:28:49	NODO OUTDOOR
164	EL JAGUITO	2	\N	2021-02-11 09:28:49	CENTRAL
165	EL PROGRESO KM 12	2	\N	2021-02-11 09:28:49	CENTRAL
166	ESCUQUE PUEBLO	2	\N	2021-02-11 09:28:49	CENTRAL
168	GIBRALTAR	2	\N	2021-02-11 09:28:50	NODO OUTDOOR
171	JUBIOTE	2	\N	2021-02-11 09:28:50	CENTRAL
172	JUNIN	2	\N	2021-02-11 09:28:50	CENTRAL
176	LA CEIBA	2	\N	2021-02-11 09:28:50	NODO OUTDOOR
178	LA MESETA DE CHIMPIRE	2	\N	2021-02-11 09:28:50	CENTRAL
181	LA PUERTA	2	\N	2021-02-11 09:28:50	NODO OUTDOOR
182	LA QUEBRADA	2	\N	2021-02-11 09:28:50	NODO OUTDOOR
270	LAS PIEDRAS	3	\N	2021-02-11 09:28:50	NODO OUTDOOR
271	LAS VEGUITAS	3	\N	2021-02-11 09:28:50	OPSUT
272	LIBERTAD	3	\N	2021-02-11 09:28:50	NODO OUTDOOR
187	LIMONCITO	2	\N	2021-02-11 09:28:50	CENTRAL
190	MESA DE ESDOVAS	2	\N	2021-02-11 09:28:50	CENTRAL
191	MITON	2	\N	2021-02-11 09:28:50	NODO OUTDOOR
194	MONTE CARMELO	2	\N	2021-02-11 09:28:50	NODO OUTDOOR
195	MOTATAN	2	\N	2021-02-11 09:28:50	NODO OUTDOOR
197	NUEVA BOLIVIA	2	\N	2021-02-11 09:28:50	CENTRAL
200	PALMARITO	2	\N	2021-02-11 09:28:50	NODO OUTDOOR
202	PARAMO GUARAMACAL	2	\N	2021-02-11 09:28:50	CENTRAL
281	PUEBLO EL LLANO	3	\N	2021-02-11 09:28:50	NODO OUTDOOR
205	REPETIDORA ESCUQUE	2	\N	2021-02-11 09:28:50	CENTRAL
210	SABANETA	2	\N	2021-02-11 09:28:51	CENTRAL
213	SAN MIGUEL	2	\N	2021-02-11 09:28:51	NODO OUTDOOR
216	SANTA APOLONIA	2	\N	2021-02-11 09:28:51	NODO OUTDOOR
220	TIMOTES	2	\N	2021-02-11 09:28:51	NODO OUTDOOR
221	TOROCOCO	2	\N	2021-02-11 09:28:51	NODO OUTDOOR
223	TOSTOS	2	\N	2021-02-11 09:28:51	CENTRAL
225	URB EL PRADO	2	\N	2021-02-11 09:28:51	CENTRAL
236	ZONA RICA KM 17	2	\N	2021-02-11 09:28:51	CENTRAL
141	ZORCA	1	\N	2021-02-11 09:28:51	NODO OUTDOOR
241	BARINAS II	3	\N	2021-02-11 09:28:51	CENTRAL CRITICA
224	TRUJILLO	2	\N	2021-02-11 09:28:52	SUPERAULA
230	VALERA I	2	\N	2021-02-11 09:28:52	CENTRAL CRITICA
233	Y01 DLC LIMONCITOS URL LA PUERTA 1	2	\N	2021-02-11 09:28:52	DLC
234	Y01 DLC VALERA NIQUITAO	2	\N	2021-02-11 09:28:52	DLC
237	24 JUNIO SABANETA BARINAS	3	\N	2021-02-11 09:28:52	NODO OUTDOOR
144	ALICIA PIETRI DE CALDERA	2	\N	2021-02-11 09:28:53	NODO OUTDOOR
148	BATATAL	2	\N	2021-02-11 09:28:53	NODO OUTDOOR
253	CIUDAD BOLIVIA PEDRAZA	3	\N	2021-02-11 09:28:53	NODO OUTDOOR
256	COLINAS DEL LLANO 02	3	\N	2021-02-11 09:28:53	NODO OUTDOOR
257	COLINAS DEL LLANO 03	3	\N	2021-02-11 09:28:53	NODO OUTDOOR
258	CORRALITO	3	\N	2021-02-11 09:28:53	NODO OUTDOOR
262	FLOR AMARILLO SABANETA BARI	3	\N	2021-02-11 09:28:54	NODO OUTDOOR
263	GUANAPA	3	\N	2021-02-11 09:28:54	NODO OUTDOOR
264	JARDINES DE ALTO BARINAS	3	\N	2021-02-11 09:28:54	NODO OUTDOOR
174	LA BEATRIZ I	2	\N	2021-02-11 09:28:54	NODO OUTDOOR
175	LA BEATRIZ II	2	\N	2021-02-11 09:28:54	NODO OUTDOOR
267	LA MULA	3	\N	2021-02-11 09:28:54	NODO OUTDOOR
179	LA MURALLA 2	2	\N	2021-02-11 09:28:54	NODO OUTDOOR
268	LA PAZ	3	\N	2021-02-11 09:28:54	NODO OUTDOOR
183	LAS ACACIAS	2	\N	2021-02-11 09:28:55	OAC
274	LOS BUCARES	3	\N	2021-02-11 09:28:55	NODO OUTDOOR
275	LOS LIRIOS	3	\N	2021-02-11 09:28:55	NODO OUTDOOR
276	MI JARDIN	3	\N	2021-02-11 09:28:55	NODO OUTDOOR
193	MONAY II	2	\N	2021-02-11 09:28:55	NODO OUTDOOR
199	NUEVA BOLIVIA II	2	\N	2021-02-11 09:28:55	NODO OUTDOOR
278	OBISPO	3	\N	2021-02-11 09:28:55	NODO OUTDOOR
280	PRIMERO DE DICIEMBRE	3	\N	2021-02-11 09:28:55	NODO OUTDOOR
204	PUEBLO LLANO	2	\N	2021-02-11 09:28:55	NODO OUTDOOR
283	PUNTA GORDA II	3	\N	2021-02-11 09:28:55	NODO OUTDOOR
207	SABANA DE MENDOZA I	2	\N	2021-02-11 09:28:55	NODO OUTDOOR
209	SABANA LIBRE	2	\N	2021-02-11 09:28:56	NODO OUTDOOR
211	SAN GENARO	2	\N	2021-02-11 09:28:56	NODO OUTDOOR
232	VALLE VERDE	2	\N	2021-02-11 09:28:56	NODO OUTDOOR
239	BARINAS	3	\N	2021-02-11 09:28:56	SUPERAULA
160	EL BATATILLO	2	\N	2021-02-11 09:28:56	OPSUT
167	FLOR DE PATRIA	2	\N	2021-02-11 09:28:56	OPSUT
184	LAS LLANADAS	2	\N	2021-02-11 09:28:57	OPSUT
288	SABANETA	3	\N	2021-03-03 15:04:53	CENTRAL
299	SANTO DOMINGO	3	\N	2021-03-03 15:06:43	CENTRAL
395	SANTA MARIA	4	\N	2021-03-03 15:43:48	CENTRAL
398	TABAY GTI	4	\N	2021-03-03 15:45:38	NODO OUTDOOR
319	APARTADEROS	4	\N	2021-03-03 15:50:17	NODO OUTDOOR
369	LOS NARANJOS	4	\N	2021-03-03 15:57:27	NODO OUTDOOR
333	CC JUNIOR MALL	4	\N	2021-03-04 08:48:07	NODO OUTDOOR
2662	MIQUILLEN 'LOS TEQUES'	9	2021-02-25 11:48:01	2021-03-23 09:19:14	CENTRAL
411	ALTA VISTA	5	\N	2021-02-11 09:28:48	CENTRAL
413	ALTOS DE MATURIN	5	\N	2021-02-11 09:28:48	CENTRAL
324	BOCONO DEL TACHIRA	4	\N	2021-02-11 09:28:49	OPSUT
327	CANAGUA	4	\N	2021-02-11 09:28:49	NODO OUTDOOR
330	CAÑO ZANCUDO	4	\N	2021-02-11 09:28:49	CENTRAL
334	CHIGUARA	4	\N	2021-02-11 09:28:49	NODO OUTDOOR
335	CHIGUARA I	4	\N	2021-02-11 09:28:49	CENTRAL
340	EJIDO III	4	\N	2021-02-11 09:28:49	CENTRAL
341	EL GUAYABO	4	\N	2021-02-11 09:28:49	NODO OUTDOOR
345	ENCONTRADOS	4	\N	2021-02-11 09:28:49	NODO OUTDOOR
346	GUARAQUE	4	\N	2021-02-11 09:28:50	CENTRAL
349	JAJI	4	\N	2021-02-11 09:28:50	CENTRAL
350	LA AGUADA	4	\N	2021-02-11 09:28:50	CENTRAL
356	LA MAJUMBA	4	\N	2021-02-11 09:28:50	CENTRAL
357	LA OLINDA	4	\N	2021-02-11 09:28:50	CENTRAL
359	LA PEDREGOSA	4	\N	2021-02-11 09:28:50	CENTRAL
361	LA PUNTA	4	\N	2021-02-11 09:28:50	CENTRAL
363	LA TENDIDA	4	\N	2021-02-11 09:28:50	NODO OUTDOOR
366	LAS HERNANDEZ	4	\N	2021-02-11 09:28:50	NODO OUTDOOR
373	MESA BOLIVAR	4	\N	2021-02-11 09:28:50	NODO OUTDOOR
375	MUCUCHIES	4	\N	2021-02-11 09:28:50	OPSUT
376	MUCUJEPE	4	\N	2021-02-11 09:28:50	NODO OUTDOOR
378	OBSERVATORIO ASTROFISICO	4	\N	2021-02-11 09:28:50	CENTRAL
379	PARAMO DE MARIÑO	4	\N	2021-02-11 09:28:50	CENTRAL
383	PUEBLO NUEVO EL CHIVO	4	\N	2021-02-11 09:28:50	NODO OUTDOOR
385	REPETIDORA PICO ESPEJO	4	\N	2021-02-11 09:28:51	CENTRAL
290	SABANETA I	3	\N	2021-02-11 09:28:51	CENTRAL
392	SANTA CRUZ DE MORA	4	\N	2021-02-11 09:28:51	CENTRAL
393	SANTA CRUZ DEL ZULIA	4	\N	2021-02-11 09:28:51	CENTRAL
298	SANTA ROSA	3	\N	2021-02-11 09:28:51	OPSUT
308	TELEFONIA PUBLICA	3	\N	2021-02-11 09:28:51	CENTRAL
399	TIBISAY I	4	\N	2021-02-11 09:28:51	CENTRAL
401	TOVAR I	4	\N	2021-02-11 09:28:51	CENTRAL
403	TUCANI	4	\N	2021-02-11 09:28:51	NODO OUTDOOR
406	ZEA CENTRO	4	\N	2021-02-11 09:28:51	CENTRAL
409	AEROPUERTO INTERNACIONAL DE MAIQUETIA	5	\N	2021-02-11 09:28:51	CENTRAL CRITICA
332	CASIGUA EL CUBO	4	\N	2021-02-11 09:28:51	OPSUT
293	SANTA BARBARA DE BARINAS	3	\N	2021-02-11 09:28:52	CENTRAL CRITICA
300	SOCOPO	3	\N	2021-02-11 09:28:52	CENTRAL CRITICA
410	ALEJANDRO FUENMAYOR	5	\N	2021-02-11 09:28:53	NODO OUTDOOR
318	ALTAVISTA	4	\N	2021-02-11 09:28:53	NODO OUTDOOR
320	ARENAL MARIANITA	4	\N	2021-02-11 09:28:53	NODO OUTDOOR
415	ARNULFO ROMERO NA10-029	5	\N	2021-02-11 09:28:53	NODO OUTDOOR
416	AV COSTA RICA	5	\N	2021-02-11 09:28:53	NODO OUTDOOR
321	BAILADORES	4	\N	2021-02-11 09:28:53	NODO OUTDOOR
418	BARRIO VISTA HERMOSA CUJICI	5	\N	2021-02-11 09:28:53	NODO OUTDOOR
419	BOSQUE VALLE U839	5	\N	2021-02-11 09:28:53	NODO OUTDOOR
325	BURBUQUI	4	\N	2021-02-11 09:28:53	NODO OUTDOOR
421	CACIQUE TIUNA	5	\N	2021-02-11 09:28:53	NODO OUTDOOR
326	CACUTE	4	\N	2021-02-11 09:28:53	NODO OUTDOOR
423	CALLE CHILE III	5	\N	2021-02-11 09:28:53	NODO OUTDOOR
424	CALLE CHILE IV	5	\N	2021-02-11 09:28:53	NODO OUTDOOR
331	CARABOBO	4	\N	2021-02-11 09:28:53	NODO OUTDOOR
337	CUATRO ESQUINAS I	4	\N	2021-02-11 09:28:54	NODO OUTDOOR
339	DON PERUCHO	4	\N	2021-02-11 09:28:54	NODO OUTDOOR
343	EL TROMPICON	4	\N	2021-02-11 09:28:54	NODO OUTDOOR
351	LA AZULITA	4	\N	2021-02-11 09:28:54	NODO OUTDOOR
353	LA BLANCA I	4	\N	2021-02-11 09:28:54	NODO OUTDOOR
355	LA LAGUNITA	4	\N	2021-02-11 09:28:54	NODO OUTDOOR
358	LA PALMITA	4	\N	2021-02-11 09:28:54	NODO OUTDOOR
365	LAS GONZALEZ	4	\N	2021-02-11 09:28:55	NODO OUTDOOR
368	LOS LLANITOS DE TABAY	4	\N	2021-02-11 09:28:55	NODO OUTDOOR
370	MARIA DOLORES	4	\N	2021-02-11 09:28:55	NODO OUTDOOR
382	PERIODISTA	4	\N	2021-02-11 09:28:55	NODO OUTDOOR
384	PUESTA DEL SOL	4	\N	2021-02-11 09:28:55	NODO OUTDOOR
285	RAUL VILLANUEVA	3	\N	2021-02-11 09:28:55	NODO OUTDOOR
386	RIO NEGRO	4	\N	2021-02-11 09:28:55	NODO OUTDOOR
287	ROSA INES	3	\N	2021-02-11 09:28:55	NODO OUTDOOR
289	SABANETA CENTRO	3	\N	2021-02-11 09:28:56	NODO OUTDOOR
389	SAN JUAN INREVI	4	\N	2021-02-11 09:28:56	NODO OUTDOOR
390	SAN SIMON	4	\N	2021-02-11 09:28:56	NODO OUTDOOR
295	SANTA BARBARA DE BARINAS II	3	\N	2021-02-11 09:28:56	NODO OUTDOOR
296	SANTA CRUZ DE GUACAS	3	\N	2021-02-11 09:28:56	NODO OUTDOOR
394	SANTA ELENA DE ARENALES	4	\N	2021-02-11 09:28:56	NODO OUTDOOR
301	SOCOPO II	3	\N	2021-02-11 09:28:56	NODO OUTDOOR
302	TAVACARE 1	3	\N	2021-02-11 09:28:56	NODO OUTDOOR
304	TAVACARE 3	3	\N	2021-02-11 09:28:56	NODO OUTDOOR
305	TAVACARE 4	3	\N	2021-02-11 09:28:56	NODO OUTDOOR
307	TAVACARE 6	3	\N	2021-02-11 09:28:56	NODO OUTDOOR
310	TERRAZAS DE SANTO DOMINGO 2	3	\N	2021-02-11 09:28:56	NODO OUTDOOR
311	TERRAZAS DE SANTO DOMINGO 3	3	\N	2021-02-11 09:28:56	NODO OUTDOOR
313	URB CARLOS RAUL VILLANUEVA	3	\N	2021-02-11 09:28:56	NODO OUTDOOR
314	URB LOS POMELOS	3	\N	2021-02-11 09:28:56	NODO OUTDOOR
405	ZEA	4	\N	2021-02-11 09:28:56	NODO OUTDOOR
371	MERIDA	4	\N	2021-02-11 09:28:57	OPSUT
316	VEGON DE NUTRIAS	3	\N	2021-02-11 09:28:57	OPSUT
400	TOVAR	4	\N	2021-02-11 09:28:57	SUPERAULA
485	LOS JARDINES	5	\N	2021-03-09 11:47:24	CENTRAL CRITICA
502	PABLO DELGADO	5	\N	2021-03-12 09:26:32	CENTRAL
517	RICARDO ZULOAGA	5	\N	2021-03-12 09:27:25	CENTRAL
523	SAN MARTIN	5	\N	2021-03-12 09:27:59	CENTRAL
437	CET- JAN DEKETH-ARVELO	5	\N	2021-03-12 09:30:47	CENTRAL
496	MONTALBAN	5	\N	2021-03-12 09:38:54	CENTRAL
507	PARACOTOS	5	\N	2021-03-12 09:40:54	CENTRAL
524	SAN OMERO	5	\N	2021-03-12 09:43:30	CENTRAL
443	CIUDAD TIUNA GTI	5	\N	2021-03-12 09:51:43	NODO OUTDOOR
477	LA VEGA LOS PINOS	5	\N	2021-03-12 09:53:46	NODO OUTDOOR
448	EL AMPARO	5	\N	2021-03-12 13:08:54	NODO OUTDOOR
518	RIO NEGRO	5	\N	2021-03-12 13:17:38	NODO OUTDOOR
489	EL PICACHO	9	\N	2021-03-15 18:41:14	CENTRAL
451	EL GUARATARO	5	\N	2021-03-17 15:57:07	NODO AUTDOOR
445	CTRO LOG LA YAGUARA	5	\N	2021-02-11 09:28:49	CENTRAL
453	EL JARILLO II	5	\N	2021-02-11 09:28:49	CENTRAL
468	KENNEDY	5	\N	2021-02-11 09:28:50	CENTRAL
478	LAS ADJUNTAS	5	\N	2021-02-11 09:28:50	CENTRAL
483	LOS CASTORES	5	\N	2021-02-11 09:28:50	CENTRAL
431	CC PROPATRIA	5	\N	2021-02-11 09:28:50	CENTRAL
433	CC SAN MARTIN AIRE ACONDICIONADO	5	\N	2021-02-11 09:28:50	CENTRAL
506	PARACOTOS	5	\N	2021-02-11 09:28:50	NODO OUTDOOR
510	PARQUE SUR	5	\N	2021-02-11 09:28:50	CENTRAL
515	REPETIDORA GEREMBA	5	\N	2021-02-11 09:28:50	CENTRAL
516	REPETIDORA IVIC	5	\N	2021-02-11 09:28:50	CENTRAL
525	SAN PEDRO	5	\N	2021-02-11 09:28:51	CENTRAL
534	URL LAS MINAS	5	\N	2021-02-11 09:28:51	CENTRAL
426	CARICUAO I	5	\N	2021-02-11 09:28:51	CENTRAL CRITICA
444	COCHE	5	\N	2021-02-11 09:28:51	CENTRAL CRITICA
458	FAJARDO II	5	\N	2021-02-11 09:28:51	CENTRAL CRITICA
491	LOS TEQUES EQUIPOS II	5	\N	2021-02-11 09:28:51	CENTRAL CRITICA
494	MADERERO	5	\N	2021-02-11 09:28:51	CENTRAL CRITICA
512	PRADO DE MARIA	5	\N	2021-02-11 09:28:51	CENTRAL CRITICA
519	SAN ANTONIO DE LOS ALTOS	5	\N	2021-02-11 09:28:52	CENTRAL CRITICA
522	SAN MARTIN	5	\N	2021-02-11 09:28:52	CENTRAL CRITICA
535	URL MONTAÑA ALTA	5	\N	2021-02-11 09:28:52	CENTRAL CRITICA
540	Y01 C LAS MINAS CECILIO ACOST FARM LOS C	5	\N	2021-02-11 09:28:52	DLC
541	Y01 C MAIQUETIA AEROPUERTO VIEJO ADUANA	5	\N	2021-02-11 09:28:52	DLC
542	Y01 C PRADO DE MARIA GRAN COLOMBIA	5	\N	2021-02-11 09:28:52	DLC
544	Y01 C SANTA TERESA MONTE CLARO	5	\N	2021-02-11 09:28:52	DLC
545	Y01 CARRIZAL LLANO ALTO	5	\N	2021-02-11 09:28:52	DLC
546	Y02 C CARRIZAL CALLE F  LLANO ALTO	5	\N	2021-02-11 09:28:52	DLC
547	Y02 C FAJARDO TERRAZAS LA VEGA	5	\N	2021-02-11 09:28:52	DLC
549	Y02 C LOS PICACHOS VIA HOYO DE LA PUERTA	5	\N	2021-02-11 09:28:52	DLC
550	Y03 C JARDINES PEÑA ALTA	5	\N	2021-02-11 09:28:52	DLC
551	Y03 C LOS PICACHOS LOMAS MONTE CLARO	5	\N	2021-02-11 09:28:52	DLC
553	Y04 C JARDINES EL CUJI	5	\N	2021-02-11 09:28:52	DLC
554	Y05 C JARDINES PANAMERICANA KM 8	5	\N	2021-02-11 09:28:52	DLC
555	Y05 C PRADO DE MARIA CENTRO AMERICA NOR	5	\N	2021-02-11 09:28:52	DLC
446	CUARTEL DE LA MONTAÑA 4F (NO CANTV)	5	\N	2021-02-11 09:28:52	NO CANTV
455	EL MANGUITO (NO CANTV)	5	\N	2021-02-11 09:28:52	NO CANTV
501	OBSERVATORIO CAJIGAL (NO CANTV)	5	\N	2021-02-11 09:28:52	NO CANTV
425	CARDENAL QUINTERO	5	\N	2021-02-11 09:28:53	NODO OUTDOOR
427	CASALTA III	5	\N	2021-02-11 09:28:53	NODO OUTDOOR
434	CENTRO AMERICA I	5	\N	2021-02-11 09:28:53	NODO OUTDOOR
438	CIUDAD TIUNA CHINA I	5	\N	2021-02-11 09:28:53	NODO OUTDOOR
439	CIUDAD TIUNA CHINA II	5	\N	2021-02-11 09:28:53	NODO OUTDOOR
441	CIUDAD TIUNA CHINA IV	5	\N	2021-02-11 09:28:53	NODO OUTDOOR
442	CIUDAD TIUNA CHINA V	5	\N	2021-02-11 09:28:53	NODO OUTDOOR
449	EL CARMEN	5	\N	2021-02-11 09:28:54	NODO OUTDOOR
450	EL CEMENTERIO	5	\N	2021-02-11 09:28:54	NODO OUTDOOR
456	EL TIGRILLO	5	\N	2021-02-11 09:28:54	NODO OUTDOOR
459	FELIPE ANT ACOSTA NA11-045	5	\N	2021-02-11 09:28:54	NODO OUTDOOR
462	GRAMOVEN	5	\N	2021-02-11 09:28:54	NODO OUTDOOR
463	INTERCOMUNAL MACUTO	5	\N	2021-02-11 09:28:54	NODO OUTDOOR
464	ISAIAS MEDINA	5	\N	2021-02-11 09:28:54	NODO OUTDOOR
466	JUAN VIVE SURIA	5	\N	2021-02-11 09:28:54	NODO OUTDOOR
467	JUSTO BRICEÑO	5	\N	2021-02-11 09:28:54	NODO OUTDOOR
470	LA CUBANA	5	\N	2021-02-11 09:28:54	NODO OUTDOOR
471	LA HOYADA	5	\N	2021-02-11 09:28:54	OAC
473	LA MATICA NA09-009	5	\N	2021-02-11 09:28:54	NODO OUTDOOR
475	LA PIEDRITA	5	\N	2021-02-11 09:28:54	NODO OUTDOOR
480	LAS CASITAS	5	\N	2021-02-11 09:28:55	NODO OUTDOOR
481	LAS QUINTAS	5	\N	2021-02-11 09:28:55	NODO OUTDOOR
484	LOS CORALES CARABALLEDA	5	\N	2021-02-11 09:28:55	NODO OUTDOOR
486	LOS LEONES	5	\N	2021-02-11 09:28:55	NODO OUTDOOR
488	LOS PARAPAROS	5	\N	2021-02-11 09:28:55	NODO OUTDOOR
497	MONTE AVILA	5	\N	2021-02-11 09:28:55	NODO OUTDOOR
498	NIÑO JESUS	5	\N	2021-02-11 09:28:55	NODO OUTDOOR
504	PALMAR ESTE	5	\N	2021-02-11 09:28:55	NODO OUTDOOR
505	PALMAR OESTE	5	\N	2021-02-11 09:28:55	NODO OUTDOOR
509	PARACOTOS III	5	\N	2021-02-11 09:28:55	NODO OUTDOOR
511	PIEDRA AZUL NA10-111	5	\N	2021-02-11 09:28:55	NODO OUTDOOR
520	SAN JOSE DE LOS ALTOS	5	\N	2021-02-11 09:28:56	NODO OUTDOOR
526	SOLIDARIDAD	5	\N	2021-02-11 09:28:56	NODO OUTDOOR
527	TAMANAQUITO	5	\N	2021-02-11 09:28:56	NODO OUTDOOR
529	TERRAZAS DE LA VEGA	5	\N	2021-02-11 09:28:56	NODO OUTDOOR
531	TERRAZAS DEL ALBA II	5	\N	2021-02-11 09:28:56	NODO OUTDOOR
532	TIRIMA	5	\N	2021-02-11 09:28:56	NODO OUTDOOR
533	TURMERITO NA11-001	5	\N	2021-02-11 09:28:56	NODO OUTDOOR
538	VINICIO ADAMES	5	\N	2021-02-11 09:28:56	NODO OUTDOOR
476	LA SABANA	5	\N	2021-02-11 09:28:57	SUPERAULA
2664	Cañada de Urdaneta	30	2021-02-25 11:52:52	2021-02-25 11:52:52	CENTRAL
582	CC CONCRESA	6	\N	2021-03-12 13:44:41	CENTRAL
584	CC LAS AMERICAS SEGUNDA ETAPA	6	\N	2021-03-12 13:45:23	CENTRAL
622	LA LAGUNITA	6	\N	2021-03-12 14:12:21	NODO OUTDOOR
570	AV SUR	6	\N	\N	\N
630	LA TRINIDAD	6	\N	2021-03-12 14:15:23	CENTRAL CRITICA
634	LAS MERCEDES	6	\N	2021-03-12 14:16:19	CENTRAL CRITICA
660	PALO VERDE II	6	\N	2021-03-12 14:24:08	CENTRAL
661	PALO VERDE III	6	\N	2021-03-12 14:24:50	CENTRAL
665	PLAZA	6	\N	2021-03-12 14:27:26	CENTRAL
667	PUEBLO NUEVO I	6	\N	2021-03-12 14:28:27	NODO OUTDOOR
672	RIO NEGRO	6	\N	2021-03-12 14:30:52	NODO OUTDOOR
679	SANTA CRUZ	9	\N	2021-03-16 21:02:22	OPSUT
616	HACIENDA EL CARMEN GAB	6	\N	\N	\N
565	ALTO PRADO	6	\N	2021-02-11 09:28:48	CENTRAL
571	AVE MARIA	6	\N	2021-02-11 09:28:48	CENTRAL
572	BELEN	6	\N	2021-02-11 09:28:48	NODO OUTDOOR
588	CERRO EL INDIO	6	\N	2021-02-11 09:28:49	CENTRAL
589	CERRO EL VAPOR	6	\N	2021-02-11 09:28:49	CENTRAL
591	CORTIJOS 4	6	\N	2021-02-11 09:28:49	CENTRAL
593	DATA CENTER EL HATILLO	6	\N	2021-02-11 09:28:49	CENTRAL
669	REPETIDORA GUATIRE	6	\N	2021-02-11 09:28:49	CENTRAL
607	FILA DE MARICHES KM 8	6	\N	2021-02-11 09:28:50	CENTRAL
612	GUARENAS	6	\N	2021-02-11 09:28:50	CENTRAL
618	HIGUEROTE II OPI	6	\N	2021-02-11 09:28:50	CENTRAL
640	LOS CHORROS	6	\N	2021-02-11 09:28:50	CENTRAL
641	LOS CORTIJOS	6	\N	2021-02-11 09:28:50	OAC
645	LOS ROQUES	6	\N	2021-02-11 09:28:50	CENTRAL
648	MACARACUAY II	6	\N	2021-02-11 09:28:50	CENTRAL
652	MIRANDA	6	\N	2021-02-11 09:28:50	NODO OUTDOOR
581	CC CONCRESA	6	\N	2021-02-11 09:28:50	CENTRAL
659	PALO ALTO	6	\N	2021-02-11 09:28:50	CENTRAL
662	PARQUE LOS ROQUES	6	\N	2021-02-11 09:28:50	CENTRAL
670	REPETIDORA LA SIRIA	6	\N	2021-02-11 09:28:50	CENTRAL
676	SAN JOSE DE RIO CHICO	6	\N	2021-02-11 09:28:51	CENTRAL
682	TACARIGUA I	6	\N	2021-02-11 09:28:51	CENTRAL
683	TACARIGUA II	6	\N	2021-02-11 09:28:51	CENTRAL
694	VALLE ARRIBA GUATIRE	6	\N	2021-02-11 09:28:51	CENTRAL
574	BOLEITA	6	\N	2021-02-11 09:28:51	CENTRAL CRITICA
609	FRANCISCO SALIAS I	6	\N	2021-02-11 09:28:51	CENTRAL CRITICA
610	FRANCISCO SALIAS II	6	\N	2021-02-11 09:28:51	CENTRAL CRITICA
617	HIGUEROTE I	6	\N	2021-02-11 09:28:51	CENTRAL CRITICA
643	LOS PALOS GRANDES	6	\N	2021-02-11 09:28:51	CENTRAL CRITICA
590	CHUAO	6	\N	2021-02-11 09:28:51	CENTRAL CRITICA
663	PETARE I	6	\N	2021-02-11 09:28:52	CENTRAL CRITICA
557	Y08 C PRADO DE MARIA AV NAIGUATA	5	\N	2021-02-11 09:28:52	DLC
558	Y13 C PRADO DE MARIA AV MARIA T TORO	5	\N	2021-02-11 09:28:52	DLC
560	ALCATEL LA UNION	6	\N	2021-02-11 09:28:53	NODO OUTDOOR
561	ALCATEL ORIPOTO III	6	\N	2021-02-11 09:28:53	NODO OUTDOOR
562	ALFREDO SADEL	6	\N	2021-02-11 09:28:53	NODO OUTDOOR
564	ALTO GRANDE	6	\N	2021-02-11 09:28:53	NODO OUTDOOR
567	ALTOS DEL HALCON	6	\N	2021-02-11 09:28:53	NODO OUTDOOR
569	AUYARES	6	\N	2021-02-11 09:28:53	NODO OUTDOOR
576	BUENAVENTURA	6	\N	2021-02-11 09:28:53	NODO OUTDOOR
577	CAICAGUANA	6	\N	2021-02-11 09:28:53	NODO OUTDOOR
578	CALLE RIO PARAGUA	6	\N	2021-02-11 09:28:53	NODO OUTDOOR
580	CAUJARITO	6	\N	2021-02-11 09:28:53	NODO OUTDOOR
583	CC LAGUNITA	6	\N	2021-02-11 09:28:53	NODO OUTDOOR
597	EL ENCANTADO HUMBOLDT	6	\N	2021-02-11 09:28:54	NODO OUTDOOR
598	EL ENCANTADO II	6	\N	2021-02-11 09:28:54	NODO OUTDOOR
602	EL PALMAR I	6	\N	2021-02-11 09:28:54	NODO OUTDOOR
603	EL PALMAR II	6	\N	2021-02-11 09:28:54	NODO OUTDOOR
605	EL RODEO NA19-010	6	\N	2021-02-11 09:28:54	NODO OUTDOOR
611	GUAICAIPURO	6	\N	2021-02-11 09:28:54	NODO OUTDOOR
619	LA ESPERANZA GUATIRE	6	\N	2021-02-11 09:28:54	NODO OUTDOOR
621	LA LAGUNA	6	\N	2021-02-11 09:28:54	NODO OUTDOOR
624	LA LAGUNITA # 6 EL HATILLO	6	\N	2021-02-11 09:28:54	NODO OUTDOOR
625	LA LAGUNITA # 8 EL HATILLO	6	\N	2021-02-11 09:28:54	NODO OUTDOOR
627	LA LAGUNITA AV CUMBRES	6	\N	2021-02-11 09:28:54	NODO OUTDOOR
628	LA TAHONA I	6	\N	2021-02-11 09:28:54	NODO OUTDOOR
629	LA TAHONA II	6	\N	2021-02-11 09:28:54	NODO OUTDOOR
633	LAS MARAVILLAS	6	\N	2021-02-11 09:28:55	NODO OUTDOOR
636	LOMA LINDA	6	\N	2021-02-11 09:28:55	NODO OUTDOOR
637	LOMAS DE BETANIA	6	\N	2021-02-11 09:28:55	NODO OUTDOOR
639	LOMAS DEL SOL	6	\N	2021-02-11 09:28:55	NODO OUTDOOR
649	MAMPORAL	6	\N	2021-02-11 09:28:55	NODO OUTDOOR
650	MATALINDA	6	\N	2021-02-11 09:28:55	NODO OUTDOOR
653	MIRAVILA	6	\N	2021-02-11 09:28:55	NODO OUTDOOR
654	NARANJOS HUMBOLDT	6	\N	2021-02-11 09:28:55	NODO OUTDOOR
656	NUEVA CASARAPA I	6	\N	2021-02-11 09:28:55	NODO OUTDOOR
657	NUEVA CASARAPA II	6	\N	2021-02-11 09:28:55	NODO OUTDOOR
668	PUEBLO NUEVO II	6	\N	2021-02-11 09:28:55	NODO OUTDOOR
674	SALAMANCA	6	\N	2021-02-11 09:28:56	NODO OUTDOOR
677	SANTA BARBARA	6	\N	2021-02-11 09:28:56	NODO OUTDOOR
681	TACARIGUA DE MAMPORAL	6	\N	2021-02-11 09:28:56	NODO OUTDOOR
684	TACARIGUITA	6	\N	2021-02-11 09:28:56	NODO OUTDOOR
685	TERRAFRANCA	6	\N	2021-02-11 09:28:56	NODO OUTDOOR
688	URB COLINAS HATILLO # 18	6	\N	2021-02-11 09:28:56	NODO OUTDOOR
690	URB GRAN VALLE DE CHARAS	6	\N	2021-02-11 09:28:56	NODO OUTDOOR
691	URB LAS MARIAS # 17 HATILLO	6	\N	2021-02-11 09:28:56	NODO OUTDOOR
693	VALLE ALTO	6	\N	2021-02-11 09:28:56	NODO OUTDOOR
586	CCCT	6	\N	2021-02-11 09:28:56	OAC
587	CENTRO GERENCIAL LOS ANDES	6	\N	2021-02-11 09:28:56	OAC
614	GUATIRE	6	\N	2021-02-11 09:28:56	OAC
675	SAN JOSE DE BARLOVENTO	6	\N	2021-02-11 09:28:57	SUPERAULA
746	CARACAS	7	\N	2021-03-09 09:13:41	CENTRAL
761	CC GALERIAS AVILA	7	\N	2021-03-09 09:14:07	CENTRAL
751	CHAGUARAMOS	7	\N	2021-03-09 09:14:57	CENTRAL
753	EL PLACER LOS GUAYABITOS	7	\N	2021-03-09 09:17:24	CENTRAL
752	EL ROSAL	7	\N	2021-03-09 09:18:02	CENTRAL
749	ROMULO GALLEGOS	7	\N	2021-03-09 09:20:46	CENTRAL
791	MACUTO	5	\N	2021-03-12 09:52:52	SUPERAULA
774	CARABALLEDA TX	5	\N	2021-03-09 11:46:14	CENTRAL
778	CATIA LA MAR	8	\N	2021-03-12 09:28:31	CENTRAL CRITICA
787	MACARAO	5	\N	2021-03-12 09:32:51	CENTRAL
792	MACUTO I	5	\N	2021-03-12 09:33:22	CENTRAL
793	MACUTO II	5	\N	2021-03-12 09:33:42	CENTRAL
794	MACUTO III	5	\N	2021-03-12 09:37:30	CENTRAL
800	MARINA GRANDE	5	\N	2021-03-12 09:38:25	CENTRAL
801	NAIGUATA I	5	\N	2021-03-12 09:39:25	CENTRAL
788	MACARAO I	5	\N	2021-03-12 13:27:05	NODO OUTDOOR
809	TANAGUARENA	5	\N	2021-03-12 09:54:50	NODO OUTDOOR
805	SECTOR MACUTO	5	\N	2021-03-12 09:56:55	NODO OUTDOOR
811	URB 10 DE MARZO	5	\N	2021-03-12 12:59:13	NODO OUTDOOR
813	URB PUNTA BRISAS MACUTO	5	\N	2021-03-12 13:02:58	NODO OUTDOOR
777	CARIBE II	5	\N	2021-03-12 13:03:37	NODO OUTDOOR
803	PLAYA GRANDE	5	\N	2021-03-12 13:04:28	NODO OUTDOOR
784	LAS SALINAS	5	\N	2021-03-12 13:09:43	NODO OUTDOOR
808	TACAGUA VIEJA II	5	\N	2021-03-12 13:10:31	NODO OUTDOOR
770	ARRECIFE NA10-124	5	\N	2021-03-12 13:12:44	NODO OUTDOOR
798	MAMO ALTO	5	\N	2021-03-12 13:14:37	NODO OUTDOOR
785	LAS TUNITAS I	5	\N	2021-03-12 13:15:08	NODO OUTDOOR
814	VISTA AL MAR	5	\N	2021-03-12 13:15:28	NODO OUTDOOR
780	CIUDAD CARIBIA II NA11-079	5	\N	2021-03-12 13:16:09	NODO OUTDOOR
806	SOTAVENTO	5	\N	2021-03-12 13:19:26	NODO OUTDOOR
771	BRISAS DE MAIQUETIA	5	\N	2021-03-12 13:20:06	NODO OUTDOOR
804	PLAYA GRANDE II	5	\N	2021-03-12 13:20:29	NODO OUTDOOR
790	MACARAO II	5	\N	2021-03-12 13:23:05	NODO OUTDOOR
773	CAMURI GRANDE	5	\N	2021-03-12 13:23:52	NODO OUTDOOR
781	CIUDAD CARIBIA III	5	\N	2021-03-12 13:24:32	NODO OUTDOOR
775	CARAYACA	5	\N	2021-03-12 13:26:40	NODO OUTDOOR
782	EL JUNKO	8	\N	2021-02-11 09:28:49	CENTRAL
754	REPETIDORA EL VOLCAN	7	\N	2021-02-11 09:28:49	CENTRAL
820	CARTANAL III	6	\N	2021-03-12 13:38:45	NODO OUTDOOR
818	CARTANAL I	6	\N	2021-03-12 13:39:06	NODO OUTDOOR
757	POTRO REDONDO	7	\N	2021-02-11 09:28:50	CENTRAL
741	BAJA FLORIDA	7	\N	2021-02-11 09:28:51	CENTRAL CRITICA
742	CHACAO I	7	\N	2021-02-11 09:28:51	CENTRAL CRITICA
743	CHACAO II	7	\N	2021-02-11 09:28:51	CENTRAL CRITICA
748	LOS CAOBOS	7	\N	2021-02-11 09:28:51	CENTRAL CRITICA
797	MAIQUETIA II	8	\N	2021-02-11 09:28:51	CENTRAL CRITICA
750	SAN AGUSTIN	7	\N	2021-02-11 09:28:51	CENTRAL CRITICA
700	Y01 C CIUDAD FAJARDO URL SHICK	6	\N	2021-02-11 09:28:52	DLC
701	Y01 C CUA NUCLEO LIBERTADOR	6	\N	2021-02-11 09:28:52	DLC
703	Y01 C GUATIRE EDIFICIO 5B LA MOLIENDA	6	\N	2021-02-11 09:28:52	DLC
704	Y01 C HATILLO AV LAS LOMAS LAGUNITA	6	\N	2021-02-11 09:28:52	DLC
705	Y01 C HIGUEROTE CURIEPE	6	\N	2021-02-11 09:28:52	DLC
707	Y01 C TRAPICHITO PARCELA 6 CIUDAD CASARA	6	\N	2021-02-11 09:28:52	DLC
708	Y01 C TRINIDAD LA TAHONA ALTOS SAN GAB	6	\N	2021-02-11 09:28:52	DLC
709	Y01 C URBINA ARAGUANEY VIA GUARENAS 1	6	\N	2021-02-11 09:28:52	DLC
760	Y01C CHACAO SAMBIL CARACAS	7	\N	2021-02-11 09:28:52	DLC
711	Y02 C BOLEITA C RICO	6	\N	2021-02-11 09:28:52	DLC
712	Y02 C CIUDAD FAJARDO URL PLASTEK	6	\N	2021-02-11 09:28:52	DLC
713	Y02 C GUATIRE EDIFICIO 4C LA MOLIENDA	6	\N	2021-02-11 09:28:52	DLC
715	Y02 C HIGUEROTE ENTRADA ARCO CAUCAGUA	6	\N	2021-02-11 09:28:52	DLC
716	Y02 C MACARACUAY EL MORRO	6	\N	2021-02-11 09:28:52	DLC
717	Y02 C TRAPICHITO TERRAZAS DEL ESTE EDF 5	6	\N	2021-02-11 09:28:52	DLC
719	Y02 C URBINA ZONA INDUSTRIAL GUAICOCO	6	\N	2021-02-11 09:28:52	DLC
720	Y03 C CIUDAD FAJARDO URL UNICASA	6	\N	2021-02-11 09:28:52	DLC
721	Y03 C GUATIRE PEDRO CAMEJO	6	\N	2021-02-11 09:28:52	DLC
722	Y03 C HATILLO VIA CAICAGUANA	6	\N	2021-02-11 09:28:52	DLC
723	Y03 C MACARACUAY PABLO VI RES SOFIA	6	\N	2021-02-11 09:28:52	DLC
725	Y03 C URBINA PETARE STA LUCIA PALO VERDE	6	\N	2021-02-11 09:28:52	DLC
759	Y04 C CHACAO LOS ROQUES	7	\N	2021-02-11 09:28:52	DLC
726	Y04 C GUATIRE CENTRO EDUCACIONAL	6	\N	2021-02-11 09:28:52	DLC
728	Y04 C MACARACUAY PABLO VI COL RAUL LEONI	6	\N	2021-02-11 09:28:52	DLC
729	Y04 C TRAPICHITO TERRAZAS MAMPOTE	6	\N	2021-02-11 09:28:52	DLC
730	Y04 C URBINA ENTRADA TERRAZAS GUAICOCO	6	\N	2021-02-11 09:28:52	DLC
731	Y05 C HATILLO ENTRADA LOMAS HALCON	6	\N	2021-02-11 09:28:52	DLC
733	Y05 C TRAPICHITO ZONA INDUS MAMPOTE	6	\N	2021-02-11 09:28:52	DLC
734	Y05 C URBINA URB MIRANDA	6	\N	2021-02-11 09:28:52	DLC
735	Y06 C TRAPICHITO CIUDAD CASARAPA	6	\N	2021-02-11 09:28:52	DLC
736	Y07 C TRAPICHITO TERRAZAS DEL ESTE EDF 4	6	\N	2021-02-11 09:28:52	DLC
738	Y09 C TRAPICHITO CIUD CASARAPA PARC  22	6	\N	2021-02-11 09:28:52	DLC
739	Y10 C TRAPICHITO CIUD CASARAPA PARC  20	6	\N	2021-02-11 09:28:52	DLC
740	Y11 C TRAPICHITO CIUD CASARAPA PARC  14	6	\N	2021-02-11 09:28:52	DLC
764	INSTITUTO IDEA (NO CANTV)	7	\N	2021-02-11 09:28:52	NO CANTV
768	EL GAVILAN LOS GUAYABITOS	7	\N	2021-02-11 09:28:54	NODO OUTDOOR
765	LOS GUAYABITOS	7	\N	2021-02-11 09:28:55	NODO OUTDOOR
769	MERCADO LA HOYADA	7	\N	2021-02-11 09:28:55	NODO OUTDOOR
767	SAN JOSE DEL AVILA	7	\N	2021-02-11 09:28:56	NODO OUTDOOR
695	VEGA ABAJO GUATIRE	6	\N	2021-02-11 09:28:56	NODO OUTDOOR
697	VIRGEN DE LA PAZ	6	\N	2021-02-11 09:28:56	NODO OUTDOOR
758	SAMBIL	7	\N	2021-02-11 09:28:56	OAC
2665	Tamanaco	7	2021-02-25 11:55:30	2021-02-25 11:55:30	CENTRAL
897	BIRUACA II	10	\N	2021-03-04 10:42:31	CENTRAL
938	CORONEL FRANCISCO	10	\N	2021-03-04 11:19:11	CENTRAL
902	EL CRUCERO LORENZO	10	\N	2021-03-04 11:20:27	OPSUT
928	GUARITICO	10	\N	2021-03-04 11:24:12	CENTRAL
907	GUAYABAL	10	\N	2021-03-04 11:25:57	CENTRAL
937	LA CAMPEREÑA	10	\N	2021-03-04 11:31:30	CENTRAL CRITICA
910	LA ESTACADA	10	\N	2021-03-04 11:32:02	NODO OUTDOOR
930	LAS MARAVILLAS	10	\N	2021-03-04 11:38:23	SUPERAULA
912	LOS ALGARROBOS	10	\N	2021-03-04 11:41:51	OPSUT
933	LOS CEDROS	10	\N	2021-03-04 11:42:39	OPSUT
932	LOS CENTAUROS	10	\N	2021-03-04 11:44:14	OPSUT
859	SALAMANCA	9	\N	\N	\N
914	PALMARITO	10	\N	2021-03-04 11:48:54	NODO OUTDOOR
862	SAN JOSE DE BARLOVENTO	9	\N	\N	\N
900	SAN FERNANDO DE APURE III	10	\N	2021-03-04 11:57:47	NODO OUTDOOR
931	SANTA BARBARA	10	\N	2021-03-04 11:58:47	NODO OUTDOOR
821	CARTANAL IV	6	\N	2021-03-12 13:38:24	NODO OUTDOOR
822	CAUCAGUA I	6	\N	2021-03-12 13:40:54	OPSUT
824	CAUCAGÜITA I	6	\N	2021-03-12 13:42:43	NODO OUTDOOR
826	CAUCAGÜITA III	6	\N	2021-03-12 13:43:50	NODO OUTDOOR
827	CC PASEO TUY	6	\N	2021-03-12 13:46:37	NODO OUTDOOR
828	CERRO COCUY	6	\N	2021-03-12 13:47:20	CENTRAL
877	Y01 C CHARALLAVE LOS ANAUCOS	9	\N	2021-03-15 18:45:51	DLC
832	CIUDAD BETANIA	6	\N	2021-03-12 13:50:22	NODO OUTDOOR
837	CIUDAD MIRANDA V	6	\N	2021-03-12 13:56:19	NODO OUTDOOR
835	CIUDAD MIRANDA III	6	\N	2021-03-12 13:56:46	NODO OUTDOOR
834	CIUDAD MIRANDA II	6	\N	2021-03-12 13:57:09	NODO OUTDOOR
838	COLINAS DE BETANIA	6	\N	2021-03-12 13:57:33	NODO OUTDOOR
839	COLINAS DE VALLE ARRIBA	6	\N	2021-03-12 13:57:58	CENTRAL
843	CURIEPE	6	\N	2021-03-12 14:01:12	CENTRAL
844	DOS LAGUNAS	6	\N	2021-03-12 14:01:50	NODO OUTDOOR
847	IZCARAGUA	6	\N	2021-03-12 14:10:36	CENTRAL
849	LA LIMONERA	6	\N	2021-03-12 14:13:15	NODO OUTDOOR
851	LA RAIZA	6	\N	2021-03-12 14:14:08	NODO OUTDOOR
852	LA RAIZA II	6	\N	2021-03-12 14:14:38	NODO OUTDOOR
853	OCUMARE DEL TUY EB A SMITH	6	\N	2021-03-12 14:20:22	SUPERAULA
829	CHARALLAVE	9	\N	2021-03-15 18:45:10	CENTRAL CRITICA
855	OCUMARE III	6	\N	2021-03-12 14:23:24	CENTRAL
857	PARQUE CAIZA	6	\N	2021-03-12 14:25:40	NODO OUTDOOR
860	SAN FERNANDO DEL GUAPO	6	\N	2021-03-12 14:31:28	CENTRAL
863	SANTA LUCIA	6	\N	2021-03-12 14:34:30	CENTRAL
954	ALMACEN ZONA INDUSTRIAL AEROPUERTO	11	\N	2021-02-11 09:28:48	CENTRAL
894	APURITO	10	\N	2021-02-11 09:28:48	OPSUT
959	BEJUMA	11	\N	2021-02-11 09:28:48	CENTRAL
960	BEJUMA I	11	\N	2021-02-11 09:28:48	NODO OUTDOOR
898	CAMAGUAN	10	\N	2021-02-11 09:28:49	OPSUT
865	SANTA LUCIA I	6	\N	2021-03-12 14:35:10	CENTRAL
899	CAZORLA	10	\N	2021-02-11 09:28:49	NODO OUTDOOR
866	SANTA LUCIA II	6	\N	2021-03-12 14:35:43	NODO OUTDOOR
867	SANTA TERESA	6	\N	2021-03-12 14:36:30	CENTRAL
868	SANTA TERESA I	6	\N	2021-03-12 14:36:52	CENTRAL
840	CUA	9	\N	2021-02-11 09:28:49	CENTRAL
841	CUPIRA	9	\N	2021-02-11 09:28:49	SUPERAULA
869	SANTA TERESA II	6	\N	2021-03-12 14:37:21	CENTRAL
872	SOTILLO TERMINAL	6	\N	2021-03-12 14:39:41	CENTRAL
903	EL RECREO	10	\N	2021-02-11 09:28:49	NODO OUTDOOR
904	EL SAMAN	10	\N	2021-02-11 09:28:49	OPSUT
905	EL YAGUAL	10	\N	2021-02-11 09:28:49	CENTRAL
873	TACATA	6	\N	2021-03-12 14:41:03	CENTRAL
874	TACATA PUEBLO	6	\N	2021-03-12 14:41:33	CENTRAL
875	TURUMO	6	\N	2021-03-12 14:42:46	CENTRAL
876	TURUMO TX	6	\N	2021-03-12 14:43:11	CENTRAL
879	Y02 C CHARALLAVE MATA LINDA 1	9	\N	2021-03-15 18:46:20	DLC
886	Y04 C CHARALLAVE PASO REAL	9	\N	2021-03-15 18:48:11	DLC
880	Y02 C CUA SAN JOSE DE SAN MARTIN	6	\N	2021-03-12 14:46:42	DLC
917	PUERTO INFANTE	10	\N	2021-02-11 09:28:50	CENTRAL
881	Y02 C OCUMARE MATA DE COCO	6	\N	2021-03-12 14:48:37	DLC
921	SAN VICENTE	10	\N	2021-02-11 09:28:51	NODO OUTDOOR
922	SANTA CATALINA	10	\N	2021-02-11 09:28:51	CENTRAL
943	SANTA INES	10	\N	2021-02-11 09:28:51	NODO OUTDOOR
882	Y02 C SANTA TERESA DOS LAGUNAS	6	\N	2021-03-12 14:49:07	DLC
884	Y03 C CUA BLOQUE 29	6	\N	2021-03-12 14:50:23	DLC
887	Y04 C CUA TERRAZAS DE CUA	6	\N	2021-03-12 14:52:09	DLC
889	Y05 C CHARALLAVE RIO TUY	6	\N	2021-03-12 14:53:32	DLC
890	Y05 C CUA TACATA	6	\N	2021-03-12 14:53:56	DLC
854	OCUMARE DEL TUY I	9	\N	2021-03-15 18:42:25	CENTRAL
891	ACHAGUAS I	10	\N	2021-02-11 09:28:51	NODO OUTDOOR
956	ARTURO MICHELENA	11	\N	2021-02-11 09:28:51	CENTRAL CRITICA
906	ELORZA	10	\N	2021-02-11 09:28:51	OPSUT
916	PASO CINARUCO	10	\N	2021-02-11 09:28:52	CENTRAL CRITICA
950	SAN BARTOLO	10	\N	2021-02-11 09:28:52	CENTRAL CRITICA
920	SAN JUAN DE PAYARA	10	\N	2021-02-11 09:28:52	NODO OUTDOOR
893	ACHAGUAS II	10	\N	2021-02-11 09:28:52	NODO OUTDOOR
951	ADACA	11	\N	2021-02-11 09:28:52	NODO OUTDOOR
947	ARICHUNA	10	\N	2021-02-11 09:28:53	NODO OUTDOOR
896	BIRUACA I	10	\N	2021-02-11 09:28:53	NODO OUTDOOR
935	EL JOBO	10	\N	2021-02-11 09:28:54	NODO OUTDOOR
941	EL SAMAN I	10	\N	2021-02-11 09:28:54	NODO OUTDOOR
949	FRUTO DE BURRO	10	\N	2021-02-11 09:28:54	NODO OUTDOOR
939	MUCURITAS	10	\N	2021-02-11 09:28:55	NODO OUTDOOR
944	PUERTO MIRANDA	10	\N	2021-02-11 09:28:55	NODO OUTDOOR
940	RIO APURE	10	\N	2021-02-11 09:28:55	NODO OUTDOOR
892	ACHAGUAS	10	\N	2021-02-11 09:28:56	OPSUT
908	GUAYABAL PB	10	\N	2021-02-11 09:28:56	OPSUT
927	LA YEGUERA	10	\N	2021-02-11 09:28:57	OPSUT
926	MATA DE CAÑA	10	\N	2021-02-11 09:28:57	OPSUT
2517	CARIACO	29	\N	2021-03-03 11:03:54	CENTRAL
968	CAMORUCO	11	\N	2021-03-04 14:36:54	NODO OUTDOOR
971	CAMPO CARABOBO	11	\N	2021-03-04 14:37:24	CENTRAL
977	CC GUAPARO	11	\N	2021-03-04 14:39:36	CENTRAL
979	CE LA UNION	11	\N	2021-03-04 14:40:45	NODO OUTDOOR
982	CHIRGUA	11	\N	2021-03-04 14:41:28	CENTRAL
983	CIUDAD ALIANZA	11	\N	2021-03-04 14:42:54	CENTRAL
989	CUMBOTO	11	\N	2021-03-04 14:44:27	CENTRAL
1000	EL TRIGAL	11	\N	2021-03-04 14:46:59	CENTRAL
1007	GUACARA CANTV	11	\N	2021-03-04 14:50:55	OAC
1015	LA ESMERALDA	11	\N	2021-03-04 14:53:08	CENTRAL
1017	LA FLORIDA	11	\N	2021-03-04 14:54:08	NODO OUTDOOR
1042	LOS NARANJOS	11	\N	2021-03-04 15:00:13	NODO OUTDOOR
1044	MANUARE	11	\N	2021-03-04 15:01:52	CENTRAL
1046	MARIARA II	11	\N	2021-03-04 15:02:52	CENTRAL
1050	MONTALBAN	11	\N	2021-03-04 15:04:16	CENTRAL
1055	NAGUANAGUA	11	\N	2021-03-04 15:06:00	CENTRAL
1068	SAN JOAQUIN	11	\N	2021-03-04 15:12:44	CENTRAL
1083	TRAPICHITO	11	\N	2021-03-04 15:17:07	NODO OUTDOOR
1085	U 96 CIUDADELA JOSÉ MARTÍ	11	\N	2021-03-04 15:19:20	NODO OUTDOOR
1090	URBANIZACION CARABOBO	11	\N	2021-03-04 15:21:11	CENTRAL
1091	VALENCIA SUR	11	\N	2021-03-04 15:22:25	CENTRAL
973	CANOABO	11	\N	2021-02-11 09:28:49	SUPERAULA
974	CANOABO I	11	\N	2021-02-11 09:28:49	NODO OUTDOOR
980	CERRO SAN ISIDRO	11	\N	2021-02-11 09:28:49	CENTRAL
981	CHIRGUA	11	\N	2021-02-11 09:28:49	NODO OUTDOOR
994	EL MOLINO	11	\N	2021-02-11 09:28:49	CENTRAL
997	EL PALITO	11	\N	2021-02-11 09:28:49	CENTRAL
1008	GUACARA III	11	\N	2021-02-11 09:28:50	CENTRAL
1014	LA ENTRADA	11	\N	2021-02-11 09:28:50	CENTRAL
1020	LA ISABELICA	11	\N	2021-02-11 09:28:50	CENTRAL
1024	LAS AGUITAS	11	\N	2021-02-11 09:28:50	CENTRAL
1026	LAS INDUSTRIAS	11	\N	2021-02-11 09:28:50	NODO OUTDOOR
1043	MANUARE	11	\N	2021-02-11 09:28:50	NODO OUTDOOR
1047	MIRANDA I	11	\N	2021-02-11 09:28:50	CENTRAL
1051	MONTESERINO	11	\N	2021-02-11 09:28:50	CENTRAL
1052	MORON	11	\N	2021-02-11 09:28:50	CENTRAL
1059	PARAPARAL	11	\N	2021-02-11 09:28:50	CENTRAL
1062	PIEDRAS NEGRAS	11	\N	2021-02-11 09:28:50	CENTRAL
1066	SAN BLAS VALENCIA	11	\N	2021-02-11 09:28:51	CENTRAL
1082	TOCUYITO I (MOVIL LA HONDA)	11	\N	2021-02-11 09:28:51	CENTRAL
1097	Y01 C CAMURUCO HIPERMERCADO EXITO	11	\N	2021-02-11 09:28:52	DLC
1098	Y01 C MICHELENA CC METROPOLIS	11	\N	2021-02-11 09:28:52	DLC
1099	Y01 C MICHELENA CC SAMBIL	11	\N	2021-02-11 09:28:52	DLC
1100	Y01 C MORON URAMA MORON	11	\N	2021-02-11 09:28:52	DLC
1095	WORLD TRADE CENTER (NO CANTV)	11	\N	2021-02-11 09:28:52	NO CANTV
1054	MOVISTAR NODO VALENCIA (NO CANTV)	11	\N	2021-02-11 09:28:52	NODO OUTDOOR
962	BELLO MONTE III	11	\N	2021-02-11 09:28:53	NODO OUTDOOR
964	BORBURATA	11	\N	2021-02-11 09:28:53	NODO OUTDOOR
965	BUCARAL II	11	\N	2021-02-11 09:28:53	NODO OUTDOOR
972	CANAIMA	11	\N	2021-02-11 09:28:53	NODO OUTDOOR
975	CASCABEL	11	\N	2021-02-11 09:28:53	NODO OUTDOOR
985	CIUDAD CHAVEZ III	11	\N	2021-02-11 09:28:53	NODO OUTDOOR
986	CIUDAD PLAZA I	11	\N	2021-02-11 09:28:53	NODO OUTDOOR
988	CIUDAD PLAZA III	11	\N	2021-02-11 09:28:53	NODO OUTDOOR
990	EL BOSQUE	11	\N	2021-02-11 09:28:54	NODO OUTDOOR
993	EL LIMONAL	11	\N	2021-02-11 09:28:54	NODO OUTDOOR
996	EL PAJAL	11	\N	2021-02-11 09:28:54	NODO OUTDOOR
998	EL ROBLE LA PRADERA	11	\N	2021-02-11 09:28:54	NODO OUTDOOR
1001	EL TULIPAN II	11	\N	2021-02-11 09:28:54	NODO OUTDOOR
1002	FELIX POLITO	11	\N	2021-02-11 09:28:54	NODO OUTDOOR
1005	GRANJA CRISTAL	11	\N	2021-02-11 09:28:54	NODO OUTDOOR
1009	GUAICA	11	\N	2021-02-11 09:28:54	NODO OUTDOOR
1011	GUIGUE I	11	\N	2021-02-11 09:28:54	NODO OUTDOOR
1013	JARDIN MAÑONGO	11	\N	2021-02-11 09:28:54	NODO OUTDOOR
1019	LA GUARICHA	11	\N	2021-02-11 09:28:54	NODO OUTDOOR
1022	LA TRIGALEÑA	11	\N	2021-02-11 09:28:55	NODO OUTDOOR
1025	LAS GARCILLERAS	11	\N	2021-02-11 09:28:55	NODO OUTDOOR
1027	LAS JOSEFINAS	11	\N	2021-02-11 09:28:55	NODO OUTDOOR
1030	LAS PALMITAS II	11	\N	2021-02-11 09:28:55	NODO OUTDOOR
1031	LEONARDO CHIRINOS I	11	\N	2021-02-11 09:28:55	NODO OUTDOOR
1033	LIBERTADOR I	11	\N	2021-02-11 09:28:55	NODO OUTDOOR
1034	LIBERTADOR II	11	\N	2021-02-11 09:28:55	NODO OUTDOOR
1036	LOMAS DE FUNVAL	11	\N	2021-02-11 09:28:55	NODO OUTDOOR
1037	LOS CHAGUARAMOS	11	\N	2021-02-11 09:28:55	NODO OUTDOOR
1038	LOS FRAILES	11	\N	2021-02-11 09:28:55	NODO OUTDOOR
1040	LOS GUAYOS II	11	\N	2021-02-11 09:28:55	NODO OUTDOOR
1045	MAÑONGO	11	\N	2021-02-11 09:28:55	NODO OUTDOOR
1057	OASIS	11	\N	2021-02-11 09:28:55	NODO OUTDOOR
1060	PARQUE AZUL	11	\N	2021-02-11 09:28:55	NODO OUTDOOR
1061	PASO REAL	11	\N	2021-02-11 09:28:55	NODO OUTDOOR
1065	RUIZ PINEDA II NA10-118	11	\N	2021-02-11 09:28:55	NODO OUTDOOR
1067	SAN ESTEBAN	11	\N	2021-02-11 09:28:56	NODO OUTDOOR
1070	SAN JOAQUIN II NDA	11	\N	2021-02-11 09:28:56	NODO OUTDOOR
1071	SAN PABLO DE URAMA	11	\N	2021-02-11 09:28:56	NODO OUTDOOR
1075	TACARIGUA	11	\N	2021-02-11 09:28:56	NODO OUTDOOR
1076	TAZAJAL	11	\N	2021-02-11 09:28:56	NODO OUTDOOR
1078	TERRAZAS DE MAÑONGO	11	\N	2021-02-11 09:28:56	NODO OUTDOOR
1079	TERRAZAS DEL COUNTRY	11	\N	2021-02-11 09:28:56	NODO OUTDOOR
1081	TOCORON	11	\N	2021-02-11 09:28:56	NODO OUTDOOR
1084	TULIPAN	11	\N	2021-02-11 09:28:56	NODO OUTDOOR
1088	URB EL PARQUE	11	\N	2021-02-11 09:28:56	NODO OUTDOOR
1089	URB PALMA SOLA ZONA 1	11	\N	2021-02-11 09:28:56	NODO OUTDOOR
1093	VALLES ALTOS	11	\N	2021-02-11 09:28:56	NODO OUTDOOR
1094	VILLA ALIANZA	11	\N	2021-02-11 09:28:56	NODO OUTDOOR
1087	URAMA	11	\N	2021-02-11 09:28:57	SUPERAULA
1225	ESTACIÓN TERRENA CAMATAGUA	13	\N	2021-03-04 11:09:53	CENTRAL CRITICA
1216	EL MEJO	13	\N	2021-03-04 11:17:33	CENTRAL CRITICA
1228	GUAYABAL	13	\N	2021-03-04 12:00:50	NODO OUTDOOR
1197	CAZORLA	13	\N	2021-03-04 12:04:59	NODO OUTDOOR
1149	LIBERTAD	12	\N	2021-03-04 14:17:35	CENTRAL
1160	SAN CARLOS KD-30	12	\N	2021-03-04 14:21:02	CENTRAL
1161	SAN LUIS	12	\N	2021-03-04 14:23:54	NODO OUTDOOR
1232	LA CEIBA	13	\N	\N	\N
1179	CABRUTA I	13	\N	2021-02-11 09:28:49	CENTRAL
1183	CALABOZO II	13	\N	2021-02-11 09:28:49	CENTRAL
1189	CAMATAGUITA	13	\N	2021-02-11 09:28:49	CENTRAL
1194	CARMEN DE CURA	13	\N	2021-02-11 09:28:49	NODO OUTDOOR
1199	CERRO EL PELIGRO	13	\N	2021-02-11 09:28:49	CENTRAL
1200	CERRO LA MINA	13	\N	2021-02-11 09:28:49	CENTRAL
1130	CERRO MORROCOY	12	\N	2021-02-11 09:28:49	CENTRAL
1202	CERRO PARIAPAN	13	\N	2021-02-11 09:28:49	CENTRAL
1203	CERRO PLATILLON	13	\N	2021-02-11 09:28:49	CENTRAL
1132	COJEDITOS	12	\N	2021-02-11 09:28:49	CENTRAL
1210	COR CANTV VALLE DE LA PASCUA	13	\N	2021-02-11 09:28:49	CENTRAL
1214	CUNAGUARO	13	\N	2021-02-11 09:28:49	CENTRAL
1138	EL BAUL	12	\N	2021-02-11 09:28:49	OPSUT
1217	EL MEREY	13	\N	2021-02-11 09:28:49	NODO OUTDOOR
1141	EL PAO I	12	\N	2021-02-11 09:28:49	CENTRAL
1221	EL RASTRO	13	\N	2021-02-11 09:28:49	NODO OUTDOOR
1222	EL SOCORRO	13	\N	2021-02-11 09:28:49	NODO OUTDOOR
1123	ARISMENDI	12	\N	2021-02-11 09:28:49	OPSUT
1229	GUIRIPA	13	\N	2021-02-11 09:28:50	CENTRAL
1236	LEZAMA I	13	\N	2021-02-11 09:28:50	CENTRAL
1237	LEZAMA II	13	\N	2021-02-11 09:28:50	CENTRAL
1155	PUEBLO NUEVO	12	\N	2021-02-11 09:28:50	NODO OUTDOOR
1159	SAN CARLOS I	12	\N	2021-02-11 09:28:51	CENTRAL
1163	TINACO	12	\N	2021-02-11 09:28:51	SUPERAULA
1165	TINAQUILLO	12	\N	2021-02-11 09:28:51	SUPERAULA
1184	CALABOZO III	13	\N	2021-02-11 09:28:51	CENTRAL CRITICA
1170	ALTAGRACIA DE ORITUCO	13	\N	2021-02-11 09:28:51	CENTRAL CRITICA
1205	CHAGUARAMAS I	13	\N	2021-02-11 09:28:51	CENTRAL CRITICA
1223	EL SOMBRERO	13	\N	2021-02-11 09:28:51	NODO OUTDOOR
1158	SAN CARLOS CENTRO	12	\N	2021-02-11 09:28:52	CENTRAL CRITICA
1102	Y02 C CAMORUCO PALMA REAL	11	\N	2021-02-11 09:28:52	DLC
1104	Y02 C MORON BOMBA DE GASOLINA MORON	11	\N	2021-02-11 09:28:52	DLC
1105	Y02 C VALENCIA SUR MERCADO MAYORISTAS	11	\N	2021-02-11 09:28:52	DLC
1106	Y03 C CAMORUCO LAS PALMAS	11	\N	2021-02-11 09:28:52	DLC
1108	Y03 C VALENCIA SUR LOS CHORRITOS	11	\N	2021-02-11 09:28:52	DLC
1109	Y04 C FUNDACION MANZ 5 L FUNVAL	11	\N	2021-02-11 09:28:52	DLC
1110	Y04 C VALENCIA SUR EL CACTUS CALLE 40	11	\N	2021-02-11 09:28:52	DLC
1111	Y05 C FUNDACION MANZ 4 FUNVAL	11	\N	2021-02-11 09:28:52	DLC
1113	Y06 C VALENCIA SUR COROMOTO EL SOCORRO	11	\N	2021-02-11 09:28:52	DLC
1114	Y07 C VALENCIA SUR CARABOBO EL SOCORRO	11	\N	2021-02-11 09:28:52	DLC
1115	Y08 C FUNDACION NORSUR 3 FUNVAL	11	\N	2021-02-11 09:28:52	DLC
1117	Y09 C VALENCIA SUR C 117	11	\N	2021-02-11 09:28:52	DLC
1118	Y10 C FUNDACION LISANDRO ALVARADO TRAPI	11	\N	2021-02-11 09:28:52	DLC
1119	Y10 C VALENCIA SUR TRAPICH II PLAZA IGLE	11	\N	2021-02-11 09:28:52	DLC
1120	Y11 C FUNDACION URB TRAPICHITO ETAPA II	11	\N	2021-02-11 09:28:52	DLC
1195	CASETA TX SAN GERONIMO A (NO CANTV)	13	\N	2021-02-11 09:28:52	NO CANTV
1124	AROITA	12	\N	2021-02-11 09:28:53	NODO OUTDOOR
1171	BAEMARI	13	\N	2021-02-11 09:28:53	NODO OUTDOOR
1173	BAJO IPARE	13	\N	2021-02-11 09:28:53	NODO OUTDOOR
1125	BRISAS DE BUENOS AIRES	12	\N	2021-02-11 09:28:53	NODO OUTDOOR
1185	CALANCHE I	13	\N	2021-02-11 09:28:53	NODO OUTDOOR
1126	CAMPO ALEGRE	12	\N	2021-02-11 09:28:53	NODO OUTDOOR
1191	CAÑAFISTULA	13	\N	2021-02-11 09:28:53	NODO OUTDOOR
1128	CAÑO DE LOS INDIOS	12	\N	2021-02-11 09:28:53	NODO OUTDOOR
1193	CARLOS ANDRES PEREZ	13	\N	2021-02-11 09:28:53	NODO OUTDOOR
1208	CIUDAD IRANI	13	\N	2021-02-11 09:28:53	NODO OUTDOOR
1131	COJEDITO	12	\N	2021-02-11 09:28:53	NODO OUTDOOR
1134	COMPLEJO IRANI 2	12	\N	2021-02-11 09:28:53	NODO OUTDOOR
1135	COMPLEJO IRANI 3	12	\N	2021-02-11 09:28:53	NODO OUTDOOR
1213	CUMBRE	13	\N	2021-02-11 09:28:54	NODO OUTDOOR
1215	EL GUAFAL	13	\N	2021-02-11 09:28:54	NODO OUTDOOR
1142	EL POTRERO	12	\N	2021-02-11 09:28:54	NODO OUTDOOR
1220	EL PUEBLO CAMATAGUA II	13	\N	2021-02-11 09:28:54	NODO OUTDOOR
1143	EZEQUIEL ZAMORA	12	\N	2021-02-11 09:28:54	NODO OUTDOOR
1226	GUAITOITO	13	\N	2021-02-11 09:28:54	NODO OUTDOOR
1230	LA ALGODONERA	13	\N	2021-02-11 09:28:54	NODO OUTDOOR
1231	LA AVANZADA	13	\N	2021-02-11 09:28:54	NODO OUTDOOR
1235	LAS MINAS	13	\N	2021-02-11 09:28:55	NODO OUTDOOR
1148	LAS VEGAS	12	\N	2021-02-11 09:28:55	SUPERAULA
1151	LUIS ARIAS ANDRADE	12	\N	2021-02-11 09:28:55	NODO OUTDOOR
1153	MANRIQUE	12	\N	2021-02-11 09:28:55	NODO OUTDOOR
1157	SAN CARLOS	12	\N	2021-02-11 09:28:56	SUPERAULA
1166	VALLECITO	12	\N	2021-02-11 09:28:56	NODO OUTDOOR
1167	VILLAS DE SANTA MARIA	12	\N	2021-02-11 09:28:56	NODO OUTDOOR
1182	CALABOZO I	13	\N	2021-02-11 09:28:56	OAC
1168	AGUA AMARILLA	13	\N	2021-02-11 09:28:56	OPSUT
1169	AGUA VERDE	13	\N	2021-02-11 09:28:56	OPSUT
1174	BANCO DE SAN PEDRO	13	\N	2021-02-11 09:28:56	OPSUT
1127	CAÑO BENITO	12	\N	2021-02-11 09:28:56	OPSUT
1192	CARDONCITO	13	\N	2021-02-11 09:28:56	OPSUT
1212	COROZOPANDO	13	\N	2021-02-11 09:28:56	OPSUT
1137	EL BARBASCO	12	\N	2021-02-11 09:28:56	OPSUT
1146	LA FE MUJICA	12	\N	2021-02-11 09:28:57	OPSUT
1233	LAS LAJITAS	13	\N	2021-02-11 09:28:57	OPSUT
1188	CAMATAGUA EB PASTOR R MANZO	13	\N	2021-02-11 09:28:57	SUPERAULA
1204	CHAGUARAMA	13	\N	2021-02-11 09:28:57	SUPERAULA
2666	Ciudad Zamora	9	2021-02-25 11:56:54	2021-02-25 11:56:54	CENTRAL
1274	SANTA ROSA	13	\N	2021-03-04 11:49:30	NODO OUTDOOR
1271	SAN SEBASTIAN	13	\N	2021-03-04 11:00:26	CENTRAL
1284	VALLE DE LA PASCUA II	13	\N	2021-03-04 11:07:10	CENTRAL CRITICA
1250	PASO REAL	13	\N	\N	\N
1275	SANTO DOMINGO	13	\N	2021-03-04 12:16:38	OPSUT
1293	ALI PRIMERA	14	\N	2021-03-04 14:23:02	NODO OUTDOOR
1304	CC LOS AVIADORES	14	\N	2021-03-04 14:52:21	NODO OUTDOOR
1316	EL CAMBUR	14	\N	2021-03-04 15:34:36	NODO OUTDOOR
1373	SANTA RITA	14	\N	2021-03-04 15:46:10	NODO OUTDOOR
1361	PALO NEGRO II	14	\N	2021-03-04 15:50:28	CENTRAL
1277	TELEPUERTO SATELITAL BAEMARI (NO CANTV)	13	\N	\N	\N
1358	NUEVA ESPERANZA	14	\N	2021-03-04 15:51:35	NODO OUTDOOR
1279	TUCUPIDO ESC N LOPEZ CAMACHO	13	\N	\N	\N
1341	LAS INDUSTRIAS	14	\N	2021-03-04 15:55:41	CENTRAL
1338	LAS ACACIAS	14	\N	2021-03-04 15:57:21	CENTRAL
1337	LA VICTORIA II	14	\N	2021-03-04 15:59:33	CENTRAL
1325	GUAYABAL	14	\N	2021-03-08 10:15:52	NODO OUTDOOR
1331	LA LAGUNITA	14	\N	2021-03-08 10:19:02	NODO OUTDOOR
1334	LA QUEBRADA	14	\N	2021-03-08 10:19:39	NODO OUTDOOR
1305	CERRO EL CALVARIO	14	\N	2021-02-11 09:28:49	CENTRAL
1306	CERRO EL PORTETE	14	\N	2021-02-11 09:28:49	CENTRAL
1308	CERRO LA CUMBRE	14	\N	2021-02-11 09:28:49	CENTRAL
1309	CHORONI	14	\N	2021-02-11 09:28:49	NODO OUTDOOR
1319	EL CONSEJO I	14	\N	2021-02-11 09:28:49	NODO OUTDOOR
1332	LA MORITA	14	\N	2021-02-11 09:28:50	CENTRAL
1336	LA VICTORIA I	14	\N	2021-02-11 09:28:50	CENTRAL
1343	LOMA DE HIERRO	14	\N	2021-02-11 09:28:50	CENTRAL
1347	LOS SAMANES	14	\N	2021-02-11 09:28:50	CENTRAL
1351	MAGDALENO I	14	\N	2021-02-11 09:28:50	CENTRAL
1241	MATAPALO	13	\N	2021-02-11 09:28:50	CENTRAL
1356	MOROCOPO ALTO	14	\N	2021-02-11 09:28:50	CENTRAL
1246	ORTIZ I	13	\N	2021-02-11 09:28:50	CENTRAL
1247	ORTIZ II	13	\N	2021-02-11 09:28:50	CENTRAL
1360	PALO NEGRO I	14	\N	2021-02-11 09:28:50	CENTRAL
1251	PASO REAL DE MACAIRA	13	\N	2021-02-11 09:28:50	CENTRAL
1254	REPETIDORA TUCUPIDO	13	\N	2021-02-11 09:28:51	CENTRAL
1366	SAMAN DE GUERE	14	\N	2021-02-11 09:28:51	CENTRAL
1259	SAN FRANCISCO DE CARA	13	\N	2021-02-11 09:28:51	CENTRAL
1260	SAN FRANCISCO DE TIZNADOS	13	\N	2021-02-11 09:28:51	CENTRAL
1262	SAN JOSE DE TIZNADOS	13	\N	2021-02-11 09:28:51	CENTRAL
1263	SAN JOSE DE UNARE	13	\N	2021-02-11 09:28:51	CENTRAL
1368	SAN MATEO I	14	\N	2021-02-11 09:28:51	NODO OUTDOOR
1266	SAN RAFAEL DE LAYA	13	\N	2021-02-11 09:28:51	CENTRAL
1267	SAN SEBASTIAN	13	\N	2021-02-11 09:28:51	OPSUT
1272	SANTA MARIA DE IPIRE I	13	\N	2021-02-11 09:28:51	NODO OUTDOOR
1273	SANTA MARIA DE IPIRE II	13	\N	2021-02-11 09:28:51	CENTRAL
1276	TAGUAY	13	\N	2021-02-11 09:28:51	CENTRAL
1285	VALLE MORIN	13	\N	2021-02-11 09:28:51	CENTRAL
1291	ZARAZA I	13	\N	2021-02-11 09:28:51	NODO OUTDOOR
1353	MARACAY II	14	\N	2021-02-11 09:28:51	CENTRAL CRITICA
1283	VALLE DE LA PASCUA I	13	\N	2021-02-11 09:28:51	OAC
1256	SAN CASIMIRO	13	\N	2021-02-11 09:28:52	OPSUT
1280	TUCUPIDO II	13	\N	2021-02-11 09:28:52	CENTRAL CRITICA
1286	Y01 C CHAGUARAMAS ZARAZA	13	\N	2021-02-11 09:28:52	DLC
1287	Y01 C SAN JUAN DE LOS MORROS EL GUAFAL	13	\N	2021-02-11 09:28:52	DLC
1289	Y02 C CHAGUARAMAS CALLE ZARAZA	13	\N	2021-02-11 09:28:52	DLC
1294	ANDRES BELLO S BELLA VISTA	14	\N	2021-02-11 09:28:53	NODO OUTDOOR
1295	ARSENAL	14	\N	2021-02-11 09:28:53	NODO OUTDOOR
1296	BASE LIBERTADOR I	14	\N	2021-02-11 09:28:53	NODO OUTDOOR
1298	BASE SUCRE I	14	\N	2021-02-11 09:28:53	NODO OUTDOOR
1299	BASE SUCRE II	14	\N	2021-02-11 09:28:53	NODO OUTDOOR
1311	CIUDAD JARDIN II	14	\N	2021-02-11 09:28:53	NODO OUTDOOR
1312	CIUDADELA LOTE 14	14	\N	2021-02-11 09:28:53	NODO OUTDOOR
1315	CORINSA II	14	\N	2021-02-11 09:28:53	NODO OUTDOOR
1318	EL CASTAÑO	14	\N	2021-02-11 09:28:54	NODO OUTDOOR
1320	EL CONSEJO II	14	\N	2021-02-11 09:28:54	NODO OUTDOOR
1321	EL HUETE	14	\N	2021-02-11 09:28:54	NODO OUTDOOR
1323	FUNDABARRIOS	14	\N	2021-02-11 09:28:54	NODO OUTDOOR
1324	GUASIMAL	14	\N	2021-02-11 09:28:54	NODO OUTDOOR
1328	LA CABRERA	14	\N	2021-02-11 09:28:54	NODO OUTDOOR
1329	LA CROQUERA	14	\N	2021-02-11 09:28:54	NODO OUTDOOR
1333	LA PLACERA	14	\N	2021-02-11 09:28:54	NODO OUTDOOR
1342	LAS TUNITAS	14	\N	2021-02-11 09:28:55	NODO OUTDOOR
1239	LOS CERRITOS	13	\N	2021-02-11 09:28:55	NODO OUTDOOR
1345	LOS OVEROS	14	\N	2021-02-11 09:28:55	NODO OUTDOOR
1346	LOS OVEROS II	14	\N	2021-02-11 09:28:55	NODO OUTDOOR
1350	MAGDALENO	14	\N	2021-02-11 09:28:55	SUPERAULA
1240	MANUEL SANTA MARIA	13	\N	2021-02-11 09:28:55	NODO OUTDOOR
1354	MONTAÑA FRESCA I	14	\N	2021-02-11 09:28:55	NODO OUTDOOR
1355	MONTAÑA FRESCA II	14	\N	2021-02-11 09:28:55	NODO OUTDOOR
1243	NUEVA ESPERANZA	13	\N	2021-02-11 09:28:55	NODO OUTDOOR
1244	ORTIZ	13	\N	2021-02-11 09:28:55	NODO OUTDOOR
1252	PLAN SECA	13	\N	2021-02-11 09:28:55	NODO OUTDOOR
1363	PRADOS DE LA ENCRUCIJADA II	14	\N	2021-02-11 09:28:55	NODO OUTDOOR
1364	RIO LIMON (ARSENAL II)	14	\N	2021-02-11 09:28:55	NODO OUTDOOR
1365	ROSARIO DE PAYA	14	\N	2021-02-11 09:28:55	NODO OUTDOOR
1258	SAN CASIMIRO II	13	\N	2021-02-11 09:28:56	NODO OUTDOOR
1367	SAN JACINTO	14	\N	2021-02-11 09:28:56	NODO OUTDOOR
1269	SAN SEBASTIAN II	13	\N	2021-02-11 09:28:56	NODO OUTDOOR
1281	UNIVERSITARIO	13	\N	2021-02-11 09:28:56	NODO OUTDOOR
1292	ZARAZA II	13	\N	2021-02-11 09:28:56	NODO OUTDOOR
1300	CAGUA	14	\N	2021-02-11 09:28:56	OAC
1245	ORTIZ (DOS CAMINOS)	13	\N	2021-02-11 09:28:57	OPSUT
1302	CAGUA UNIDAD EDUC TAGUARES	14	\N	2021-02-11 09:28:57	SUPERAULA
1282	VALLE DE LA PASCUA 12 DE OCT	13	\N	2021-02-11 09:28:57	SUPERAULA
2258	LOS GODOS	26	\N	2021-03-03 13:22:55	CENTRAL
1383	TURMERO II	14	\N	2021-03-04 15:39:25	CENTRAL
1449	LAS MERCEDES	15	\N	2021-03-08 12:02:58	NODO OUTDOOR
1452	LAS VIRTUDES	15	\N	2021-03-08 12:04:01	NODO OUTDOOR
1461	MAPARARI I	15	\N	2021-03-08 12:06:40	CENTRAL
1466	OASIS	15	\N	2021-03-08 12:07:35	NODO OUTDOOR
1479	PUNTO FIJO I	15	\N	2021-03-08 12:10:24	OAC
1482	SAN LUIS	15	\N	2021-03-08 12:12:13	OPSUT
1494	URB LA COLONIA	15	\N	2021-03-08 12:18:28	NODO OUTDOOR
1483	SANTA ANA	15	\N	2021-03-08 12:14:39	SUPERAULA
1394	ALMACEN CORO INDUSTRIAL	15	\N	2021-02-11 09:28:48	CENTRAL
1506	ARAURE	16	\N	2021-02-11 09:28:48	CENTRAL
1401	BARAIVED	15	\N	2021-02-11 09:28:48	CENTRAL
1512	BISCUCUY I (MESA DEL ZORRO)	16	\N	2021-02-11 09:28:49	CENTRAL
1404	BISURES	15	\N	2021-02-11 09:28:49	CENTRAL
1515	BOCONOITO	16	\N	2021-02-11 09:28:49	NODO OUTDOOR
1406	BUENA VISTA	15	\N	2021-02-11 09:28:49	SUPERAULA
1410	CAPADARE	15	\N	2021-02-11 09:28:49	NODO OUTDOOR
1411	CAPATARIDA	15	\N	2021-02-11 09:28:49	NODO OUTDOOR
1414	CERRO ROMERO	15	\N	2021-02-11 09:28:49	CENTRAL
1415	CERRO SILENCIO	15	\N	2021-02-11 09:28:49	CENTRAL
1423	COMUNIDAD CARDON	15	\N	2021-02-11 09:28:49	CENTRAL
1424	CORO I	15	\N	2021-02-11 09:28:49	CENTRAL
1428	CURIMAGUA CERRO GALICIA	15	\N	2021-02-11 09:28:49	CENTRAL
1430	DABAJURO I	15	\N	2021-02-11 09:28:49	CENTRAL
1431	DABAJURO II	15	\N	2021-02-11 09:28:49	CENTRAL
1433	EL CHARAL	15	\N	2021-02-11 09:28:49	CENTRAL
1441	LA CRUZ DE TARATARA	15	\N	2021-02-11 09:28:50	OPSUT
1444	LA VELA	15	\N	2021-02-11 09:28:50	CENTRAL
1460	MAPARARI I	15	\N	2021-02-11 09:28:50	OPSUT
1467	PECAYA	15	\N	2021-02-11 09:28:50	CENTRAL
1469	PIRITU	15	\N	2021-02-11 09:28:50	NODO OUTDOOR
1471	PUERTA MARAVEN	15	\N	2021-02-11 09:28:50	CENTRAL
1485	SANTA CRUZ DE BUCARAL	15	\N	2021-02-11 09:28:51	NODO OUTDOOR
1376	TIARA	14	\N	2021-02-11 09:28:51	CENTRAL
1489	TOCOPERO	15	\N	2021-02-11 09:28:51	NODO OUTDOOR
1492	TUCACAS	15	\N	2021-02-11 09:28:51	CENTRAL
1381	TURIAMO	14	\N	2021-02-11 09:28:51	CENTRAL
1382	TURMERO I	14	\N	2021-02-11 09:28:51	CENTRAL
1496	URUMACO	15	\N	2021-02-11 09:28:51	CENTRAL
1391	ZONA INDUSTRIAL II	14	\N	2021-02-11 09:28:51	NODO OUTDOOR
1501	ACARIGUA	16	\N	2021-02-11 09:28:51	CENTRAL CRITICA
1416	CHICHIRIVICHE I	15	\N	2021-02-11 09:28:51	CENTRAL CRITICA
1425	CORO II	15	\N	2021-02-11 09:28:51	CENTRAL CRITICA
1464	MIRIMIRE I	15	\N	2021-02-11 09:28:52	CENTRAL CRITICA
1478	PUNTO FIJO CENTRO	15	\N	2021-02-11 09:28:52	CENTRAL CRITICA
1388	Y02 C TURMERO PAYITA	14	\N	2021-02-11 09:28:52	DLC
1503	AGUA CLARA	16	\N	2021-02-11 09:28:52	NODO OUTDOOR
1504	ALGODONAL	16	\N	2021-02-11 09:28:53	NODO OUTDOOR
1505	ALTOS DE LA COLONIA	16	\N	2021-02-11 09:28:53	NODO OUTDOOR
1396	ANTIGUO AEROPUERTO I	15	\N	2021-02-11 09:28:53	NODO OUTDOOR
1397	ANTIGUO AEROPUERTO II	15	\N	2021-02-11 09:28:53	NODO OUTDOOR
1507	ARAURE 00	16	\N	2021-02-11 09:28:53	NODO OUTDOOR
1508	ARAURE 01	16	\N	2021-02-11 09:28:53	NODO OUTDOOR
1400	AVENIDA TACHIRA	15	\N	2021-02-11 09:28:53	NODO OUTDOOR
1402	BARRIO LA CHINITA	15	\N	2021-02-11 09:28:53	NODO OUTDOOR
1403	BELLA VISTA	15	\N	2021-02-11 09:28:53	NODO OUTDOOR
1511	BISCUCUY I	16	\N	2021-02-11 09:28:53	NODO OUTDOOR
1514	BOCA DE MONTE	16	\N	2021-02-11 09:28:53	NODO OUTDOOR
1408	CAJA DE AGUA I	15	\N	2021-02-11 09:28:53	NODO OUTDOOR
1516	CAMBURITO NORTE	16	\N	2021-02-11 09:28:53	NODO OUTDOOR
1517	CAMBURITO SUR	16	\N	2021-02-11 09:28:53	NODO OUTDOOR
1418	CHURUGUARA 00	15	\N	2021-02-11 09:28:53	NODO OUTDOOR
1419	CHURUGUARA 01	15	\N	2021-02-11 09:28:53	NODO OUTDOOR
1422	CIUDAD PARAGUANA	15	\N	2021-02-11 09:28:53	NODO OUTDOOR
1426	CREOLANDIA	15	\N	2021-02-11 09:28:54	NODO OUTDOOR
1434	EL MAMON	15	\N	2021-02-11 09:28:54	OPSUT
1439	JUAN CRISOSTOMO	15	\N	2021-02-11 09:28:54	NODO OUTDOOR
1442	LA ENCRUCIJADA	15	\N	2021-02-11 09:28:54	OPSUT
1446	LAS EUGENIAS	15	\N	2021-02-11 09:28:55	NODO OUTDOOR
1448	LAS MARGARITAS	15	\N	2021-02-11 09:28:55	NODO OUTDOOR
1450	LAS VELITAS 2 NORTE	15	\N	2021-02-11 09:28:55	NODO OUTDOOR
1453	LOS CACIQUES	15	\N	2021-02-11 09:28:55	NODO OUTDOOR
1454	LOS CARDONES	15	\N	2021-02-11 09:28:55	NODO OUTDOOR
1459	MAPARARI	15	\N	2021-02-11 09:28:55	NODO OUTDOOR
1463	MIRIMIRE	15	\N	2021-02-11 09:28:55	NODO OUTDOOR
1473	PUERTO CUMAREBO 00	15	\N	2021-02-11 09:28:55	NODO OUTDOOR
1474	PUERTO CUMAREBO 01	15	\N	2021-02-11 09:28:55	NODO OUTDOOR
1476	PUNTA CARDON PUEBLO	15	\N	2021-02-11 09:28:55	NODO OUTDOOR
1480	SABANA LARGA	15	\N	2021-02-11 09:28:56	NODO OUTDOOR
1487	SANTA IRENE	15	\N	2021-02-11 09:28:56	NODO OUTDOOR
1488	TACUATO	15	\N	2021-02-11 09:28:56	NODO OUTDOOR
1377	TIQUIERE FLORES	14	\N	2021-02-11 09:28:56	NODO OUTDOOR
1378	TOCORON I SAN LUIS	14	\N	2021-02-11 09:28:56	NODO OUTDOOR
1379	TUCUTUNEMO I	14	\N	2021-02-11 09:28:56	NODO OUTDOOR
1380	TUCUTUNEMO II	14	\N	2021-02-11 09:28:56	NODO OUTDOOR
1386	VILLA ATHENEA	14	\N	2021-02-11 09:28:56	NODO OUTDOOR
1497	VILLA FUTURO	15	\N	2021-02-11 09:28:56	NODO OUTDOOR
1390	ZONA INDUSTRIAL I	14	\N	2021-02-11 09:28:56	NODO OUTDOOR
1500	ZUMURUCUARE	15	\N	2021-02-11 09:28:56	NODO OUTDOOR
1420	CIENAGA	15	\N	2021-02-11 09:28:56	OPSUT
1432	DUVISI	15	\N	2021-02-11 09:28:56	OPSUT
1435	EL PAUJI	15	\N	2021-02-11 09:28:56	OPSUT
1443	LA PEÑA	15	\N	2021-02-11 09:28:57	OPSUT
1455	LOS QUEMAOS	15	\N	2021-02-11 09:28:57	OPSUT
1462	MARIA DIAZ	15	\N	2021-02-11 09:28:57	OPSUT
1384	UE JUAN USLAR EL CONSEJO	14	\N	2021-02-11 09:28:57	SUPERAULA
1526	CORDOVA	16	\N	2021-03-08 10:54:05	CENTRAL
1553	PIRITU	16	\N	2021-03-08 11:12:57	NODO OUTDOOR
1570	TUREN VILLA BRUZUAL	16	\N	2021-03-08 11:19:35	CENTRAL
1656	HUMOCARO BAJO I	17	\N	2021-03-08 11:52:37	CENTRAL
1624	CERRO CUCUTUYO	17	\N	2021-03-08 11:42:12	CENTRAL
1612	BOBARE	17	\N	2021-03-08 11:46:36	CENTRAL
1645	EL PORVENIR	17	\N	2021-03-08 11:46:01	CENTRAL
1593	ALI PRIMERA GTI	17	\N	2021-03-08 12:22:34	NODO OUTDOOR
1613	BOBARE PT	17	\N	2021-03-08 12:26:31	CENTRAL
1639	EL LIMON	17	\N	2021-03-08 12:31:06	NODO OUTDOOR
1586	AGUA NEGRA	17	\N	2021-02-11 09:28:48	CENTRAL
1603	AREGUE	17	\N	2021-02-11 09:28:48	NODO OUTDOOR
1605	ARENALES	17	\N	2021-02-11 09:28:48	CENTRAL
1609	BARBACOAS	17	\N	2021-02-11 09:28:48	CENTRAL
1611	BOBARE	17	\N	2021-02-11 09:28:49	OPSUT
1518	CAMPO ELIAS	16	\N	2021-02-11 09:28:49	NODO OUTDOOR
1622	CERRO ALTO DE LA LAGUNA	17	\N	2021-02-11 09:28:49	CENTRAL
1623	CERRO BUENA VISTA	17	\N	2021-02-11 09:28:49	CENTRAL
1523	CERRO LA CRUZ	16	\N	2021-02-11 09:28:49	CENTRAL
1626	CERRO PALMICHE	17	\N	2021-02-11 09:28:49	CENTRAL
1627	CERRO TUMAQUE	17	\N	2021-02-11 09:28:49	CENTRAL
1524	CHABASQUEN	16	\N	2021-02-11 09:28:49	NODO OUTDOOR
1630	CUBIRO	17	\N	2021-02-11 09:28:49	NODO OUTDOOR
1633	DUACA	17	\N	2021-02-11 09:28:49	SUPERAULA
1636	EL BOJO	17	\N	2021-02-11 09:28:49	CENTRAL
1644	EL PORVENIR	17	\N	2021-02-11 09:28:49	OPSUT
1647	EL ROBLE	17	\N	2021-02-11 09:28:49	NODO OUTDOOR
1608	BARAGUA	17	\N	2021-02-11 09:28:49	CENTRAL
1651	FILA LA QUINTA	17	\N	2021-02-11 09:28:50	CENTRAL
1528	GUACHE	16	\N	2021-02-11 09:28:50	CENTRAL
1531	GUANARITO	16	\N	2021-02-11 09:28:50	SUPERAULA
1532	GUAYABITAL	16	\N	2021-02-11 09:28:50	CENTRAL
1655	HUMOCARO BAJO I	17	\N	2021-02-11 09:28:50	NODO OUTDOOR
1536	LA APARICION	16	\N	2021-02-11 09:28:50	OPSUT
1538	LA MISION	16	\N	2021-02-11 09:28:50	NODO OUTDOOR
1544	MESA DE CAVACAS	16	\N	2021-02-11 09:28:50	CENTRAL
1547	OSPINO	16	\N	2021-02-11 09:28:50	NODO OUTDOOR
1550	PAYARA	16	\N	2021-02-11 09:28:50	NODO OUTDOOR
1557	RIO ACARIGUA	16	\N	2021-02-11 09:28:51	NODO OUTDOOR
1559	SAN FRANCISCO DE MIRANDA	16	\N	2021-02-11 09:28:51	CENTRAL
1560	SAN NICOLAS	16	\N	2021-02-11 09:28:51	CENTRAL
1564	SIPORORO	16	\N	2021-02-11 09:28:51	CENTRAL
1607	AYACUCHO	17	\N	2021-02-11 09:28:51	CENTRAL CRITICA
1618	CABUDARE I	17	\N	2021-02-11 09:28:51	CENTRAL CRITICA
1620	CARORA	17	\N	2021-02-11 09:28:51	SUPERAULA
1563	SANTA ROSALIA	16	\N	2021-02-11 09:28:52	NODO OUTDOOR
1581	Y01 C GUANARE VEREDA D13 JUAN PABLO II	16	\N	2021-02-11 09:28:52	DLC
1582	Y02 C GUANARE VEREDA A8 JUAN PABLO II	16	\N	2021-02-11 09:28:52	DLC
1583	Y03 C GUANARE JUAN PABLO II	16	\N	2021-02-11 09:28:52	DLC
1565	TEMPLO VOTIVO GUANARE (NO CANTV)	16	\N	2021-02-11 09:28:52	NO CANTV
1584	24 DE JULIO	17	\N	2021-02-11 09:28:52	NODO OUTDOOR
1587	AGUA VIVA	17	\N	2021-02-11 09:28:53	NODO OUTDOOR
1589	AGUA VIVA LAS CUIBAS III	17	\N	2021-02-11 09:28:53	NODO OUTDOOR
1590	AGUA VIVA LAS TUNAS II	17	\N	2021-02-11 09:28:53	NODO OUTDOOR
1594	ALI PRIMERA I	17	\N	2021-02-11 09:28:53	NODO OUTDOOR
1595	ALI PRIMERA II	17	\N	2021-02-11 09:28:53	NODO OUTDOOR
1597	ALI PRIMERA IV	17	\N	2021-02-11 09:28:53	NODO OUTDOOR
1598	ALI PRIMERA V	17	\N	2021-02-11 09:28:53	NODO OUTDOOR
1600	ANDRES BELLO II	17	\N	2021-02-11 09:28:53	NODO OUTDOOR
1601	ANTONIO JOSE DE SUCRE	17	\N	2021-02-11 09:28:53	NODO OUTDOOR
1604	ARENA PLAZA	17	\N	2021-02-11 09:28:53	NODO OUTDOOR
1614	BOLIVAR I	17	\N	2021-02-11 09:28:53	NODO OUTDOOR
1616	BRISAS DE CARORITA	17	\N	2021-02-11 09:28:53	NODO OUTDOOR
1617	BRISAS DEL TURBIO	17	\N	2021-02-11 09:28:53	NODO OUTDOOR
1621	CASPO ABAJO	17	\N	2021-02-11 09:28:53	NODO OUTDOOR
1521	CC BUENAVENTURA	16	\N	2021-02-11 09:28:53	NODO OUTDOOR
1525	CHORO GONZALERO	16	\N	2021-02-11 09:28:53	NODO OUTDOOR
1629	COCO E MONO	17	\N	2021-02-11 09:28:53	NODO OUTDOOR
1634	DUACA I	17	\N	2021-02-11 09:28:54	NODO OUTDOOR
1635	DUACA II	17	\N	2021-02-11 09:28:54	NODO OUTDOOR
1527	EL LIBERTADOR	16	\N	2021-02-11 09:28:54	NODO OUTDOOR
1641	EL MANZANO II	17	\N	2021-02-11 09:28:54	NODO OUTDOOR
1643	EL PLACER	17	\N	2021-02-11 09:28:54	NODO OUTDOOR
1652	GAMELOTAL	17	\N	2021-02-11 09:28:54	NODO OUTDOOR
1653	GLORIA DEL SUR	17	\N	2021-02-11 09:28:54	NODO OUTDOOR
1533	IRANI I	16	\N	2021-02-11 09:28:54	NODO OUTDOOR
1534	IRANI II	16	\N	2021-02-11 09:28:54	NODO OUTDOOR
1537	LA GRANJA	16	\N	2021-02-11 09:28:54	NODO OUTDOOR
1540	LAS GUAFITAS	16	\N	2021-02-11 09:28:55	NODO OUTDOOR
1541	LOS UNIDOS	16	\N	2021-02-11 09:28:55	NODO OUTDOOR
1542	MARIA LAYA	16	\N	2021-02-11 09:28:55	NODO OUTDOOR
1548	PALMA REAL	16	\N	2021-02-11 09:28:55	NODO OUTDOOR
1555	PRADOS DEL SOL MORICHAL	16	\N	2021-02-11 09:28:55	NODO OUTDOOR
1561	SAN RAFAEL DE ONOTO	16	\N	2021-02-11 09:28:56	SUPERAULA
1566	TRICENTENARIA	16	\N	2021-02-11 09:28:56	NODO OUTDOOR
1571	URB EL PASEO	16	\N	2021-02-11 09:28:56	NODO OUTDOOR
1572	VALLE ARRIBA	16	\N	2021-02-11 09:28:56	NODO OUTDOOR
1574	VILLA ARAURE I	16	\N	2021-02-11 09:28:56	NODO OUTDOOR
1575	VILLA COLONIAL	16	\N	2021-02-11 09:28:56	NODO OUTDOOR
1577	VILLA DEL PILAR II	16	\N	2021-02-11 09:28:56	NODO OUTDOOR
1578	VILLA DEL PILAR III	16	\N	2021-02-11 09:28:56	NODO OUTDOOR
1648	EL TOCUYO	17	\N	2021-02-11 09:28:56	SUPERAULA
1545	MESA DEL ZORRO	16	\N	2021-02-11 09:28:57	OPSUT
1568	TUCUPIDO	16	\N	2021-02-11 09:28:57	OPSUT
1529	GUANARE	16	\N	2021-02-11 09:28:57	SUPERAULA
1569	TUREN	16	\N	2021-02-11 09:28:57	SUPERAULA
1756	AROA GTI	18	\N	2021-03-08 10:25:42	CENTRAL
1774	JUAN JOSÉ DE MAYA I	18	\N	2021-03-08 10:35:07	NODO OUTDOOR
1775	JUAN JOSÉ DE MAYA II	18	\N	2021-03-08 10:35:33	NODO OUTDOOR
1786	PRADOS DEL NORTE	18	\N	2021-03-08 10:38:28	NODO OUTDOOR
1791	SAN PABLO	18	\N	2021-03-08 10:40:44	CENTRAL
1458	LOS TAQUES	15	\N	2021-03-08 12:05:53	CENTRAL
1666	LA ESTANCIA	17	\N	2021-03-08 12:36:55	NODO OUTDOOR
1674	LA PLAYA	17	\N	2021-03-08 12:39:19	NODO OUTDOOR
1681	LAS ACACIAS	17	\N	2021-03-08 12:40:21	NODO OUTDOOR
1686	LAS PALMITAS	17	\N	2021-03-08 12:41:25	NODO OUTDOOR
1691	LOS MANGOS	17	\N	2021-03-08 12:42:17	NODO OUTDOOR
1720	ROMULO GALLEGOS	17	\N	2021-03-08 12:47:45	NODO OUTDOOR
1724	SANTA ELENA	17	\N	2021-03-08 12:49:01	CENTRAL CRITICA
1725	SANTA INES	17	\N	2021-03-08 12:49:38	NODO OUTDOOR
1754	ZONA INDUSTRIAL II	17	\N	2021-03-08 12:56:49	CENTRAL
1805	ALTA VISTA	19	\N	2021-03-09 09:35:02	CENTRAL
1761	CERRO EL PICACHO	18	\N	2021-02-11 09:28:49	CENTRAL
1763	CHIVACOA DIG	18	\N	2021-02-11 09:28:49	CENTRAL
1765	CHIVACOA TX	18	\N	2021-02-11 09:28:49	CENTRAL
1766	COCOROTE	18	\N	2021-02-11 09:28:49	CENTRAL
1769	FARRIAR	18	\N	2021-02-11 09:28:50	NODO OUTDOOR
1663	LA CARUCIEÑA	17	\N	2021-02-11 09:28:50	CENTRAL
1664	LA COSTA	17	\N	2021-02-11 09:28:50	CENTRAL
1673	LA PIEDAD	17	\N	2021-02-11 09:28:50	CENTRAL
1778	LA RUEDA	18	\N	2021-02-11 09:28:50	CENTRAL
1690	LOS CREPUSCULOS	17	\N	2021-02-11 09:28:50	CENTRAL
1783	MARIN	18	\N	2021-02-11 09:28:50	NODO OUTDOOR
1697	MOROTURO	17	\N	2021-02-11 09:28:50	CENTRAL
1785	NIRGUA TX	18	\N	2021-02-11 09:28:50	CENTRAL
1702	PATA E PALO	17	\N	2021-02-11 09:28:50	CENTRAL
1714	QUIBOR I	17	\N	2021-02-11 09:28:50	CENTRAL
1717	QUIBOR TX	17	\N	2021-02-11 09:28:50	CENTRAL
1718	RIO CLARO	17	\N	2021-02-11 09:28:51	NODO OUTDOOR
1787	SABANA DE PARRA	18	\N	2021-02-11 09:28:51	NODO OUTDOOR
1788	SALOM	18	\N	2021-02-11 09:28:51	NODO OUTDOOR
1723	SANARE	17	\N	2021-02-11 09:28:51	NODO OUTDOOR
1728	SARARE	17	\N	2021-02-11 09:28:51	OPSUT
1736	TORRE CANTV	17	\N	2021-02-11 09:28:51	CENTRAL
1794	TOTUMILLO	18	\N	2021-02-11 09:28:51	CENTRAL
1737	UNION	17	\N	2021-02-11 09:28:51	CENTRAL
1741	VERSALLES	17	\N	2021-02-11 09:28:51	CENTRAL
1798	YARITAGUA DIG	18	\N	2021-02-11 09:28:51	CENTRAL
1800	YUMARE	18	\N	2021-02-11 09:28:51	SUPERAULA
1789	SAN FELIPE	18	\N	2021-02-11 09:28:51	OAC
1749	Y01  EL CERCADO I	17	\N	2021-02-11 09:28:52	DLC
1750	Y02  EL CERCADO II	17	\N	2021-02-11 09:28:52	DLC
1792	SANTA CLARA (NO CANTV)	18	\N	2021-02-11 09:28:52	NO CANTV
1801	25 DE MARZO III	19	\N	2021-02-11 09:28:52	NODO OUTDOOR
1802	25 MARZO I	19	\N	2021-02-11 09:28:52	NODO OUTDOOR
1758	CAMBURAL	18	\N	2021-02-11 09:28:53	NODO OUTDOOR
1760	CAÑAVERAL	18	\N	2021-02-11 09:28:53	NODO OUTDOOR
1767	EL CHARITO	18	\N	2021-02-11 09:28:54	NODO OUTDOOR
1772	HUGO CHAVEZ FRIAS II	18	\N	2021-02-11 09:28:54	NODO OUTDOOR
1773	HUGO CHAVEZ FRIAS III	18	\N	2021-02-11 09:28:54	NODO OUTDOOR
1659	LA APOSTOLEÑA I	17	\N	2021-02-11 09:28:54	NODO OUTDOOR
1660	LA APOSTOLEÑA II	17	\N	2021-02-11 09:28:54	NODO OUTDOOR
1662	LA BATALLA II	17	\N	2021-02-11 09:28:54	NODO OUTDOOR
1665	LA CUCHILLA	17	\N	2021-02-11 09:28:54	NODO OUTDOOR
1667	LA FLORESTA	17	\N	2021-02-11 09:28:54	NODO OUTDOOR
1668	LA LIBERTAD	17	\N	2021-02-11 09:28:54	NODO OUTDOOR
1670	LA MIEL	17	\N	2021-02-11 09:28:54	NODO OUTDOOR
1676	LA SABILA II	17	\N	2021-02-11 09:28:54	NODO OUTDOOR
1677	LA SABILA III	17	\N	2021-02-11 09:28:54	NODO OUTDOOR
1679	LA TOMATERA	17	\N	2021-02-11 09:28:54	NODO OUTDOOR
1779	LA VIRGEN	18	\N	2021-02-11 09:28:55	NODO OUTDOOR
1682	LAS CASITAS DEL CUJI I	17	\N	2021-02-11 09:28:55	NODO OUTDOOR
1684	LAS CASITAS DEL CUJI III	17	\N	2021-02-11 09:28:55	NODO OUTDOOR
1687	LAS VERITAS	17	\N	2021-02-11 09:28:55	NODO OUTDOOR
1688	LOMAS DE LEON	17	\N	2021-02-11 09:28:55	NODO OUTDOOR
1781	LOS MANGOS II	18	\N	2021-02-11 09:28:55	NODO OUTDOOR
1692	LOS POCITOS	17	\N	2021-02-11 09:28:55	NODO OUTDOOR
1694	LOS VENEZOLANOS PRIMERO	17	\N	2021-02-11 09:28:55	NODO OUTDOOR
1695	MERCABAR	17	\N	2021-02-11 09:28:55	NODO OUTDOOR
1699	MOYETONES II	17	\N	2021-02-11 09:28:55	NODO OUTDOOR
1700	NARANJILLOS	17	\N	2021-02-11 09:28:55	NODO OUTDOOR
1704	PILA DE MONTEZUMA	17	\N	2021-02-11 09:28:55	NODO OUTDOOR
1705	PLAZA MAYOR	17	\N	2021-02-11 09:28:55	NODO OUTDOOR
1707	PRADO DE OCCIDENTE	17	\N	2021-02-11 09:28:55	NODO OUTDOOR
1708	PRADOS DEL NORTE	17	\N	2021-02-11 09:28:55	NODO OUTDOOR
1710	PURICAURE	17	\N	2021-02-11 09:28:55	NODO OUTDOOR
1712	QUEBRADA DE ORO	17	\N	2021-02-11 09:28:55	NODO OUTDOOR
1722	SAN JOSE OBRERO	17	\N	2021-02-11 09:28:56	NODO OUTDOOR
1727	SANTA TERESA DE BORO	17	\N	2021-02-11 09:28:56	NODO OUTDOOR
1733	TAMARINDO	17	\N	2021-02-11 09:28:56	NODO OUTDOOR
1734	TIERRA NEGRA	17	\N	2021-02-11 09:28:56	NODO OUTDOOR
1738	URB VILLA PRODUCTIVA	17	\N	2021-02-11 09:28:56	NODO OUTDOOR
1739	VALLE LINDO I	17	\N	2021-02-11 09:28:56	NODO OUTDOOR
1742	VILLA ACROPOLIS	17	\N	2021-02-11 09:28:56	NODO OUTDOOR
1743	VILLA CANTEVISTA	17	\N	2021-02-11 09:28:56	NODO OUTDOOR
1745	VILLA CREPUSCULAR II	17	\N	2021-02-11 09:28:56	NODO OUTDOOR
1796	VILLAS DE YARA	18	\N	2021-02-11 09:28:56	NODO OUTDOOR
1747	VISTA VERDE	17	\N	2021-02-11 09:28:56	NODO OUTDOOR
1752	YUCATAN I	17	\N	2021-02-11 09:28:56	NODO OUTDOOR
1762	CHIVACOA	18	\N	2021-02-11 09:28:56	SUPERAULA
1797	YARITAGUA	18	\N	2021-02-11 09:28:56	SUPERAULA
1715	QUIBOR PB	17	\N	2021-02-11 09:28:57	OPSUT
1716	QUIBOR PT	17	\N	2021-02-11 09:28:57	OPSUT
1730	SARARE PT	17	\N	2021-02-11 09:28:57	OPSUT
344	EL VIGIA	4	\N	2021-03-03 15:04:47	OPSUT
1821	DOÑA BARBARA	19	\N	2021-03-09 09:47:27	CENTRAL
1822	EDELCA CALLAO CVG (NO CANTV)	19	\N	2021-03-09 09:51:05	NO CANTV
1826	EL LIMON	19	\N	2021-03-09 09:55:02	NODO OUTDOOR
1848	LOS NARANJOS	19	\N	2021-03-09 10:08:51	NODO OUTDOOR
1851	LOS OLIVOS II	19	\N	2021-03-09 10:10:08	CENTRAL
1858	MOVISTAR PUERTO ORDAZ (NO CANTV)	19	\N	2021-03-09 10:15:36	NODO OUTDOOR
1863	PIACOA GTI	19	\N	2021-03-09 10:20:41	NODO OUTDOOR
1838	GUASIPATI	19	\N	\N	\N
1884	TX TORRE ALFEREZ (NO CANTV)	19	\N	2021-03-09 10:35:05	NO CANTV
1906	5 DE JULIO	20	\N	2021-03-09 10:47:42	CENTRAL
1921	CIUDAD PIAR	20	\N	2021-03-09 11:00:39	CENTRAL
1929	LA ESMERALDA	20	\N	2021-03-09 11:13:13	CENTRAL CRITICA
1831	EL ROBLE	19	\N	2021-03-19 11:23:45	CENTRAL
1873	SAN FRANCISCO DE CUCHIVERO	22	\N	2021-03-19 10:04:11	OPSUT
1867	PUERTO NUEVO (EL BURRO)	22	\N	2021-03-19 10:05:11	OPSUT
1930	LA FLORIDA	22	\N	2021-03-19 10:07:16	NODO OUTDOOR
1813	CERRO ALGARROBO	19	\N	2021-02-11 09:28:49	CENTRAL
1917	CERRO BOLIVAR	20	\N	2021-02-11 09:28:49	CENTRAL
1918	CERRO EL CLUECO	20	\N	2021-02-11 09:28:49	CENTRAL
1814	CHIRICA	19	\N	2021-02-11 09:28:49	CENTRAL
1920	CIUDAD PIAR	20	\N	2021-02-11 09:28:49	NODO OUTDOOR
1820	DOÑA BARBARA	19	\N	2021-02-11 09:28:49	NODO OUTDOOR
1823	EDIFICIO BUDAPEST	19	\N	2021-02-11 09:28:49	CENTRAL
1825	EL DORADO	19	\N	2021-02-11 09:28:49	CENTRAL
1829	EL PALMAR	19	\N	2021-02-11 09:28:49	NODO OUTDOOR
1830	EL PAO II	19	\N	2021-02-11 09:28:49	NODO OUTDOOR
1932	LA PARAGUA	20	\N	2021-02-11 09:28:49	CENTRAL
1837	GUASIPATI	19	\N	2021-02-11 09:28:50	NODO OUTDOOR
1935	LAS MOREAS	20	\N	2021-02-11 09:28:50	CENTRAL
1849	LOS OLIVOS I	19	\N	2021-02-11 09:28:50	CENTRAL
1856	MATANZAS NORTE	19	\N	2021-02-11 09:28:50	CENTRAL
1872	SAN FELIX	19	\N	2021-02-11 09:28:51	OAC
1876	SANTA ELENA DE UAIREN EDELCA	19	\N	2021-02-11 09:28:51	CENTRAL
1883	TUMEREMO	19	\N	2021-02-11 09:28:51	SUPERAULA
1885	UD-145	19	\N	2021-02-11 09:28:51	CENTRAL
1886	UD-146	19	\N	2021-02-11 09:28:51	CENTRAL
1888	UPATA	19	\N	2021-02-11 09:28:51	SUPERAULA
1824	EL CALLAO	19	\N	2021-02-11 09:28:51	CENTRAL CRITICA
1846	LAS CLARITAS CVG (NO CANTV)	19	\N	2021-02-11 09:28:52	CENTRAL CRITICA
1855	MARIPA	19	\N	2021-02-11 09:28:52	SUPERAULA
1940	MOVIL CAICARA DEL ORINOCO II	20	\N	2021-02-11 09:28:52	CENTRAL CRITICA
1819	DLC 06 CC ORINOKIA	19	\N	2021-02-11 09:28:52	DLC
1898	Y02 C UNARE MANZ 29 SECTOR CORE 8	19	\N	2021-02-11 09:28:52	DLC
1899	Y03 C UNARE MANZ 5 SECTOR CORE 8	19	\N	2021-02-11 09:28:52	DLC
1900	Y04 C UNARE MANZ 8 SECTOR CORE 8	19	\N	2021-02-11 09:28:52	DLC
1905	12 DE OCTUBRE	20	\N	2021-02-11 09:28:52	NODO OUTDOOR
1803	25 MARZO II	19	\N	2021-02-11 09:28:52	NODO OUTDOOR
1804	4 DE FEBRERO	19	\N	2021-02-11 09:28:52	NODO OUTDOOR
1908	ALTOS DE CAYAURIMA	20	\N	2021-02-11 09:28:53	NODO OUTDOOR
1806	AMAZONAS	19	\N	2021-02-11 09:28:53	NODO OUTDOOR
1909	BETANIA DEL TOPOCHO	20	\N	2021-02-11 09:28:53	NODO OUTDOOR
1810	BRICEÑO 01	19	\N	2021-02-11 09:28:53	NODO OUTDOOR
1911	CAICARA DEL ORINOCO	20	\N	2021-02-11 09:28:53	OPSUT
1811	CARONI	19	\N	2021-02-11 09:28:53	NODO OUTDOOR
1914	CAYAURIMA	20	\N	2021-02-11 09:28:53	NODO OUTDOOR
1815	CHIRICA VIEJA	19	\N	2021-02-11 09:28:53	NODO OUTDOOR
1817	CORE 8	19	\N	2021-02-11 09:28:53	NODO OUTDOOR
1923	EL CRISTO	20	\N	2021-02-11 09:28:54	NODO OUTDOOR
1833	FE Y ALEGRIA	19	\N	2021-02-11 09:28:54	NODO OUTDOOR
1834	FLORIDA COUNTRY	19	\N	2021-02-11 09:28:54	NODO OUTDOOR
1835	FRANCISCA DUARTE	19	\N	2021-02-11 09:28:54	NODO OUTDOOR
1925	GURI	20	\N	2021-02-11 09:28:54	NODO OUTDOOR
1839	INES ROMERO I	19	\N	2021-02-11 09:28:54	NODO OUTDOOR
1841	INES ROMERO III	19	\N	2021-02-11 09:28:54	NODO OUTDOOR
1926	LA CANDELARIA I	20	\N	2021-02-11 09:28:54	NODO OUTDOOR
1842	LA FABRICA	19	\N	2021-02-11 09:28:54	NODO OUTDOOR
1931	LA LLOVIZNA	20	\N	2021-02-11 09:28:54	NODO OUTDOOR
1844	LA VISTA	19	\N	2021-02-11 09:28:55	NODO OUTDOOR
1847	LIBERTADOR	19	\N	2021-02-11 09:28:55	NODO OUTDOOR
1936	LIMON DE PARHUEÑA	20	\N	2021-02-11 09:28:55	NODO OUTDOOR
1852	LOS PINOS	19	\N	2021-02-11 09:28:55	NODO OUTDOOR
1937	MARHUANTA	20	\N	2021-02-11 09:28:55	NODO OUTDOOR
1939	MONTE BLANCO	20	\N	2021-02-11 09:28:55	NODO OUTDOOR
1941	MOVIL LA PARAGUA	20	\N	2021-02-11 09:28:55	NODO OUTDOOR
1860	NIETO GIL	19	\N	2021-02-11 09:28:55	NODO OUTDOOR
1861	ORINOCO	19	\N	2021-02-11 09:28:55	NODO OUTDOOR
1864	PIAR	19	\N	2021-02-11 09:28:55	NODO OUTDOOR
1866	PORTO BELLO	19	\N	2021-02-11 09:28:55	NODO OUTDOOR
1870	RIO GRANDE	19	\N	2021-02-11 09:28:55	NODO OUTDOOR
1871	RIVERAS DEL CARONI	19	\N	2021-02-11 09:28:55	NODO OUTDOOR
1877	SIERRA IMATACA	19	\N	2021-02-11 09:28:56	NODO OUTDOOR
1878	SIERRA PARIMA	19	\N	2021-02-11 09:28:56	NODO OUTDOOR
1880	TRES RAICES	19	\N	2021-02-11 09:28:56	NODO OUTDOOR
1881	TRIUNFITO	19	\N	2021-02-11 09:28:56	NODO OUTDOOR
1889	URB GUAYANA COUNTRY CLUB	19	\N	2021-02-11 09:28:56	NODO OUTDOOR
1891	VENEZUELA	19	\N	2021-02-11 09:28:56	NODO OUTDOOR
1892	VILLA BETANIA	19	\N	2021-02-11 09:28:56	NODO OUTDOOR
1893	VILLA CARUACHI	19	\N	2021-02-11 09:28:56	NODO OUTDOOR
1895	VISTA AL SOL II	19	\N	2021-02-11 09:28:56	NODO OUTDOOR
1896	VISTA DEL SOL I	19	\N	2021-02-11 09:28:56	NODO OUTDOOR
1903	ZONA IND 25 DE MARZO II	19	\N	2021-02-11 09:28:56	NODO OUTDOOR
1904	ZONA IND 25 DE MARZO III	19	\N	2021-02-11 09:28:56	NODO OUTDOOR
1812	CC BABILONIA	19	\N	2021-02-11 09:28:56	OAC
1942	PARIA	20	\N	2021-02-11 09:28:57	SUPERAULA
2485	SANTA ELENA	28	\N	2021-03-04 09:44:01	CENTRAL
1955	SAN FCO PARAGUA	20	\N	2021-03-09 11:25:22	NODO OUTDOOR
1961	SOLEDAD II GTI	20	\N	2021-03-09 11:28:38	NODO OUTDOOR
1963	VISTA HERMOSA	20	\N	2021-03-09 11:31:21	CENTRAL
1964	AEROPUERTO LA CHINITA	30	\N	2021-03-16 14:42:39	NODO OUTDOOR
1965	ALTOS DE MARACAIBO	30	\N	2021-03-16 14:43:05	NODO OUTDOOR
1968	AVENIDA 6	30	\N	2021-03-16 14:54:08	NODO OUTDOOR
1969	BAJO SECO	30	\N	2021-03-16 14:54:30	NODO OUTDOOR
1971	BARRIO CHINO	30	\N	2021-03-16 14:55:35	NODO OUTDOOR
1972	BARRIO MODELO	30	\N	2021-03-16 14:56:15	NODO OUTDOOR
1974	BARRIO SECO	30	\N	2021-03-16 14:57:00	NODO OUTDOOR
1975	BARRIO TORITO FERNANDEZ	30	\N	2021-03-16 14:57:21	NODO OUTDOOR
1978	CALENDARIO	30	\N	2021-03-16 14:58:32	NODO OUTDOOR
1980	CAMPO BOSCAN	30	\N	2021-03-16 15:00:55	CENTRAL
1981	CAMPO LA CONCEPCION	30	\N	2021-03-16 15:02:58	NODO OUTDOOR
1984	CARRASQUERO	30	\N	2021-03-16 15:06:27	NODO OUTDOOR
1985	CASSIANO LOZADA I	30	\N	2021-03-16 15:06:55	NODO OUTDOOR
1987	CASSIANO LOZADA III	30	\N	2021-03-16 15:07:17	NODO OUTDOOR
1988	CAUJARITO NUEVO	30	\N	2021-03-16 15:08:09	NODO OUTDOOR
1989	CC SALTO ANGEL	30	\N	2021-03-16 15:08:48	CENTRAL
1993	CHINO JULIO	30	\N	2021-03-16 15:12:24	NODO OUTDOOR
1995	CIUDAD LOSSADA	30	\N	2021-03-16 15:12:56	NODO OUTDOOR
1996	CONCEPCION LA CAÑADA	30	\N	2021-03-16 15:13:07	CENTRAL
2026	LAS INDUSTRIAS	21	\N	\N	\N
1998	CORTIJOS 02	30	\N	2021-03-16 15:14:28	NODO OUTDOOR
2000	CUATRICENTENARIA	30	\N	2021-03-16 15:15:20	NODO OUTDOOR
2001	CUJICITO	30	\N	2021-03-16 15:15:35	NODO OUTDOOR
2002	EL ARAGUANEY	30	\N	2021-03-16 15:15:57	NODO OUTDOOR
2003	EL CARMELO	30	\N	2021-03-16 15:16:39	NODO OUTDOOR
2005	EL CRUCE	30	\N	2021-03-16 15:18:23	CENTRAL CRITICA
2006	EL MAMON	30	\N	2021-03-16 15:18:54	NODO OUTDOOR
2009	GUANIPA MATOS	30	\N	2021-03-16 15:20:29	NODO OUTDOOR
2055	SAN FELIPE	21	\N	\N	\N
2011	ISLA DORADA	30	\N	2021-03-16 15:21:56	NODO OUTDOOR
2060	SAN MIGUEL	21	\N	\N	\N
2013	LA CAÑADA I	30	\N	2021-03-16 15:22:48	NODO OUTDOOR
2014	LA CAÑADA II	30	\N	2021-03-16 15:22:54	NODO OUTDOOR
2015	LA COROMOTO	30	\N	2021-03-16 15:23:10	CENTRAL
2017	LA ENSENADA CHIQUINQUIRA	30	\N	2021-03-16 15:23:57	CENTRAL
2016	LA ENCRUCIJADA	30	\N	2021-03-16 15:23:43	NODO OUTDOOR
2019	LA MONTAÑITA	30	\N	2021-03-16 15:24:35	NODO OUTDOOR
2020	LA PASTORA	30	\N	2021-03-16 15:24:55	NODO OUTDOOR
2022	LA ROSITA	30	\N	2021-03-16 15:25:31	NODO OUTDOOR
2023	LA SIERRITA S R DEL MOJAN	30	\N	2021-03-16 15:25:44	DLC
1983	CAMPO MARA	21	\N	2021-02-11 09:28:49	CENTRAL
2024	LA SIERRITA UE ANA M CAMPOS	30	\N	2021-03-16 15:25:55	SUPERAULA
2025	LA TRINIDAD	30	\N	2021-03-16 15:26:15	NODO OUTDOOR
2028	LAS LOLAS	30	\N	2021-03-16 15:27:46	CENTRAL
2007	EL TREBOL	21	\N	2021-02-11 09:28:49	CENTRAL
2032	LOS JOVITOS	30	\N	2021-03-16 15:31:03	NODO OUTDOOR
2029	LAS PIEDRAS	30	\N	2021-03-16 15:28:12	CENTRAL
2035	LOS ROSALES	30	\N	2021-03-16 15:32:56	NODO OUTDOOR
2045	PARAISO NORTE	30	\N	2021-03-16 15:35:37	NODO OUTDOOR
2050	PUNTA DE PIEDRA	30	\N	2021-03-16 15:36:52	NODO OUTDOOR
1957	SAN FRANCISCO DE LA PARAGUA	20	\N	2021-02-11 09:28:51	CENTRAL
2065	SANTA ISABEL	30	\N	2021-03-16 15:42:52	NODO OUTDOOR
1959	SOLEDAD I	20	\N	2021-02-11 09:28:51	CENTRAL
1960	SOLEDAD II	20	\N	2021-02-11 09:28:51	CENTRAL
2075	TX PARAGUAIPOA PTJ (NO CANTV)	30	\N	2021-03-16 15:47:00	NO CANTV
1976	BELLA VISTA III	21	\N	2021-02-11 09:28:51	CENTRAL CRITICA
2039	MANZANILLO I	30	\N	2021-03-16 15:33:51	NODO OUTDOOR
1953	PUERTO PAEZ	22	\N	2021-03-19 10:05:44	OPSUT
2036	LOS TOTUMOS	30	\N	2021-03-16 15:33:16	NODO OUTDOOR
2008	FRONTERAS	21	\N	2021-02-11 09:28:51	CENTRAL CRITICA
2034	LOS OLIVOS	21	\N	2021-02-11 09:28:52	OAC
1945	PERIFERICO SUR	22	\N	2021-03-19 10:08:10	NODO OUTDOOR
2063	SAN RAFAEL DEL MOJAN II	30	\N	2021-03-16 15:41:48	NODO OUTDOOR
2079	VILLA DEL ROSARIO DE PERIJA	30	\N	2021-03-16 15:47:52	SUPERAULA
2038	MACHIQUES I	30	\N	2021-03-16 15:33:41	CENTRAL CRITICA
2072	TAMARE MARA	30	\N	2021-03-16 15:46:09	NODO OUTDOOR
2077	URB CUATRICENTENARIO 2 AV	30	\N	2021-03-16 15:47:24	NODO OUTDOOR
2040	MANZANILLO II	30	\N	2021-03-16 15:34:04	NODO OUTDOOR
2041	MONTE SANTO	30	\N	2021-03-16 15:34:21	NODO OUTDOOR
2044	PARAGUAIPOA	30	\N	2021-03-16 15:35:18	CENTRAL
2046	PARAISO SUR	30	\N	2021-03-16 15:35:47	NODO OUTDOOR
2048	PIEDRAS DEL SOL	30	\N	2021-03-16 15:36:32	NODO OUTDOOR
2049	POTRERITO	30	\N	2021-03-16 15:36:41	CENTRAL
2051	RAFAEL URDANETA	30	\N	2021-03-16 15:37:08	NODO OUTDOOR
2053	SAMBIL MARACAIBO	30	\N	2021-03-16 15:37:37	NODO OUTDOOR
2054	SAN AGUSTIN 01	30	\N	2021-03-16 15:37:51	NODO OUTDOOR
2057	SAN ISIDRO I	30	\N	2021-03-16 15:39:17	NODO OUTDOOR
2058	SAN ISIDRO II	30	\N	2021-03-16 15:39:26	NODO OUTDOOR
2059	SAN JOSE DE PERIJA	30	\N	2021-03-16 15:39:57	CENTRAL CRITICA
2064	SANTA CRUZ DE MARA	30	\N	2021-03-16 15:42:29	NODO OUTDOOR
2066	SANTA LUCIA	30	\N	2021-03-16 15:43:08	NODO OUTDOOR
2070	SUB EDELCA MARACAIBO (NO CANTV)	30	\N	2021-03-16 15:45:42	NO CANTV
2073	TORRE EMPRESARIAL CLARET	30	\N	2021-03-16 15:46:39	CENTRAL
2078	VILLA DEL ROSARIO	30	\N	2021-03-16 15:47:41	CENTRAL CRITICA
2080	VILLA TAMARE	30	\N	2021-03-16 15:48:03	NODO OUTDOOR
1951	PUERTO AYACUCHO I	22	\N	2021-03-19 09:59:34	CENTRAL CRITICA
1944	PARQUES DEL SUR	20	\N	2021-02-11 09:28:55	NODO OUTDOOR
1947	PROCERES I	20	\N	2021-02-11 09:28:55	NODO OUTDOOR
1958	SOLEDAD	20	\N	2021-02-11 09:28:56	OPSUT
1962	VILLA KARIÑA	20	\N	2021-02-11 09:28:56	NODO OUTDOOR
1949	PROVINCIAL	20	\N	2021-02-11 09:28:57	SUPERAULA
2213	CARIPE	26	\N	2021-03-03 11:49:32	CENTRAL
2088	AV 40 MILAGRO NORTE	22	\N	\N	\N
2210	CACHIPO	26	\N	2021-03-03 13:43:04	NODO OUTDOOR
2081	Y01 C COQUIVACOA SALA FXB CC SAMBIL	30	\N	2021-03-16 15:48:13	DLC
2083	Y01 C SAN FELIPE LA CAÑADA	30	\N	2021-03-16 15:48:33	DLC
2099	BARRIO VENEZUELA	22	\N	\N	\N
2084	Y02 C BELLA VISTA III LA SIERRITA	30	\N	2021-03-16 15:48:43	DLC
2085	Y03 C BELLA VISTA III SINAMAICA	30	\N	2021-03-16 15:48:55	DLC
2219	JOAQUIN DEL TIGRE	26	\N	2021-03-16 19:32:41	OPSUT
2136	EL PORVENIR	22	\N	\N	\N
2153	LAGUNILLAS	22	\N	\N	\N
2154	LAS ACACIAS	22	\N	\N	\N
2155	LAS DELICIAS	22	\N	\N	\N
2156	LAS INDUSTRIAS	22	\N	\N	\N
2157	LAS VEGAS	22	\N	\N	\N
2158	LOS HATICOS	22	\N	\N	\N
2176	PUERTO DE ALTAGRACIA	22	\N	\N	\N
2182	SAN FRANCISCO	22	\N	\N	\N
2185	SANTA RITA	22	\N	\N	\N
2186	SUB ESTACIÓN EL TABLAZO (NO CANTV)	22	\N	\N	\N
2090	BACHAQUERO II	22	\N	2021-02-11 09:28:48	CENTRAL
2089	BACHAQUERO	22	\N	2021-02-11 09:28:53	NODO OUTDOOR
2211	CAICARA DE MATURIN	26	\N	2021-02-11 09:28:49	NODO OUTDOOR
2216	CARIPITO II	26	\N	2021-02-11 09:28:53	NODO OUTDOOR
2116	CIUDAD OJEDA UNION	22	\N	2021-02-11 09:28:49	CENTRAL
2145	EL VENADO	22	\N	2021-02-11 09:28:51	NODO OUTDOOR
2217	CC LA CASCADA	26	\N	2021-02-11 09:28:53	NODO OUTDOOR
2218	CC PETRORIENTE	26	\N	2021-02-11 09:28:53	NODO OUTDOOR
2112	CIRUMA	22	\N	2021-02-11 09:28:53	NODO OUTDOOR
2100	BELLA VISTA I	22	\N	2021-02-11 09:28:49	CENTRAL
2101	BELLA VISTA II	22	\N	2021-02-11 09:28:49	CENTRAL
2121	CUATRICENTENARIO	22	\N	2021-02-11 09:28:49	CENTRAL
2132	EL MENE	22	\N	2021-02-11 09:28:49	NODO OUTDOOR
2144	EL TIGRE	22	\N	2021-02-11 09:28:49	SUPERAULA
2148	GALERAS DE MISOA	22	\N	2021-02-11 09:28:50	CENTRAL
2152	LAGOMAR II	22	\N	2021-02-11 09:28:50	CENTRAL
2163	MACHANGO	22	\N	2021-02-11 09:28:50	CENTRAL
2164	MECOCAL	22	\N	2021-02-11 09:28:50	CENTRAL
2167	MENE MAUROA I	22	\N	2021-02-11 09:28:50	CENTRAL
2168	MENE MAUROA II	22	\N	2021-02-11 09:28:50	CENTRAL
2173	PALMAREJO	22	\N	2021-02-11 09:28:50	CENTRAL
2175	PUEBLO NUEVO BARALT	22	\N	2021-02-11 09:28:50	NODO OUTDOOR
2177	PUNTA GORDA	22	\N	2021-02-11 09:28:50	NODO OUTDOOR
2184	SAN TIMOTEO	22	\N	2021-02-11 09:28:51	NODO OUTDOOR
2187	TAMARE	22	\N	2021-02-11 09:28:51	CENTRAL
2105	CABIMAS	22	\N	2021-02-11 09:28:51	SUPERAULA
2107	CABIMAS DELICIAS	22	\N	2021-02-11 09:28:51	CENTRAL CRITICA
2150	LA LAGO	22	\N	2021-02-11 09:28:52	CENTRAL CRITICA
2199	Y01 C CABIMAS CENTRO S RAFAEL EL AMPARO	22	\N	2021-02-11 09:28:52	DLC
2200	Y01 C SANTA RITA ALIJIMIRO GONZALEZ	22	\N	2021-02-11 09:28:52	DLC
2201	Y01 URL SANTA RITA PALMAREJO	22	\N	2021-02-11 09:28:52	DLC
2203	Y03 C SAN MIGUEL ENTRADA ALTOS SOL AMA	22	\N	2021-02-11 09:28:52	DLC
2086	AMERICA ARAUJO	22	\N	2021-02-11 09:28:53	NODO OUTDOOR
2087	ANGELICA LUSINCHI	22	\N	2021-02-11 09:28:53	NODO OUTDOOR
2092	BARRIO GUAICAIPURO	22	\N	2021-02-11 09:28:53	NODO OUTDOOR
2093	BARRIO JOSE F RIVAS II	22	\N	2021-02-11 09:28:53	NODO OUTDOOR
2094	BARRIO JOSE F RIVAS III	22	\N	2021-02-11 09:28:53	NODO OUTDOOR
2096	BARRIO LOS MANANTIALES	22	\N	2021-02-11 09:28:53	NODO OUTDOOR
2097	BARRIO PUNTO FIJO II	22	\N	2021-02-11 09:28:53	NODO OUTDOOR
2102	BETULIO GONZALEZ	22	\N	2021-02-11 09:28:53	NODO OUTDOOR
2103	CABILLAS	22	\N	2021-02-11 09:28:53	NODO OUTDOOR
2108	CAMPO LARA	22	\N	2021-02-11 09:28:53	NODO OUTDOOR
2110	CAMPO ROJO	22	\N	2021-02-11 09:28:53	NODO OUTDOOR
2114	CIUDAD BOLIVAR II	22	\N	2021-02-11 09:28:53	NODO OUTDOOR
2115	CIUDAD OJEDA	22	\N	2021-02-11 09:28:53	SUPERAULA
2118	CONCEPCION 7	22	\N	2021-02-11 09:28:53	NODO OUTDOOR
2119	CORTIJOS 01	22	\N	2021-02-11 09:28:54	NODO OUTDOOR
2122	CUMAREBO I	22	\N	2021-02-11 09:28:54	NODO OUTDOOR
2123	CUMAREBO II	22	\N	2021-02-11 09:28:54	NODO OUTDOOR
2125	EL CAUJARO I	22	\N	2021-02-11 09:28:54	NODO OUTDOOR
2127	EL DANTO I	22	\N	2021-02-11 09:28:54	NODO OUTDOOR
2129	EL DANTO III	22	\N	2021-02-11 09:28:54	NODO OUTDOOR
2130	EL DANTO IV	22	\N	2021-02-11 09:28:54	NODO OUTDOOR
2133	EL MENITO	22	\N	2021-02-11 09:28:54	NODO OUTDOOR
2135	EL MUSEO	22	\N	2021-02-11 09:28:54	NODO OUTDOOR
2137	EL REMOLINO	22	\N	2021-02-11 09:28:54	NODO OUTDOOR
2139	EL SOLER I	22	\N	2021-02-11 09:28:54	NODO OUTDOOR
2140	EL SOLER II	22	\N	2021-02-11 09:28:54	NODO OUTDOOR
2142	EL SOLER V	22	\N	2021-02-11 09:28:54	NODO OUTDOOR
2143	EL SOLER VI	22	\N	2021-02-11 09:28:54	NODO OUTDOOR
2147	FUNDABARRIOS II	22	\N	2021-02-11 09:28:54	NODO OUTDOOR
2149	LA CAÑAITA	22	\N	2021-02-11 09:28:54	NODO OUTDOOR
2159	LOS HORNITOS	22	\N	2021-02-11 09:28:55	NODO OUTDOOR
2161	LOS POSTES NEGROS	22	\N	2021-02-11 09:28:55	NODO OUTDOOR
2166	MENE MAUROA	22	\N	2021-02-11 09:28:55	NODO OUTDOOR
2171	OASIS VILLA	22	\N	2021-02-11 09:28:55	NODO OUTDOOR
2172	OLALLA	22	\N	2021-02-11 09:28:55	NODO OUTDOOR
2174	PLAN BONITO	22	\N	2021-02-11 09:28:55	NODO OUTDOOR
2179	SABANA DE MACHANGO	22	\N	2021-02-11 09:28:55	NODO OUTDOOR
2181	SAN BENITO	22	\N	2021-02-11 09:28:56	NODO OUTDOOR
2189	URB NUEVO HORNITO	22	\N	2021-02-11 09:28:56	NODO OUTDOOR
2191	URB VILLA VENEZUELA	22	\N	2021-02-11 09:28:56	NODO OUTDOOR
2193	VILLA DE SAN JOSE	22	\N	2021-02-11 09:28:56	NODO OUTDOOR
2194	VILLA FELIZ	22	\N	2021-02-11 09:28:56	NODO OUTDOOR
2196	VILLA RITA	22	\N	2021-02-11 09:28:56	NODO OUTDOOR
2197	VILLA SUR I	22	\N	2021-02-11 09:28:56	NODO OUTDOOR
2106	CABIMAS - COSTA MARFIL	22	\N	2021-02-11 09:28:56	OAC
2205	AMANA DEL TAMARINDO	26	\N	2021-02-11 09:28:56	OPSUT
2313	EL POBLADO	27	\N	2021-03-04 10:32:34	CENTRAL
2252	LAS INDUSTRIAS	26	\N	2021-03-03 13:18:56	CENTRAL
2229	EL TEJERO	26	\N	2021-03-03 13:21:06	CENTRAL
2280	SANTA BARBARA	26	\N	2021-03-03 13:29:15	NODO OUTDOOR
2293	URACOA	26	\N	2021-03-03 13:32:49	CENTRAL
2282	SANTA RITA	26	\N	2021-03-03 13:35:03	OPSUT
2251	LAS CAROLINAS	26	\N	2021-03-03 13:39:57	NODO OUTDOOR
2266	PALMA REAL	26	\N	2021-03-03 13:41:38	NODO OUTDOOR
2246	LA PAZ	26	\N	2021-03-03 13:43:42	NODO OUTDOOR
2226	EL PIÑAL	26	\N	2021-03-03 13:44:53	NODO OUTDOOR
2224	EL COROZO	26	\N	2021-03-03 13:45:16	NODO OUTDOOR
2330	LA ISLETA	27	\N	2021-03-04 11:01:10	CENTRAL
2243	LAS CLAVELLINAS	34	\N	2021-03-03 14:29:24	NODO OUTDOOR
2348	SAN NICOLAS	27	\N	2021-03-04 11:47:42	CENTRAL
2334	LAS HERNANDEZ	27	\N	2021-03-04 11:08:08	NODO OUTDOOR
2351	SANTA ANA	27	\N	2021-03-04 11:51:21	CENTRAL
961	BELEN	11	\N	2021-03-04 14:34:08	CENTRAL
1672	LA PASTORA	17	\N	2021-03-08 12:37:57	CENTRAL
2305	CONEJEROS	27	\N	2021-03-19 19:27:41	CENTRAL
2308	COTOPERIZ I	27	\N	2021-02-11 09:28:54	NODO OUTDOOR
2317	EL YAQUE SANTIAGO MARIÑO	27	\N	2021-02-11 09:28:49	CENTRAL
2230	EL TORNO	26	\N	2021-02-11 09:28:54	NODO OUTDOOR
2316	EL YAQUE II	27	\N	2021-02-11 09:28:54	NODO OUTDOOR
2234	GRAN CACIQUE	26	\N	2021-02-11 09:28:54	NODO OUTDOOR
2236	GUANAGUANAY	26	\N	2021-02-11 09:28:54	NODO OUTDOOR
2319	GUINIMA	27	\N	2021-02-11 09:28:50	CENTRAL
2318	GUAYACANCITO	27	\N	2021-02-11 09:28:54	NODO OUTDOOR
2322	JORGE COLL II	27	\N	2021-02-11 09:28:54	NODO OUTDOOR
2242	LA ARBOLEDA	26	\N	2021-02-11 09:28:50	CENTRAL
2324	LA ASUNCION	27	\N	2021-02-11 09:28:50	CENTRAL
2328	LA GUARDIA	27	\N	2021-02-11 09:28:50	SUPERAULA
2327	LA FUENTE	27	\N	2021-02-11 09:28:54	NODO OUTDOOR
2247	LA PICA	26	\N	2021-02-11 09:28:54	NODO OUTDOOR
2248	LA SEVILLANA	26	\N	2021-02-11 09:28:54	NODO OUTDOOR
2331	LAS BLANQUILLAS	27	\N	2021-02-11 09:28:55	NODO OUTDOOR
2332	LAS GOMEZ	27	\N	2021-02-11 09:28:55	NODO OUTDOOR
2255	LOS BARRANCOS	26	\N	2021-02-11 09:28:50	CENTRAL
2262	MATURIN I / ALTO GURI	26	\N	2021-02-11 09:28:50	CENTRAL
2257	LOS GIRASOLES II	26	\N	2021-02-11 09:28:55	NODO OUTDOOR
2260	LOS TAPIALES	26	\N	2021-02-11 09:28:55	NODO OUTDOOR
2265	MORICHAL	26	\N	2021-02-11 09:28:55	NODO OUTDOOR
2350	SAN RAFAEL	27	\N	2021-02-11 09:28:52	CENTRAL CRITICA
2263	MIRAFLORES	26	\N	2021-02-11 09:28:50	CENTRAL
2339	PLAYA EL AGUA	27	\N	2021-02-11 09:28:50	CENTRAL
2267	PALOMA	26	\N	2021-02-11 09:28:55	NODO OUTDOOR
2268	PARARI	26	\N	2021-02-11 09:28:55	NODO OUTDOOR
2270	PUEBLO LA HORQUETA	26	\N	2021-02-11 09:28:55	NODO OUTDOOR
2271	PUERTAS DEL SUR	26	\N	2021-02-11 09:28:55	NODO OUTDOOR
2344	PUNTA DE MANGLE	27	\N	2021-02-11 09:28:55	NODO OUTDOOR
2275	SAN ANTONIO DE MATURIN	26	\N	2021-02-11 09:28:51	NODO OUTDOOR
2347	SAN JUAN BAUTISTA	27	\N	2021-02-11 09:28:51	NODO OUTDOOR
2277	SAN NICOLAS JUSEPIN	26	\N	2021-02-11 09:28:51	CENTRAL
2349	SAN PEDRO DE COCHE	27	\N	2021-02-11 09:28:51	NODO OUTDOOR
2287	TERRAZAS DEL OESTE	26	\N	2021-02-11 09:28:56	NODO OUTDOOR
2284	TELECOM MATURIN	26	\N	2021-02-11 09:28:51	CENTRAL
2286	TERESEN	26	\N	2021-02-11 09:28:51	NODO OUTDOOR
2359	Y01 C JORGE COLL AGUA DE VACA LA FLORIDA	27	\N	2021-02-11 09:28:52	DLC
2289	TOSCANA	26	\N	2021-02-11 09:28:51	CENTRAL
2288	TIPURO II	26	\N	2021-02-11 09:28:56	NODO OUTDOOR
2290	TROPICAL	26	\N	2021-02-11 09:28:56	NODO OUTDOOR
2357	VILLA ROSA II	27	\N	2021-02-11 09:28:56	NODO OUTDOOR
2354	VALLE DEL ESPIRITU SANTO	27	\N	2021-02-11 09:28:51	CENTRAL
2358	VILLA SAN ANTONIO	27	\N	2021-02-11 09:28:56	NODO OUTDOOR
2337	PEDREGALES	27	\N	2021-02-11 09:28:57	SUPERAULA
2299	BOCA DEL POZO	27	\N	2021-02-11 09:28:49	CENTRAL
2304	CHACACHACARE	27	\N	2021-02-11 09:28:53	NODO OUTDOOR
2300	BOCA DEL RIO	27	\N	2021-02-11 09:28:49	CENTRAL
2302	CERRO GENOVES	27	\N	2021-02-11 09:28:49	CENTRAL
2303	CERRO PALMA REAL COPEY	27	\N	2021-02-11 09:28:49	CENTRAL
2306	CONEJEROS	27	\N	2021-02-11 09:28:53	NODO OUTDOOR
2310	EL ESPINAL	27	\N	2021-02-11 09:28:49	NODO OUTDOOR
2307	CONUCO VIEJO	27	\N	2021-02-11 09:28:53	NODO OUTDOOR
2223	COPORITO	26	\N	2021-02-11 09:28:53	NODO OUTDOOR
2311	EL GUAMACHE	27	\N	2021-02-11 09:28:54	NODO OUTDOOR
2353	TERMINAL EL GUAMACHE	27	\N	2021-02-11 09:28:50	CENTRAL
2314	EL SALADO	27	\N	2021-02-11 09:28:54	NODO OUTDOOR
2228	EL SAMAN PUNTA DE MATA	26	\N	2021-02-11 09:28:54	NODO OUTDOOR
2239	JUANA LA AVANZADORA	26	\N	2021-02-11 09:28:54	NODO OUTDOOR
2245	LA GRAN VICTORIA	26	\N	2021-02-11 09:28:54	NODO OUTDOOR
2235	GUANAGUANA	26	\N	2021-02-11 09:28:50	CENTRAL
2333	LAS GUEVARAS	27	\N	2021-02-11 09:28:55	NODO OUTDOOR
2321	JORGE COLL	27	\N	2021-02-11 09:28:50	CENTRAL
2323	JUAN GRIEGO I	27	\N	2021-02-11 09:28:50	CENTRAL
2336	LOMAS DE MARGARITA	27	\N	2021-02-11 09:28:55	NODO OUTDOOR
2240	JUANICO	26	\N	2021-02-11 09:28:50	CENTRAL
2278	SAN SALVADOR	26	\N	2021-02-11 09:28:56	NODO OUTDOOR
2341	PORLAMAR GALERIAS FENTE	27	\N	2021-02-11 09:28:50	CENTRAL
2342	PUERTO FERMIN	27	\N	2021-02-11 09:28:50	NODO OUTDOOR
2272	PUNTA DE MATA	26	\N	2021-02-11 09:28:50	CENTRAL
2355	VILLA ROSA	27	\N	2021-02-11 09:28:51	CENTRAL
2285	TEMBLADOR	26	\N	2021-02-11 09:28:52	CENTRAL CRITICA
2343	PUERTO FERMIN GTI	27	\N	2021-02-10 11:13:42	\N
2361	Y01 C SAN RAFAEL ARENA CERNIDA	27	\N	2021-02-11 09:28:52	DLC
2298	BOCA DE RIO	27	\N	2021-02-11 09:28:53	NODO OUTDOOR
2297	ALTAGRACIA	27	\N	2021-02-11 09:28:48	NODO OUTDOOR
2396	CACHIPO	28	\N	2021-03-04 09:11:50	CENTRAL
2443	LECHERIAS	28	\N	2021-03-04 09:27:41	CENTRAL CRITICA
2450	MENE GRANDE	28	\N	2021-03-04 09:32:43	CENTRAL
2498	UVERITO	28	\N	2021-03-04 09:37:09	CENTRAL
2426	EL SAMAN	28	\N	2021-03-04 09:39:19	NODO OUTDOOR
2441	LAS DELICIAS	28	\N	2021-03-04 09:40:33	CENTRAL
2464	PUEBLO NUEVO	28	\N	2021-03-04 09:41:15	CENTRAL
2477	SAN JOAQUIN	28	\N	2021-03-04 09:42:05	CENTRAL
2480	SAN MIGUEL	28	\N	2021-03-04 09:42:39	CENTRAL
2482	SANTA ANA	28	\N	2021-03-04 09:43:06	CENTRAL
2487	SANTA INES	28	\N	2021-03-04 09:44:44	CENTRAL
2488	SANTA ROSA	28	\N	2021-03-04 09:45:10	CENTRAL
2427	EL TIGRE	28	\N	2021-03-04 10:01:46	SUPERAULA
2442	LAS PALMERAS	28	\N	2021-03-04 10:05:40	NODO OUTDOOR
2446	LOS OLIVOS	28	\N	2021-03-04 10:08:44	NODO OUTDOOR
2483	SANTA BARBARA	28	\N	2021-03-04 10:10:02	NODO OUTDOOR
2463	PRIMERO DE MAYO	28	\N	2021-03-04 10:12:10	NODO OUTDOOR
2437	LA CANDELARIA	28	\N	2021-03-04 10:15:22	NODO OUTDOOR
2439	LA ISLETA	28	\N	2021-03-04 10:15:53	NODO OUTDOOR
1049	MIRANDA II	11	\N	2021-03-04 15:03:42	CENTRAL
2366	Y03 C JUAN GRIEGO LA GUARDIA	27	\N	2021-02-11 09:28:52	DLC
2367	Y04 C JORGE COLL LA CARANTA	27	\N	2021-02-11 09:28:52	DLC
2368	Y04 C JUANGRIEGO COMPLEJO TURISTICO DUNE	27	\N	2021-02-11 09:28:52	DLC
2369	Y04 C SAN RAFAEL VILLA ROSA	27	\N	2021-02-11 09:28:52	DLC
2370	Y05 C JORGE COLL AGUA DE VACA CASA CAMP	27	\N	2021-02-11 09:28:52	DLC
2371	Y05 C JUANGRIEGO URB BAHIA DE PLATA	27	\N	2021-02-11 09:28:52	DLC
2372	Y05 C SAN RAFAEL PARAGUAICHI	27	\N	2021-02-11 09:28:52	DLC
2374	Y06 C SAN RAFAEL PUERTO GUAMACHE	27	\N	2021-02-11 09:28:52	DLC
2375	Y07 C JORGE COLL HOTEL LAGUNAMAR	27	\N	2021-02-11 09:28:52	DLC
2377	Y08 C SAN RAFAEL PUNTA DE PIEDRA	27	\N	2021-02-11 09:28:52	DLC
2378	AEROCENTRO	28	\N	2021-02-11 09:28:52	NODO OUTDOOR
2384	ARAGUA DE BARCELONA I	28	\N	2021-02-11 09:28:53	NODO OUTDOOR
2390	BAJO HONDO	28	\N	2021-02-11 09:28:53	OPSUT
2381	ANZOATEGUI I	28	\N	2021-02-11 09:28:51	CENTRAL CRITICA
2382	ANZOATEGUI II	28	\N	2021-02-11 09:28:51	CENTRAL CRITICA
2385	ARAGUA DE BARCELONA I (CM/TX ARAGUA DE B)	28	\N	2021-02-11 09:28:51	CENTRAL CRITICA
2392	BOCA DE UCHIRE ANZOATEGUI	28	\N	2021-02-11 09:28:57	SUPERAULA
2393	BOSQUES DEL NEVERI	28	\N	2021-02-11 09:28:53	NODO OUTDOOR
2402	CANTAURA I	28	\N	2021-02-11 09:28:51	CENTRAL CRITICA
2387	ARAGUITA	28	\N	2021-02-11 09:28:48	CENTRAL
2388	ATAPIRIRE	28	\N	2021-02-11 09:28:48	CENTRAL
2394	BOYACA	28	\N	2021-02-11 09:28:49	CENTRAL
2397	CAIGUA	28	\N	2021-02-11 09:28:53	NODO OUTDOOR
2407	CERRO CORAZON	28	\N	2021-02-11 09:28:49	CENTRAL
2398	CANTA CLARO	28	\N	2021-02-11 09:28:53	NODO OUTDOOR
2408	CERRO GRANDE	28	\N	2021-02-11 09:28:49	CENTRAL
2399	CANTAURA 00	28	\N	2021-02-11 09:28:53	NODO OUTDOOR
2403	CASA GRANDE	28	\N	2021-02-11 09:28:53	NODO OUTDOOR
2405	CC UNIMALL	28	\N	2021-02-11 09:28:53	NODO OUTDOOR
2406	CERRO AMARILLO	28	\N	2021-02-11 09:28:53	NODO OUTDOOR
2412	CONSTANTINO	28	\N	2021-02-11 09:28:53	NODO OUTDOOR
2413	CRUZ VERDE	28	\N	2021-02-11 09:28:54	NODO OUTDOOR
2414	DON IGNACIO	28	\N	2021-02-11 09:28:54	NODO OUTDOOR
2410	CLARINES	28	\N	2021-02-11 09:28:49	NODO OUTDOOR
2417	EL CARITO	28	\N	2021-02-11 09:28:49	CENTRAL
2419	EL CHAPARRO	28	\N	2021-02-11 09:28:54	NODO OUTDOOR
2421	EL CHAPARRO II	28	\N	2021-02-11 09:28:49	CENTRAL
2424	EL MORICHE	28	\N	2021-02-11 09:28:54	NODO OUTDOOR
2430	EL VIÑEDO	28	\N	2021-02-11 09:28:54	NODO OUTDOOR
2431	ELIEZER  ZAMORA	28	\N	2021-02-11 09:28:54	NODO OUTDOOR
2418	EL CARO	28	\N	2021-02-11 09:28:56	OPSUT
2429	EL TIGRITO	28	\N	2021-02-11 09:28:49	SUPERAULA
2423	EL MANGUITO	28	\N	2021-02-11 09:28:56	OPSUT
2452	MUNDO NUEVO	28	\N	2021-02-11 09:28:55	NODO OUTDOOR
2432	FARALLONES	28	\N	2021-02-11 09:28:50	OPSUT
2454	NARICUAL	28	\N	2021-02-11 09:28:55	NODO OUTDOOR
2476	SAN DIEGO DE CABRUTICA	28	\N	2021-02-11 09:28:57	OPSUT
2455	OCANA	28	\N	2021-02-11 09:28:55	NODO OUTDOOR
2433	GUANAPE	28	\N	2021-02-11 09:28:50	NODO OUTDOOR
2434	GUANTA	28	\N	2021-02-11 09:28:50	CENTRAL
2458	PARIAGUAN 00	28	\N	2021-02-11 09:28:55	NODO OUTDOOR
2460	PICA DEL NEVERI	28	\N	2021-02-11 09:28:55	NODO OUTDOOR
2444	LOS CEREZOS	28	\N	2021-02-11 09:28:50	CENTRAL
2447	MAPIRE	28	\N	2021-02-11 09:28:50	CENTRAL
2453	MUNICIPAL (CARAQUEÑA)	28	\N	2021-02-11 09:28:50	CENTRAL
2495	TIGRE II (PLAZA)	28	\N	2021-02-11 09:28:52	CENTRAL CRITICA
2456	ONOTO	28	\N	2021-02-11 09:28:50	NODO OUTDOOR
2470	R2000 JOSE	28	\N	2021-02-11 09:28:50	CENTRAL
2478	SAN JOSE	28	\N	2021-02-11 09:28:57	OPSUT
2462	PRADERA COUNTRY	28	\N	2021-02-11 09:28:55	NODO OUTDOOR
2465	PUENTE REAL	28	\N	2021-02-11 09:28:55	NODO OUTDOOR
2471	RAHME	28	\N	2021-02-11 09:28:50	CENTRAL
2466	PUERTO COLON	28	\N	2021-02-11 09:28:55	NODO OUTDOOR
2469	PUTUCUAL	28	\N	2021-02-11 09:28:55	NODO OUTDOOR
2472	REPETIDORA SABANA LARGA	28	\N	2021-02-11 09:28:51	CENTRAL
2475	SABATINO	28	\N	2021-02-11 09:28:56	NODO OUTDOOR
2493	TERRAZAS DEL MAR	28	\N	2021-02-11 09:28:56	NODO OUTDOOR
2416	EL ALAMBRE	28	\N	2021-02-11 09:28:56	OPSUT
2494	TERRONAL	28	\N	2021-02-11 09:28:57	OPSUT
2383	ARAGUA DE BARCELONA	28	\N	2021-02-11 09:28:57	SUPERAULA
2479	SAN MATEO	28	\N	2021-02-11 09:28:51	NODO OUTDOOR
2484	SANTA CLARA	28	\N	2021-02-11 09:28:51	CENTRAL
2491	TELECOM BARCELONA	28	\N	2021-02-11 09:28:51	CENTRAL
2492	TELECOM PUERTO LA CRUZ	28	\N	2021-02-11 09:28:51	CENTRAL
2496	URICA	28	\N	2021-02-11 09:28:51	NODO OUTDOOR
2386	ARAGUA DE BARCELONA II (TX ARAGUA DE BAR)	28	\N	2021-02-11 09:28:48	CENTRAL
2514	ARAYA	29	\N	2021-03-03 11:02:37	CENTRAL
2518	CARUPANO	29	\N	2021-03-03 10:09:43	CENTRAL CRITICA
2521	CERRO ARROJATA	29	\N	2021-03-03 11:06:40	CENTRAL
2524	CERRO MACURO	29	\N	2021-03-03 11:07:45	CENTRAL
2525	CERRO PACHOLI	29	\N	2021-03-03 11:08:14	CENTRAL
2526	CERRO PERU	29	\N	2021-03-03 11:08:41	CENTRAL
2531	CUMANA CENTRO	29	\N	2021-03-03 11:09:06	CENTRAL CRITICA
2529	CUMANA AEROPUERTO 00	29	\N	2021-03-03 11:09:58	NODO OUTDOOR
2530	CUMANA AEROPUERTO 01	29	\N	2021-03-03 11:10:38	NODO OUTDOOR
2520	CATUARO	29	\N	2021-03-03 11:14:29	NODO OUTDOOR
2516	CAIMANCITO	29	\N	2021-03-03 11:15:01	NODO OUTDOOR
2533	CUMANACOA	29	\N	2021-03-03 11:15:24	CENTRAL
2542	FRANJA EL PENON	29	\N	2021-03-03 11:15:32	NODO OUTDOOR
2535	EL MORRO DE PUERTO SANTO	29	\N	2021-03-03 11:16:39	CENTRAL
2577	SAN JUANILLO	29	\N	2021-03-03 11:16:41	NODO OUTDOOR
2536	EL PAUJIL	29	\N	2021-03-03 11:17:08	CENTRAL
2541	EL YOCO	29	\N	2021-03-03 11:17:41	NODO OUTDOOR
2555	LA GRANJA	29	\N	2021-03-03 11:19:05	\N
2513	ALI RAFAEL PRIMERA	29	\N	2021-03-03 11:19:34	NODO OUTDOOR
2571	SABANA DE PIEDRA	29	\N	2021-03-03 11:19:36	CENTRAL
2565	PLAYA GRANDE	29	\N	2021-03-03 11:34:12	CENTRAL
2545	GUACA	29	\N	2021-03-03 11:20:36	NODO OUTDOOR
2582	SAUCEDO	29	\N	2021-03-03 11:21:41	NODO OUTDOOR
2558	LOS ARROYOS	29	\N	2021-03-03 11:22:47	NODO OUTDOOR
2556	LA LLANADA DE GUIRIA	29	\N	2021-03-03 11:23:50	NODO OUTDOOR
2590	PANAMERICANO	30	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2592	DELICIAS	30	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2594	SAN FRANCISCO	30	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2595	INDUSTRIAS	30	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2597	LA LAGO	30	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2599	URDANETA	30	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2600	BELLA VISTA I,II	30	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2601	CUATRICENTENARIO	30	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2602	LOS HATICOS 	30	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2604	LAGOMAR	30	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2607	LA CAÑADA	30	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2608	CUATRICENTENARIO EDELCA	30	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2610	OAC SANTO ANGEL	30	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2611	OAC DORAL CENTER	30	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2612	OAC SAN FRANCISCO	30	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2619	SINAMAICA	31	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2621	NODO OPSUT EL CRUCE	32	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2623	NODO SAN IGNACIO	32	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2624	EL GULLAVO	32	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2625	EL MORALITO	32	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2626	ENCONTRADOS ZULIA	32	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2501	VALLE DE GUANAPE REPETIDORA	28	\N	2021-02-11 09:28:51	CENTRAL
2587	YAGUARAPARO	29	\N	2021-03-03 11:43:43	CENTRAL
2561	MARIGUITAR	29	\N	2021-03-03 11:30:05	CENTRAL
2505	Y01 C CUMANA CENTRO ALTOS DE SANTA FE	28	\N	2021-02-11 09:28:52	DLC
2506	Y01 C PUERTO PIRITU LAS ISLETAS	28	\N	2021-02-11 09:28:52	DLC
2508	Y03 C ANZOATEGUI PETROZUATA	28	\N	2021-02-11 09:28:52	DLC
2509	Y03 C PUERTO PIRITU LAS ISLETAS	28	\N	2021-02-11 09:28:52	DLC
2510	Y04 C BARCELONA LA COSTANERA	28	\N	2021-02-11 09:28:52	DLC
2511	Y04 C PUERTO PIRITU LAS ISLETAS	28	\N	2021-02-11 09:28:52	DLC
2543	FRANJA LA LLANADA	29	\N	2021-03-03 11:24:24	NODO OUTDOOR
1327	LA BARRACA	14	\N	2021-03-08 10:18:23	CENTRAL
2546	GUAYACAN DE LAS FLORES	29	\N	2021-03-03 11:24:47	CENTRAL
2540	EL TACAL	29	\N	2021-03-03 11:25:29	NODO OUTDOOR
2549	GUIRIA I	29	\N	2021-03-03 11:26:07	NODO OUTDOOR
2554	LA ESMERALDA	29	\N	2021-03-03 11:26:19	\N
2550	GUIRIA II	29	\N	2021-03-03 11:26:40	NODO OUTDOOR
2553	IRAPA	29	\N	2021-03-03 11:27:16	CENTRAL
2552	IRAPA	29	\N	2021-03-03 11:27:35	NODO OUTDOOR
2563	MOCHIMA	29	\N	2021-03-03 11:27:46	NODO OUTDOOR
2559	MACURO	29	\N	2021-03-03 11:28:00	CENTRAL
2562	MERITO	29	\N	2021-03-03 11:28:15	NODO OUTDOOR
2584	SOTILLO	29	\N	2021-03-03 11:28:55	\N
2564	MUELLE DE CARIACO	29	\N	2021-03-03 11:33:26	CENTRAL
2576	SAN JUAN DE UNARE	29	\N	2021-03-03 11:29:23	NODO OUTDOOR
2527	CHACARACUAL	29	\N	2021-03-03 11:29:50	OPSUT
2568	RIO CARIBE	29	\N	2021-03-03 11:35:01	NODO OUTDOOR
2569	RIO GRANDE ARRIBA	29	\N	2021-03-03 11:35:43	CENTRAL
2572	SAN ANTONIO DE IRAPA	29	\N	2021-03-03 11:36:58	CENTRAL
2574	SAN JOSE DE AEROCUAR	29	\N	2021-03-03 11:38:46	NODO OUTDOOR
2575	SAN JUAN DE LAS GALDONAS	29	\N	2021-03-03 11:39:45	CENTRAL
2578	SAN VICENTE	26	\N	2021-03-03 11:39:48	CENTRAL
2580	SANTA FE	29	\N	2021-03-03 11:40:49	CENTRAL
2581	SANTA MARIA DE CARIACO	29	\N	2021-03-03 11:42:13	CENTRAL
2585	TUNAPUY	29	\N	2021-03-03 11:42:37	NODO OUTDOOR
2586	VILLA OLIMPICA	29	\N	2021-03-03 11:43:23	CENTRAL
2539	EL POBLADO	29	\N	2021-03-03 11:43:58	CENTRAL
2512	ZUATA	28	\N	2021-03-04 09:46:25	CENTRAL
2622	CAMPO BERNAL	30	2021-01-26 10:03:22	2021-03-16 15:00:31	OPSUT
2606	CAMPO LA CONCEPCION	30	2021-01-26 10:03:22	2021-03-16 15:01:21	CENTRAL
2618	CAMPO MARA	31	2021-01-26 10:03:22	2021-03-16 15:05:06	CENTRAL
2615	CARRASQUERO	30	2021-01-26 10:03:22	2021-03-16 15:05:44	CENTRAL
2504	Y01 C ANACO LA FLORIDA	28	\N	2021-02-11 09:28:52	DLC
2609	CENTRO OPERATIVO SABANETA	30	2021-01-26 10:03:22	2021-03-16 15:09:16	CENTRAL
2588	COQUIVACOA	30	2021-01-26 10:03:22	2021-03-16 15:13:54	CENTRAL CRITICA
2603	EL TREBOL	30	2021-01-26 10:03:22	2021-03-16 15:19:26	CENTRAL
2591	FRONTERAS	30	2021-01-26 10:03:22	2021-03-16 15:20:00	CENTRAL CRITICA
2620	PARAGUAIPOA	30	2021-01-26 10:03:22	2021-03-16 15:35:06	NODO OUTDOOR
2596	SAN FELIPE	30	2021-01-26 10:03:22	2021-03-16 15:38:18	CENTRAL CRITICA
2593	SAN MIGUEL	30	2021-01-26 10:03:22	2021-03-16 15:40:34	CENTRAL
2616	SANTA CRUZ DE MARA	30	2021-01-26 10:03:22	2021-03-16 15:42:10	CENTRAL
2503	VILLAS DE ANZOATEGUI	28	\N	2021-02-11 09:28:56	NODO OUTDOOR
2627	PUEBLO NUEVO	32	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2628	SANTA CRUZ DEL ZULIA	32	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2629	SAN CARLO DEL ZULIA	32	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2630	BELLO MONTE	32	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2631	NODO OPSUT SAN JOSE DE PERIJA	32	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2632	NODO  OPSUT CASIGUA EL CUBO	32	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2633	LA VILLA	32	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2634	MACHIQUES	32	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2635	LA LOLAS	32	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2636	SANTA BARBARA	32	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2637	CABIMAS CABILLAS	33	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2638	CIUDAD OJEDA	33	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2639	OJEDA UNION	33	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2640	BACHAQUERO	33	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2641	CABIMAS CENTRO	33	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2642	TAMARE OJEDA	33	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2643	CABIMAS DELICIAS	33	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2644	LOS PUERTOS	33	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2645	EL TABLAZO	33	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2646	MENEGRANDE	33	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2647	EL VENADO	33	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2648	CABIMAS LAURELES	33	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2649	LAGUNILLAS	33	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2650	PUNTA GORDA	33	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2651	OAC OJEDA CENTRO	33	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2652	NUEVAS DELICIAS	33	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2653	GALERA DE MISOA	33	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2654	BEGOTE	33	2021-01-26 10:03:22	2021-01-26 10:03:22	\N
2204	AGUASAY	26	\N	2021-02-11 09:28:48	CENTRAL
952	AGUIRRE	11	\N	2021-02-11 09:28:48	CENTRAL
953	ALMACEN NODAL FLOR AMARILLO	11	\N	2021-02-11 09:28:48	CENTRAL
1967	AMPARO	21	\N	2021-02-11 09:28:48	NODO OUTDOOR
2656	TUCUPITA	34	2021-02-04 12:35:01	2021-03-03 13:24:26	CENTRAL CRITICA
2659	ATABAPO	22	2021-02-04 12:40:05	2021-02-04 12:40:05	\N
2279	SAN VICENTE	29	\N	2021-03-03 11:38:25	CENTRAL
2232	FE Y ALEGRIA	26	\N	2021-03-03 11:49:14	NODO OUTDOOR
1	ABEJALES	1	\N	2021-03-03 15:20:40	CENTRAL
2291	TUCUPITA I	34	\N	2021-03-03 14:33:39	CENTRAL
2244	LA FLORIDA	26	\N	2021-03-03 13:47:36	NODO OUTDOOR
2411	CLARINES	28	\N	2021-03-04 09:17:16	CENTRAL
2497	URL LOS MESONES	28	\N	2021-03-04 09:28:24	CENTRAL
2467	PUERTO PIRITU I (TRANSMISIÓN)	28	\N	2021-03-04 09:31:53	CENTRAL
2395	BUENA VISTA	28	\N	2021-03-04 09:37:43	CENTRAL
2489	SUBEDELCA EL TIGRE II NO CANTV	28	\N	2021-03-04 10:02:38	NO CANTV
407	23 DE ENERO	5	\N	2021-02-11 09:28:48	CENTRAL
408	9 DE DICIEMBRE	5	\N	2021-02-11 09:28:48	CENTRAL
1393	ADICORA	15	\N	2021-02-11 09:28:48	NODO OUTDOOR
559	AEROPUERTO CARACAS	6	\N	2021-02-11 09:28:48	CENTRAL
1502	AGUA BLANCA	16	\N	2021-02-11 09:28:48	NODO OUTDOOR
1585	AGUA BUENA	17	\N	2021-02-11 09:28:48	CENTRAL
142	AGUA SANTA	2	\N	2021-02-11 09:28:48	NODO OUTDOOR
1592	AGUADA GRANDE	17	\N	2021-02-11 09:28:48	NODO OUTDOOR
143	AGUAS CALIENTES DE VALERA	2	\N	2021-02-11 09:28:48	CENTRAL
2380	ANACO II (CENTRO DE TRABAJO ANACO)	28	\N	2021-02-11 09:28:48	CENTRAL
2206	ARAGUA DE MATURIN	26	\N	2021-02-11 09:28:48	NODO OUTDOOR
568	ARAIRA	6	\N	2021-02-11 09:28:48	CENTRAL
147	ARAPUEY	2	\N	2021-02-11 09:28:48	NODO OUTDOOR
1755	AROA	18	\N	2021-02-11 09:28:48	NODO OUTDOOR
1606	ATARIGUA	17	\N	2021-02-11 09:28:48	CENTRAL
322	BAILADORES I	4	\N	2021-02-11 09:28:48	CENTRAL
1509	BARAURE	16	\N	2021-02-11 09:28:48	CENTRAL
1176	BARBACOA II	13	\N	2021-02-11 09:28:48	CENTRAL
1177	BARBACOAS I	13	\N	2021-02-11 09:28:48	CENTRAL
2220	CHAGUARAMAL	26	\N	2021-02-11 09:28:49	NODO OUTDOOR
2415	EDIFICIO TRANS	28	\N	2021-02-11 09:28:49	CENTRAL
2420	EL CHAPARRO I (EL CHAPARRO)	28	\N	2021-02-11 09:28:49	CENTRAL
2435	INTERCOMUNAL	28	\N	2021-02-11 09:28:50	CENTRAL
2329	LA GUARDIA I	27	\N	2021-02-11 09:28:50	CENTRAL
2451	MESA DE GUANIPA	28	\N	2021-02-11 09:28:50	CENTRAL
2269	PEDERNALES	26	\N	2021-02-11 09:28:50	CENTRAL
2345	PUNTA DE PIEDRAS	27	\N	2021-02-11 09:28:50	SUPERAULA
2474	SABANA DE UCHIRE	28	\N	2021-02-11 09:28:51	NODO OUTDOOR
2481	SAN TOME	28	\N	2021-02-11 09:28:51	NODO OUTDOOR
2221	CHAGUARAMAS	26	\N	2021-02-11 09:28:51	NODO OUTDOOR
2428	EL TIGRE III (DIGITAL EL TIGRE)	28	\N	2021-02-11 09:28:51	CENTRAL CRITICA
2486	SANTA FE	28	\N	2021-03-04 10:03:45	OPSUT
2363	Y02 C JUAN GRIEGO PALMA CRUZ	27	\N	2021-02-11 09:28:52	DLC
2507	Y02 C PUERTO PIRITU LAS ISLETAS	28	\N	2021-02-11 09:28:52	DLC
2365	Y03 C JORGE COLL CAPITANIA DE PUERTO	27	\N	2021-02-11 09:28:52	DLC
2296	Y03 C MATURIN LA CASCADA	26	\N	2021-02-11 09:28:52	DLC
2373	Y06 C JORGE COLL SAMBIL MARGARITA	27	\N	2021-02-11 09:28:52	DLC
2461	PLAZA MAR (NO CANTV)	28	\N	2021-02-11 09:28:52	NO CANTV
2400	CANTAURA 01	28	\N	2021-02-11 09:28:53	NODO OUTDOOR
2215	CARIPITO I	26	\N	2021-02-11 09:28:53	NODO OUTDOOR
2404	CC LOS PINOS	28	\N	2021-02-11 09:28:53	NODO OUTDOOR
2227	EL SAMAN MATURIN	26	\N	2021-02-11 09:28:54	NODO OUTDOOR
2315	EL YAQUE	27	\N	2021-02-11 09:28:54	NODO OUTDOOR
2320	HESPERIA	27	\N	2021-02-11 09:28:54	NODO OUTDOOR
2325	LA CAPILLA	27	\N	2021-02-11 09:28:54	NODO OUTDOOR
2440	LAS ANCLAS	28	\N	2021-02-11 09:28:55	NODO OUTDOOR
2335	LAS MARITES	27	\N	2021-02-11 09:28:55	NODO OUTDOOR
2254	LOS ANGELES	26	\N	2021-02-11 09:28:55	NODO OUTDOOR
2264	MONUMENTAL	26	\N	2021-02-11 09:28:55	NODO OUTDOOR
2340	PLAZA PARAGUACHI	27	\N	2021-02-11 09:28:55	NODO OUTDOOR
2274	RORAIMA	26	\N	2021-02-11 09:28:55	NODO OUTDOOR
2502	VIEJO NEVERI	28	\N	2021-02-11 09:28:56	NODO OUTDOOR
2356	VILLA ROSA I	27	\N	2021-02-11 09:28:56	NODO OUTDOOR
2445	LOS CHAGUARAMOS	28	\N	2021-03-04 10:06:38	NODO OUTDOOR
2222	COLORADITO	26	\N	2021-02-11 09:28:56	OPSUT
2352	TACARIGUA	27	\N	2021-03-04 11:52:39	NODO OUTDOOR
2657	PUERTO AYACUCHO	22	2021-02-04 12:38:37	2021-03-19 10:10:40	CENTRAL
2658	CERRO CLUECO	22	2021-02-04 12:39:23	2021-03-19 15:52:07	REPETIDORA
2208	BARRANCAS DEL ORINOCO	26	\N	2021-02-11 09:28:48	NODO OUTDOOR
149	BATATAL (TX)	2	\N	2021-02-11 09:28:48	CENTRAL
1809	BELLA VISTA CERRO CHUCUTA	19	\N	2021-02-11 09:28:49	CENTRAL
573	BIRONGO	6	\N	2021-02-11 09:28:49	CENTRAL
895	BIRUACA	10	\N	2021-02-11 09:28:49	CENTRAL
1405	BOCA DE AROA	15	\N	2021-02-11 09:28:49	CENTRAL
2391	BOCA DE UCHIRE	28	\N	2021-02-11 09:28:49	NODO OUTDOOR
152	BOCONO	2	\N	2021-02-11 09:28:49	SUPERAULA
1757	BORAURE	18	\N	2021-02-11 09:28:49	NODO OUTDOOR
14	BRAMON	1	\N	2021-02-11 09:28:49	CENTRAL
246	BUM BUM	3	\N	2021-02-11 09:28:49	NODO OUTDOOR
2104	CABILLAS II	22	\N	2021-02-11 09:28:49	CENTRAL
1619	CABUDARE II	17	\N	2021-02-11 09:28:49	CENTRAL
1407	CABURE	15	\N	2021-02-11 09:28:49	NODO OUTDOOR
1301	CAGUA BELLA VISTA	14	\N	2021-02-11 09:28:49	CENTRAL
1912	CAJA DE AGUA	20	\N	2021-02-11 09:28:49	CENTRAL
1187	CALVARIO	13	\N	2021-02-11 09:28:49	CENTRAL
969	CAMORUCO GUATAPARO	11	\N	2021-02-11 09:28:49	CENTRAL
970	CAMPO CARABOBO	11	\N	2021-02-11 09:28:49	NODO OUTDOOR
328	CAÑO MUERTO	4	\N	2021-02-11 09:28:49	CENTRAL
249	CAPITANEJO	3	\N	2021-02-11 09:28:49	NODO OUTDOOR
976	CASTILLITO	11	\N	2021-02-11 09:28:49	CENTRAL
2301	CENTRO DE ATENCION SAMBIL MARGARITA	27	\N	2021-02-11 09:28:49	CENTRAL
1990	CENTRO OPERATIVO SABANETA	21	\N	2021-02-11 09:28:49	CENTRAL
1522	CERRO ARAURE	16	\N	2021-02-11 09:28:49	CENTRAL
2111	CERRO BEGOTE	22	\N	2021-02-11 09:28:49	CENTRAL
436	CERRO CADAFE	5	\N	2021-02-11 09:28:49	CENTRAL
1198	CERRO CAMBURITO	13	\N	2021-02-11 09:28:49	CENTRAL
21	CERRO EL CRISTO	1	\N	2021-02-11 09:28:49	CENTRAL
1307	CERRO EL TANQUE	14	\N	2021-02-11 09:28:49	CENTRAL
1413	CERRO LA GOYA	15	\N	2021-02-11 09:28:49	CENTRAL
1201	CERRO LA OLLA	13	\N	2021-02-11 09:28:49	CENTRAL
1919	CERRO LAS PALMAS	20	\N	2021-02-11 09:28:49	CENTRAL
1625	CERRO LOMA DE LA MATICA	17	\N	2021-02-11 09:28:49	CENTRAL
250	CERRO MUPATE	3	\N	2021-02-11 09:28:49	CENTRAL
24	CERRO SANTA ROSA QUENIQUEA	1	\N	2021-02-11 09:28:49	CENTRAL
1206	CHAGUARAMAS II	13	\N	2021-02-11 09:28:49	OPSUT
158	CHEJENDE	2	\N	2021-02-11 09:28:49	NODO OUTDOOR
1764	CHIVACOA PLANTA EXT	18	\N	2021-02-11 09:28:49	CENTRAL
1417	CHURUGUARA	15	\N	2021-02-11 09:28:49	CENTRAL
1313	COLONIA TOVAR	14	\N	2021-02-11 09:28:49	NODO OUTDOOR
336	CONCHA GARCITA	4	\N	2021-02-11 09:28:49	CENTRAL
592	CORTIJOS 5	6	\N	2021-02-11 09:28:49	CENTRAL
1427	CTRO COM COSTA AZUL	15	\N	2021-02-11 09:28:49	CENTRAL
842	CUPIRA I	9	\N	2021-02-11 09:28:49	CENTRAL
1818	CURAGUA UD-307	19	\N	2021-02-11 09:28:49	CENTRAL
1632	CURARIGUA	17	\N	2021-02-11 09:28:49	CENTRAL
36	EL AMPARO II	1	\N	2021-02-11 09:28:49	CENTRAL
1136	EL AMPARO PUEBLO	12	\N	2021-02-11 09:28:49	CENTRAL
991	EL BOSQUE I	11	\N	2021-02-11 09:28:49	CENTRAL
2126	EL CONSEJO DE CIRUMA I	22	\N	2021-02-11 09:28:49	CENTRAL
41	EL COROZO	1	\N	2021-02-11 09:28:49	NODO OUTDOOR
162	EL DIVIDIVE	2	\N	2021-02-11 09:28:49	NODO OUTDOOR
2225	EL FURRIAL	26	\N	2021-02-11 09:28:49	NODO OUTDOOR
1638	EL GUARICO	17	\N	2021-02-11 09:28:49	NODO OUTDOOR
452	EL JARILLO	5	\N	2021-02-11 09:28:49	CENTRAL
1828	EL MANTECO I	19	\N	2021-02-11 09:28:49	NODO OUTDOOR
342	EL MORALITO	4	\N	2021-02-11 09:28:49	CENTRAL
1924	EL PERU	20	\N	2021-02-11 09:28:49	NODO OUTDOOR
50	EL POBLADO	1	\N	2021-02-11 09:28:49	NODO OUTDOOR
1218	EL PUEBLO CAMATAGUA	13	\N	2021-02-11 09:28:49	OPSUT
261	EL REAL	3	\N	2021-02-11 09:28:49	NODO OUTDOOR
2425	EL RINCON (R2000 CANTACLARO)	28	\N	2021-02-11 09:28:49	CENTRAL
1649	EL TOCUYO I DIGITAL	17	\N	2021-02-11 09:28:49	CENTRAL
1437	EL VINCULO	15	\N	2021-02-11 09:28:49	CENTRAL
1224	ESPINO	13	\N	2021-02-11 09:28:49	CENTRAL
1556	REPETIDORA SAN JOSE DE LA MONTA	16	\N	2021-02-11 09:28:49	CENTRAL
606	FILA DE MARICHES KM 14	6	\N	2021-02-11 09:28:50	CENTRAL
1770	GUAMA	18	\N	2021-02-11 09:28:50	NODO OUTDOOR
1227	GUARDATINAJAS	13	\N	2021-02-11 09:28:50	NODO OUTDOOR
347	GUAYABONES	4	\N	2021-02-11 09:28:50	CENTRAL
1010	GUIGUE	11	\N	2021-02-11 09:28:50	SUPERAULA
755	HOYO DE LA PUERTA	7	\N	2021-02-11 09:28:50	CENTRAL
1654	HUMOCARO ALTO	17	\N	2021-02-11 09:28:50	NODO OUTDOOR
170	JAJO	2	\N	2021-02-11 09:28:50	CENTRAL
2241	JUSEPIN	26	\N	2021-02-11 09:28:50	NODO OUTDOOR
352	LA AZULITA I	4	\N	2021-02-11 09:28:50	CENTRAL
173	LA BEATRIZ	2	\N	2021-02-11 09:28:50	CENTRAL
61	LA CONCORDIA II (19 de Abril)	1	\N	2021-02-11 09:28:50	CENTRAL
177	LA MESA DE ESNUJAQUE	2	\N	2021-02-11 09:28:50	NODO OUTDOOR
1671	LA MORA PIEDRA AZUL	17	\N	2021-02-11 09:28:50	CENTRAL
360	LA PLAYA	4	\N	2021-02-11 09:28:50	NODO OUTDOOR
1021	LA PRADERA	11	\N	2021-02-11 09:28:50	CENTRAL
1934	LA SABANITA	20	\N	2021-02-11 09:28:50	CENTRAL
911	LA TRINIDAD DE ORICHUNA	10	\N	2021-02-11 09:28:50	CENTRAL
74	LAS DANTAS	1	\N	2021-02-11 09:28:50	CENTRAL
1028	LAS MANZANAS	11	\N	2021-02-11 09:28:50	CENTRAL
2523	CERRO LOS MANANTIALES	29	\N	2021-03-03 11:07:21	CENTRAL
2532	CUMANA INDUSTRIAS	29	\N	2021-03-03 11:09:35	CENTRAL
2547	GUIRIA	29	\N	2021-03-03 11:25:40	CENTRAL
2538	EL PILAR	29	\N	2021-03-03 11:44:36	NODO OUTDOOR
259	CUATRICENTENARIA	3	\N	2021-03-03 14:58:29	CENTRAL
364	LAGUNILLAS	4	\N	2021-03-03 15:15:36	OPSUT
77	LAS LOMAS	1	\N	2021-03-03 15:39:01	CENTRAL
901	CUNAVICHE	10	\N	2021-03-04 11:19:51	CENTRAL CRITICA
474	LA PASTORA	5	\N	2021-03-09 11:46:52	CENTRAL
815	CABO CODERA	6	\N	2021-03-12 13:34:47	CENTRAL
816	CAPAYA	6	\N	2021-03-12 13:35:39	OPSUT
817	CARTANAL	6	\N	2021-03-12 13:37:14	CENTRAL
823	CAUCAGUA II	6	\N	2021-03-12 13:41:42	NODO OUTDOOR
830	CHARALLAVE I	6	\N	2021-03-12 13:49:16	CENTRAL
845	EL GUAPO	6	\N	2021-03-12 14:06:00	SUPERAULA
858	REPETIDORA CHARALLAVE	6	\N	2021-03-12 14:29:13	CENTRAL
1982	CAMPO LA PAZ	30	\N	2021-03-16 15:03:31	CENTRAL
2010	ISLA DE TOAS	30	\N	2021-03-16 15:21:34	NODO OUTDOOR
186	LAS VIRTUDES	2	\N	2021-02-11 09:28:50	NODO OUTDOOR
1238	LIBERTAD DE ORITUCO	13	\N	2021-02-11 09:28:50	CENTRAL
2253	LOMA DE LA VIRGEN	26	\N	2021-02-11 09:28:50	CENTRAL
638	LOMAS DE LA LAGUNITA	6	\N	2021-02-11 09:28:50	CENTRAL
2160	LOS LAURELES	22	\N	2021-02-11 09:28:50	CENTRAL
1850	LOS OLIVOS II	19	\N	2021-02-11 09:28:50	NODO OUTDOOR
1349	LOS TANQUES	14	\N	2021-02-11 09:28:50	NODO OUTDOOR
1457	LOS TAQUES	15	\N	2021-02-11 09:28:50	NODO OUTDOOR
1782	LOS TUBOS	18	\N	2021-02-11 09:28:50	CENTRAL
646	LOS VELASQUEZ	6	\N	2021-02-11 09:28:50	CENTRAL
492	LUIS HURTADO	5	\N	2021-02-11 09:28:50	CENTRAL
1152	MACAPO	12	\N	2021-02-11 09:28:50	NODO OUTDOOR
189	MENDOZA FRIA	2	\N	2021-02-11 09:28:50	NODO OUTDOOR
2165	MENE GRANDE	22	\N	2021-02-11 09:28:50	NODO OUTDOOR
1543	MESA CAVACA	16	\N	2021-02-11 09:28:50	NODO OUTDOOR
374	MESA DE EJIDO	4	\N	2021-02-11 09:28:50	CENTRAL
1048	MIRANDA II	11	\N	2021-02-11 09:28:50	NODO OUTDOOR
192	MONAY	2	\N	2021-02-11 09:28:50	NODO OUTDOOR
1053	MORRO SAN BLAS	11	\N	2021-02-11 09:28:50	CENTRAL
377	MUCURUBA	4	\N	2021-02-11 09:28:50	NODO OUTDOOR
196	NIQUITAO	2	\N	2021-02-11 09:28:50	CENTRAL
1784	NIRGUA DIG	18	\N	2021-02-11 09:28:50	CENTRAL
2169	NUEVA DELICIAS	22	\N	2021-02-11 09:28:50	CENTRAL
585	CC PLAZA LAS AMERICAS	6	\N	2021-02-11 09:28:50	CENTRAL
432	CC SAN MARTIN	5	\N	2021-02-11 09:28:50	CENTRAL
1359	OCUMARE DE LA COSTA	14	\N	2021-02-11 09:28:50	NODO OUTDOOR
201	PAMPAN	2	\N	2021-02-11 09:28:50	NODO OUTDOOR
1549	PAPELON	16	\N	2021-02-11 09:28:50	SUPERAULA
380	PARAMO GUARAQUE	4	\N	2021-02-11 09:28:50	CENTRAL
1249	PARAPARA DE ORTIZ	13	\N	2021-02-11 09:28:50	CENTRAL
2457	PARIAGUAN	28	\N	2021-02-11 09:28:50	SUPERAULA
279	PEDRAZA LA VIEJA	3	\N	2021-02-11 09:28:50	NODO OUTDOOR
1468	PEDREGAL	15	\N	2021-02-11 09:28:50	NODO OUTDOOR
1552	PIMPINELA	16	\N	2021-02-11 09:28:50	NODO OUTDOOR
513	PROPATRIA	5	\N	2021-02-11 09:28:50	CENTRAL
102	PUEBLO NUEVO (PARAMILLO II)	1	\N	2021-02-11 09:28:50	CENTRAL
2468	PUERTO PIRITU II (DIGITAL PUERTO PIRITU)	28	\N	2021-02-11 09:28:50	CENTRAL
1711	QUEBRADA ARRIBA	17	\N	2021-02-11 09:28:50	CENTRAL
918	QUINTERO	10	\N	2021-02-11 09:28:50	CENTRAL
2273	QUIRIQUIRE	26	\N	2021-02-11 09:28:50	NODO OUTDOOR
1063	QUIZANDA	11	\N	2021-02-11 09:28:50	CENTRAL
1156	REPETIDORA EL MIRADOR SAN CARLOS	12	\N	2021-02-11 09:28:50	CENTRAL
107	REPETIDORA MATA MULA	1	\N	2021-02-11 09:28:50	CENTRAL
671	RIO CHICO	6	\N	2021-02-11 09:28:51	OPSUT
1719	RIO TOCUYO	17	\N	2021-02-11 09:28:51	CENTRAL
1255	SABANA GRANDE DE ORITUCO	13	\N	2021-02-11 09:28:51	CENTRAL
2180	SABANETA DE PALMA	22	\N	2021-02-11 09:28:51	CENTRAL
113	SAN DIEGO LOS QUEMADOS	1	\N	2021-02-11 09:28:51	CENTRAL
1790	SAN FELIPE ZONA INDUSTRIAL	18	\N	2021-02-11 09:28:51	CENTRAL
1261	SAN JOSE DE GUARIBE	13	\N	2021-02-11 09:28:51	NODO OUTDOOR
521	SAN JOSE DE LOS ALTOS TX	5	\N	2021-02-11 09:28:51	CENTRAL
291	SAN JOSE FILA LA HONDA	3	\N	2021-02-11 09:28:51	CENTRAL
116	SAN JOSECITO	1	\N	2021-02-11 09:28:51	CENTRAL
1481	SAN JUAN DE LOS CAYOS	15	\N	2021-02-11 09:28:51	SUPERAULA
212	SAN LAZARO	2	\N	2021-02-11 09:28:51	NODO OUTDOOR
1369	SAN MATEO II	14	\N	2021-02-11 09:28:51	NODO OUTDOOR
1562	SAN RAFAEL DE PALO ALZADO	16	\N	2021-02-11 09:28:51	CENTRAL
2281	SANTA BARBARA MATURIN	26	\N	2021-02-11 09:28:51	CENTRAL
1372	SANTA CRUZ DE ARAGUA	14	\N	2021-02-11 09:28:51	CENTRAL
1875	SANTA ELENA DE UAIREN	19	\N	2021-02-11 09:28:51	NODO OUTDOOR
218	SANTA ISABEL	2	\N	2021-02-11 09:28:51	NODO OUTDOOR
297	SANTA LUCIA	3	\N	2021-02-11 09:28:51	NODO OUTDOOR
864	SANTA LUCIA I	9	\N	2021-02-11 09:28:51	NODO OUTDOOR
2283	SANTA ROSA (SAN ANTONIO TX)	26	\N	2021-02-11 09:28:51	CENTRAL
124	SEBORUCO	1	\N	2021-02-11 09:28:51	NODO OUTDOOR
1731	SIQUISIQUE	17	\N	2021-02-11 09:28:51	NODO OUTDOOR
1073	SORPRESA	11	\N	2021-02-11 09:28:51	CENTRAL
397	TABAY	4	\N	2021-02-11 09:28:51	NODO OUTDOOR
680	TACARIGUA DE LA LAGUNA	6	\N	2021-02-11 09:28:51	NODO OUTDOOR
1374	TEJERIAS II	14	\N	2021-02-11 09:28:51	CENTRAL
2188	TIA JUANA NP	22	\N	2021-02-11 09:28:51	CENTRAL
1490	TOCUYO DE LA COSTA	15	\N	2021-02-11 09:28:51	NODO OUTDOOR
222	TORONDOY	2	\N	2021-02-11 09:28:51	CENTRAL
402	TOVAR II	4	\N	2021-02-11 09:28:51	CENTRAL
1887	UNARE	19	\N	2021-02-11 09:28:51	CENTRAL
1795	URACHICHE	18	\N	2021-02-11 09:28:51	NODO OUTDOOR
227	URB EL ROSAL PAMPANITO	2	\N	2021-02-11 09:28:51	NODO OUTDOOR
404	URBANIZACION CARABOBO	4	\N	2021-02-11 09:28:51	CENTRAL
1495	URL JUDIBANA I	15	\N	2021-02-11 09:28:51	CENTRAL
132	URL PARAMILLO	1	\N	2021-02-11 09:28:51	CENTRAL
536	URL RIO CRISTAL	5	\N	2021-02-11 09:28:51	CENTRAL
929	UVERITO	10	\N	2021-02-11 09:28:51	OPSUT
2500	VALLE DE GUANAPE I R2000	28	\N	2021-02-11 09:28:51	CENTRAL
698	VISTA HERMOSA	6	\N	2021-02-11 09:28:51	NODO OUTDOOR
1799	YARITAGUA TX	18	\N	2021-02-11 09:28:51	CENTRAL
1392	ZUATA	14	\N	2021-02-11 09:28:51	CENTRAL
2379	ANACO I (DIGITAL ANACO)	28	\N	2021-02-11 09:28:51	CENTRAL CRITICA
2566	PUERTO DE HIERRO	29	\N	2021-03-03 11:34:36	CENTRAL
2573	SAN ANTONIO DEL GOLFO	29	\N	2021-03-03 11:37:16	NODO OUTDOOR
2579	SANTA CRUZ CARIACO	29	\N	2021-03-03 11:40:15	CENTRAL
100	PIRINEOS	1	\N	2021-03-03 15:41:00	CENTRAL
269	LAS PALMAS	3	\N	2021-03-03 15:12:49	NODO OUTDOOR
87	LOS NARANJOS	1	\N	2021-03-03 15:48:46	CENTRAL
1610	BARQUISIMETO CENTRO	17	\N	2021-03-08 12:25:11	CENTRAL CRITICA
795	MACUTO JUBILADOS	5	\N	2021-03-12 09:38:02	CENTRAL
802	NAIGUATA III	5	\N	2021-03-12 09:39:53	CENTRAL
856	PAPARO	6	\N	2021-03-12 14:25:11	CENTRAL
870	SANTIAGO DE LEON	6	\N	2021-03-12 14:38:35	NODO OUTDOOR
861	SAN FRANCISCO DE YARE	6	\N	2021-03-12 14:31:59	CENTRAL
2043	PANAMERICANA	30	\N	2021-03-16 15:34:46	CENTRAL
2071	TAMARE LA SOLITA	30	\N	2021-03-16 15:45:51	CENTRAL
1956	SAN FERNANDO DE ATABAPO	22	\N	2021-03-19 10:08:50	NODO OUTDOOR
957	BASE NAVAL PUERTO CABELLO	11	\N	2021-02-11 09:28:51	CENTRAL CRITICA
1178	CABRUTA	13	\N	2021-02-11 09:28:51	NODO OUTDOOR
1979	CAMPO BERNAL	21	\N	2021-02-11 09:28:51	CENTRAL CRITICA
2214	CARIPITO	26	\N	2021-02-11 09:28:51	CENTRAL CRITICA
1196	CASETA TX SAN GERONIMO B (NO CANTV)	13	\N	2021-02-11 09:28:51	CENTRAL CRITICA
744	ALTA FLORIDA	7	\N	2021-02-11 09:28:51	CENTRAL CRITICA
595	EL CAFETAL I	6	\N	2021-02-11 09:28:51	CENTRAL CRITICA
457	FAJARDO I	5	\N	2021-02-11 09:28:51	CENTRAL CRITICA
1530	GUANARE II	16	\N	2021-02-11 09:28:51	CENTRAL CRITICA
615	GUATIRE I	6	\N	2021-02-11 09:28:51	CENTRAL CRITICA
747	LA SALLE	7	\N	2021-02-11 09:28:51	CENTRAL CRITICA
631	LA URBINA	6	\N	2021-02-11 09:28:51	CENTRAL CRITICA
490	LOS TEQUES EQUIPOS I	5	\N	2021-02-11 09:28:51	CENTRAL CRITICA
647	MACARACUAY	6	\N	2021-02-11 09:28:51	CENTRAL CRITICA
796	MAIQUETIA I	8	\N	2021-02-11 09:28:51	CENTRAL CRITICA
1352	MARACAY I	14	\N	2021-02-11 09:28:51	CENTRAL CRITICA
499	NUEVA GRANADA	5	\N	2021-02-11 09:28:51	CENTRAL CRITICA
1264	SAN JUAN DE LOS MORROS  II	13	\N	2021-02-11 09:28:51	CENTRAL CRITICA
1387	VILLA DE CURA	14	\N	2021-02-11 09:28:51	NODO OUTDOOR
1816	CIUDAD BOLIVAR	19	\N	2021-02-11 09:28:51	CENTRAL CRITICA
601	EL HATILLO	6	\N	2021-02-11 09:28:51	CENTRAL CRITICA
461	FUERTE TIUNA GUARNICION (NO CANTV)	5	\N	2021-02-11 09:28:51	CENTRAL CRITICA
1234	LAS MERCEDES DEL LLANO	13	\N	2021-02-11 09:28:52	NODO OUTDOOR
1854	MACAGUA PTO ORDAZ (NO CANTV)	19	\N	2021-02-11 09:28:52	CENTRAL CRITICA
2261	MATURIN CENTRO II	26	\N	2021-02-11 09:28:52	CENTRAL CRITICA
372	MERIDA CENTRO	4	\N	2021-02-11 09:28:52	CENTRAL CRITICA
1696	MORAN	17	\N	2021-02-11 09:28:52	CENTRAL CRITICA
1472	PUERTO CUMAREBO	15	\N	2021-02-11 09:28:52	CENTRAL CRITICA
1868	PUERTO ORDAZ	19	\N	2021-02-11 09:28:52	OAC
206	SABANA DE MENDOZA	2	\N	2021-02-11 09:28:52	SUPERAULA
111	SAN CRISTOBAL CENTRO	1	\N	2021-02-11 09:28:52	CENTRAL CRITICA
919	SAN FERNANDO DE APURE II	10	\N	2021-02-11 09:28:52	CENTRAL CRITICA
2192	URDANETA II	22	\N	2021-02-11 09:28:52	CENTRAL CRITICA
231	VALERA II	2	\N	2021-02-11 09:28:52	CENTRAL CRITICA
1499	YARACAL	15	\N	2021-02-11 09:28:52	NODO OUTDOOR
1290	ZARAZA	13	\N	2021-02-11 09:28:52	CENTRAL CRITICA
666	PLAZA BONITA	6	\N	2021-02-11 09:28:52	DLC
1580	Y01 C ACARIGUA S RAFAEL DE ONOTO	16	\N	2021-02-11 09:28:52	DLC
1096	Y01 C CAMORUCO LAS CHIMENEAS	11	\N	2021-02-11 09:28:52	DLC
702	Y01 C FRANCISCO SALIAS EL PEÑON	6	\N	2021-02-11 09:28:52	DLC
539	Y01 C JARDINES CAÑO SECO	5	\N	2021-02-11 09:28:52	DLC
2360	Y01 C JUAN GRIEGO VIA PEDRO GONZALEZ	27	\N	2021-02-11 09:28:52	DLC
706	Y01 C MACARACUAY LOMA ALTA EDF A	6	\N	2021-02-11 09:28:52	DLC
134	Y01 C SAN CRISTOBAL CENTRO EL MIRADOR	1	\N	2021-02-11 09:28:52	DLC
543	Y01 C SAN OMERO URL LAGUNETICA	5	\N	2021-02-11 09:28:52	DLC
878	Y01 C SANTA TERESA DOS LAGUNAS BLOQUE 3	9	\N	2021-02-11 09:28:52	DLC
1897	Y01 C UNARE PUNTA CUCHILLA	19	\N	2021-02-11 09:28:52	DLC
710	Y01 C URBINA GUAICOCO LOS PINOS	6	\N	2021-02-11 09:28:52	DLC
1101	Y01 C VALENCIA SUR NVA VALENCIA	11	\N	2021-02-11 09:28:52	DLC
1288	Y01 C VALLE DE LA PASCUA LA TRINID CASA	13	\N	2021-02-11 09:28:52	DLC
1103	Y02 C FUNDACION LA LOMA	11	\N	2021-02-11 09:28:52	DLC
714	Y02 C HATILLO CC LAS LOMAS LAGUNITA	6	\N	2021-02-11 09:28:52	DLC
548	Y02 C JARDINES LA BLANCA	5	\N	2021-02-11 09:28:52	DLC
2362	Y02 C JORGE COLL AGUA DE VACA LAS ACACIA	27	\N	2021-02-11 09:28:52	DLC
2202	Y02 C SAN MIGUEL CASA 560 ALTOS SOL AMA	22	\N	2021-02-11 09:28:52	DLC
2364	Y02 C SAN RAFAEL TERRAZAS DE GUACUCO	27	\N	2021-02-11 09:28:52	DLC
718	Y02 C URBINA ARAGUANEY VIA GUARENAS 2	6	\N	2021-02-11 09:28:52	DLC
235	Y02 DLC LIMONCITOS URL LA PUERTA 2	2	\N	2021-02-11 09:28:52	DLC
883	Y03 C CHARALLAVE MATA LINDA 2	9	\N	2021-02-11 09:28:52	DLC
1107	Y03 C FUNDACION PRINC L FUNVAL	11	\N	2021-02-11 09:28:52	DLC
552	Y03 C PRADO DE MARIA AV CATALUÑA	5	\N	2021-02-11 09:28:52	DLC
724	Y03 C TRAPICHITO TERRAZ DEL ESTE EDF 10	6	\N	2021-02-11 09:28:52	DLC
1389	Y03 C TURMERO CENTRO IND TRASILINI	14	\N	2021-02-11 09:28:52	DLC
1751	Y03 EL MANZANO III	17	\N	2021-02-11 09:28:52	DLC
727	Y04 C HATILLO ENTRADA VILLA NUEV HATILL	6	\N	2021-02-11 09:28:52	DLC
732	Y05 C MACARACUAY PABLO VI FARMACIA POPU	6	\N	2021-02-11 09:28:52	DLC
1901	Y05 C UNARE MANZ 9 SECTOR CORE 8	19	\N	2021-02-11 09:28:52	DLC
1112	Y05 C VALENCIA SUR VIA TOCUYITO CALLE 4	11	\N	2021-02-11 09:28:52	DLC
556	Y06 C JARDINES BRICEÑO OTTALORA FUERT T	5	\N	2021-02-11 09:28:52	DLC
2376	Y07 C SAN RAFAEL CONJUNTO RECREACIONAL M	27	\N	2021-02-11 09:28:52	DLC
737	Y08 C TRAPICHITO CIUD CASARAPA PARC  26	6	\N	2021-02-11 09:28:52	DLC
1116	Y08 C VALENCIA SUR CALLE 3 LOMAS FUNVAL	11	\N	2021-02-11 09:28:52	DLC
1121	Y11 C VALENCIA SUR LOS CHORRITOS LOS ALM	11	\N	2021-02-11 09:28:52	DLC
2	AEROPUERTO SANTO DOMINGO (NO CANTV)	1	\N	2021-02-11 09:28:52	NO CANTV
1658	INTERCON MOVISTAR  LOS LEONES (NO CANTV)	17	\N	2021-02-11 09:28:52	NO CANTV
503	PALACIO BLANCO (NO CANTV)	5	\N	2021-02-11 09:28:52	NO CANTV
664	PGC ALTAMIRA (NO CANTV)	6	\N	2021-02-11 09:28:52	NO CANTV
763	PGC LA CANDELARIA (NO CANTV)	7	\N	2021-02-11 09:28:52	NO CANTV
123	SANTA MARIA DE CAPARO CADAFE (NO CANTV)	1	\N	2021-02-11 09:28:52	NO CANTV
2519	CASANAY	29	\N	2021-03-03 11:05:30	CENTRAL CRITICA
913	MANTECAL	10	\N	2021-03-04 11:44:56	NODO OUTDOOR
745	BELLO MONTE	7	\N	2021-03-09 09:13:13	CENTRAL
1335	LA ROMANA	14	\N	2021-03-08 10:20:02	CENTRAL
58	GUASDUALITO	1	\N	2021-03-08 10:21:46	CENTRAL
1997	COQUIVACOA	30	\N	2021-03-16 15:13:24	OAC
2027	LAS INDUSTRIAS  (KM40)	30	\N	2021-03-16 15:27:11	CENTRAL CRITICA
2056	SAN IGNACIO	30	\N	2021-03-16 15:38:36	NODO OUTDOOR
2061	SAN RAFAEL DEL MOJAN	30	\N	2021-03-16 15:41:22	CENTRAL CRITICA
2074	TX AEROPUERTO LA CHINITA (NO CANTV)	30	\N	2021-03-16 15:46:36	NO CANTV
2082	Y01 C INDUSTRIAS CENTRO CONV MARACAIBO	30	\N	2021-03-16 15:48:22	DLC
756	CNT EQUIPOS I	7	\N	2021-03-22 14:32:27	CENTRAL CRITICA
1748	WIFI SANTA ROSA (NO CANTV)	17	\N	2021-02-11 09:28:52	NO CANTV
1588	AGUA VIVA CENTRO	17	\N	2021-02-11 09:28:53	NODO OUTDOOR
1591	AGUA VIVA VALLECITO	17	\N	2021-02-11 09:28:53	NODO OUTDOOR
1907	ALBARICAL	20	\N	2021-02-11 09:28:53	NODO OUTDOOR
317	ALBERTO ADRIANI	4	\N	2021-02-11 09:28:53	NODO OUTDOOR
563	ALI PRIMERA	6	\N	2021-02-11 09:28:53	NODO OUTDOOR
1596	ALI PRIMERA III	17	\N	2021-02-11 09:28:53	NODO OUTDOOR
412	ALTO VERDE	5	\N	2021-02-11 09:28:53	NODO OUTDOOR
4	ALTOS DE TUCAPE	1	\N	2021-02-11 09:28:53	NODO OUTDOOR
566	ALTOS DE VILLANUEVA	6	\N	2021-02-11 09:28:53	NODO OUTDOOR
1395	AMUAY	15	\N	2021-02-11 09:28:53	NODO OUTDOOR
1599	ANDRES BELLO I	17	\N	2021-02-11 09:28:53	NODO OUTDOOR
1398	ANTIGUO AEROPUERTO III	15	\N	2021-02-11 09:28:53	NODO OUTDOOR
1602	APAMATE	17	\N	2021-02-11 09:28:53	NODO OUTDOOR
955	ARAGUANEY	11	\N	2021-02-11 09:28:53	NODO OUTDOOR
1399	ARISTIDES CALVANI	15	\N	2021-02-11 09:28:53	NODO OUTDOOR
414	ARNULFO ROMERO II	5	\N	2021-02-11 09:28:53	NODO OUTDOOR
2207	AVE PARAISO	26	\N	2021-02-11 09:28:53	NODO OUTDOOR
417	AVENIDA NICARAGUA	5	\N	2021-02-11 09:28:53	NODO OUTDOOR
2389	AVES DE BORA BORA	28	\N	2021-02-11 09:28:53	NODO OUTDOOR
1175	BARBACOA I	13	\N	2021-02-11 09:28:53	NODO OUTDOOR
9	BARRANCAS TACHIRA II	1	\N	2021-02-11 09:28:53	NODO OUTDOOR
2091	BARRIO FEDERACION III	22	\N	2021-02-11 09:28:53	NODO OUTDOOR
2095	BARRIO LAS VEGAS II	22	\N	2021-02-11 09:28:53	NODO OUTDOOR
1808	BARRIO OBRERO	19	\N	2021-02-11 09:28:53	NODO OUTDOOR
2098	BARRIO PUNTO FIJO III	22	\N	2021-02-11 09:28:53	NODO OUTDOOR
1510	BARRIO VENEZUELA	16	\N	2021-02-11 09:28:53	NODO OUTDOOR
1297	BASE LIBERTADOR II	14	\N	2021-02-11 09:28:53	NODO OUTDOOR
958	BATALLA DE CARABOBO	11	\N	2021-02-11 09:28:53	NODO OUTDOOR
323	BELLO MONTE	4	\N	2021-02-11 09:28:53	NODO OUTDOOR
963	BICENTENARIA	11	\N	2021-02-11 09:28:53	NODO OUTDOOR
1513	BISCUCUY II	16	\N	2021-02-11 09:28:53	NODO OUTDOOR
1615	BOLIVAR II	17	\N	2021-02-11 09:28:53	NODO OUTDOOR
575	BOSQUE DEL INGENIO  NA08-03	6	\N	2021-02-11 09:28:53	NODO OUTDOOR
420	BRISAS DE PROPATRIA NA10-11	5	\N	2021-02-11 09:28:53	NODO OUTDOOR
1910	BRISAS DEL ESTE LA SABANITA	20	\N	2021-02-11 09:28:53	NODO OUTDOOR
247	CAFETAL	3	\N	2021-02-11 09:28:53	NODO OUTDOOR
1409	CAJA DE AGUA II	15	\N	2021-02-11 09:28:53	NODO OUTDOOR
1186	CALANCHE II	13	\N	2021-02-11 09:28:53	NODO OUTDOOR
967	CALICANTO	11	\N	2021-02-11 09:28:53	NODO OUTDOOR
15	CALICHE	1	\N	2021-02-11 09:28:53	NODO OUTDOOR
422	CALLE CHILE I	5	\N	2021-02-11 09:28:53	NODO OUTDOOR
2109	CAMPO LINDO	22	\N	2021-02-11 09:28:53	NODO OUTDOOR
1759	CAMUNARE ROJO	18	\N	2021-02-11 09:28:53	NODO OUTDOOR
17	CANEYES II	1	\N	2021-02-11 09:28:53	NODO OUTDOOR
1190	CANTAGALLO	13	\N	2021-02-11 09:28:53	NODO OUTDOOR
2401	CANTAURA 02	28	\N	2021-02-11 09:28:53	NODO OUTDOOR
1129	CAÑO HONDO	12	\N	2021-02-11 09:28:53	NODO OUTDOOR
329	CAÑO SECO	4	\N	2021-02-11 09:28:53	NODO OUTDOOR
2212	CARAPAL DE GUARA I	26	\N	2021-02-11 09:28:53	NODO OUTDOOR
579	CASA AGRARIA	6	\N	2021-02-11 09:28:53	NODO OUTDOOR
1520	CASERIO TAPA DE PIEDRA	16	\N	2021-02-11 09:28:53	NODO OUTDOOR
1913	CAUCAGÜITA	20	\N	2021-02-11 09:28:53	NODO OUTDOOR
1412	CC PARAGUANA MALL	15	\N	2021-02-11 09:28:53	NODO OUTDOOR
435	CENTRO AMERICA II	5	\N	2021-02-11 09:28:53	NODO OUTDOOR
1628	CHIRGUAS	17	\N	2021-02-11 09:28:53	NODO OUTDOOR
2409	CIUDAD BAHIA	28	\N	2021-02-11 09:28:53	NODO OUTDOOR
2113	CIUDAD BOLIVAR I	22	\N	2021-02-11 09:28:53	NODO OUTDOOR
984	CIUDAD CHAVEZ I	11	\N	2021-02-11 09:28:53	NODO OUTDOOR
1421	CIUDAD FEDERACION	15	\N	2021-02-11 09:28:53	NODO OUTDOOR
1310	CIUDAD JARDIN I	14	\N	2021-02-11 09:28:53	NODO OUTDOOR
987	CIUDAD PLAZA II	11	\N	2021-02-11 09:28:53	NODO OUTDOOR
440	CIUDAD TIUNA CHINA III	5	\N	2021-02-11 09:28:53	NODO OUTDOOR
1209	CIUDADELA	13	\N	2021-02-11 09:28:53	NODO OUTDOOR
2117	COLINA DE BELLO MONTE	22	\N	2021-02-11 09:28:53	NODO OUTDOOR
255	COLINAS DEL LLANO 01	3	\N	2021-02-11 09:28:53	NODO OUTDOOR
27	COLINAS DEL PARAISO	1	\N	2021-02-11 09:28:53	NODO OUTDOOR
1133	COMPLEJO IRANI 1	12	\N	2021-02-11 09:28:53	NODO OUTDOOR
1314	CORINSA I	14	\N	2021-02-11 09:28:53	NODO OUTDOOR
1922	COROMOTO	20	\N	2021-02-11 09:28:53	NODO OUTDOOR
2120	COSTA MALL	22	\N	2021-02-11 09:28:54	NODO OUTDOOR
2309	COTOPERIZ II	27	\N	2021-02-11 09:28:54	NODO OUTDOOR
338	CUATRO ESQUINAS II	4	\N	2021-02-11 09:28:54	NODO OUTDOOR
1631	CUJI CENTRO	17	\N	2021-02-11 09:28:54	NODO OUTDOOR
33	CUMBRES ANDINAS	1	\N	2021-02-11 09:28:54	NODO OUTDOOR
1429	DABAJURO	15	\N	2021-02-11 09:28:54	NODO OUTDOOR
2124	DELICIAS CABIMAS	22	\N	2021-02-11 09:28:54	NODO OUTDOOR
447	EL ALMENDRON	5	\N	2021-02-11 09:28:54	NODO OUTDOOR
1139	EL CACAO	12	\N	2021-02-11 09:28:54	NODO OUTDOOR
594	EL CAFE NA-0-012	6	\N	2021-02-11 09:28:54	NODO OUTDOOR
2515	BRASIL SUR	29	\N	2021-03-03 11:24:56	NODO OUTDOOR
936	BUCARE II	10	\N	2021-03-04 10:44:21	CENTRAL
946	COLINAS DEL PUENTE	10	\N	2021-03-04 11:18:26	OPSUT
779	CIUDAD CARIBIA I NA10-125	5	\N	2021-03-12 13:14:00	NODO OUTDOOR
776	CARIBE	5	\N	2021-03-12 13:18:57	NODO OUTDOOR
819	CARTANAL II	6	\N	2021-03-12 13:37:38	NODO OUTDOOR
825	CAUCAGÜITA II	6	\N	2021-03-12 13:43:19	NODO OUTDOOR
833	CIUDAD MIRANDA I	6	\N	2021-03-12 13:55:08	NODO OUTDOOR
836	CIUDAD MIRANDA IV	6	\N	2021-03-12 13:55:56	NODO OUTDOOR
1970	BARRIO BLANCO	30	\N	2021-03-16 14:54:53	NODO OUTDOOR
1973	BARRIO OBRERO II	30	\N	2021-03-16 14:56:35	NODO OUTDOOR
1977	BICENTENARIO	30	\N	2021-03-16 14:58:12	NODO OUTDOOR
1986	CASSIANO LOZADA II	30	\N	2021-03-16 15:07:37	NODO OUTDOOR
1991	CHAMARRETA I	30	\N	2021-03-16 15:09:49	NODO OUTDOOR
1994	CHIQUINQUIRA	30	\N	2021-03-16 15:12:32	NODO OUTDOOR
1999	COSTA DE ROSMINI	30	\N	2021-03-16 15:14:51	NODO OUTDOOR
1916	CERRO AUTANA II	22	\N	2021-03-18 09:53:13	NODO OUTDOOR
992	EL CAMBUR	11	\N	2021-02-11 09:28:54	NODO OUTDOOR
1317	EL CARMEN (TOCORON II)	14	\N	2021-02-11 09:28:54	NODO OUTDOOR
1637	EL COREANO	17	\N	2021-02-11 09:28:54	NODO OUTDOOR
2128	EL DANTO II	22	\N	2021-02-11 09:28:54	NODO OUTDOOR
599	EL ENCANTADO NA11-029	6	\N	2021-02-11 09:28:54	NODO OUTDOOR
2422	EL FRANCES	28	\N	2021-02-11 09:28:54	NODO OUTDOOR
163	EL GALLO	2	\N	2021-02-11 09:28:54	NODO OUTDOOR
42	EL GAMERO	1	\N	2021-02-11 09:28:54	NODO OUTDOOR
2131	EL JABILLO	22	\N	2021-02-11 09:28:54	NODO OUTDOOR
454	EL LIMON NA09-008	5	\N	2021-02-11 09:28:54	NODO OUTDOOR
2312	EL MANGLILLO	27	\N	2021-02-11 09:28:54	NODO OUTDOOR
1640	EL MANZANO I	17	\N	2021-02-11 09:28:54	NODO OUTDOOR
2134	EL MENITO II	22	\N	2021-02-11 09:28:54	NODO OUTDOOR
995	EL MOLINO NDA	11	\N	2021-02-11 09:28:54	NODO OUTDOOR
1140	EL PAO	12	\N	2021-02-11 09:28:54	SUPERAULA
47	EL PIÑAL I	1	\N	2021-02-11 09:28:54	NODO OUTDOOR
1219	EL PUEBLO CAMATAGUA I	13	\N	2021-02-11 09:28:54	NODO OUTDOOR
604	EL PUENTE	6	\N	2021-02-11 09:28:54	NODO OUTDOOR
999	EL ROBLE LOS GUAYOS	11	\N	2021-02-11 09:28:54	NODO OUTDOOR
942	EL SAMAN II	10	\N	2021-02-11 09:28:54	NODO OUTDOOR
2138	EL SOL	22	\N	2021-02-11 09:28:54	NODO OUTDOOR
2141	EL SOLER III	22	\N	2021-02-11 09:28:54	NODO OUTDOOR
1832	EL TIAMO COUNTRY CLUB	19	\N	2021-02-11 09:28:54	NODO OUTDOOR
1650	EL TOSTAO	17	\N	2021-02-11 09:28:54	NODO OUTDOOR
52	EL VALLE II	1	\N	2021-02-11 09:28:54	NODO OUTDOOR
608	FORTIN NUEVA CASARAPA	6	\N	2021-02-11 09:28:54	NODO OUTDOOR
460	FUERTE TIUNA GUARNICION	5	\N	2021-02-11 09:28:54	NODO OUTDOOR
2146	FUNDABARRIOS I	22	\N	2021-02-11 09:28:54	NODO OUTDOOR
1004	FUNDACION CAP	11	\N	2021-02-11 09:28:54	NODO OUTDOOR
1836	GRAN SABANA	19	\N	2021-02-11 09:28:54	NODO OUTDOOR
169	GRANADOS	2	\N	2021-02-11 09:28:54	NODO OUTDOOR
56	GUASDUALITO I	1	\N	2021-02-11 09:28:54	NODO OUTDOOR
1012	GUIGUE II	11	\N	2021-02-11 09:28:54	NODO OUTDOOR
59	HATO DE LA VIRGEN	1	\N	2021-02-11 09:28:54	NODO OUTDOOR
348	HUGO CHAVEZ FRIAS	4	\N	2021-02-11 09:28:54	NODO OUTDOOR
1771	HUGO CHAVEZ FRIAS I	18	\N	2021-02-11 09:28:54	NODO OUTDOOR
1657	INDIO MANAURE	17	\N	2021-02-11 09:28:54	NODO OUTDOOR
1840	INES ROMERO II	19	\N	2021-02-11 09:28:54	NODO OUTDOOR
1535	IRANI III	16	\N	2021-02-11 09:28:54	NODO OUTDOOR
2237	JARDINES	26	\N	2021-02-11 09:28:54	NODO OUTDOOR
1326	JARDINES DE TURAGUA	14	\N	2021-02-11 09:28:54	NODO OUTDOOR
465	JOSE GREGORIO HERNANDEZ	5	\N	2021-02-11 09:28:54	NODO OUTDOOR
265	JUAN PABLO II	3	\N	2021-02-11 09:28:54	NODO OUTDOOR
1776	KM LA CERO	18	\N	2021-02-11 09:28:54	NODO OUTDOOR
1145	LA AGUADITA	12	\N	2021-02-11 09:28:54	NODO OUTDOOR
1661	LA BATALLA I	17	\N	2021-02-11 09:28:54	NODO OUTDOOR
354	LA BLANCA II	4	\N	2021-02-11 09:28:54	NODO OUTDOOR
1440	LA CANDELARIA	15	\N	2021-02-11 09:28:54	NODO OUTDOOR
1927	LA CANDELARIA II	20	\N	2021-02-11 09:28:54	NODO OUTDOOR
266	LA CARAMUCA	3	\N	2021-02-11 09:28:54	NODO OUTDOOR
469	LA CORTADA DE CATIA	5	\N	2021-02-11 09:28:54	NODO OUTDOOR
2326	LA CRUZ DEL PASTEL	27	\N	2021-02-11 09:28:54	NODO OUTDOOR
1330	LA CURIA	14	\N	2021-02-11 09:28:54	NODO OUTDOOR
1777	LA ENSENADA	18	\N	2021-02-11 09:28:54	NODO OUTDOOR
1016	LA ESTANCIA	11	\N	2021-02-11 09:28:54	NODO OUTDOOR
68	LA GRITA III	1	\N	2021-02-11 09:28:54	NODO OUTDOOR
623	LA LAGUNITA # 10 EL HATILLO	6	\N	2021-02-11 09:28:54	NODO OUTDOOR
626	LA LAGUNITA # 9 EL HATILLO	6	\N	2021-02-11 09:28:54	NODO OUTDOOR
472	LA LLANADA	5	\N	2021-02-11 09:28:54	NODO OUTDOOR
1669	LA LUCHA	17	\N	2021-02-11 09:28:54	NODO OUTDOOR
1147	LA MAPORA	12	\N	2021-02-11 09:28:54	NODO OUTDOOR
1539	LA MONTAÑUELA	16	\N	2021-02-11 09:28:54	NODO OUTDOOR
180	LA MURALLA I	2	\N	2021-02-11 09:28:54	NODO OUTDOOR
73	LA QUIRACHA	1	\N	2021-02-11 09:28:54	NODO OUTDOOR
1933	LA REFORMA	20	\N	2021-02-11 09:28:54	NODO OUTDOOR
1675	LA SABILA I	17	\N	2021-02-11 09:28:54	NODO OUTDOOR
1678	LA SABILA IV	17	\N	2021-02-11 09:28:54	NODO OUTDOOR
362	LA SERRANIA	4	\N	2021-02-11 09:28:54	NODO OUTDOOR
2249	LA TOSCANA	26	\N	2021-02-11 09:28:54	NODO OUTDOOR
2151	LA VACA	22	\N	2021-02-11 09:28:55	NODO OUTDOOR
1023	LA VICTORIA NA10-102	11	\N	2021-02-11 09:28:55	NODO OUTDOOR
2250	LAGUNA PARAISO	26	\N	2021-02-11 09:28:55	NODO OUTDOOR
1680	LARA PALACE	17	\N	2021-02-11 09:28:55	NODO OUTDOOR
1845	LAS BERMUDEZ	19	\N	2021-02-11 09:28:55	NODO OUTDOOR
479	LAS BRISAS	5	\N	2021-02-11 09:28:55	NODO OUTDOOR
632	LAS CAROLINAS	6	\N	2021-02-11 09:28:55	NODO OUTDOOR
1683	LAS CASITAS DEL CUJI II	17	\N	2021-02-11 09:28:55	NODO OUTDOOR
1340	LAS ESMERALDAS	14	\N	2021-02-11 09:28:55	NODO OUTDOOR
1447	LAS MALVINAS	15	\N	2021-02-11 09:28:55	NODO OUTDOOR
635	LAS MERCEDES # 14 ORIPOTO	6	\N	2021-02-11 09:28:55	NODO OUTDOOR
79	LAS MESAS II	1	\N	2021-02-11 09:28:55	NODO OUTDOOR
1029	LAS PALMITAS	11	\N	2021-02-11 09:28:55	NODO OUTDOOR
482	LAS TORRES	5	\N	2021-02-11 09:28:55	NODO OUTDOOR
1451	LAS VELITAS 2 SUR	15	\N	2021-02-11 09:28:55	NODO OUTDOOR
1032	LEONARDO CHIRINOS II	11	\N	2021-02-11 09:28:55	NODO OUTDOOR
273	LINDA BARINAS	3	\N	2021-02-11 09:28:55	NODO OUTDOOR
2551	HATO ROMAR	29	\N	2021-03-03 11:17:11	NODO OUTDOOR
2548	GUIRIA DE LA PLAYA	29	\N	2021-03-03 11:26:52	NODO OUTDOOR
1322	EL LIMON	14	\N	2021-03-08 10:14:40	CENTRAL
1519	CAMPO ELIAS GTI	16	\N	2021-03-08 10:29:24	CENTRAL
783	EL JUNQUITO KILOMETRO 7	5	\N	2021-03-12 13:21:17	NODO OUTDOOR
846	EL JOBITO	6	\N	2021-03-12 14:07:34	NODO OUTDOOR
848	JARDINES DE BETANIA	6	\N	2021-03-12 14:11:01	NODO OUTDOOR
850	LA MATA	6	\N	2021-03-12 14:13:44	NODO OUTDOOR
2004	EL CAUJARO II	30	\N	2021-03-16 15:17:51	NODO OUTDOOR
2012	JOSE FELIX RIBAS	30	\N	2021-03-16 15:22:13	NODO OUTDOOR
2018	LA GUAJIRITA	30	\N	2021-03-16 15:24:10	NODO OUTDOOR
2231	EL ZAMURO	34	\N	2021-03-22 11:52:40	NODO OUTDOOR
1035	LISANDRO ALVARADO	11	\N	2021-02-11 09:28:55	NODO OUTDOOR
934	LLANO FRESCO	10	\N	2021-02-11 09:28:55	NODO OUTDOOR
83	LOBATERA	1	\N	2021-02-11 09:28:55	NODO OUTDOOR
1689	LOS ARANGUES	17	\N	2021-02-11 09:28:55	NODO OUTDOOR
2256	LOS GIRASOLES I	26	\N	2021-02-11 09:28:55	NODO OUTDOOR
367	LOS GUAIMAROS	4	\N	2021-02-11 09:28:55	NODO OUTDOOR
1039	LOS GUAYOS	11	\N	2021-02-11 09:28:55	NODO OUTDOOR
1344	LOS HORNOS	14	\N	2021-02-11 09:28:55	NODO OUTDOOR
2259	LOS IRANIES	26	\N	2021-02-11 09:28:55	NODO OUTDOOR
1041	LOS MANGOS	11	\N	2021-02-11 09:28:55	NODO OUTDOOR
487	LOS MANGOS LA VEGA	5	\N	2021-02-11 09:28:55	NODO OUTDOOR
644	LOS PORTALES	6	\N	2021-02-11 09:28:55	NODO OUTDOOR
1150	LOS PROCERES	12	\N	2021-02-11 09:28:55	NODO OUTDOOR
1693	LOS QUEMADOS	17	\N	2021-02-11 09:28:55	NODO OUTDOOR
188	LOS RIOS	2	\N	2021-02-11 09:28:55	NODO OUTDOOR
1456	LOS ROSALES	15	\N	2021-02-11 09:28:55	NODO OUTDOOR
1348	LOS TAMARINDOS	14	\N	2021-02-11 09:28:55	NODO OUTDOOR
1853	LUCHADORES	19	\N	2021-02-11 09:28:55	NODO OUTDOOR
2162	MA VIEJA	22	\N	2021-02-11 09:28:55	NODO OUTDOOR
88	MANUEL FELIPE RUGELES	1	\N	2021-02-11 09:28:55	NODO OUTDOOR
495	MARIA TERESA TORO	5	\N	2021-02-11 09:28:55	NODO OUTDOOR
2449	MARINA DEL REY	28	\N	2021-02-11 09:28:55	NODO OUTDOOR
1154	MATA ABDO	12	\N	2021-02-11 09:28:55	NODO OUTDOOR
1857	MAURAK	19	\N	2021-02-11 09:28:55	NODO OUTDOOR
945	MERECURE	10	\N	2021-02-11 09:28:55	NODO OUTDOOR
651	MESUCA	6	\N	2021-02-11 09:28:55	NODO OUTDOOR
1242	MISION ARRIBA	13	\N	2021-02-11 09:28:55	NODO OUTDOOR
1465	MONSEÑOR ITURRIZA	15	\N	2021-02-11 09:28:55	NODO OUTDOOR
1546	MONSEÑOR UNDA	16	\N	2021-02-11 09:28:55	NODO OUTDOOR
1698	MOYETONES I	17	\N	2021-02-11 09:28:55	NODO OUTDOOR
1357	MUCURA II	14	\N	2021-02-11 09:28:55	NODO OUTDOOR
1859	NEGRA HIPOLITA	19	\N	2021-02-11 09:28:55	NODO OUTDOOR
655	NEREIDAS NA11-084	6	\N	2021-02-11 09:28:55	NODO OUTDOOR
198	NUEVA BOLIVIA I	2	\N	2021-02-11 09:28:55	NODO OUTDOOR
2170	NUEVA VENEZUELA	22	\N	2021-02-11 09:28:55	NODO OUTDOOR
500	NUEVO HORIZONTE	5	\N	2021-02-11 09:28:55	NODO OUTDOOR
658	OASIS SUITE	6	\N	2021-02-11 09:28:55	NODO OUTDOOR
1248	PADRE CHACIN	13	\N	2021-02-11 09:28:55	NODO OUTDOOR
1058	PALMA SOLA COUNTRY CLUB	11	\N	2021-02-11 09:28:55	NODO OUTDOOR
95	PALO GORDO LUNA MAR	1	\N	2021-02-11 09:28:55	NODO OUTDOOR
508	PARACOTOS II	5	\N	2021-02-11 09:28:55	NODO OUTDOOR
1943	PARIA GRANDE	20	\N	2021-02-11 09:28:55	NODO OUTDOOR
2459	PARIAGUAN 01	28	\N	2021-02-11 09:28:55	NODO OUTDOOR
381	PARQUE CHAMA	4	\N	2021-02-11 09:28:55	NODO OUTDOOR
1703	PAVIA ARRIBA	17	\N	2021-02-11 09:28:55	NODO OUTDOOR
925	PAYARITA	10	\N	2021-02-11 09:28:55	OPSUT
2338	PEDRO LUIS BRICEÑO	27	\N	2021-02-11 09:28:55	NODO OUTDOOR
99	PERIBECA	1	\N	2021-02-11 09:28:55	NODO OUTDOOR
1862	PIACOA	19	\N	2021-02-11 09:28:55	NODO OUTDOOR
766	PLAZA JARDIN	7	\N	2021-02-11 09:28:55	NODO OUTDOOR
1865	PORTAL DE LA ESPERANZA	19	\N	2021-02-11 09:28:55	NODO OUTDOOR
1706	POTRERITO DE MANZANO	17	\N	2021-02-11 09:28:55	NODO OUTDOOR
1362	PRADO LA ENCRUCIJADA	14	\N	2021-02-11 09:28:55	NODO OUTDOOR
1554	PRADOS DEL SOL MERCANTIL	16	\N	2021-02-11 09:28:55	NODO OUTDOOR
203	PRIMERA SABANA	2	\N	2021-02-11 09:28:55	NODO OUTDOOR
1709	PRIMERO DE MAYO	17	\N	2021-02-11 09:28:55	NODO OUTDOOR
1948	PROCERES II	20	\N	2021-02-11 09:28:55	NODO OUTDOOR
514	PUERTA NEGRA	5	\N	2021-02-11 09:28:55	NODO OUTDOOR
1475	PUERTO CUMAREBO 03	15	\N	2021-02-11 09:28:55	NODO OUTDOOR
282	PUNTA GORDA I	3	\N	2021-02-11 09:28:55	NODO OUTDOOR
284	QUEBRADA SECA	3	\N	2021-02-11 09:28:55	NODO OUTDOOR
2178	RANCHO BELLO	22	\N	2021-02-11 09:28:55	NODO OUTDOOR
2473	RIBERA GUAICA	28	\N	2021-02-11 09:28:55	NODO OUTDOOR
286	ROMULO GALLEGOS	3	\N	2021-02-11 09:28:55	NODO OUTDOOR
673	ROSA MISTICA	6	\N	2021-02-11 09:28:55	NODO OUTDOOR
2346	SABANA DE GUACUCO	27	\N	2021-02-11 09:28:55	NODO OUTDOOR
208	SABANA DE MENDOZA II	2	\N	2021-02-11 09:28:56	NODO OUTDOOR
1257	SAN CASIMIRO I	13	\N	2021-02-11 09:28:56	NODO OUTDOOR
1069	SAN JOAQUIN I NDA	11	\N	2021-02-11 09:28:56	NODO OUTDOOR
1874	SAN JOSE DE CACAGUAL	19	\N	2021-02-11 09:28:56	NODO OUTDOOR
1721	SAN JOSE DE POTRERITO	17	\N	2021-02-11 09:28:56	NODO OUTDOOR
2183	SAN JUAN	22	\N	2021-02-11 09:28:56	NODO OUTDOOR
388	SAN JUAN DE LAGUNILLAS	4	\N	2021-02-11 09:28:56	NODO OUTDOOR
214	SAN RAFAEL DE PAMPAN I	2	\N	2021-02-11 09:28:56	NODO OUTDOOR
1162	SAN RAMON	12	\N	2021-02-11 09:28:56	NODO OUTDOOR
1268	SAN SEBASTIAN I	13	\N	2021-02-11 09:28:56	NODO OUTDOOR
1484	SANTA ANA DE FALCON	15	\N	2021-02-11 09:28:56	NODO OUTDOOR
294	SANTA BARBARA DE BARINAS I	3	\N	2021-02-11 09:28:56	NODO OUTDOOR
678	SANTA BARBARA II	6	\N	2021-02-11 09:28:56	NODO OUTDOOR
1072	SANTA EDUVIGES	11	\N	2021-02-11 09:28:56	NODO OUTDOOR
948	SANTA JUANA	10	\N	2021-02-11 09:28:56	NODO OUTDOOR
396	SENIAT	4	\N	2021-02-11 09:28:56	NODO OUTDOOR
1879	SIMON BOLIVAR	19	\N	2021-02-11 09:28:56	NODO OUTDOOR
2567	PUERTO DE LA MADERA	29	\N	2021-03-03 11:23:21	NODO OUTDOOR
2557	LOMAS DE AYACUCHO	29	\N	2021-03-03 11:27:18	NODO OUTDOOR
1064	RAFAEL URDANETA	11	\N	2021-03-04 15:10:57	CENTRAL
799	MAMO BAJO NA10-129	5	\N	2021-03-12 13:13:34	NODO OUTDOOR
789	MACARAO I NA11-065	5	\N	2021-03-12 13:16:56	NODO OUTDOOR
2030	LO DE DORIA	30	\N	2021-03-16 15:29:05	NODO OUTDOOR
2033	LOS MODINES	30	\N	2021-03-16 15:31:38	NODO OUTDOOR
2037	LUIS ANGEL GARCIA	30	\N	2021-03-16 15:33:27	NODO OUTDOOR
2042	OASIS COUNTRY	30	\N	2021-03-16 15:34:35	NODO OUTDOOR
2047	PATRIA BOLIVARIANA	30	\N	2021-03-16 15:36:07	NODO OUTDOOR
2052	REY DE REYES	30	\N	2021-03-16 15:37:22	NODO OUTDOOR
2062	SAN RAFAEL DEL MOJAN I	30	\N	2021-03-16 15:41:38	NODO OUTDOOR
2069	SOL AMADO IV	30	\N	2021-03-16 15:45:21	NODO OUTDOOR
1732	TAMACA	17	\N	2021-02-11 09:28:56	NODO OUTDOOR
303	TAVACARE 2	3	\N	2021-02-11 09:28:56	NODO OUTDOOR
306	TAVACARE 5	3	\N	2021-02-11 09:28:56	NODO OUTDOOR
1077	TAZAJAL II	11	\N	2021-02-11 09:28:56	NODO OUTDOOR
528	TELARES LOS ANDES	5	\N	2021-02-11 09:28:56	NODO OUTDOOR
1375	TERRAZAS DE PAYA	14	\N	2021-02-11 09:28:56	NODO OUTDOOR
1278	TERRAZAS DE SANTA INES	13	\N	2021-02-11 09:28:56	NODO OUTDOOR
309	TERRAZAS DE SANTO DOMINGO 1	3	\N	2021-02-11 09:28:56	NODO OUTDOOR
530	TERRAZAS DEL ALBA I NA10-1	5	\N	2021-02-11 09:28:56	NODO OUTDOOR
312	TERRAZAS DEL CAIPE	3	\N	2021-02-11 09:28:56	NODO OUTDOOR
1735	TINTORERO	17	\N	2021-02-11 09:28:56	OPSUT
1080	TIZIANA VILLA	11	\N	2021-02-11 09:28:56	NODO OUTDOOR
1491	TOMAS MARZAL (INDEPENDENCIA	15	\N	2021-02-11 09:28:56	NODO OUTDOOR
1567	TRINA MORENO	16	\N	2021-02-11 09:28:56	NODO OUTDOOR
1882	TRIUNFO	19	\N	2021-02-11 09:28:56	NODO OUTDOOR
128	TUCAPE BAJO	1	\N	2021-02-11 09:28:56	NODO OUTDOOR
1086	U610 EL RINCON VALENCIA	11	\N	2021-02-11 09:28:56	NODO OUTDOOR
1493	UNIVERSITARIO II	15	\N	2021-02-11 09:28:56	NODO OUTDOOR
687	URB CASTILLEJO	6	\N	2021-02-11 09:28:56	NODO OUTDOOR
689	URB COLINAS HATILLO # 19	6	\N	2021-02-11 09:28:56	NODO OUTDOOR
692	URB LOMA LARGA # 15 HATILLO	6	\N	2021-02-11 09:28:56	NODO OUTDOOR
315	URB PRADOS DE BARINAS	3	\N	2021-02-11 09:28:56	NODO OUTDOOR
1890	URB TERRAZAS DEL ATLANTICO	19	\N	2021-02-11 09:28:56	NODO OUTDOOR
2190	URB VILLA CHINITA SAN FCO	22	\N	2021-02-11 09:28:56	NODO OUTDOOR
2499	VALLE DE GUANAPE	28	\N	2021-02-11 09:28:56	NODO OUTDOOR
2294	VALLE DE LUNA	26	\N	2021-02-11 09:28:56	NODO OUTDOOR
1092	VALLE DE ORO	11	\N	2021-02-11 09:28:56	NODO OUTDOOR
1740	VALLE LINDO II	17	\N	2021-02-11 09:28:56	NODO OUTDOOR
1573	VENCEDORES DE ARAURE	16	\N	2021-02-11 09:28:56	NODO OUTDOOR
1744	VILLA CREPUSCULAR I	17	\N	2021-02-11 09:28:56	NODO OUTDOOR
1576	VILLA DEL PILAR I	16	\N	2021-02-11 09:28:56	NODO OUTDOOR
1498	VILLA MARINA	15	\N	2021-02-11 09:28:56	NODO OUTDOOR
2195	VILLA PARAISO	22	\N	2021-02-11 09:28:56	NODO OUTDOOR
2198	VILLA SUR II	22	\N	2021-02-11 09:28:56	NODO OUTDOOR
537	VILLA ZOILA	5	\N	2021-02-11 09:28:56	NODO OUTDOOR
696	VILLAS DEL ESTE	6	\N	2021-02-11 09:28:56	NODO OUTDOOR
1746	VILLAS DEL OESTE	17	\N	2021-02-11 09:28:56	NODO OUTDOOR
1579	VIRGEN DE COROMOTO	16	\N	2021-02-11 09:28:56	NODO OUTDOOR
1894	VIRGEN DEL VALLE	19	\N	2021-02-11 09:28:56	NODO OUTDOOR
699	VISTA REAL	6	\N	2021-02-11 09:28:56	NODO OUTDOOR
1753	YUCATAN II	17	\N	2021-02-11 09:28:56	NODO OUTDOOR
1902	ZONA IND 25 DE MARZO I	19	\N	2021-02-11 09:28:56	NODO OUTDOOR
430	CC LITORAL MAIQUETIA	5	\N	2021-02-11 09:28:56	OAC
1006	GUACARA	11	\N	2021-02-11 09:28:56	SUPERAULA
1477	PUNTO FIJO	15	\N	2021-02-11 09:28:56	SUPERAULA
229	VALERA	2	\N	2021-02-11 09:28:56	SUPERAULA
1180	CALABOZO	13	\N	2021-02-11 09:28:56	OPSUT
1211	COROZO MATAFRAILES	13	\N	2021-02-11 09:28:56	OPSUT
600	EL GUACUCO	6	\N	2021-02-11 09:28:56	OPSUT
1646	EL PORVENIR PT	17	\N	2021-02-11 09:28:56	OPSUT
1436	EL TURAL	15	\N	2021-02-11 09:28:56	OPSUT
909	GUAYABAL PT	10	\N	2021-02-11 09:28:56	OPSUT
2448	MAPIRE CASETA	28	\N	2021-02-11 09:28:57	OPSUT
1551	PEÑA BLANCA	16	\N	2021-02-11 09:28:57	OPSUT
112	SAN CRISTOBAL	1	\N	2021-02-11 09:28:57	OPSUT
1270	SAN SEBASTIAN PB	13	\N	2021-02-11 09:28:57	OPSUT
1729	SARARE PB	17	\N	2021-02-11 09:28:57	OPSUT
219	SIQUISAY	2	\N	2021-02-11 09:28:57	OPSUT
1181	CALABOZO EB ANTONIO ESTEVEZ	13	\N	2021-02-11 09:28:57	SUPERAULA
1827	EL MANTECO	19	\N	2021-02-11 09:28:57	SUPERAULA
613	GUARENAS UE LAUDELINO MEJIAS	6	\N	2021-02-11 09:28:57	SUPERAULA
1445	LAS CALDERAS	15	\N	2021-02-11 09:28:57	SUPERAULA
1952	PUERTO AYACUCHO MARAHUACA	20	\N	2021-02-11 09:28:57	SUPERAULA
387	SAN CARLOS DEL ZULIA	4	\N	2021-02-11 09:28:57	SUPERAULA
1370	SAN MATEO UNIDAD EDUC LA CURIA	14	\N	2021-02-11 09:28:57	SUPERAULA
2667	EL CABRITO	11	2021-03-01 12:46:07	2021-03-01 12:49:26	NODO OUTDOOR
2522	CERRO CAMPEARE	29	\N	2021-03-01 12:54:09	CENTRAL
2663	CAMATAGAUA	14	2021-02-25 11:50:37	2021-03-03 10:27:35	ESTACION TERRENAL
106	LOS COLORADOS	1	\N	2021-03-03 10:39:34	CENTRAL
1253	REPETIDORA SANTA RITA	13	\N	2021-03-03 10:42:55	CENTRAL
2670	ARAYA	29	2021-03-03 11:03:22	2021-03-03 11:03:22	NODO OUTDOOR
2671	CARIACO	29	2021-03-03 11:04:24	2021-03-03 11:04:24	NODO OUTDOOR
2672	CASANAY	29	2021-03-03 11:05:59	2021-03-03 11:05:59	NODO OUTDOOR
2673	CUMANACOA	29	2021-03-03 11:15:56	2021-03-03 11:15:56	NODO OUTDOOR
2528	CRISTOBAL COLON	29	\N	2021-03-03 11:17:40	NODO OUTDOOR
2674	EL YOCO	29	2021-03-03 11:18:01	2021-03-03 11:18:01	CENTRAL
2537	EL PEÑON	29	\N	2021-03-03 11:18:12	NODO OUTDOOR
2544	GRAN MARISCAL CUMANA	29	\N	2021-03-03 11:20:03	CENTRAL
2583	SOLEDAD DE CARIACO	29	\N	2021-03-03 11:22:14	NODO OUTDOOR
2675	GUACA	29	2021-03-03 11:23:46	2021-03-03 11:24:18	CENTRAL
2560	MANICUARE	29	\N	2021-03-03 11:28:21	NODO OUTDOOR
2676	MANICUARE	29	2021-03-03 11:28:47	2021-03-03 11:28:47	NODO OUTHOOR
2677	MARIGUITAR	29	2021-03-03 11:29:46	2021-03-03 11:29:46	NODO OUTDOOR
2678	ARAYA	29	2021-03-03 11:32:53	2021-03-03 11:32:53	SUPERAULA
2679	CUMANACOA	29	2021-03-03 11:34:13	2021-03-03 11:34:13	SUPERAULA
2681	CARIACO	29	2021-03-03 11:35:25	2021-03-03 11:35:25	SUPERAULA
923	BIRUACA EB HECTOR VIDAL B	10	\N	2021-03-04 10:43:54	OPSUT
2680	RIO CARIBE	29	2021-03-03 11:35:17	2021-03-08 11:19:13	TX
1713	QUIBOR	17	\N	2021-03-08 12:45:49	OPSUT
812	URB LOS CARMENES	5	\N	2021-03-12 12:58:41	NODO OUTDOOR
807	TACAGUA VIEJA I	5	\N	2021-03-12 13:10:51	NODO OUTDOOR
810	TARMA	5	\N	2021-03-12 13:26:04	NODO OUTDOOR
871	SOTILLO	6	\N	2021-03-12 14:39:17	NODO OUTDOOR
2076	URB CAMINO LA LAGUNITA	30	\N	2021-03-16 15:47:12	NODO OUTDOOR
2238	VELADERO	26	\N	2021-03-16 19:33:33	OPSUT
2668	SANTA ROSALIA	22	2021-03-01 12:48:12	2021-03-19 10:04:36	OPSUT
1950	PUERTO AYACUCHO	22	\N	2021-03-19 10:09:43	OAC
2570	RIO GRANDE CASANAY	29	\N	2021-03-03 11:36:11	NODO OUTDOOR
2682	RIO GRANDE CASANAY	29	2021-03-03 11:36:26	2021-03-03 11:36:26	CENTRAL
2683	CASANAY	29	2021-03-03 11:36:31	2021-03-03 11:36:31	SUPERAULA
2684	SAN ANTONIO DEL GOLFO	29	2021-03-03 11:37:37	2021-03-03 11:37:37	CENTRAL
2685	SAN JOSE DE AEROCUAR	29	2021-03-03 11:39:03	2021-03-03 11:39:03	CENTRAL
2686	SAN VICENTE	26	2021-03-03 11:40:46	2021-03-03 11:40:46	NODO OUTDOOR
2687	SANTA FE	29	2021-03-03 11:41:50	2021-03-03 11:41:50	NODO OUTDOOR
2688	TUNAPUY	29	2021-03-03 11:43:07	2021-03-03 11:43:07	CENTRAL
2689	YAGUARAPARO	29	2021-03-03 11:44:14	2021-03-03 11:44:14	NODO OUTDOOR
2690	EL PILAR	29	2021-03-03 11:44:55	2021-03-03 11:44:55	CENTRAL
2691	EL POBLADO	29	2021-03-03 11:45:04	2021-03-03 11:45:04	NODO OUTDOOR
2692	ARAGUA DE MATURIN	26	2021-03-03 11:47:12	2021-03-03 11:47:12	CENTRAL
2693	BARRANCAS DEL ORINOCO	26	2021-03-03 11:47:47	2021-03-03 11:47:47	CENTRAL
2209	BUENA VISTA	26	\N	2021-03-03 11:48:17	NODO OUTDOOR
2694	CAICARA DE MATURIN	26	2021-03-03 11:48:46	2021-03-03 11:48:46	CENTRAL
2695	CARIPE	26	2021-03-03 11:49:56	2021-03-03 11:49:56	NODO OUTDOOR
2696	CHAGUARAMAL	26	2021-03-03 13:16:52	2021-03-03 13:17:16	CENTRAL
2697	CHAGUARAMAS	26	2021-03-03 13:17:48	2021-03-03 13:17:48	CENTRAL
2698	EL FURRIAL	26	2021-03-03 13:20:22	2021-03-03 13:20:22	CENTRAL
2699	QUIRIQUIRE	26	2021-03-03 13:27:39	2021-03-03 13:27:39	CENTRAL
2700	SAN ANTONIO DE MATURIN	26	2021-03-03 13:28:24	2021-03-03 13:28:24	CENTRAL
2701	TERESEN	26	2021-03-03 13:30:02	2021-03-03 13:30:02	CENTRAL
2702	URACOA	26	2021-03-03 13:33:34	2021-03-03 13:33:34	NODO OUTDOOR
2703	SANTA RITA	26	2021-03-03 13:35:25	2021-03-03 13:35:25	CENTRAL
2704	URACOA	26	2021-03-03 13:36:16	2021-03-03 13:36:16	SUPER AULA
2276	SAN MIGUEL	26	\N	2021-03-03 13:42:04	NODO OUTDOOR
2233	FRANCISCO DE MIRANDA	26	\N	2021-03-03 13:46:53	NODO OUTDOOR
2655	TUCUPITA	34	2021-02-04 12:20:15	2021-03-03 14:35:05	OAC
2705	BRUZUAL I	3	2021-03-03 14:54:24	2021-03-03 14:54:24	CENTRAL
2706	BUM BUM	3	2021-03-03 14:54:55	2021-03-03 14:54:55	CENTRAL
2707	CALDERAS	3	2021-03-03 14:55:39	2021-03-03 14:55:39	CENTRAL
2708	CAPITANEJO	3	2021-03-03 14:56:10	2021-03-03 14:56:10	CENTRAL
2709	BOCONO DEL TACHIRA	4	2021-03-03 14:56:12	2021-03-03 14:56:12	CENTRAL
2710	CHAMETA	3	2021-03-03 14:56:55	2021-03-03 14:56:55	CENTRAL
2711	CANAGUA	4	2021-03-03 14:57:23	2021-03-03 14:57:23	CENTRAL
2712	EL REAL	3	2021-03-03 14:59:57	2021-03-03 14:59:57	CENTRAL
2713	CASIGUA EL CUBO	4	2021-03-03 15:00:54	2021-03-03 15:00:54	NODO OUTDOOR
2714	LAS VEGUITAS	3	2021-03-03 15:02:12	2021-03-03 15:02:12	CENTRAL
2715	LAS VEGUITAS	3	2021-03-03 15:02:36	2021-03-03 15:02:36	CENTRAL
2716	PEDRAZA LA VIEJA	3	2021-03-03 15:03:41	2021-03-03 15:03:41	CENTRAL
2717	PUEBLO EL LLANO	3	2021-03-03 15:04:03	2021-03-03 15:04:03	CENTRAL
2669	EL VIGIA	4	2021-03-02 09:04:06	2021-03-03 15:05:24	CENTRAL CRITICA
2718	CHIGUARA	4	2021-03-03 15:06:26	2021-03-03 15:06:26	CENTRAL
2719	LAS PIEDRAS	3	2021-03-03 15:07:26	2021-03-03 15:07:26	CENTRAL
2720	LIBERTAD	3	2021-03-03 15:08:14	2021-03-03 15:08:14	CENTRAL
2721	EL GUAYABO	4	2021-03-03 15:08:51	2021-03-03 15:08:51	CENTRAL
2722	STA BARBARA DE BARINAS	3	2021-03-03 15:09:33	2021-03-03 15:09:33	SUPERAULA
2723	ENCONTRADOS	4	2021-03-03 15:10:15	2021-03-03 15:10:15	CENTRAL
243	BARRANCAS	3	\N	2021-03-03 15:11:25	NODO OUTDOOR
2724	LA PLAYA	4	2021-03-03 15:13:23	2021-03-03 15:13:23	CENTRAL
292	SANTA BARBARA DE BARINAS III	3	\N	2021-03-03 15:13:43	NODO OUTDOOR
2725	LA TENDIDA	4	2021-03-03 15:14:21	2021-03-03 15:14:21	CENTRAL
2726	MIRI	3	2021-03-03 15:16:14	2021-03-03 15:16:14	NODO OUTDOOR
2727	LAGUNILLAS	4	2021-03-03 15:16:26	2021-03-03 15:16:26	CENTRAL
2728	LAS HERNANDEZ	4	2021-03-03 15:18:13	2021-03-03 15:18:13	CENTRAL
2729	ABEJALES	1	2021-03-03 15:21:06	2021-03-03 15:21:06	NODO OUTDOOR
2730	BOROTA	1	2021-03-03 15:21:37	2021-03-03 15:21:37	CENTRAL
2731	COLONCITO	1	2021-03-03 15:24:06	2021-03-03 15:24:06	CENTRAL CRITICA
2732	COLONCITO	1	2021-03-03 15:24:30	2021-03-03 15:24:30	OPSUT
2733	EL COBRE	1	2021-03-03 15:28:21	2021-03-03 15:28:21	CENTRAL
2734	EL COROZO	1	2021-03-03 15:28:52	2021-03-03 15:28:52	CENTRAL
2735	LA FRIA	1	2021-03-03 15:33:51	2021-03-03 15:33:51	CENTRAL CRITICA
2736	LA FRIA I	1	2021-03-03 15:34:15	2021-03-03 15:34:15	CENTRAL
2737	MESA BOLIVAR	4	2021-03-03 15:36:03	2021-03-03 15:36:03	CENTRAL
2738	LA PEDRERA	1	2021-03-03 15:36:41	2021-03-03 15:36:41	CENTRAL CRITICA
2739	LAS DELICIAS	1	2021-03-03 15:37:32	2021-03-03 15:37:32	NODO OUTDOOR
2740	MUCUCHIES	4	2021-03-03 15:37:52	2021-03-03 15:37:52	CENTRAL
2741	MUCUCHIES	4	2021-03-03 15:38:47	2021-03-03 15:38:47	NODO OUTDOOR
2742	MUCUJEPE	4	2021-03-03 15:39:45	2021-03-03 15:39:45	CENTRAL
2743	MUCURUBA	4	2021-03-03 15:40:28	2021-03-03 15:40:28	CENTRAL
2744	PREGONERO	1	2021-03-03 15:41:40	2021-03-03 15:41:40	NODO OUTDOOR
2745	PUEBLO NUEVO EL CHIVO	4	2021-03-03 15:42:10	2021-03-03 15:42:10	CENTRAL
2746	QUENIQUEA	1	2021-03-03 15:42:46	2021-03-03 15:42:46	CENTRAL
2747	REPETIDORA LOS COLORADOS	1	2021-03-03 15:43:13	2021-03-03 15:43:13	CENTRAL
2748	SAN JOSE DE BOLIVAR	1	2021-03-03 15:45:33	2021-03-03 15:45:33	CENTRAL
2749	TABAY	4	2021-03-03 15:46:09	2021-03-03 15:46:09	CENTRAL
2750	SAN JUAN DE COLON	1	2021-03-03 15:46:11	2021-03-03 15:46:11	CENTRAL CRITICA
2751	SEBORUCO	1	2021-03-03 15:46:35	2021-03-03 15:46:35	CENTRAL
2752	UMUQUENA	1	2021-03-03 15:47:05	2021-03-03 15:47:05	CENTRAL
2753	ZORCA	1	2021-03-03 15:47:31	2021-03-03 15:47:31	CENTRAL
2754	TUCANI	4	2021-03-03 15:47:33	2021-03-03 15:47:33	CENTRAL
2755	SANTA ANA	1	2021-03-03 15:50:26	2021-03-03 15:50:26	NODO OUTDOOR
2756	NODO OUTDOOR	4	2021-03-03 15:50:38	2021-03-03 15:50:38	CENTRAL
2757	NODO OUTDOOR	4	2021-03-03 15:50:39	2021-03-03 15:50:39	CENTRAL
2758	EL VIGIA	4	2021-03-03 15:52:38	2021-03-03 15:52:38	SUPERAULA
2759	LAGUNILLAS	4	2021-03-03 15:54:11	2021-03-03 15:54:11	SUPERAULA
2760	EL RUBIO	1	2021-03-03 15:55:05	2021-03-03 15:55:05	OPSUT
2761	APARTADEROS	4	2021-03-03 15:55:12	2021-03-03 15:55:12	SUPERAULA
2762	ABEJALES	1	2021-03-03 15:55:54	2021-03-03 15:55:54	NODO OUTDOOR
2763	EL ZUMBADOR VTV (NO CANTV)	1	2021-03-03 15:57:51	2021-03-03 15:57:51	NO CANTV
2764	EL CANTON	1	2021-03-03 15:58:25	2021-03-03 15:58:25	OPSUT
2765	LA FUNDACIÓN	1	2021-03-04 08:46:00	2021-03-04 08:46:00	NODO OUTDOOR
2766	19 DE ABRIL	1	2021-03-04 08:47:46	2021-03-04 08:47:46	NODO OUTDOOR
2767	PARAMILLO II	1	2021-03-04 08:51:34	2021-03-04 08:51:34	NODO OUTDOOR
2768	AGUA SANTA	2	2021-03-04 09:05:46	2021-03-04 09:05:46	CENTRAL
2436	JOSE GREGORIO HERNANDEZ	28	\N	2021-03-04 09:10:05	NODO OUTDOOR
2769	BOCA DE UCHIRE	28	2021-03-04 09:11:15	2021-03-04 09:11:15	CENTRAL
2770	ARAPUEY	2	2021-03-04 09:17:28	2021-03-04 09:17:28	CENTRAL
2771	EL TIGRITO	28	2021-03-04 09:20:52	2021-03-04 09:20:52	CENTRAL
2772	BOBURES	2	2021-03-04 09:21:00	2021-03-04 09:21:00	CENTRAL
2773	FARALLONES	28	2021-03-04 09:21:43	2021-03-04 09:21:43	CENTRAL
2774	BOCONO	2	2021-03-04 09:24:39	2021-03-04 09:24:39	CENTRAL
2775	GUANAPE	28	2021-03-04 09:25:59	2021-03-04 09:25:59	CENTRAL
2776	BURBUSAY	2	2021-03-04 09:26:31	2021-03-04 09:26:31	CENTRAL
2777	CARACHE	2	2021-03-04 09:27:17	2021-03-04 09:27:17	CENTRAL
2778	CHACHOPO	2	2021-03-04 09:29:42	2021-03-04 09:29:42	CENTRAL
2779	ONOTO	28	2021-03-04 09:29:49	2021-03-04 09:29:49	CENTRAL
2780	CHEJENDE	2	2021-03-04 09:30:28	2021-03-04 09:30:28	CENTRAL
2781	PARIAGUAN	28	2021-03-04 09:30:51	2021-03-04 09:30:51	CENTRAL
2782	CUICAS	2	2021-03-04 09:30:59	2021-03-04 09:30:59	CENTRAL
2783	EL BATEY	2	2021-03-04 09:31:28	2021-03-04 09:31:28	CENTRAL
2784	EL DIVIDIVE	2	2021-03-04 09:32:24	2021-03-04 09:32:24	CENTRAL
2785	SABANA DE UCHIRE	28	2021-03-04 09:33:15	2021-03-04 09:33:15	CENTRAL
2786	GIBRALTAR	2	2021-03-04 09:33:35	2021-03-04 09:33:35	CENTRAL
2787	SAN MATEO	28	2021-03-04 09:34:02	2021-03-04 09:34:02	CENTRAL
2788	SAN TOME	28	2021-03-04 09:34:32	2021-03-04 09:34:32	CENTRAL
2789	LA MESA DE ESNUJAQUE	2	2021-03-04 09:35:59	2021-03-04 09:35:59	CENTRAL
2790	URICA	28	2021-03-04 09:36:12	2021-03-04 09:36:12	CENTRAL
2791	LA PUERTA	2	2021-03-04 09:37:18	2021-03-04 09:37:18	CENTRAL
2792	LA QUEBRADA	2	2021-03-04 09:38:13	2021-03-04 09:38:13	CENTRAL
2793	BUENA VISTA	28	2021-03-04 09:38:14	2021-03-04 09:38:14	NODO OUTDOOR
2794	EL HATILLO	28	2021-03-04 09:38:51	2021-03-04 09:38:51	CENTRAL
2795	LAS VIRTUDES	2	2021-03-04 09:39:18	2021-03-04 09:39:18	CENTRAL
2796	EL SAMAN	28	2021-03-04 09:39:43	2021-03-04 09:39:43	CENTRAL
2797	MENDOZA FRIA	2	2021-03-04 09:39:47	2021-03-04 09:39:47	CENTRAL
2798	MITON	2	2021-03-04 09:40:38	2021-03-04 09:40:38	CENTRAL
2799	MONAY	2	2021-03-04 09:41:09	2021-03-04 09:41:09	CENTRAL
2800	MONTE CARMELO	2	2021-03-04 09:42:18	2021-03-04 09:42:18	CENTRAL
2801	SANTA ANA	28	2021-03-04 09:43:33	2021-03-04 09:43:33	NODO OUTDOOR
2802	SANTA ROSA	28	2021-03-04 09:45:32	2021-03-04 09:45:32	NODO OUTDOOR
2803	MOTATAN	2	2021-03-04 09:45:40	2021-03-04 09:45:40	CENTRAL
2804	PAMPAN	2	2021-03-04 09:49:27	2021-03-04 09:49:27	CENTRAL
2805	SAN LAZARO	2	2021-03-04 09:53:24	2021-03-04 09:53:24	CENTRAL
2806	SAN MIGUEL	2	2021-03-04 09:54:15	2021-03-04 09:54:15	CENTRAL
2807	TIMOTES	2	2021-03-04 09:55:40	2021-03-04 09:55:40	CENTRAL
2808	TOROCOCO	2	2021-03-04 09:56:12	2021-03-04 09:56:12	CENTRAL
228	URB EL ROSAL PAMPANITO	2	\N	2021-03-04 10:00:04	CENTRAL
2809	ALTAGRACIA	27	2021-03-04 10:00:29	2021-03-04 10:00:29	CENTRAL
2810	LA CEIBA	2	2021-03-04 10:00:40	2021-03-04 10:00:40	CENTRAL
2811	LIMONCITO	2	2021-03-04 10:00:57	2021-03-04 10:00:57	CENTRAL
2812	PALMARITO	2	2021-03-04 10:01:35	2021-03-04 10:01:35	CENTRAL
2813	SANTA ISABEL	2	2021-03-04 10:02:47	2021-03-04 10:02:47	CENTRAL
2490	SUBEDELCA LA CANOA (NO CANTV)	28	\N	2021-03-04 10:03:07	NO CANTV
2814	SABANA DE MENDOZA	2	2021-03-04 10:04:54	2021-03-04 10:04:54	CENTRAL CRITICA
2815	TRUJILLO	2	2021-03-04 10:05:35	2021-03-04 10:05:35	CENTRAL CRITICA
2816	CHACHOPO	2	2021-03-04 10:09:59	2021-03-04 10:09:59	NODO OUTDOOR
2817	BOCA DE POZO	27	2021-03-04 10:11:24	2021-03-04 10:11:24	NODO OUTDOOR
2818	SAN JOAQUIN	28	2021-03-04 10:14:06	2021-03-04 10:14:06	NODO OUTDOOR
2819	BOCA DE POZO	27	2021-03-04 10:14:44	2021-03-04 10:14:44	SUPERAULA
2438	LA FLORESTA	28	\N	2021-03-04 10:16:33	NODO OUTDOOR
2820	SANTA APOLONIA GTI	2	2021-03-04 10:16:49	2021-03-04 10:16:49	NODO OUTDOOR
2821	BAJO HONDO	28	2021-03-04 10:17:48	2021-03-04 10:17:48	NODO OUTDOOR
2822	SANTA ANA	2	2021-03-04 10:19:32	2021-03-04 10:19:32	NODO OUTDOOR
2823	EL ESPINAL	27	2021-03-04 10:21:19	2021-03-04 10:21:19	CENTRAL
2534	Y04 C CUMANA CENTRO BRISAS DEL GOLFO	29	\N	2021-03-04 10:21:37	DLC
2824	CARMEN DE CURA	13	2021-03-04 10:32:21	2021-03-04 10:32:21	CENTRAL
1265	SAN JUAN DE LOS MORROS I	13	\N	2021-03-04 10:33:40	CENTRAL
924	ACHAGUA ESC EL NAZARENO	10	\N	2021-03-04 10:36:44	CENTRAL CRITICA
1207	CHAGUARAMAS II	13	\N	2021-03-04 10:37:41	CENTRAL
2825	COROZO PANDO	13	2021-03-04 10:38:12	2021-03-04 10:38:12	CENTRAL
2826	EL MEREY	13	2021-03-04 10:38:52	2021-03-04 10:38:52	CENTRAL
2827	APURITO	10	2021-03-04 10:40:10	2021-03-04 10:40:10	CENTRAL
2828	APURITO	10	2021-03-04 10:40:39	2021-03-04 10:40:39	NODO OUTDOOR
2829	LA GUARDIA	27	2021-03-04 10:48:56	2021-03-04 10:48:56	CENTRAL
2830	EL PUEBLO CAMATAGUA	13	2021-03-04 10:49:47	2021-03-04 10:49:47	CENTRAL
2831	EL RASTRO	13	2021-03-04 10:50:28	2021-03-04 10:50:28	CENTRAL
2832	GUARDATINAJAS	13	2021-03-04 10:51:12	2021-03-04 10:51:12	CENTRAL
2833	LA GUARDIA	27	2021-03-04 10:52:12	2021-03-04 10:52:12	NODO OUTDOOR
2834	SAN JOSE DE GUARIBE	13	2021-03-04 10:55:21	2021-03-04 10:55:21	CENTRAL
2835	EL SOCORRO	13	2021-03-04 11:02:57	2021-03-04 11:02:57	CENTRAL
2836	ZARAZA I	13	2021-03-04 11:03:59	2021-03-04 11:03:59	CENTRAL
2837	CABRUTA	13	2021-03-04 11:05:02	2021-03-04 11:05:02	CENTRAL CRITICA
2838	EL SOMBRERO	13	2021-03-04 11:09:14	2021-03-04 11:09:14	CENTRAL CRITICA
2839	SAN CASIMIRO	13	2021-03-04 11:11:32	2021-03-04 11:11:32	CENTRAL CRITICA
2841	REPETIDORA SANTA RITA	13	2021-03-04 11:14:44	2021-03-04 11:14:44	CENTRAL CRITICA
2842	PUERTO FERMIN	27	2021-03-04 11:14:54	2021-03-04 11:14:54	CENTRAL
2843	TELEPUERTO SATELITAL BAMARI (NO CANTV)	13	2021-03-04 11:16:34	2021-03-04 11:16:34	CENTRAL CRITICA
2844	CAZORLA	10	2021-03-04 11:17:26	2021-03-04 11:17:26	CENTRAL
2845	EL RECREO	10	2021-03-04 11:21:33	2021-03-04 11:21:33	CENTRAL
2846	ELORZA	10	2021-03-04 11:23:01	2021-03-04 11:23:01	CENTRAL
2847	ELORZA	10	2021-03-04 11:23:29	2021-03-04 11:23:29	CENTRAL CRITICA
2848	GUAYABAL	10	2021-03-04 11:26:24	2021-03-04 11:26:24	CENTRAL CRITICA
2840	LAS MERCEDES DEL LLANO	13	2021-03-04 11:12:29	2021-03-04 11:27:13	OPSUT
2849	PUNTA DE PIEDRAS	27	2021-03-04 11:41:08	2021-03-04 11:41:08	CENTRAL
2850	PUNTA DE PIEDRAS	27	2021-03-04 11:41:49	2021-03-04 11:41:49	NODO OUTDOOR
1172	BAEMARI GTI	13	\N	2021-03-04 11:44:00	NODO OUTDOOR
2851	MANTECAL	10	2021-03-04 11:45:28	2021-03-04 11:45:28	OPSUT
2852	SAN JUAN BAUTISTA	27	2021-03-04 11:45:36	2021-03-04 11:45:36	CENTRAL
2853	SAN PEDRO DE COCHE	27	2021-03-04 11:48:49	2021-03-04 11:48:49	CENTRAL
915	PASO CAPANAPARO	10	\N	2021-03-04 11:49:54	NODO OUTDOOR
2854	PASO CINARUCO	10	2021-03-04 11:50:28	2021-03-04 11:50:28	NODO OUTDOOR
2855	PUERTO INFANTE	10	2021-03-04 11:51:35	2021-03-04 11:51:35	NODO OUTDOOR
2856	SANTA ANA	27	2021-03-04 11:51:49	2021-03-04 11:51:49	NODO OUTDOOR
2857	QUINTERO	10	2021-03-04 11:52:37	2021-03-04 11:52:37	NODO OUTDOOR
1122	APARTADEROS	12	\N	2021-03-04 14:07:11	NODO OUTDOOR
2858	ARISMENDI	12	2021-03-04 14:07:42	2021-03-04 14:07:42	CENTRAL
2859	EL BAUL	12	2021-03-04 14:11:18	2021-03-04 14:11:18	CENTRAL
2860	EL BAUL	12	2021-03-04 14:11:56	2021-03-04 14:11:56	NODO OUTDOOR
2861	EL PAO	12	2021-03-04 14:14:11	2021-03-04 14:14:11	NODO OUTDOOR
2862	EL PAO	12	2021-03-04 14:14:44	2021-03-04 14:14:44	CENTRAL
2863	LAS VEGAS	12	2021-03-04 14:16:22	2021-03-04 14:16:22	NODO OUTDOOR
2864	LIBERTAD	12	2021-03-04 14:17:56	2021-03-04 14:17:56	NODO OUTDOOR
2865	MACAPO	12	2021-03-04 14:18:43	2021-03-04 14:18:43	CENTRAL
2866	SAN CARLOS	12	2021-03-04 14:21:42	2021-03-04 14:21:42	NODO OUTDOOR
2867	SAN CARLOS	12	2021-03-04 14:22:00	2021-03-04 14:22:00	OAC
2868	TINACO	12	2021-03-04 14:24:31	2021-03-04 14:24:31	CENTRAL
1164	TINACO/TX CASETA DE SERV	12	\N	2021-03-04 14:25:35	NODO INDOOR
2869	TINAQUILLO	12	2021-03-04 14:26:03	2021-03-04 14:26:03	CENTRAL
2870	BEJUMA I	11	2021-03-04 14:33:45	2021-03-04 14:33:45	CENTRAL
2871	BELEN	11	2021-03-04 14:34:41	2021-03-04 14:34:41	NODO OUTDOOR
966	BUENAVENTURA	11	\N	2021-03-04 14:36:09	NODO OUTDOOR
2872	CANOABO	11	2021-03-04 14:37:59	2021-03-04 14:37:59	CENTRAL
2873	CANOABO I	11	2021-03-04 14:38:25	2021-03-04 14:38:25	CENTRAL
978	CC PASEO LAS INDUSTRIAS	11	\N	2021-03-04 14:40:06	CENTRAL
2874	CAGUA	14	2021-03-04 14:43:26	2021-03-04 14:43:26	CENTRAL
1303	CC LAS AMERICAS	14	\N	2021-03-04 14:44:51	CENTRAL
1003	FLOR AMARILLO	11	\N	2021-03-04 14:49:39	CENTRAL
2875	GUIGUE	11	2021-03-04 14:51:51	2021-03-04 14:51:51	CENTRAL
1018	LA FUNDACION	11	\N	2021-03-04 14:54:41	CENTRAL
2876	MONTALBAN	11	2021-03-04 15:04:42	2021-03-04 15:04:42	NODO OUTDOOR
2877	NEGRO PRIMERO	11	2021-03-04 15:06:32	2021-03-04 15:06:32	NODO OUTDOOR
1074	SUBLA ARENOSA (NO CANTV)	11	\N	2021-03-04 15:13:49	CENTRAL CRITICA
2878	CHORONI	14	2021-03-04 15:14:45	2021-03-04 15:14:45	NODO OUTDOOR
2879	COLONIA TOVAR	14	2021-03-04 15:18:41	2021-03-04 15:18:41	CENTRAL
2880	VILLA DE CURA	14	2021-03-04 15:36:04	2021-03-04 15:36:04	CENTRAL
1385	URL TEJERIAS I	14	\N	2021-03-04 15:38:32	CENTRAL
1371	SAN OMERO	14	\N	2021-03-04 15:47:07	NODO OUTDOOR
2881	SAN MATEO I	14	2021-03-04 15:47:51	2021-03-04 15:47:51	CENTRAL
2882	SAN MATEO II	14	2021-03-04 15:48:23	2021-03-04 15:48:23	CENTRAL
2883	OCUMARE DE LA COSTA	14	2021-03-04 15:50:59	2021-03-04 15:50:59	CENTRAL
2884	MAGDALENO	14	2021-03-04 15:53:36	2021-03-04 15:53:36	NODO OUTDOOR
2885	LOS TANQUES	14	2021-03-04 15:54:04	2021-03-04 15:54:04	CENTRAL
1339	LAS DELICIAS	14	\N	2021-03-04 15:56:46	CENTRAL
2886	EL CONSEJO I	14	2021-03-08 10:13:44	2021-03-08 10:13:44	CENTRAL
2887	AROA	18	2021-03-08 10:23:56	2021-03-08 10:23:56	CENTRAL
2888	BORAURE	18	2021-03-08 10:26:51	2021-03-08 10:26:51	CENTRAL
2889	CAMORUCO	11	2021-03-08 10:28:08	2021-03-08 10:28:08	CENTRAL
2890	CAMPO ELIAS	18	2021-03-08 10:29:53	2021-03-08 10:29:53	CENTRAL
2891	CAMPO ELIAS	18	2021-03-08 10:30:18	2021-03-08 10:30:18	NODO OUTDOOR
1768	EL GUAYABO	18	\N	2021-03-08 10:32:32	NODO OUTDOOR
2892	FARRIAR	18	2021-03-08 10:33:05	2021-03-08 10:33:05	CENTRAL
2893	GUAMA	18	2021-03-08 10:33:39	2021-03-08 10:33:39	CENTRAL
1780	LAS PIEDRAS	18	\N	2021-03-08 10:36:40	NODO OUTDOOR
2894	MARIN	18	2021-03-08 10:37:41	2021-03-08 10:37:41	CENTRAL
2895	SABANA DE PARRA	18	2021-03-08 10:38:54	2021-03-08 10:38:54	CENTRAL
2896	SALOM	18	2021-03-08 10:39:20	2021-03-08 10:39:20	CENTRAL
2897	SAN FELIPE	18	2021-03-08 10:40:02	2021-03-08 10:40:02	CENTRAL
2898	SAN FELIPE	18	2021-03-08 10:40:08	2021-03-08 10:40:08	CENTRAL CRITICA
2899	SAN PABLO	18	2021-03-08 10:41:13	2021-03-08 10:41:13	NODO OUTDOOR
2900	SAN PABLO	18	2021-03-08 10:41:36	2021-03-08 10:41:36	SUPERAULA
1793	SUBLA VELA (NO CANTV)	18	\N	2021-03-08 10:42:10	NO CANTV
2901	URACHICHE	18	2021-03-08 10:42:41	2021-03-08 10:42:41	CENTRAL
2902	YUMARE	18	2021-03-08 10:43:32	2021-03-08 10:43:32	CENTRAL
2903	YUMARE	18	2021-03-08 10:44:01	2021-03-08 10:44:01	NODO OUTDOOR
2904	AGUA BLANCA	16	2021-03-08 10:46:35	2021-03-08 10:46:35	CENTRAL
2905	BOCONOITO	16	2021-03-08 10:50:43	2021-03-08 10:50:43	CENTRAL
2906	CAMPO ELIAS	16	2021-03-08 10:51:37	2021-03-08 10:51:37	CENTRAL
2907	CHABASQUEN	16	2021-03-08 10:53:12	2021-03-08 10:53:12	CENTRAL
2908	GUACHE	16	2021-03-08 10:55:27	2021-03-08 10:55:27	CENTRAL
2909	GUANARITO	16	2021-03-08 11:01:09	2021-03-08 11:01:09	CENTRAL
2910	GUANARITO	16	2021-03-08 11:01:39	2021-03-08 11:01:39	NODO OUTDOOR
2911	LA APARICION	16	2021-03-08 11:06:43	2021-03-08 11:06:43	CENTRAL
2912	LA APARICION	16	2021-03-08 11:07:10	2021-03-08 11:07:10	NODO OUTDOOR
2913	LA MISION	16	2021-03-08 11:07:47	2021-03-08 11:07:47	CENTRAL
2914	OSPINO	16	2021-03-08 11:09:17	2021-03-08 11:09:17	CENTRAL
2915	PAPELON	16	2021-03-08 11:09:57	2021-03-08 11:09:57	CENTRAL
2916	PAPELON	16	2021-03-08 11:10:24	2021-03-08 11:10:24	NODO OUTDOOR
2917	PAYARA	16	2021-03-08 11:10:55	2021-03-08 11:10:55	CENTRAL
2918	PIMPINELA	16	2021-03-08 11:11:40	2021-03-08 11:11:40	CENTRAL
2919	PIRITU	16	2021-03-08 11:13:19	2021-03-08 11:13:19	CENTRAL
2920	RIO ACARIGUA	16	2021-03-08 11:15:31	2021-03-08 11:15:31	CENTRAL
1558	SAN FRANCISCO	16	\N	2021-03-08 11:16:25	NODO OUTDOOR
2921	SAN RAFAEL DE ONOTO	16	2021-03-08 11:17:28	2021-03-08 11:17:28	NODO OUTDOOR
2922	RIO CARIBE	29	2021-03-08 11:20:19	2021-03-08 11:20:19	OPSUT
2923	MARIN	18	2021-03-08 11:22:16	2021-03-08 11:22:16	CENTRAL
1056	NEGRO PRIMERO	11	\N	2021-03-08 11:26:59	CENTRAL
2924	ADICORA	15	2021-03-08 11:32:03	2021-03-08 11:32:03	CENTRAL
2925	AGUADA GRANDE	17	2021-03-08 11:34:41	2021-03-08 11:34:41	CENTRAL
2926	BUENA VISTA	15	2021-03-08 11:34:54	2021-03-08 11:34:54	CENTRAL
2927	ANZOATEGUI	17	2021-03-08 11:35:33	2021-03-08 11:35:33	CENTRAL
2928	BUENA VISTA	15	2021-03-08 11:35:36	2021-03-08 11:35:36	CENTRAL
2929	CABURE	15	2021-03-08 11:36:12	2021-03-08 11:36:12	CENTRAL
2930	AREGUE	17	2021-03-08 11:36:34	2021-03-08 11:36:34	CENTRAL
2931	CAPADARE	15	2021-03-08 11:36:53	2021-03-08 11:36:53	CENTRAL
2932	CAPATARIDA	15	2021-03-08 11:37:34	2021-03-08 11:37:34	CENTRAL
2933	CUBIRO	17	2021-03-08 11:43:28	2021-03-08 11:43:28	CENTRAL
2934	DUACA	17	2021-03-08 11:44:37	2021-03-08 11:44:37	CENTRAL
2935	EL GUARICO	17	2021-03-08 11:51:18	2021-03-08 11:51:18	CENTRAL
1438	EZEQUIEL ZAMORA	15	\N	2021-03-08 11:51:51	NODO OUTDOOR
2936	HUMOCARO ALTO	17	2021-03-08 11:52:06	2021-03-08 11:52:06	CENTRAL
2937	LA CRUZ DE TARATARA	15	2021-03-08 11:54:40	2021-03-08 11:54:40	CENTRAL
2938	LA CRUZ DE TARATARA	15	2021-03-08 11:55:41	2021-03-08 11:55:41	NODO OUTDOOR
2939	IRAPA	29	2021-03-08 12:02:10	2021-03-08 12:02:10	OPSUT
2940	PEDREGAL	15	2021-03-08 12:08:03	2021-03-08 12:08:03	CENTRAL
2941	PIRITU	15	2021-03-08 12:08:27	2021-03-08 12:08:27	CENTRAL
1470	PUEBLO NUEVO	15	\N	2021-03-08 12:08:49	CENTRAL
2942	PUEBLO NUEVO	15	2021-03-08 12:09:11	2021-03-08 12:09:11	NODO OUTDOOR
2943	SAN JUAN DE LOS CAYOS	15	2021-03-08 12:11:05	2021-03-08 12:11:05	NODO OUTDOOR
2944	SAN JUAN DE LOS CAYOS	15	2021-03-08 12:11:44	2021-03-08 12:11:44	CENTRAL
2945	SAN LUIS	15	2021-03-08 12:12:35	2021-03-08 12:12:35	CENTRAL
2946	SAN LUIS	15	2021-03-08 12:13:00	2021-03-08 12:13:00	NODO OUTDOOR
2947	SANTA ANA	15	2021-03-08 12:14:14	2021-03-08 12:14:14	CENTRAL
2948	SANTA CRUZ DE BUCARAL	15	2021-03-08 12:15:18	2021-03-08 12:15:18	CENTRAL
1486	SANTA ELENA	15	\N	2021-03-08 12:15:48	NODO OUTDOOR
2949	TOCOPERO	15	2021-03-08 12:17:01	2021-03-08 12:17:01	CENTRAL
2950	TOCUYO DE LA COSTA	15	2021-03-08 12:17:43	2021-03-08 12:17:43	CENTRAL
2951	YARACAL	15	2021-03-08 12:19:49	2021-03-08 12:19:49	CENTRAL CRITICA
2952	CARORA	17	2021-03-08 12:28:30	2021-03-08 12:28:30	CENTRAL CRITICA
1642	EL MEREY	17	\N	2021-03-08 12:32:07	NODO OUTDOOR
2953	EL PORVENIR	17	2021-03-08 12:33:01	2021-03-08 12:33:01	NODO OUTDOOR
2954	EL TOCUYO	17	2021-03-08 12:34:06	2021-03-08 12:34:06	OPSUT
2955	LA PASTORA	17	2021-03-08 12:38:25	2021-03-08 12:38:25	NODO OUTDOOR
1685	LAS MARGARITAS	17	\N	2021-03-08 12:40:55	NODO OUTDOOR
1701	OBELISCO	17	\N	2021-03-08 12:44:07	CENTRAL CRITICA
2956	QUIBOR	17	2021-03-08 12:46:10	2021-03-08 12:46:10	SUPERAULA
2957	RIO CLARO	17	2021-03-08 12:47:11	2021-03-08 12:47:11	CENTRAL
2958	SANARE	17	2021-03-08 12:48:32	2021-03-08 12:48:32	CENTRAL
2959	SANTA INES	17	2021-03-08 12:50:08	2021-03-08 12:50:08	CENTRAL
1726	SANTA ISABEL	17	\N	2021-03-08 12:50:45	CENTRAL
2960	SARARE	17	2021-03-08 12:51:46	2021-03-08 12:51:46	NODO OUTDOOR
2961	SARARE	17	2021-03-08 12:52:13	2021-03-08 12:52:13	CENTRAL
2962	SIQUISIQUE	17	2021-03-08 12:52:47	2021-03-08 12:52:47	CENTRAL
2963	TINTORERO	17	2021-03-08 12:53:38	2021-03-08 12:53:38	CENTRAL
762	EDIFICIO EL RECREO	7	\N	2021-03-09 09:15:48	CENTRAL CRITICA
1807	BAMARI-LUEPA  (NO CANTV)	19	\N	2021-03-09 09:36:17	CENTRAL CRITICA
2964	EL MANTECO I	19	2021-03-09 09:56:36	2021-03-09 09:56:36	NODO OUTDOOR
2965	EL PALMAR	19	2021-03-09 09:59:20	2021-03-09 09:59:20	CENTRAL
2966	EL PAO II	19	2021-03-09 10:00:42	2021-03-09 10:00:42	CENTRAL
1843	LA HOYADA	19	\N	2021-03-09 10:06:19	NODO OUTDOOR
2967	PUERTO ORDAZ	19	2021-03-09 10:23:17	2021-03-09 10:23:17	CENTRAL CRITICA
1869	RIO CLARO	19	\N	2021-03-09 10:24:24	NODO OUTDOOR
2968	SAN FELIX	19	2021-03-09 10:27:09	2021-03-09 10:27:09	CENTRAL
2969	SANTA ELENA DE UAIREN	19	2021-03-09 10:29:10	2021-03-09 10:29:10	CENTRAL
2970	TUMEREMO	19	2021-03-09 10:34:29	2021-03-09 10:34:29	CENTRAL
2971	UPATA	19	2021-03-09 10:37:16	2021-03-09 10:37:16	CENTRAL
2972	CAICARA DEL ORINOCO	20	2021-03-09 10:51:41	2021-03-09 10:51:41	NODO OUTDOOR
2973	GURI	20	2021-03-09 11:09:41	2021-03-09 11:09:41	CENTRAL CRITICA
1928	LA CAROLINA	20	\N	2021-03-09 11:12:05	CENTRAL CRITICA
1946	PIJIGUAO CORPOELEC (NO CANTV)	20	\N	2021-03-09 11:23:07	CENTRAL CRITICA
2974	SOLEDAD	20	2021-03-09 11:29:40	2021-03-09 11:29:40	NODO OUTDOOR
772	CAMURI CABLE SUBMARINO	5	\N	2021-03-09 11:40:06	CENTRAL CRITICA
2975	LAS DELICIAS	30	2021-03-09 14:19:01	2021-03-09 14:19:01	CENTRAL
2976	EL GUAYABO	30	2021-03-10 00:58:50	2021-03-10 00:58:50	CENTRAL
2977	EL GUAYABO	30	2021-03-10 00:59:44	2021-03-10 00:59:44	CENTRAL
2978	SANTA CRUZ DEL ZULIA	30	2021-03-10 01:00:33	2021-03-10 01:00:33	CENTRAL
2979	BELLO MONTE	30	2021-03-10 01:01:21	2021-03-10 01:01:21	CENTRAL
2980	MORALITO	30	2021-03-10 01:01:50	2021-03-10 01:01:50	URL
2981	EL CHIVO	30	2021-03-10 11:54:48	2021-03-10 11:54:48	URL
2982	EL CHIVO	30	2021-03-10 11:54:50	2021-03-10 11:54:50	URL
2983	BOCONO	2	2021-03-10 12:15:57	2021-03-10 12:15:57	URL
2984	SANTA CRUZ DE MORA	4	2021-03-10 14:30:50	2021-03-10 14:30:50	URL
2985	SANTA CRUZ DE MORA	4	2021-03-10 14:30:54	2021-03-10 14:30:54	URL
2986	SANTA CRUZ DE MORA	4	2021-03-10 14:30:59	2021-03-10 14:30:59	URL
2987	LA FLORIDA	34	2021-03-11 12:39:31	2021-03-11 12:39:31	NGN
2988	MAIQUETIA I	5	2021-03-12 09:23:16	2021-03-12 09:23:16	CENTRAL CRITICA
2989	MAIQUETIA II	4	2021-03-12 09:25:09	2021-03-12 09:25:09	CENTRAL CRITICA
429	CC LA HOYADA	5	\N	2021-03-12 09:29:56	CENTRAL
2990	TINAQUILLO TX	12	2021-03-12 10:31:43	2021-03-12 10:31:43	CENTRAL
2991	TINAQUILLO DIGITAL	12	2021-03-12 10:35:13	2021-03-12 10:35:13	CENTRAL
2992	EL PILAR	29	2021-03-12 11:14:29	2021-03-12 11:14:29	OPSUT
2993	RUBIO	1	2021-03-12 12:49:52	2021-03-12 12:49:52	CENTRAL
2994	ARAGUA DE BARCELONA	28	2021-03-12 12:57:20	2021-03-12 12:57:20	OPSUT
428	CC CARICUAO NA10-143	5	\N	2021-03-12 13:12:05	NODO OUTDOOR
786	LAS TUNITAS II NA10-127	5	\N	2021-03-12 13:13:14	NODO OUTDOOR
2995	AV SUR # 20 HATILLO	6	2021-03-12 13:32:47	2021-03-12 13:32:47	NODO OUTDOOR
2996	CAPAYA	6	2021-03-12 13:35:55	2021-03-12 13:35:55	CENTRAL
2997	CAPAYA	6	2021-03-12 13:36:17	2021-03-12 13:36:17	NODO OUTDOOR
2998	CAUCAGUA I	6	2021-03-12 13:39:49	2021-03-12 13:39:49	CENTRAL
2999	CAUCAGUA I	6	2021-03-12 13:40:08	2021-03-12 13:40:08	NODO OUTDOOR
3000	CAUCAGUA II	6	2021-03-12 13:42:04	2021-03-12 13:42:04	CENTRAL
3001	CHARALLAVE	6	2021-03-12 13:48:35	2021-03-12 13:48:35	OAC
831	CHIRIMENA	6	\N	2021-03-12 13:49:48	NODO OUTDOOR
3002	CIUDAD CASARAPA I	6	2021-03-12 13:51:26	2021-03-12 13:51:26	NODO OUTDOOR
3003	CIUDAD CASARAPA II	6	2021-03-12 13:52:15	2021-03-12 13:52:15	NODO OUTDOOR
3004	CIUDAD CASARAPA III	6	2021-03-12 13:53:01	2021-03-12 13:53:01	NODO OUTDOOR
3005	CIUDAD FAJARDO	6	2021-03-12 13:54:26	2021-03-12 13:54:26	CENTRAL
1938	MARIPA	20	\N	2021-03-12 15:08:14	SUPERAULA
3006	CUA	6	2021-03-12 13:59:20	2021-03-12 13:59:20	CENTRAL
3007	CUPIRA	6	2021-03-12 13:59:56	2021-03-12 13:59:56	CENTRAL
3008	CUPIRA	6	2021-03-12 14:00:18	2021-03-12 14:00:18	NODO OUTDOOR
3009	EL CARMEN	6	2021-03-12 14:02:44	2021-03-12 14:02:44	NODO OUTDOOR
3010	EL GUAPO	6	2021-03-12 14:04:14	2021-03-12 14:04:14	CENTRAL
3011	EL GUAPO	6	2021-03-12 14:05:04	2021-03-12 14:05:04	OPSUT
3012	EL GUAPO	6	2021-03-12 14:05:33	2021-03-12 14:05:33	NODO OUTDOOR
3013	HACIENDA EL CARMEN GAB # 13	6	2021-03-12 14:09:29	2021-03-12 14:09:29	NODO OUTDOOR
620	LA FUNDACION	6	\N	2021-03-12 14:11:39	NODO OUTDOOR
3014	RIO CHICO	6	2021-03-12 14:30:00	2021-03-12 14:30:00	CENTRAL
3015	RIO CHICO	6	2021-03-12 14:30:20	2021-03-12 14:30:20	NODO OUTDOOR
3016	SAN FRANCISCO DE YARE	6	2021-03-12 14:32:21	2021-03-12 14:32:21	NODO OUTDOOR
3017	SAN JOSE DE RIO CHICO	6	2021-03-12 14:33:10	2021-03-12 14:33:10	NODO OUTDOOR
3018	SANTIAGO DE LEON	6	2021-03-12 14:38:50	2021-03-12 14:38:50	CENTRAL
3019	SANTIAGO DE LEON	6	2021-03-12 14:38:51	2021-03-12 14:38:51	CENTRAL
3020	TACARIGUA DE LA LAGUNA	6	2021-03-12 14:40:16	2021-03-12 14:40:16	CENTRAL
686	TRAPICHITO	6	\N	2021-03-12 14:42:22	CENTRAL CRITICA
3021	Y02 C OCUMARE AVE MARIA	6	2021-03-12 14:48:05	2021-03-12 14:48:05	DLC
885	Y03 C SANTA TERESA DOS LAGUNAS CASETA P	6	\N	2021-03-12 14:51:01	DLC
888	Y04 C SANTA TERESA ENTRADA VISTA LINDA	6	\N	2021-03-12 14:53:02	DLC
3022	EL PERU	20	2021-03-12 15:00:58	2021-03-12 15:00:58	CENTRAL
3023	VILLA EL ROSARIO	30	2021-03-12 15:49:43	2021-03-12 15:49:43	CENTRAL
3025	TX LAS INDUSTRIAS	29	2021-03-16 12:18:05	2021-03-16 12:18:05	CENTRAL
3026	ALMACEN	28	2021-03-16 12:19:46	2021-03-16 12:19:46	CENTRAL
3027	CONCORDIA	28	2021-03-16 12:20:22	2021-03-16 12:20:22	CENTRAL
3028	EDIF GERENCIA	28	2021-03-16 12:20:54	2021-03-16 12:20:54	CENTRAL
3029	PUERTO LA CRUZ	28	2021-03-16 12:21:31	2021-03-16 12:21:31	OAC
3030	GUARATARO	19	2021-03-16 13:34:56	2021-03-16 13:34:56	NGN
1966	ALTOS DEL SOL AMADO	30	\N	2021-03-16 14:43:40	NODO OUTDOOR
2605	AMPARO	30	2021-01-26 10:03:22	2021-03-16 14:53:03	CENTRAL
3031	AMPARO	30	2021-03-16 14:53:36	2021-03-16 14:53:36	NODO OUTDOOR
2598	BELLA VISTA III	30	2021-01-26 10:03:22	2021-03-16 14:57:45	CENTRAL CRITICA
1992	CHAMARRETA II	30	\N	2021-03-16 15:10:00	NODO OUTDOOR
3032	EL CARMELO	30	2021-03-16 15:17:15	2021-03-16 15:17:15	CENTRAL
3033	ISLA DE TOAS	30	2021-03-16 15:21:27	2021-03-16 15:21:27	CENTRAL
2021	LA PAZ	8	\N	2021-03-16 15:25:12	NODO OUTDOOR
3024	LAS INDUSTRIAS	30	2021-03-15 18:52:45	2021-03-16 15:26:51	CENTRAL CRITICA
3034	LAS PIEDRAS	30	2021-03-16 15:28:34	2021-03-16 15:28:34	NODO OUTDOOR
2031	LOS COMPATRIOTAS	30	\N	2021-03-16 15:29:34	NODO OUTDOOR
2589	LOS OLIVOS	30	2021-01-26 10:03:22	2021-03-16 15:32:07	CENTRAL CRITICA
3035	SAN IGNACIO	30	2021-03-16 15:38:42	2021-03-16 15:38:42	CENTRAL
3036	SAN JOSE DE PERIJA	30	2021-03-16 15:40:08	2021-03-16 15:40:08	NODO OUTDOOR
3037	SIMON BOLIVAR	30	2021-03-16 15:43:36	2021-03-16 15:43:36	NODO OUTDOOR
2068	SINGAPUR	30	\N	2021-03-16 15:45:05	NODO OUTDOOR
3038	CARIPE	26	2021-03-16 19:20:58	2021-03-16 19:20:58	TX
3039	CARIPE	26	2021-03-16 19:21:00	2021-03-16 19:21:00	TX
3040	CAICARA DE MATURIN	26	2021-03-16 19:25:47	2021-03-16 19:25:47	TX
3041	BARRANCAS DEL ORINOCO	26	2021-03-16 19:26:35	2021-03-16 19:26:35	TX
3042	CARIPITO	26	2021-03-16 19:27:16	2021-03-16 19:27:16	TX
3043	CHAGUARAMAL	26	2021-03-16 19:27:45	2021-03-16 19:27:45	TX
3044	CHAGUARAMA	26	2021-03-16 19:28:23	2021-03-16 19:28:23	TX
3045	JUSEPIN	26	2021-03-16 19:29:00	2021-03-16 19:30:12	TX
3046	TERESEN	26	2021-03-16 19:30:44	2021-03-16 19:30:44	TX
3047	URACOA	26	2021-03-16 19:31:12	2021-03-16 19:31:12	TX
3048	CENTELLITA	26	2021-03-16 19:31:47	2021-03-16 19:31:47	OPSUT
2295	URBINA	6	\N	2021-03-16 19:36:00	OPSUT
3049	ORUZA	9	2021-03-16 19:37:17	2021-03-16 19:37:17	OPSUT
1144	GALERAS DEL PAO	12	\N	2021-03-16 22:05:36	OPSUT
3050	MARIPA	21	2021-03-17 09:56:57	2021-03-17 09:56:57	OPSUT
3051	SAN SALVADOR	34	2021-03-17 15:12:11	2021-03-17 15:13:15	NODO AUTDOOR
3052	CABIMAS CENTRO	33	2021-03-17 23:40:30	2021-03-17 23:40:30	CENTRAL
3053	CABIMAS CENTRO	33	2021-03-17 23:40:33	2021-03-17 23:40:33	CENTRAL
3054	SAN ENRIQUE	22	2021-03-18 09:42:44	2021-03-18 09:42:44	NODO OUTDOOR
1915	CERRO AUTANA	22	\N	2021-03-18 09:52:46	NODO OUTDOOR
3055	SANTA RITA	30	2021-03-18 14:33:17	2021-03-18 14:33:17	CENTRAL
391	SANTA BARBARA DEL ZULIA	32	\N	2021-03-18 20:47:21	CENTRAL
3057	CERRO CLOECO	22	2021-03-19 10:01:14	2021-03-19 10:01:14	CENTRAL
3058	CAICARA DEL ORINOCO	22	2021-03-19 10:02:33	2021-03-19 10:02:33	CENTRAL
2660	LOS PIJIGUAOS	22	2021-02-04 12:40:54	2021-03-19 10:06:18	OPSUT
3056	MANIAPURE	22	2021-03-18 16:47:28	2021-03-19 10:11:49	RB MOVILNET
3059	CIUDAD OJEDA	33	2021-03-19 11:26:43	2021-03-19 11:26:43	CENTRAL
3060	CARABOBO	4	2021-03-19 16:12:44	2021-03-19 16:12:44	URL
3061	CONEJEROS	27	2021-03-19 19:25:30	2021-03-19 19:25:30	URl
3062	CNT EQUIPOS II	7	2021-03-22 14:30:11	2021-03-22 14:32:49	CENTRAL CRITICA
3063	ENCONTRADOS	32	2021-03-22 17:26:56	2021-03-22 17:26:56	NODO AUTDOOR
\.


--
-- Data for Name: mantenimiento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.mantenimiento (id, descripcion, tipo, falla_id, created_at, updated_at) FROM stdin;
1	MEDICION DE VOLTAJE / AMPERAJE	1	1	\N	\N
2	SUSTITUCION DE BATERIA DE ARRANQUE	2	1	\N	\N
3	COMPLETACION DE NIVELES DE ELECTROLITOS	1	2	\N	\N
4	AJUSTE DE VOLTAJE / AMPERAJE EN ATS	2	2	\N	\N
5	LIMPIEZA DE BORNES	1	3	\N	\N
6	LIMPIEZA DE BORNES	1	4	\N	\N
7	REEMPLAZO DE FUSIBLE	2	4	\N	\N
8	AJUSTE DE VOLTAJE / AMPERAJE EN ATS	1	5	\N	\N
9	INSPECCION VISUAL	1	6	\N	\N
10	INSPECCION VISUAL	1	7	\N	\N
11	REMPLAZO	2	7	\N	\N
12	SUSTITUCION DE BATERIA DE ARRANQUE	2	8	\N	\N
13	COMPLETACION DE NIVELES DE ELECTROLITOS	1	9	\N	\N
14	REMPLAZO CELDA	2	9	\N	\N
15	LIMPIEZA DE FILTRO DE COMBUSTIBLE	1	10	\N	\N
16	CAMBIO DE FILTRO DE COMBUSTIBLE	2	10	\N	\N
17	MEDICION DE NIVELES DE COMBUSTIBLE	1	10	\N	\N
18	SURTIDO DE COMBUSTIBLE	2	10	\N	\N
19	MEDICION DE NIVELES DE COMBUSTIBLE	1	11	\N	\N
20	SURTIDO DE COMBUSTIBLE	2	11	\N	\N
21	LIMPIEZA DE FILTROS ACEITE / AIRE	1	12	\N	\N
22	CAMBIO DE FILTROS	2	12	\N	\N
23	RECUPERACION NIVELES DE ACEITE	1	13	\N	\N
24	CAMBIO DE ACEITE	2	13	\N	\N
25	REVISION DE EMPACADURAS Y ESTOPERAS	1	13	\N	\N
26	CAMBIO DE EMPACADURAS Y ESTOPERAS	2	13	\N	\N
27	REVISION DE MANGUERAS	1	14	\N	\N
28	CAMBIO O AJUSTE DE MANGUERAS DE ALTA PRESION	2	14	\N	\N
29	AJUSTES DE CONEXIONES A MANGUERAS	1	14	\N	\N
30	MEDICION DE VOLTAJE / AMPERAJE	1	15	\N	\N
31	LIMPIEZA DE ATS	1	15	\N	\N
32	AJUSTE DE TORNILLERIA	1	15	\N	\N
33	REPARACION O CAMBIO DE ATS	2	15	\N	\N
34	LIMPIEZA / BAQUETEO DE RADIADOR	1	16	\N	\N
35	SOLDADURA O CAMBIO DE RADIADOR	2	16	\N	\N
36	MEDICION DE VOLTAJE / AMPERAJE	1	17	\N	\N
37	LIMPIEZA DE TURBO	1	17	\N	\N
38	MANTENIMIENTO EN RODAMIENTOS	2	17	\N	\N
39	REEMPLAZO DE TURBO	2	17	\N	\N
40	REVISION DE TARJETA REGULADORA DE VOLTAJE	1	18	\N	\N
41	INSPECCION VISUAL	1	18	\N	\N
42	REMPLAZO DE TARJETA REGULADORA DE VOLTAJE	2	18	\N	\N
43	REEMPLAZO DE COMPONENTES ELECTRONICOS	2	18	\N	\N
44	REVISION DE CAMPO	1	19	\N	\N
45	REMPLAZO DE RODAMIENTO	2	19	\N	\N
46	INSPECCION VISUAL	1	20	\N	\N
47	REPARACION DE ESTATOR	2	20	\N	\N
48	REVISION DE RODAMIENTO	1	21	\N	\N
49	REPARACION DE ROTOR	2	21	\N	\N
50	REVISION DE ROTOR	1	22	\N	\N
51	REPARACION DE EXITATRIS	2	22	\N	\N
52	REMPLAZO DE RODAMIENTO	1	23	\N	\N
53	MEDICION DE VOLTAJE / AMPERAJE EN RECTIFICADOR	1	24	\N	\N
54	RESET, REPARACION REEMPLAZO DE RECTIFICADOR	2	24	\N	\N
55	MEDICION DE VOLTAJE / AMPERAJE	1	25	\N	\N
56	REVISION DE ACOMETIDA	1	25	\N	\N
57	INSPECCION VISUAL	1	25	\N	\N
58	REPARACION O REEMPLAZO DE SUPERVISOR DE VOLTAJE	2	25	\N	\N
59	MEDICION DE VOLTAJE / AMPERAJE	1	26	\N	\N
60	CAMBIO DE CELDAS	2	26	\N	\N
61	COMPLETACION DE NIVELES DE ELECTROLITOS	1	27	\N	\N
62	AJUSTE DE VOLTAJE / AMPERAJE EN ATS	2	27	\N	\N
63	INSPECCION VISUAL	1	28	\N	\N
64	REEMPLAZO DE FUSIBLE	2	28	\N	\N
65	LIMPIEZA Y/O APLICACIÓN DE ANTICORROCIVO	1	29	\N	\N
66	RECONSTRUCCION DEL BORDE/TERMINAL	2	29	\N	\N
67	MEDICION DE VOLTAJE / AMPERAJE EN BATERIA	1	30	\N	\N
68	LIMPIEZA DE CONTACTOS EN BATERIA	1	30	\N	\N
69	SUSTITUCION DE BATERIA	2	30	\N	\N
70	MEDICION DE VOLTAJE / AMPERAJE EN BATERIA	1	31	\N	\N
71	LIMPIEZA DE BREAKER	1	31	\N	\N
72	INSPECCION VISUAL	1	31	\N	\N
73	CAMBIO DE BREAKER	2	31	\N	\N
\.


--
-- Data for Name: menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.menu (id, nombre, rol_id, url, icon, observacion, created_at, updated_at) FROM stdin;
1	Inicio	1	home	fas fa-home nav-icon	Menu Inicio	\N	\N
2	Inventario	1	#	fas fa-home nav-icon	Menu Inicio	\N	\N
3	Mantenimiento	1	#	fas fa-home nav-icon	Mantenimiento	\N	\N
4	Administracion	1	#	fas fa-home nav-icon	Administracion	\N	\N
5	Cerrar Sesion	1	#	fas fa-tty	Usuarios	\N	\N
\.


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.migrations (id, migration, batch) FROM stdin;
1	2014_10_12_000000_create_users_table	1
2	2014_10_12_100000_create_password_resets_table	1
3	2019_08_19_000000_create_failed_jobs_table	1
4	2020_09_25_160625_motor	1
5	2020_09_25_164616_roles	1
6	2020_09_25_164938_equipo	1
7	2020_09_25_165056_localidad	1
8	2020_09_25_165144_mantenimiento	1
9	2020_09_25_165229_componente	1
10	2020_09_25_165358_fallas	1
11	2020_09_25_165452_tickets	1
12	2020_09_25_165550_regiones	1
13	2020_09_25_165650_estados	1
14	2020_09_25_165752_comentarios	1
15	2020_09_25_165842_centrales	1
16	2020_09_25_165933_generador	1
17	2020_09_25_170048_aire_acondicionado	1
18	2020_09_25_170345_status	1
19	2020_09_25_170447_ubicacion	1
20	2020_09_25_170559_banco_bateria	1
21	2020_09_25_170653_rectificadores	1
22	2020_09_25_170739_ups	1
23	2020_09_25_170830_personal	1
24	2020_10_01_183608_moto_generador	1
25	2020_10_06_140928_ups_b_b	1
26	2020_10_07_152842_rect_bcobb	1
27	2020_10_21_103738_multimedia	1
28	2020_11_02_112649_registro	1
29	2020_11_03_113221_plantillas	1
30	2020_11_04_141412_nodo_opsut	1
31	2020_11_04_141601_reque_opsut	1
32	2020_11_04_141658_detalle_opsut_pivot	1
33	2020_11_09_083422_detalle_bypas_opsut_pivot	1
34	2020_11_19_103352_menu	1
35	2020_11_19_103647_sub_menu	1
36	2020_11_19_105022_inv_equip_opsut	1
37	2020_11_19_131603_sub_sub_menu	1
38	2020_12_15_160140_cuadrilla	1
39	2020_12_15_160457_contratista	1
40	2021_01_11_141857_motog_batctrl	1
41	2021_01_25_085518_region_user	2
42	2021_01_26_115306_campo_ton_refrigeracin	3
43	2021_02_10_122422_tipo_local	4
44	2021_03_15_092935_acciones	5
45	2021_03_15_093026_salas	5
46	2021_03_15_093102_pisos	5
47	2021_03_17_113853_estado_personal	6
\.


--
-- Data for Name: motogenerador; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.motogenerador (id, motor_id, generador_id, tipo_id, created_at, updated_at) FROM stdin;
1	1	1	1	2021-01-12 12:29:07	2021-01-12 12:29:07
4	4	4	1	2021-01-12 12:53:01	2021-01-12 12:53:01
6	6	6	1	2021-01-12 13:42:58	2021-01-12 13:42:58
7	7	7	1	2021-01-12 14:25:41	2021-01-12 14:25:41
8	8	8	1	2021-01-12 14:35:46	2021-01-12 14:35:46
9	9	9	1	2021-01-12 15:46:05	2021-01-12 15:46:05
10	10	10	1	2021-01-13 13:38:03	2021-01-13 13:38:03
11	11	11	1	2021-01-13 14:09:13	2021-01-13 14:09:13
12	12	12	1	2021-01-20 10:06:43	2021-01-20 10:06:43
14	14	14	1	2021-01-20 10:19:20	2021-01-20 10:19:20
15	15	15	1	2021-01-20 10:36:09	2021-01-20 10:36:09
16	16	16	1	2021-01-20 10:47:07	2021-01-20 10:47:07
17	17	17	1	2021-01-20 10:53:45	2021-01-20 10:53:45
18	18	18	1	2021-01-20 11:04:50	2021-01-20 11:04:50
19	19	19	1	2021-01-20 11:10:49	2021-01-20 11:10:49
21	21	21	1	2021-01-20 12:33:11	2021-01-20 12:33:11
23	23	23	1	2021-01-20 12:54:48	2021-01-20 12:54:48
24	24	24	1	2021-01-22 10:55:37	2021-01-22 10:55:37
25	25	25	1	2021-01-22 11:01:31	2021-01-22 11:01:31
28	28	28	1	2021-01-22 11:02:37	2021-01-22 11:02:37
29	29	29	1	2021-01-22 11:08:48	2021-01-22 11:08:48
30	30	30	1	2021-01-22 11:15:10	2021-01-22 11:15:10
31	31	31	1	2021-01-22 11:22:35	2021-01-22 11:22:35
32	32	32	1	2021-01-22 11:28:27	2021-01-22 11:28:27
33	33	33	1	2021-01-22 11:57:27	2021-01-22 11:57:27
34	34	36	1	2021-01-22 12:12:55	2021-01-22 12:12:55
37	37	39	1	2021-01-22 12:40:15	2021-01-22 12:40:15
39	39	41	1	2021-01-22 12:52:34	2021-01-22 12:52:34
40	40	42	1	2021-01-26 10:51:18	2021-01-26 10:51:18
41	41	43	1	2021-01-26 12:20:18	2021-01-26 12:20:18
42	42	44	1	2021-01-27 11:57:28	2021-01-27 11:57:28
43	43	45	1	2021-01-27 11:57:43	2021-01-27 11:57:43
44	44	46	1	2021-01-28 08:13:17	2021-01-28 08:13:17
47	47	49	1	2021-01-29 13:16:31	2021-01-29 13:16:31
48	48	50	1	2021-02-01 11:19:31	2021-02-01 11:19:31
49	49	51	1	2021-02-01 11:27:45	2021-02-01 11:27:45
50	50	52	1	2021-02-01 11:38:05	2021-02-01 11:38:05
51	51	53	1	2021-02-01 11:47:16	2021-02-01 11:47:16
53	53	55	1	2021-02-10 13:05:29	2021-02-10 13:05:29
55	55	57	1	2021-02-10 14:19:07	2021-02-10 14:19:07
56	56	58	1	2021-02-10 16:45:48	2021-02-10 16:45:48
57	57	59	1	2021-02-11 10:00:25	2021-02-11 10:00:25
58	58	60	1	2021-02-11 10:53:39	2021-02-11 10:53:39
59	59	61	1	2021-02-11 11:05:14	2021-02-11 11:05:14
62	62	64	1	2021-02-11 14:44:20	2021-02-11 14:44:20
64	64	66	1	2021-02-12 08:37:34	2021-02-12 08:37:34
65	65	74	1	2021-02-12 10:36:16	2021-02-12 10:36:16
66	66	75	1	2021-02-12 11:06:16	2021-02-12 11:06:16
68	68	78	1	2021-02-12 11:20:32	2021-02-12 11:20:32
69	69	79	1	2021-02-12 12:10:24	2021-02-12 12:10:24
70	70	80	1	2021-02-12 15:40:17	2021-02-12 15:40:17
71	71	81	1	2021-02-17 08:26:21	2021-02-17 08:26:21
72	72	82	1	2021-02-17 15:27:02	2021-02-17 15:27:02
73	73	83	1	2021-02-17 15:40:27	2021-02-17 15:40:27
74	74	84	1	2021-02-17 15:53:12	2021-02-17 15:53:12
75	75	85	1	2021-02-23 11:36:39	2021-02-23 11:36:39
77	77	87	1	2021-02-23 12:06:52	2021-02-23 12:06:52
78	78	88	1	2021-02-23 12:22:06	2021-02-23 12:22:06
80	80	90	1	2021-02-23 14:10:14	2021-02-23 14:10:14
81	81	91	1	2021-02-23 14:17:51	2021-02-23 14:17:51
82	82	92	1	2021-02-23 14:27:54	2021-02-23 14:27:54
83	83	93	1	2021-02-23 14:32:24	2021-02-23 14:32:24
84	84	94	1	2021-02-23 14:53:10	2021-02-23 14:53:10
85	85	95	1	2021-02-24 14:01:48	2021-02-24 14:01:48
86	86	96	1	2021-02-24 14:38:20	2021-02-24 14:38:20
87	87	97	1	2021-02-24 14:43:59	2021-02-24 14:43:59
88	88	98	1	2021-02-25 13:53:30	2021-02-25 13:53:30
89	89	99	1	2021-02-25 14:14:40	2021-02-25 14:14:40
90	90	100	1	2021-02-25 14:18:03	2021-02-25 14:18:03
91	91	101	1	2021-02-25 14:23:59	2021-02-25 14:23:59
92	92	102	1	2021-02-25 14:32:19	2021-02-25 14:32:19
96	96	106	1	2021-03-01 12:43:41	2021-03-01 12:43:41
97	97	107	1	2021-03-01 13:11:38	2021-03-01 13:11:38
98	98	108	1	2021-03-01 13:25:53	2021-03-01 13:25:53
99	99	109	1	2021-03-18 10:37:17	2021-03-18 10:37:17
100	100	110	1	2021-03-18 11:20:08	2021-03-18 11:20:08
104	104	114	1	2021-03-18 12:37:57	2021-03-18 12:37:57
108	108	118	1	2021-03-22 17:29:29	2021-03-22 17:29:29
\.


--
-- Data for Name: motor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.motor (id, marca, modelo, estado_arranque, cap_tanq_aceite, cap_tanq_refrig, comb_tanq_princ, cantidad_filtro, modelo_filtro, cant_bat_arranq, carg_bat, cant_baterias, fases, amp_bateria, status_id, ubicacion_id, inventario_cant, serial, operatividad, fecha_instalacion, criticidad, garantia, observaciones, created_at, updated_at) FROM stdin;
2	Cummins	KTA50G9	Manual	50	100	116000	2	BF1216	0	No	2	416V	\N	\N	2	5033324	33176158	Operativo	\N	Alta	no	\N	2021-01-12 12:35:29	2021-01-12 12:35:29
3	Cummins	KTA50G9	Manual	50	100	116000	2	BF1216	0	No	2	416V	\N	\N	3	5033324	33176158	Operativo	\N	Alta	no	\N	2021-01-12 12:35:38	2021-01-12 12:35:38
1	Cummins	KTA50G9	Manual	50	100	116000	2	BF1216	0	No	2	416V	2000	\N	1	5033324	33176158	Operativo	\N	Alta	no	\N	2021-01-12 12:29:07	2021-01-12 12:43:39
4	Cummins	KTA50G9	Inoperativo	50	100	116000	2	BF1216	0	No	2	416V	2000	\N	4	5033392	33176176	Inoperativo	\N	Alta	no	\N	2021-01-12 12:53:01	2021-01-12 12:53:01
5	SDMO	Catterpillar	Manual	1234123	21321	1234	1	Btf215	23	Si	2	208V	321	\N	5	123123	no posee	Inoperativo	2021-01-12	Media	si	sin observaciones	2021-01-12 13:36:01	2021-01-12 13:36:01
6	Cummins	QST30G4	Inoperativo	50	100	116000	2	FS1006	0	No	2	416V	2000	\N	6	5033388	37237955	Inoperativo	\N	Alta	no	\N	2021-01-12 13:42:58	2021-01-12 13:42:58
8	MTU	16V2000	Automático	50	100	116000	2	P551402	0	No	2	416V	2000	\N	8	5061747	536106833	Operativo	\N	Alta	no	\N	2021-01-12 14:35:46	2021-01-12 14:35:46
9	Cummins	QST30G4	Manual	50	100	116000	2	LF9001	0	No	2	416V	2000	\N	9	5033384	37243099	Operativo	\N	Alta	no	\N	2021-01-12 15:46:05	2021-01-12 15:46:05
7	Cummins	NTTA855G	Automático	50	100	6000	1	51970	0	No	2	416V	1000	\N	7	3108961	18105083	Operativo	\N	Alta	no	\N	2021-01-12 14:25:41	2021-01-12 15:59:07
10	Detroit Diesel	R1637K35	Automático	50	100	10000	2	B7180	1	Si	2	480V	2000	\N	10	3125562	5362002358	Operativo	2008-12-19	Alta	no	\N	2021-01-13 13:38:03	2021-01-13 13:38:03
11	Seleccione una marca	P222LE-S	Automático	50	100	6000	2	6505510-5020B	0	No	2	208V	2000	\N	11	6579305	EAY0E900962	Operativo	2011-09-12	Alta	no	\N	2021-01-13 14:09:13	2021-01-13 14:09:13
12	DOOSAN	PI180LE	Automático	57	100	10000	4	DOOSAN 650551050220B, DOOSAN 65-15203-5016B	1	Si	2	208V	1000	\N	14	3126695	EAS0A800950	Operativo	2010-03-01	Baja	no	\N	2021-01-20 10:06:43	2021-01-20 10:06:43
13	DEUTZ	F6L912	Automático	37	100	6500	4	WP / WIX  2801 / 51820, WIX 33358	1	Si	2	208V	1050	\N	15	6411464	82697666	Operativo	1993-08-27	Baja	no	\N	2021-01-20 10:17:56	2021-01-20 10:17:56
14	DEUTZ	F6L912	Automático	37	100	6500	4	WP / WIX  2801 / 51820, WIX 33358	1	Si	2	208V	1050	\N	16	6411464	82697666	Operativo	1993-08-27	Baja	no	\N	2021-01-20 10:19:20	2021-01-20 10:19:20
15	DEUTZ	F6L912	Manual	37	100	4000	4	WP / WIX  2801 / 51820, WIX 33358	1	Si	2	208V	850	\N	17	507109	no posee	Operativo	1992-11-15	Media	no	\N	2021-01-20 10:36:09	2021-01-20 10:36:09
16	PERKINS	SIN INFORMACION	Automático	37	100	2500	4	SIN INFORMACION	0	No	1	208V	SIN INFORMACION	\N	18	5033488	no posee	Operativo	2014-06-25	Media	no	\N	2021-01-20 10:47:07	2021-01-20 10:47:07
17	PERKINS	SIN INFORMACION	Manual	37	100	2500	4	SIN INFORMACION	0	No	1	208V	SIN INFORMACION	\N	19	5033465	no posee	Operativo	2014-06-25	Media	no	\N	2021-01-20 10:53:45	2021-01-20 10:53:45
18	PERKINS	SIN INFORMACION	Manual	37	100	2500	4	SIN INFORMACION	0	No	1	208V	SIN INFORMACION	\N	20	SIN INFORMACION	no posee	Operativo	\N	Media	no	\N	2021-01-20 11:04:50	2021-01-20 11:04:50
19	PERKINS	SIN INFORMACION	Manual	37	100	500	4	SIN INFORMACION	0	No	1	208V	SIN INFORMACION	\N	21	SIN INFORMACION	no posee	Operativo	\N	Media	no	\N	2021-01-20 11:10:49	2021-01-20 11:10:49
20	JOHN DEER	SIN INFORMACION	Manual	37	100	600	4	SIN INFORMACION	1	Si	1	208V	SIN INFORMACION	\N	22	5047829	4024HF285B	Operativo	2011-08-03	Media	no	\N	2021-01-20 12:33:10	2021-01-20 12:33:10
21	JOHN DEER	SIN INFORMACION	Manual	37	100	600	4	SIN INFORMACION	1	Si	1	208V	SIN INFORMACION	\N	23	5047829	4024HF285B	Operativo	2011-08-03	Media	no	\N	2021-01-20 12:33:11	2021-01-20 12:33:11
22	JOHN DEER	SIN INFORMACION	Manual	37	100	600	4	SIN INFORMACION	1	Si	1	208V	SIN INFORMACION	\N	24	5047829	4024HF285B	Operativo	2011-08-03	Media	no	\N	2021-01-20 12:33:11	2021-01-20 12:33:11
23	DEUTZ	SIN INFORMACION	Automático	37	100	6000	4	LF670 o 3363,3405/33405,WIX 3022209,WF2071	1	Si	2	416V	950	\N	25	507298	no posee	Operativo	1985-05-01	Baja	no	\N	2021-01-20 12:54:48	2021-01-20 12:54:48
24	Cummins	NTA.855-G2	Automático	57	100	6000	4	LF670 o 3363,3405/33405,WIX 3022209,WIF2071	1	Si	2	416V	1100	\N	27	506693	23206377	Operativo	2011-01-01	Baja	no	\N	2021-01-22 10:55:37	2021-01-22 10:55:37
25	Cummins	6BT	Automático	00	00	6000	6	50	1	Si	2	208V	1100	\N	28	3142532	10781384	Operativo	1978	Alta	no	NO TIENE BATERIA DE CONTROL.\r\nGENERADOR MARCA ONAN NI APARECE EN LAS OPCIONES	2021-01-22 11:01:31	2021-01-22 11:01:31
26	Cummins	6BT	Automático	00	00	6000	6	50	1	Si	2	208V	1100	\N	29	3142532	10781384	Operativo	1978	Alta	no	NO TIENE BATERIA DE CONTROL.\r\nGENERADOR MARCA ONAN NI APARECE EN LAS OPCIONES	2021-01-22 11:01:49	2021-01-22 11:01:49
27	Cummins	6BT	Automático	00	00	6000	6	50	1	Si	2	208V	1100	\N	30	3142532	10781384	Operativo	1978	Alta	no	NO TIENE BATERIA DE CONTROL.\r\nGENERADOR MARCA ONAN NI APARECE EN LAS OPCIONES	2021-01-22 11:01:58	2021-01-22 11:01:58
28	Cummins	NT.855-G4	Automático	37	100	3000	4	LF670 o 3363,3405/33405,WIX 3022209,WIF2071	0	No	2	208V	800	\N	31	6411501	23209983	Operativo	1992-12-10	Baja	no	\N	2021-01-22 11:02:37	2021-01-22 11:02:37
29	Seleccione una marca	3406DI	Inoperativo	50	00	6000	4	500	1	Si	2	480V	1100	\N	32	3142574	2WB12684	Inoperativo	2000	Alta	no	LA MARCA DE MOTOR Y GENERADOR ES CATERPILLAR NO APARECE ENTRE LAS OPCIONES.\r\nNO TIENE BATERIA DE CONTROL	2021-01-22 11:08:48	2021-01-22 11:08:48
30	JOHN DEER	6076AF-00	Automático	50	50	4800	4	50	1	Si	2	208V	1100	\N	33	3142687	R66076A1905	Operativo	2000	Media	no	EL GENERADOR MARCA KHOLER NO APARECE EN LAS OPCIONES	2021-01-22 11:15:10	2021-01-22 11:15:10
31	JOHN DEER	4045TF250	Inoperativo	00	00	3000	500	50	1	Si	1	208V	800	\N	34	5048929	GE4045T452485	Inoperativo	1998	Media	no	GENERADOR KHOLER.\r\nFALLA EN GENERADOR QUEMADO.	2021-01-22 11:22:35	2021-01-22 11:22:35
32	Cummins	6BT	Inoperativo	50	50	3000	5	50	2	Si	2	208V	1100	\N	35	3142616	23202365	Inoperativo	1985	Alta	no	inoperativo necesita overhault, en espera de contratista ya se tiene los repuestos enero 2021	2021-01-22 11:28:27	2021-01-22 11:28:27
33	Cummins	NT.855-G1	Automático	37	100	3000	4	LF670 o 3363,3405/33405,WIX 3022209,WIF2071	0	No	2	416V	1050	\N	38	6412053	23203919	Operativo	1993-08-01	Baja	no	\N	2021-01-22 11:57:27	2021-01-22 11:57:27
34	Cummins	NTA.855-G2	Automático	37	100	3000	4	LF670 o 3363,3405/33405,WIX 3022209,WIF2071	0	No	2	480V	1225	\N	41	6411626	23201366	Operativo	1998-06-01	Baja	no	\N	2021-01-22 12:12:55	2021-01-22 12:12:55
35	Cummins	NT.855-G2	Manual	37	100	3000	4	LF670 o 3363,3405/33405,WIX 3022209,WIF2071	0	No	2	208V	1225	\N	42	6581166	30302457	Operativo	1993-06-01	Media	no	\N	2021-01-22 12:38:45	2021-01-22 12:38:45
36	Cummins	NT.855-G2	Manual	37	100	3000	4	LF670 o 3363,3405/33405,WIX 3022209,WIF2071	0	No	2	208V	1225	\N	43	6581166	30302457	Operativo	1993-06-01	Media	no	\N	2021-01-22 12:39:19	2021-01-22 12:39:19
37	Cummins	NT.855-G2	Manual	37	100	3000	4	LF670 o 3363,3405/33405,WIX 3022209,WIF2071	0	No	2	208V	1225	\N	44	6581166	30302457	Operativo	1993-06-01	Media	no	\N	2021-01-22 12:40:15	2021-01-22 12:40:15
38	Cummins	6CT8-36	Automático	37	100	3000	4	FF5018 / FF200,LF3000,WIX 3022209,WF2060 WIX 24074	0	No	2	208V	1225	\N	45	6411819	276210	Operativo	1999-09-07	Baja	no	\N	2021-01-22 12:52:00	2021-01-22 12:52:00
39	Cummins	6CT8-36	Automático	37	100	3000	4	FF5018 / FF200,LF3000,WIX 3022209,WF2060 WIX 24074	0	No	2	208V	1225	\N	46	6411819	276210	Operativo	1999-09-07	Baja	no	\N	2021-01-22 12:52:34	2021-01-22 12:52:34
40	Cummins	SR100A	Manual	37	100	3000	4	LF3000,3405 / 33405,WIX 3022209,WF2060	1	Si	2	208V	1000	\N	47	507432	no posee	Inoperativo	1999-05-15	Alta	no	\N	2021-01-26 10:51:18	2021-01-26 10:51:18
41	Cummins	NTA855 G2	Automático	37	100	10.000	4	LF670 o 3363, 3405/33405, WIX 3022209, WIF2071	2	Si	2	208V	1500	\N	48	6411984	no posee	Operativo	1993-08-01	Baja	no	\N	2021-01-26 12:20:18	2021-01-26 12:20:18
42	Cummins	KTA50G9	Manual	50	100	116000	2	P1734C	0	No	2	416V	2000	\N	54	5033324	33176158	Operativo	\N	Alta	no	\N	2021-01-27 11:57:28	2021-01-27 11:57:28
43	Cummins	KTA50G9	Manual	50	100	116000	2	P1734C	0	No	2	416V	2000	\N	55	5033324	33176158	Operativo	\N	Alta	no	\N	2021-01-27 11:57:43	2021-01-27 11:57:43
45	Cummins	200DGFM-2698	Automático	19	100	4800	02	100	1	Si	1	416V	1100	\N	59	3135934	no posee	Operativo	S/I	Baja	no	\N	2021-01-28 08:13:50	2021-01-28 08:13:50
46	Cummins	200DGFM-2698	Automático	19	100	4800	02	100	1	Si	1	416V	1100	\N	60	3135934	no posee	Operativo	S/I	Baja	no	El motor ya no cuenta con garantia.	2021-01-28 08:16:35	2021-01-28 08:16:35
69	Cummins	4BT A3.9-G2	Automático	50 Lts	50 Lts	No Posee	4	4	2	Si	2	208V	900	\N	121	3125967	no posee	Operativo	2010-02-01	Alta	no	EL EQUIPO REQUIERE EL SUMINISTRO E INSTALACIÓN DE UN TPC DE 1400 LTS INCLUYENDO LA CONSTRUCCIÓN DE CUBETO.	2021-02-12 12:10:24	2021-02-12 12:10:24
47	VOLVO	TAD1242GE	Automático	57	100	5800	06	06	01	Si	2	416V	1140	\N	64	0005056143	22075817	Operativo	01/01/2000	Baja	no	OPERATIVO	2021-01-29 13:16:31	2021-01-29 13:16:31
48	Cummins	NT-855-G2	Automático	28	100	7500	3	WIX FILTER	1	Si	2	480V	2250	\N	67	3118007	30302460	Operativo	2008-12-08	Alta	no	OPERATIVO	2021-02-01 11:19:31	2021-02-01 11:19:31
49	Cummins	HC434D	Automático	38	100	100	02	3000	01	Si	2	208V	1000	\N	68	3135039	P3401/1	Operativo	1988-01-01	Alta	no	Modelo Filtros-INTRODUZCA CAPACIDAD DEL TPC	2021-02-01 11:27:45	2021-02-01 11:27:45
50	Cummins	NTA855 G2	Inoperativo	37	100	3.000	4	LF670 o 3363, 3405/33405, WIX 3022209, WIF2071	0	No	4	208V	1225	\N	69	507203	NT855G2	Inoperativo	1996-08-01	Baja	no	\N	2021-02-01 11:38:05	2021-02-01 11:38:05
51	Cummins	LTA10-G1	Manual	34	100	7500	2	WIX FILTER	1	Si	2	480V	2250	\N	70	3118008	34883693	Operativo	2008-12-08	Alta	no	OPERATIVO	2021-02-01 11:47:16	2021-02-01 11:47:16
44	Cummins	200DGFM-2698	Automático	19	100	4800	04	100	1	Si	2	416V	1100	\N	58	3135934	no posee	Operativo	S/N	Baja	no	\N	2021-01-28 08:13:17	2021-02-03 12:38:33
52	Cummins	NT855G-1	Automático	38	100	3000	02	01	01	Si	1	208V	950	\N	95	3135039	no posee	Operativo	1900-01-01	Alta	no	\N	2021-02-10 12:53:37	2021-02-10 12:53:37
53	KOHLER	KD12504M	Automático	19	30	3000	02	02	01	No	1	208V	900	\N	96	MV00958718	4514601590	Operativo	1900-01-01	Media	no	\N	2021-02-10 13:05:29	2021-02-10 13:05:29
54	KOHLER	KD12504M	Automático	19	30	3000	02	02	01	No	1	208V	900	\N	97	MV00958718	4514601590	Operativo	1900-01-01	Media	no	\N	2021-02-10 13:08:49	2021-02-10 13:08:49
55	PERKINS	2300	Automático	2000	50	10000	03	03	02	No	2	208V	1100	\N	98	0005053610	070336	Operativo	1900-01-01	Alta	no	\N	2021-02-10 14:19:07	2021-02-10 14:19:07
56	JOHN DEER	40CFR	Inoperativo	19	100	500	4	vl3614, mf1191	1	Si	1	208V	725	\N	99	5047835	no posee	Inoperativo	2008-01-01	Alta	no	\N	2021-02-10 16:45:48	2021-02-10 16:45:48
57	SDMO	D9A2A	Automático	50	50	10000	6	3	1	Si	2	208V	1100	\N	100	3126379/3126379	no posee	Operativo	\N	Alta	no	\N	2021-02-11 10:00:25	2021-02-11 10:02:25
58	KOHLER	DETROIT DIESEL SERIES 60	Automático	150	50	10000	6	3	1	Si	2	480V	1100	\N	101	3139423	no posee	Operativo	\N	Alta	no	\N	2021-02-11 10:53:39	2021-02-11 10:53:39
59	VOLVO	TAD1642GF	Automático	150	50	10000	6	3	1	Si	2	480V	1100	\N	102	3126013	D9A2A 7009162416	Operativo	\N	Alta	no	\N	2021-02-11 11:05:14	2021-02-11 11:05:14
60	VOLVO	TAD1640GE	Automático	50 Lts	50 Lts	10000 Lts	6	1	2	Si	2	416V	1600	\N	103	6580690	7016043943	Operativo	2011-04-01	Alta	no	El equipo tiene la pantalla del controlador dañada y el flotador del tanque sub base dañado. Requiere el suministro de 7500 Lts de combustible.	2021-02-11 14:43:02	2021-02-11 14:43:02
61	VOLVO	TAD1640GE	Automático	50 Lts	50 Lts	10000 Lts	6	1	2	Si	2	416V	1600	\N	104	6580690	7016043943	Operativo	2011-04-01	Alta	no	El equipo tiene la pantalla del controlador dañada y el flotador del tanque sub base dañado. Requiere el suministro de 7500 Lts de combustible.	2021-02-11 14:43:11	2021-02-11 14:43:11
62	VOLVO	TAD1640GE	Automático	50 Lts	50 Lts	10000 Lts	6	1	2	Si	2	416V	1600	\N	105	6580690	7016043943	Operativo	2011-04-01	Alta	no	El equipo tiene la pantalla del controlador dañada y el flotador del tanque sub base dañado. Requiere el suministro de 7500 Lts de combustible.	2021-02-11 14:44:20	2021-02-11 14:44:20
63	Cummins	6CT8.3-G2	Automático	50 Lts	50 Lts	2500 Lts	5	6	1	Si	1	208V	1000	\N	106	506769	no posee	Operativo	1998-05-01	Alta	no	El equipo requiere sustitución de aceite, filtros, batería de arranque de 700 amp en adelante, cargador de batería de 12vdc / 5 amp y suministro de 2500 Lts de combustible.	2021-02-12 08:12:48	2021-02-12 08:12:48
64	Cummins	6CT8.3-G2	Automático	50 Lts	50 Lts	2500 Lts	6	6	1	Si	1	208V	1000	\N	107	506769	no posee	Operativo	1998-05-01	Alta	no	El equipo requiere la sustitución de aceite, filtros, batería de arranque de 700 amp en adelante, cargador de batería de 12 vdc / 5 amp y el suministro de 2500 Lts.	2021-02-12 08:37:34	2021-02-12 08:37:34
65	JOHN DEER	PE3029T217128	Automático	50 Lts	50 Lts	2500 Lts	3	3	1	Si	1	208V	700	\N	116	506700	no posee	Operativo	2003-06-01	Alta	no	El equipo requiere sustitución de batería de arranque de 700 amp y cargador de batería de 12 vdc / 5 amp.	2021-02-12 10:36:16	2021-02-12 10:36:16
66	Cummins	NTA855 - G2A	Automático	50 Lts	50 Lts	10000 Lts	5	3	1	Si	2	208V	1110	\N	117	5059408	S015447	Operativo	2011-07-17	Alta	no	El equipo tiene dañado el flotador del tanque sub base y requiere el suministro de 10000 Lts de combustible.	2021-02-12 11:06:16	2021-02-12 11:06:16
67	Cummins	4BT3.9-G2	Automático	50 Lts	50 Lts	2500 Lts	5	5	1	Si	1	208V	700	\N	119	509305	no posee	Operativo	1998-05-01	Alta	no	El equipo requiere una bateria de arranque de 700 amp, la cual le fue hurtada, la sustitución de aceite, filtros y cargador de batería de 12 vdc /5 amp y el suministro de 2000 Lts de combustible.	2021-02-12 11:17:41	2021-02-12 11:17:41
68	Cummins	4BT3.9-G2	Automático	50 Lts	50 Lts	2500 Lts	5	5	1	Si	1	208V	700	\N	120	509305	no posee	Operativo	1998-05-01	Alta	no	El equipo requiere una bateria de arranque de 700 amp, la cual le fue hurtada provisionalmente se le instaló una de 100 amp, la sustitución de aceite, filtros y cargador de batería de 12 vdc /5 amp y el suministro de 2000 Lts de combustible.	2021-02-12 11:20:32	2021-02-12 11:20:32
70	JOHN DEER	4024HF285	Inoperativo	50 Lts	50 Lts	2500 Lts	2	2	1	Si	1	208V	900	\N	122	509300	no posee	Inoperativo	2011-06-01	Alta	no	EL EQUIPO PRESENTA DAÑOS EN COMPONENTES INTERNOS DEL MOTOR.	2021-02-12 15:40:17	2021-02-12 15:40:17
108	CATERPILLAR	sr-4	Automático	100	100	6000	2	00	1	Si	2	208V	1100	\N	195	000000	9ff00981	Operativo	01/01/2021	Alta	no	\N	2021-03-22 17:29:29	2021-03-22 17:29:29
71	Cummins	4BT A3.9-G2	Inoperativo	45 Lts	45 Lts	6500 Lts	4	4	No Posee	No	2	208V	1100	\N	123	No Posee	no posee	Inoperativo	2010-02-01	Alta	no	EL EQUIPO REQUIERE MANTENIMIENTO  AL SISTEMA DE INYECCIÓN , ARRANQUE DE 24V Y REEMPLAZO DE DOS BATERÍAS DE ARRANQUE DE 1100 AMP.	2021-02-17 08:26:21	2021-02-17 08:26:21
72	PERKINS	DK51278 / U1925705	Inoperativo	50 Lts	50 Lts	10000	4	4	0	No	1	208V	750	\N	124	5059294	2502-1500	Vandalizado	2008-01-08	Alta	no	El EQUIPO FUE HURTADO TOTALMENTE.	2021-02-17 15:27:02	2021-02-17 15:27:02
73	DEUTZ	F6L912	Inoperativo	70 Lts	70 Lts	18000	4	4	0	No	1	208V	900	\N	125	2191373	8267662	Vandalizado	1992-12-01	Alta	no	EL EQUIPO FUE DESINCORPORADO. EL MOTOR DE ESTE EQUIPO FUE TRASLADADO A LA CENTRAL DIGITAL DE PUERTO PIRITU -  ANZOÁTEGUI.	2021-02-17 15:40:27	2021-02-17 15:40:27
74	Cummins	S041009	Inoperativo	100 Lts	50 Lts	6500 Lts	4	4	1	Si	1	208V	900	\N	126	506796	392161	Vandalizado	2006-06-01	Alta	no	EL EQUIPO PRESENTA DAÑOS INTERNOS EN EL MOTOR, TIENE EL BLOQUE ROTO, LE FALTA  ARRANQUE, ALTERNADOR, BOMBA DE AGUA E INYECCIÓN Y BREAKER BOMBIN.	2021-02-17 15:53:12	2021-02-17 15:53:12
75	IVECO	F5CE0455A*B001	Automático	15	10	528	3	33357;WEB W-3900; 8050800	1	Si	1	208V	900	\N	128	2027889	072367	Operativo	\N	Baja	no	\N	2021-02-23 11:36:39	2021-02-23 11:36:39
76	IVECO	F5CE0455A*B001	Automático	15	10	528	3	33357;WEB W-3900; 8050800	1	Si	1	208V	900	\N	129	2027889	072367	Operativo	\N	Baja	no	\N	2021-02-23 11:38:28	2021-02-23 11:38:28
77	IVECO	F2CE9685A-E	Automático	30	20	4800	4	522878; 504836; 529643;8041419	1	Si	2	416V	1100	\N	130	511323	019998	Operativo	2014	Baja	no	TANQUE INTERNO DEL MOTOR 1000 LITROS; NUMERO PARTE FILTRO GASOIL 504179764 ; NUMERO DE PARTE FILTRO ACEITE 504199551;	2021-02-23 12:06:52	2021-02-23 12:06:52
78	PERKINS	MGTP75-SS-P/AA	Automático	15	10	5500	3	CS1416; 26560201; 1106D-E66TA	1	Si	1	208V	1100	\N	131	64000393	2918900	Operativo	2015	Baja	no	TANQUE DE GASOIL INTERNO  500 LITROS	2021-02-23 12:22:06	2021-02-23 12:22:06
79	PERKINS	1	Automático	25	20	3000	4	4	1	Si	1	208V	800	\N	132	SIINFORMACIOM	no posee	Operativo	2018	Alta	no	\N	2021-02-23 14:06:13	2021-02-23 14:06:13
80	GENESAL	SIN INFORMACION	Automático	25	20	3000	4	4	1	Si	1	208V	800	\N	133	SIINFORMACIOM	no posee	Operativo	2018	Alta	no	\N	2021-02-23 14:10:14	2021-02-23 14:10:14
81	JOHN DEER	3029TF120	Manual	25	20	3000	4	4	1	Si	1	208V	800	\N	134	5049542	CD3029B022607	Operativo	2005	Alta	no	ESTA EN STANDBY SOLO EN CASO QUE FALLE EL MG PRINCIPAL.	2021-02-23 14:17:51	2021-02-23 14:17:51
82	LISTER	HR631V/05	Inoperativo	25	00	6000	4	4	1	Si	1	208V	800	\N	137	3142913	LO2939/005	Inoperativo	1985	Media	no	NECESITA OVERHAULT	2021-02-23 14:27:54	2021-02-23 14:27:54
83	LISTER	HR631V/05	Inoperativo	25	00	6000	4	4	1	Si	1	208V	800	\N	138	3142916	LO2939/006	Inoperativo	1985	Media	no	BOMBIN Y CAMBIO DE ACEITE	2021-02-23 14:32:24	2021-02-23 14:32:24
84	LISTER	HR631V/05	Inoperativo	25	00	6000	4	4	1	Si	1	208V	800	\N	139	5048998	LO2939/007	Inoperativo	1985	Media	no	OVERHAULT	2021-02-23 14:53:10	2021-02-23 14:53:10
85	Cummins	6BT5,9-62	Automático	50	50	2500	4	4	1	Si	1	208V	900	\N	140	3142795	21316681	Operativo	2000	Alta	no	\N	2021-02-24 14:01:47	2021-02-24 14:01:47
86	Cummins	6CT83-G	Automático	50	50	2500	4	4	1	Si	1	208V	900	\N	141	5049165	21320369	Operativo	2000	Alta	no	\N	2021-02-24 14:38:20	2021-02-24 14:38:20
87	JOHN DEER	4039TFOCX1	Automático	25	20	2500	4	4	1	Si	1	208V	800	\N	142	5049430	CD4039T402938	Operativo	1998	Alta	no	\N	2021-02-24 14:43:59	2021-02-24 14:43:59
88	LOMBARDINE	LDW1404	Automático	10	10	600	4	4	1	Si	1	208V	800	\N	143	SIN INFORMACION	SIN INFORMACION	Operativo	2008	Media	no	\N	2021-02-25 13:53:30	2021-02-25 13:53:30
89	PERKINS	SIN INFORMACION	Inoperativo	12	12	1500	4	4	1	Si	1	208V	800	\N	144	SIN INFORMACION	SIN INFORMACION	Inoperativo	2018	Media	no	NO TIENE BATERIA DE ARRANQUE, TRANSFER DAÑADO	2021-02-25 14:14:40	2021-02-25 14:14:40
90	PERKINS	SIN INFORMACION	Inoperativo	12	12	1500	4	4	1	Si	1	208V	800	\N	145	SIN INFORMACION	SIN INFORMACION	Inoperativo	2018	Media	no	NO TIENE BATERIA, TRANSFER DAÑADO	2021-02-25 14:18:03	2021-02-25 14:18:03
91	PERKINS	SIN INFORMACION	Automático	12	12	1000	4	4	1	Si	1	208V	800	\N	146	SIN INFORMACION	SIN INFORMACION	Operativo	2016	Media	no	\N	2021-02-25 14:23:59	2021-02-25 14:23:59
92	PERKINS	SIN INFORMACION	Automático	12	12	2200	4	4	1	Si	1	208V	800	\N	147	SIN INFORMACION	SIN INFORMACION	Operativo	2016	Media	no	\N	2021-02-25 14:32:19	2021-02-25 14:32:19
94	JOHN DEER	R11G194	Manual	50	50	3400	3	RE62418	0	No	1	208V	800	\N	150	3138037	CD4039D411288	Operativo	2000-08-03	Baja	no	\N	2021-02-26 13:18:17	2021-02-26 13:18:17
95	JOHN DEER	R11G194	Automático	50	00	3400	2	70316A-RE62418	00	No	1	208V	800	\N	153	3138037	CD4039D411288	Operativo	2000-08-03	Baja	no	\N	2021-03-01 12:32:41	2021-03-01 12:32:41
93	JOHN DEER	R11G194	Manual	50	50	3400	3	70316A	0	No	1	208V	800	\N	149	3138037	CD4039D411288	Operativo	2000-08-03	Baja	no	\N	2021-02-26 13:11:58	2021-03-01 13:13:11
99	CATERPILLAR	SR-4	Automático	57	100	6000	02	02	01	Si	2	416V	1000	\N	160	3135827	no posee	Operativo	2000-01-01	Alta	no	\N	2021-03-18 10:37:17	2021-03-18 10:37:17
100	PETBOW	65891	Automático	57	100	10000	03	03	00	No	2	416V	1100	\N	163	3135806	65891BN200AE	Operativo	2000-03-17	Alta	no	\N	2021-03-18 11:20:08	2021-03-18 11:20:08
96	JOHN DEER	R11G194	Automático	50	00	3400	2	70316-62418	00	No	1	208V	800	\N	154	3138037	CD4039D411288	Operativo	2000-08-03	Baja	no	\N	2021-03-01 12:43:41	2021-03-01 13:30:16
97	JOHN DEER	RG6081A171347	Manual	150	00	4800	3	33375	1	Si	2	208V	800	\N	155	3138108	6081AF001	Inoperativo	2006-08-01	Alta	no	Es necesario la reparación del MG por personal especializado en el área, ya que respalda una de las centrales mas importantes del Estado merida	2021-03-01 13:11:38	2021-03-01 13:33:28
101	PETBOW	BN200AE	Automático	38	100	10000	03	SIN INFORMACION	02	No	2	416V	1100	\N	164	3135806	065891	Operativo	2021-03-01	Alta	no	\N	2021-03-18 11:30:09	2021-03-18 11:30:09
98	Seleccione una marca	TD232V12	Manual	00	50	2600	2	H18W01 - H90WK	2	Si	2	208V	800	\N	156	000000	2321202709	Operativo	1980-06-15	Alta	no	Equipo con mas de 40 años de servicio, se trabaja manualmente	2021-03-01 13:25:53	2021-03-16 10:54:37
102	PETBOW	BN200AE	Automático	38	100	10000	3	03	02	No	2	416V	1100	\N	165	3135806	065891	Operativo	2021-03-01	Alta	no	\N	2021-03-18 11:56:57	2021-03-18 11:56:57
103	PETBOW	BN200AE	Automático	38	100	10000	03	03	02	No	2	416V	1100	\N	166	3135806	065891	Operativo	2021-01-01	Alta	no	\N	2021-03-18 12:10:34	2021-03-18 12:10:34
104	PETBOW	bn200ae	Automático	38	100	10000	03	03	02	No	2	416V	1100	\N	168	3135806	065891	Operativo	2007-03-17	Alta	no	operativo	2021-03-18 12:37:57	2021-03-18 12:37:57
105	PETBOW	ji3341	Manual	15000	79999	8787877	6	modleo	3	Si	3	416V	345	\N	190	3567888	no posee	Operativo	2021-03-22	Media	si	sin observacion	2021-03-22 11:56:47	2021-03-22 11:56:47
106	MODASA	generico	Manual	300	122	3000	2	generic	2	No	1	208V	200	\N	191	309455	no posee	Inoperativo	2021-03-22	Media	si	Registro de prueba	2021-03-22 12:59:23	2021-03-22 12:59:23
107	PETBOW	generico	Manual	333	2222	22222	21121	generic	24	Si	1	208V	200	\N	193	305099	no posee	Vandalizado	2021-03-22	Baja	si	Observacion de prueba	2021-03-22 13:01:56	2021-03-22 13:01:56
\.


--
-- Data for Name: multimedia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.multimedia (id, archivo, tipo, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: nodoopsut; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.nodoopsut (id, nombre, codigo, tipo, ubicacion, latitud, longitud, stat_nod, estado_id, region_id, personal_id, observacion, created_at, updated_at) FROM stdin;
1	ACHAGUAS	08OSUT APU 026	1	Calle Arismendi entre calles 4 y 5	7,780165	-68,2238	1	10	3	1	NO DISPONIBLE	\N	\N
3	Agua Blanca	08OSUT POR 161	2	Agua Blanca.Calle 10 entre Avenidas Nº 3 y 4.En Terreno	9,660578	-69,108194	1	16	4	1	NO DISPONIBLE	\N	\N
5	Aguada Grande	08OSUT LAR 131	1	Calle vía El Cementerio diagonal a la Plaza Bolívar Aguada Grande	10,586639	-69,486048	1	17	4	1	NO DISPONIBLE	\N	\N
6	Veladero	08OSUT MON 155	2	Calle 5 Julio con avenida Guárico	9,427615	-64,477912	1	26	7	1	NO DISPONIBLE	\N	\N
7	Anaco	08OSUT ANZ 003	2	Intersección de la calle Sucre y calle 1, con Libertador,	9,396829	-69,383688	1	28	7	1	NO DISPONIBLE	\N	\N
8	Aparición	08OSUT POR 166	1	Desde Coro: se toma la T-4 con rumbo Sur, vía Churuguara, por 51km hasta el poblado de La Peña. Desde San Luis se sigue rumbo Suroeste unos 6,4km hasta la Troncal 4 y de ahi rumbo Norte vía Coro por 4,4km	9,680433	-68,938936	1	16	4	1	NO DISPONIBLE	\N	\N
9	Apartaderos	08OSUT COJ 069	1	Apurito.Carretera Nacional vía aeropuerto al Lado del cementerio, Apurito. En Terreno	7,922203	-68,480095	4	12	3	1	NO DISPONIBLE	\N	\N
10	Apurito	08OSUT APU 027	1	CALLE MIRANDA DE ARAGUA DE BARCELONA EDIFICIO CANTV	9,451045	-64,831577	1	10	3	1	NO DISPONIBLE	\N	\N
11	Aragua de Barcelona	08OSUT ANZ 004	1	Central Acarigua Centro. Cll. 29 entre Avs. 32 y 33	9,556654	-69,206377	1	28	7	1	NO DISPONIBLE	\N	\N
12	ARAURE (Acarigua Centro)	08OSUT POR 162	2	S/DOCUMENTO: AV. MARIA SANCHERA CON CALLE EL GUAPO, ARISMENDI, MUNICIPIO ARISMENDI, EDO BARINAS  S/LEVANTAMIENTO:  Av. ARISMENDI CON Cll.8 DE DICIEMBRE	8,478069	-68,369751	1	16	4	1	NO DISPONIBLE	\N	\N
13	Arismendi	08OSUT BAR 050	1	Ubicado en la comunidad indigena Bajo Hondo a 19 Km, al norte de El Tigre y a 29 Km al sur de Cantaura.	9,048286	-64,301991	1	3	1	1	NO DISPONIBLE	\N	\N
14	Bajo Hondo	08OSUT ANZ 019	1	Sector Las Trincheras, a un lado del Mercal	7,25043	-70,245865	1	28	7	1	NO DISPONIBLE	\N	\N
15	Banco Largo	08OSUT APU 036	1	Sector Barbasco, Municipio Pao de San Juan Bautista Parroquia el Pao, frente a la Carretera Nacional que conduce desde Tinaco hacia Baúl, Estado Cojedes	9,053246	-68,15367	1	10	3	1	NO DISPONIBLE	\N	\N
16	Barbasco	08OSUT COJ 072	1	Avenida Bolívar con Calle San Félix	10,134958	-64,685421	1	12	3	1	NO DISPONIBLE	\N	\N
17	BARCELONA	08OSUT ANZ 024	2	CA. 30 ENTRE CR 22 Y 23	10,069556	-69,321363	1	28	7	1	NO DISPONIBLE	\N	\N
18	Barquisimeto	08OSUT LAR 118	2	Saliendo al Norte de Biscucuy por la Troncal 7 hasta empalmar con la vía que conduce a Campo Elías, se sigue por esta vía unos 2,4km aproximadamente	9,389778	-70,020806	1	17	4	1	NO DISPONIBLE	\N	\N
19	Biscucuy	08OSUT POR 167	1	Bobare.Intersección de carrera 2 y calle 4.En Terreno	10,267357	-69,474705	1	16	4	1	NO DISPONIBLE	\N	\N
20	Bobare	08OSUT LAR 119	1	3ra Transversal con Calle Peñalver Urb. Boca de Uchire	10,133442	-65,427217	1	17	4	1	NO DISPONIBLE	\N	\N
21	Boca de Uchire	08OSUT ANZ 023	2	frente a calle 2, entre calles 3 y 4, bocono edo. Táchira	8,483814	-71,921943	1	28	7	1	NO DISPONIBLE	\N	\N
22	Boconó (Táchira)	08OSUT TAC 187	1	Calle Bolívar entre Av. Sucre y Miranda, centro de Bocono	9,244391	-70,271176	1	1	1	1	NO DISPONIBLE	\N	\N
23	Bocono (Trujillo)	08OSUT TRU 189	2	la Calle 5, Urbanización Villa Bruzual, Municipio Muñoz, Estado Apure	8,047351	-69,336171	1	2	1	1	NO DISPONIBLE	\N	\N
24	Bruzual	08OSUT APU 033	2	Calle Coronel Miguel y Cegarra	9,418639	-70,269513	1	10	3	1	NO DISPONIBLE	\N	\N
25	Burbusay	08OSUT TRU 190	1	Prolongación de la calle El Carmen	7,643012	-66,252325	1	2	1	1	NO DISPONIBLE	\N	\N
26	Cabruta	08OSUT GUA 106	2	Calle Gregorio Segundo, Guarecuco Con calle Concordia	11,143892	-69,617964	2	13	3	1	NO DISPONIBLE	\N	\N
27	Cabure	08OSUT FAL 093	2	Centro poblado Cachipo, Calle Principal, a 120m de la carretera Nacional que conduce de Maturín a Caripito. al lado de la estación de bombeo de agua y tanque de PDVSA	9,913384	-63,150765	1	15	4	1	NO DISPONIBLE	\N	\N
28	Cachipo	08OSUT MON 160	1	Entre la calles 5 de Julio y El Rincón con Av. Aeropuerto, Caicara del Orinoco	7,6469	-66,1711	1	26	7	1	NO DISPONIBLE	\N	\N
29	Caicara del Orinoco	08OSUT BOL 055	1	Carrera 11 Con Calle 5 Calabozo	8,933628	-67,426177	1	21	5	1	NO DISPONIBLE	\N	\N
30	Calabozo	08OSUT GUA 101	2	Entre Calle Soublete y Carabobo, frente a la Calle Junín	8,103935	-67,607236	1	13	3	1	NO DISPONIBLE	\N	\N
31	Camaguán	08OSUT GUA 097	2	Estación terrena Camatagua	9,81557	-66,88428	1	13	3	1	NO DISPONIBLE	\N	\N
32	Camatagua	08OSUT ARA 045	2	Desde Ciudad Bolívar: se toma la T-19 rumbo Oeste vía Caicara del Orinoco, por 319km hasta el poblado Cuchivero. Desde Caicara del Orinoco: tomar la T-12 rumbo Sur por 6,5km hasta interceptar la T-19 de ahí rumbo Sureste	9,650163	-68,818477	1	14	3	1	NO DISPONIBLE	\N	\N
33	Camoruco	08OSUT COJ 077	1	Sector Río Negro, entre las poblaciones de Cachamana a 15Km al Noroeste y Río Negro a 400m al Sureste. En terrenos para planta procesadora de Yuca	9,664575	-72,519017	1	12	3	1	NO DISPONIBLE	\N	\N
35	Cantaura	08OSUT ANZ 020	2	Sector Caño Benito frente a carretera Nal. q conduce desde Tinaco hacia El Baúl.	9,330786	-68,1562	1	28	7	1	NO DISPONIBLE	\N	\N
36	Caño Benito	08OSUT COJ 073	1	Frente a la calle Brisas de Campo Alegre	10,428573	-66,268227	1	12	3	1	NO DISPONIBLE	\N	\N
37	Capaya	08OSUT MIR 141	2	Sector Capital Uverito, al lado de la Flia. Arsiniega, frente a Carretera Nal. que\tconduce  desde Uverito a la Negra y Camaguán	8,128843	-67,481564	1	9	2	1	NO DISPONIBLE	\N	\N
38	Capital Uverito	08OSUT GUA 096	1	Ubicado en Cardoncito a 41 Km desde el peaje de El Sombrero via El Sombrero-Valle de la Pascua	9,392881	-66,610174	1	13	3	1	NO DISPONIBLE	\N	\N
4	Agua Verde	08OSUT GUA 113	1	Sector Agua Verde, al lado de la Bodega de la Flia. Oviedo, frente a la Carretera Nal.\tque conduce  desde Guayabal a Cazorla.	7,923047	-67,24948	7	13	3	1	Fuera de servicio	\N	2021-01-19 10:01:00
34	Campo Bernal	08OSUT ZUL 202	1	Calle freites C/C Guevara Rojas	9,308359	-64,361159	1	30	6	1	NO DISPONIBLE	\N	2021-02-02 13:02:59
40	Caripito	08OSUT MON 154	2	CALLE CARABOBO ESQUINA CALLE RIVAS	10,175459	-70,082759	1	26	7	1	NO DISPONIBLE	\N	\N
41	Carora	08OSUT LAR 128	2	ENTRE AV. CALVARIO Y CARABOBO CON CALLES ARAURE Y QUEBRADA\tHONDA. PARROQUIA SANTA  ROSA	10,667028	-63,245811	1	17	4	1	NO DISPONIBLE	\N	\N
42	Carúpano	08OSUT SUC 174	2	Intersección Calle Santa Marta con Calle Bolivia	10,502291	-63,419058	1	29	7	1	NO DISPONIBLE	\N	\N
43	Casanay	08OSUT SUC 168	2	Av. Principal, esquina calle San Jose, Casigua	8,746348	-72,515401	1	29	7	1	NO DISPONIBLE	\N	\N
45	Caucagua	08OSUT MIR 142	1	Calle Bolivar7/Calle Girardot. FRENTE A LA CARRETERA QUE CONDUCE A\tGUAYABAL, A 400 MTS. DE LA PLAZA BOLIVAR	8,012746	-67,008306	1	9	2	1	NO DISPONIBLE	\N	\N
47	Chacaracual	08OSUT SUC 171	1	Calle barranca entre calle el esfuerzo y calle alegre	8,936296	-70,771243	1	29	7	1	NO DISPONIBLE	\N	\N
48	Chachopo	08OSUT MER 137	2	Chaguaramas.Carretera Nacional entre calle Zaraza y calle Bernardo Leal. En Terreno	9,337361	-66,25361	1	4	1	1	NO DISPONIBLE	\N	\N
49	Chaguaramas	08OSUT GUA 098	1	Frente a la Calle Bosset, con esquina de la Calle Bolívar	8,49159	-71,522929	1	13	3	1	NO DISPONIBLE	\N	\N
50	Chiguará	08OSUT MER 139	2	CA. MONAGAS ENTRE CA. BOLIVAR Y CA. MUNICIPAL	10,813148	-69,537692	1	4	1	1	NO DISPONIBLE	\N	\N
51	Churuguara	08OSUT FAL 085	2	Central 5 de Julio. Av.5 de Julio con Av.Táchira,Ciudad Bolivar, Municipio Heres. Edo.\tBolivar. En Terreno	8,137325	-63,546015	1	15	4	1	NO DISPONIBLE	\N	\N
52	CIUDAD BOLÍVAR	08OSUT BOL 064	2	Calle General Cancio Gonzalez con Calle Comercio	9,940286	-65,166578	1	21	5	1	NO DISPONIBLE	\N	\N
53	Clarines	08OSUT ANZ 013	2	COLONCITO CENTRO CARRERA 5 ENTRE CALLES 6 Y 7	8,323505	-72,090288	1	28	7	1	NO DISPONIBLE	\N	\N
54	Coloncito	08OSUT TAC 186	2	Saliendo al sur de El Tigre, se toma rumbo sureste la Troncal 16 por 45Km hasta sector La Viuda desde ahí se toma rumbo Este, vía Edo. Monagas, una carretera asfaltada por 5Km. Hay un tanque elevado y una cancha deportiva frente al terreno	8,738365	-63,50609	1	1	1	1	NO DISPONIBLE	\N	\N
55	Coloradito	08OSUT ANZ 009	1	Calle Bolivar, Esq. Calle Talavera Frente a la Plaza Falcon	11,408534	-69,676165	1	28	7	1	NO DISPONIBLE	\N	\N
56	Coro	08OSUT FAL 091	2	Carretera que conduce de Calabozo a San Fernando de Apure	8,605451	-67,56824	1	15	4	1	NO DISPONIBLE	\N	\N
57	Corozo Pando	08OSUT GUA 102	2	CALLE BOLIVAR CON CALLE ZAMORA	10,160061	-66,886641	1	13	3	1	NO DISPONIBLE	\N	\N
58	Cúa	08OSUT MIR 152	2	Centro poblado El Duvisi, a 143km aproximadamente al Norte del centro de Barquisimeto, Edo. Lara por la Troncal 4 o a 19,6km al sureste de\tChuruguara Edo. Falcón	10,782231	-69,392389	1	9	2	1	NO DISPONIBLE	\N	\N
59	Ortiz	08OSUT GUA 110	1	Localidad El Alambre, a 19,5Km al Noroeste de Clarines y a 22Km alSureste de Boca de Uchire. En terrenos propiedad de Providencia Aray al Este de Caserío El Alambre	9,998738	-65,317148	1	13	3	1	NO DISPONIBLE	\N	\N
60	Duvisi	08OSUT FAL 086	1	CALLE SIMÓN RODRIGUEZ	9,338266	-70,14455	1	15	4	1	NO DISPONIBLE	\N	\N
61	El Alambre	08OSUT ANZ 014	1	Carretera Panamericana a 10,2km al noreste de Sabana Grande vía Carora. El\tterreno esta al lado de la Bloquera Socialista El Batatillo	9,735101	-70,433857	1	28	7	1	NO DISPONIBLE	\N	\N
62	El Batatal	08OSUT TRU 191	2	El Baul entre calles Bolívar y Laurencio	8,962579	-68,291518	1	2	1	1	NO DISPONIBLE	\N	\N
63	El Batatillo	08OSUT TRU 192	1	Vía La Pedrera - Guasdualito, a 30Km al Sureste de la alcabala GN de La Pedrera. Entre Estación CANTV y planta de tratamiento de agua, frente al taller Monsalve	7,467322	-71,307458	1	2	1	1	NO DISPONIBLE	\N	\N
64	El Baúl	08OSUT COJ 071	1	Av. Bolívar en el centro poblado de Maripa ubicado a 60km al Este de Santa\tRosalía por la Troncal 19	9,533919	-64,655888	1	12	3	1	NO DISPONIBLE	\N	\N
65	Puerto Nuevo	08OSUT BOL 056	2	Terreno uicado en El Corozo a 35 Km desde la alcabala de Calabozo, via Calabozao-Dos Caminos	9,224024	-67,380062	1	21	5	1	NO DISPONIBLE	\N	\N
66	El Cantón	08OSUT BAR 049	1	CARRETERA ROSARIO, RIO DE ORO, SECTOR ELCRUCE. FINCA SAN ISIDRO.	9,188109	-72,659851	1	3	1	1	NO DISPONIBLE	\N	\N
67	El Caro	08OSUT ANZ 005	1	FRENTE A LA CALLE PRINCIPAL AL LADO DEL GRUPO ESCOLAR ENMA\tSILVEIRA, EL EMPEDRADO.	9,866924	-70,282763	1	28	7	1	NO DISPONIBLE	\N	\N
68	El Corozo	08OSUT GUA 111	1	Sector ubicado a 10 km de la Carretera Nacional que conduce desde Cupira -Boca de Uchire , Estado Miranda.	10,167835	-65,652531	5	13	3	1	NO DISPONIBLE	\N	\N
70	El Empedrado	08OSUT LAR 129	1	Se localiza a 23,4km al Norte de Bobare por la Troncal 4 que conduce de Barquisimeto (LAR) hacia Churuguara (FAL)	10,695288	-69,263213	1	17	4	1	NO DISPONIBLE	\N	\N
72	El Guapo	08OSUT MIR 144	1	Sector El Manguito, a 27Km al Sureste de Pariaguan y 27Km al Norte de San Diego de\tCabrutica. Frente al fundo Rancho La Marisela	8,674749	-64,873189	1	9	2	1	NO DISPONIBLE	\N	\N
73	El Limón	08OSUT LAR 132	1	Intersección de las calles Independencia y Constitución	9,638359	-68,128369	1	17	4	1	NO DISPONIBLE	\N	\N
39	Cardoncito	08OSUT ARA 047	1	Campo Libertad, Avenida Nueva Jerusalén de la Población de Caripito	10,117923	-63,094363	3	14	3	1	No esta declarado en el gestor	\N	2021-01-19 09:24:36
74	El Mamón	08OSUT FAL 080	1	Desde Coro se toma la T-4 rumbo Sur, vía Churuguara por 106km hasta El Paují. Desde Churuguara, sector el Calvario se toma la T-4, rumbo Oeste, vía Coro por unos 9,5km hasta poblado de El Paují	10,81193	-69,620742	2	15	4	1	bypasss	\N	2021-01-19 09:37:12
44	Casigua	08OSUT ZUL 199	2	Calle El Calvario, entre el Placer y la Colina.	10,28302	-66,377049	1	30	6	1	NO DISPONIBLE	\N	2021-02-02 13:03:31
46	Cazorla	08OSUT GUA 114	2	Centro poblado de San Francisco de Chacaracual, calle Principal, a 150m de la plaza Bolívar. al lado de la casa de la Sra. Doris Lugo	10,655548	-63,031408	7	13	3	1	fuera de servicio	\N	2021-01-19 10:01:22
69	El Cruce	08OSUT ZUL 200	2	CALLE REAL, ENTRE LA AV. PPAL Y CALLE PALO VERDE, POBLACIÓN EL\tGUAPO, FRENTE A PLAZA EL GUAPO.	10,145637	-65,971328	1	30	6	1	NO DISPONIBLE	\N	2021-02-02 13:03:50
75	El Pao	08OSUT COJ 074	2	ENTRE LAS CALLES ESPAÑA Y BOLIVAR CON CALLE LAS MARGARITAS.	10,54748	-63,154752	1	12	3	1	NO DISPONIBLE	\N	\N
76	El Paují	08OSUT FAL 087	1	Calle La Pica 2, frente a calle 4,	10,363387	-63,401709	1	15	4	1	NO DISPONIBLE	\N	\N
77	El Pilar	08OSUT SUC 173	2	CT. PORVENIR  - SANTA INES (FRENTE AL AMBULATORIO LA GARZA)	10,554402	-69,384158	1	29	7	1	NO DISPONIBLE	\N	\N
78	El Poblado	08OSUT SUC 178	2	A 35km aproximadamente saliendo de El Cantón vía Guasdualito. Frente a la bodega El Rubio	7,285875	-71,105567	1	29	7	1	NO DISPONIBLE	\N	\N
79	El Porvenir	08OSUT LAR 133	1	Vía que conduce de Achaguas al Saman frente a la manga de coleo	7,912908	-68,690775	1	17	4	1	NO DISPONIBLE	\N	\N
81	El Samán de Apure	08OSUT APU 028	1	Calle Alegría con calle Bella Vista	9,382876	-67,052636	1	10	3	1	NO DISPONIBLE	\N	\N
82	El Socorro	08OSUT GUA 099	2	Intersección de la Segunda Carrera Norte con calle Norte 6 y Av. Fco. de Miranda	8,889623	-64,252843	1	13	3	1	NO DISPONIBLE	\N	\N
83	El Sombrero	08OSUT GUA 105	2	Centro poblado El Tocuyo, Calle 2 a 300m transversal a la Av. Circunvalación,	9,797107	-69,79484	1	13	3	1	NO DISPONIBLE	\N	\N
84	El Tigre	08OSUT ANZ 025	2	Centro poblado Las Mercedes, a 41km al Sureste de Churuguara por la Troncal 4.	10,794643	-69,492317	1	28	7	1	NO DISPONIBLE	\N	\N
85	El Tocuyo	08OSUT LAR 124	1	Intersección de la Av. 14, con la Calle 9, Barrio la Inmaculada, Parroquia Presidente Paez	8,61694	-71,649961	1	17	4	1	NO DISPONIBLE	\N	\N
86	El Tural	08OSUT FAL 090	1	Calle Teo Galíndez con calles 12 y 13	7,061244	-69,503882	1	15	4	1	NO DISPONIBLE	\N	\N
87	El Vigía	08OSUT MER 135	2	Desde E/S El Cruce, al Norte de Flor de Patria, seguir rumbo Norte vía\tMonay unos 908m	9,471571	-70,470502	1	4	1	1	NO DISPONIBLE	\N	\N
88	Elorza	08OSUT APU 030	2	Entre las calles 6 y 7, calles 16 y 17	9,043584	-69,750812	1	10	3	1	NO DISPONIBLE	\N	\N
89	Flor de Patria	08OSUT TRU 194	1	TERRENOS DE LA EMISORA DE RADIO LA VOZ DE FLORENTINO, CARRERA 1, PARALELA A LA TRONCAL 19, AV PPAL DE GUARATARO	7,444222	-64,807444	1	2	1	1	NO DISPONIBLE	\N	\N
90	GUANARE	08OSUT POR 163	2	PUEBLO ARRIBA, FRENTE A LA PLAZA BOLIVAR CON CALLE AMBROSIO Y BOLIVAR.	10,471352	-66,615655	1	16	4	1	NO DISPONIBLE	\N	\N
91	Guarataro	08OSUT BOL 066	2	Se localiza a 1,5km del centro poblado Guárico vía El Tocuyo, dentro de la\tHda. del Sr. Cándido Escalona	9,638139	-69,781972	1	21	5	1	NO DISPONIBLE	\N	\N
92	Guarenas	08OSUT MIR 149	2	Carretera Mantecal - Bruzual, en la salida de Guaritico vía Bruzual, al Sur de la\tescuela de Guaritico	7,704932	-69,33178	1	9	2	1	NO DISPONIBLE	\N	\N
93	Guarico	08OSUT LAR 125	2	Intersección de la carrera Mariño y calle Sucre	7,242698	-70,733249	1	17	4	1	NO DISPONIBLE	\N	\N
94	Guaritico	08OSUT APU 211	1	Sector Guatire a 17Km al Sur de San Diego de Cabrutica por la carretera que conduce a Mapire al Sur del Edo. Anzoátegui. Al lado de la escuela Guatire	8,272877	-64,874655	1	10	3	1	NO DISPONIBLE	\N	\N
95	Guasdualito	08OSUT APU 032	2	FRENTE A CALLE PAEZ CON CALLE REVOLUCION.	8,003637	-67,399558	1	10	3	1	NO DISPONIBLE	\N	\N
96	Guatire	08OSUT ANZ 015	1	FRENTE A LA CALLE CONCEPCION, ENTRE CALLES TRINCHERAS Y PAGALLOS,\tGUIRIA, MUN VALDEZ, EDO SUCRE	10,574798	-62,300631	5	28	7	1	NO DISPONIBLE	\N	\N
97	Guayabal	08OSUT GUA 115	2	Intersección de las Calles Bolívar y Cedeño	10,569282	-62,582094	1	13	3	1	NO DISPONIBLE	\N	\N
98	Güiria	08OSUT SUC 179	2	Centro poblado Joaquin del Tigre, calle El Parque, a 180m de la carretera Nacional. al\tlado de la Unidad Educativa Gilfredo Ríos	8,971695	-63,273319	1	29	7	1	NO DISPONIBLE	\N	\N
99	Irapa	08OSUT SUC 177	2	Desde Ciudad Bolívar: se toma la T-19 rumbo Oeste vía Caicara del Orinoco,\tpor 57,8km hasta el sector La Carolina.	7,735908	-63,903608	1	29	7	1	NO DISPONIBLE	\N	\N
100	Joaquín del Tigre	08OSUT MON 157	1	Centro poblado La Centellita entre calle El Agua con Calle 7, a 80m carretera nacional\tvía Maturín - El Tigre. Al lado del ambulatorio	9,205536	-63,052682	1	26	7	1	NO DISPONIBLE	\N	\N
102	La Carolina	08OSUT BOL 065	1	Calle Coromoto frente a la iglesia	11,062114	-69,714238	1	21	5	1	NO DISPONIBLE	\N	\N
103	La Centellita	08OSUT MON 158	1	Desde Coro: se toma la T-4 con rumbo Sur, vía Churuguara, por 62km hasta la salida de La Cruz de Taratara, de ahí se toma la vía a Cabure rumbo Este, por 7,2km hasta La Encrucijada. Si se sale de San Luis: tomar	11,086668	-69,656758	1	26	7	1	NO DISPONIBLE	\N	\N
105	La Cruz de Taratara	08OSUT FAL 094	1	A 80km desde El Sombrero rumno Noroeste por la Troncal 13,	9,53057	-68,069148	1	15	4	1	NO DISPONIBLE	\N	\N
106	La Encrucijada	08OSUT FAL 081	1	Intersección de la Calle 8 con Calle 6. Estado Táchira	8,208179	-72,250376	1	15	4	1	NO DISPONIBLE	\N	\N
107	La Esmeralda	08OSUT BOL 067	1	La Galera del Pao.Fundo El Potrero de Canoa, frente a la  Carretera Nacional, Las\tGaleras del Pao	9,573322	-68,185482	1	21	5	1	NO DISPONIBLE	\N	\N
108	La Fe	08OSUT COJ 075	1	Carretera Pavimentada, sector La Llanada a 9,82km al norte de Monay,  a mano derecha via Carora	9,634042	-70,422201	1	12	3	1	NO DISPONIBLE	\N	\N
109	La Fría	08OSUT TAC 184	2	Carretera Principal La Pedrera	7,532094	-71,571721	1	1	1	1	NO DISPONIBLE	\N	\N
110	La Galera	08OSUT COJ 076	2	Desde Coro: se toma la T-4 con rumbo Sur, vía Churuguara, por 51km hasta el poblado de La Peña. Desde San Luis se sigue rumbo Suroeste unos 6,4km hasta la Troncal 4 y de ahi rumbo Norte vía Coro por 4,4km	11,105762	-69,748539	1	12	3	1	NO DISPONIBLE	\N	\N
111	La Llanada	08OSUT TRU 193	1	FRENTE A LA CARRETERA NACIONAL ELORZA GUASDUALITO, ENTRE DOS CALLES S/N	7,113849	-69,787479	1	2	1	1	NO DISPONIBLE	\N	\N
112	La Pedrera	08OSUT TAC 185	2	Final Calle 3B, Urb. La Urbina, Sector  Sur, Cerca de Petare	10,492251	-66,804379	1	1	1	1	NO DISPONIBLE	\N	\N
101	Las Industrias	08OSUT ZUL 201	2	Carretera San Luis - Capure sector La Ciénaga - El Desengaño, a 6,7km al Noreste de San Luis y a 6,5km al Oeste de Capure	11,138577	-69,661439	1	30	6	1	NO DISPONIBLE	\N	2021-02-02 13:04:06
114	La Trinidad de Orichuna	08OSUT APU 042	2	CARRETERA NACIONAL LA VIUDA MACAPAIMA, TRAMO MORICHAL LARGO, PUENTE GUARAMPO, FUNDO VIRGEN DEL VALLE, KM 84	8,723194	-63,932292	1	10	3	1	NO DISPONIBLE	\N	\N
115	La Urbina	08OSUT MIR 151	2	Sector La Yeguera, frente a la entrada a la finca San Gregorio y a 25m de la carretera\tNal. q conduce desde San Juan de Payara hacia Puerto Páez.	7,320562	-67,739983	1	9	2	1	NO DISPONIBLE	\N	\N
117	La Viuda	08OSUT ANZ 022	2	Av Circunvalación 2 entre calle 115 y Av 48 , Edif Las Industrias Maracaibo	10,597211	-71,645529	1	28	7	1	NO DISPONIBLE	\N	\N
118	La Yegüera	08OSUT APU 037	1	Carretera Nacional Troncal-9, Sector Santa Cruz, en terrenos propiedad del\tSr. Nelson Machado	9,542543	-67,661464	1	10	3	1	NO DISPONIBLE	\N	\N
119	Lagunillas (F)(Capital)	08OSUT MER 140	2	Calle Doña Bárbara Con Calle San Jose.  DIAGONAL PLAZA BOLIVAR.	9,112084	-66,395378	1	4	1	1	NO DISPONIBLE	\N	\N
120	Las Lajitas	08OSUT GUA 112	1	Ubicado en centro poblado Los Bancos de San Pedro a 22 Km desde el puente a la\tsalida de Calabozo, via Camaguan- San Fernando de Apure	8,740311	-67,539654	1	13	3	1	NO DISPONIBLE	\N	\N
121	Las Mercedes	08OSUT GUA 107	2	En la población de Morichalito, a 5Km al Noroeste de Los Pijiguaos y a 170Km al Suroeste de Caicara del Orinoco. Frente a la iglesia del centro poblado	6,578944	-66,777667	1	13	3	1	NO DISPONIBLE	\N	\N
122	Los Bancos de San Pedro	08OSUTG UA 103	1	Sector Los Quemados, se localiza a16km aproximadamente al Suroeste de\tCoro por la Troncal 4, sector Los Quemados	11,279428	-69,705021	1	13	3	1	NO DISPONIBLE	\N	\N
123	El Manguito	08OSUT ANZ 016	1	Calle Guillermo Alvizu, con frente a la esquina de la calle 1	10,028103	-69,261091	7	28	7	1	NO DISPONIBLE	\N	\N
124	Los Pijiguaos	08OSUT BOL 060	2	CALLE INDEPENDENCIA CON ESQUINA CALLE DELICIAS.	10,056765	-72,554501	1	21	5	1	NO DISPONIBLE	\N	\N
125	Los Quemados	08OSUT FAL 092	1	Carrera 3 Con Calle 3 Anillo Este	7,568158	-69,144607	1	15	4	1	NO DISPONIBLE	\N	\N
126	Los Rastrojos (Cabudare)	08OSUT LAR 126	2	CENTRAL MOVIL CA. COMERCIO SECTOR EL MANTECO	10,798407	-69,442167	1	17	4	1	NO DISPONIBLE	\N	\N
128	Mantecal	08OSUT APU 034	2	Calle 76, esquina Av. 3E detrás de la Iglesia Padre Claret. Bella vista III. Piso 4	10,668599	-71,602961	1	10	3	1	NO DISPONIBLE	\N	\N
129	Maparari	08OSUT FAL 088	2	Centro poblado Valle de Ma. Díaz, a 27,2km al Noroeste de Churuguara Edo.\tFalcón por la Troncal 4 o a 91,5 al Sur de Coro por la T-4	10,887411	-69,682617	1	15	4	1	NO DISPONIBLE	\N	\N
130	Mapire	08OSUT ANZ 017	1	Av. Bolívar en el centro poblado de Maripa ubicado a 60km al Este de Santa\tRosalía por la Troncal 19	7,417051	-65,181515	1	28	7	1	NO DISPONIBLE	\N	\N
132	María Díaz	08OSUT FAL 089	1	ENTRE CALLES 14 Y 13 CON CARRERA 10 Y 11	9,743781	-63,181768	1	15	4	1	NO DISPONIBLE	\N	\N
133	Maripa	08OSUT BOL 068	1	población Mejo , a 92 m de la Carretera Nacional que conduce desde las Mercedes hacia Santa Rita y Cabruta , Estado Guarico. en terrenos del Sr. Leopoldo Matos	8,655239	-66,477568	1	21	5	1	NO DISPONIBLE	\N	\N
134	Mata de Caña	08OSUT APU 035	1	Entre calles Zerpa 5 y Bolivar 4 con calle Lazo 21,	8,597626	-71,143083	1	10	3	1	NO DISPONIBLE	\N	\N
135	MATURIN	08OSUT MON 159	2	Centro Calle Principal frente al cementerio	8,746577	-70,920103	2	26	7	1	NO DISPONIBLE	\N	\N
136	Mejo	08OSUT GUA 095	1	Ortiz.Entre las calles Plaza y Marrón Cabrera frente a la Calle San Juan. En Terreno	9,619415	-67,286718	1	13	3	1	NO DISPONIBLE	\N	\N
137	Mérida	08OSUT MER 136	2	Sector de Oruza, calle Ppal. de Oruza. En el Km 42 de la autopista Gran Mariscal de Ayacucho. al lado del campo de softball de Oruza	10,390918	-66,503253	1	4	1	1	NO DISPONIBLE	\N	\N
138	Mucuchíes	08OSUT MER 138	2	Entrada Ppal. De la Urbanización El Prado Pampanito	9,422839	-70,453359	1	4	1	1	NO DISPONIBLE	\N	\N
139	Oruza	08OSUT MIR 153	1	Vía principal que conduce de Guarero a Maicao, frente al Fuerte Páez	11,347568	-71,968187	1	9	2	1	NO DISPONIBLE	\N	\N
140	Pampanito	08OSUT TRU 196	1	Población Pariaguan Calle El cementerio	8,836736	-64,722842	1	2	1	1	NO DISPONIBLE	\N	\N
142	Pariaguán	08OSUT ANZ 008	2	Troncal 19, a 41km desde la alcabala de la GN a la salida de Achaguas Edo. Apure vía San Fernando de Apure. a 114m por camino de tierra desde la T-19	7,783309	-67,872127	1	28	7	1	NO DISPONIBLE	\N	\N
143	Paso del Cinaruco	08OSUT APU 038	2	Sector Peña Blanca, Troncal 7, a 13km al Norte de Chabasquén (Por) y 14km al Sur de Sabana Grange (Lar). Terreno ubicado a 600m de la entrada a Peña Blanca	9,51292	-69,920566	1	10	3	1	NO DISPONIBLE	\N	\N
144	Payarita	08OSUT APU 029	1	Se localiza a 6,4km al Norte de Bobare por la Troncal 4 que conduce de\tBarquisimeto (LAR) hacia Churuguara (FAL)	10,317178	-69,462714	1	10	3	1	NO DISPONIBLE	\N	\N
145	Peña Blanca	08OSUT POR 165	1	AV. AGUERREVERE, ENTRE AV. AMAZONAS Y LA GUARDIA.	5,665071	-67,626473	1	16	4	1	NO DISPONIBLE	\N	\N
146	Potrero de Bucare	08OSUT LAR 121	1	Puerto Nutrias.Entre las calles Real Izquierda y Real derecha, Ciudad de Nutrias.\tMunicipio Sosa.En Terreno	8,073114	-69,301507	1	17	4	1	NO DISPONIBLE	\N	\N
148	Puerto de Nutrias	08OSUT BAR 053	1	Subestación Puerto Nuevo, ubicada sobre la troncal 12 que comunica Puerto Ayacucho con Caicara del Orinoco a 6,3 Km al sur este de El Burro y a 31 Km al suroeste de la Sabanita. Parroquia Pijiguaos, Municipio Cedeño, Estado Bolívar,	6,186503	-67,386714	1	3	1	1	NO DISPONIBLE	\N	\N
116	La Villa del Rosario	08OSUT ZUL 209	2	Frente a la av. Sucre	8,505827	-71,388544	1	30	6	1	NO DISPONIBLE	\N	2021-02-02 13:04:27
141	Paraguaipoa	08OSUT ZUL 208	2	Poblado Paso del Cinaruco, frente a carretera Nal. q conduce desde San Juan de\tPayara hacia Puerto Páez. Al lado del colegio El Progreso	6,554652	-67,504532	1	30	6	1	NO DISPONIBLE	\N	2021-02-02 13:05:26
147	Puerto Ayacucho	08OSUT AMA 001	2	Av. Municipal con calles La Providencia y Sucre, Ed. Anzoátegui II, piso 3	10,212218	-64,633491	1	30	6	1	NO DISPONIBLE	\N	2021-02-02 13:05:43
149	Puerto la Cruz	08OSUT ANZ 214	2	Vía al Matadero, Frente a la Planta de CADAFE	6,210952	-67,449088	1	28	7	1	NO DISPONIBLE	\N	\N
150	Puerto Páez	08OSUT APU 039	2	CALLE LA PLANTA EDIFICIO CANTV	10,061324	-65,04335	1	10	3	1	NO DISPONIBLE	\N	\N
151	Puerto Píritu	08OSUT ANZ 007	2	Avenida 10 (México) con Av. Paraguay, entre las calles 6 (Falcón) con  calle 7 (Mariño)	11,692364	-70,205443	1	28	7	1	NO DISPONIBLE	\N	\N
152	Punto Fijo	08OSUT FAL 213	2	Calle N°11 entre Avenidas 6 y 7, a ½ cuadra de la Plaza Bolívar	9,92483	-69,620784	1	15	4	1	NO DISPONIBLE	\N	\N
153	Quibor	08OSUT LAR 122	1	Intersección de las calles Rivero, Piar y Zea	10,697371	-63,108638	1	17	4	1	NO DISPONIBLE	\N	\N
154	Río Caribe	08OSUT SUC 172	2	FRENTE A CALLE LAS MERCEDES	10,319785	-65,976982	1	29	7	1	NO DISPONIBLE	\N	\N
155	Río Chico	08OSUT MIR 145	1	Sector Sabana de Parhueña, a 3,8Km de la entrada da la comunidad Monte\tBlanco, carretera El Burro - Puerto Ayacucho	5,933458	-67,408075	1	9	2	1	NO DISPONIBLE	\N	\N
157	San Bartolo	08OSUT APU 040	2	Av. José Tadeo Monagas. con Av. José Laurencio Silva	9,661019	-68,579669	1	10	3	1	NO DISPONIBLE	\N	\N
158	San Carlos	08OSUT COJ 078	2	Calle García de Sena con calle Miranda	9,999654	-67,017822	1	12	3	1	NO DISPONIBLE	\N	\N
159	San Casimiro	08OSUT ARA 044	2	Calle 11 entre las carreras 9 y 10	7,770664	-72,229677	1	14	3	1	NO DISPONIBLE	\N	\N
160	SAN CRISTÓBAL	08OSUT TAC 188	2	Sector San Diego de Cabrutica a 61Km al Sur de Pariaguan por la carretera que\tconduce a Mapire al Sur del Edo. Anzoátegui. Al lado del ambulatorio José Francisco Torrealba, cercano a un PDVAL	8,421038	-64,889415	1	1	1	1	NO DISPONIBLE	\N	\N
161	San Diego de Cabrutica	08OSUT ANZ 018	1	Calle Muñoz con calle Girardot	7,89123	-67,473746	1	28	7	1	NO DISPONIBLE	\N	\N
162	San Fernando de Apure	08OSUT APU 043	2	Desde Ciudad Bolívar: se toma la T-19 rumbo Oeste vía Caicara del Orinoco, por 319km hasta el poblado Cuchivero. Desde Caicara del Orinoco: tomar la T-12 rumbo Sur por 6,5km hasta interceptar la T-19 de ahí rumbo Sureste	7,523492	-65,848267	1	10	3	1	NO DISPONIBLE	\N	\N
163	San Francisco de Cuchivero	08OSUT BOL 062	1	A 12km de Santa Teresa dirección Sur, desde la carretera nacional que conduce desde Sta. Teresa hacia Ocumare del Tuy, 300m antes de la entrada a Yare	10,179616	-66,743952	1	21	5	1	NO DISPONIBLE	\N	\N
166	San José	08OSUT ANZ 006	1	Calle San Francisco con Calle  Bermúdez	10,598672	-63,329465	1	28	7	1	NO DISPONIBLE	\N	\N
167	San José de Areocuar	08OSUT SUC 170	2	Calle 11 entre Av. Sucre  y Av. Colon	10,009663	-72,393696	1	29	7	1	NO DISPONIBLE	\N	\N
169	San Juan de Colón	08OSUT TAC 182	2	Gobernación de Guárico, diagonal a la estatua de San Juan	9,91173	-67,354522	1	1	1	1	NO DISPONIBLE	\N	\N
170	SAN JUAN DE LOS MORROS	08OSUT GUA 104	2	Av. Negro Primero San Juan de Payara	7,646282	-67,608927	1	13	3	1	NO DISPONIBLE	\N	\N
171	San Juan de Payara	08OSUT APU 041	2	San Luis.Calle Principal y Guillermo Coronado.En Terreno	11,120436	-69,687544	1	10	3	1	NO DISPONIBLE	\N	\N
172	San Luis	08OSUT FAL 083	1	Tío Victor Av. 7 con calles 21 y 22	10,957395	-71,731427	1	15	4	1	NO DISPONIBLE	\N	\N
174	San Rafael del Piñal	08OSUT TAC 183	2	Intersección de la calle Bolívar y Ricaute	9,945386	-67,179369	1	1	1	1	NO DISPONIBLE	\N	\N
175	San Sebastián	08OSUT ARA 046	2	Calle principal, Carretera Vía Caripito- Casanay, frente a La Junta Comunal	10,228447	-63,194141	1	14	3	1	NO DISPONIBLE	\N	\N
176	San Vicente	08OSUT SUC 169	2	Carretera Nacional Troncal-9, Sector Santa Cruz, en terrenos propiedad del Sr. Nelson\tMachado.  al lado de la Qda. Santa Cruz	10,174741	-65,804573	1	29	7	1	NO DISPONIBLE	\N	\N
177	Santa Cruz	08OSUT MIR 148	1	A. ppal. Carretera El Moján.  DIAGONAL A LA PLAZA  BOLIVAR.	10,792325	-71,682042	1	9	2	1	NO DISPONIBLE	\N	\N
179	Santa Fe	08OSUT ANZ 021	1	Carretera vía Churuguara Barquisimeto, Diagonal a la Inspectoría	10,612481	-69,28355	1	28	7	1	NO DISPONIBLE	\N	\N
180	Santa Inés	08OSUT LAR 134	2	Sector Sifón, frente a Transversal 8, entre las Avs. Paz Castillo y Sucre	10,303	-66,65908	1	17	4	1	NO DISPONIBLE	\N	\N
181	Santa Lucía	08OSUT MIR 146	2	Calle Mérida, sector Banco Obrero, al lado de la Escuela	8,808146	-65,32603	1	9	2	1	NO DISPONIBLE	\N	\N
182	Santa María de Ipire	08OSUT GUA 117	1	FRENTE AL ESTADIO, ACCESO VÍA AGUARO, SANTA RITA DE MANAPIRE.	8,136012	-66,26089	1	13	3	1	NO DISPONIBLE	\N	\N
183	Santa Rita	08OSUT GUA 108	2	Av. Sucre a 200  mts de la Plaza Bolívar Carretera vía las Cocuizas, Municipio Rojas -\tBarinas	8,437761	-69,701901	1	13	3	1	NO DISPONIBLE	\N	\N
184	Santa Rosa	08OSUT BAR 052	1	Calle Maray con Av. Principal de Santa Rosalía, a 68kmOeste de Maripa por la Troncal 19	7,484607	-65,650834	1	3	1	1	NO DISPONIBLE	\N	\N
185	Santa Rosalía	08OSUT BOL 063	2	Centro poblado Santo Domingo a 60 Km desde la salida de Valle de la Pascua, via Valle de la Pascua - Santa Maria de Ipire	8,910364	-65,610654	1	21	5	1	NO DISPONIBLE	\N	\N
186	Santo Domingo	08OSUT GUA 100	1	Av. San Felipe entre las calles Comercio y Miranda	9,780704	-69,166903	1	13	3	1	NO DISPONIBLE	\N	\N
156	Sabaneta de Parhueña	08OSUT AMA 002	1	Poblado San Bartolo, frente a carretera Nal. que conduce desde San Juan de Payara\thacia Puerto Páez.	7,021875	-67,564385	1	30	6	1	NO DISPONIBLE	\N	2021-02-02 13:05:58
165	San Ignacio	08OSUT ZUL 210	1	Ubicado en asentamiento rural de San José a 60 Km al noroeste de Aragua de\tBarcelona y a 45 Km al su de la población Onoto	9,585317	-64,979223	1	30	6	1	NO DISPONIBLE	\N	2021-02-02 13:06:11
168	San José de Perijá	08OSUT ZUL 204	2	Intersección calle 6 con carrera 8, frente al parque Sucre	8,031946	-72,260205	1	30	6	1	NO DISPONIBLE	\N	2021-02-02 13:06:24
173	San Rafael del Moján	08OSUT ZUL 205	1	Carrera 3 con calles 2 y 3 Municipio Cárdenas	7,531151	-71,961007	1	30	6	1	NO DISPONIBLE	\N	2021-02-02 13:06:43
178	Santa Cruz de Mara	08OSUT ZUL 206	1	Asentamiento rural de Santa fe a 20 Km al sur de Clarines y a 45 Km al norte de Onoto	9,871368	-65,071721	1	30	6	1	NO DISPONIBLE	\N	2021-02-02 13:07:02
187	Sarare	08OSUT LAR 127	1	Sector Sicarigua entre las calles La Manga y El Estadio. Está entre las poblaciones Los Araques a 10Km (Lara) y 18Km entrada del Central Azucarero La Pastora (Trujillo). El terreno esta al lado de la Casa Comunal y ambulatorio de Sicarigua	9,943939	-70,097041	1	17	4	1	NO DISPONIBLE	\N	\N
188	Sicarigua	08OSUT LAR 130	1	Carretera Flor de Patra - Siquisay a 1km pasando el puente sobre el río Limón viniendo de Flor de Patria o a 4,2km del cruce de la vía que conduce\ta Trujillo viniendo de Siquisay a F.P.	9,46207	-70,404199	1	17	4	1	NO DISPONIBLE	\N	\N
189	Siquisay	08OSUT TRU 195	1	Final de la calle Guzmán Blanco Soledad	8,167651	-63,567789	1	2	1	1	NO DISPONIBLE	\N	\N
190	Soledad	08OSUT ANZ 010	1	ENTRE LAS CALLES REDENCION Y FRATERNIDADFRENTE A FUNDA-BARLOVENTO	10,396868	-66,152791	5	28	7	1	NO DISPONIBLE	\N	\N
191	Tacarigua de Mamporal	08OSUT MIR 143	2	Asentamiento rural Terronal a 60Km al noroeste de Aragua de Barcelona y a 35 Km al\tsur de Onoto	9,733442	-65,053955	1	9	2	1	NO DISPONIBLE	\N	\N
192	Terronal	08OSUT ANZ 012	1	Av. Bolívar con calle Vargas	9,700833	-68,431319	1	28	7	1	NO DISPONIBLE	\N	\N
193	Tinaco	08OSUT COJ 079	2	Centro Poblado Tintorero	9,990954	-69,567631	1	12	3	1	NO DISPONIBLE	\N	\N
194	Tintorero	08OSUT LAR 123	1	Av. principal, a 240m de la salida Sur de Tucupido para tomar la Troncal 5	8,949503	-69,84686	1	17	4	1	NO DISPONIBLE	\N	\N
195	Tucupido	08OSUT POR 164	1	Conocida como Barrio Ajuro, se localiza a 23,4km al Norte de Bobare por la\tTroncal 4 que conduce de Barquisimeto (LAR) hacia Churuguara (FAL)	10,402558	-69,420954	1	16	4	1	NO DISPONIBLE	\N	\N
196	Usera	08OSUT LAR 120	1	FINAL CALLE BOLIVAR, VIA SANTA CRUZ DEL ORINOCO.	8,113018	-64,679402	1	17	4	1	NO DISPONIBLE	\N	\N
197	Uverito	08OSUT ANZ 011	1	Calle 6 y 7 y avenidas 11 y 12 diagonal a la Plaza Bolívar	9,31553	-70,6083	1	28	7	1	NO DISPONIBLE	\N	\N
198	Valera	08OSUT TRU 198	2	Valle de La Pascua	9,212922	-66,006717	1	2	1	1	NO DISPONIBLE	\N	\N
199	Valle de la Pascua	08OSUT GUA 212	2	Se encuentra ubicada en la población Las Casitas, carretera nacional Barinas - Apure, a 150 Km al sureste de la ciiudad de Barinas y a 30 Km al noreste del estado Apure	8,236012	-69,418389	1	13	3	1	NO DISPONIBLE	\N	\N
200	Vegón de Nutrias	08OSUT BAR 054	1	Las Veguitas.Av. Bolívar entre calles Cementerio e Independencia, Las Veguitas.En\tTerreno	8,826558	-69,995488	1	3	1	1	NO DISPONIBLE	\N	\N
201	Veguitas	08OSUT BAR 048	2	Estación transmisión PDVSA Veladero , troncal 10 vía entre Maturin y El Blanquero, en la parte posterior del punto de control de la Guardia Nacional Bolivariana.	9,562757	-63,112194	1	3	1	1	NO DISPONIBLE	\N	\N
202	Yaguaraparo	08OSUT SUC 175	2	Intersección de las calles Cantaura y El Piar	10,568625	-62,826956	1	29	7	1	NO DISPONIBLE	\N	\N
203	Yoco	08OSUT SUC 181	2	Calle principal del Caserío Pueblo Nuevo	10,60733	-62,421763	1	29	7	1	NO DISPONIBLE	\N	\N
80	El Rubio	08OSUT APU 031	1	Intersección de las Calles Mac Gregor, Páez y Pedro Molina,	8,99	-65,743639	4	10	3	1	NO DISPONIBLE	\N	2021-01-19 09:12:05
71	El Guacuco	08OSUT MIR 147	1	Desde Coro: se toma la T-4 rumbo Sur vía Churuguara, por 41,1km hasta El Mamón. Desde San Luis: se sigue rumbo Suroeste hasta la T-4 unos 6,4km luego rumbo Noroeste por 14,3km hasta Calle Principal El Mamón con	11,150892	-69,738445	5	9	2	1	Cupira-  Fuera de servicio	\N	2021-01-19 09:17:00
164	San Francisco de Yare	08OSUT MIR 150	2	Sector San Ignacio, entre las poblaciones de Villa del Rosario a 17,3Km al Norte y Machiques a 25,3Km por el Sur. al lado del estadio de beisbol Leonel Quintero al Sur de la Av. Bolívar	10,18222	-72,384697	4	9	2	1	Vandalizado totalmente, pero se ve iluminado en el gestor.	\N	2021-01-19 09:22:03
2	Agua Amarilla	08OSUT GUA 116	1	Sector Agua Amarilla, Troncal 15, a 33,6km al Este de alcabala de la GN ubicada en la salida de Santa Ma. de Ipire (Guá.) y 40km al Oeste de Pariaguán (Anz.)	8,827225	-65,028297	4	13	3	1	No declarado en el gestor.	\N	2021-01-19 09:26:25
113	La Peña	08OSUT FAL 082	1	CALLE AURORA ESQ. CALLE 18 DE OCTUBRE.	10,326083	-72,314491	1	15	4	1	Declarado . Fuera de servicio	\N	2021-01-19 09:36:33
104	La Ciénaga, El Desengaño	08OSUT FAL 084	1	Se localiza Cll. 2 con Cll. 1 en el sector La Esmeralda, ubicado a 53km al\tSuroeste de La Carolina por la  Troncal 19,	7,564461	-64,279308	1	15	4	1	Declarado fuera de servicio	\N	2021-01-19 09:37:52
127	Machiques	08OSUT ZUL 203	2	BARRIO BUENOS AIRES SECTOR STADIUM.	7,745772	-64,707438	1	30	6	1	NO DISPONIBLE	\N	2021-02-02 13:04:47
131	Maracaibo (Capital)	08OSUT ZUL 207	2	De Mantecal se sigue rumbo Oeste por la Troncal 19 unos 13km hasta La Ye, de ahí se continúa rumbo Suroeste vía Elorza  por la Troncal 4 unos\t24,5km, el terreno está en la entrada de Mata de Caña	7,337593	-69,300262	1	30	6	1	NO DISPONIBLE	\N	2021-02-02 13:05:09
\.


--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.password_resets (email, token, created_at) FROM stdin;
\.


--
-- Data for Name: personal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.personal (id, nombre, apellido, cedula, cod_p00, region_id, num_oficina, num_personal, correo, cargo, status, "FotoPersonal", created_at, updated_at, estado_id) FROM stdin;
1	SIN ASIGNAR	SIN ASIGNAR	No 12312312	No p000000	1	No Asignado	No Asignado	Nodisponible@no.com	No Asignado	1	\N	\N	\N	\N
2	RAFFAELE	SICURANZA	11.601.341	149031	\N	02125006159	04264211573	rsicur01@cantv.com.ve	SUPERVISOR	\N	\N	2021-01-12 12:29:07	2021-01-12 12:29:07	\N
4	Lewis	Villanueva	16.273.527	141802	\N	02125001963	04242174709	lvilla11@cantv.com.ve	SUPERVISOR	\N	\N	2021-01-13 13:38:03	2021-01-13 13:38:03	\N
5	Erick	Perez	00000000	0000000	\N	00000000000	00000000000	email@cantv.com	None	\N	\N	2021-01-13 18:28:45	2021-01-13 18:28:45	\N
7	ANYELO ANTONIO	PRADO DURAN	16717368	135679	\N	02869234991	04265947689	aprado01@cantv.com.ve	COORDINADOR ENERGIA	\N	\N	2021-01-20 10:06:43	2021-01-20 10:06:43	\N
8	NATHANIEL	CASTELLANO	17045797	153498	\N	02856522560	04165017755	ncaste02@cantv.com.ve	COORDINADOR ENERGIA	\N	\N	2021-01-20 10:36:09	2021-01-20 10:36:09	\N
9	PERCY	TOVAR	13964597	149102	\N	02856522560	04265333843	PTOVAR01@CANTV.COM.VE	COORDINADOR ENERGIA	\N	\N	2021-01-20 12:33:10	2021-01-20 12:33:10	\N
10	Ember	Nagua	14541863	150856	\N	02556219155	04126710866	Enagua01@gmail.com	Supervisor Energia	\N	\N	2021-01-22 08:52:07	2021-01-22 08:52:07	\N
11	ALFONSO	NUÑEZ	16015991	128745	\N	02712213068	04166803158	anunez02@cantv.com.ve	COORDINADOR	\N	\N	2021-01-22 11:01:31	2021-01-22 11:01:31	\N
12	David Daniel	Diaz Colina	16121996	153416	\N	04127694322	04127694322	ddiazc01@cantv.com.ve	Especialista	\N	\N	2021-01-28 08:13:17	2021-01-28 08:13:17	\N
13	JESUS RAFAEL	MEDINA CASTILLO	22145939	153418	\N	04246082434	04246082434	Jmedina30@cantv.com.ve	ESPECIALISTA	\N	\N	2021-01-28 10:56:27	2021-01-28 10:56:27	\N
14	GEORGE JOSE	HENEECH DIAZ	25.062.261	150680	\N	02812695377	04248767528	ghenee01@cantv.com.ve	Especialista CCT	\N	\N	2021-02-11 10:00:25	2021-02-11 10:00:25	\N
15	Edward	SUNIAGA	11435011	110902	\N	02832351457	04166810661	esunia@cantv.com.ve	Supervisor	\N	\N	2021-02-11 14:43:02	2021-02-11 14:43:02	\N
16	RICHARD FEDERICO	SOLER FLORES	16371669	152339	\N	02735332990	04140576964	rsoler02@cantv.com.ve	SUPERVISOR	\N	\N	2021-02-23 11:35:31	2021-02-23 11:35:31	\N
17	Jose Ivan	Arismendi Perez	8047463	141964	\N	02742521864	04265780153	jarism01@cantv.com.ve	Especialista Energia	\N	\N	2021-02-26 13:11:58	2021-02-26 13:11:58	\N
18	Jose Ivan	Arismendi Perez	8047463	141964	\N	02742521864	04265780153	Jarism01@cantv.com.ve	Especialista Energia	\N	\N	2021-03-01 12:32:41	2021-03-02 12:31:05	\N
6	joel	diaz	20956281	153705	3	02353428031	04144780929	joeldiz1805@gmail.com	Especialista	1	\N	2021-01-15 14:03:39	2021-03-17 15:37:20	13
19	robert antonio	moreno peña	19212231	153817	\N	04143644474	04143644474	rmoren09@cantv.com.ve	Especilasta	\N	\N	2021-03-18 11:56:57	2021-03-18 11:56:57	\N
20	JULIO	GONZALEZ	14439718	152842	\N	02916414489	04149927777	JGONZA154@CANTV.COM.VE	SUPERVISOR	\N	\N	2021-03-18 13:25:12	2021-03-18 13:25:12	\N
21	Luis	Medina	14468770	154020	\N	02832351457	04248133574	lmedin09@cantv.com.ve	Supervisor	\N	\N	2021-03-19 11:16:21	2021-03-19 11:16:21	\N
22	joel	diaz	20956281	153700	\N	04165144444	00865567777	joeldiaz1805@gmail.com	Especialista	\N	\N	2021-03-22 11:56:47	2021-03-22 11:56:47	\N
23	alexander	alvarez	14789987	1457887	\N	02353412535	04144780929	joeldiaz1805@gmail.com	Supervisor	\N	\N	2021-03-23 10:12:42	2021-03-23 10:12:42	\N
\.


--
-- Data for Name: pisos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.pisos (id, nombre, created_at, updated_at) FROM stdin;
1	ALMACEN	\N	\N
2	AZOTEA	\N	\N
3	FURGON	\N	\N
4	MEZZANINA	\N	\N
5	PB	\N	\N
6	PH	\N	\N
7	SEMI SOTANO	\N	\N
8	SOTANO	\N	\N
9	UNICO	\N	\N
10	VARIOS PISOS	\N	\N
20	10	\N	\N
21	11	\N	\N
22	12	\N	\N
23	13	\N	\N
24	14	\N	\N
25	15	\N	\N
26	16	\N	\N
27	17	\N	\N
28	18	\N	\N
29	19	\N	\N
30	20	\N	\N
31	21	\N	\N
32	22	\N	\N
11	01	\N	2021-03-22 08:53:11
19	09	\N	2021-03-22 08:53:27
12	02	\N	2021-03-22 08:53:41
13	03	\N	2021-03-22 08:53:59
14	04	\N	2021-03-22 08:54:12
15	05	\N	2021-03-22 08:54:26
16	06	\N	2021-03-22 08:54:41
17	07	\N	2021-03-22 08:55:08
18	08	\N	2021-03-22 08:55:24
\.


--
-- Data for Name: plantillas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.plantillas (id, formato_id, descripcion, user_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: rect_bcobb; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rect_bcobb (id, rect_id, bancob_id, ubicacion_id, tipo_id, operatividad, fecha_instalacion, criticidad, garantia, observaciones, created_at, updated_at) FROM stdin;
1	1	2	61	2	Operativo	S/I	Baja	no	NO POSEE INVENTARIO DE CANTV	2021-01-28 08:26:34	2021-01-28 08:26:34
\.


--
-- Data for Name: rectificadores; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rectificadores (id, marca, modelo, serial, inventario_cant, num_fases, cant_oper, cant_inoper, cant_total, consumo_amp, carga_total_amp, volt_operacion, tablero, status_id, ubicacion_id, created_at, updated_at) FROM stdin;
1	Emerson	S/I	no posee	3111663	Trifásico	12	00	12	1200	1200	480 VAC	si	\N	\N	2021-01-28 08:26:34	2021-01-28 08:26:34
2	Emerson	generic	no posee	309667	Bifásico	20	20	20	20	20	100-240 VAC	si	\N	\N	2021-03-23 10:12:42	2021-03-23 10:12:42
\.


--
-- Data for Name: regiones; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.regiones (id, nombre, created_at, updated_at) FROM stdin;
1	ANDES	\N	\N
2	CAPITAL	\N	\N
3	CENTRAL	\N	\N
4	CENTRO OCCIDENTE	\N	\N
5	GUAYANA	\N	\N
6	OCCIDENTE	\N	\N
7	ORIENTE	\N	\N
\.


--
-- Data for Name: registro; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.registro (id, tipo_actividad, tipo_servicio, equipo_id, componente_id, ubicacion_id, sala, piso, estatus, accion, trabajo, porcentaje, cuadrilla_id, fecha_inicio, fecha_fin, ticket_cosec, name_informe, observaciones, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: requeopsut; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.requeopsut (id, cod_inv_equipo_id, requerimiento, stat_req_nod, observacion, nodoopsut_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.roles (id, denominacion, descripcion, created_at, updated_at) FROM stdin;
1	Super Usuario	Super Usuario	\N	\N
2	Administrador	Administrador	\N	\N
3	Monitor	Monitor	\N	\N
4	Secretaria	Secretaria	\N	\N
5	Gestion	Gestion	\N	\N
6	 RolCosec	RolCosec	2021-03-03 10:33:28	2021-03-03 10:33:28
\.


--
-- Data for Name: salas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.salas (id, nombre, created_at, updated_at) FROM stdin;
1	BANCO DE BATERIAS	\N	\N
2	CASETA	\N	\N
3	CENTRO DE CONTROL	\N	\N
4	CENTRO DE DESPACHO	\N	\N
5	CIC	\N	\N
6	CX	\N	\N
7	DATA CENTER	\N	\N
8	DIGITAL	\N	\N
9	DIGITAL TANDEM	\N	\N
10	DP	\N	\N
11	DSLAM	\N	\N
12	DX	\N	\N
13	EX	\N	\N
14	FURGON	\N	\N
15	LIBRE	\N	\N
16	MG	\N	\N
17	MONITOREO	\N	\N
18	OAC	\N	\N
19	OFICINA ADMINISTRATIVA	\N	\N
20	OPSUT	\N	\N
21	OUTDOOR	\N	\N
22	PCA	\N	\N
23	PCM	\N	\N
24	PRESURIZADORES	\N	\N
25	RECTIFICADORES	\N	\N
26	SERVIDORES	\N	\N
27	SSP	\N	\N
28	TABLERO	\N	\N
29	TERCEROS (USUARIOS)	\N	\N
30	TX	\N	\N
31	TX INTERNACIONAL	\N	\N
32	UMA	\N	\N
33	UMG	\N	\N
34	UNICA	\N	\N
35	UPS	\N	\N
36	VARIAS SALAS	\N	\N
37	VENEXPAQ	\N	\N
\.


--
-- Data for Name: status; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.status (id, status, ticket_id, fecha_instalacion, observaciones, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: sub_menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sub_menu (id, nombre, rol_id, menu_id, url, icon, observacion, created_at, updated_at) FROM stdin;
1	Lenvatamiento en Planta	1	2	#	fas fa-home nav-icon	Menu Inicio	\N	\N
2	Nodos OPSUT	1	2	#	fas fa-home nav-icon	Menu Inicio	\N	\N
3	Nodos Ngn	1	2	#	fas fa-home nav-icon	Menu Inicio	\N	\N
4	Registro de Tickets	1	3	#	fas fa-home nav-icon	Registro de Tickets	\N	\N
5	Usuarios	1	4	#	fas fa-tty	Usuarios	\N	\N
6	Personal	1	4	#	fas fa-user	Personal	\N	\N
7	Formatos y/o Reportes	1	4	#	fas fa-archive	Formatos y/o Reportes	\N	\N
\.


--
-- Data for Name: sub_sub_menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sub_sub_menu (id, nombre, rol_id, sub_menu_id, url, icon, observacion, created_at, updated_at) FROM stdin;
1	Motogenerador	1	1	motogenerador	fas fa-cogs nav-icon	MotoGenerador	\N	\N
2	Aire Acondicionado	1	1	aire_acondicionado	fas fa-cubes nav-icon	MotoGenerador	\N	\N
3	Rectificador	1	1	rectificador	fas fa-bolt nav-icon	Rectificador	\N	\N
4	Ups	1	1	ups	fas fa-battery-full nav-icon	Ups	\N	\N
5	Estatus	1	2	opsut	fas fa-chart-bar nav-icon	Estatus	\N	\N
6	Registro	1	2	nodos	fas fa-list-alt nav-icon	Registro	\N	\N
7	Requerimientos	1	2	reqopsut	fas fa-clipboard-list nav-icon	Requerimientos	\N	\N
8	Actualizaciones	1	2	#	fas fa-sync-alt nav-icon	Actualizaciones	\N	\N
9	Registro de Mantenimiento	1	4	#	fas fa-paste	Registro de Tickets	\N	\N
10	Tickets	1	4	#	fas fa-list-alt	Tickets	\N	\N
\.


--
-- Data for Name: tickets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tickets (id, numero, equipo_id, componente_id, responsable_id, usuario_id, central_id, tipo_falla, multimedia_id, tipo_servicio, tipo_mantenimiento, tipo_actividad, telefono, sala, piso, cargo, correo, correo_supervisor, status, fecha_inicio, fecha_fin_falla, hora_inicio, hora_fin_actividad, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: ubicacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ubicacion (id, central_id, piso, estructura, criticidad_esp, responsable_id, supervisor_id, sala, created_at, updated_at) FROM stdin;
1	1	PB	Fija	Crítica	2	\N	LIBRE	2021-01-12 12:29:07	2021-01-12 12:29:07
2	2	PB	Fija	Crítica	2	\N	LIBRE	2021-01-12 12:35:29	2021-01-12 12:35:29
3	3	PB	Fija	Crítica	2	\N	LIBRE	2021-01-12 12:35:38	2021-01-12 12:35:38
4	4	PB	Fija	Crítica	2	\N	LIBRE	2021-01-12 12:53:01	2021-01-12 12:53:01
5	5	P9	CENTRAL FIJA	Crítica	3	\N	LIBRE	2021-01-12 13:36:01	2021-01-12 13:36:01
6	6	PB	Fija	Crítica	2	\N	LIBRE	2021-01-12 13:42:58	2021-01-12 13:42:58
7	7	PB	Fija	Crítica	2	\N	MG	2021-01-12 14:25:41	2021-01-12 14:25:41
8	8	PB	Fija	Crítica	2	\N	LIBRE	2021-01-12 14:35:46	2021-01-12 14:35:46
9	9	PB	Fija	Crítica	2	\N	LIBRE	2021-01-12 15:46:05	2021-01-12 15:46:05
10	10	PB	Fija	Óptima	4	\N	MG	2021-01-13 13:38:03	2021-01-13 13:38:03
11	11	PB	Fija	Óptima	4	\N	MG	2021-01-13 14:09:13	2021-01-13 14:09:13
12	12	P2	CENTRAL FIJA	\N	5	\N	Transmisión	2021-01-13 18:28:45	2021-01-13 18:28:45
13	13	PB	URL	Óptima	6	\N	DSLAM	2021-01-15 14:03:39	2021-01-15 14:03:54
15	15	PB	URL	Óptima	7	\N	TX	2021-01-20 10:17:56	2021-01-20 10:17:56
16	16	PB	URL	Óptima	7	\N	TX	2021-01-20 10:19:20	2021-01-20 10:19:20
17	17	PH	CENTRAL FIJA	Óptima	8	\N	MG	2021-01-20 10:36:09	2021-01-20 10:36:09
18	18	PB	OPSUT	Óptima	8	\N	MG	2021-01-20 10:47:07	2021-01-20 10:47:07
19	19	PH	OPSUT	Óptima	8	\N	MG	2021-01-20 10:53:44	2021-01-20 10:53:44
20	20	PB	OPSUT	Óptima	8	\N	MG	2021-01-20 11:04:50	2021-01-20 11:04:50
21	21	PB	OPSUT	Óptima	8	\N	MG	2021-01-20 11:10:49	2021-01-20 11:10:49
22	22	PH	CENTRAL FIJA	Óptima	9	\N	MG	2021-01-20 12:33:10	2021-01-20 12:33:10
23	23	PH	CENTRAL FIJA	Óptima	9	\N	MG	2021-01-20 12:33:11	2021-01-20 12:33:11
24	24	PH	CENTRAL FIJA	Óptima	9	\N	MG	2021-01-20 12:33:11	2021-01-20 12:33:11
25	25	PB	CENTRAL FIJA	Óptima	8	\N	MG	2021-01-20 12:54:48	2021-01-20 12:54:48
26	26	PB	CENTRAL FIJA	Óptima	10	\N	UMA	2021-01-22 08:52:07	2021-01-22 08:52:07
27	27	PB	CENTRAL FIJA	Óptima	7	\N	MG	2021-01-22 10:55:37	2021-01-22 10:55:37
28	28	ST	Fija	Óptima	11	\N	MG	2021-01-22 11:01:31	2021-01-22 11:01:31
29	29	ST	Fija	Óptima	11	\N	MG	2021-01-22 11:01:49	2021-01-22 11:01:49
30	30	ST	Fija	Óptima	11	\N	MG	2021-01-22 11:01:58	2021-01-22 11:01:58
31	31	PB	CENTRAL FIJA	Óptima	7	\N	MG	2021-01-22 11:02:37	2021-01-22 11:02:37
32	32	PB	CENTRAL FIJA	Óptima	11	\N	MG	2021-01-22 11:08:48	2021-01-22 11:08:48
33	33	Unico piso	CENTRAL FIJA	Óptima	11	\N	MG	2021-01-22 11:15:10	2021-01-22 11:15:10
34	34	Unico piso	CENTRAL FIJA	Óptima	11	\N	MG	2021-01-22 11:22:35	2021-01-22 11:22:35
35	35	PB	CENTRAL FIJA	Óptima	11	\N	MG	2021-01-22 11:28:27	2021-01-22 11:28:27
36	36	Unico piso	CENTRAL FIJA	Óptima	11	\N	MG	2021-01-22 11:38:13	2021-01-22 11:38:13
37	37	Unico piso	CENTRAL FIJA	Óptima	11	\N	MG	2021-01-22 11:42:37	2021-01-22 11:42:37
38	38	PB	CENTRAL FIJA	Óptima	7	\N	MG	2021-01-22 11:57:27	2021-01-22 11:57:27
39	39	PB	CENTRAL FIJA	Óptima	7	\N	MG	2021-01-22 12:09:50	2021-01-22 12:09:50
40	40	PB	CENTRAL FIJA	Óptima	7	\N	MG	2021-01-22 12:10:57	2021-01-22 12:10:57
41	41	PB	CENTRAL FIJA	Óptima	7	\N	MG	2021-01-22 12:12:55	2021-01-22 12:12:55
42	42	PB	CENTRAL FIJA	Óptima	7	\N	MG	2021-01-22 12:38:45	2021-01-22 12:38:45
43	43	PB	CENTRAL FIJA	Óptima	7	\N	MG	2021-01-22 12:39:19	2021-01-22 12:39:19
44	44	PB	CENTRAL FIJA	Óptima	7	\N	MG	2021-01-22 12:40:15	2021-01-22 12:40:15
45	45	PB	CENTRAL FIJA	Óptima	7	\N	MG	2021-01-22 12:52:00	2021-01-22 12:52:00
46	46	PB	CENTRAL FIJA	Óptima	7	\N	MG	2021-01-22 12:52:34	2021-01-22 12:52:34
47	47	PB	CENTRAL FIJA	Óptima	7	\N	MG	2021-01-26 10:51:18	2021-01-26 10:51:18
48	48	PB	CENTRAL FIJA	Óptima	8	\N	MG	2021-01-26 12:20:18	2021-01-26 12:20:18
49	49	PH	CENTRAL FIJA	Óptima	6	\N	ADSL	2021-01-26 12:36:01	2021-01-26 12:36:01
50	50	PH	CENTRAL FIJA	Crítica	6	\N	ADSL	2021-01-26 12:48:57	2021-01-26 12:48:57
51	51	MZ	URL	Crítica	6	\N	CENTRO DE DISTRIBUCION	2021-01-26 13:02:55	2021-01-26 13:02:55
52	52	MZ	URL	Óptima	6	\N	BANCO DE BATERIA	2021-01-26 13:06:20	2021-01-26 13:06:20
53	53	PH	URL	Óptima	6	\N	ADSL	2021-01-26 13:10:39	2021-01-26 13:10:39
14	14	PB	Fija	Óptima	7	\N	MG	2021-01-20 10:06:43	2021-01-26 15:01:53
54	54	PB	Fija	Óptima	2	\N	LIBRE	2021-01-27 11:57:28	2021-01-27 11:57:28
55	55	PB	Fija	Óptima	2	\N	LIBRE	2021-01-27 11:57:43	2021-01-27 11:57:43
56	56	PB	CENTRAL FIJA	Óptima	10	\N	UMA	2021-01-27 12:24:57	2021-01-27 12:24:57
57	57	P1	CENTRAL FIJA	Óptima	10	\N	UMA	2021-01-27 12:32:14	2021-01-27 12:32:14
58	58	PB	Fija	Óptima	12	\N	MG	2021-01-28 08:13:17	2021-01-28 08:13:17
59	59	PB	Fija	Óptima	12	\N	MG	2021-01-28 08:13:50	2021-01-28 08:13:50
60	60	PB	Fija	Óptima	12	\N	MG	2021-01-28 08:16:35	2021-01-28 08:16:35
61	61	01	Fija	BAJA	12	\N	PCM	2021-01-28 08:26:34	2021-01-28 08:26:34
62	62	PB	Fija	Óptima	13	\N	ADSL	2021-01-28 10:56:28	2021-01-28 10:56:28
64	64	PB	Fija	Óptima	12	\N	MG	2021-01-29 13:16:31	2021-01-29 13:16:31
65	65	P3	CENTRAL FIJA	Crítica	10	\N	UMA	2021-02-01 10:08:37	2021-02-01 10:08:37
66	66	P4	CENTRAL FIJA	Óptima	10	\N	UMA	2021-02-01 10:13:24	2021-02-01 10:13:24
67	67	PB	CENTRAL FIJA	Óptima	10	\N	MG	2021-02-01 11:19:31	2021-02-01 11:19:31
68	68	PB	URL	Óptima	12	\N	MG	2021-02-01 11:27:45	2021-02-01 11:27:45
69	69	PB	CENTRAL FIJA	Óptima	8	\N	MG	2021-02-01 11:38:05	2021-02-01 11:38:05
70	70	PB	CENTRAL FIJA	Óptima	10	\N	MG	2021-02-01 11:47:16	2021-02-01 11:47:16
71	71	P2	CENTRAL FIJA	Óptima	10	\N	UMA	2021-02-01 11:53:30	2021-02-01 11:53:30
72	72	PB	URL	Óptima	13	\N	FURGON	2021-02-03 11:18:15	2021-02-03 11:34:59
63	63	PB	URL	Crítica	13	\N	FURGON	2021-01-28 11:50:50	2021-02-03 11:35:18
73	73	PB	URL	Óptima	13	\N	FURGON	2021-02-03 11:22:23	2021-02-03 11:35:41
74	74	PB	Fija	Óptima	13	\N	ADSL	2021-02-03 13:49:57	2021-02-03 13:49:57
75	75	PB	Fija	Óptima	13	\N	ADSL	2021-02-03 13:56:21	2021-02-03 13:56:21
77	77	P5	Seleccione tipo de estructura	Óptima	13	\N	TX	2021-02-04 12:20:00	2021-02-04 12:33:42
76	76	P5	Fija	Óptima	13	\N	TX	2021-02-04 12:08:14	2021-02-04 12:32:33
79	79	P5	Fija	Óptima	13	\N	TX	2021-02-04 12:38:58	2021-02-04 12:38:58
80	80	P4	Seleccione tipo de estructura	Óptima	13	\N	DX	2021-02-04 12:59:42	2021-02-12 09:54:05
81	81	P4	Fija	Óptima	13	\N	DX	2021-02-04 13:03:16	2021-02-04 13:03:16
82	82	P2	Fija	Óptima	13	\N	CX	2021-02-09 08:57:44	2021-02-09 08:57:44
83	83	P2	Fija	Óptima	13	\N	CX	2021-02-09 09:00:39	2021-02-09 09:00:39
84	84	P2	Fija	Óptima	13	\N	CX	2021-02-09 09:02:43	2021-02-09 09:02:43
85	85	P2	Fija	Óptima	13	\N	CX	2021-02-09 09:04:24	2021-02-09 09:04:24
86	86	P2	Fija	Óptima	13	\N	CX	2021-02-09 09:25:51	2021-02-09 09:25:51
87	87	P2	Fija	Óptima	13	\N	CX	2021-02-09 09:28:57	2021-02-09 09:28:57
88	88	P2	Fija	Óptima	13	\N	CX	2021-02-09 09:40:06	2021-02-09 09:40:06
89	89	P2	Fija	Óptima	13	\N	CX	2021-02-09 09:42:05	2021-02-09 09:42:05
90	90	P2	Fija	Óptima	13	\N	CX	2021-02-09 09:43:46	2021-02-09 09:43:46
92	92	PB	Fija	Óptima	13	\N	PCM	2021-02-09 11:12:48	2021-02-09 11:12:48
91	91	PB	Seleccione tipo de estructura	Crítica	13	\N	PCM	2021-02-09 10:42:20	2021-02-09 11:16:42
93	93	P1	Fija	Óptima	13	\N	PCM	2021-02-09 11:55:03	2021-02-09 11:55:03
94	94	AZOTEA	Fija	Óptima	13	\N	PCM	2021-02-09 11:58:05	2021-02-09 11:58:05
95	95	PB	URL	Óptima	12	\N	MG	2021-02-10 12:53:37	2021-02-10 12:53:37
96	96	PB	CENTRAL FIJA	Óptima	12	\N	MG	2021-02-10 13:05:29	2021-02-10 13:05:29
97	97	PB	CENTRAL FIJA	Óptima	12	\N	MG	2021-02-10 13:08:49	2021-02-10 13:08:49
98	98	PB	Fija	Óptima	12	\N	MG	2021-02-10 14:19:07	2021-02-10 14:19:07
99	99	PB	CENTRAL FIJA	Óptima	9	\N	MG	2021-02-10 16:45:48	2021-02-10 16:45:48
78	78	P5	Fija	Óptima	13	\N	TX	2021-02-04 12:24:14	2021-02-11 09:50:14
100	100	PB	Fija	Crítica	14	\N	MG	2021-02-11 10:00:25	2021-02-11 10:00:25
101	101	PB	Fija	Crítica	14	\N	MG	2021-02-11 10:53:39	2021-02-11 10:53:39
102	102	PB	Fija	Crítica	14	\N	MG	2021-02-11 11:05:14	2021-02-11 11:05:14
103	103	PB	Fija	Crítica	15	\N	MG	2021-02-11 14:43:02	2021-02-11 14:43:02
104	104	PB	Fija	Crítica	15	\N	MG	2021-02-11 14:43:11	2021-02-11 14:43:11
105	105	PB	Fija	Crítica	15	\N	MG	2021-02-11 14:44:20	2021-02-11 14:44:20
106	106	Unico piso	Fija	Crítica	15	\N	FURGON	2021-02-12 08:12:48	2021-02-12 08:12:48
108	108	Unico piso	URL	Crítica	15	\N	FURGON	2021-02-12 09:09:05	2021-02-12 09:09:05
109	109	Unico piso	URL	Crítica	15	\N	FURGON	2021-02-12 09:09:50	2021-02-12 09:09:50
110	110	Unico piso	URL	Crítica	15	\N	FURGON	2021-02-12 09:19:29	2021-02-12 09:19:29
111	111	Unico piso	URL	Crítica	15	\N	FURGON	2021-02-12 09:21:01	2021-02-12 09:21:01
112	112	Unico piso	URL	Crítica	15	\N	FURGON	2021-02-12 10:02:13	2021-02-12 10:02:13
113	113	Unico piso	URL	Crítica	15	\N	FURGON	2021-02-12 10:02:37	2021-02-12 10:02:37
114	114	PB	NODO OUTDOOR	Crítica	6	\N	DIGITAL	2021-02-12 10:11:54	2021-02-12 10:11:54
115	115	Unico piso	URL	Crítica	15	\N	FURGON	2021-02-12 10:20:33	2021-02-12 10:20:33
116	116	Unico piso	URL	Crítica	15	\N	FURGON	2021-02-12 10:36:16	2021-02-12 10:36:16
117	117	PB	Fija	Crítica	15	\N	MG	2021-02-12 11:06:16	2021-02-12 11:06:16
107	107	Seleccione ubicación piso	Fija	Crítica	15	\N	FURGON	2021-02-12 08:37:34	2021-02-12 11:07:32
118	118	Unico piso	URL	Crítica	15	\N	FURGON	2021-02-12 11:15:56	2021-02-12 11:15:56
119	119	Unico piso	URL	Crítica	15	\N	FURGON	2021-02-12 11:17:41	2021-02-12 11:17:41
120	120	Unico piso	URL	Crítica	15	\N	FURGON	2021-02-12 11:20:32	2021-02-12 11:20:32
121	121	Unico piso	Fija	Crítica	15	\N	TX	2021-02-12 12:10:24	2021-02-12 12:10:24
122	122	Unico piso	CENTRAL FIJA	Crítica	15	\N	MG	2021-02-12 15:40:17	2021-02-12 15:40:17
123	123	Unico piso	Fija	Crítica	15	\N	MG	2021-02-17 08:26:21	2021-02-17 08:26:21
124	124	Unico piso	Fija	Crítica	15	\N	LIBRE	2021-02-17 15:27:02	2021-02-17 15:27:02
125	125	Unico piso	Fija	Crítica	15	\N	LIBRE	2021-02-17 15:40:27	2021-02-17 15:40:27
126	126	Seleccione ubicación piso	Fija	Crítica	15	\N	MG	2021-02-17 15:53:12	2021-02-17 15:54:21
127	127	PB	CENTRAL FIJA	Óptima	16	\N	LIBRE	2021-02-23 11:35:31	2021-02-23 11:35:31
129	129	PB	CENTRAL FIJA	Óptima	16	\N	LIBRE	2021-02-23 11:38:28	2021-02-23 11:38:28
130	130	PB	CENTRAL FIJA	Óptima	16	\N	MG	2021-02-23 12:06:52	2021-02-23 12:06:52
128	128	PB	Fija	Óptima	16	\N	LIBRE	2021-02-23 11:36:39	2021-02-23 12:08:33
132	132	Unico piso	Fija	Crítica	11	\N	MG	2021-02-23 14:06:13	2021-02-23 14:06:13
133	133	Unico piso	Fija	Crítica	11	\N	MG	2021-02-23 14:10:14	2021-02-23 14:10:14
134	134	Unico piso	Fija	Crítica	11	\N	MG	2021-02-23 14:17:51	2021-02-23 14:17:51
135	135	Unico piso	CENTRAL FIJA	Óptima	11	\N	MG	2021-02-23 14:22:26	2021-02-23 14:22:26
136	136	Unico piso	CENTRAL FIJA	Óptima	11	\N	MG	2021-02-23 14:27:11	2021-02-23 14:27:11
137	137	Unico piso	CENTRAL FIJA	Óptima	11	\N	MG	2021-02-23 14:27:54	2021-02-23 14:27:54
138	138	Unico piso	CENTRAL FIJA	Óptima	11	\N	MG	2021-02-23 14:32:24	2021-02-23 14:32:24
139	139	Unico piso	CENTRAL FIJA	Óptima	11	\N	MG	2021-02-23 14:53:10	2021-02-23 14:53:10
140	140	PB	Fija	Óptima	11	\N	MG	2021-02-24 14:01:47	2021-02-24 14:01:47
141	141	PB	CENTRAL FIJA	Óptima	11	\N	MG	2021-02-24 14:38:20	2021-02-24 14:38:20
142	142	PB	CENTRAL FIJA	Crítica	11	\N	MG	2021-02-24 14:43:59	2021-02-24 14:43:59
143	143	Unico piso	CENTRAL FIJA	Crítica	11	\N	MG	2021-02-25 13:53:30	2021-02-25 13:53:30
144	144	Unico piso	OPSUT	Crítica	11	\N	MG	2021-02-25 14:14:40	2021-02-25 14:14:40
145	145	Unico piso	OPSUT	Crítica	11	\N	MG	2021-02-25 14:18:03	2021-02-25 14:18:03
146	146	Unico piso	OPSUT	Crítica	11	\N	MG	2021-02-25 14:23:59	2021-02-25 14:23:59
147	147	Unico piso	OPSUT	Crítica	11	\N	MG	2021-02-25 14:32:19	2021-02-25 14:32:19
148	148	PB	Fija	Óptima	12	\N	MG	2021-02-26 12:44:00	2021-02-26 12:44:00
150	150	ST	CENTRAL FIJA	Óptima	17	\N	MG	2021-02-26 13:18:17	2021-02-26 13:18:17
151	151	PB	Fija	Crítica	12	\N	MG	2021-02-26 13:23:59	2021-02-26 13:23:59
152	152	PB	Fija	Crítica	12	\N	MG	2021-02-26 13:24:35	2021-02-26 13:24:35
153	153	ST	CENTRAL FIJA	Óptima	18	\N	MG	2021-03-01 12:32:41	2021-03-01 12:32:41
149	149	ST	Fija	Óptima	17	\N	MG	2021-02-26 13:11:58	2021-03-01 13:13:11
155	155	PB	Fija	Óptima	17	\N	MG	2021-03-01 13:11:38	2021-03-01 13:26:43
154	154	ST	Fija	Óptima	18	\N	MG	2021-03-01 12:43:41	2021-03-01 13:30:16
156	156	ST	Fija	Óptima	17	\N	MG	2021-03-01 13:25:53	2021-03-01 13:34:43
131	131	PB	Fija	Óptima	16	\N	LIBRE	2021-02-23 12:22:06	2021-03-10 07:14:10
157	157	PB	Fija	Óptima	13	\N	PCM	2021-03-16 14:35:31	2021-03-16 14:35:31
158	158	PB	Fija	Óptima	13	\N	PCM	2021-03-16 14:43:19	2021-03-16 14:43:19
159	159	PH	Fija	Crítica	6	\N	ABA	2021-03-17 10:35:38	2021-03-17 10:35:38
160	160	PB	Fija	Óptima	12	\N	MG	2021-03-18 10:37:17	2021-03-18 10:37:17
161	161	PB	URL	Óptima	13	\N	FURGON	2021-03-18 10:52:39	2021-03-18 10:52:39
162	162	PB	Fija	Óptima	13	\N	PCM	2021-03-18 11:01:34	2021-03-18 11:01:34
163	163	PB	Fija	Óptima	12	\N	MG	2021-03-18 11:20:08	2021-03-18 11:20:08
164	164	PB	Fija	Óptima	12	\N	MG	2021-03-18 11:30:09	2021-03-18 11:30:09
165	165	PB	Fija	Óptima	19	\N	MG	2021-03-18 11:56:57	2021-03-18 11:56:57
166	166	PB	Fija	Óptima	19	\N	MG	2021-03-18 12:10:34	2021-03-18 12:10:34
167	167	PB	Fija	Óptima	13	\N	PCM	2021-03-18 12:32:05	2021-03-18 12:32:05
168	168	PB	Fija	Óptima	19	\N	MG	2021-03-18 12:37:57	2021-03-18 12:37:57
169	169	P4	Fija	Óptima	13	\N	DX	2021-03-18 13:09:52	2021-03-18 13:09:52
170	170	Unico piso	CENTRAL FIJA	Óptima	20	\N	TX	2021-03-18 13:25:12	2021-03-18 13:25:12
171	171	Unico piso	CENTRAL FIJA	Óptima	20	\N	TX	2021-03-18 13:40:35	2021-03-18 13:40:35
172	172	P4	Fija	Óptima	13	\N	DX	2021-03-18 13:46:33	2021-03-18 13:46:33
173	173	P4	Fija	Óptima	13	\N	DX	2021-03-18 13:55:13	2021-03-18 13:55:13
174	174	P4	Fija	Óptima	13	\N	DX	2021-03-18 13:59:26	2021-03-18 13:59:26
175	175	P4	Fija	Óptima	13	\N	SSP	2021-03-18 14:09:42	2021-03-18 14:09:42
176	176	P4	Fija	Óptima	13	\N	SSP	2021-03-18 14:14:30	2021-03-18 14:14:30
177	177	P4	Fija	Óptima	13	\N	SSP	2021-03-18 14:24:04	2021-03-18 14:24:04
178	178	PB	Fija	Crítica	21	\N	UMA	2021-03-19 11:16:21	2021-03-19 11:16:21
179	179	PB	Fija	Óptima	12	\N	PCM	2021-03-19 11:36:49	2021-03-19 11:36:49
180	180	Unico piso	NO CANTV	Crítica	21	\N	TX INTERNACIONAL	2021-03-19 12:25:16	2021-03-19 12:25:16
181	181	Unico piso	Fija	Crítica	21	\N	TX	2021-03-19 15:44:47	2021-03-19 15:44:47
182	182	Unico piso	Fija	Crítica	21	\N	TX	2021-03-19 15:45:06	2021-03-19 15:45:06
183	183	Unico piso	Fija	Crítica	21	\N	TX	2021-03-19 15:45:28	2021-03-19 15:45:28
184	184	Unico piso	Fija	Crítica	21	\N	TX	2021-03-19 15:51:55	2021-03-19 15:51:55
185	185	Unico piso	Fija	Crítica	21	\N	TX	2021-03-19 15:53:05	2021-03-19 15:53:05
186	186	Unico piso	Fija	Crítica	21	\N	TX	2021-03-19 15:54:19	2021-03-19 15:54:19
187	187	Unico piso	Fija	Crítica	21	\N	TX	2021-03-19 16:02:26	2021-03-19 16:02:26
188	188	Unico piso	Fija	Crítica	21	\N	TX	2021-03-19 16:08:56	2021-03-19 16:08:56
189	189	P4	Fija	Óptima	13	\N	SSP	2021-03-19 16:31:08	2021-03-19 16:31:08
190	190	PH	CENTRAL FIJA	Crítica	22	\N	ADSL	2021-03-22 11:56:47	2021-03-22 11:56:47
191	191	MZ	Fija	Crítica	6	\N	ABA	2021-03-22 12:59:23	2021-03-22 12:59:23
192	192	P2	Fija	Óptima	13	\N	DIGITAL	2021-03-22 13:01:28	2021-03-22 13:01:28
193	193	PH	Fija	Crítica	6	\N	ADSL	2021-03-22 13:01:56	2021-03-22 13:01:56
194	194	P1	Fija	Óptima	13	\N	OAC	2021-03-22 13:34:41	2021-03-22 13:34:41
195	195	PB	CENTRAL FIJA	Óptima	19	\N	MG	2021-03-22 17:29:29	2021-03-22 17:29:29
196	196	P2	DLC	Óptima	6	23	FURGON	2021-03-23 10:12:42	2021-03-23 10:12:42
197	197	P1	CENTRAL FIJA	Óptima	7	\N	CENTRO DE DISTRIBUCION	2021-03-23 10:33:17	2021-03-23 10:33:17
198	198	P1	CENTRAL FIJA	Óptima	7	\N	RECTIFICADORES	2021-03-23 10:36:08	2021-03-23 10:36:08
199	199	P1	CENTRAL FIJA	Óptima	7	\N	RECTIFICADORES	2021-03-23 10:38:03	2021-03-23 10:38:03
200	200	P1	CENTRAL FIJA	Óptima	7	\N	DSLAM	2021-03-23 10:40:03	2021-03-23 10:40:03
201	201	P1	CENTRAL FIJA	Óptima	7	\N	CX	2021-03-23 10:42:12	2021-03-23 10:42:12
202	202	P1	CENTRAL FIJA	Óptima	7	\N	CX	2021-03-23 10:43:56	2021-03-23 10:43:56
203	203	P1	CENTRAL FIJA	Óptima	7	\N	TX	2021-03-23 10:46:18	2021-03-23 10:46:18
204	204	P1	CENTRAL FIJA	Óptima	7	\N	UNICA	2021-03-23 10:48:13	2021-03-23 10:48:13
205	205	P1	Seleccione tipo de estructura	Óptima	7	\N	CX	2021-03-23 10:50:11	2021-03-23 10:50:11
206	206	P1	CENTRAL FIJA	Óptima	7	\N	PCM	2021-03-23 10:51:59	2021-03-23 10:51:59
207	207	P1	CENTRAL FIJA	Óptima	8	\N	CENTRO DE DISTRIBUCION	2021-03-23 10:54:26	2021-03-23 10:54:26
208	208	P1	CENTRAL FIJA	Óptima	8	\N	CX	2021-03-23 10:57:02	2021-03-23 10:57:02
209	209	P1	CENTRAL FIJA	Óptima	8	\N	PCM	2021-03-23 10:59:57	2021-03-23 10:59:57
\.


--
-- Data for Name: ups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ups (id, marca, modelo, serial, inventario_cant, cap_kva, carga_conectada, tension_salida, status_id, ubicacion_id, created_at, updated_at) FROM stdin;
1	POWERWARE	asdf123	no posee	1233	123123	1233	1233123	\N	\N	2021-01-15 14:03:39	2021-01-15 14:03:39
\.


--
-- Data for Name: ups_bb; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ups_bb (id, ups_id, bancob_id, ubicacion_id, tipo_id, operatividad, fecha_instalacion, criticidad, observaciones, garantia, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, name, last_name, email, cedula, departamento, cargo, email_verified_at, rol_id, active, int_fail, password, remember_token, created_at, updated_at, region_id) FROM stdin;
2	Antonio Rafael Carrasquero Florez	\N	acarra05@cantv.com.ve	\N	\N	\N	\N	4	1	1	$2y$10$5brVtGeOdO9Gw2BYzYnyFuaegvUYyFLGO27ellj0vXd98o.8ODPMi	\N	\N	\N	\N
6	Gabriel Graffe	\N	ggraff01@cantv.com.ve	\N	\N	\N	\N	1	1	1	$2y$10$DeiiH81vOW.fsEIH/djHveVHcIrAsVeJLA/rMcr6x7YpeLBSt5uC6	\N	\N	\N	\N
9	Joens Aranguren	\N	jarang01@cantv.com.ve	\N	\N	\N	\N	1	1	1	$2y$10$dHn1FjDoPXwcNZL/KpPYe.OwCs317MHVfwaDNMNaSdaP/UVJq6xCW	\N	\N	\N	\N
11	Luis Ramirez	\N	lramir13@cantv.com.ve	\N	\N	\N	\N	4	1	1	$2y$10$YUuTWI0BnfJkssNXuOmeMe.5Lp0A243ROxj7cmLtNe4wYIb3z3VmK	\N	\N	\N	\N
14	Monica Berra Gotto	\N	mberra01@cantv.com.ve	\N	\N	\N	\N	4	1	1	$2y$10$ZlnEcgz2rWCnc/XEM/VRfuoUupCREY2JegFEhbLvvFBs1rQPt8FHu	\N	\N	\N	\N
15	Ronald Porfirio Gerder Castillo	\N	rgerde01@cantv.com.ve	\N	\N	\N	\N	4	1	1	$2y$10$p855SOcRIyrAA32g3nT0U.3qTaDUfpIsD9tinPt5ZFYDXNCLfgJxq	\N	\N	\N	\N
16	Segundo Fernando Reyes Castano	\N	sreyes01@cantv.com.ve	\N	\N	\N	\N	4	1	1	$2y$10$PQyyFr6LwjL6U/x/Q1bYBeTZJ0qDObfqZ5YdycqvIIhqjIiY53/vO	\N	\N	\N	\N
10	Jose Benjamin Pena Leal	\N	jpenal04@cantv.com.ve	\N	\N	\N	\N	4	1	\N	$2y$10$PmUkiiPUnSceQULArVuGQekiP3mh1J0h7WI/ldI7Vl2fmPmgCf..6	\N	\N	2021-01-12 09:13:47	\N
5	Erick Pérez	\N	eperez45@cantv.com.ve	\N	\N	\N	\N	1	1	\N	$2y$10$2.In.9JMKPSgH2fqOorLAeWURag5RJHOx2oUbGnxQNhaVZ6.43bla	\N	\N	2021-01-12 09:59:31	\N
13	Milanyela Blanco	\N	mblan4@cantv.com.ve	\N	\N	\N	\N	5	2	1	$2y$10$sD7.0xhzKMZVKB/ZDM/u0OY0yddPI2XFZ0McyRmZPMMrDkmUTsxqS	\N	\N	2021-03-09 13:27:29	\N
8	joel diaz velasquez	\N	joeldiaz1805@gmail.com	\N	\N	\N	\N	1	1	\N	$2y$10$LarQxgSekJiOMOumdU9CYeQ7V8/9/HpX2eROC5IZpiMn0wfHpUu2C	\N	\N	2021-01-12 10:12:23	\N
22	Danny	Hurtado	dhurta01@cantv.com.ve	14645569	Gcia. Cost. Energía y Climatización	Supervisor	\N	6	1	\N	$2y$10$vRRVL67MXmQDn2.qzFhN..MuAlybau2fP22I0wlNUbNZVqFnCVX1y	\N	2021-01-12 15:28:43	2021-03-09 10:39:00	3
21	Francisco	Masso	fmasso01@cantv.com.ve	\N	Seleccione departamento	Seleccione un cargo	\N	1	1	\N	$2y$10$TRkffgxuMJ8TcmHFZsPGX.KYmmhfeHA0fbq/DprQto1oQNq8v01P6	\N	2021-01-12 11:49:46	2021-01-12 11:49:46	\N
1	Adriana González	\N	agonz3@cantv.com.ve	\N	\N	\N	\N	1	1	\N	$2y$10$YqagB2al5yLmKAPIvPpsOeFqPIkqsNCF.VLRr33VBHaWkgqt4rMzG	\N	\N	2021-01-12 17:14:35	\N
12	Michelle Ansidey	\N	mansid01@cantv.com.ve	\N	\N	\N	\N	1	1	\N	$2y$10$o.m1VI56PVk6C8b/Of7wXOmaUEp6n035w8Kme65Xl8L7f3ArRw.P.	\N	\N	2021-01-13 10:42:54	\N
3	Carlos Manuel Leon Rodriguez	\N	cleon4@cantv.com.ve	\N	\N	\N	\N	4	1	\N	$2y$10$GZbAuQXDyBBZi2ve/Rlim.tDSMSCsd8P2JoROLW9a/U2TFUJoKw9W	\N	\N	2021-01-13 12:33:29	\N
4	Deivinson Gabriel Soto Soto	\N	dsotos01@cantv.com.ve	\N	\N	\N	\N	4	1	1	$2y$10$CSdlwRC4.G5QtKkdF.3zmuSCbWdekmbKmT3bIfw0tQFFgfhOrDZ/C	\N	\N	2021-01-28 11:28:25	\N
35	Yarlin	Calderon	ycalde04@cantv.com.ve	21207095	Gcia. Oper. Mtto Reg. Los Andes	Especialista CCT	\N	4	1	\N	$2y$10$nLdjyc.lU7nXoJxMs8t2euT444V3kGUpckvs66k7AGmTw2ow2hm7e	\N	2021-01-22 15:22:25	2021-01-25 13:35:29	1
37	Alfonso	Nuñez	anunez02@cantv.com.ve	16015991	Gcia. Oper. Mtto Reg. Los Andes	Especialista CCT	\N	4	1	\N	$2y$10$eCaaNBZYEXH8V3xYIv7Fd.HClV5Y0ToMJ64o/x/EOpYL9EEYhaGI.	\N	2021-01-22 15:23:36	2021-01-25 13:31:19	1
38	Pedro	Leon	pleong01@cantv.com.ve	15584791	Gcia. Oper. Mtto Reg. Los Andes	Especialista CCT	\N	4	1	\N	$2y$10$4nYKgjOM0Sq76lQONjDR1uMJRVWCkMs6SJkjF1ljRlzBYkdT7xPuG	\N	2021-01-22 15:24:06	2021-01-25 13:30:19	1
33	Bladimir	Vergara	bverga@cantv.com.ve	8022179	Gcia. Oper. Mtto Reg. Los Andes	Especialista CCT	\N	4	1	\N	$2y$10$cgFULGOlLGdnf1y7SJupZ.OLpMAAeiUeqN1LpVtCIQlKeo0s197zW	\N	2021-01-22 15:21:18	2021-01-25 13:29:25	1
7	Giuseppe Arbore Blanco	\N	garbor01@cantv.com.ve	\N	\N	\N	\N	4	1	3	$2y$10$tOFVXQ0agQo9xdYYYFFM1e25qJlNFrqHZkaVuYRnxx1UizYupXJXy	\N	\N	2021-02-25 11:59:22	\N
28	Roxana	Uzcategui	ruzcat04@cantv.com.ve	23499585	Gcia. Oper. Mtto Reg. Los Andes	Especialista CCT	\N	4	1	\N	$2y$10$qeNf.cYEMKGdtcUmaQHLPOF3gLt90qUDrgR/PIRdDIZWNHlgNWM8G	\N	2021-01-22 15:14:34	2021-01-25 13:30:33	1
17	Simon	Subero	ssuber@cantv.com.ve	8345687	Gcia. Oper. Mtto Reg. Centro Oriente	Gerente	\N	4	1	\N	$2y$10$/1PXbADGGPK4a4oI3N392eHMhDM3FkR8LDLq6VQhY9LyfCz3P7wCm	\N	\N	2021-02-02 10:05:23	7
27	Ilich	Añez	ianez01@cantv.com.ve	21063772	Gcia. Oper. Mtto Reg. Los Andes	Gerente	\N	4	1	\N	$2y$10$c.XnkOX2.Rmw3Yh6TIhoRepqLGWK9i6kqZotf5sW52uoedEUU9M26	\N	2021-01-22 10:17:55	2021-01-25 13:31:34	1
36	Freddy	Briceño	fbric2@cantv.com.ve	5789174	Gcia. Oper. Mtto Reg. Los Andes	Especialista CCT	\N	4	1	\N	$2y$10$nP46NOTVaktNaIITkohC/eC/fuzcXIVmVBQy3XbdgOC2RrWtQYWDK	\N	2021-01-22 15:22:59	2021-01-25 13:34:48	1
26	Yohenys Nohely	Alvarez Solano	yalvar09@cantv.com.ve	26048182	Gcia. Oper. Mtto Reg. Guayana	Especialista CCT	\N	4	1	\N	$2y$10$9k.chrfFHdEHrmqaBxO8oeCzwuHwdBGiQ6d78r7AEEnKATHhIu5YO	\N	2021-01-18 10:22:47	2021-01-25 13:35:11	5
34	Richard	soler	rsoler02@cantv.com.ve	16371669	Gcia. Oper. Mtto Reg. Los Andes	Especialista CCT	\N	4	1	\N	$2y$10$zmCzHfVwlcNMDph/K6xChuCahVLkPgjTERAYWFlbyN.MlG92/3I06	\N	2021-01-22 15:21:53	2021-02-18 10:17:46	1
32	Jose	Diaz Quintero	jdiazq01@cantv.com.ve	13099302	Gcia. Oper. Mtto Reg. Los Andes	Especialista CCT	\N	4	1	\N	$2y$10$QY0ztClpLMSp7vHDYNmZVOK3DKRDLhzE8pN6GFCembNiFixi0d./W	\N	2021-01-22 15:20:43	2021-02-23 14:06:38	1
25	Luixandris	Hernandez	lherna48@cantv.com.ve	26359907	Gcia. Oper. Mtto Reg. Guayana	Asistente	\N	4	1	\N	$2y$10$IuyJ6WKPgLWIKUOoo.Fh0.Ao6jO7KPR0HHBy9p7SS7xOpMItcrwmq	\N	2021-01-18 10:17:20	2021-01-26 09:54:08	5
31	Jose Ivan	Arismendi	jarism01@cantv.com.ve	8047463	Gcia. Oper. Mtto Reg. Los Andes	Especialista CCT	\N	4	1	\N	$2y$10$DIQdxBSoyIjHhpZtXfW6f.MW/SzbsET89vCteRrlLc1QgC4Imvkqq	\N	2021-01-22 15:19:47	2021-01-25 13:36:29	1
30	Xiomara	sanabria	Xsanab01@cantv.com.ve	18772515	Gcia. Oper. Mtto Reg. Los Andes	Especialista CCT	\N	4	1	\N	$2y$10$wmoWGGYtdVDWNOTD.YcnFedQxgJTSu2XeDeGv0FqfAHdXrtGo9AeS	\N	2021-01-22 15:19:07	2021-01-25 13:36:47	1
29	José Alexander	Briceño Chacón	Jbrice23@cantv.com.ve	9241996	Gcia. Oper. Mtto Reg. Los Andes	Especialista CCT	\N	4	1	\N	$2y$10$o0drQI7HFY3JX6Es3kp64.c8BCOXr8YiYqqhhstGjj1JXhAHXLT.C	\N	2021-01-22 15:17:58	2021-01-25 13:37:03	1
24	Jeniree Alexandra	Mendez Gamez	jmende17@cantv.com.ve	18713003	Gcia. Oper. Mtto Reg. Centro Oriente	Asistente	\N	4	1	\N	$2y$10$lHjdsA00U/b48kjnM8CIje3VheyYkG.HaRGDJ3A1jiKPR52HijkN.	\N	2021-01-18 08:12:01	2021-01-25 13:37:37	7
18	Zailú Mejías	\N	zmejia01@cantv.com.ve	\N	\N	\N	\N	1	1	\N	$2y$10$8L8XI4FPdB2dey1L2nmUmeaXDGNgNB7c2MXkEuh7PF7GVF779Ybxi	\N	\N	2021-01-26 10:21:29	\N
20	Jesus Rafael	Medina Castillo	jmedin30@cantv.com.ve	\N	Gcia. Oper. Mtto Reg. Occidente	Tecnico	\N	4	1	\N	$2y$10$vTUCnWUhHuFFFMXxfgPDBe8dI2kRPVko5kntfLw459kttdY9FvE1C	\N	2021-01-12 10:04:29	2021-03-22 12:51:41	6
23	Eduin	Guerra	eguerr12@cantv.com.ve	\N	Coordinación Normas, Análisis y Diseño	Especialista CCT	\N	1	1	1	$2y$10$PqpznSpGQLcLWXf82r4rEuclxerdsk9eZpLONtJAr/TNgXP6XHSse	\N	2021-01-14 15:04:18	2021-03-17 09:03:42	\N
39	Hector Jose	Campos  Macuaran	hcampo@cantv.com.ve	9459049	Gcia. Oper. Mtto Reg. Centro Oriente	Coordinador	\N	4	1	\N	$2y$10$6SUldIDxDcRMbySKM0ie.O7BIy54w/qx3HZka61ayLTlq2ZYNEJhK	\N	2021-02-02 15:06:51	2021-02-02 15:06:51	7
41	Reinaldo Emmanuel	Cedeño Patiño	rceden01@cantv.com.ve	25623477	Gcia. Oper. Mtto Reg. Centro Oriente	Supervisor	\N	4	1	\N	$2y$10$fTH1SKP3m65RrFSzdXkKj.zhiIbLYR42uDS8CuD7X/IY2ALwpoj1S	\N	2021-02-02 15:10:06	2021-02-02 15:10:06	7
42	Luis Alberto	Medina	lmedin09@cantv.com.ve	14468770	Seleccione departamento	Supervisor	\N	4	1	\N	$2y$10$0Y9UHDpKNuMnnE1CuNNlEexcVea7pCntfGYnT.TmXusx2/Ep5l2Hu	\N	2021-02-02 15:11:38	2021-02-02 15:11:38	7
44	Jesus	Cordova	jcordo02@cantv.com.ve	15936101	Gcia. Oper. Mtto Reg. Centro Oriente	Coordinador	\N	4	1	\N	$2y$10$t3MJMvApU/7YGm/UT6moh.RqA/L.pEX/37FU8mgJ7z2xpyhnuQ.fO	\N	2021-02-02 15:28:03	2021-02-02 15:28:03	7
45	Ernesto	Velásquez	evelas07@cantv.com.ve	13522471	Gcia. Oper. Mtto Reg. Centro Oriente	Consultor CCT	\N	4	1	\N	$2y$10$92qUFN4/ypY.FI/x3zjDiupDt.xB1ckoz08TtxYFHJjZXkZfK/orW	\N	2021-02-02 15:29:21	2021-02-02 15:29:21	7
46	Richard	Hernández	rherna22@cantv.com.ve	13359220	Gcia. Oper. Mtto Reg. Centro Oriente	Supervisor	\N	4	1	\N	$2y$10$cZuwZH7Cf8RE/LMyeQKuNegffaay.MJTSPf.kvUSsocUbUumHi9UC	\N	2021-02-02 15:31:15	2021-02-02 15:31:15	7
47	Roberth	Teran	rteran01@cantv.com.ve	12435999	Gcia. Oper. Mtto Reg. Centro Oriente	Supervisor	\N	4	1	\N	$2y$10$135a0.9W3nvMs.p29jQ1gOi2Xe4IV4SZFlVCG7yUYheqGcMEoKUle	\N	2021-02-02 15:33:59	2021-02-02 15:33:59	7
48	Erasti	Núñez	enunez06@cantv.com.ve	12739249	Seleccione departamento	Seleccione un cargo	\N	4	1	\N	$2y$10$3SdFWA8FgbG.kz/MPyej9ei2J1VbTh1sas1CVuCYq8rJUznr64xvi	\N	2021-02-02 15:35:02	2021-02-02 15:35:02	7
49	Pedro	Diaz	pdiazl01@cantv.com.ve	14064087	Gcia. Oper. Mtto Reg. Centro Oriente	Seleccione un cargo	\N	4	1	\N	$2y$10$lw0TMPMPLMCQJYGCbgJgG.LbfyLARP/Yyufih/01dCwUOleIqogki	\N	2021-02-02 15:36:26	2021-02-02 15:36:26	7
50	Luis	Montaño	lmonta02@cantv.com.ve	15676993	Gcia. Oper. Mtto Reg. Centro Oriente	Coordinador	\N	4	1	\N	$2y$10$r8xgbQ90SYXbXkB4D98ao./ia7dwC5AGnkw0m8W/ulPreb.64KLti	\N	2021-02-02 15:38:26	2021-02-02 15:38:26	7
51	Gleudy	Alfonzo	galfon04@cantv.com.ve	17898725	Gcia. Oper. Mtto Reg. Centro Oriente	Supervisor	\N	4	1	\N	$2y$10$x0kaL9Y9E5gb3ssq/Q0mf.VRPaOb4xQLN3F53fVecSN7k08afu7t.	\N	2021-02-02 15:39:15	2021-02-02 15:39:15	7
52	Joel	Velasquez	jvelas07@cantv.com.ve	27352319	Gcia. Oper. Mtto Reg. Centro Oriente	Consultor CCT	\N	4	1	\N	$2y$10$Ekbx.GdgltxnRypk7z6EkuLSRhscn06XUDileu/bKL0QYRYzhTRZO	\N	2021-02-02 15:40:23	2021-02-02 15:40:23	7
54	Julio Antonio	González	jgonza154@cantv.com.ve	14439718	Gcia. Oper. Mtto Reg. Centro Oriente	Supervisor	\N	4	1	\N	$2y$10$kPnWHaRWQTlLsXnGpMZcBui9Y7ysCn5ekzXfmQ2KOze2V53wqJzcC	\N	2021-02-02 15:45:40	2021-02-02 15:45:40	7
55	José Gregorio	Chávez	jchav6@cantv.com.ve	13636851	Seleccione departamento	Consultor CCT	\N	4	1	\N	$2y$10$FFH0dynJioh6/HRIx0l0t.MvOdvuO2UBGcXYgmxpiIp946IyR8zse	\N	2021-02-02 15:46:33	2021-02-02 15:46:33	7
53	Ángel Gabriel	Muñoz	amunoz04@cantv.com.ve	15.883.672	Gcia. Oper. Mtto Reg. Centro Oriente	Supervisor	\N	4	1	\N	$2y$10$zjymxu6EnnrJWdpcV6sCC.A/5zGjrbbHrjElsL5ikt/WyvH2qYh2O	\N	2021-02-02 15:42:45	2021-02-08 11:47:26	7
43	Naryoli	Belmonte	nbelmo01@cantv.com.ve	15520392	Gcia. Oper. Mtto Reg. Centro Oriente	Consultor CCT	\N	4	1	\N	$2y$10$MHkIU0TS.oTaFBDybrM0s.tDQUTI/D5vp2Ockf13V6IMqpuiOeNa.	\N	2021-02-02 15:13:21	2021-02-08 13:56:27	7
19	David Daniel	Diaz Colina	ddiazc01@cantv.com.ve	\N	Gcia. Oper. Mtto Reg. Occidente	Especialista CCT	\N	4	1	\N	$2y$10$b6cd6DQWD.ihhucbVAMODefYEfB9ZL6StZuDK.V48dS.uPf5jkVk.	\N	2021-01-12 10:01:12	2021-03-12 10:49:22	6
57	Barbara	Aranguren	barang01@cantv.com.ve	18304872	Coordinación Seguimiento y Control de Gestión	Especialista CCT	\N	4	1	\N	$2y$10$M1PTdxR4FJVTLBkJHkWrpOEXopRy9JkVO6Yr5OorBiWnhE4c/Dcjq	\N	2021-03-17 15:06:00	2021-03-17 15:06:00	2
58	Michell	Torres	mtorre40@cantv.com.ve	14407699	Coordinación Seguimiento y Control de Gestión	Consultor CCT	\N	4	1	\N	$2y$10$EeZlExArF.K8MDtOCFBcd.S7HcLhwlsd6votlkLl5ZQu0vd4uiZiC	\N	2021-03-17 15:09:38	2021-03-17 15:09:38	2
40	Edward Jose	Suniaga Rivera	esunia@cantv.com.ve	11435011	Gcia. Oper. Mtto Reg. Centro Oriente	Supervisor	\N	4	1	\N	$2y$10$GXZjH4DJ.3xHfcEk3oBX8.XvGkx9QR3V3NGG.WtEyY9A6q3FlxRIS	\N	2021-02-02 15:09:02	2021-03-18 10:29:36	7
56	JUDITH	BERRIOS	jberr3@cantv.com.ve	5540845	Coordinación Normas, Análisis y Diseño	Coordinador	\N	6	1	\N	$2y$10$6BYA37pSWtrBEzuN5vgTxentuuMwp.IqazCNu4Y27AWHkFyvOGfwe	\N	2021-03-09 13:27:15	2021-03-22 14:01:41	\N
\.


--
-- Name: acciones_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.acciones_id_seq', 26, true);


--
-- Name: aire_acondicionado_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.aire_acondicionado_id_seq', 67, true);


--
-- Name: bateria_control_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.bateria_control_id_seq', 35, true);


--
-- Name: bco_baterias_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.bco_baterias_id_seq', 3, true);


--
-- Name: centrales_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.centrales_id_seq', 209, true);


--
-- Name: comentarios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.comentarios_id_seq', 1, false);


--
-- Name: componente_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.componente_id_seq', 41, true);


--
-- Name: contratista_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.contratista_id_seq', 1, false);


--
-- Name: cuadrilla_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cuadrilla_id_seq', 1, false);


--
-- Name: detallebypasopsut_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.detallebypasopsut_id_seq', 1, false);


--
-- Name: detalleopsutpivot_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.detalleopsutpivot_id_seq', 193, true);


--
-- Name: equipo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.equipo_id_seq', 24, true);


--
-- Name: estados_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.estados_id_seq', 34, true);


--
-- Name: failed_jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.failed_jobs_id_seq', 1, false);


--
-- Name: fallas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.fallas_id_seq', 65, true);


--
-- Name: generador_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.generador_id_seq', 118, true);


--
-- Name: invequipopsut_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.invequipopsut_id_seq', 68, true);


--
-- Name: localidad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.localidad_id_seq', 3063, true);


--
-- Name: mantenimiento_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.mantenimiento_id_seq', 73, true);


--
-- Name: menu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.menu_id_seq', 5, true);


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.migrations_id_seq', 47, true);


--
-- Name: motogenerador_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.motogenerador_id_seq', 108, true);


--
-- Name: motor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.motor_id_seq', 108, true);


--
-- Name: multimedia_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.multimedia_id_seq', 1, false);


--
-- Name: nodoopsut_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.nodoopsut_id_seq', 203, true);


--
-- Name: personal_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.personal_id_seq', 23, true);


--
-- Name: pisos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.pisos_id_seq', 32, true);


--
-- Name: plantillas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.plantillas_id_seq', 1, false);


--
-- Name: rect_bcobb_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rect_bcobb_id_seq', 2, true);


--
-- Name: rectificadores_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rectificadores_id_seq', 2, true);


--
-- Name: regiones_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.regiones_id_seq', 7, true);


--
-- Name: registro_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.registro_id_seq', 1, false);


--
-- Name: requeopsut_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.requeopsut_id_seq', 1, false);


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.roles_id_seq', 6, true);


--
-- Name: salas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.salas_id_seq', 37, true);


--
-- Name: status_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.status_id_seq', 1, false);


--
-- Name: sub_menu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sub_menu_id_seq', 7, true);


--
-- Name: sub_sub_menu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sub_sub_menu_id_seq', 10, true);


--
-- Name: tickets_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tickets_id_seq', 1, false);


--
-- Name: ubicacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ubicacion_id_seq', 209, true);


--
-- Name: ups_bb_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ups_bb_id_seq', 1, true);


--
-- Name: ups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ups_id_seq', 1, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 58, true);


--
-- Name: acciones acciones_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.acciones
    ADD CONSTRAINT acciones_pkey PRIMARY KEY (id);


--
-- Name: aire_acondicionado aire_acondicionado_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aire_acondicionado
    ADD CONSTRAINT aire_acondicionado_pkey PRIMARY KEY (id);


--
-- Name: bateria_control bateria_control_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bateria_control
    ADD CONSTRAINT bateria_control_pkey PRIMARY KEY (id);


--
-- Name: bco_baterias bco_baterias_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bco_baterias
    ADD CONSTRAINT bco_baterias_pkey PRIMARY KEY (id);


--
-- Name: centrales centrales_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.centrales
    ADD CONSTRAINT centrales_pkey PRIMARY KEY (id);


--
-- Name: comentarios comentarios_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comentarios
    ADD CONSTRAINT comentarios_pkey PRIMARY KEY (id);


--
-- Name: componente componente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.componente
    ADD CONSTRAINT componente_pkey PRIMARY KEY (id);


--
-- Name: contratista contratista_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contratista
    ADD CONSTRAINT contratista_pkey PRIMARY KEY (id);


--
-- Name: cuadrilla cuadrilla_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cuadrilla
    ADD CONSTRAINT cuadrilla_pkey PRIMARY KEY (id);


--
-- Name: detallebypasopsut detallebypasopsut_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detallebypasopsut
    ADD CONSTRAINT detallebypasopsut_pkey PRIMARY KEY (id);


--
-- Name: detalleopsutpivot detalleopsutpivot_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalleopsutpivot
    ADD CONSTRAINT detalleopsutpivot_pkey PRIMARY KEY (id);


--
-- Name: equipo equipo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.equipo
    ADD CONSTRAINT equipo_pkey PRIMARY KEY (id);


--
-- Name: estados estados_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estados
    ADD CONSTRAINT estados_pkey PRIMARY KEY (id);


--
-- Name: failed_jobs failed_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.failed_jobs
    ADD CONSTRAINT failed_jobs_pkey PRIMARY KEY (id);


--
-- Name: failed_jobs failed_jobs_uuid_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.failed_jobs
    ADD CONSTRAINT failed_jobs_uuid_unique UNIQUE (uuid);


--
-- Name: fallas fallas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fallas
    ADD CONSTRAINT fallas_pkey PRIMARY KEY (id);


--
-- Name: generador generador_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.generador
    ADD CONSTRAINT generador_pkey PRIMARY KEY (id);


--
-- Name: invequipopsut invequipopsut_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.invequipopsut
    ADD CONSTRAINT invequipopsut_pkey PRIMARY KEY (id);


--
-- Name: localidad localidad_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.localidad
    ADD CONSTRAINT localidad_pkey PRIMARY KEY (id);


--
-- Name: mantenimiento mantenimiento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mantenimiento
    ADD CONSTRAINT mantenimiento_pkey PRIMARY KEY (id);


--
-- Name: menu menu_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.menu
    ADD CONSTRAINT menu_pkey PRIMARY KEY (id);


--
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: motogenerador motogenerador_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.motogenerador
    ADD CONSTRAINT motogenerador_pkey PRIMARY KEY (id);


--
-- Name: motor motor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.motor
    ADD CONSTRAINT motor_pkey PRIMARY KEY (id);


--
-- Name: multimedia multimedia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.multimedia
    ADD CONSTRAINT multimedia_pkey PRIMARY KEY (id);


--
-- Name: nodoopsut nodoopsut_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nodoopsut
    ADD CONSTRAINT nodoopsut_pkey PRIMARY KEY (id);


--
-- Name: personal personal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.personal
    ADD CONSTRAINT personal_pkey PRIMARY KEY (id);


--
-- Name: pisos pisos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pisos
    ADD CONSTRAINT pisos_pkey PRIMARY KEY (id);


--
-- Name: plantillas plantillas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.plantillas
    ADD CONSTRAINT plantillas_pkey PRIMARY KEY (id);


--
-- Name: rect_bcobb rect_bcobb_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rect_bcobb
    ADD CONSTRAINT rect_bcobb_pkey PRIMARY KEY (id);


--
-- Name: rectificadores rectificadores_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rectificadores
    ADD CONSTRAINT rectificadores_pkey PRIMARY KEY (id);


--
-- Name: regiones regiones_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regiones
    ADD CONSTRAINT regiones_pkey PRIMARY KEY (id);


--
-- Name: registro registro_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.registro
    ADD CONSTRAINT registro_pkey PRIMARY KEY (id);


--
-- Name: requeopsut requeopsut_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.requeopsut
    ADD CONSTRAINT requeopsut_pkey PRIMARY KEY (id);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: salas salas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.salas
    ADD CONSTRAINT salas_pkey PRIMARY KEY (id);


--
-- Name: status status_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.status
    ADD CONSTRAINT status_pkey PRIMARY KEY (id);


--
-- Name: sub_menu sub_menu_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sub_menu
    ADD CONSTRAINT sub_menu_pkey PRIMARY KEY (id);


--
-- Name: sub_sub_menu sub_sub_menu_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sub_sub_menu
    ADD CONSTRAINT sub_sub_menu_pkey PRIMARY KEY (id);


--
-- Name: tickets tickets_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tickets
    ADD CONSTRAINT tickets_pkey PRIMARY KEY (id);


--
-- Name: ubicacion ubicacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ubicacion
    ADD CONSTRAINT ubicacion_pkey PRIMARY KEY (id);


--
-- Name: ups_bb ups_bb_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ups_bb
    ADD CONSTRAINT ups_bb_pkey PRIMARY KEY (id);


--
-- Name: ups ups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ups
    ADD CONSTRAINT ups_pkey PRIMARY KEY (id);


--
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);


--
-- PostgreSQL database dump complete
--

