@extends ('/layouts/index')

@section ('content')
<style>
    .inputpicker {width:225%;background:#f2f2f2}

    .oculto {width:200%;background:#f2f2f2;border-radius:0 0 10px 10px;padding:10px;overflow:auto;max-height:200px;display:none}
    .oculto ul {display:inline;float:left;width:100%;margin:0;padding:0}
    .oculto ul li {margin:0;padding:0;display:block;width:30px;height:30px;text-align:center;font-size:15px;font-family:"FontAwesome";float:left;cursor:pointer;color:#666;line-height:30px;transition:0.2s all}
    .oculto ul li:hover {background:#FFF;color:#000}
</style>

<div class="content-wrapper">
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>
            Sub Sub Menu 
          </h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Registro</a></li>
            <li class="breadcrumb-item active">Sub Sub Menu </li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12 col-sm-12">
          <div class="card card-primary card-tabs">
            <div class="card-body">
              <form method="POST" id="formulario_usuarios" enctype="multipart/form-data" name="formulario_usuarios"  action="/sub_submenu/{{encrypt($submenu->id)}}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PUT">
                <div class="col-md-12">
                  <div class="card card-success">
                    <div class="card-header">
                      <h3 class="card-title">Sub Sub Menu </h3>
                    </div>
                    <div class="card-body">
                      <div class="row" id="field_wrapper">
                        <div class="col-sm-6">
                          <div class="form-group">
                            <label>Nombre del Sub Sub Menu </label>
                            <input type="text" name="nombre" value="{{$submenu->nombre}}"  class="form-control" placeholder="Nombre de Sub Sub Menu ">
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="col-sm-4 picker">
                            <div class="form-group">
                              <label>Icono</label>
                              <input type="text" name="icon" value="{{$submenu->icon}}" readonly class="inputpicker form-control" placeholder="Haz click aqui para elegir tu icono preferido..." id="icono">
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-sm-6">
                          <div class="form-group">
                            <label>Url del Sub Menu</label>
                            <input type="text" name="url" value="{{$submenu->url}}"  class="form-control" placeholder="Nombre de Menù">
                          </div>
                        </div>
                      </div>
                      
                      <div class="row">
                        <div class="col-sm-6">
                          <div class="form-group">
                            <label>Sub Menu Padre</label>
                            <select name="sub_menu_id" class="form-control select2">
                              
                              @foreach($sub_sub  as $sub_menu)
                                <option @if($sub_menu->id==$submenu->sub_menu_id)) selected @endif value="{{$sub_menu->id}}" >{{$sub_menu->nombre}}</option>
                                
                              @endforeach
                            </select>                            
                            </div>
                          </div>
                           <div class="col-sm-6">
                          <div class="form-group">
                            <label>Roles Asignado:</label>
                            <div class="form-check" data-children-count="1">
                              @foreach($roles as $rol)
                                <input @if(in_array($rol->id,$idmenus)) checked @endif type="checkbox" class="form-check-input" value="{{$rol->id}}" name="rol_id[]" id="examp{{$rol->id}}"><label class="form-check-label" for="exampleCheck1">{{$rol->denominacion}}</label>
                                <br>
                              @endforeach
                            </div>
                          </div>
                        </div>
                        </div>
                      </div>
                    </div>
                  <div class="card-footer" align="right">
                    <button type="submit" class="btn btn-primary">Actualizar</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

@endsection
@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_usuarios") !!}
@endif
 <script>
        $(function () {
            $(".picker").each(function()
            {
                div=$(this);
                if (icos)
                {
                    var iconos="<ul>";
                    for (var i=0; i<icos.length; i++) { iconos+="<li><i data-valor='"+icos[i]+"' class='fa "+icos[i]+"'></i></li>"; }
                    iconos+="</ul>";
                }
                div.append("<div class='oculto'>"+iconos+"</div>");
                $(".inputpicker").click(function()
                {
                    $(".oculto").fadeToggle("fast");
                });
                $(document).on("click",".oculto ul li",function()
                {
                    $(".inputpicker").val($(this).find("i").data("valor"));
                    $(".oculto").fadeToggle("fast");
                });
            });
        });
    </script>



<script type="text/javascript">




$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection





