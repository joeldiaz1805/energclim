@extends ('/layouts/index')

@section ('content')
<style>
    .inputpicker {width:225%;background:#f2f2f2}

    .oculto {width:200%;background:#f2f2f2;border-radius:0 0 10px 10px;padding:10px;overflow:auto;max-height:200px;display:none}
    .oculto ul {display:inline;float:left;width:100%;margin:0;padding:0}
    .oculto ul li {margin:0;padding:0;display:block;width:30px;height:30px;text-align:center;font-size:15px;font-family:"FontAwesome";float:left;cursor:pointer;color:#666;line-height:30px;transition:0.2s all}
    .oculto ul li:hover {background:#FFF;color:#000}
</style>

<div class="content-wrapper">
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>
           Sub Sub Menu
          </h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Registro</a></li>
            <li class="breadcrumb-item active">Sub Sub Menu</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12 col-sm-12">
          <div class="card card-primary card-tabs">
            <div class="card-body">
            	<form method="POST" id="formulario_usuarios" enctype="multipart/form-data" name="formulario_usuarios"  action="{{ url('/sub_submenu')}}">
                {{ csrf_field() }}
               	<div class="col-md-12">
				          <div class="card card-success">
				            <div class="card-header">
				              <h3 class="card-title">Sub Menu</h3>
			              </div>
			              <div class="card-body">
			              	<div class="row" id="field_wrapper">
                        <div class="col-sm-6">
                          <div class="form-group">
                            <label>Nombre del Sub Menu</label>
                            <input type="text" name="nombre"  class="form-control" placeholder="Nombre de Menù">
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="col-sm-4 picker">
                            <div class="form-group">
                              <label>Icono</label>
                              <input type="text" name="icon" readonly class="inputpicker form-control" placeholder="Haz click aqui para elegir tu icono preferido..." id="icono">
                            </div>
                          </div>
                        </div>
				              </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <div class="form-group">
                            <label>Url del Sub Menu</label>
                            <input type="text" name="url"  class="form-control" placeholder="Nombre de Menù">
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <div class="form-group">
                            <label>Asignar Sub Menu Padre:</label>
                              <select name="sub_menu_id" class="form-control select2">
                                @foreach($sub_menu as $sub)
                                  <option value="{{$sub->id}}">{{$sub->nombre}}</option>
                                @endforeach
                              </select>
                          </div>
                        </div>
                      </div>
				            </div>
		                <div class="card-footer" align="right">
			                <button type="submit" class="btn btn-primary">Guardar</button>
			              </div>
			            </form>
                </div>
		          </div>
		        </div>
          </div>
        </div>
  	  </section>
    </div>


@endsection
@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_usuarios") !!}
@endif
 <script>
        $(function () {
            $(".picker").each(function()
            {
                div=$(this);
                if (icos)
                {
                    var iconos="<ul>";
                    for (var i=0; i<icos.length; i++) { iconos+="<li><i data-valor='"+icos[i]+"' class='fa "+icos[i]+"'></i></li>"; }
                    iconos+="</ul>";
                }
                div.append("<div class='oculto'>"+iconos+"</div>");
                $(".inputpicker").click(function()
                {
                    $(".oculto").fadeToggle("fast");
                });
                $(document).on("click",".oculto ul li",function()
                {
                    $(".inputpicker").val($(this).find("i").data("valor"));
                    $(".oculto").fadeToggle("fast");
                });
            });
        });
    </script>



<script type="text/javascript">

   
    var addButton = $('#add_button'); //Add button selector
    var wrapper = $('#field_wrapper'); //Input field wrapper
    var fieldHTML = '<div class="col-sm-6"><div class="form-group"><label>Nombre o Descripciòn</label><input type="text" name="nombre[]"  class="form-control" placeholder="Nombre de Proveedor"><a href="javascript:void(0);" id="remove_button" class="remove_button" title="Remove field"><i class="fas fa-trash" style="color:red;"></i></a></div></div><div class="col-sm-6"><div class="form-group"><label>Codigo de Proveedor</label><input type="text" name="cod_proveedor[]"  class="form-control" placeholder="Codigo de Proveedor"></div></div>'; //New input field html 
    $(addButton).click(function(){ //Once add button is clicked
        
            $(wrapper).append(fieldHTML); // Add field html
        
    });
    $(wrapper).on('click', '#remove_button', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });


$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection





