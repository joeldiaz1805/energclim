@extends ('/layouts/index')

@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Banco Bateria 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inventario</a></li>
              <li class="breadcrumb-item active">Banco Bateria</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Vista Preliminar de Registros</h3>
              <div align="right">
                  <a href="/bancob/create" class="btn btn-success">Nuevo Registro</a>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class=" display nowrap  table table-bordered table-striped" style="width:100%">
                <thead>
                  <tr align="center" >
                    <th class="not-export-col">Acciones</th>
                    <th>Región</th>
                    <th>Estado</th>
                    <th>Central</th>
                    <th>Estructura</th>
                    <th>Sala</th>
                    <th>Piso</th>
                    <th>Responsable</th>
                   
                    <th>Marca Banco de Baterías</th>
                    <th>Modelo Banco de Baterías</th>
                    <th>Serial Banco de Baterías</th>
                    <th>Inventario CANTV Banco de Baterías</th>
                    <th>Capacidad (Amp/AmpH)</th>
                    <th>Carga de la Localidad</th>
                    <th>Numero de celdas</th>
                    <th>Numero de Celdas Dañadas</th>
                    <th>Voltaje nominal DC(p/c)</th>
                    <th>Horas de Respaldo (hrs)</th>
                    <th>Tablero de Control</th>
                    <th>Tipo Rack</th>
                    <th>Tiempo vida Util</th>
                    <th>Operatividad</th>
                    <th>Porcentaje Operativo</th>
                    <th>Fecha de Instalación</th>
                    <th>Criticidad del Equipo</th>
                    <th>Garantía</th>
                    <th>Observaciones</th>
                    <th>Fecha Actualización</th>
                    <th>Usuario</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($bancob as $rect)
                  <tr align="center" id="tdId_{{$rect->id}}">
                    <td>
                      <a href="/bancob/{{encrypt($rect->id)}}/edit" class="btn btn-warning"><i class="far fa-edit"></i></a>
                      <a href="/bancob/{{encrypt($rect->id)}}" class="btn btn-success"><i class="far fa-eye"></i></a>
                      <a  onclick="deleteItem('{{$rect->id}}')"  class="btn btn-danger"><i class="fa fa-times"></i></a>
                    </td>
                    <td>@php try { echo $rect->ubicacion->central->region->nombre;}catch(Exception $e){echo 'Inexistente';} @endphp </td>
                    <td>@php try { echo  $rect->ubicacion->central->estados->nombre;}catch(Exception $e){echo 'Inexistente';} @endphp</td>
                    <td>@php try { echo  $rect->ubicacion->central->localidad->nombre;}catch(Exception $e){echo 'Inexistente';} @endphp</td>
                    <td>@php try { echo   $rect->ubicacion->central->localidad->tipo;}catch(Exception $e){echo 'Inexistente';} @endphp</td>
                    <td>@php try { echo   $rect->ubicacion->sala;}catch(Exception $e){echo 'Inexistente';} @endphp</td>
                    <td>@php try { echo   $rect->ubicacion->piso;}catch(Exception $e){echo 'Inexistente';} @endphp</td>
                    <td>
                      {{$rect->ubicacion->responsable->nombre  ?? 'no existe'}}
                    </td>
               
                    <td>{{$rect->marca}}</td>
                    <td>{{$rect->modelo}}</td>
                    <td>{{$rect->serial}}</td>
                    <td>{{$rect->inventario_cant}}</td>
                    <td>{{$rect->cap_kva}}</td>
                    <td>{{$rect->carga_total_bb}}</td>
                    <td>{{$rect->elem_oper_bb}}</td>
                    <td>{{$rect->elem_inoper_bb}}</td>
                    <td>{{$rect->amp_elem}}</td>
                    <td>{{$rect->autonomia_respaldo}}</td>
                    <td>{{$rect->tablero}}</td>
                    <td>{{$rect->tipo_rack}}</td>
                    <td>{{$rect->tiempo_util}}</td>
                    <td>{{$rect->operatividad}}</td>
                    <td>
                         @if($rect->operatividad =='Falla' && $rect->porc_falla)
                            {{$rect->porc_falla}}%
                      @else
                       @if($rect->operatividad == "Inoperativo" ||  $rect->operatividad == "Vandalizado" ) 0% @else  100% @endif 
                       
                      @endif

                    </td>




                    <td>{{$rect->fecha_instalacion}}</td>
                    <td>{{$rect->criticidad}}</td>
                    <td>{{$rect->garantia}}</td>
                    <td>{{$rect->observaciones}}</td>
                    <td>{{$rect->updated_at}}</td>
                    <td>{{$rect->usuario->name ?? 'Sin Cambios'}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
           </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('scriptsalerts')


<script>
   @if(Session::has('exito'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Banco Bateria Guardado con exito'
      })
      })
    });
  @endif
   @if(Session::has('actualizado'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Datos actualizados con exito'
      })
      })
    });
  @endif
  
</script>

  <script>
    function deleteItem(item){
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Desea eliminar este banco de bateria del intentario?',
      text: "Estas por quitar este equipo del Inventario!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, quitarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {


      if (result.value) {



            $.ajax({
                url: "/bancob/"+item,
                type:"DELETE",
                   headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    

                    success:function(result, status, xhr){                    
                        
                        if (status == 'success') {

                          swalWithBootstrapButtons.fire(
                            'Quitado con Exito!',
                            'Se eliminó con exito',
                            'success'
                          )

                          $("#tdId_"+item).hide();
                         // txt.innerHTML = result ;



                        }
                    },
                    error:function(xhr, status, error){
                      

                      Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se eliminó el producto, vuelva a intentarlo mas tarde '+error,
                        footer: '<a href>Seguramente hubo un problema de conexión</a>'
                      })
                      

                    }
                });
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No se ha quitado el producto.',
          'error'
        )
      }
    })
  }
</script>


@endsection