@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Banco de Bateria
              <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Rectificador','Un Rectificador es un dispositivo electrónico que permite convertir la corriente alterna en corriente continua.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a> 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inventario</a></li>
              <li class="breadcrumb-item active">Banco de Bateria</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
      <div class="row ">
	    <div class="col s12 m12 l12 ">
	      @if ($errors->any())
	        <div class="alerta-errores">
	          <ul>
	            @foreach ($errors->all() as $error)
	              <li>{{ $error }}</li>
	            @endforeach
	          </ul>
	        </div>
	      @endif
	    </div>
	  </div>

    <!-- Main content -->
    <section class="content">
      	<div class="container-fluid">
	        <div class="row">
         		<div class="col-12 col-sm-12">
            		<div class="card card-primary card-tabs">
              			<div class="card-body">
	              			<form method="POST" id="formulario_rect" name="formulario_rect"  action="{{url('/bancob')}}">
	                        	{{ csrf_field() }}
              	 				<input type="hidden" name="bco_baterias[user_id]" value="{{Auth::user()->id}}">
	                        		@if($parametros != null)
				                    <?php

				                    	$localidad = App\Models\Models\localidades::where('id',decrypt($parametros['url']))->first();
				                    	# code...
				                    
				                    ?>
				                    <input type="hidden" name="parametro" value="{{$parametros['url']}}">
				  	 				<input type="hidden" name="central[estado_id]" value="{{$localidad->estado_id}}">
				  	 				<input type="hidden" name="central[region_id]" value="{{$localidad->estados->region_id}}">
				  	 				<input type="hidden" name="central[localidad_id]" value="{{decrypt($parametros['url'])}}">

				  	 				@endif
				                <div class="col-md-12" @if($parametros != null) style="display: none" @endif>
							        <div class="card card-success">
						              	<div class="card-header">
						                	<h3 class="card-title">Ubicación y Responsable</h3>
						              	</div>
							              <!-- /.card-header -->
						              	<div class="card-body">
						                	<div class="card-header">
								                <h3 class="card-title">Ubicación</h3>
								            </div>
								              <br>

											<div class="row">
							                  	<div class="col-sm-4">
							                      	<div class="form-group">
							                        	<label>Región:</label>
							                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Región','Seleccione región donde está ubicado el rectificador', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        	<select @if($parametros != null) disabled @endif  name="central[region_id]" id="region" class="form-control">
								                        		<option>Seleccione una Región</option>
									                        		@foreach($regiones as $region)
										                        		<option value="{{$region->id}}">{{$region->nombre}}</option>
										                        	@endforeach
									                    	</select>
								                    </div>
							                    </div>
							                    <div class="col-sm-4">
								                      <div class="form-group">
								                        <label>Estado:</label>
								                        <a href="#" @if($parametros != null) disabled @endif onmouseover="getMouseXY( event ); showHelp( 'help_popup','Estado','Seleccione estado donde está ubicado el rectificador', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        <select @if($parametros != null) disabled @endif name="central[estado_id]" id="estado" class="form-control">
								                        	
									                       		<option value="">Seleccione un estado</option>
									                        
								                    	</select>
								                      </div>
								                </div>
						                   
							                    <div class="col-sm-4">
							                      	<div class="form-group">
							                        	<label>Central:</label>
							                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Central','Seleccione central donde está ubicado el rectificador', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        	<select @if($parametros != null) disabled @endif name="central[localidad_id]" id="central" class="form-control">
										                       	<option value="">Seleccione una Central</option>
									                    	</select>
								                    </div>
							                    </div>
						                 	</div>
					                 		<div class="row">
					                 			<div class="col-sm-4">
							                      	<div class="form-group">
							                        	<label>Piso:</label>
							                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Piso','Introduzca piso donde se encuentra el rectificador. Solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        	<select name="ubicacion[piso]" id="box_ModeloRectificador" class="form-control">
							                        			<option value="">Seleccione un piso</option>
																@foreach($pisos as $piso)
								                            		<option value="{{$piso->nombre}}">{{$piso->nombre}}</option>
								                            	@endforeach
															</select>
								                    </div>
						                      	</div>

					                 			<div class="col-sm-4">
							                      	<div class="form-group">
							                        	<label>Sala o espacio físico:</label>
							                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Sala','Seleccione sala donde está alojado el rectificador', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        	<select name="ubicacion[sala]" id="box_ModeloRectificador" class="form-control">
								                        		<option value="">Seleccione una Sala</option>
																@foreach($salas as $sala)
								                            		<option value="{{$sala->nombre}}">{{$sala->nombre}}</option>
								                            	@endforeach
															</select>
							                      	</div>
						                    	</div>

							                  	<div class="col-sm-4">
							                      <!-- text input -->
							                      	<div class="form-group">
							                        	<label>Estructura:</label>
							                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Estructura','Tipo de infraestrcutura tecnológica donde está alojado el rectificador', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        	<select name="ubicacion[estructura]" required class="form-control">
						                        				<option>Seleccione tipo de estructura</option>
						                            			<option value="CASETA DE TRANSMISION">CASETA DE TRANSMISION</option>
						                            			<option value="CENTRAL CRITICA">CENTRAL CRITICA</option>
							                        			<option  value="CENTRAL FIJA">CENTRAL FIJA</option>
							                        			<option  value="URL">URL</option>
							                        			<option  value="NODO INDOOR">NODO INDOOR</option>
							                        			<option  value="NODO OUTDOOR">NODO OUTDOOR</option>
							                        			<option  value="GTI">GTI</option>
							                        			<option  value="OPSUT">OPSUT</option>
							                        			<option  value="REPETIDORA">REPETIDORA</option>
							                        			<option  value="SUPER AULA">SUPER AULA</option>
							                        			<option  value="OAC">OAC</option>
							                        			<option  value="DLC">DLC</option>
							                        			<option  value="NO CANTV">NO CANTV</option>
							                    			</select>
								                    </div>
							                    </div>
					                 		</div>

											<div class="row">
												<div class="col-sm-4">
							                      <!-- text input -->
							                      	<div class="form-group">
							                        	<label>Criticidad del Espacio:</label>
							                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Criticidad del Espacio','Introduzca Criticidad del Espacio', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        	<select name="ubicacion[criticidad_esp]" required class="form-control">
					                            				<option>Seleccione criticidad del espacio</option>
																<option value="Crítica">Crítica</option>
																<option value="Óptima">Óptima</option>
						                    				</select>
								                    </div>
							                      </div>
											</div>
											<!--
											<div class="card-header" style="display: none">
								               <h3 class="card-title">Responsable Banco de Baterias</h3>
								            </div>
							            		<br>
							                <div class="row" style="display: none">
							                    <div class="col-sm-4">
							                      <div class="form-group">
							                        <label>Nombres:</label>
							                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Nombres','Nombre persona encargada y responsable del Banco de Baterias', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                        <input type="text" class="form-control" required name="responsable[nombre]" placeholder="Ej: Pedro...">

							                      </div>
							                    </div>
							                    <div class="col-sm-4">
							                     
							                      <div class="form-group">
							                        <label>Apellidos:</label>
							                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Apellidos','Apellido persona encargada y responsable del Banco de Baterias', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                        <input type="text" class="form-control" required name="responsable[apellido]" placeholder="Ej: Tortoza...">

							                      </div>
							                    </div>
							                    <div class="col-sm-4">
							          
							                      <div class="form-group">

							                        <label>Cédula:</label>
							                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cédula','Número de cédula', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                        <input type="text" class="form-control" required name="responsable[cedula]" placeholder="Introduzca cédula">

							                      </div>
							                    </div>
							                </div>
					                  
				                  			<div class="row" style="display: none">

						                  		<div class="col-sm-4">
							                      <div class="form-group">

							                        <label>P00:</label>
							                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','P00','Número interno que identifica al trabajador de la empresa. Ej. 112233', 75 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                        <input type="text" class="form-control" required name="responsable[cod_p00]" placeholder="Introduzca P00">

							                      </div>
							                    </div>
						                  		<div class="col-sm-4">
							                      <div class="form-group">
							                        <label>Número de Oficina:</label>
							                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Número de Oficina','Número de Telf.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                        <div class="input-group">
									                    <div class="input-group-prepend">
									                      <span class="input-group-text"><i class="fas fa-phone"></i></span>
									                    </div>
									                    <input type="text" name="responsable[num_oficina]" required  class="form-control" data-inputmask='"mask": "99999999999"' data-mask>
									                  </div>
							                      </div>
							                    </div>

							                    <div class="col-sm-4">
							                      <div class="form-group">
							                        <label>Número Personal:</label>
							                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Número Personal','Número de Telf.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                      <div class="input-group">
									                    <div class="input-group-prepend">
									                      <span class="input-group-text"><i class="fas fa-phone"></i></span>
									                    </div>
									                    <input type="text" name="responsable[num_personal]" required  class="form-control" data-inputmask='"mask": "99999999999"' data-mask>
									                  </div>
								                    </div>
							                    </div>
				                  		
				                  			</div>

						                  	<div class="row" style="display: none">
						                  		<div class="col-sm-4">
							              
							                      <div class="form-group">

							                        <label>Cargo:</label>
							                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cargo','Denominación del cargo de la persona encargada del Banco de Baterias.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                        <input type="text" class="form-control" required name="responsable[cargo]" placeholder="Introduzca cargo">

							                      </div>
							                    </div>
						                  		<div class="col-sm-8">
					
						                      		<div class="form-group">

						                        		<label>Correo:</label>
						                        		<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Correo','Dirección de correo electrónico. Ej. empleado@cantv.com.ve', 300 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
						                        		<input type="email" name="responsable[correo]" required class="form-control" placeholder="Introduzca correo corporativo/personal">
						                        		
						                      		</div>
						                    	</div>
						                  	</div>
						                  	<div class="card-header" style="display: none">
								               <h3 class="card-title">Supervisor Banco de Baterias</h3>
								            </div>
							            <br>
							                <div class="row" style="display: none">
							                    <div class="col-sm-4">
							                      <div class="form-group">
							                        <label>Nombres:</label>
							                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Nombres','Nombre persona supervisor Banco de Baterias', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                        <input type="text" class="form-control" required name="supervisor[nombre]" placeholder="Ej: Pedro...">

							                      </div>
							                    </div>
							                    <div class="col-sm-4">
							                     
							                      <div class="form-group">
							                        <label>Apellidos:</label>
							                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Apellidos','Apellido persona supervisor del Banco de Baterias', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                        <input type="text" class="form-control" required name="supervisor[apellido]" placeholder="Ej: Tortoza...">

							                      </div>
							                    </div>
							                     <div class="col-sm-4">
							                
							                      <div class="form-group">

							                        <label>Cédula:</label>
							                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cédula','Número de cédula supervisor', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                        <input type="text" class="form-control" required name="supervisor[cedula]" placeholder="Introduzca cédula">

							                      </div>
							                    </div>
							                </div>
					                  
				                  			<div class="row" style="display: none">
						                  		<div class="col-sm-4">
							                      <div class="form-group">
							                        <label>P00:</label>
							                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','P00','Número interno que identifica al supervisor del Banco de Baterias. Ej. 112233', 75 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                        <input type="text" class="form-control" required name="supervisor[cod_p00]" placeholder="Introduzca P00">

							                      </div>
							                    </div>
						                  		<div class="col-sm-4">
							                      <div class="form-group">
							                        <label>Número de Oficina:</label>
							                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Número de Oficina','Número de Telf. supervisor', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                        <div class="input-group">
									                    <div class="input-group-prepend">
									                      <span class="input-group-text"><i class="fas fa-phone"></i></span>
									                    </div>
									                    <input type="text" name="supervisor[num_oficina]" required  class="form-control" data-inputmask='"mask": "99999999999"' data-mask>
									                  </div>
							                      </div>
							                    </div>

							                    <div class="col-sm-4">
							                     	<div class="form-group">
								                        <label>Número Personal:</label>
								                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Número Personal','Número de Telf. supervisor', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
									                    <div class="input-group">
										                  	<div class="input-group-prepend">
										                      <span class="input-group-text"><i class="fas fa-phone"></i></span>
										                    </div>
										                    <input type="text" name="supervisor[num_personal]" required  class="form-control" data-inputmask='"mask": "99999999999"' data-mask>
										                </div>
								                    </div>
							                    </div>
				                  		
				                  			</div>

						                  	<div class="row" style="display: none">
						                  		<div class="col-sm-4">
							                     
							                      <div class="form-group">

							                        <label>Cargo:</label>
							                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cargo','Denominación del cargo del supervisor del Banco de Baterias.', 75 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                        <input type="text" class="form-control" required name="supervisor[cargo]" placeholder="Introduzca cargo">

							                      </div>
							                    </div>
						                  		<div class="col-sm-8">
						                      		<div class="form-group">
						                        		<label>Correo:</label>
						                        		<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Correo','Dirección de correo electrónico supervisor. Ej. empleado@cantv.com.ve', 300 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
						                        		<input type="email" name="supervisor[correo]" required class="form-control" placeholder="Introduzca correo corporativo/personal">
						                        		
						                      		</div>
						                    	</div>
						                  	</div>
									-->
										</div>
									</div>
								</div>
				             	
						        <div class="col-md-12">
					            	<div class="card card-success">
							            <div class="card-header">
							                <h3 class="card-title">Banco de Baterías - Banco de Baterias</h3>
							            </div>
						              	<div class="card-body">
						               
						                  	<div class="row">
												<div class="col-sm-4">
							                      <div class="form-group">
							                        <label>Marca:</label>
							                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Marca','Seleccionar fabricante de la batería', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                        <select name="bco_baterias[marca]" class="form-control">
						                            <option value=""></option>
													@foreach($marcas as $mar)
														<option value="{{$mar->descripcion}}">{{$mar->descripcion}}</option>
													@endforeach				
									            	</select>
							                      </div>
							                    </div>
							                    <div class="col-sm-4">
							                      	<div class="form-group">
							                        	<label>Modelo:</label>
							                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Modelo Batería','Caracteres y números que lo identifican Ej. BA202011112525', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        <input type="text" name="bco_baterias[modelo]"  class="form-control" placeholder="Introduzca Modelo">
								                    </div>
							                    </div>
												<div class="col-sm-4">
							                      	<div class="form-group">
							                        	<label>Serial:</label>
							                        	<input type="checkbox" name="bco_baterias[no_posee]" id="no_posee_bb">No Posee
							                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Serial','de Batería. Secuencia de números y/o caracteres', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        <input type="text" id="serial_bb" name="bco_baterias[serial]"  class="form-control" placeholder="Introduzca Serial">
								                    </div>
							                  	</div>
							                 </div>

							                <div class="row">
							                    <div class="col-sm-4">
							                      <!-- text input -->
								                    <div class="form-group">
								                    	<label>Inventario Cantv:</label>
								                    	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Inventario Cantv','Código interno de inventario en Cantv. Ej. 223344', 150 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        <input type="text" class="form-control" name="bco_baterias[inventario_cant]"  placeholder="Introduzca Inventario Cantv">
								                    </div>
							                    </div>

							                    <div class="col-sm-4">
						                      <!-- text input -->
							                      	<div class="form-group">
							                        	<label>Capacidad Amp/AmpH:</label>
							                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Capacidad KVA','kilovoltiamperio. Solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                        	<input type="text" name = "bco_baterias[cap_kva]" class="form-control" placeholder="Introduzca Capacidad Amp/AmpH">
							                      	</div>
							                    </div>
												<div class="col-sm-4">
							                      <!-- text input -->
							                      	<div class="form-group">
							                        	<label>Carga de la Localidad:</label>
							                       	 	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Carga total en Banco de Batería','Suma de todas las cargas de las baterías. kilovoltiamperio. Solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                        	<input type="text" name = "bco_baterias[carga_total_bb]" class="form-control" placeholder="Introduzca Carga de la Localidad">
							                      	</div>
							                    </div>
											</div>
											<div class="row">
							                    <div class="col-sm-4">
							                      <!-- text input -->
							                      	<div class="form-group">
							                        	<label>Numero de Celdas:</label>
							                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cantidad de Elementos Operativos','Solo números sin letras. Ej. 1, 10, 20, etc.', 150 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                        	<input type="text" class="form-control" name="bco_baterias[elem_oper_bb]"  placeholder="Introduzca Numero de Celdas">
							                      	</div>
							                    </div>

								                <div class="col-sm-4">
							                      <!-- text input -->
							                      	<div class="form-group">
								                        <label>Numero de Celdas Dañadas:</label>
								                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cantidad de Elementos Criticos','Solo números sin letras. Ej. 1, 10, 20, etc.', 150 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        <input type="text" name = "bco_baterias[elem_inoper_bb]" class="form-control" placeholder="Introduzca Numero de Celdas Dañadas">
							                      	</div>
							                    </div>
							                    <div class="col-sm-4">
							                      <!-- text input -->
							                      <div class="form-group">
							                       		<label>Voltaje nominal DC por celda:</label>
							                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Voltaje por celda','Introduzca un número. Ej. 20, 30,100, etc. , solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                        	<input type="text" name = "bco_baterias[amp_elem]" class="form-control" placeholder="Introduzca Voltaje nominal DC por celda">
							                      </div>
							                    </div>
											
											</div>
											<div class="row">
												
												<div class="col-sm-4">
							                      <!-- text input -->
							                      	<div class="form-group">
								                        <label>Tipo de rack:</label>
								                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Tipo de rack','Introduzca un número. Ej. Vertical, Horizontal. , solo letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        <input type="text" name = "bco_baterias[tipo_rack]" class="form-control" placeholder="Introduzca Tipo de rack">
								                    </div>
							                    </div>
							                 
											
												<div class="col-sm-4">
							                      <!-- text input -->
							                      	<div class="form-group">
								                        <label>Horas de Respaldo:</label>
								                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Horas de Respaldo','Introduzca un número. Ej. 20, 30,100, etc. , solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        <input type="text" class="form-control" name="bco_baterias[autonomia_respaldo]"  placeholder="Introduzca Horas de Respaldo">
							                      	</div>
							                    </div>
												
							                    <div class="col-sm-4">
									                <div class="form-group">
									                  	<label>¿Posee Tablero de Control?</label>
									                  	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Tablero de Control','¿Banco Baterías tiene tablero de control?', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        <div class="form-check">
								                          	<input class="form-check-input" type="radio" value="si" name="bco_baterias[tablero]">
								                          	<label class="form-check-label">Si</label>
								                        </div>
								                        <div class="form-check">
								                          	<input class="form-check-input" type="radio" value="no" name="bco_baterias[tablero]">
								                          	<label class="form-check-label">No</label>
								                    	</div>				                        
								                   	</div>
								                </div>
											</div>
										</div>
					        		</div>
					        	</div>
				              	<div class="col-md-12">
						            <div class="card card-success">
						              	<div class="card-header">
						                	<h3 class="card-title">Instalación Banco de Baterias</h3>
						              	</div>
						              	<div class="card-body">
		 				                  	<div class="row">
							                    <div class="col-sm-4">
							                      	<div class="form-group">
							                        	<label>Operatividad:</label>
							                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Operatividad','Seleccione el estado en que se encuentra el Banco de Baterias', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        <select name="bco_baterias[operatividad]" id="operatividad" class="form-control">
							                            	<option value="">Indique operatividad del equipo</option>
																						<option value="Optimo">Optimo</option>
																						<option value="Critico">Critico</option>
																						<option value="Vandalizado">Vandalizado</option>
																						<option value="Deficiente">Deficiente</option>
																						<option value="Desincorporado">Desincorporado</option>
														</select>
							                      	</div>
							                    </div>
							                    <div class="col-sm-4">
							                      	<div class="form-group">
							                        	<label>Fecha de Instalación:</label>
							                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Fecha de Instalación','del Banco de Baterias', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        	<input id="box_ModeloRectificador" name="bco_baterias[fecha_instalacion]" class="form-control" type="date">
								                    </div>
							                    </div>
							                    <div class="col-sm-4">
								                    <div class="form-group">
								                        <label>Criticidad del Equipo:</label>
								                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Criticidad del Equipo','Selecione criticidad Banco de Baterias. Por Ej. para su atención inmediata o posterior', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        <select name="bco_baterias[criticidad]" class="form-control">
							                            	<option value="">Indique criticidad del equipo</option>
															<option value="Alta">Alta</option>
															<option value="Media">Media</option>
															<option value="Baja">Baja</option>
														</select>
							                      	</div>
							                    </div>
						                 	</div>
						                 	<div class="row" id="falla" style="display: none">
							                 	<div class="col-sm-4">
						                 			<label>Porcentaje Operatividad:</label>
						                 			<div class="form-group">
								                        <input name="bco_baterias[porc_falla]" onblur="falla()" id="input_falla" disabled type="text" value="">
							                 		</div>
							                 	</div>
							                 </div>




						                 	<div class="row">
						                  		<div class="col-sm-4">
						                      		<div class="form-group">
							                        	<label>¿Equipo en Garantía?</label>
							                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Garantía','¿Tiene o está vigente la garantía del Banco de Baterias?', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                        	<div class="form-check">
									                        <input class="form-check-input" type="radio" value="si" name="bco_baterias[garantia]">
									                        <label class="form-check-label">Si</label>
									                    </div>
									                    <div class="form-check">
									                        <input class="form-check-input" type="radio" value="no" name="bco_baterias[garantia]">
									                        <label class="form-check-label">No</label>
									                    </div>				                    
									                </div>
						                    	</div>
						                    	<div class="col-sm-4">
							                      <!-- text input -->
							                      <div class="form-group">
							                        <label>Tiempo de vida util en años:</label>
							                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Tiempo de vida util en años','Introduzca un número. Ej. 20, 30,100, etc. , solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                        <input type="text" name = "bco_baterias[tiempo_util]" class="form-control" placeholder="Introduzca Tiempo de vida util en años">
							                      </div>
							                    </div>
						                	</div>

							                <div class="form-group">
												<label for="comment">Observaciones:</label>
												<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Observaciones','Información complementaria sobre seguimientos e incidencias a destacar. Solo de la instalación del Banco de Baterias', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
												<textarea class="form-control" name="bco_baterias[observaciones]" rows="3" id="comment"></textarea>			
											</div>
						              	</div>
						            </div>
				          		</div>
				              	<div class="card-footer" align="right">
									<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
								</div>
							</form>
						</div>
            		</div>
        		</div>
    		</div>
		</div>
	</section>
</div>

@endsection


@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_rect") !!}
@endif



<script type="text/javascript">
$("#no_posee_rect").click(function(){
	var no_posee_rect = document.getElementById('no_posee_rect');
	var serial_rect = document.getElementById('serial_rect');
	if(no_posee_rect.checked){
        	//$('#serial_gen').hide();
        	serial_rect.value="no posee"
        	$('#serial_rect').prop('readonly', true);
 		}
 	if(!no_posee_rect.checked){
        	//$('#serial_gen').hide();
        	serial_rect.value=""
        	$('#serial_rect').prop('readonly', false);
 		}

 	});



 	 $("#no_posee_bb").click(function(){
	var no_posee_bb = document.getElementById('no_posee_bb');
	var serial_bb = document.getElementById('serial_bb');
	if(no_posee_bb.checked){
        	//$('#serial_gen').hide();
        	serial_bb.value="no posee"
        	$('#serial_bb').prop('readonly', true);
 		}
 	if(!no_posee_bb.checked){
        	//$('#serial_gen').hide();
        	serial_bb.value=""
        	$('#serial_bb').prop('readonly', false);
 		}

 	});

 function falla(){
  	var falla = document.getElementById('input_falla')
  	var range = document.getElementById('range')
  	var opera = document.getElementById('operatividad')
  	console.log(falla.value)

  	range.value == falla.value
  	if(falla.value < 1){
  		opera.value == "Critico"
  	}else if(falla.value>99){
  		opera.value == "Optimo"
  	}

  }

 $('select[id=operatividad]').change(function(){
 	var valor = $(this).val();
 	//console.log(valor)
 	if (valor == "Deficiente"){
 		$('#falla').show();
		$('#input_falla').prop('disabled',false);
 	}else{
		$('#falla').hide();
		$('#input_falla').prop('disabled',true);

 	}



 });

$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });






  	$('select[id=region]').change(function () {
    	console.log('hola')
    var region_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'estado/' + region_id, function (cidades) {
        $('select[id=estado]').empty();

        if(cidades.length != 0){
            $('select[id=estado]').append('<option value=""> Selecciona un estado</option>');
            $.each(cidades, function (key, value) {
                $('select[id=estado]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });

    });


	$('select[id=estado]').change(function () {
    	//console.log('hola')
    var estado_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'central/' + estado_id, function (cidades) {
        $('select[id=central]').empty();

        if(cidades.length != 0){
            $('select[id=central]').append('<option value=""> Selecciona un central</option>');
            $.each(cidades, function (key, value) {
                $('select[id=central]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });

    });



});
</script>
@endsection
