@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Banco Bateria 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inventario</a></li>
              <li class="breadcrumb-item active">Banco Bateria</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
       <div class="row ">
      <div class="col s12 m12 l12 ">
          @if ($errors->any())
              <div class="alerta-errores">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif
      </div>
  </div>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
     
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
              
              <div class="card-body">
                <form method="POST" id="formulario_bco_baterias" name="formulario_bco_baterias"  action="/bancob/{{encrypt($bancob->id)}}">
                        {{ csrf_field() }}
                  <input type="hidden" name="_method" value="PUT">
                  <input type="hidden" name="bco_baterias[user_id]" value="{{Auth::user()->id}}">

                 @if($parametros != null)
                           
                            <input type="hidden" name="parametro" value="{{$parametros['url']}}">
                    
                    @endif
                    
                  <div class="col-md-12">
                    <div class="card card-success">
                      <div class="card-header">
                        <h3 class="card-title">Ubicación y Responsable</h3>
                      </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                      <div class="card-header">
                        <h3 class="card-title">Ubicación</h3>
                      </div>
                      <br>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="form-group">
                            <label>Región:</label>
                            <select name="central[region_id]" class="form-control">
                              @foreach($regiones as $region)
                                <option @if($region->id == $bancob->ubicacion->central->region->id) selected @endif value="{{$region->id}}">{{$region->nombre}}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="form-group">
                            <label>Estado:</label>
                            <select name="central[estado_id]" class="form-control">
                              @foreach($estados as $estado)
                                <option @if($estado->id == $bancob->ubicacion->central->estados->id) selected @endif value="{{$estado->id}}">{{$estado->nombre}}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="form-group">
                            <label>Central:</label>
                              <select name="central[localidad_id]" class="form-control">
                                @foreach($localidades as $central)
                                  <option @if($central->id == $bancob->ubicacion->central->localidad->id) selected @endif value="{{$central->id}}">{{$central->nombre}}</option>
                                @endforeach
                              </select>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="form-group">
                            <label>Estructura:</label>
                              <select name="ubicacion[estructura]" class="form-control">
                                <option @if($bancob->ubicacion->estructura="CASETA DE TRANSMISION") selected @endif  value="CASETA DE TRANSMISION">CASETA DE TRANSMISION</option>
                                <option @if($bancob->ubicacion->estructura="CENTRAL CRITICA") selected @endif value="CENTRAL CRITICA">CENTRAL CRITICA</option>
                                <option @if($bancob->ubicacion->estructura="CENTRAL FIJA") selected @endif value="CENTRAL FIJA">CENTRAL FIJA</option>
                                <option @if($bancob->ubicacion->estructura="URL") selected @endif value="URL">URL</option>
                                <option @if($bancob->ubicacion->estructura="NODO INDOOR") selected @endif  value="NODO INDOOR">NODO INDOOR</option>
                                <option @if($bancob->ubicacion->estructura="NODO OUTDOOR") selected @endif value="NODO OUTDOOR">NODO OUTDOOR</option>
                                <option @if($bancob->ubicacion->estructura="GTI") selected @endif value="GTI">GTI</option>
                                <option  @if($bancob->ubicacion->estructura="OPSUT") selected @endif value="OPSUT">OPSUT</option>
                                <option  @if($bancob->ubicacion->estructura="REPETIDORA") selected @endif value="REPETIDORA">REPETIDORA</option>
                                <option  @if($bancob->ubicacion->estructura="SUPER AULA") selected @endif value="SUPER AULA">SUPER AULA</option>
                                <option @if($bancob->ubicacion->estructura="OAC") selected @endif value="OAC">OAC</option>
                                <option @if($bancob->ubicacion->estructura="DLC") selected @endif value="DLC">DLC</option>
                                <option @if($bancob->ubicacion->estructura="NO CANTV") selected @endif  value="NO CANTV">NO CANTV</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="form-group">
                            <label>Sala:</label>
                            <select name="ubicacion[sala]" id="box_Modelobancob" name="box_Modelobancob" class="form-control">
                              <option value="">Seleccione una sala</option>
                                @foreach($salas as $sala)
                                  <option @if($sala->nombre == $bancob->ubicacion->sala) selected @endif value="{{$sala->nombre}}">{{$sala->nombre}}</option>
                                @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="form-group">
                            <label>Piso:{{$bancob->ubicacion->piso}}</label>
                            <select name="ubicacion[piso]" id="box_Modelobancob"   class="form-control">
                              <option value="">Seleccione un piso   </option>
                                @foreach($pisos as $piso)
                                  <option @if($piso->nombre == $bancob->ubicacion->piso) selected @endif  value="{{$piso->nombre}}">{{$piso->nombre}}</option>
                                @endforeach
                            </select>
                            <!--<input class="form-control" value="{{$bancob->ubicacion->piso}}" required>-->
                          </div>
                        </div>
                      </div>
                      
                    </div>
                  </div>
                <div class="col-md-12">
                      <div class="card card-success">
                        <div class="card-header">
                          <h3 class="card-title">Banco de Baterías - Cuadro de Fuerza</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">


                              <div class="col-sm-4">
                                <!-- text input -->
                                <div class="form-group">
                                  <label>Marca</label>
                                  <select name="bco_baterias[marca]" class="form-control">
                                    @foreach($marcas as $mar)
                                        <option @if($mar->descripcion == $bancob->marca) selected @endif value="{{$mar->descripcion}}">{{$mar->descripcion}}</option>
                                    @endforeach
                                  </select>
                                </div>
                              </div>
                              <div class="col-sm-4">
                                <!-- text input -->
                                <div class="form-group">
                                  <label>Modelo</label>
                                  <input type="text" name="bco_baterias[modelo]"  value="{{$bancob->modelo}}" class="form-control" placeholder="Introduzca Modelo">
                                </div>
                              </div>

                              <div class="col-sm-4">
                                <div class="form-group">
                                  <label>Serial</label>
                                  <input type="text" name="bco_baterias[serial]"  value="{{$bancob->serial}}" class="form-control" placeholder="Introduzca Serial">
                                </div>
                              </div>
                            </div>

                            <div class="row">
                             <div class="col-sm-4">
                              <!-- text input -->
                              <div class="form-group">
                                <label>Inventario Cantv</label>
                                <input type="text" class="form-control" name="bco_baterias[inventario_cant]" value="{{$bancob->inventario_cant}}" placeholder="Introduzca Inventario Cantv">
                              </div>
                            </div>

                            <div class="col-sm-4">
                              <!-- text input -->
                              <div class="form-group">
                                <label>Capacidad Amp/AmpH:</label>
                                <input type="text" name = "bco_baterias[cap_kva]" value="{{$bancob->cap_kva}}" class="form-control" placeholder="Introduzca Capacidad KVA">
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <!-- text input -->
                              <div class="form-group">
                                <label>Carga de la Localidad:</label>
                                <input type="text" name = "bco_baterias[carga_total_bb]"  value="{{$bancob->carga_total_bb}}" class="form-control" placeholder="Introduzca Carga Total en bancob">
                              </div>
                            </div>

                          </div>
                          <div class="row">
                            <div class="col-sm-4">
                              <!-- text input -->
                              <div class="form-group">
                                <label>Numero de Celdas:</label>
                                <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cantidad de Elementos Operativos','Solo números sin letras. Ej. 1, 10, 20, etc.', 150 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                <input type="text" class="form-control" value="{{$bancob->elem_oper_bb}}" name="bco_baterias[elem_oper_bb]"  placeholder="Introduzca Numero de Celdas">
                              </div>
                            </div>
                            <div class="col-sm-4">
                                <!-- text input -->
                                <div class="form-group">
                                  <label>Numero de Celdas Dañadas:</label>
                                  <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cantidad de Elementos Inoperativos','Solo números sin letras. Ej. 1, 10, 20, etc.', 150 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                  <input type="text" name = "bco_baterias[elem_inoper_bb]" value="{{$bancob->elem_inoper_bb}}" class="form-control" placeholder="Introduzca Numero de Celdas Dañadas">
                                </div>
                              </div>
                              <div class="col-sm-4">
                                <!-- text input -->
                                <div class="form-group">
                                  <label>Voltaje nominal DC por celda:</label>
                                  <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Voltaje por celda','Introduzca un número. Ej. 20, 30,100, etc. , solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                  <input type="text" name = "bco_baterias[amp_elem]" value="{{$bancob->amp_elem}}" class="form-control" placeholder="Introduzca Voltaje nominal DC por celda">
                                </div>
                              </div>
                          </div>
                          <div class="row">
                        
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Tipo de rack:</label>
                                <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Tipo de rack','Introduzca un número. Ej. Vertical, Horizontal. , solo letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                <input type="text" value="{{$bancob->tipo_rack}}" name = "bco_baterias[tipo_rack]" class="form-control" placeholder="Introduzca Tipo de rack">
                              </div>
                            </div>
                           
                      
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Horas de Respaldo:</label>
                                <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Horas de Respaldo','Introduzca un número. Ej. 20, 30,100, etc. , solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                <input type="text" value="{{$bancob->autonomia_respaldo}}" class="form-control" name="bco_baterias[autonomia_respaldo]"  placeholder="Introduzca Horas de Respaldo">
                              </div>
                            </div>
                        </div>
                      <div class="row">

                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>¿Posee Tablero de Control?</label>
                              <div class="form-check">
                                <input class="form-check-input" @if($bancob->tablero=="si") checked @endif type="radio" value="si" name="bco_baterias[tablero]">
                                <label class="form-check-label">Si</label>
                              </div>
                              <div class="form-check">
                                <input class="form-check-input" @if($bancob->tablero=="no") checked @endif type="radio" value="no" name="bco_baterias[tablero]">
                                <label class="form-check-label">No</label>
                              </div>                                
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>
                    <!-- /.card-body -->
                  </div>

                  <div class="col-md-12">
                  <div class="card card-success">
                    <div class="card-header">
                      <h3 class="card-title">Instalación Cuadro de Fuerza</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                          <div class="col-sm-4">
                            <!-- text input -->
                            <div class="form-group">
                              <label>Operatividad:</label>
                              <select name="bco_baterias[operatividad]" id="operatividad" class="form-control">
                                  <option @if($bancob->operatividad=="Optimo")selected @endif value="Optimo">Optimo</option>
                                  <option @if($bancob->operatividad=="Critico")selected @endif value="Critico">Critico</option>
                                  <option @if($bancob->operatividad=="Vandalizado")selected @endif value="Vandalizado">Vandalizado</option>
                                  <option @if($bancob->operatividad=="Deficiente")selected @endif value="Deficiente">Deficiente</option>
                                  <option @if($bancob->operatividad=="Desincorporado")selected @endif value="Desincorporado">Desincorporado</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <!-- text input -->
                              <div class="form-group">
                                <label>Fecha de Instalación:</label>
                                  <input name="bco_baterias[fecha_instalacion]" value="{{$bancob->fecha_instalacion}}" id="box_Modelobancob" class="form-control" type="date">
                            </div>
                            </div>
                         
                            <div class="col-sm-4">
                            <!-- text input -->
                            <div class="form-group">
                              <label>Criticidad del Equipo:</label>
                              <select name="bco_baterias[criticidad]" class="form-control">
                                  <option @if($bancob->criticidad=="Alta")selected @endif
                                  value="Alta">Alta</option>
                                  <option @if($bancob->criticidad=="Media")selected @endif
                                  value="Media">Media</option>
                                  <option @if($bancob->criticidad=="Baja")selected @endif
                                  value="Baja">Baja</option>
                              </select>
                            </div>
                          </div>
                       </div>

                       <div class="row" id="falla" @if($bancob->operatividad != "Deficiente") style="display: none" @endif>
                            <div class="col-sm-4">
                              <label>Porcentaje Operatividad:</label>
                              <div class="form-group">
                                    <input name="bco_baterias[porc_falla]" onblur="falla()" id="input_falla" disabled type="text" value="{{$bancob->porc_falla}}">
                              </div>
                            </div>
                           </div>
                       <div class="row">
                          <div class="col-sm-4">
                            <!-- text input -->
                              <div class="form-group">
                                <label>¿Equipo en Garantía?</label>
                                  <div class="form-check">
                                      <input class="form-check-input" type="radio" @if($bancob->garantia=="si") checked @endif value="si" name="bco_baterias[garantia]">
                                      <label class="form-check-label">Si</label>
                                    </div>
                                    <div class="form-check">
                                      <input class="form-check-input" type="radio" @if($bancob->garantia=="no") checked @endif value="no" name="bco_baterias[garantia]">
                                      <label class="form-check-label">No</label>
                                    </div>                            
                                </div>
                          </div>
                          <div class="col-sm-4">
                                    <!-- text input -->
                                    <div class="form-group">
                                      <label>Tiempo de vida util en años:</label>
                                      <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Tiempo de vida util en años','Introduzca un número. Ej. 20, 30,100, etc. , solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                      <input type="text" value="{{$bancob->tiempo_util}}" name = "bco_baterias[tiempo_util]" class="form-control" placeholder="Introduzca Tiempo de vida util en años">
                                    </div>
                                  </div>
                      </div>

                      <div class="form-group">
                        <label for="comment">Observaciones:</label>
                          <textarea class="form-control" rows="3" name="bco_baterias[observaciones]" id="comment">{{$bancob->observaciones}}</textarea>      
                      </div>
             
                    </div>
                    <!-- /.card-body -->
                  </div>
                    


                      <div class="card-footer" align="right">
                        <button type="submit" class="btn btn-primary">Actualizar</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection

@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_bco_baterias") !!}
@endif



<script type="text/javascript">
$(document).ready(function () {
 function falla(){
    var falla = document.getElementById('input_falla')
    var range = document.getElementById('range')
    var opera = document.getElementById('operatividad')
    console.log(falla.value)

    range.value == falla.value
    if(falla.value < 1){
      opera.value == "Critico"
    }else if(falla.value>99){
      opera.value == "Optimo"
    }

  }

 $('select[id=operatividad]').change(function(){
  var valor = $(this).val();
  //console.log(valor)
  if (valor == "Deficiente"){
    $('#falla').show();
    $('#input_falla').prop('disabled',false);
  }else{
    $('#falla').hide();
    $('#input_falla').prop('disabled',true);

  }



 });



  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection


