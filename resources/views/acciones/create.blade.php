@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Acciones 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Registro</a></li>
              <li class="breadcrumb-item active">Acciones</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
     
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
              
              <div class="card-body">
              	 <form method="POST" id="formulario_usuarios" enctype="multipart/form-data" name="formulario_usuarios"  action="{{ url('/acciones')}}">
                        {{ csrf_field() }}
                     	<div class="col-md-12">
				            <!-- general form elements disabled -->
				            <div class="card card-success">
				              <div class="card-header">
				                <h3 class="card-title">Acciones</h3>
                        <!--div align="right">
                          
                        <a href="javascript:void(0);" id="add_button" class="btn btn-warning" title="Add field"><i class="fas fa-plus"></i></a>
                        </div-->
				              </div>
				              <!-- /.card-header -->
				              <div class="card-body">
				              	 <div class="row" id="field_wrapper">


				                    


                            <div class="col-sm-6">
                                <div class="form-group">
                                  <label>Nombre o Descripciòn</label>
                                    <input type="text" name="nombre"  class="form-control" placeholder="Nombre de Acción">

                                    <label>Tipo Fluido</label>
                                    <div class="form-check" required>
                                      <input class="form-check-input" type="radio"  value="1" name="tipo_fluido">
                                      <label class="form-check-label">Si</label><br>

                                      <input class="form-check-input" type="radio"  value="2" name="tipo_fluido">
                                      <label class="form-check-label">No</label><br>
                                  </div>
                              </div>
                             </div>
                              <div class="col-sm-6">
                              <label>Actividad a Relacionar:</label>
                                <div class="form-group">
                                  <div class="form-check" required>
                                    <input class="form-check-input" type="radio" id="programado" value="1" name="actividad[prog]">
                                    <label class="form-check-label">Programado</label><br>

                                    <input class="form-check-input" type="radio" id="atencion" value="2" name="actividad[atencion]">
                                    <label class="form-check-label">Atencion de Fallas</label><br>

                                    <input class="form-check-input" type="radio" id="gestion" value="3" name="actividad[gestion]">
                                    <label class="form-check-label">Gestion</label><br>

                                    <input class="form-check-input" type="radio" id="no_programado" value="4" name="actividad[n_prog]">
                                    <label class="form-check-label">No Programado</label>

                                    </div>
                                  </div>
                                 </div>


                            
				                   
									           

	                       </div>
				            </div>
				        </div>
		                 	 <div class="card-footer" align="right">
			                  <button type="submit" class="btn btn-primary">Crear</button>
			                </div>
			                </form>
                        </div>
		            </div>
		        </div>
            </div>
        </div>
  	</section>
</div>


@endsection
@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_usuarios") !!}
@endif



<script type="text/javascript">


  
   
    var addButton = $('#add_button'); //Add button selector
    var wrapper = $('#field_wrapper'); //Input field wrapper
    var fieldHTML = '<div class="col-sm-12"><div class="form-group"><label>Nombre o Descripciòn</label><input type="text" name="nombre[]"  class="form-control" placeholder="Nombre de Acción"><a href="javascript:void(0);" id="remove_button" class="remove_button" title="Remove field"><i class="fas fa-trash" style="color:red;"></i></a><label>Tipo Fluido</label><div class="form-check" required><input class="form-check-input" type="radio"  value="1" name="tipo_fluido[]"><label class="form-check-label">Si</label><br><input class="form-check-input" type="radio"  value="2" name="tipo_fluido[]"><label class="form-check-label">No</label><br></div></div></div>'; //New input field html 
    $(addButton).click(function(){ //Once add button is clicked
        
            $(wrapper).append(fieldHTML); // Add field html
        
    });
    $(wrapper).on('click', '#remove_button', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });


$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection





