<!DOCTYPE html>
<html lang="es-es" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title>.::Gerencia General Energía y Climatización::.</title>

    <!--JQUERY-->
    <script src="{{asset('static/Js/jquery.min.js')}}"></script>
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
    <!-- FRAMEWORK BOOTSTRAP para el estilo de la pagina-->
    <link rel="stylesheet" href="{{asset('static/css/bootstrap.min.css')}}">
    <script src="{{asset('static/Js/bootstrap.min.js')}}"></script>
    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    
    <!-- Los iconos tipo Solid de Fontawesome-->
        <!-- -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css">
    <script src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>

    <style type="text/css">
        body{
            background: url(../img/background.jpg) no-repeat center center fixed;
            background-size: cover;
        }
    </style>
</head>

<body>
    <div class="banner">
        <img src="{{asset('images/banner.png')}}" width="500" height="70">
        <h2>Bienvenido al Portal Web Interactivo</h2>
	</div>
    <div class="modal-dialog text-center">
        <div class="col-sm-8 main-section">
            <div class="modal-content">
                <div class="col-12 user-img">
                    <img src="{{asset('static/img/user.png')}}" />
                    </div>
                        <form class="col-12" th:action="@{/login}" method="get">
                            <div class="form-group" id="user-group">
                                <input type="text" class="form-control" placeholder="Email" name="username"/>
                            </div>
                            <div class="form-group" id="contrasena-group">
                                <input type="password" class="form-control" placeholder="Contraseña" name="password"/>
                            </div>
                            
                            <button onclick="window.location.href='form-menu.html'" type="submit" class="btn btn-primary"><i class="fas fa-sign-in-alt"></i>Ingresar</button>
                            
                        </form>


                        <div class="col-12 forgot">
                            <a href="#">Recordar contrasena?</a>
                        </div>
                        
                    <div>               
		    </div>
        </div>
    </div>
    </div>
    <div class="footer">
        <p>GGEC/GSC - Todos los derechos reservados &copy;</p>
        <p>Sitio desarrollado por la Coordinación de Normas, Análisis y Diseños</p>
    </div>
</body>
</html>
