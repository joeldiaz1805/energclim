@extends ('/layouts/index')

@section ('content')
<div class="content-wrapper">
    <section class="content-header">
	    <div class="container-fluid">
        	<div class="row mb-2">
          		<div class="col-sm-6">
            		<h1>
              		Registro de Actividades por Región 
            		</h1>
          		</div>
		        <div class="col-sm-6">
		            <ol class="breadcrumb float-sm-right">
		              <li class="breadcrumb-item"><a href="/home">Inicio</a></li>
		              <li class="breadcrumb-item active">Registro de Actividades</li>
		            </ol>
		        </div>
        	</div>
      	</div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      	<div class="container-fluid">
	        <div class="row">
	          	<div class="col-12 col-sm-12">
	            	<div class="card card-primary card-tabs">
	              		<div class="card-body">
			              	<form method="POST" id="formulario_registro" name="formulario_registro"  action="{{ url('/registro_actividades')}}">
	                        	{{ csrf_field() }}
	                        	<input type="hidden" name="user_id" value="{{Auth::user()->id}}">
		                     	<div class="col-md-12">
						            <div class="card card-primary">
						              	<div class="card-header">
					                		<h3 class="card-title">Nueva Actividad</h3>
					              		</div>
					              		<div class="card-body">
					                		<div class="row">
								                <div class="col-sm-4">
					                				<label>Tipo de Actividad:</label>
					                  				<div class="col-sm-12">
								                      	<div class="form-group">
								                        	<div class="form-check" required>
									                          <!--<input class="form-check-input" onblur="generar()" id="checkbox1" type="radio" required value="Preventivo" name="tipo_mantenimiento">-->
									                          <input class="form-check-input" type="radio" value="Preventivo" name="tipo_actividad">
									                          <label class="form-check-label">Mantenimiento Preventivo</label><br>
									                          <!--<input class="form-check-input" onblur="generar()" id="checkbox2" type="radio" required value="Correctivo"  name="tipo_mantenimiento">-->
									                          <input class="form-check-input" type="radio" value="Correctivo" name="tipo_actividad">
									                          <label class="form-check-label">Mantenimiento Correctivo</label><br>
									                          <!--<input class="form-check-input" onblur="generar()" id="checkbox3" type="radio" required value="Inspeccion"  name="tipo_mantenimiento">-->
									                          <input class="form-check-input" type="radio" value="Inspección" name="tipo_actividad">
									                          <label class="form-check-label">Inspección</label><br>
									                        </div>
						                    			</div>
					                    			</div>
					                 			</div>
								                <div class="col-sm-4">
								                	<label>Tipo de Servicio:</label>
								                  	<div class="col-sm-4">
								                      	<div class="form-group">
								                        	<div class="form-check" required>
									                          <input class="form-check-input" type="radio" id="tipo_servicio" value="Ex" name="tipo_servicio">
									                          <label class="form-check-label">Ex</label><br>
									                          <input class="form-check-input" type="radio" id="tipo_servicio2" value="A/A" name="tipo_servicio">
									                          <label class="form-check-label">A/A</label>
									                        </div>
									                    </div>
								                    </div>
								                </div>
							             	</div>
						             		<div class="row">
					                    		<div class="col-sm-4">
							                      	<div class="form-group">
						                        		<label>Región:</label>
							                        	<select  name="central[region_id]" id="region" class="form-control">
							                        		<option>Seleccione una Región</option>
							                        		@foreach($regiones as $region)
								                        		<option value="{{$region->id}}">{{$region->nombre}}</option>
								                        	@endforeach
								                    	</select>
							                    	</div>
						                    	</div>

						                    	<div class="col-sm-4">
								                    <div class="form-group">
						                        		<label>Estado:</label>
						                        			<select name="central[estado_id]" id="estado" class="form-control">
							                       				<option value="">Seleccione un estado</option>
									                    	</select>
							                      	</div>
						                    	</div>

						                    	<div class="col-sm-4">
						                      		<div class="form-group">
							                        	<label>Localidad:</label>
								                        	<select name="central[localidad_id]" id="central" class="form-control select2">
									                       		<option value="">Seleccione una Central</option>
									                    	</select>
							                    	</div>
						                      	</div>
						                    </div>

						                    <div class="row">
							                    <div class="col-sm-4">
							                      <div class="form-group">
							                        <label>Sala o Espacio Físico:</label>
							                        	<select name="sala" id="box_ModeloRectificador" name="box_ModeloRectificador" class="form-control">
							                        		<option>Seleccione una sala</option>
							                        		<option value="ABA">ABA</option>
															<option value="ADSL">ADSL</option>
															<option value="BANCO DE BATERIA">BANCO DE BATERIA</option>
															<option value="CENTRO DE DISTRIBUCION">CENTRO DE DISTRIBUCION</option>
															<option value="CX">CX</option>
															<option value="DATA CENTER">DATA CENTER</option>
															<option value="DIGITAL">DIGITAL</option>
															<option value="DP">DP</option>
															<option value="DSLAM">DSLAM</option>
															<option value="DX">DX</option>
															<option value="FURGON">FURGON</option>
															<option value="LIBRE">LIBRE</option>
															<option value="MG">MG</option>
															<option value="OAC">OAC</option>
															<option value="OFICINA ADMINISTRATIVA">OFICINA ADMINISTRATIVA</option>
															<option value="OPSUT">OPSUT</option>
															<option value="OUTDOOR">OUTDOOR</option>
															<option value="PCM">PCM</option>
															<option value="PSTN">PSTN</option>
															<option value="RECTIFICADORES">RECTIFICADORES</option>
															<option value="SSP">SSP</option>
															<option value="TX">TX</option>
															<option value="TX INTERNACIONAL">TX INTERNACIONAL</option>
															<option value="UMA">UMA</option>
															<option value="UNICA">UNICA</option>
															<option value="UPS">UPS</option>
															<option value="VARIAS SALAS">VARIAS SALAS</option>
														</select>
							                      	</div>
						                    	</div>

						                    	<div class="col-sm-4">
							                      	<div class="form-group">
							                      		<label>Piso:</label>
							                      		<select name="piso" id="box_ModeloRectificador" name="box_ModeloRectificador" class="form-control">
							                        		<option>Seleccione ubicación piso</option>
								                        	<option value="AZOTEA">AZOTEA</option>
															<option value="PH">PH</option>
															<option value="MZ">MZ</option>
															<option value="PB">PB</option>
															<option value="ST">ST</option>
															<option value="S1">S1</option>
															<option value="S2">S2</option>
															<option value="S3">S3</option>
															<option value="Unico piso">Unico piso</option>
															<option value="P1">P1</option>
															<option value="P2">P2</option>
															<option value="P3">P3</option>
															<option value="P4">P4</option>
															<option value="P5">P5</option>
															<option value="P6">P6</option>
															<option value="P7">P7</option>
															<option value="P8">P8</option>
															<option value="P9">P9</option>
															<option value="P10">P10</option>
															<option value="P11">P11</option>
															<option value="P12">P12</option>
															<option value="P13">P13</option>
															<option value="P14">P14</option>
															<option value="P15">P15</option>
															<option value="P16">P16</option>
															<option value="P17">P17</option>
															<option value="P18">P18</option>
															<option value="P19">P19</option>
															<option value="P20">P20</option>
															<option value="P21">P21</option>
															<option value="TERRAZA">TERRAZA</option>
														</select>
								                    </div>
						                      	</div>
						                    	
								                <div class="col-sm-4">
							                      	<div class="form-group">
								                        <label>Equipo:</label>
								                        <select name="equipo_id" id="equipo" class="form-control">
								                        	<option value="">Seleccione un equipo</option>
														</select>
								                    </div>
								                </div>
						                    </div>

						                    <div class="row">
							                	<div class="col-sm-4">
							                      	<div class="form-group">
							                        	<label>Componente de Equipo:</label>
							                        	<select name="componente_id" id="componente" class="form-control">
							                            	<option value="">Seleccione un componente</option>
														</select>
							                      	</div>
							                    </div>

								                <div class="col-sm-4">
							                      	<div class="form-group">
							                        	<label>Estatus:</label>
							                        	<select name="estatus"  class="form-control">
						                            		<option>Seleccione estatus de la actividad</option>
															<option value="SIN INICIAR">SIN INICIAR</option>
															<option value="EN PROCESO">EN PROCESO</option>
															<option value="PARALIZADO">PARALIZADO</option>
															<option value="CULMINADO">CULMINADO</option>
									            		</select>
								                    </div>
							                    </div>

								                <div class="col-sm-4">
							                      	<div class="form-group">
							                        	<label>Acción:</label>
							                        	<select name="accion"  class="form-control">
						                            		<option>Seleccione acción llevada a cabo</option>
						                            		<option value="ADECUACION DE ESPACIOS">ADECUACION DE ESPACIOS</option>
															<option value="MEDICION DE COMBUSTIBLE">MEDICION DE COMBUSTIBLE</option>
															<option value="SUMINISTRO DE COMBUSTIBLE">SUMINISTRO DE COMBUSTIBLE</option>
															<option value="CAMBIO DE ACEITE">CAMBIO DE ACEITE</option>
															<option value="CAMBIO DE CAPACITOR">CAMBIO DE CAPACITOR</option>
															<option value="CAMBIO DE FILTROS">CAMBIO DE FILTROS</option>
															<option value="CARGA">CARGA</option>
															<option value="COMPLETACION AGUA DESTILADA">COMPLETACION AGUA DESTILADA</option>
															<option value="COMPLETACION DE FLUIDOS">COMPLETACION DE FLUIDOS</option>
															<option value="COMPLETACION DE REFRIGERANTE">COMPLETACION DE REFRIGERANTE</option>
															<option value="CORTE PROGRAMADO RED ELECTRICA">CORTE PROGRAMADO RED ELECTRICA</option>
															<option value="DESINCORPORAR">DESINCORPORAR</option>
															<option value="FALLA">FALLA</option>
															<option value="INSTALACION">INSTALACION</option>
			                                      			<option value="INTEGRAL">INTEGRAL</option>
															<option value="LAVADO">LAVADO</option>
															<option value="MOTOR EN SERVICIO">MOTOR EN SERVICIO</option>
															<option value="DESTAPADO DE DESAGUE">DESTAPADO DE DESAGUE</option>
															<option value="OTROS">OTROS (ESPECIFICAR EN INFORMACION ADICIONAL)</option>
															<option value="REPUESTOS">REPUESTOS</option>
															<option value="SOLDADURA">SOLDADURA</option>
															<option value="SUSTITUCION DE CORREA">SUSTITUCION DE CORREA</option>
															<option value="TRASLADO">TRASLADO</option>
										            	</select>
									                </div>
								                </div>
								                  
						                    	
						                    </div>

					                   
											<div class="row">
								               
								                <div class="col-sm-4">
							                      	<div class="form-group">
							                        	<label>Fecha de Inicio: </label>
								                      	<input type="date"  class="form-control" name="fecha_inicio">
								                    </div>
							                    </div>

							                  	<div class="col-sm-4">
							                      	<label>Porcentaje de Avance:</label>
							                      	<div class="form-group">
							                        	<input name="porcentaje" type="text" class="knob" data-thickness="0.2" 
									                        data-anglearc="250" data-angleoffset="-125" value="0" 
									                        data-width="120" data-height="120" data-fgcolor="#00c0ef" 
									                        style="width: 60px; height: 36px; position: absolute; 
									                         margin-top: 40px; margin-left: -92px; 
									                        border: 0px; background: none; font: bold 24px Arial; 
									                        text-align: center; color: rgb(0, 192, 239); padding: 0px; appearance: none;">
							                      	</div>
							                    </div>

							                    <div class="col-sm-4">
							                      	<div class="form-group">
							                        	<label>Asignado a Cuadrilla:</label>
							                        	<select name="cuadrilla_id"  class="form-control" id="cuadrilla">
							                        		<option>Seleccione una cuadrilla</option>
							                        		@foreach($cuadrilla as $cua)
									                        	<option value="{{$cua->id}}">{{$cua->nombre}}</option>
									                        @endforeach
							                        	</select>
								                    </div>
								                </div>
											

							                </div>
							                
											<div class="row">
								               
								                <div class="col-sm-4">
							                     	<div class="form-group">
							                        	<label>Ticket COSEC:</label>
								                        	<input type="text" name="ticket_cosec"  class="form-control" placeholder="Introduzca ticket COSEC asociado">
								                    </div>
							                  	</div>
					                    
							                    <div class="col-sm-4" style="display: none">
						                      		<div class="form-group">
						                        		<label>Fecha de Fin:</label>
						                        		<input type="checkbox" name="pendiente" id="pendiente_culminar">¿Actividad Culminada?
						                        		<input type="date" readonly="true" class="form-control" id="fecha_fin_registro" name="fecha_fin">
							                    	</div>
						                      	</div>
							                
							                  	<div class="col-sm-4" style="display: none">
							                      	<div class="form-group">
							                        	<label>Nombre del Informe:</label>
						                        		<input type="checkbox" name="pendiente" id="informe">¿Informe Entregado?
							                        	<input type="text" name="name_informe" id="nombre_informe"  class="form-control" placeholder="Pendiente" readonly="true">
								                    </div>
							                  	</div>

					                 		</div>
						                
						              	<div class="row">
						                  	<div class="col-md-12">
												<div class="form-group">
													<label for="comment">Observaciones:</label>
													<textarea class="form-control" rows="3" name="observaciones" id="comment"></textarea>			
												</div>
						              		</div>
						                </div>
				            		</div>
				        		</div>
		                
				                <div class="card-footer" align="right">
					                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
					            </div>
	                		</form>
						</div>
	        		</div>
	    		</div>
			</div>
		</div>
	</section>
    <!-- /.content -->
</div>

@endsection
@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_registro") !!}
@endif

<script type="text/javascript">
 $("#pendiente_culminar").click(function(){
	var pendiente_culminar = document.getElementById('pendiente_culminar');
	var fecha_fin_registro = document.getElementById('fecha_fin_registro');
	if(pendiente_culminar.checked){
        	//$('#serial_motor').hide();
        	fecha_fin_registro.value="00/00/00"
        	$('#fecha_fin_registro').prop('readonly', false);
 		}
 	if(!pendiente_culminar.checked){
        	//$('#serial_motor').hide();
        	fecha_fin_registro.value=" "
        	$('#fecha_fin_registro').prop('readonly', true);
 		}

 	});

 $("#informe").click(function(){
	var informe = document.getElementById('informe');
	var nombre_informe = document.getElementById('nombre_informe');
	if(informe.checked){
        	//$('#serial_motor').hide();
        	nombre_informe.value=""
        	$('#nombre_informe').prop('readonly', false);
 		}
 	if(!informe.checked){
        	//$('#serial_motor').hide();
        	nombre_informe.value="Pendiente"
        	$('#nombre_informe').prop('readonly', true);
 		}

 	});

	$(document).ready(function(){
        $("#tipo_servicio").click(function(){
    		
		    var tipo_servicio = 1;
		    
		    var APP_URL = {!!json_encode(url('/'))!!}+'/';
		    $.get(APP_URL+'tipo_servicio/' + tipo_servicio, function (cidades) {
		        $('select[id=equipo]').empty();

		        if(cidades.length != 0){
		            $('select[id=equipo]').append('<option value=""> Seleccione un equipo </option>');
		            $.each(cidades, function (key, value) {
		                $('select[id=equipo]').append('<option value=' + value.id + '>'+ value.descripcion + '</option>');
		            });
		        }

		        });

		    });
          	
        $("#tipo_servicio2").click(function(){
        		 var tipo_servicio = 2;
		    
		    var APP_URL = {!!json_encode(url('/'))!!}+'/';
		    $.get(APP_URL+'tipo_servicio/' + tipo_servicio, function (cidades) {
		        $('select[id=equipo]').empty();

		        if(cidades.length != 0){
		            $('select[id=equipo]').append('<option value=""> Seleccione un equipo </option>');
		            $.each(cidades, function (key, value) {
		                $('select[id=equipo]').append('<option value=' + value.id + '>'+ value.descripcion + '</option>');
		            });
		        }

		        });
         
   		 });
});


	$('select[id=equipo]').change(function () {
      //console.log("estoy aqui papa");
      var equipo_id = $(this).val();
      
      var APP_URL = {!!json_encode(url('/'))!!}+'/';
      $.get(APP_URL+'componentes/' + equipo_id, function (cidades) {
          $('select[id=componente]').empty();

          if(cidades.length != 0){
              $('select[id=componente]').append('<option value=""> Seleccione un componente </option>');
              $.each(cidades, function (key, value) {
                  $('select[id=componente]').append('<option value=' + value.id + '>'+ value.descripcion + '</option>');
              });
          }

      });
    });

    function generar(){

    	var box_ModeloRectificador = document.getElementById('box_ModeloRectificador');
		var equipo_en_central = document.getElementById('equipo_en_central');
		var componente = document.getElementById('componente');
		var cuadrilla = document.getElementById('cuadrilla');
		var contratista = document.getElementById('contratista');
    }


    $('select[id=equipo]').change(function () {
      var equipo_en_central = $(this).val();
      var central = document.getElementById('central').value;
      // var select =  $('select[id="equipo"] option:selected').text();
      // console.log(select);
      var APP_URL = {!!json_encode(url('/'))!!}+'/';
      $.get(APP_URL+'equipos_centrales/' + equipo_en_central, function (cidades) {
      	//console.log(cidades[0].motor);
          $('select[id=equipo_en_central]').empty();

          if(cidades.length != 0){
              $('select[id=equipo_en_central]').append('<option value=""> Seleccione un equipo </option>');

              
              	if (equipo_en_central == 1){
              		///////////////MOTOGENERADOR
	              	$.each(cidades, function (key, value) {
	                	  $('select[id=equipo_en_central]').append('<option value=' + value.id + '>'+ value.motor.marca +' '+ value.motor.serial + '</option>');
	              	});
              	}else if(equipo_en_central == 2){
              		///////// RECTIFICADORES
              		$.each(cidades, function (key, value) {
	                	  $('select[id=equipo_en_central]').append('<option value=' + value.id + '>'+ value.rectificador.marca +' '+ value.rectificador.serial + '</option>');
	              	});
              	}else if(equipo_en_central == 3){
              		$.each(cidades, function (key, value) {
	                	  $('select[id=equipo_en_central]').append('<option value=' + value.id + '>'+ value.marca +' '+ value.serial + '</option>');
	              	});
              	}else if(equipo_en_central == 7){
              		/*UPS BANCO DE BATERIA*/
              		$.each(cidades, function (key, value) {
	                	  $('select[id=equipo_en_central]').append('<option value=' + value.id + '>'+ value.ups.marca +' '+ value.ups.serial + '</option>');
	              	});
              	}



          }else{
          	$('select[id=equipo_en_central]').append('<option disabled value=""> No hay equipos registrados </option>');

          }

      });
    });




$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
  $(document).ready(function(){



  	$('select[id=region]').change(function () {
    	console.log('hola')
    var region_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'estado/' + region_id, function (cidades) {
        $('select[id=estado]').empty();

        if(cidades.length != 0){
            $('select[id=estado]').append('<option value=""> Selecciona un estado</option>');
            $.each(cidades, function (key, value) {
                $('select[id=estado]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });

    });


	$('select[id=estado]').change(function () {
    	//console.log('hola')
    var estado_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'central/' + estado_id, function (cidades) {
        $('select[id=central]').empty();

        if(cidades.length != 0){
            $('select[id=central]').append('<option value=""> Selecciona un central</option>');
            $.each(cidades, function (key, value) {
                $('select[id=central]').append('<option value=' + value.id + '>'+ value.nombre +' - '+value.tipo+  '</option>');
            });
        }

        });

    });

     $('select[id=cuadrilla]').change(function () {
    	var cuadrilla_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'contratista/' + cuadrilla_id, function (cidades) {
        $('select[id=contratista]').empty();
    	console.log(cidades.nombre)

        if(cidades.length != 0){
            $('select[id=contratista]').append('<option value=""> Seleccione contratista</option>');
            $.each(cidades, function (key, value) {
                $('select[id=contratista]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });
    });



	});
	});

 
</script>
@endsection
<!-- probemos -->

