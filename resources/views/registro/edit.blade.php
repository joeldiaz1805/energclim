@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Modificar Registro Actividad
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/registro_actividades">Registro de Actividad</a></li>
              <li class="breadcrumb-item active">Modificar</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content --> 
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
             
              <div class="card-body">
              <form method="POST" id="formulario_registro" name="formulario_registro"  action="/registro_actividades/{{encrypt($registro->id)}}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PUT">
                    
                   
                    <div class="col-md-12">
                    <!-- general form elements disabled -->
                     <div class="card card-primary">
                      <div class="card-header">
                        <h3 class="card-title">Modificar/Actualizar Registro Actividad</h3>
                      </div>
                      <div class="card-body">                  
                        <div class="row">
                          <div class="col-sm-4">
                            <label>Tipo de Actividad:</label>
                            <div class="col-sm-12">
                              <div class="form-group">
                                <div class="form-check" required>
                                  <input class="form-check-input" type="radio" value="Preventivo" name="tipo_actividad" @if($registro->tipo_actividad == "Preventivo") checked @endif>
                                  <label class="form-check-label">Mantenimiento Preventivo</label><br>
                                  <input class="form-check-input" type="radio" value="Correctivo" @if($registro->tipo_actividad == "Correctivo") checked @endif name="tipo_actividad">
                                  <label class="form-check-label">Mantenimiento Correctivo</label><br>
                                  <input class="form-check-input" type="radio" value="Inspección" @if($registro->tipo_actividad == "Inspección") checked @endif name="tipo_actividad">
                                  <label class="form-check-label">Inspección</label><br>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <label>Tipo de Servicio:</label>
                            <div class="col-sm-4">
                              <div class="form-group">
                                <div class="form-check" required>
                                  <input class="form-check-input" type="radio" id="tipo_servicio" name="tipo_servicio" @if($registro->tipo_servicio=="Ex") checked @endif  value="Ex">
                                  <label class="form-check-label">Ex</label><br>
                                  <input class="form-check-input" type="radio" id="tipo_servicio2" name="tipo_servicio" @if($registro->tipo_servicio=="A/A") checked @endif  value="A/A">
                                  <label class="form-check-label">A/A</label>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Región:</label>
                              <select name="central[region_id]" id="region" class="form-control">
                                <option>Seleccione una Región</option>
                                @foreach($regiones as $region)
                                <option @if($region->id == $registro->ubicacion->region->id) selected @endif value="{{$region->id}}">{{$region->nombre}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Estado:</label>
                              <select name="central[estado_id]" id="estado" class="form-control">
                                <option value="">Seleccione un estado</option>
                                @foreach($estados as $estado)
                                  <option @if($estado->id == $registro->ubicacion->estados->id) selected @endif value="{{$estado->id}}">{{$estado->nombre}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Localidad:</label>
                              <select name="central[localidad_id]" id="central" class="form-control">
                                <option value="">Seleccione una Central</option>
                                @foreach($localidades as $central)
                                <option @if($central->id == $registro->ubicacion->localidad->id) selected @endif value="{{$central->id}}">{{$central->nombre}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Sala o espacio físico:</label>
                              <select name="sala" id="box_ModeloRectificador" class="form-control">
                                <option>Seleccione una sala</option>
                                <option @if($registro->sala=="ABA") selected @endif value="ABA">ABA</option>
                                <option @if($registro->sala=="ADSL") selected @endif value="ADSL">ADSL</option>
                                <option @if($registro->sala=="BANCO DE BATERIA") selected @endif value="BANCO DE BATERIA">BANCO DE BATERIA</option>
                                <option @if($registro->sala=="CENTRO DE DISTRIBUCION") selected @endif value="CENTRO DE DISTRIBUCION">CENTRO DE DISTRIBUCION</option>
                                <option @if($registro->sala=="CX") selected @endif value="CX">CX</option>
                                <option @if($registro->sala=="DATA CENTER") selected @endif value="DATA CENTER">DATA CENTER</option>
                                <option @if($registro->sala=="DIGITAL") selected @endif value="DIGITAL">DIGITAL</option>
                                <option @if($registro->sala=="DP") selected @endif value="DP">DP</option>
                                <option @if($registro->sala=="DSLAM") selected @endif value="DSLAM">DSLAM</option>
                                <option @if($registro->sala=="DX") selected @endif value="DX">DX</option>
                                <option @if($registro->sala=="FURGON") selected @endif value="FURGON">FURGON</option>
                                <option @if($registro->sala=="LIBRE") selected @endif value="LIBRE">LIBRE</option>
                                <option @if($registro->sala=="MG") selected @endif value="MG">MG</option>
                                <option @if($registro->sala=="OAC") selected @endif value="OAC">OAC</option>
                                <option @if($registro->sala=="OFICINA ADMINISTRATIVA") selected @endif value="OFICINA ADMINISTRATIVA">OFICINA ADMINISTRATIVA</option>
                                <option @if($registro->sala=="OPSUT") selected @endif value="OPSUT">OPSUT</option>
                                <option @if($registro->sala=="OUTDOOR") selected @endif value="OUTDOOR">OUTDOOR</option>
                                <option @if($registro->sala=="PCM") selected @endif value="PCM">PCM</option>
                                <option @if($registro->sala=="PSTN") selected @endif value="PSTN">PSTN</option>
                                <option @if($registro->sala=="RECTIFICADORES") selected @endif value="RECTIFICADORES">RECTIFICADORES</option>
                                <option @if($registro->sala=="SSP") selected @endif value="SSP">SSP</option>
                                <option @if($registro->sala=="TX") selected @endif value="TX">TX</option>
                                <option @if($registro->sala=="TX INTERNACIONAL") selected @endif value="TX INTERNACIONAL">TX INTERNACIONAL</option>
                                <option @if($registro->sala=="UMA") selected @endif value="UMA">UMA</option>
                                <option @if($registro->sala=="UNICA") selected @endif value="UNICA">UNICA</option>
                                <option @if($registro->sala=="UPS") selected @endif value="UPS">UPS</option>
                                <option @if($registro->sala=="VARIAS SALAS") selected @endif value="VARIAS SALAS">VARIAS SALAS</option>
                              </select>
                            </div>
                          </div>

                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Piso:</label>
                                <select name="piso" id="box_ModeloRectificador" class="form-control" required>
                                  <option>Seleccione ubicación piso</option>
                                  <option @if($registro->piso=="AZOTEA") selected @endif value="AZOTEA">AZOTEA</option>
                                  <option @if($registro->piso=="PH") selected @endif value="PH">PH</option>
                                  <option @if($registro->piso=="MZ") selected @endif value="MZ">MZ</option>
                                  <option @if($registro->piso=="PB") selected @endif value="PB">PB</option>
                                  <option @if($registro->piso=="ST") selected @endif value="ST">ST</option>
                                  <option @if($registro->piso=="S1") selected @endif value="S1">S1</option>
                                  <option @if($registro->piso=="S2") selected @endif value="S2">S2</option>
                                  <option @if($registro->piso=="S3") selected @endif value="S3">S3</option>
                                  <option @if($registro->piso=="Unico piso") selected @endif value="Unico piso">Unico piso</option>
                                  <option @if($registro->piso=="P1") selected @endif value="P1">P1</option>
                                  <option @if($registro->piso=="P2") selected @endif value="P2">P2</option>
                                  <option @if($registro->piso=="P3") selected @endif value="P3">P3</option>
                                  <option @if($registro->piso=="P4") selected @endif value="P4">P4</option>
                                  <option @if($registro->piso=="P5") selected @endif value="P5">P5</option>
                                  <option @if($registro->piso=="P6") selected @endif value="P6">P6</option>
                                  <option @if($registro->piso=="P7") selected @endif value="P7">P7</option>
                                  <option @if($registro->piso=="P8") selected @endif value="P8">P8</option>
                                  <option @if($registro->piso=="P9") selected @endif value="P9">P9</option>
                                  <option @if($registro->piso=="P10") selected @endif value="P10">P10</option>
                                  <option @if($registro->piso=="P11") selected @endif value="P11">P11</option>
                                  <option @if($registro->piso=="P12") selected @endif value="P12">P12</option>
                                  <option @if($registro->piso=="P13") selected @endif value="P13">P13</option>
                                  <option @if($registro->piso=="P14") selected @endif value="P14">P14</option>
                                  <option @if($registro->piso=="P15") selected @endif value="P15">P15</option>
                                  <option @if($registro->piso=="P16") selected @endif value="P16">P16</option>
                                  <option @if($registro->piso=="P17") selected @endif value="P17">P17</option>
                                  <option @if($registro->piso=="P18") selected @endif value="P18">P18</option>
                                  <option @if($registro->piso=="P19") selected @endif value="P19">P19</option>
                                  <option @if($registro->piso=="P20") selected @endif value="P20">P20</option>
                                  <option @if($registro->piso=="P21") selected @endif value="P21">P21</option>
                                  <option @if($registro->piso=="TERRAZA") selected @endif value="TERRAZA">TERRAZA</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Equipo:</label>
                                <select name="equipo_id" id="equipo" class="form-control">
                                  <option value="">Seleccione un equipo</option>
                                  @foreach($equipos as $equipo)
                                      <option @if($equipo->id == $registro->equipo->id) selected @endif value="{{$equipo->id}}">{{$equipo->descripcion}}</option>
                                  @endforeach
                                </select>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Componente de Equipo:</label>
                                <select name="componente_id" id="componente" class="form-control">
                                    <option value="">Seleccione un componente</option>
                                    @foreach($componentes as $componente)
                                        <option @if($componente->id == $registro->componente->id) selected @endif value="{{$componente->id}}">{{$componente->descripcion}}</option>
                                    @endforeach
                                </select>
                              </div>
                            </div>                        
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Estatus:</label>
                                  <select name="estatus"  class="form-control">
                                    <option value=""></option>
                                    <option @if($registro->estatus=="SIN INICIAR") selected @endif value="SIN INICIAR">SIN INICIAR</option>
                                    <option @if($registro->estatus=="EN PROCESO") selected @endif value="EN PROCESO">EN PROCESO</option>
                                    <option @if($registro->estatus=="PARALIZADO") selected @endif value="PARALIZADO">PARALIZADO</option>
                                    <option @if($registro->estatus=="CULMINADO") selected @endif value="CULMINADO">CULMINADO</option>
                                  </select>
                                </div>
                              </div>

                              <div class="col-sm-4">
                                <div class="form-group">
                                  <label>Acción:</label>
                                    <select name="accion"  class="form-control">
                                      <option>Seleccione acción llevada a cabo</option>
                                      <option @if($registro->accion=="ADECUACION DE ESPACIOS") selected @endif  value="ADECUACION DE ESPACIOS">ADECUACION DE ESPACIOS</option>
                                      <option @if($registro->accion=="MEDICION DE COMBUSTIBLE") selected @endif  value="MEDICION DE COMBUSTIBLE">MEDICION DE COMBUSTIBLE</option>
                                      <option @if($registro->accion=="SUMINISTRO DE COMBUSTIBLE") selected @endif  value="SUMINISTRO DE COMBUSTIBLE">SUMINISTRO DE COMBUSTIBLE</option>
                                      <option @if($registro->accion=="CAMBIO DE ACEITE") selected @endif  value="CAMBIO DE ACEITE">CAMBIO DE ACEITE</option>
                                      <option @if($registro->accion=="CAMBIO DE CAPACITOR") selected @endif  value="CAMBIO DE CAPACITOR">CAMBIO DE CAPACITOR</option>
                                      <option @if($registro->accion=="CAMBIO DE FILTROS") selected @endif  value="CAMBIO DE FILTROS">CAMBIO DE FILTROS</option>
                                      <option @if($registro->accion=="CARGA") selected @endif  value="CARGA">CARGA</option>
                                      <option @if($registro->accion=="COMPLETACION AGUA DESTILADA") selected @endif  value="COMPLETACION AGUA DESTILADA">COMPLETACION AGUA DESTILADA</option>
                                      <option @if($registro->accion=="COMPLETACION DE FLUIDOS") selected @endif  value="COMPLETACION DE FLUIDOS">COMPLETACION DE FLUIDOS</option>
                                      <option @if($registro->accion=="COMPLETACION DE REFRIGERANTE") selected @endif  value="COMPLETACION DE REFRIGERANTE">COMPLETACION DE REFRIGERANTE</option>
                                      <option @if($registro->accion=="CORTE PROGRAMADO DE RED ELECTRICA") selected @endif  value="CORTE PROGRAMADO RED ELECTRICA">CORTE PROGRAMADO RED ELECTRICA</option>
                                      <option @if($registro->accion=="DESINCORPORAR") selected @endif  value="DESINCORPORAR">DESINCORPORAR</option>
                                      <option @if($registro->accion=="FALLA") selected @endif  value="FALLA">FALLA</option>
                                      <option @if($registro->accion=="INSTALACION") selected @endif  value="INSTALACION">INSTALACION</option>
                                      <option @if($registro->accion=="INTEGRAL") selected @endif  value="INTEGRAL">INTEGRAL</option>
                                      <option @if($registro->accion=="LAVADO") selected @endif  value="LAVADO">LAVADO</option>
                                      <option @if($registro->accion=="MOTOR EN SERVICIO") selected @endif  value="MOTOR EN SERVICIO">MOTOR EN SERVICIO</option>
                                      <option @if($registro->accion=="DESTAPADO DE DESAGUE") selected @endif  value="DESTAPADO DE DESAGUE">DESTAPADO DE DESAGUE</option>
                                      <option @if($registro->accion=="OTROS (ESPECIFICAR EN INFORMACION ADICIONAL)") selected @endif  value="OTROS">OTROS (ESPECIFICAR EN INFORMACION ADICIONAL)</option>
                                      <option @if($registro->accion=="REPUESTOS") selected @endif  value="REPUESTOS">REPUESTOS</option>
                                      <option @if($registro->accion=="SOLDADURA") selected @endif  value="SOLDADURA">SOLDADURA</option>
                                      <option @if($registro->accion=="SUSTITUCION DE CORREA") selected @endif  value="SUSTITUCION DE CORREA">SUSTITUCION DE CORREA</option>
                                      <option @if($registro->accion=="TRASLADO") selected @endif  value="TRASLADO">TRASLADO</option>
                                    </select>
                                  </div>
                                </div>
                              </div>

                              <div class="row">
                                <div class="col-sm-4">
                                  <div class="form-group">
                                    <label>Fecha de Inicio: </label>
                                    <input type="date" value="{{$registro->fecha_inicio}}" class="form-control" name="fecha_inicio" readonly>
                                  </div>
                                </div>
                                
                                <div class="col-sm-4">
                                  <label>Porcentaje:</label>
                                  <div class="col-sm-4">
                                    <input name="porcentaje" type="text" class="knob" data-thickness="0.2" 
                                      data-anglearc="250" data-angleoffset="-125" value="{{$registro->porcentaje}}" 
                                      data-width="120" data-height="120" data-fgcolor="#00c0ef" 
                                      style="width: 60px; height: 36px; position: absolute; 
                                       margin-top: 40px; margin-left: -92px; 
                                      border: 0px; background: none; font: bold 24px Arial; 
                                      text-align: center; color: rgb(0, 192, 239); padding: 0px; appearance: none;">
                                  </div>
                                </div>
                                
                                <div class="col-sm-4">
                                  <div class="form-group">
                                    <label>Realizado por Cuadrilla:</label>
                                    <select name="cuadrilla[nombre]"  class="form-control" id="cuadrilla">
                                      <option>Seleccione una cuadrilla</option>
                                      @foreach($cuadrilla as $cua)
                                      <option @if($cua->id == $registro->cuadrilla->id) selected @endif value="{{$cua->id}}">{{$cua->nombre}}</option>
                                      @endforeach
                                    </select>
                                  </div>
                                </div>
                              </div>

                              <div class="row">
                                <div class="col-sm-4">
                                  <div class="form-group">
                                    <label>Ticket COSEC:</label>
                                      <input type="text" name="ticket_cosec" value="{{$registro->ticket_cosec}}"  class="form-control" placeholder="Introduzca ticket COSEC asociado" readonly>
                                  </div>
                                </div>
                      
                                <div class="col-sm-4">
                                  <div class="form-group">
                                    <label>Fecha de Fin:</label>
                                    <input type="checkbox" name="pendiente" id="pendiente_culminar">¿Actividad Culminada?
                                    <input type="date" readonly="true" id="fecha_fin_registro" class="form-control" name="fecha_fin" value="{{$registro->fecha_fin}}">
                                  </div>
                                </div>
                        
                                <div class="col-sm-4">
                                  <div class="form-group">
                                    <label>Informe:</label>
                                    <input type="file" readonly="true" name="name_informe"  id="nombre_informe" class="form-control" placeholder="Nombre Informe" >
                                  </div>
                                </div>
                              </div>

                              <div class="row">
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label for="comment">Observaciones:</label>
                                    <textarea class="form-control" rows="3" name="observaciones" > {{$registro->observaciones}}</textarea>     
                                  </div>
                                </div>
                              </div>
                            </div>
                           </div>
                          </div> 
      
         <div class="card-footer" align="right">
           <button type="submit" class="btn btn-primary"><i class="fas fa-sync-alt"></i> Actualizar</button>
         </div>
                 
    </form>
                  </div>
                </div>
            </div>
            </div>
        </div>
    </section>
</div>


@endsection


@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_registro") !!}
@endif

<script type="text/javascript">

  $("#pendiente_culminar").click(function(){
  var pendiente_culminar = document.getElementById('pendiente_culminar');
  var fecha_fin_registro = document.getElementById('fecha_fin_registro');
  if(pendiente_culminar.checked){
          //$('#serial_motor').hide();
          fecha_fin_registro.value="00/00/00"
          $('#fecha_fin_registro').prop('readonly', false);
    }
  if(!pendiente_culminar.checked){
          //$('#serial_motor').hide();
          fecha_fin_registro.value=""
          $('#fecha_fin_registro').prop('readonly', true);
    }

  });

 $("#informe").click(function(){
  var informe = document.getElementById('informe');
  var nombre_informe = document.getElementById('nombre_informe');
  if(informe.checked){
          //$('#serial_motor').hide();
          nombre_informe.value=""
          $('#nombre_informe').prop('readonly', false);
    }
  if(!informe.checked){
          //$('#serial_motor').hide();
          nombre_informe.value="Pendiente"
          $('#nombre_informe').prop('readonly', true);
    }

  });

  $(document).ready(function(){
        $("#tipo_servicio").click(function(){
        
        var tipo_servicio = 1;
        
        var APP_URL = {!!json_encode(url('/'))!!}+'/';
        $.get(APP_URL+'tipo_servicio/' + tipo_servicio, function (cidades) {
            $('select[id=equipo]').empty();

            if(cidades.length != 0){
                $('select[id=equipo]').append('<option value=""> Seleccione un equipo </option>');
                $.each(cidades, function (key, value) {
                    $('select[id=equipo]').append('<option value=' + value.id + '>'+ value.descripcion + '</option>');
                });
            }

            });

        });
            
        $("#tipo_servicio2").click(function(){
             var tipo_servicio = 2;
        
        var APP_URL = {!!json_encode(url('/'))!!}+'/';
        $.get(APP_URL+'tipo_servicio/' + tipo_servicio, function (cidades) {
            $('select[id=equipo]').empty();

            if(cidades.length != 0){
                $('select[id=equipo]').append('<option value=""> Seleccione un equipo </option>');
                $.each(cidades, function (key, value) {
                    $('select[id=equipo]').append('<option value=' + value.id + '>'+ value.descripcion + '</option>');
                });
            }

            });
         
       });
});


  $('select[id=equipo]').change(function () {
      //console.log("estoy aqui papa");
      var equipo_id = $(this).val();
      
      var APP_URL = {!!json_encode(url('/'))!!}+'/';
      $.get(APP_URL+'componentes/' + equipo_id, function (cidades) {
          $('select[id=componente]').empty();

          if(cidades.length != 0){
              $('select[id=componente]').append('<option value=""> Seleccione un componente </option>');
              $.each(cidades, function (key, value) {
                  $('select[id=componente]').append('<option value=' + value.id + '>'+ value.descripcion + '</option>');
              });
          }

      });
    });

    function generar(){

    var box_ModeloRectificador = document.getElementById('box_ModeloRectificador');
    var equipo_en_central = document.getElementById('equipo_en_central');
    var componente = document.getElementById('componente')


    }


    $('select[id=equipo]').change(function () {
      var equipo_en_central = $(this).val();
      var central = document.getElementById('central').value;
      // var select =  $('select[id="equipo"] option:selected').text();
      // console.log(select);
      var APP_URL = {!!json_encode(url('/'))!!}+'/';
      $.get(APP_URL+'equipos_centrales/' + equipo_en_central, function (cidades) {
        //console.log(cidades[0].motor);
          $('select[id=equipo_en_central]').empty();

          if(cidades.length != 0){
              $('select[id=equipo_en_central]').append('<option value=""> Seleccione un equipo </option>');

              
                if (equipo_en_central == 1){
                  ///////////////MOTOGENERADOR
                  $.each(cidades, function (key, value) {
                      $('select[id=equipo_en_central]').append('<option value=' + value.id + '>'+ value.motor.marca +' '+ value.motor.serial + '</option>');
                  });
                }else if(equipo_en_central == 2){
                  ///////// RECTIFICADORES
                  $.each(cidades, function (key, value) {
                      $('select[id=equipo_en_central]').append('<option value=' + value.id + '>'+ value.rectificador.marca +' '+ value.rectificador.serial + '</option>');
                  });
                }else if(equipo_en_central == 3){
                  $.each(cidades, function (key, value) {
                      $('select[id=equipo_en_central]').append('<option value=' + value.id + '>'+ value.marca +' '+ value.serial + '</option>');
                  });
                }else if(equipo_en_central == 7){
                  /*UPS BANCO DE BATERIA*/
                  $.each(cidades, function (key, value) {
                      $('select[id=equipo_en_central]').append('<option value=' + value.id + '>'+ value.ups.marca +' '+ value.ups.serial + '</option>');
                  });
                }



          }else{
            $('select[id=equipo_en_central]').append('<option disabled value=""> No hay equipos registrados </option>');

          }

      });
    });




$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
  $(document).ready(function(){



    $('select[id=region]').change(function () {
      console.log('hola')
    var region_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'estado/' + region_id, function (cidades) {
        $('select[id=estado]').empty();

        if(cidades.length != 0){
            $('select[id=estado]').append('<option value=""> Selecciona un estado</option>');
            $.each(cidades, function (key, value) {
                $('select[id=estado]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });

    });


  $('select[id=estado]').change(function () {
      //console.log('hola')
    var estado_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'central/' + estado_id, function (cidades) {
        $('select[id=central]').empty();

        if(cidades.length != 0){
            $('select[id=central]').append('<option value=""> Selecciona un central</option>');
            $.each(cidades, function (key, value) {
                $('select[id=central]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });

    });


  });
});
</script>
@endsection