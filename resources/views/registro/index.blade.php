@extends ('/layouts/index')

@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Registro de Actividades por Región 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/home">Inicio</a></li>
              <li class="breadcrumb-item active">Registro de Actividades</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Vista Preliminar de Registros</h3>
              <div align="right">
                  <a href="/registro_actividades/create" class="btn btn-primary"><i class="fas fa-plus nav-icon"></i>Nueva Actividad</a>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="display table table-bordered table-striped nowrap" style="width:100%">
                <thead>
                  <tr align="center">
                    <th>Acciones</th>
                    <th>% Avance</th>
                    <th>Ticket COSEC</th>
                    <th>Región</th>
                    <th>Estado</th>
                    <th>Localidad</th>
                    <th>Tipo de Actividad</th>
                    <th>Tipo de Servicio</th>
                    <th>Equipo</th>
                    <th>Componente de Equipo</th>
                    <th>Sala</th>
                    <th>Piso</th>
                    <th>Estatus</th>
                    <th>Acción</th>
                    <th>Trabajo</th>
                    <th>Cuadrilla</th>
                    <th>Fecha de Inicio</th>
                    <th>Fecha de Fin</th>
                    <th>Nombre del Informe</th>
                    <th>Observaciones</th>
                    <th>Usuario</th>
                    <th>Ultima Actualizacion</th>

                  </tr>
                </thead>
                <tbody>
                  @foreach($registro as $registro)
                  <tr align="center"  id="tdId_{{$registro->id}}">
                    <td>
                      @if($registro->fecha_fin == null)
                      <a href="/registro_actividades/{{encrypt($registro->id)}}/edit" class="btn btn-warning"><i class="far fa-edit"></i></a>
                      @endif
                      <a href="/registro_actividades/{{encrypt($registro->id)}}" class="btn btn-success"><i class="far fa-eye"></i></a>
                      <a onclick="deleteItem('{{$registro->id}}')" class="btn btn-danger"><i class="fa fa-times"></i></a>
                    </td>
                    <td>
                        <input type="text" class="knob" value="{{$registro->porcentaje}}" data-width="90" data-height="90" @if($registro->porcentaje < 25 ) data-fgcolor="#BC3C3C" @elseif($registro->porcentaje < 50) data-fgcolor="#F57A0A" @elseif($registro->porcentaje < 75) data-fgcolor="#E7D20D" @else data-fgcolor="#0AF52C" @endif  data-readonly="true" readonly="readonly" style="width: 49px; height: 30px; position: absolute; font: bold 18px Arial; text-align: center; color: rgb(60, 141, 188); padding: 0px; appearance: none;">
                    </td>
                    <td>{{$registro->ticket_cosec}}</td>
                    <td>{{$registro->ubicacion->region->nombre}}</td>
                    <td>{{$registro->ubicacion->estados->nombre}}</td>
                    <td>{{$registro->ubicacion->localidad->nombre}}</td>
                    <td>{{$registro->tipo_actividad}}</td>
                    <td>{{$registro->tipo_servicio}}</td>
                    <td>{{$registro->equipo->descripcion}}</td>
                    <td>{{$registro->componente->descripcion}}</td>
                    <td>{{$registro->sala}}</td>
                    <td>{{$registro->piso}}</td>
                    <td>{{$registro->estatus}}</td>
                    <td>{{$registro->accion}}</td>
                    <td>{{$registro->trabajo}}</td>
                    <td>{{$registro->cuadrilla->nombre ?? 'sin crear'}}</td>
                   
                    <td>{{$registro->fecha_inicio}}</td>
                    <td>{{$registro->fecha_fin}}</td>
                    <td>
                      @if($registro->informe != null)
                        <p><a href="{{url('download_plan',$registro->informe->archivo)}}" class="btn btn-success"><i class="fa fa-download"></i> Descargar</a></p>
                      @endif
                      </td>
                    <td>{{$registro->observaciones}}</td>
                    <td>{{$registro->usuario->name ?? 'No data'}}</td>
                    <td>{{$registro->updated_at}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
           </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('scriptsalerts')


<script>
   @if(Session::has('exito'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Actividad guardada con éxito'
      })
      })
    });
  @endif
   @if(Session::has('actualizado'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Datos actualizados con exito'
      })
      })
    });
  @endif
  
</script>

  <script>
    function deleteItem(item){
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Desea eliminar este registro?',
      text: "Estas por quitar la actividad registrada",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, quitarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {

      if (result.value) {



            $.ajax({
                url: "/registro_actividades/"+item,
                type:"DELETE",
                   headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    

                    success:function(result, status, xhr){                    
                        
                        if (status == 'success') {

                          swalWithBootstrapButtons.fire(
                            'Quitado con Exito!',
                            'Se eliminó con exito',
                            'success'
                          )

                          $("#tdId_"+item).hide();
                         // txt.innerHTML = result ;



                        }
                    },
                    error:function(xhr, status, error){
                      

                      Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se eliminó el producto, vuelva a intentarlo mas tarde '+error,
                        footer: '<a href>Seguramente hubo un problema de conexión</a>'
                      })
                      

                    }
                });
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No se ha quitado el producto.',
          'error'
        )
      }
    })
  }
</script>


@endsection