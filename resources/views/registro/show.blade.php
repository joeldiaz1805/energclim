@extends ('/layouts/index')

@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Vista Registro de Actividad - CSCG 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/registro_actividades">Vista Registro Actividad</a></li>
              <li class="breadcrumb-item active">Detalle</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
          <div class="col-md-6">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fa fa-map"></i>
                     Ubicación
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <dl>
                  <dt>Región</dt>
                  <dd>{{$registro->ubicacion->region->nombre}}</dd>
                  <dt>Estado</dt>
                  <dd>{{$registro->ubicacion->estados->nombre}}</dd>
                  <dt>Localidad</dt>
                  <dd>{{$registro->ubicacion->localidad->nombre}}</dd>
                  <dt>Sala</dt>
                  <dd>{{$registro->sala}}</dd>
                  <dt>Piso</dt>
                  <dd>{{$registro->piso}}</dd>
                </dl>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <div class="row">
              <div class="col-md-12">
                <div class="card card-primary">
                  <div class="card-header">
                    <h3 class="card-title">
                      <i class="fa fa-cogs"></i>
                     Equipo
                    </h3>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <dl>
                      <dt>Tipo de Servicio</dt>
                      <dd>{{$registro->tipo_servicio}}</dd>
                      <dt>Equipo</dt>
                      <dd>{{$registro->equipo->descripcion}}</dd>
                      <dt>Componente de Equipo</dt>
                      <dd>{{$registro->componente->descripcion}}</dd>
                    </dl>
                  </div>
                  <!-- /.card-body -->
                </div>
              </div>
            </div>



          </div>
          <!-- ./col -->
          <div class="col-md-6">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-chart-line"></i>
                     Actividad
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <dl>
                  <dt>Porcentaje de Avance</dt>
                  <dd>{{$registro->porcentaje}}</dd>
                  <dt>Estatus</dt>
                  <dd>{{$registro->estatus}}</dd>
                  <dt>Acción</dt>
                  <dd>{{$registro->accion}}</dd>
                  <dt>Trabajo</dt>
                  <dd>{{$registro->trabajo}}</dd>
                  <dt>Cuadrilla</dt>
                  <dd>{{$registro->cuadrilla->nombre}}: @foreach($registro->cuadrilla->cuadrante as $especialista) {{$especialista->personal->nombre}} / @endforeach</dd>
                  <dt>Contratista</dt>
                  <dd>{{$registro->contratista}}</dd>
                  <dt>Fecha Inicio</dt>
                  <dd>{{$registro->fecha_inicio}}</dd>
                  <dt>Fecha Fin</dt>
                  <dd>{{$registro->fecha_fin}}</dd>
                  <dt>Ticket COSEC</dt>
                  <dd>{{$registro->ticket_cosec}}</dd>
                  <dt>Informe</dt>
                  <dd>@if($registro->informe != null)
                        <p><a href="{{url('download_plan',$registro->informe->archivo)}}" class="btn btn-success"><i class="fa fa-download"></i> Descargar</a></p>
                      @endif</dd>
                  <dt>Observaciones</dt>
                  <dd>{{$registro->observaciones}}</dd>
                </dl>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
          <!-- ./col -->
       
          </div>
        
      </div>
    </div>
  </section>
</div>
@endsection