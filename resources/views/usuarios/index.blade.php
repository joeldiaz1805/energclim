@extends ('/layouts/index')

@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Usuarios
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Usuarios </a></li>
              <li class="breadcrumb-item active">lista</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Vista Preliminar de Registros</h3>
              <div align="right">
                  <a href="/usuarios/create" class="btn btn-success">Nuevo Usuario</a>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class=" display nowrap  table table-bordered table-striped" style="width:100%">
                <thead>
                  <tr align="center">
                    <th class="not-export-col">Acciones</th>
                    <th>Nombres y Apellidos</th>
                    <th>Email</th>
                    <th>Alias</th>
                    <th>Departamento</th>
                    <th>Regiòn (Rol Secretario(a) )</th>
                    <th>Cargo</th>
                    <th>Status</th>
                    <th>Rol o Privilegio</th>
                    <th>Permisos</th>
                    <th>Supervisor Inmediato</th>
                    <th>Usuario en Sesión</th>
                    <th>Ip de navegación</th>
                  </tr>
                </thead>
                <tbody>
                
                  @foreach($usuarios as $user)
                  <tr align="center"  id="tdId_{{$user->id}}">
                    <td>
                      <a href="/usuarios/{{encrypt($user->id)}}/edit" class="btn btn-warning"><i class="far fa-edit"></i></a>
                      <a href="/usuarios/{{encrypt($user->id)}}" class="btn btn-success"><i class="far fa-eye"></i></a>
                      <a href="/reset_psw/{{encrypt($user->id)}}" class="btn btn-info" title="resetear password"><i class="fa fa-retweet"></i></a>
                      <a id="intt"  @if($user->active ==1 ) class="btn btn-success" onclick="deleteItem('{{$user->id}}')" @else class="btn btn-danger" onclick="activeItem('{{$user->id}}')" @endif class="btn btn-danger"><i id="ele"  class="fa fa-user-times"></i></a>
                    </td>
                    <td>{{$user->name}} {{$user->last_name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->alias}}</td>
                    <td>{{$user->departamento}}</td>
                    <td>
                      @if($user->rol_id == 4)
                          @if($user->region)
                            {{$user->region->nombre}}
                          @else
                              Asigne una regiòn
                          @endif
                        @else
                            N/A
                        @endif

                    </td>
                    <td>{{$user->cargo}}</td>
                    <td id="act_{{$user->id}}"> @if($user->active==1) Activo @else Inactivo @endif</td>
                    <td>{{$user->rol->denominacion}}</td>
                    <td>@foreach($user->permisos as $per) {{$user->helper($per->permiso)}} @endforeach</td>
                    
                    <td>{{$user->supevisor->name ?? 'No asignado'}}</td>
                    <td>@if(Auth()->id() == $user->id) En Sesión @else No hay Sesión @endif</td>

                    <td>@if(Auth()->id() == $user->id) @php echo $_SERVER['REMOTE_ADDR'] @endphp @else No conectado @endif</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
           </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('scriptsalerts')


  <script>

    function deleteItem(item){
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Desea Inhabilitar este usuario de la lista?',
      text: "Estas por Inhabilitar un usuario!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, Inhabilitarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {

      if (result.value) {



            $.ajax({
                url: "/usuarios/"+item,
                type:"DELETE",
                   headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    

                    success:function(result, status, xhr){                    
                        
                        if (status == 'success') {

                          swalWithBootstrapButtons.fire(
                            'Inhabilitado con Exito!',
                            'Se Inhabilito con exito',
                            'success'
                          )

                          //$("#tdId_"+item).hide();
                          document.getElementById("intt").className="btn btn-danger";
                          //intt.style(color:green);
                           document.getElementById("act_"+item).innerHTML = "Inativo";
                          //$("#act_"+item).innerHTML = 'Inativo' ;



                        }
                    },
                    error:function(xhr, status, error){
                      

                      Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se Inhabilito ningun usuario '+error,
                        footer: '<a href>Seguramente hubo un problema de conexión</a>'
                      })
                      

                    }
                });
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No se ha quitado el producto.',
          'error'
        )
      }
    })
  }
  function activeItem(item){
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Desea Habilitar este usuario de la lista?',
      text: "Estas por Habilitar un usuario!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, Habilitarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {

      if (result.value) {



            $.ajax({
                url: "/usuarios/"+item,
                type:"DELETE",
                   headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    

                    success:function(result, status, xhr){                    
                        
                        if (status == 'success') {

                          swalWithBootstrapButtons.fire(
                            'Habilitado con Exito!',
                            'Se Habilitò con exito',
                            'success'
                          )

                          //$("#tdId_"+item).hide();
                          document.getElementById("intt").className="btn btn-success";
                          var h= document.getElementById("act_"+item);
                          h.innerHTML = "Activo";

                          //$("#act_"+item).innerHTML = 'Activo' ;



                        }
                    },
                    error:function(xhr, status, error){
                      

                      Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se Habilitado ningun usuario '+error,
                        footer: '<a href>Seguramente hubo un problema de conexión</a>'
                      })
                      

                    }
                });
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No se ha Habilitado el usuario.',
          'error'
        )
      }
    })
  }
</script>


@endsection

