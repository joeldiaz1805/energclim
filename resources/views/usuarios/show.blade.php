@extends ('/layouts/index')

@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Usuario
              <small>Información</small>
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Usuario</a></li>
              <li class="breadcrumb-item active">Datos</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fa fa-user"></i>
                      usuario
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <dl>
                  <dt>Nombre y Apellido</dt>
                  <dd>{{$usuarios->name}} / 
                 {{$usuarios->last_name}}</dd>
                  <dt>Email</dt>
                  <dd>{{$usuarios->email}}</dd>
                  <dt>Rol o Privilegio</dt>
                  <dd>{{$usuarios->rol->denominacion}}</dd>
                </dl>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
          
        </div>
      </div>
          <!-- ./col -->
       
          </div>
        
      </div>
    </div>
  </section>
</div>
@endsection