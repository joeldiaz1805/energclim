@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Usuario 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Registro</a></li>
              <li class="breadcrumb-item active">Usuario</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
     
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
              
              <div class="card-body">
              	 <form method="POST" id="formulario_usuarios" name="formulario_usuarios"  action="{{ url('/usuarios')}}">
                        {{ csrf_field() }}
                    <input type="hidden" name="active" value="1">
                     	<div class="col-md-12">
				            <!-- general form elements disabled -->
				            <div class="card card-success">
				              <div class="card-header">
				                <h3 class="card-title">Usuario</h3>
				              </div>
				              <!-- /.card-header -->
				              <div class="card-body">
				              	 <div class="row">
				                    <div class="col-sm-4">
				                      <!-- text input -->
				                      	<div class="form-group">
				                        	<label>Nombres:</label>
					                        	<input type="text" name="name"  class="form-control" placeholder="Nombres">
					                    </div>
				                     </div>
				                   
									<div class="col-sm-4">
				                      <div class="form-group">
				                        	<label>Apellidos:</label>
					                        	<input type="text" name="last_name"  class="form-control" placeholder="Apellidos">
					                    </div>
				                  	</div>

                            <div class="col-sm-4">
                              <div class="form-group">
                                  <label>Cédula:</label>
                                    <input type="text" name="cedula"  class="form-control" placeholder="Introduzca cédula">
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="form-group">
                                  <label>Departamento:</label>
                                    <select name="departamento" id="departamento" class="form-control">
                                      <option>Seleccione departamento</option>

                                      <option value="Coordinación Normas, Análisis y Diseño">Coordinación Normas, Análisis y Diseño</option>

                                      <option value="Coordinación Seguimiento y Control de Gestión">Coordinación Seguimiento y Control de Gestión</option>

                                      <option value="Coordinación Seguimiento y Control de Gastos">Coordinación Seguimiento y Control de Gastos</option>

                                      <option value="Coordinación Seguimiento y Control de Inversión">Coordinación Seguimiento y Control de Inversión</option>

                                      <option value="Cosec">Cosec</option>
                                      <option value="Gerencia General">Gerencia General</option>
                                      <option value="Gerencia de Seguimiento y Control">Gerencia de Seguimiento y Control</option>

                                      <option value="Gcia. Cost. Energía y Climatización">Gcia. Cost. Energía y Climatización</option>

                                      <option value="Gcia. Oper. Mtto Reg. Capital">Gcia. Oper. Mtto Reg. Capital</option>

                                      <option value="Gcia. Oper. Mtto Reg. Centro">Gcia. Oper. Mtto Reg. Centro</option>
                                      
                                      <option value="Gcia. Oper. Mtto Reg. Los Llanos">Gcia. Oper. Mtto Reg.  Los Llanos</option>

                                      <option value="Gcia. Oper. Mtto Reg. Guayana">Gcia. Oper. Mtto Reg. Guayana</option>

                                      <option value="Gcia. Oper. Mtto Reg. Oriente">Gcia. Oper. Mtto Reg. Oriente</option>

                                      <option value="Gcia. Oper. Mtto Reg. Occidente">Gcia. Oper. Mtto Reg. Occidente</option>

                                      <option value="Gcia. Oper. Mtto Reg. Los Andes">Gcia. Oper. Mtto Reg. Los Andes</option>

                                      <option value="Gcia. de Proyectos e Ingeniería">Gcia. de Proyectos e Ingeniería</option>
                                    </select>
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="form-group">
                                  <label>Cargo:</label>
                                    <select name="cargo" id="cargo" class="form-control">
                                      <option>Seleccione un cargo</option>
                                      <option value="Asistente">Asistente</option>
                                      <option value="Asesor">Asesor</option>
                                      <option value="Coordinador">Coordinador</option>
                                      <option value="Gerente">Gerente</option>
                                      <option value="Lider">Lider</option>
                                      <option value="Supervisor">Supervisor</option>
                                      <option value="Tecnico">Tecnico</option>
                                      <option value="Especialista CCT">Especialista CCT</option>
                                      <option value="Consultor CCT">Consultor CCT</option>
                                    </select>
                              </div>
                            </div>

				                   <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Correo:</label>
				                        <input type="text" class="form-control" name="email"  placeholder="Email">
				                      </div>
				                    </div>
				                  </div>

				                  <div class="row">
				                    <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Rol o Privilegio:</label>
				                          <select id="rol_id" name="rol_id" class="form-control">
			                            	@foreach($roles as $rol)
			                            		<option value="{{$rol->id}}">{{$rol->denominacion}}</option>
              											@endforeach
              						        </select>
              				          </div>
              				        </div>

					                    <div class="col-sm-4">
				                      <!-- text input -->
					                      <div class="form-group">
					                        <label>Contraseña:</label>
					                        <input type="password" name = "password" class="form-control" placeholder="Introduzca contraseña">
					                      </div>
					                    </div>
									<div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Confirme Contraseña:</label>
				                        <input type="password" name="confirm_password" class="form-control" placeholder="Confirmar contraseña">
				                      </div>
				                    </div>
				                    </div>
                            <div class="row" id="row_region" style="display: none">
                              <div class="col-sm-4">
                              <!-- text input -->
                              <div class="form-group">
                                <label>Regiòn</label>
                                  <select id="region" disabled name="region_id" class="form-control">
                                    @foreach($regiones as $region)
                                      <option value="{{$region->id}}">{{$region->nombre}}</option>
                                    @endforeach
                                  </select>
                                </div>
                              </div>
                            </div>


	                            </div>
				            </div>
				        </div>
		                 	 <div class="card-footer" align="right">
			                  <button type="submit" class="btn btn-primary">Crear</button>
			                </div>
			                </form>
                        </div>
		            </div>
		        </div>
            </div>
        </div>
  	</section>
</div>


@endsection
@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_usuarios") !!}
@endif



<script type="text/javascript">
$(document).ready(function () {

  $('#rol_id').click(function(){
     var valor = $(this).val();
     // console.log('valor '+valor);
      if (valor==4){
        $('#region').prop('disabled', false);
        $('#row_region').show();
        //  $('#responsable_id').hide();
      }else{
        $('#region').prop('disabled', true);
        $('#row_region').hide();
      }
  });






  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection





