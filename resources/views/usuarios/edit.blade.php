@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Modificar Usuario 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Registro</a></li>
              <li class="breadcrumb-item active">Usuario</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>


   @php 
      use App\Http\Controllers\Controller\UsuariosController;
    @endphp
 <div class="row ">
      <div class="col s12 m12 l12 ">
        @if ($errors->any())
          <div class="alerta-errores">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif
      </div>
    </div>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
     
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
              
              <div class="card-body">
              	 <form method="POST" id="formulario_usuarios" name="formulario_usuarios"  action="/usuarios/{{encrypt($usuarios->id)}}">
                        <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="active" value="1">
                        {{ csrf_field() }}
                
                     	<div class="col-md-12">
				            <!-- general form elements disabled -->
				            <div class="card card-success">
				              <div class="card-header">
				                <h3 class="card-title">Usuario</h3>
				              </div>
				              <!-- /.card-header -->
				              <div class="card-body">
				              	 <div class="row">
				                    <div class="col-sm-4">
				                      <!-- text input -->
				                      	<div class="form-group">
				                        	<label>Nombres:</label>
					                        	<input type="text" name="name" value="{{$usuarios->name}}"  class="form-control" placeholder="Nombres">
					                    </div>
				                     </div>
				                   
									<div class="col-sm-4">
				                      <div class="form-group">
				                        	<label>Apellidos:</label>
					                        	<input type="text" name="last_name" value="{{$usuarios->last_name}}" class="form-control" placeholder="Apellidos">
					                    </div>
				                  	</div>

                            <div class="col-sm-4">
                              <div class="form-group">
                                  <label>Cédula:</label>
                                    <input type="text" name="cedula"  class="form-control" placeholder="Introduzca cédula" value="{{$usuarios->cedula}}">
                              </div>
                            </div>

                            <div class="col-sm-4" @if(Auth::user()->rol_id != 1) readonly @endif>
                              <div class="form-group">
                                  <label>Departamento:</label>
                                    <select name="departamento" @if(Auth::user()->rol_id != 1) disabled @endif class="form-control">
                                      <option>Seleccione departamento</option>

                                      <option @if($usuarios->departamento=="Coordinación Normas, Análisis y Diseño") selected @endif value="Coordinación Normas, Análisis y Diseño">Coordinación Normas, Análisis y Diseño</option>

                                      <option @if($usuarios->departamento=="Coordinación Seguimiento y Control de Gestión") selected @endif value="Coordinación Seguimiento y Control de Gestión">Coordinación Seguimiento y Control de Gestión</option>

                                      <option @if($usuarios->departamento=="Coordinación Seguimiento y Control de Gastos") selected @endif value="Coordinación Seguimiento y Control de Gastos">Coordinación Seguimiento y Control de Gastos</option>

                                      <option @if($usuarios->departamento=="Coordinación Seguimiento y Control de Inversión") selected @endif value="Coordinación Seguimiento y Control de Inversión">Coordinación Seguimiento y Control de Inversión</option>

                                      <option @if($usuarios->departamento=="Cosec") selected @endif value="Cosec">Cosec</option>

                                      <option @if($usuarios->departamento=="Gerencia General") selected @endif value="Gcia. General de Energía y Climatización">Gcia. General de Energía y Climatización</option>

                                      <option @if($usuarios->departamento=="Gerencia de Seguimiento y Control") selected @endif value="Gerencia de Seguimiento y Control">Gerencia de Seguimiento y Control</option>

                                      <option @if($usuarios->departamento=="Gcia. Cost. Energía y Climatización") selected @endif value="Gcia. Cost. Energía y Climatización">Gcia. Cost. Energía y Climatización</option>

                                      <option @if($usuarios->departamento=="Gcia. Oper. Mtto Reg. Capital") selected @endif value="Gcia. Oper. Mtto Reg. Capital">Gcia. Oper. Mtto Reg. Capital</option>

                                      <option @if($usuarios->departamento=="Gcia. Oper. Mtto Reg. Centro") selected @endif value="Gcia. Oper. Mtto Reg. Centro">Gcia. Oper. Mtto Reg. Centro </option>

                                      <option  @if($usuarios->departamento=="Gcia. Oper. Mtto Reg. Guayana") selected @endif value="Gcia. Oper. Mtto Reg. Guayana">Gcia. Oper. Mtto Reg. Guayana</option>
                                      
                                      <option  @if($usuarios->departamento=="Gcia. Oper. Mtto Reg. Oriente") selected @endif value="Gcia. Oper. Mtto Reg. Oriente">Gcia. Oper. Mtto Reg. Oriente</option>


                                        
                                      <option @if($usuarios->departamento=="Gcia. Oper. Mtto Reg. Los Llanos") selected @endif value="Gcia. Oper. Mtto Reg. Los Llanos">Gcia. Oper. Mtto Reg. Los Llanos</option>

                                      <option @if($usuarios->departamento=="Gcia. Oper. Mtto Reg. Occidente") selected @endif value="Gcia. Oper. Mtto Reg. Occidente">Gcia. Oper. Mtto Reg. Occidente</option>

                                      <option @if($usuarios->departamento=="Gcia. Oper. Mtto Reg. Los Andes") selected @endif value="Gcia. Oper. Mtto Reg. Los Andes">Gcia. Oper. Mtto Reg. Los Andes</option>

                                      <option @if($usuarios->departamento=="Gcia. de Proyectos e Ingeniería") selected @endif value="Gcia. de Proyectos e Ingeniería">Gcia. de Proyectos e Ingeniería</option>
                                    </select>
                              </div>
                            </div>
                            <div class="col-sm-4"  @if(Auth::user()->rol_id != 1) readonly @endif>
                              <div class="form-group">
                                  <label>Cargo:</label>
                                    <select name="cargo" @if(Auth::user()->rol_id != 1) disabled @endif class="form-control">
                                      <option>Seleccione un cargo</option>
                                      <option @if($usuarios->cargo=="Asistente") selected @endif value="Asistente">Asistente</option>

                                      <option @if($usuarios->cargo=="Asesor") selected @endif value="Asesor">Asesor</option>

                                      <option @if($usuarios->cargo=="Coordinador") selected @endif value="Coordinador">Coordinador</option>

                                      <option @if($usuarios->cargo=="Gerente") selected @endif value="Gerente">Gerente</option>

                                      <option @if($usuarios->cargo=="Lider") selected @endif value="Lider">Lider</option>

                                      <option @if($usuarios->cargo=="Supervisor") selected @endif value="Supervisor">Supervisor</option>

                                      <option @if($usuarios->cargo=="Tecnico") selected @endif value="Tecnico">Tecnico</option>

                                      <option @if($usuarios->cargo=="Especialista CCT") selected @endif value="Especialista CCT">Especialista CCT</option>

                                      <option @if($usuarios->cargo=="Consultor CCT") selected @endif value="Consultor CCT">Consultor CCT</option>
                                    </select>
                              </div>
                            </div>

				                   <div class="col-sm-4" @if(Auth::user()->rol_id != 1) readonly @endif>
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Correo:</label>
				                        <input type="text" @if(Auth::user()->rol_id != 1) readonly @endif class="form-control" name="email" value="{{$usuarios->email}}"  placeholder="Email">
				                      </div>
				                    </div>
				                  </div>

				                  <div class="row">
				                    <div class="col-sm-4" @if(Auth::user()->rol_id != 1) readonly @endif>
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Rol o Privilegio:</label>
				                        <select name="rol_id" @if(Auth::user()->rol_id != 1) disabled @endif class="form-control">
			                            	@foreach($roles as $rol)
			                            		<option @if($rol->id == $usuarios->rol_id) selected @endif value="{{$rol->id}}">{{$rol->denominacion}}</option>
											              @endforeach
						            	</select>
				                      </div>
				                    </div>
                           
					                    <div class="col-sm-4" @if(Auth::user()->rol_id != 1) readonly @endif>
				                      <!-- text input -->
					                      <div class="form-group">
					                        <label>Contraseña:</label>
					                        <input type="password"@if(Auth::user()->rol_id != 1) disabled @endif name = "password" value="{{$usuarios->password}}" class="form-control" placeholder="Password">
					                      </div>
					                    </div>
									           <div class="col-sm-4" @if(Auth::user()->rol_id != 1) readonly @endif>
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Confirme Contraseña:</label>
				                        <input type="password"@if(Auth::user()->rol_id != 1) disabled @endif name="confirm_password" class="form-control" value="{{$usuarios->password}}" placeholder="Confirmar Password">
				                      </div>
				                    </div>
				                    </div>
                            @if($usuarios->rol_id == 4)
                            <div class="row" id="row_region" >
                              <div class="col-sm-4">
                              <!-- text input -->
                              <div class="form-group">
                                <label>Regiòn</label>
                                  <select id="region"  name="region_id" class="form-control">
                                    @foreach($regiones as $region)
                                      <option @if($region->id == $usuarios->region_id ) selected @endif  value="{{$region->id}}">{{$region->nombre}}</option>
                                    @endforeach
                                  </select>
                                </div>
                              </div>
                            </div>
                            @endif

                            <div class="row">
                              <div class="col-md-12">
                                <label>Permisos(Solo): @foreach($usuarios->permisos as $per) {{$usuarios->helper($per->permiso)}}, @endforeach</label>
                                <div class="col-sm-12">
                              <!-- text input -->
                                <div class="form-group">

                                 
                                  <select multiple="permisos[]" name="permisos[]" class="form-control">
                                    <option value="GET">Ver</option>
                                    <option value="PUT">Actualizar</option>
                                    <option value="POST">Guardar</option>
                                    <option value="DELETE">Eliminar</option>
                                  </select>
                                </div>
                            </div>
                              </div>
                            </div>


	                            </div>
				            </div>
				        </div>
		                 	 <div class="card-footer" align="right">
			                  <button type="submit" class="btn btn-primary"><i class="fas fa-sync-alt nav-icon"></i>Actualizar</button>
			                </div>
			                </form>
                        </div>
		            </div>
		        </div>
            </div>
        </div>
  	</section>
</div>


@endsection

@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_usuarios") !!}
@endif



<script type="text/javascript">
$(document).ready(function () {

  $('#rol_id').click(function(){
     var valor = $(this).val();
     // console.log('valor '+valor);
      if (valor==4){
        $('#region').prop('disabled', false);
        $('#row_region').show();
        //  $('#responsable_id').hide();
      }else{
        $('#region').prop('disabled', true);
        $('#row_region').hide();
      }
  });







  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
   @if(Session::has('actualizado'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Datos actualizados con exito'
      })
      })
    });
  @endif
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection





