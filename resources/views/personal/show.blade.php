@extends ('/layouts/index')

@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
             Información del Persona
            <!--  <small>l</small> -->
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/personal">Personal</a></li>
              <li class="breadcrumb-item active">Info</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fa fa-user"></i>
                     Personal
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <dl>
                  <dt>Nombres y Apellidos</dt>
                  <dd>{{$personal->nombre}}  {{$personal->apellido}}</dd>
                  <dt>Cedula</dt>
                  <dd>{{$personal->cedula}}</dd>
                  <dt>Codigo P-00 CANTV</dt>
                  <dd>{{$personal->cod_p00}}</dd>
                 
                  <dt>Region</dt>
                 
                  <dd>{{$personal->regiones->nombre}}</dd>
                 
                  <dt>Telefono de Oficina</dt>
                  <dd>{{$personal->num_oficina}}</dd>
                  <dt>Telefono de Celular</dt>
                  <dd>{{$personal->num_personal}}</dd>
                  <dt>Email</dt>
                  <dd>{{$personal->correo}}</dd>
                  <dt>Cargo</dt>
                  
                  <dd>{{$personal->cargo}}</dd>
                  <dt>Estatus Empleado</dt>
                 

                 <!-- @var ={{$personal->status}}-->
                  @if ($personal->status===1)
                  <dd>ACTIVO</dd>
                  @elseif ($personal->status===2)

                  <dd>INACTIVO</dd>
                  @else
                  <dd>RETIRADO</dd>
                  @endif


                  <dt>Fotografia Empleado</dt>         
                
                 @if(isset($personal->FotoPersonal))
        <div class="image">
          <img src="{{asset('/img/multimedio/avatar').'/'.$personal->Multimedia->archivo}}" class="brand-image ">
        </div>
        @else
         <div class="image">
          <img src="{{asset('/img/multimedio/avatar/00.jpeg')}}" class="brand-image ">
        </div>
        @endif
        

                </dl>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
          
        </div>
      </div>
          <!-- ./col -->
       
          </div>
        
      </div>
    </div>
  </section>
</div>
@endsection