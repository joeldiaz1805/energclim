@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Registrar Nuevo Personal
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/personal">Personal</a></li>
              <li class="breadcrumb-item active">Crear </li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content --> 
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
             
              <div class="card-body">
      <form method="POST" id="formulario_personal" name="formulario_personal"  action="{{ url('/personal')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                   
                    <div class="col-md-12">
				            <!-- general form elements disabled -->
				             <div class="card card-success">
				              <div class="card-header">
				                <h3 class="card-title">Nuevo Personal</h3>
				              </div>
				    
				       <div class="card-body">

				              <!-- /.primera fila-->
                       <div class="row">
				                     <div class="col-sm-4">
				                      <!-- text input -->
				                       <div class="form-group">
				                         <label>Nombres</label>
					                      <input type="text" name="nombre"  class="form-control" maxlength="49" placeholder="Nombres">
					                     </div>
				                     </div>

				                   <div class="col-sm-4">
				                    <div class="form-group">
				                      <label>Apellidos</label>
					                    	<input type="text" name="apellido" required class="form-control" maxlength="49" placeholder="Apellidos">
					                  </div>
				                   </div>

				                   <div class="col-sm-4">
				                      <!-- text input -->
				                     <div class="form-group">
				                       <label>Cédula de Identidad</label>
				                       <input type="text" class="form-control" required name="cedula" maxlength="12" placeholder="Numero de Cedula">
				                     </div>
				                   </div>
				                </div>
          <!-- segunda fila -->
                     
                         <div class="row">
                               <div class="col-sm-4">
                              <!-- text input -->
                                <div class="form-group">
                                  <label>Código P-00</label>
                                    <input type="text" name="cod_p00" required class="form-control" maxlength="20" placeholder="Cod - P-00"> 

                               </div>
                              </div>
                           
                              <div class="col-sm-4">
                                <!-- text input -->
                                  <div class="form-group">
                                    <label>Región:</label>
                                      <select  name="region_id" id="region" class="form-control">
                                        <option>Seleccione una región</option>
                                        @foreach($regiones as $region)
                                          <option value="{{$region->id}}">{{$region->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                              </div>
                               <div class="col-sm-4">
                            <!-- text input -->
                              <div class="form-group">
                                <label>Estado:</label>
                                
                                <select name="estado_id" id="estado" class="form-control">
                                  
                                    <option value="">Seleccione un estado</option>
                                  
                              </select>
                              </div>
                          </div>

                         </div>



          <!-- tercera fila -->

				                  <div class="row">
                            <div class="col-sm-4">
                                  <!-- text input -->
                              <div class="form-group">
                                <label>Teléfono Oficina</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text"><i class="fas fa-phone"></i></span>
                                    </div>
                                    <input type="text" name="num_oficina" required  class="form-control" data-inputmask='"mask": "99999999999"' data-mask placeholder="02000000000">
                                  </div>
                              </div>
                            </div>
				                    <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Teléfono Celular</label>
				                        <div class="input-group">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text"><i class="fas fa-phone"></i></span>
                                    </div>
                                    <input type="text" name="num_personal" required  class="form-control" data-inputmask='"mask": "99999999999"' data-mask placeholder="04000000000">
                                  </div>
			                            	
				                      </div>
				                    </div>

					                    <div class="col-sm-4">
				                      <!-- text input -->
					                      <div class="form-group">
					                        <label>Correo Electrónico</label>
					                        <input type="text" class="form-control" required name="correo" maxlength="49" placeholder="TuEmail@Servidor.com">
					                      </div>
					                    </div>
									            
				                  </div>


          <!-- cuarta fila -->
                      
                       

                       
                          <div class="row">
                               <div class="col-sm-4">
				                      <div class="form-group">
				                        <label>Cargo</label>
                                    <select name="cargo" id="cargo" class="form-control">
                                      <option>Seleccione un cargo</option>
                                      <option value="Asistente">Asistente</option>
                                      <option value="Asesor">Asesor</option>
                                      <option value="Coordinador">Coordinador</option>
                                      <option value="Gerente">Gerente</option>
                                      <option value="Lider">Lider</option>
                                      <option value="Supervisor">Supervisor</option>
                                      <option value="Tecnico">Tecnico</option>
                                      <option value="Especialista CCT">Especialista CCT</option>
                                      <option value="Consultor CCT">Consultor CCT</option>
                                    </select>
				                      </div>
				                    </div>
                            <div class="col-sm-4">
                                  <!-- text input -->
                              <div class="form-group">
                                <label>Unidad</label>
                                <div class="input-group">
                                    
                                    <input type="text" name="unidad" required  class="form-control">
                                  </div>
                              </div>
                            </div>
                            <div class="col-sm-4">
                                  <!-- text input -->
                              <div class="form-group">
                                <label>Gerencia</label>
                                <div class="input-group">
                                    
                                    <input type="text" name="gerencia" required  class="form-control" >
                                  </div>
                              </div>
                            </div>
                            

                          
                      </div>
                       <div class="row">
                               <div class="col-sm-4">
                                <!-- text input -->
                                  <div class="form-group">
                                    <label>Supervisor inmediato:</label>
                                      <select  name="supervisor_id" id="supervisor" class="form-control select2">
                                        <option>Seleccione un supervisor</option>
                                        @foreach($personal as $super)
                                          <option value="{{$super->id}}">{{$super->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                              </div>
                              <div class="col-sm-4">
                              <div class="form-group">
                                <label>Estatus Empleado</label>
                                <select name="status" class="form-control">
                                         <option value="1">ACTIVO</option>
                                         <option value="2">INACTIVO</option>
                                         <option value="3">RETIRADO</option>
                                </select>
                              </div>
                            </div>

                            <div class="col-sm-4">
                             <div class="form-group">
                                <label>Subir Foto</label>
                                <input type="file" name="archivo">



                              
                              </div>
                            </div>
                              </div>
                              




                      <!--  CIERRA EL BODY -->
                   </div>
                <!-- CIERRA EL CAR CAR SUCES  --> 
         </div>
         <!--CIERRA EL CSFR-->
				</div>
      
         <div class="card-footer" align="right">
			     <button type="submit" class="btn btn-primary">Crear</button>
			   </div>
			               

    </form>
                  </div>
		            </div>
		        </div>
            </div>
        </div>
  	</section>
</div>


@endsection


@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_personal") !!}
@endif



<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });

$('select[id=region]').change(function () {
      console.log('hola')
    var region_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'estado/' + region_id, function (cidades) {
        $('select[id=estado]').empty();

        if(cidades.length != 0){
            $('select[id=estado]').append('<option value=""> Selecciona un estado</option>');
            $.each(cidades, function (key, value) {
                $('select[id=estado]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });

    });




  
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection