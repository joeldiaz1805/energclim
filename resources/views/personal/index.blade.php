@extends ('/layouts/index')

@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Personal
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Personal </a></li>
              <li class="breadcrumb-item active">lista</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Vista Preliminar de Registros del Personal</h3>
              <div align="right">
                  <a href="/personal/create" class="btn btn-success">Agregar Personal</a>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class=" display nowrap  table table-bordered table-striped" style="width:100%">
                <thead>

                  <tr align="center">
                    <th class="not-export-col">Acciones</th>
                    <th>Nombres y Apellidos</th>
                    <th>Cedula</th>
                    <th>Cod P00</th>
                    <th>Region</th>
                    <th>Estado</th>
                    <th>Unidad</th>
                    <th>Gerencia</th>
                    <th>Telefono</th>
                    <th>Celular</th>
                    <th>Email</th>
                    <th>Cargo</th>
                    <th>Supervisor inmediato</th>
                    <th>Status</th>
                    <!--<th>Foto</th> -->
                  </tr>
                </thead>
                <tbody>
               <!-- aca se empieza a cargar el row principal de personal-->
                 @foreach($personal as $pers)
                  <tr align="center"  id="tdId_{{$pers->id}}">
                    <td>
                     <a href="/personal/{{$pers->id}}/edit" class="btn btn-warning"><i class="far fa-edit"></i></a>
                      <a href="/personal/{{$pers->id}}" class="btn btn-success"><i class="far fa-eye"></i></a>
                      <a id="intt" @if($pers->status==1) class="btn btn-success" onclick="deleteItem('{{$pers->id}}')" @else class="btn btn-danger" onclick="activeItem('{{$pers->id}}')" @endif class="btn btn-danger"><i id="ele"  class="fa fa-user-times"></i></a>
                      </td>
                    <td>{{$pers->nombre}} {{$pers->apellido}}</td>
                    <td>{{$pers->cedula}}</td>
                    <td>{{$pers->cod_p00}}</td>
                    <td>@if(!empty($pers->regiones)) {{$pers->regiones->nombre}} @endif</td>
                    <td>@if(!empty($pers->estado)) {{$pers->estado->nombre}} @endif</td>
                    <td>{{$pers->unidad}}</td>
                    <td>{{$pers->gerencia}}</td>
                    <td>{{$pers->num_oficina}}</td>
                    <td>{{$pers->num_personal}}</td>
                    <td>{{$pers->correo}}</td>
                    <td>{{$pers->cargo}}</td>
                    <td>{{$pers->supervisor->nombre  ?? 'no asignado'}} {{$pers->supervisor->apellido ?? 'no asignado'}}</td>
                    <td id="act_{{$pers->status}}"> @if($pers->status==1) ACTIVO @elseif($pers->status==2) INACTIVO @else RETIRADO @endif</td>
                   <!-- <td>{{$pers->FotoPersonal}}</td>-->
                  </tr>
                  @endforeach






                 
                
                </tbody>
              </table>
           </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('scriptsalerts')


<script>

  
</script>

  <script>

    function deleteItem(item){
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'Â¿Desea Inhabilitar este usuario de la lista?',
      text: "Estas por Inhabilitar un usuario!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, Inhabilitarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {

      if (result.value) {



            $.ajax({
                url: "/personal/"+item,
                type:"DELETE",
                   headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    

                    success:function(result, status, xhr){                    
                        
                        if (status == 'success') {

                          swalWithBootstrapButtons.fire(
                            'Inhabilitado con Exito!',
                            'Se Inhabilito con exito',
                            'success'
                          )

                          //$("#tdId_"+item).hide();
                          document.getElementById("intt").className="btn btn-danger";
                          //intt.style(color:green);
                           document.getElementById("act_"+item).innerHTML = "Inativo";
                          //$("#act_"+item).innerHTML = 'Inativo' ;



                        }
                    },
                    error:function(xhr, status, error){
                      

                      Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se Inhabilito ningun usuario '+error,
                        footer: '<a href>Seguramente hubo un problema de conexiÃ³n</a>'
                      })
                      

                    }
                });
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No se ha quitado el producto.',
          'error'
        )
      }
    })
  }
  function activeItem(item){
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'Â¿Desea Habilitar este usuario de la lista?',
      text: "Estas por Habilitar un usuario!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, Habilitarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {

      if (result.value) {



            $.ajax({
                url: "/personal/"+item,
                type:"DELETE",
                   headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    

                    success:function(result, status, xhr){                    
                        
                        if (status == 'success') {

                          swalWithBootstrapButtons.fire(
                            'Habilitado con Exito!',
                            'Se HabilitÃ² con exito',
                            'success'
                          )

                          //$("#tdId_"+item).hide();
                          document.getElementById("intt").className="btn btn-success";
                          var h= document.getElementById("act_"+item);
                          h.innerHTML = "Activo";

                          //$("#act_"+item).innerHTML = 'Activo' ;



                        }
                    },
                    error:function(xhr, status, error){
                      

                      Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se Habilitado ningun usuario '+error,
                        footer: '<a href>Seguramente hubo un problema de conexiÃ³n</a>'
                      })
                      

                    }
                });
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No se ha Habilitado el usuario.',
          'error'
        )
      }
    })
  }
</script>


@endsection
