@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Modificar Datos Del Personal
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/personal">Personal</a></li>
              <li class="breadcrumb-item active">Modificar Personal</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content --> 
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
             
              <div class="card-body">
      <form method="POST" id="formulario_personal" name="formulario_personal"  action="/personal/{{$personal->id}}" enctype="multipart/form-data">
        <input type="hidden" name="_method" value="PUT">
                        {{ csrf_field() }}
                    
                   
                    <div class="col-md-12">
                    <!-- general form elements disabled -->
                     <div class="card card-success">
                      <div class="card-header">
                        <h3 class="card-title">Modificar/Actualizar Personal</h3>
                      </div>
            
               <div class="card-body">

                      <!-- /.primera fila-->
                       



 <div class="row">
                             <div class="col-sm-4">
                              <!-- text input -->
                               <div class="form-group">
                                <label>Nombres</label>
                                 <input type="text" name="nombre" value="{{$personal->nombre}}"  class="form-control" maxlength="49" placeholder="Nombres" required>
                               </div>
                             </div>

                           <div class="col-sm-4">
                            <div class="form-group">
                              <label>Apellidos</label>
                               <input type="text" name="apellido" value="{{$personal->apellido}}"  class="form-control" maxlength="49" placeholder="Apellidos">
                            </div>
                           </div>

                           <div class="col-sm-4">
                              <!-- text input -->
                             <div class="form-group">
                               <label>Cedula de Identidad</label>
                               <input type="text" name="cedula" value="{{$personal->cedula}}"  class="form-control" maxlength="12" placeholder="Cedula">
                             </div>
                           </div>
                        </div>

          <!-- segunda fila -->
                     
                         <div class="row">
                               <div class="col-sm-4">
                              <!-- text input -->
                                <div class="form-group">
                                  <label>Codigo P-00 CANTV</label>
                                     <input type="text" name="cod_p00" value="{{$personal->cod_p00}}"  class="form-control" maxlength="20" placeholder="cod_p00">

                               </div>
                              </div>
                           
                               <div class="col-sm-4">
                                <!-- text input -->
                                  <div class="form-group">
                                    <label>Región:</label>

                                <select name="region_id" id="region" class="form-control">
                                    @foreach($regiones as $region)
                                          <option @if($region->id == $personal->region_id) seleted @endif value="{{$region->id}}">{{$region->nombre}}</option>
                                        @endforeach
                   
                                    </select>
                                </div>
                              </div>
                              <div class="col-sm-4">
                            <!-- text input -->
                              <div class="form-group">
                                <label>Estado:</label>
                                
                                <select name="estado_id" id="estado" class="form-control">
                                  @if($personal->estado)
                                    <option value="{{$personal->estado->id}}">{{$personal->estado->nombre}}</option>
                                  @else
                                    <option value="">Selecciona un Estado</option>
                                    @endif
                              </select>
                              </div>
                          </div>
                              
                         </div>

  

          <!-- tercera fila -->

                          <div class="row">
                            <div class="col-sm-4">
                                  <!-- text input -->
                              <div class="form-group">
                                <label>Telefono Oficina</label>
                               <input type="text" name="num_oficina" value="{{$personal->num_oficina}}"  required data-inputmask='"mask": "99999999999"' data-mask class="form-control" placeholder="02000000000">
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <!-- text input -->
                              <div class="form-group">
                                <label>Telefono Celular</label>
                                 <input type="text" name="num_personal" value="{{$personal->num_personal}}"  data-inputmask='"mask": "99999999999"' data-mask class="form-control" placeholder="Telefono Celular">
                                    
                              </div>
                            </div>

                              <div class="col-sm-4">
                              <!-- text input -->
                                <div class="form-group">
                                  <label>Correo Electronico</label>
                                  <input type="text" name="correo" value="{{$personal->correo}}"  class="form-control" maxlength="49" placeholder="E-Mail">
                                </div>
                              </div>
                              
                          </div>


          <!-- cuarta fila -->
              <div class="row">
                               <div class="col-sm-4">
                              <!-- text input -->
                             <div class="form-group">
                                <label>Cargo</label>
                                    <select name="cargo" id="cargo" class="form-control">
                                      <option>Seleccione un cargo</option>
                                      <option @if($personal->cargo == "Asistente") selected @endif value="Asistente">Asistente</option>
                                      <option @if($personal->cargo == "Asesor") selected @endif value="Asesor">Asesor</option>
                                      <option @if($personal->cargo == "Coordinador") selected @endif value="Coordinador">Coordinador</option>
                                      <option @if($personal->cargo == "Gerente") selected @endif value="Gerente">Gerente</option>
                                      <option @if($personal->cargo == "Lider") selected @endif value="Lider">Lider</option>
                                      <option @if($personal->cargo == "Supervisor") selected @endif value="Supervisor">Supervisor</option>
                                      <option @if($personal->cargo == "Tecnico") selected @endif value="Tecnico">Tecnico</option>
                                      <option @if($personal->cargo == "Especialista CCT") selected @endif value="Especialista CCT">Especialista CCT</option>
                                      <option @if($personal->cargo == "Consultor CCT") selected @endif value="Consultor CCT">Consultor CCT</option>
                                    </select>
                              </div>
                            </div>

                            <div class="col-sm-4">
                                  <!-- text input -->
                              <div class="form-group">
                                <label>Unidad</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text"><i class="fas fa-phone"></i></span>
                                    </div>
                                    <input type="text" value="{{$personal->unidad}}" name="unidad" required  class="form-control" >
                                  </div>
                              </div>
                            </div>
                            <div class="col-sm-4">
                                  <!-- text input -->
                              <div class="form-group">
                                <label>Gerencia</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text"><i class="fas fa-phone"></i></span>
                                    </div>
                                    <input type="text" value="{{$personal->gerencia}}" name="gerencia" required  class="form-control">
                                  </div>
                              </div>
                            </div>






                      </div>
                         <div class="row">
                               <div class="col-sm-8">
                                <!-- text input -->
                                  <div class="form-group">
                                    <label>Supervisor inmediato:</label>
                                      <select  name="supervisor_id" id="supervisor" class="form-control select2">
                                        <option>Seleccione un supervisor</option>
                                        @foreach($supervisores as $super)
                                          <option @if($super->id == $personal->supervisor_id) selected @endif value="{{$super->id}}">{{$super->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                              </div>


                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Status Empleado</label>
                                <select name="status" class="form-control" value="">
                                            <option @if($personal->status == 1 ) selected @endif value="1" >ACTIVO</option>
                                            <option @if($personal->status == 2 ) selected @endif value="2" >INACTIVO</option>
                                            <option @if($personal->status == 3 ) selected @endif value="3" >RETIRADO</option>
                                 </select>
                               </div>
                               </div>
<!--  la foto aca --> 
                       <div class="col-sm-4">
                            <label>Foto Empleado</label>
                               @if(isset($personal->FotoPersonal))
                                 <div class="image">
                                 <img src="{{asset('/img/multimedio/avatar').'/'.$personal->Multimedia->archivo}}" class="brand-image ">
                                 </div>
                                @else
                                  <div class="image">
                                  <img src="{{asset('/img/multimedio/avatar/00.jpeg')}}" class="brand-image ">
                                  

                                  </div>

                                 @endif
                                 <label>Actualizar Foto</label>
                                <input type="file" name="archivo">
                           </div>
                              
                              </div>
                   
                <!-- CIERRA EL CAR CAR SUCES  --> 
         </div>
         <!--CIERRA EL CSFR-->
        </div>
      
         <div class="card-footer" align="right">
           <button type="submit" class="btn btn-primary">Actualizar</button>
         </div>
                 
    </form>
                  </div>
                </div>
            </div>
            </div>
        </div>
    </section>
</div>


@endsection


@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_personal") !!}
@endif



<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });

$('select[id=region]').change(function () {
      console.log('hola')
    var region_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'estado/' + region_id, function (cidades) {
        $('select[id=estado]').empty();

        if(cidades.length != 0){
            $('select[id=estado]').append('<option value=""> Selecciona un estado</option>');
            $.each(cidades, function (key, value) {
                $('select[id=estado]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });

    });


  $('#quickForm').validate({
    rules: {
      email: {
        
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection