@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Octavo Proyecto de Servicio Universal de Telecomnicaciones - O.P.S.U.T. 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/opsut">O.P.S.U.T.</a></li>
              <li class="breadcrumb-item active">Datos de Nodo - Requerimientos</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content --> 
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
             
              <div class="card-body">
                 @foreach ($requeopsut as $req1)
      <form method="POST" id="formulario_requerimiento_actualizar" name="formulario_requerimiento_actualizar"  action="/nodos/{{$nodoopsut->id}}/{{$req1->id}}" enctype="multipart/form-data">
       @endforeach


          <input type="hidden" name="_method" value="PUT">
        
                        {{ csrf_field() }}
                    </input>
                   
                    <div class="col-md-12">
                    <!-- general form elements disabled -->
                     <div class="card card-success">
                      <div class="card-header">
                        <h3 class="card-title">Nodo O.P.S.U.T. {{$nodoopsut->codigo}}, Requerimientos - Editar</h3>
                      </div>
            
               <div class="card-body">

                      <!-- /.primera fila-->
                       



        <div class="row">




                <!-- /.card-body -->

                             <div class="col-sm-4">
                              <!-- text input -->
                               <div class="form-group">
                                <label>Nombre</label>
                                <dd>{{$nodoopsut->nombre}}</dd>
                               </div>
                             </div>

                           <div class="col-sm-4">
                            <div class="form-group">
                              <label>Codigo CONATEL</label>
                              <dd>{{$nodoopsut->codigo}}</dd>
                            </div>
                           </div>

                           <div class="col-sm-4">
                              <!-- text input -->
                             <div class="form-group">
                               <label>Tipo de Nodo</label>
                               <dd>@if($nodoopsut->tipo == 1) NUEVO  @else ADECUACION @endif </dd>
                             </div>
                           </div>
                        </div>

          <!-- segunda fila -->
                     
                         <div class="row">
                               <div class="col-sm-4">
                              <!-- text input -->
                                <div class="form-group">
                                  <label>Direcion Fisica del Nodo</label>
                                     <dd>{{$nodoopsut->ubicacion}}</dd>

                               </div>
                              </div>
                           
                               <div style="display: none" class="col-sm-4">
                                <!-- text input -->
                                  <div class="form-group">
                                    <label>Latitud UTM</label>
                                     <dd>{{$nodoopsut->latitud}}</dd>
                               
                                </div>
                              </div>
                               
                            <div style="display: none" class="col-sm-4">
                                  <!-- text input -->
                              <div class="form-group">
                                <label>Longitud UTM</label>
                               <dd>{{$nodoopsut->longitud}}</dd>
                              </div>
                            </div>
                         </div>

  

          <!-- tercera fila -->

                          <div class="row">
                            <div class="col-sm-4">
                              <!-- text input -->
                              <div class="form-group">
                                <label>Status del Nodo</label>
                                 
                                     <select disabled="disabled" name="status" class="form-control" value="">
                                            <option @if($nodoopsut->stat_nod== 1 ) selected @endif value="1" >ACTIVO</option1>
                                            <option @if($nodoopsut->stat_nod== 2 ) selected @endif value="2" >BYPASEADO</option>
                                            <option @if($nodoopsut->stat_nod== 3 ) selected @endif value="3" >RETIRADO</option>
                                 </select>
                              </div>
                            </div>

                              <div class="col-sm-4">
                              <!-- text input -->
                                <div class="form-group">
                                  <label>Estado</label>
                                 <dd>{{$nodoopsut->estados->nombre}}</dd>
                                </div>
                              </div>
                              
                               <div class="col-sm-4">
                              <!-- text input -->
                              <div class="form-group">
                                <label>Region</label>
                               <dd>{{$nodoopsut->regiones->nombre}}</dd>
                              </div>
                            </div>
                          </div>


          <!-- cuarta fila -->
                <div class="row">
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Reaponsable</label>
                                 <dd>{{$nodoopsut->personal->nombre}} 
                                     {{$nodoopsut->personal->apellido}}</dd>
                                <dd>{{$nodoopsut->personal->cod_p00}}</dd>
                                <dd>{{$nodoopsut->personal->num_oficina}}</dd>
                                <dd>{{$nodoopsut->personal->num_personal}}</dd>
                               </div>
                               </div>

                       <div class="col-sm-4">
                        <div class="form-group">
                            <label>Observacion</label>
                             <dd> <textarea disabled readonly style="height: 100px; width: 100%";>{{$nodoopsut->observacion}}</textarea></dd>
                            </div>      
                           </div>

                              <div class="col-sm-4">
                             <div class="form-group">

                                </div>
                            </div>
                    
               </div>
                   
                <!-- CIERRA EL CAR CAR SUCES  --> 
         </div>

 <div class="col-md-12">


            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Actualizar Datos del Requerimiento </h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->


              <form role="form">
                <div class="card-body">
                   <div class="row">

                   @foreach($requeopsut as $req) 
                 <div class="col-md-4 text-center">
                  <div class="form-group">
                    <label for="">Codigo ID de Requerimiento</label>
                    <dd>{{$req->id}}</dd>
                   
                  </div>
                  </div>

                  <div class="col-md-4 text-center">
                  <div class="form-group">
                    <label for="">Equipo que Necesita el Requerimiento</label>
                    <dd>{{$req->invequipo2->nombre}}</dd>
                    
                  </div>
                  </div>

                  <div class="col-md-4 text-center">
                   <div class="form-group">
                    <label for="">Nodo Donde Esta Instalado el Equipo</label>

                    <dd>Nodo: {{$req->nodoopsut->nombre}}, Codigo: {{$req->nodoopsut->codigo}}</dd>
                    
                  </div>
                  </div>



                 
 </div>


 <div class="row">
                   
              <div class="col-md-4 text-center">
                   <div class="form-group">
                    <label for="">Requerimiento</label>
                    <dd><textarea name="requerimiento" style="height: 100px; width: 100%";>{{$req->requerimiento}}</textarea></dd>

                    
                  </div>
                  </div>
                   <div class="col-md-4 text-center">
                   <div class="form-group">
                    <label for="">Observacion</label>
                    <dd><textarea name="observacion" style="height: 100px; width: 100%";>{{$req->observacion}}</textarea></dd>

                   
                  </div>
                  </div>

                   

                  <div class="col-md-4 text-center">
                   <div class="form-group">
                    <label for="">Estado del Requerimiento</label>
                   
                      <select  name="stat_req_nod" class="form-control" value="">
                                            <option @if($req->stat_req_nod==1) selected @endif value="1" >REQUERIDO</option>
                                            <option @if($req->stat_req_nod==2) selected @endif value="2" >PROCESADO</option>
                                            <option @if($req->stat_req_nod==3) selected @endif value="3" >DESCARTADO</option>
                                           
                      </select>

                  </div>
                  </div>
                 
                @endforeach
                </div>
                 </div>


                 
              </form>
            </div>





         <!--CIERRA EL CSFR-->
        </div>
      
         <div class="card-footer" align="right">
           <button type="button" onclick="history.back()" name="volver atrás" value="volver atrás" class="btn btn-primary"> Volver </button>
           <button type="submit" class="btn btn-primary">Actualizar Informacion del Requerimiento</button>
         </div>
                 
    </form>
                  </div>
                </div>
            </div>
          </div>
        </div>
    </section>
</div>





@endsection


@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_requerimiento_actualizar") !!}
@endif



<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection