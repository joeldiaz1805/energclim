@extends ('/layouts/index')

@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Aire Acondicionado 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inventario</a></li>
              <li class="breadcrumb-item active">Aire Acondicionado</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Vista Preliminar de Registros</h3>
              <div align="right">
                          <a onclick="history.back()" class="btn btn-danger"><i class="fa fa-arrow-left"></i></a>

                 @if(sizeof($aa) == 0)
                  <a href="/aire_acondicionado/create?url={{Request::segment(2)}}" class="btn btn-success">Nuevo Registro</a>
                @else
                  <a href="/aire_acondicionado/create?url={{encrypt($aa[0]->ubicacion->central->localidad_id)}}" class="btn btn-success">Nuevo Registro</a>
                @endif
              </div>
                 
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class=" display nowrap table table-bordered table-striped" style="width:100%">
                <thead>
                  <tr align="center">
                    <th class="not-export-col">Acciones</th>
                    <th>Operatividad</th>
                   
                    <th>Marca</th>
                    <th>Modelo</th>
                   
                    <th>Tipo de Compresor</th>
                    <th>Toneladas de refrigeración Instaladas</th>
                    <th>Toneladas de refrigeración Operativas</th>
                    <th>Toneladas de refrigeración Faltantes</th>
                    <th>Tipo de Refrigerante</th>
                    
                    <th>Falla/Observaciones</th>
                    <th>Fecha Attualización</th>
                    <th>Usuario</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($aa as $aire)
                  <tr align="center"  id="tdId_{{$aire->id}}">
                    <td>
                      <a href="/aire_acondicionado/{{encrypt($aire->id)}}/edit?url={{encrypt($aire->ubicacion->central->localidad_id)}}" class="btn btn-warning"><i class="far fa-edit"></i></a>
                    </td>
                    <td>{{$aire->operatividad}}</td>
        
                    <td>{{$aire->marca}}</td>
                    <td>{{$aire->modelo}}</td>
                   
                 
                    <td>{{$aire->tipo_compresor}}</td>
                    <td>{{$aire->ton_refrig_instalada}}</td>
                    <td>{{$aire->ton_refrig_operativa}}</td>
                    <td>{{$aire->ton_refrig_faltante}}</td>
                    <td>{{$aire->tipo_refrigerante}}</td>

                    <td>{{$aire->observaciones}}</td>
                    <td>{{$aire->created_at}}</td>
                    <td>{{$aire->usuario->name ?? 'Sin Cambios'}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
           </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('scriptsalerts')


<script>
   @if(Session::has('exito'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Aire Acondicionado Guardado con exito'
      })
      })
    });
  @endif
   @if(Session::has('actualizado'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Datos actualizados con exito'
      })
      })
    });
  @endif
  
</script>

  <script>
    function deleteItem(item){
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Desea eliminar este Aire Acondicionado del intentario?',
      text: "Estas por quitar este equipo del Inventario!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, quitarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {

      if (result.value) {



            $.ajax({
                url: "/aire_acondicionado/"+item,
                type:"DELETE",
                   headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    

                    success:function(result, status, xhr){                    
                        
                        if (status == 'success') {

                          swalWithBootstrapButtons.fire(
                            'Quitado con Exito!',
                            'Se eliminó con exito',
                            'success'
                          )

                          $("#tdId_"+item).hide();
                         // txt.innerHTML = result ;



                        }
                    },
                    error:function(xhr, status, error){
                      

                      Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se eliminó el producto, vuelva a intentarlo mas tarde '+error,
                        footer: '<a href>Seguramente hubo un problema de conexión</a>'
                      })
                      

                    }
                });
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No se ha quitado el producto.',
          'error'
        )
      }
    })
  }
</script>


@endsection