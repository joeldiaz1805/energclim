@extends ('/layouts/index')

@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
             Mapa
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Mapa </a></li>
              <li class="breadcrumb-item active">Mpas</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title"> Vista Preliminar para los diferentes Mapa</h3>
              <div align="right">
                  <a href="#" class="btn btn-success">Mapa</a>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              
              <div id="mapid" style="width: 100%; height: 400px;"></div>


              <input type="hidden" id="nodos" name="nodos" value="{{$nodos}}">

           </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('scriptsalerts')

<script>
  var mymap = L.map('mapid').setView([10.102150, -68,2238], 5);
 const tiles = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
  }).addTo(mymap);
 var cant_nod= document.getElementById('nodos').value;
 var json = JSON.parse(cant_nod)
 console.log('cantidad de nodos',json.length );
   for (var i = 0; i < json.length; i++) {
     console.log(json[i].latitud, json[i].longitud);
       L.marker([ parseInt(json[i].latitud),parseInt(json[i].longitud) ]).addTo(mymap).bindPopup(json[i].nombre ,"</b><br />nodoOpsut.");


   }




 // L.marker([10.102150, -64,421763]).addTo(mymap).bindPopup("<b>Las Lajitas</b><br />Nodo Opsut");
</script>
@endsection