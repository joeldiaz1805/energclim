@extends ('/layouts/index')

@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
             Nodos O.P.S.U.T.
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">O.P.S.U.T. </a></li>
              <li class="breadcrumb-item active">Listado General de Nodos</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title"> Vista Preliminar para los diferentes Nodos O.P.S.U.T.</h3>
             <!--  <div align="right">
                  <a href="#" class="btn btn-success">Agregar Nodo</a>
              </div> -->
              <div class="row">
                <div class="col-12">
                  <i class="fa fa-unlink" ></i>Sin informaciòn
                  <i class="fa fa-times" style="color: red"></i> Critico
                  <i class="fas fa-exclamation-triangle"></i> Deficiente
                  <i class="fa fa-minus" style="color: skyblue"></i> Regular
                   <i class="fa fa-check" style="color: green"></i> Òptimo
                </div>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>

                  <tr align="center">
                       <th>Region</th>
                       <th>Estado</th>
                       <th>Nombre y Cod</th>
                       <th>MG</th>
                       <th>BB</th>
                       <th>CUADRO FUERZA</th>
                       <th>AA</th>
                  </tr>
                </thead>
                <tbody>
               <!-- aca se empieza a cargar el row principal de personal-->
                 @foreach($nodoopsut as $nodo)
                  <tr @if($nodo->stat_nod==2 || $nodo->stat_nod==4) style="background-color:red; color:white" @endif align="center"  id="nod_{{$nodo->id}}">
                    <td>{{$nodo->regiones->nombre}}</td>
                    <td>{{$nodo->estados->nombre}}</td>
                    <td>{{$nodo->nombre}} / {{$nodo->codigo}}</td>
                    <td>
                        @if(sizeof($nodo->detalleopsutpivot) > 0)
                          @php($total = $nodo->detalleopsutpivot->where('equipo_id',1)->count())
                          @php( $mgOper  = $nodo->detalleopsutpivot->where('equipo_id',1)->where('status',1)->count() )
                         @if($total > 0 )                          
                              @php($value = round(($mgOper*100)/$total ))
                             
                              @if($value <= 25 )
                                <i class="fa fa-times" style="color: red">
                              @elseif($value > 25 &&  $value <= 50 )
                                <i class="fas fa-exclamation-triangle"></i>
                               @elseif($value > 50 && $value < 75 )
                                <i class="fa fa-minus" style="color: skyblue"></i>
                               @elseif($value >= 75 )
                                 <i class="fa fa-check" style="color: green"></i>
                              @endif
                          @else
                            <i class="fa fa-unlink" ></i>
                          @endif
                        @endif
                    </td>
                    <td>
                       @if(sizeof($nodo->detalleopsutpivot) > 0)
                          @php($total = $nodo->detalleopsutpivot->where('equipo_id',9)->count())
                          @php( $mgOper  = $nodo->detalleopsutpivot->where('equipo_id',9)->where('status',1)->count() )
                          
                          @if($total > 0 )                          
                              @php($value = round(($mgOper*100)/$total ))
                             
                              @if($value <= 25 )
                                <i class="fa fa-times" style="color: red">
                              @elseif($value > 25 &&  $value <= 50 )
                                <i class="fas fa-exclamation-triangle"></i>
                               @elseif($value > 50 && $value < 75 )
                                <i class="fa fa-minus" style="color: skyblue"></i>
                               @elseif($value >= 75 )
                                 <i class="fa fa-check" style="color: green"></i>
                              @endif
                          @else
                            <i class="fa fa-unlink" ></i>

                          @endif
                        @endif
                  
                    </td>
                    <td>
                       
                        @if(sizeof($nodo->detalleopsutpivot) > 0)
                          @php($total = $nodo->detalleopsutpivot->where('equipo_id',2)->count())
                          @php( $mgOper  = $nodo->detalleopsutpivot->where('equipo_id',2)->where('status',1)->count() )
                          @if($total > 0 )                          
                              @php($value = round(($mgOper*100)/$total ))
                              
                             
                              @if($value <= 25 )
                                <i class="fa fa-times" style="color: red">
                              @elseif($value > 25 &&  $value <= 50 )
                                <i class="fas fa-exclamation-triangle"></i>
                               @elseif($value > 50 && $value < 75 )
                                <i class="fa fa-minus" style="color: skyblue"></i>
                               @elseif($value >= 75 )

                                 <i class="fa fa-check" style="color: green"></i>
                              @endif
                          @else
                            <i class="fa fa-unlink" ></i>
                          @endif

                        @endif
                    </td>
                    <td>
                        @if(sizeof($nodo->detalleopsutpivot) > 0)
                          @php($total = $nodo->detalleopsutpivot->where('equipo_id',3)->count())
                          @php( $mgOper  = $nodo->detalleopsutpivot->where('equipo_id',3)->where('status',1)->count() )
                         @if($total > 0 )                          
                            @php($value = round(($mgOper*100)/$total ))
                           
                            @if($value <= 25 )
                              <i class="fa fa-times" style="color: red">
                            @elseif($value > 25 &&  $value <= 50 )
                              <i class="fas fa-exclamation-triangle"></i>
                             @elseif($value > 50 && $value < 75 )
                              <i class="fa fa-minus" style="color: skyblue"></i>
                             @elseif($value >= 75 )
                               <i class="fa fa-check" style="color: green"></i>
                            @endif
                          @else
                            <i class="fa fa-unlink" ></i>

                          @endif
                        @endif
                    </td>
                  </tr>
                  @endforeach






                 
                
                </tbody>
              </table>


              
           </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('scriptsalerts')


<script>

  
</script>

  <script>

    function deleteItem(item){
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Desea Inhabilitar este usuario de la lista?',
      text: "Estas por Inhabilitar un usuario!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, Inhabilitarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {

      if (result.value) {



            $.ajax({
                url: "/personal/"+item,
                type:"DELETE",
                   headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    

                    success:function(result, status, xhr){                    
                        
                        if (status == 'success') {

                          swalWithBootstrapButtons.fire(
                            'Inhabilitado con Exito!',
                            'Se Inhabilito con exito',
                            'success'
                          )

                          //$("#tdId_"+item).hide();
                          document.getElementById("intt").className="btn btn-danger";
                          //intt.style(color:green);
                           document.getElementById("act_"+item).innerHTML = "Inativo";
                          //$("#act_"+item).innerHTML = 'Inativo' ;



                        }
                    },
                    error:function(xhr, status, error){
                      

                      Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se Inhabilito ningun usuario '+error,
                        footer: '<a href>Seguramente hubo un problema de conexión</a>'
                      })
                      

                    }
                });
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No se ha quitado el producto.',
          'error'
        )
      }
    })
  }
  function activeItem(item){
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Desea Habilitar este usuario de la lista?',
      text: "Estas por Habilitar un usuario!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, Habilitarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {

      if (result.value) {



            $.ajax({
                url: "/personal/"+item,
                type:"DELETE",
                   headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    

                    success:function(result, status, xhr){                    
                        
                        if (status == 'success') {

                          swalWithBootstrapButtons.fire(
                            'Habilitado con Exito!',
                            'Se Habilitò con exito',
                            'success'
                          )

                          //$("#tdId_"+item).hide();
                          document.getElementById("intt").className="btn btn-success";
                          var h= document.getElementById("act_"+item);
                          h.innerHTML = "Activo";

                          //$("#act_"+item).innerHTML = 'Activo' ;



                        }
                    },
                    error:function(xhr, status, error){
                      

                      Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se Habilitado ningun usuario '+error,
                        footer: '<a href>Seguramente hubo un problema de conexión</a>'
                      })
                      

                    }
                });
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No se ha Habilitado el usuario.',
          'error'
        )
      }
    })
  }
</script>


@endsection