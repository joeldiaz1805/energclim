@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
             Octavo Proyecto de Servicio Universal de Telecomnicaciones - O.P.S.U.T.
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/opsut">O.P.S.U.T.</a></li>
              <li class="breadcrumb-item active">Informacion General del Nodo </li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content --> 
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
             
              <div class="card-body">
      <form method="POST" id="formulario_nodo_all" name="requerimiento"  action="#" enctype="multipart/form-data">
        <input type="hidden" name="_method" value="PUT">
                        {{ csrf_field() }}
                    
                   
                    <div class="col-md-12">
                    <!-- general form elements disabled -->
                     <div class="card card-primary">
                      <div class="card-header">
                        <h3 class="card-title"> Datos del Nodo O.P.S.U.T.  {{$nodoopsut->nombre}}</h3>
                        <div align="right">
                          <a href="/nodos" class="btn btn-danger"><i class="fa fa-arrow-left"></i></a>
                        </div>
                      </div>
            
               <div class="card-body">

                      <!-- /.primera fila-->
                       



        <div class="row">
                             <div class="col-sm-4">
                              <!-- text input -->
                               <div class="form-group">
                                <label>Nombre</label>
                                <dd>{{$nodoopsut->nombre}}</dd>
                               </div>
                             </div>

                           <div class="col-sm-4">
                            <div class="form-group">
                              <label>Codigo CONATEL</label>
                              <dd>{{$nodoopsut->codigo ?? 'No asignado'}}</dd>
                            </div>
                           </div>

                           <div class="col-sm-4">
                              <!-- text input -->
                             <div class="form-group">
                               <label>Tipo de Nodo</label>
                               <dd>{{$nodoopsut->tipo ?? 'sin tipo'}} </dd>
                             </div>
                           </div>
                        </div>

          <!-- segunda fila -->
                     
                         <div class="row">
                               <div class="col-sm-4">
                              <!-- text input -->
                                <div class="form-group">
                                  <label>Direcion Fisica del Nodo</label>
                                     <dd>{{$nodoopsut->ubicacion ?? 'no ubicado'}}</dd>

                               </div>
                              </div>
                              <div class="col-sm-4">
                              <!-- text input -->
                                <div class="form-group">
                                   <label>Estado</label>
                               <dd>{{$nodoopsut->estados->nombre}}</dd>
                                </div>
                              </div>
                           
                               <div style="display: none" class="col-sm-4">
                                <!-- text input -->
                                  <div class="form-group">
                                    <label>Latitud Grados UTM</label>
                                     <dd>{{$nodoopsut->latitud ?? 'sin valores'}}º</dd>
                               
                                </div>
                              </div>
                               
                            <div style="display: none" class="col-sm-4">
                                  <!-- text input -->
                              <div class="form-group">
                                <label>Longitud Grados UTM</label>
                               <dd>{{$nodoopsut->longitud ?? 'sin valores'}}º</dd>
                              </div>
                            </div>
                          </div>
  
                          <div class="row">
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Status del Nodo</label>
                                  <select name="status" class="form-control" value="">
                                    <option @if($nodoopsut->status == 'Optimo' ) selected @endif value="Optimo" >Optimo</option>
                                    <option @if($nodoopsut->status == 'APAGADO' ) selected @endif value="APAGADO" >APAGADO</option>
                                    <option @if($nodoopsut->status == 'BYPASEADO' ) selected @endif value="BYPASEADO" >BYPASEADO</option>
                                    <option @if($nodoopsut->status == 'DESINCORPORADO') selected @endif value="DESINCORPORADO" >DESINCORPORADO</option>
                                    <option @if($nodoopsut->status == 'ILUMINADO EN GESTOR' ) selected @endif value="ILUMINADO EN GESTOR" >ILUMINADO EN GESTOR</option>
                                    <option @if($nodoopsut->status == 'VANDALIZADO' ) selected @endif value="VANDALIZADO" >VANDALIZADO</option>
                                  </select>
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Tipo</label>
                                  <select  name="tipo_opsut" id="estado_id" class="form-control">
                                    <option @if($nodoopsut->tipo_opsut == "ADECUADO EN CENTRAL") selected @endif value="ADECUADO EN CENTRAL">ADECUADO EN CENTRAL</option>
                                    <option @if($nodoopsut->tipo_opsut == "CASETA") selected @endif value="CASETA">CASETA</option>
                                  </select>
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Interferencia con anillos:</label><br>
                                <input type="checkbox" style="margin-left: 1%" name="anillos[]" value="1" id="1">1
                                <input type="checkbox" style="margin-left: 1%" name="anillos[]" value="2"   id="2">2
                                <input type="checkbox" style="margin-left: 1%" name="anillos[]" value="3"   id="3">3
                                <input type="checkbox" style="margin-left: 1%" name="anillos[]" value="4"   id="4">4
                                <input type="checkbox" style="margin-left: 1%" name="anillos[]" value="5"   id="5">5
                                <input type="checkbox" style="margin-left: 1%" name="anillos[]"  value="6"  id="6">6
                                <input type="checkbox" style="margin-left: 1%" name="anillos[]" value="7"   id="7">7
                                <input type="checkbox" style="margin-left: 1%" name="anillos[]" value="8"   id="8">8
                                <input type="checkbox" style="margin-left: 1%" name="anillos[]" value="9"   id="9">9
                              </div>
                            </div>
                            <div style="display: none;" class="col-sm-4">
                              <div class="form-group">
                                <label>Region</label>
                               <dd>{{$nodoopsut->regiones->nombre ?? 'no valores'}}</dd>
                              </div>
                            </div>
                          </div>
                        <div class="row">
                          <div class="col-md-6 text-center">
                            <div class="card card-success">
                              <div class="card-header">
                                <h3 class="card-title">MOTOGENERADOR</h3>

                                <div class="card-tools">
                                  <a href="/nodos_mg/{{encrypt($nodoopsut->id)}}" style="color: orange;" type="button" class="btn btn-tool" ><i class="fas fa-eye"></i>
                                  </a>
                                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                  </button>
                                </div>
                              </div>
                              <div class="card-body">
                                  @php ($amg = 0)
                                    @php ($bmg = 0)
                                    @php ($cmg = 0)
                                      @foreach($motor as $inv)
                                        @php ($amg++)                 
                                        @if($inv->motor->operatividad=='Optimo')
                                          @php ($bmg++)                 
                                        @endif
                                      @endforeach
                                      @if($bmg>0)
                                        @if($amg>0)
                                          @php ( $cmg=round(($bmg*100)/$amg)) 
                                        @endif
                                      @endif
                                <dd> <input type="text" class="knob" data-readonly="true" value="{{$cmg}}" data-min="0" data-max="100" data-width="90" data-height="90" data-angleArc="250" data-angleOffset="-125"
                                  @if($amg == 0)data-fgColor="#FFFFFF" data-bgColor="#FFFFFF" @elseif($cmg < 25 ) data-fgColor="#BC3C3C" data-bgColor="#BC3C3C" @elseif($cmg < 50) data-fgColor="#F57A0A" data-bgColor="#BC3C3C" @elseif($cmg < 75) data-fgColor="#E7D20D" data-bgColor="#BC3C3C" @else data-fgColor="#0AF52C" data-bgColor="#BC3C3C" @endif> </dd>
                                  <dd class="text-black">ITEMS INSTALADOS = {{$amg}}</dd>
                                  <dd class="text-black">ITEMS Optimos = {{$bmg}}</dd>
                                  <dd class="text-black">OPERATIBILIDAD = {{$cmg}} %</dd>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6 text-center" >
                            <div class="card card-success">
                              <div class="card-header">
                                <h3 class="card-title">CUADRO DE FUERZA</h3>
                                <div class="card-tools">
                                  <a href="/nodos_cuadroF/{{encrypt($nodoopsut->id)}}" style="color: orange;" type="button" class="btn btn-tool" ><i class="fas fa-eye"></i>
                                  </a>
                                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                  </button>
                                </div>
                              </div>
                              <div class="card-body">
                                @php ($acf = 0)
                                @php ($bcf = 0)
                                @php ($ccf = 0)
                                @foreach($rectificadores as $inv)
                                  @php ($acf++)                 
                                  @if($inv->operatividad=='Optimo')
                                    @php ($bcf++)                 
                                  @endif
                                @endforeach
                                  @if($bcf>0)
                                    @if($acf>0)
                                      @php (   $ccf= round(($bcf*100)/$acf))
                                    @endif
                                  @endif
                                  <dd> <input type="text" class="knob" data-readonly="true" value="{{$ccf}}" data-min="0" data-max="100" data-width="90" data-height="90" data-angleArc="250" data-angleOffset="-125"
                                  @if ($acf == 0)data-fgColor="#FFFFFF" data-bgColor="#FFFFFF" @elseif($ccf < 25 ) data-fgColor="#BC3C3C" data-bgColor="#BC3C3C" @elseif($ccf < 50) data-fgColor="#F57A0A" data-bgColor="#BC3C3C" @elseif($ccf < 75) data-fgColor="#E7D20D" data-bgColor="#BC3C3C" @else data-fgColor="#0AF52C" data-bgColor="#BC3C3C"@endif> </dd>                                     <dd class="text-black">ITEMS INSTALADOS = {{$acf}}</dd>
                                  <dd class="text-black">ITEMS Optimos = {{$bcf}}</dd>
                                  <dd class="text-black">OPERATIBILIDAD = {{$ccf}} %</dd>
                              </div>
                            </div>
                          </div>
                       </div>
<!-- SEXTA fila -->
      <div class="row">
        <div class="col-md-6 text-center">
          <div class="card card-success">
            <div class="card-header">
              <h3 class="card-title">CLIMATIZACION</h3>
                <div class="card-tools">
                  <a href="/nodos_aa/{{encrypt($nodoopsut->id)}}" style="color: orange;" type="button" class="btn btn-tool" ><i class="fas fa-eye"></i>
                  </a>
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">
             
              @php ($acl = 0)
              @php ($bcl = 0)
              @php ($ccl = 0)
              @foreach($aa as $inv)
                @php ($acl++)                 
                @if($inv->operatividad=='Optimo')
                  @php ($bcl++)                 
                @endif
              @endforeach
              @if($bcl>0)
                @if($acl>0)
                  @php (   $ccl= round(($bcl*100)/$acl))
                @endif
              @endif
               
              <dd><input type="text" class="knob" data-readonly="true" value="{{$ccl}}" data-min="0" data-max="100" data-width="90" data-height="90" data-angleArc="250" data-angleOffset="-125" @if ($acl == 0)data-fgColor="#FFFFFF" data-bgColor="#FFFFFF" @elseif($ccl < 25 ) data-fgColor="#BC3C3C" data-bgColor="#BC3C3C" @elseif($ccl < 50) data-fgColor="#F57A0A" data-bgColor="#BC3C3C" @elseif($ccl < 75) data-fgColor="#E7D20D" data-bgColor="#BC3C3C" @else data-fgColor="#0AF52C" data-bgColor="#BC3C3C" @endif></dd>
              <dd class="text-black">ITEMS INSTALADOS = {{$acl}}</dd>
              <dd class="text-black">ITEMS Optimos = {{$bcl}}</dd>
              <dd class="text-black">OPERATIBILIDAD = {{$ccl}} %</dd>
            </div>
          </div>
        </div>
          


            <div class="col-md-6 text-center">
            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">BANCO DE BATERIAS</h3>

                <div class="card-tools">
                <a href="/nodos_bcobb/{{encrypt($nodoopsut->id)}}" style="color: orange;" type="button" class="btn btn-tool" ><i class="fas fa-eye"></i>
                  </a>
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                </div>
                <!-- /.card-tools -->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                    @php ($abb = 0)
                      @php ($bbb = 0)
                       @php ($cbb = 0)
                       @foreach($bancob as $inv)
                                       @php ($abb++)                 
                                      
                               @if($inv->operatividad=="Optimo")
                               @php ($bbb++)                 
                                      @endif
                                     
                           @endforeach


                           @if($bbb>0)
                            @if($abb>0)

                             @php (   $cbb= round(($bbb*100)/$abb))
                           
                              @endif
                           @endif

                       
                  <dd><input type="text" class="knob" data-readonly="true" value="{{$cbb}}" data-min="0" data-max="100" data-width="90" data-height="90" data-angleArc="250" data-angleOffset="-125"
                         @if ($abb == 0)data-fgColor="#FFFFFF" data-bgColor="#FFFFFF" @elseif($cbb < 25 ) data-fgColor="#BC3C3C" data-bgColor="#BC3C3C" @elseif($cbb < 50) data-fgColor="#F57A0A" data-bgColor="#BC3C3C" @elseif($cbb < 75) data-fgColor="#E7D20D" data-bgColor="#BC3C3C" @else data-fgColor="#0AF52C" data-bgColor="#BC3C3C" @endif></dd>
                   
                     <dd class="text-black">ITEMS INSTALADOS = {{$abb}}</dd>
                    <dd class="text-black">ITEMS Optimos = {{$bbb}}</dd>
                    <dd class="text-black">OPERATIBILIDAD = {{$cbb}} %</dd>
              
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>


                       

         </div>

         <div class="card-footer" align="right">
           <button type="submit" class="btn btn-primary">Actualizar</button>
         </div>
                 
    </form>
                  </div>
                </div>
            </div>
            </div>
        </div>
    </section>
</div>





@endsection

<!-- detalleopsutpivotaire -->
@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#requerimiento") !!}
@endif



<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection