@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Nodo Opsut  
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Registro</a></li>
              <li class="breadcrumb-item active"> Nodo Opsut  </li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
     
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
              
              <div class="card-body">
                 <form method="POST" id="formulario_usuarios" enctype="multipart/form-data" name="formulario_usuarios"  action="/nodos/{{encrypt($localidad->id)}}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">
                      <div class="col-md-12">
                    <!-- general form elements disabled -->
                    <div class="card card-success">
                      <div class="card-header">
                        <h3 class="card-title">Localidad</h3>
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body">
                         <div class="row">
                            <div class="col-sm-4">
                              <!-- text input -->
                                <div class="form-group">
                                  <label>Estado Asociado:</label>
                                    <select  name="estado_id" id="estado_id" class="form-control">
                                      <option>Seleccione una Estado</option>
                                        @foreach($estados as $edo)
                                          <option @if($edo->id == $localidad->estado_id ) selected @endif value="{{$edo->id}}">{{$edo->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <div class="col-sm-4">
                                <div class="form-group">
                                  <label>Nombre o Descripciòn</label>
                                    <input type="text" name="nombre" value="{{$localidad->nombre}}"  class="form-control" placeholder="Nombre de Localidad">
                              </div>
                             </div>

                             <div class="col-sm-4">
                              <!-- text input -->
                                <div class="form-group">
                                  <label>Tipo de Localidad</label>
                                    <input readonly type="text" name="tipo" value="{{$localidad->tipo}}" class="form-control" placeholder="Tipo de Localidad">
                              </div>
                             </div>
                         </div>

                         <div class="row">
                            <div class="col-sm-4">
                              <!-- text input -->
                                <div class="form-group">
                                  <label>Status:</label>
                                    <select  name="status" id="estado_id" class="form-control">
                                      <option @if($localidad->status == "OPERATIVO") selected @endif value="OPERATIVO">OPERATIVO</option>
                                      <option @if($localidad->status == "APAGADO") selected @endif value="APAGADO">APAGADO</option>
                                      <option @if($localidad->status == "BYPASEADO") selected @endif value="BYPASEADO">BYPASEADO</option>
                                      <option @if($localidad->status == "DESINCORPORADO") selected @endif value="DESINCORPORADO">DESINCORPORADO</option>
                                      <option @if($localidad->status == "ILUMINADO EN GESTOR") selected @endif value="ILUMINADO EN GESTOR">ILUMINADO EN GESTOR</option>
                                      <option @if($localidad->status == "VANDALIZADO") selected @endif value="VANDALIZADO">VANDALIZADO</option>
                                      
                                    </select>
                                </div>
                            </div>


                            <div class="col-sm-4">
                                <div class="form-group">
                                  <label>Tipo</label>
                                   <select  name="tipo_opsut" id="estado_id" class="form-control">
                                      <option @if($localidad->status == "ADECUADO EN CENTRAL") selected @endif value="ADECUADO EN CENTRAL">ADECUADO EN CENTRAL</option>
                                      <option @if($localidad->status == "CASETA") selected @endif value="CASETA">CASETA</option>
                                    </select>
                              </div>
                             </div>

                            <div class="col-sm-4">
                              <div class="form-group">
                                      <label>Interferencia con anillos:</label><br>
                                      <input type="checkbox" style="margin-left: 1%" name="anillos[]" value="1" id="1">1
                                      <input type="checkbox" style="margin-left: 1%" name="anillos[]" value="2"   id="2">2
                                      <input type="checkbox" style="margin-left: 1%" name="anillos[]" value="3"   id="3">3
                                      <input type="checkbox" style="margin-left: 1%" name="anillos[]" value="4"   id="4">4
                                      <input type="checkbox" style="margin-left: 1%" name="anillos[]" value="5"   id="5">5
                                      <input type="checkbox" style="margin-left: 1%" name="anillos[]"  value="6"  id="6">6
                                      <input type="checkbox" style="margin-left: 1%" name="anillos[]" value="7"   id="7">7
                                      <input type="checkbox" style="margin-left: 1%" name="anillos[]" value="8"   id="8">8
                                      <input type="checkbox" style="margin-left: 1%" name="anillos[]" value="9"   id="9">9

                                  </div>
                            </div>
                         </div>                      
                 
                    </div>
          
                       <div class="card-footer" align="right">
                        <button type="submit"  class="btn btn-primary">Actualizar</button>
                      </div>
                      </form>
                        </div>
                </div>
            </div>
            </div>
        </div>
    </section>
</div>


@endsection
@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_usuarios") !!}
@endif



<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});


</script>
@endsection





