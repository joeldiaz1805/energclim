@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
             Octavo Proyecto de Servicio Universal de Telecomnicaciones - O.P.S.U.T.
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/opsut">O.P.S.U.T.</a></li>
              <li class="breadcrumb-item active">Informacion General del Nodo </li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content --> 
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
             
              <div class="card-body">
      <form method="POST" id="formulario_nodo_all" name="requerimiento"  action="#" enctype="multipart/form-data">
        <input type="hidden" name="_method" value="PUT">
                        {{ csrf_field() }}
                    
                   
                    <div class="col-md-12">
                    <!-- general form elements disabled -->
                     <div class="card card-primary">
                      <div class="card-header">
                        <h3 class="card-title"> Datos del Nodo O.P.S.U.T.  {{$nodoopsut->codigo}}</h3>
                      </div>
            
               <div class="card-body">

                      <!-- /.primera fila-->
                       



        <div class="row">
                             <div class="col-sm-4">
                              <!-- text input -->
                               <div class="form-group">
                                <label>Nombre</label>
                                <dd>{{$nodoopsut->nombre}}</dd>
                               </div>
                             </div>

                           <div class="col-sm-4">
                            <div class="form-group">
                              <label>Codigo CONATEL</label>
                              <dd>{{$nodoopsut->codigo ?? 'No asignado'}}</dd>
                            </div>
                           </div>

                           <div class="col-sm-4">
                              <!-- text input -->
                             <div class="form-group">
                               <label>Tipo de Nodo</label>
                               <dd>{{$nodoopsut->tipo}} </dd>
                             </div>
                           </div>
                        </div>

          <!-- segunda fila -->
                     
                         <div class="row">
                               <div class="col-sm-4">
                              <!-- text input -->
                                <div class="form-group">
                                  <label>Direcion Fisica del Nodo</label>
                                     <dd>{{$nodoopsut->ubicacion}}</dd>

                               </div>
                              </div>
                           
                               <div style="display: none" class="col-sm-4">
                                <!-- text input -->
                                  <div class="form-group">
                                    <label>Latitud Grados UTM</label>
                                     <dd>{{$nodoopsut->latitud}}º</dd>
                               
                                </div>
                              </div>
                               
                            <div style="display: none" class="col-sm-4">
                                  <!-- text input -->
                              <div class="form-group">
                                <label>Longitud Grados UTM</label>
                               <dd>{{$nodoopsut->longitud}}º</dd>
                              </div>
                            </div>
                         </div>
  

          <!-- tercera fila -->

                          <div class="row">
                            <div class="col-sm-4">
                              <!-- text input -->
                              <div class="form-group">
                                <label>Status del Nodo</label>
                                 

                                     <select disabled="disabled" name="status" class="form-control" value="">
                                            <option @if($nodoopsut->stat_nod == 1 ) selected @endif value="1" >ACTIVO</option1>
                                            <option @if($nodoopsut->stat_nod == 2 ) selected @endif value="2" >BYPASEADO</option>
                                            <option @if($nodoopsut->stat_nod == 3) selected @endif value="3" >DESINCORPORADO</option1>
                                            <option @if($nodoopsut->stat_nod == 4 ) selected @endif value="4" >VANDALIZADO</option>
                                            <option @if($nodoopsut->stat_nod == 5 ) selected @endif value="5" >RETIRADO</option>
                                            <option @if($nodoopsut->stat_nod == 6 ) selected @endif value="6" >SIN ESPECIFICACION</option>
                                            
                                 </select>
                              </div>
                            </div>

                              <div class="col-sm-4">
                              <!-- text input -->
                                <div class="form-group">
                                   <label>Estado</label>
                               <dd>{{$nodoopsut->estados->nombre}}</dd>
                                </div>
                              </div>
                              
                               <div class="col-sm-4">
                              <!-- text input -->
                              <div class="form-group">
                                <label>Region</label>
                               <dd>{{$nodoopsut->regiones->nombre}}</dd>
                              </div>
                            </div>
                          </div>


          <!-- cuarta fila -->
        <div class="row">
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Reaponsable</label>
                                 <dd>{{$nodoopsut->personal->nombre}}  
                                     {{$nodoopsut->personal->apellido}}</dd>
                                <dd>{{$nodoopsut->personal->cod_p00}}</dd>
                                <dd>{{$nodoopsut->personal->num_oficina}}</dd>
                                <dd>{{$nodoopsut->personal->num_personal}}</dd>
                               </div>
                               </div>

                       <div class="col-sm-4">
                        <div class="form-group">
                            <label>Observacion</label>
                             <dd> <textarea disabled readonly style="height: 100px; width: 100%";>{{$nodoopsut->observacion}}</textarea></dd>
                            </div>      
                           </div>

                              <div class="col-sm-4">
                             <div class="form-group">

                                </div>
                            </div>
                    
               </div>


  <!-- quinta fila -->
        <div class="row">

      <div class="col-md-6 text-center">
            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">MOTOGENERADOR</h3>

                <div class="card-tools">
                  <a href="/nodos/add/{{$nodoopsut->id}}" type="button" class="btn btn-tool" ><i class="fas fa-plus"></i>
                  </a>
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                </div>
                <!-- /.card-tools -->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                  @php ($amg = 0)
                      @php ($bmg = 0)
                      @php ($cmg = 0)
                    
                       @foreach($nodoopsut->detalleopsutpivot as $inv)
                        @if($inv->equipo_id == 1)
                                 @php ($amg++)                 
                                      
                               @if($inv->status==1)
                               @php ($bmg++)                 
                                      @endif
                                     @endif
                           @endforeach
                           @if($bmg>0)
                            @if($amg>0)

                               @php ( $cmg=round(($bmg*100)/$amg)) 
                             @endif
                           @endif
                  
                   <dd> <input type="text" class="knob" data-readonly="true" value="{{$cmg}}" data-min="0" data-max="100" data-width="90" data-height="90" data-angleArc="250" data-angleOffset="-125"
                           @if($amg == 0)data-fgColor="#FFFFFF" data-bgColor="#FFFFFF" @elseif($cmg < 25 ) data-fgColor="#BC3C3C" data-bgColor="#BC3C3C" @elseif($cmg < 50) data-fgColor="#F57A0A" data-bgColor="#BC3C3C" @elseif($cmg < 75) data-fgColor="#E7D20D" data-bgColor="#BC3C3C" @else data-fgColor="#0AF52C" data-bgColor="#BC3C3C" @endif> </dd>
                    <dd class="text-black">ITEMS INSTALADOS = {{$amg}}</dd>
                    <dd class="text-black">ITEMS OPERATIVOS = {{$bmg}}</dd>
                    <dd class="text-black">OPERATIBILIDAD = {{$cmg}} %</dd>
              
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>


      <div class="col-md-4 text-center" style="display: none">
            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">CUADRO DE FUERZA</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                </div>
                <!-- /.card-tools -->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                   @php ($acf = 0)
                      @php ($bcf = 0)
                      @php ($ccf = 0)
                      @foreach($nodoopsut->detalleopsutpivot as $inv)
                      @if($inv->equipo_id==2)
                        @php ($acf++)                 
                          @if($inv->status==1)
                          @php ($bcf++)                 
                            @endif
                            @endif
                          @endforeach
                        @if($bcf>0)
                          @if($acf>0)
                            @php (   $ccf= round(($bcf*100)/$acf))
                            @endif
                          @endif

                    <dd> <input type="text" class="knob" data-readonly="true" value="{{$ccf}}" data-min="0" data-max="100" data-width="90" data-height="90" data-angleArc="250" data-angleOffset="-125"
                           @if ($acf == 0)data-fgColor="#FFFFFF" data-bgColor="#FFFFFF" @elseif($ccf < 25 ) data-fgColor="#BC3C3C" data-bgColor="#BC3C3C" @elseif($ccf < 50) data-fgColor="#F57A0A" data-bgColor="#BC3C3C" @elseif($ccf < 75) data-fgColor="#E7D20D" data-bgColor="#BC3C3C" @else data-fgColor="#0AF52C" data-bgColor="#BC3C3C"@endif> </dd>
                   
                     <dd class="text-black">ITEMS INSTALADOS = {{$acf}}</dd>
                    <dd class="text-black">ITEMS OPERATIVOS = {{$bcf}}</dd>
                    <dd class="text-black">OPERATIBILIDAD = {{$ccf}} %</dd>
              
              </div>
            </div>
          </div>

        <div class="col-md-6 text-center">
                    <div class="card card-success">
                      <div class="card-header">
                        <h3 class="card-title">CLIMATIZACION</h3>

                        <div class="card-tools">
                          <a href="/nodos/add/{{$nodoopsut->id}}" type="button" class="btn btn-tool" ><i class="fas fa-plus"></i>
                           </a>
                          <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                          </button>
                        </div>
                        <!-- /.card-tools -->
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body">
                            @php ($acl = 0)
                              @php ($bcl = 0)
                               @php ($ccl = 0)
                               @foreach($nodoopsut->detalleopsutpivot as $inv)
                                @if($inv->equipo_id == 3)
                                         
                                               @php ($acl++)                 
                                              
                                       @if($inv->status==1)
                                       @php ($bcl++)                 
                                              @endif
                                              @endif
                                   @endforeach

                                   @if($bcl>0)
                                    @if($acl>0)

                                     @php (   $ccl= round(($bcl*100)/$acl))
                                   
                                      @endif
                                   @endif


                            <dd><input type="text" class="knob" data-readonly="true" value="{{$ccl}}" data-min="0" data-max="100" data-width="90" data-height="90" data-angleArc="250" data-angleOffset="-125"
                                   @if ($acl == 0)data-fgColor="#FFFFFF" data-bgColor="#FFFFFF" @elseif($ccl < 25 ) data-fgColor="#BC3C3C" data-bgColor="#BC3C3C" @elseif($ccl < 50) data-fgColor="#F57A0A" data-bgColor="#BC3C3C" @elseif($ccl < 75) data-fgColor="#E7D20D" data-bgColor="#BC3C3C" @else data-fgColor="#0AF52C" data-bgColor="#BC3C3C" @endif></dd>
                           
                             <dd class="text-black">ITEMS INSTALADOS = {{$acl}}</dd>
                            <dd class="text-black">ITEMS OPERATIVOS = {{$bcl}}</dd>
                            <dd class="text-black">OPERATIBILIDAD = {{$ccl}} %</dd>
                      
                      </div>
                      <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                  </div>
                 
                   </div>
<!-- SEXTA fila -->
    <div class="row">
          <div class="col-md-4 text-center" style="display: none">
            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">CORRIENTE ALTERNA</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                </div>
                <!-- /.card-tools -->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                    @php ($aca = 0)
                      @php ($bca = 0)
                       @php ($cca = 0)
                      @foreach($nodoopsut->detalleopsutpivot as $inv)
                        @if($inv->equipo_id==8)
                                 
                                       @php ($aca++)                 
                                      
                               @if($inv->status==1)
                               @php ($bca++)                 
                                      @endif
                                      @endif
                           @endforeach
                        @if($bca>0)
                            @if($aca>0)

                             @php (   $cca= round(($bca*100)/$aca))
                           
                              @endif
                           @endif

                    <dd><input type="text" class="knob" data-readonly="true" value="{{$cca}}" data-min="0" data-max="100" data-width="90" data-height="90" data-angleArc="250" data-angleOffset="-125"
                             @if ($aca == 0)data-fgColor="#FFFFFF" data-bgColor="#FFFFFF" @elseif($cca < 25 ) data-fgColor="#BC3C3C" data-bgColor="#BC3C3C" @elseif($cca < 50) data-fgColor="#F57A0A" data-bgColor="#BC3C3C" @elseif($cca < 75) data-fgColor="#E7D20D" data-bgColor="#BC3C3C" @else data-fgColor="#0AF52C" data-bgColor="#BC3C3C" @endif></dd>
                   
                     <dd class="text-black">ITEMS INSTALADOS = {{$aca}}</dd>
                    <dd class="text-black">ITEMS OPERATIVOS = {{$bca}}</dd>
                    <dd class="text-black">OPERATIBILIDAD = {{$cca}} %</dd>
              
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>


            <div class="col-md-6 text-center">
            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">BANCO DE BATERIAS</h3>

                <div class="card-tools">
                  <a href="/nodos/add/{{$nodoopsut->id}}" type="button" class="btn btn-tool" ><i class="fas fa-plus"></i>
                  </a>
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                </div>
                <!-- /.card-tools -->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                    @php ($abb = 0)
                      @php ($bbb = 0)
                       @php ($cbb = 0)
                       @foreach($nodoopsut->detalleopsutpivot as $inv)
                        @if($inv->equipo_id == 9)
                                 
                                       @php ($abb++)                 
                                      
                               @if($inv->status==1)
                               @php ($bbb++)                 
                                      @endif
                                      @endif
                           @endforeach


                           @if($bbb>0)
                            @if($abb>0)

                             @php (   $cbb= round(($bbb*100)/$abb))
                           
                              @endif
                           @endif

                       
                  <dd><input type="text" class="knob" data-readonly="true" value="{{$cbb}}" data-min="0" data-max="100" data-width="90" data-height="90" data-angleArc="250" data-angleOffset="-125"
                         @if ($abb == 0)data-fgColor="#FFFFFF" data-bgColor="#FFFFFF" @elseif($cbb < 25 ) data-fgColor="#BC3C3C" data-bgColor="#BC3C3C" @elseif($cbb < 50) data-fgColor="#F57A0A" data-bgColor="#BC3C3C" @elseif($cbb < 75) data-fgColor="#E7D20D" data-bgColor="#BC3C3C" @else data-fgColor="#0AF52C" data-bgColor="#BC3C3C" @endif></dd>
                   
                     <dd class="text-black">ITEMS INSTALADOS = {{$abb}}</dd>
                    <dd class="text-black">ITEMS OPERATIVOS = {{$bbb}}</dd>
                    <dd class="text-black">OPERATIBILIDAD = {{$cbb}} %</dd>
              
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>


               <div class="col-md-6 text-center">
            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">RECTIFICADORES</h3>

                <div class="card-tools">
                  <a href="/nodos/add/{{$nodoopsut->id}}" type="button" class="btn btn-tool" ><i class="fas fa-plus"></i>
                  </a>
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                </div>
                <!-- /.card-tools -->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                  @php ($abms = 0)
                      @php ($bbms = 0)
                      @php ($cbms = 0)
                      @foreach($nodoopsut->detalleopsutpivot as $inv)
                        @if($inv->equipo_id == 2)
                                 
                                       @php ($abms++)                 
                                      
                               @if($inv->status==1)
                               @php ($bbms++)                 
                                      @endif
                                      @endif
                           @endforeach

                        @if($bbms>0)
                            @if($abms>0)

                             @php ( $cbms= round(($bbms*100)/$abms))
                           
                              @endif
                           @endif

                     <dd><input type="text" class="knob" data-readonly="true" value="{{$cbms}}" data-min="0" data-max="100" data-width="90" data-height="90" data-angleArc="250" data-angleOffset="-125"
                         @if($abms==0) data-fgColor="#FFFFFF" data-bgColor="#FFFFFF" @elseif($cbms < 25 ) data-fgColor="#BC3C3C" data-bgColor="#BC3C3C" @elseif($cbms < 50) data-fgColor="#F57A0A" data-bgColor="#BC3C3C" @elseif($cbms < 75) data-fgColor="#E7D20D" data-bgColor="#BC3C3C" @else data-fgColor="#0AF52C" data-bgColor="#BC3C3C" @endif></dd>
                   
                     <dd class="text-black">ITEMS INSTALADOS = {{$abms}}</dd>
                    <dd class="text-black">ITEMS OPERATIVOS = {{$bbms}}</dd>
                    <dd class="text-black">OPERATIBILIDAD = {{$cbms}} %</dd>
              
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>

    </div>
                       
                      
        
                 
                <!-- CIERRA EL CAR CAR SUCES  --> 
         </div>
<!--carga de datos del REQUERIMIENTOS del nodo-->

          <div class="col-md-12">
            <div class="card card-warning collapsed-card">
              <div class="card-header">
                <h3 class="card-title">Requerimientos del Nodo O.P.S.U.T.  " {{$nodoopsut->nombre}} ", Codigo = {{$nodoopsut->codigo}}</h3>

               <div class="card-tools">
                  <a href="/nodos/{{$nodoopsut->id}}/addrequerimiento"  class="btn btn-info text-white" >Nuevo Requerimiento <i class="fas fa-plus"></i>
                  </a>
                </div>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                  </button>
                </div>
                <!-- /.card-tools -->
              </div>
              <!-- /.card-header -->
              
    <div class="card-body">



              <table id="requerimiento1" class="table table-bordered table-striped">
                <thead>

                  <tr align="center">
                       <th>Codigo</th>
                       <th>Equipo</th>
                       <th>Requerimiento</th>
                       <th>Status Requerimiento</th>
                       <th>Observacion</th>
                       <th>ID del nodo</th>
                       <th>Acciones</th>
                   </tr>
                </thead>
                <tbody>
               <!-- aca se empieza a cargar el row principal de requerimientos-->
                 @foreach($requeopsut as $reque)
             
                 <tr align="center"  id="nod_{{$reque->id}}">
                    <td>{{$reque->id}}</td>
                    <td>{{$reque->invequipopsut->nombre}} </td>
                    <td>{{$reque->requerimiento}}</td>
                    <td>@if($reque->stat_req_nod==1)
                       REQUERIDO
                       @elseif($reque->stat_req_nod==2)
                        PROCESADO
                          @elseif($reque->stat_req_nod==3)
                           DESCARTADO
                           @endif



                     </td>
                    <td>{{$reque->observacion}}</td>
                    <td>{{$reque->nodoopsut->codigo}}</td>
                    <td>
                     <a href="/nodos/{{$nodoopsut->id}}/{{$reque->id}}/requerimiento" class="btn btn-warning"><i class="far fa-edit"></i></a>
                      </td>
                  </tr>
                  @endforeach

                
                </tbody>
              </table>
           </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>

<!--fin de carga de datos del requermientos-->
<!--carga de datos de MOTOGENERADOR del nodo-->
<div class="col-md-12">
            <div class="card card-success collapsed-card">
              <div class="card-header">
                 <h3 class="card-title"> Inventario de Equipos de MOTOGENERADOR Asociado al Nodo "{{$nodoopsut->nombre}}"</h3>
                        
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                  </button>
                </div>
                <!-- /.card-tools -->
              </div>
              <!-- /.card-header -->
             <div class="card-body">
              <table id="inventarioaire" class="table table-bordered table-striped table-indigo">
                <thead>

                  <tr align="center">
                       <th>Codigo de Inventario</th>
                     <!--  <th>Instalado en el Nodo</th>  1,2,3,8,9,10-->
                       <th>Equipo</th>
                       <th>Capacidad</th>
                       <th>Estado</th>
                       <th>Observacion</th>
                       <th>Acciones</th>
                   </tr>
                </thead>
                <tbody>
               <!-- aca se empieza a cargar el row principal de requerimientos-->
                @foreach($nodoopsut->detalleopsutpivot as $inv)
                  @if($inv->equipo_id==1)

                 <tr align="center"  id="nod_{{$inv->id}}">
                    <td>{{$inv->id}}</td>
                    <td>{{$inv->equipo->descripcion}} </td>
                    <td>{{$inv->kva}} kva</td>
                    <td>@if($inv->status==1)
                       Operativo
                       @elseif($inv->status==2)
                        Inoperativo
                          @elseif($inv->status==3)
                           Dañado
                        @elseif($inv->status==4)
                           Hurtado
                        @elseif($inv->status==5)
                          Vandalizado total
                           
                        @elseif($inv->status==6)
                          Vandalizado, desincorporado
                          
                        @elseif($inv->status==7)
                         Vandalizado sin plan de recuperaciòn
                           
                        @elseif($inv->status==8)
                          Vandalizado totalmente
                        @elseif($inv->status==8)
                           Bypaseado
                           @endif

                     </td>
                    <td>{{$inv->observacion}}</td>
                    
                    <td>
                     <a href="/opsut/{{$inv->id}}/edit" class="btn btn-warning"><i class="far fa-edit"></i></a>
                      <a href="/opsut/{{$inv->id}}" name="vista de requerimientos" class="btn btn-success"><i class="far fa-eye"></i></a>
                      <a id="intt" @if($inv->stat_req_nod==1) class="btn btn-success" onclick="deleteItem('{{$inv->id}}')" @else class="btn btn-danger" onclick="activeItem('{{$inv->id}}')" @endif class="btn btn-danger"><i id="ele"  class="fa fa-user-times"></i></a>
                      </td>
                  </tr>
                


                  @endif
                  @endforeach

                </tbody>
              </table>
            </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>



       <!--fin de carga de datos de detalles equipos del nodo-->
       <!--  carga de cuadro de fuerza datos -->

          <div class="col-md-12">
            <div class="card card-success collapsed-card">
              <div class="card-header">
                 <h3 class="card-title"> Inventario de Equipos de CUADRO DE FUERZA Asociado al Nodo "{{$nodoopsut->nombre}}"</h3>
                        
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                  </button>
                </div>
                <!-- /.card-tools -->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              <table id="inventariocuadrofuerza" class="table table-bordered table-striped">
                <thead>

                  <tr align="center">
                       <th>Codigo de Inventario</th>
                     <!--  <th>Instalado en el Nodo</th>  1,2,3,8,9,10-->
                       <th>Nombre del Equipo</th>
                       <th>Capacidad</th>
                       <th>Estado</th>
                       <th>Observacion</th>
                       <th>Acciones</th>
                   </tr>
                </thead>
                <tbody>
              
                @foreach($nodoopsut->detalleopsutpivot as $inv)
                  @if($inv->equipo_id==2)

                 <tr align="center"  id="nod_{{$inv->id}}">
                    <td>{{$inv->id}}</td>
                    <td>{{$inv->equipo->descripcion}} </td>
                    <td>{{$inv->watts}} Watts</td>
                    <td>@if($inv->status==1)
                       Operativo
                       @elseif($inv->status==2)
                        Inoperativo
                          @elseif($inv->status==3)
                           Dañado
                        @elseif($inv->status==4)
                           Hurtado
                        @elseif($inv->status==5)
                          Vandalizado total
                           
                        @elseif($inv->status==6)
                          Vandalizado, desincorporado
                          
                        @elseif($inv->status==7)
                         Vandalizado sin plan de recuperaciòn
                           
                        @elseif($inv->status==8)
                          Vandalizado totalmente
                        @elseif($inv->status==8)
                           Bypaseado
                           @endif

                     </td>
                    <td>{{$inv->observacion}}</td>
                    
                    <td>
                     <a href="/opsut/{{$inv->id}}/edit" class="btn btn-warning"><i class="far fa-edit"></i></a>
                      <a href="/opsut/{{$inv->id}}" name="vista de requerimientos" class="btn btn-success"><i class="far fa-eye"></i></a>
                      <a id="intt" @if($inv->stat_req_nod==1) class="btn btn-success" onclick="deleteItem('{{$inv->id}}')" @else class="btn btn-danger" onclick="activeItem('{{$inv->id}}')" @endif class="btn btn-danger"><i id="ele"  class="fa fa-user-times"></i></a>
                      </td>
                  </tr>
                


                  @endif
                  @endforeach

                </tbody>
              </table>

            </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>


        <!--    fin carga de datos-- >

 <!--  carga de CLIMATIZACION datos -->
      <div class="col-md-12">
            <div class="card card-success collapsed-card">
              <div class="card-header">
               <h3 class="card-title"> Inventario de Equipos de CLIMATIZACION Asociado al Nodo "{{$nodoopsut->nombre}}"</h3>
                       
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                  </button>
                </div>
                <!-- /.card-tools -->
              </div>
              <!-- /.card-header -->
                   <div class="card-body">
              <table id="inventariocuadrofuerza" class="table table-bordered table-striped">
                <thead>

                  <tr align="center">
                       <th>Codigo de Inventario</th>
                       <th>Nombre del Equipo</th>
                       <th>Capacidad</th>
                       <th>Estado</th>
                       <th>Observacion</th>
                       <th>Acciones</th>
                   </tr>
                </thead>
                <tbody>
              
                @foreach($nodoopsut->detalleopsutpivot as $inv)
                  @if($inv->equipo_id == 3)

                 <tr align="center"  id="nod_{{$inv->id}}">
                    <td>{{$inv->id}}</td>
                    <!--<td>{{$inv->nodoopsut->codigo}} </td>-->
                   

                    <td>{{$inv->equipo->descripcion}}</td>

                    <td>{{$inv->tr}} tr</td>
                    <td>@if($inv->status==1)
                       Operativo
                       @elseif($inv->status==2)
                        Inoperativo
                          @elseif($inv->status==3)
                           Dañado
                        @elseif($inv->status==4)
                           Hurtado
                        @elseif($inv->status==5)
                          Vandalizado total
                           
                        @elseif($inv->status==6)
                          Vandalizado, desincorporado
                          
                        @elseif($inv->status==7)
                         Vandalizado sin plan de recuperaciòn
                           
                        @elseif($inv->status==8)
                          Vandalizado totalmente
                        @elseif($inv->status==8)
                           Bypaseado
                           @endif

                     </td>
                    <td>{{$inv->observacion}}</td>
                    
                    <td>
                     <a href="/opsut/{{$inv->id}}/edit" class="btn btn-warning"><i class="far fa-edit"></i></a>
                      <a href="/opsut/{{$inv->id}}" name="vista de requerimientos" class="btn btn-success"><i class="far fa-eye"></i></a>
                      <a id="intt" @if($inv->stat_req_nod==1) class="btn btn-success" onclick="deleteItem('{{$inv->id}}')" @else class="btn btn-danger" onclick="activeItem('{{$inv->id}}')" @endif class="btn btn-danger"><i id="ele"  class="fa fa-user-times"></i></a>
                      </td>
                  </tr>
                


                  @endif
                  @endforeach

                </tbody>
              </table>

             </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <div class="col-md-12">
            <div class="card card-success collapsed-card">
              <div class="card-header">
               <h3 class="card-title"> Inventario de Equipos de BANCO DE BATERIAS Asociado al Nodo "{{$nodoopsut->nombre}}"</h3>
                       
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                  </button>
                </div>
                <!-- /.card-tools -->
              </div>
              <!-- /.card-header -->
                   <div class="card-body">
              <table id="inventariocuadrofuerza" class="table table-bordered table-striped">
                <thead>

                  <tr align="center">
                       <th>Codigo de Inventario</th>
                       <th>Nombre del Equipo</th>
                       <th>Capacidad</th>
                       <th>Estado</th>
                       <th>Observacion</th>
                       <th>Acciones</th>
                   </tr>
                </thead>
                <tbody>
              
                @foreach($nodoopsut->detalleopsutpivot as $inv)
                  @if($inv->equipo_id == 9)

                 <tr align="center"  id="nod_{{$inv->id}}">
                    <td>{{$inv->id}}</td>
                    <!--<td>{{$inv->nodoopsut->codigo}} </td>-->
                   

                    <td>{{$inv->equipo->descripcion}}</td>

                    <td>{{$inv->amph}} amph</td>
                    <td>@if($inv->status==1)
                       Operativo
                       @elseif($inv->status==2)
                        Inoperativo
                          @elseif($inv->status==3)
                           Dañado
                        @elseif($inv->status==4)
                           Hurtado
                        @elseif($inv->status==5)
                          Vandalizado total
                           
                        @elseif($inv->status==6)
                          Vandalizado, desincorporado
                          
                        @elseif($inv->status==7)
                         Vandalizado sin plan de recuperaciòn
                           
                        @elseif($inv->status==8)
                          Vandalizado totalmente
                        @elseif($inv->status==8)
                           Bypaseado
                           @endif

                     </td>
                    <td>{{$inv->observacion}}</td>
                    
                    <td>
                     <a href="/opsut/{{$inv->id}}/edit" class="btn btn-warning"><i class="far fa-edit"></i></a>
                      <a href="/opsut/{{$inv->id}}" name="vista de requerimientos" class="btn btn-success"><i class="far fa-eye"></i></a>
                      <a id="intt" @if($inv->stat_req_nod==1) class="btn btn-success" onclick="deleteItem('{{$inv->id}}')" @else class="btn btn-danger" onclick="activeItem('{{$inv->id}}')" @endif class="btn btn-danger"><i id="ele"  class="fa fa-user-times"></i></a>
                      </td>
                  </tr>
                


                  @endif
                  @endforeach

                </tbody>
              </table>

             </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>

        <!--    fin carga de datos-- >

         
        <!--    fin carga de datos-- >

         <div class="card-footer" align="right">
           <button type="submit" class="btn btn-primary">Actualizar</button>
         </div>
                 
    </form>
                  </div>
                </div>
            </div>
            </div>
        </div>
    </section>
</div>





@endsection

<!-- detalleopsutpivotaire -->
@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#requerimiento") !!}
@endif



<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection