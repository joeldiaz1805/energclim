@extends ('/layouts/index')

@section('content')

<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Motogenerador 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inventario</a></li>
              <li class="breadcrumb-item active">Motogenerador</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Vista Preliminar de Registros</h3>
              <div  align="right">
                          <a onclick="history.back()" class="btn btn-danger"><i class="fa fa-arrow-left"></i></a>
                        
                @if(sizeof($motor) == 0)
                  <a href="/motogenerador/create?url={{Request::segment(2)}}" class="btn btn-success">Nuevo Registro</a> 
                @else
                  <a href="/motogenerador/create?url={{encrypt($motor[0]->motor->ubicacion->central->localidad_id)}}" class="btn btn-success">Nuevo Registro</a>
                @endif
              </div>
            </div>
            <!-- /.card-header -->

            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr align="center">
                    <th class="not-export-col">Acciones</th>
                  
                    <th>Marca Motor</th>
                    <th>Modelo Motor</th>
                 
                    <th>Operatividad</th>
                    <th>Falla/Observacion</th>
          
                    <th>Capacidad (KVA)</th>
                    <th>Fecha Actualización</th>
                    <th>Usuario</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($motor as $equipos)
                  <tr align="center" id="tdId_{{$equipos->id}}">
                    <td>
                      <a href="/motogenerador/{{encrypt($equipos->id)}}/edit?url={{encrypt($equipos->motor->ubicacion->central->localidad_id)}}" class="btn btn-warning"><i class="far fa-edit"></i></a>
                    </td>
                    <td>{{$equipos->motor->marca}}</td>
                    <td>{{$equipos->motor->modelo}}</td>
                    <td>{{$equipos->motor->operatividad}}</td>
                    <td>
                       {{$equipos->motor->observaciones}}
                    </td>
                    <td>{{$equipos->generador->cap_kva}}</td>
                    <td>{{$equipos->motor->updated_at}}</td>
                    <td>{{$equipos->motor->usuario->name ?? 'Sin Cambios'}}</td>
                  </tr>
                    @endforeach
                </tbody>
              </table>
           </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('scriptsalerts')


<script>
   @if(Session::has('exito'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Motogenerador Guardado con éxito'
      })
      })
    });
  @endif
   @if(Session::has('actualizado'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Datos actualizados con exito'
      })
      })
    });
  @endif
  
</script>

  <script>
    function deleteItem(item){
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Desea eliminar este motogenerador del intentario?',
      text: "Estas por quitar este equipo del Inventario!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, quitarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {

      var txt = document.getElementById("totalCar");
      console.log(item)

      if (result.value) {



            $.ajax({
                url: "/motogenerador/"+item,
                type:"DELETE",
                   headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    

                    success:function(result, status, xhr){                    
                        
                        if (status == 'success') {

                          swalWithBootstrapButtons.fire(
                            'Quitado con Exito!',
                            'Se eliminó con exito',
                            'success'
                          )

                          $("#tdId_"+item).hide();
                         // txt.innerHTML = result ;



                        }
                    },
                    error:function(xhr, status, error){
                      

                      Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se eliminó el producto, vuelva a intentarlo mas tarde '+error,
                        footer: '<a href>Seguramente hubo un problema de conexión</a>'
                      })
                      

                    }
                });
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No se ha quitado el producto.',
          'error'
        )
      }
    })
  }
</script>


@endsection
