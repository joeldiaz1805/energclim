@extends ('/layouts/index')

@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Inventario total por Localidad 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">Levantamiento de Planta</li>
              <li class="breadcrumb-item ">Nodos Opsut</li>
              <li class="breadcrumb-item active"><a href="/nodos">Inventario</a></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Resumen de Actualizaciòn de Condiciones de Equipos en NodosOpsut  {{date('Y-d-m')}}</h3>
              <!-- <div align="right">
                  <a href="/tickets/create" class="btn btn-success">Nuevo Ticket</a>
              </div> -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class=" display table table-bordered table-striped nowrap" style="width:100%">
                <thead>
                  <tr align="center">
                    <th>Localidad Opsut</th>
                    <th>MG</th>
                    <th>A/A</th>
                    <th>CF</th>
                    <th>UPS</th>
                    <th>BB</th>
                    
                    
                  </tr>
                </thead>
                <tbody>
                
                  <?php
                  foreach($localidades as $localidad){
                        
                        $motor = App\Models\Models\motogeneradorPivot::whereDate('updated_at',date('Y-m-d'))->whereHas('motor', function ($query) use($localidad) {
                          $query->whereHas('ubicacion',function($query2) use($localidad){
                              $query2->whereHas('central',function($query3) use($localidad){
                                 $query3->whereHas('localidad',function($query4)  use($localidad){
                                  return $query4->where('localidad_id',$localidad->id);

                             });

                                });
                              });
                           })->get();

                         $aa = App\Models\Models\AireA::whereDate('updated_at',date('Y-m-d'))->whereHas('ubicacion',function($query2) use($localidad){
                            $query2->whereHas('central',function($query3) use($localidad){
                                $query3->whereHas('localidad',function($query4)  use($localidad){
                                  return $query4->where('localidad_id',$localidad->id);

                             });
                            });
                      
                         })->get();



                         $ups1 = App\Models\Models\ups_bancob::whereDate('updated_at',date('Y-m-d'))->whereHas('ubicacion',function($query2)  use($localidad){
                            $query2->whereHas('central',function($query3)  use($localidad){
                                $query3->whereHas('localidad',function($query4)  use($localidad){
                                  return $query4->where('localidad_id',$localidad->id);

                             });
                            });
                        })->get();

                         $rectificadores = App\Models\Models\rect_bcobb::whereDate('updated_at',date('Y-m-d'))->whereHas('ubicacion',function($query2) use($localidad){
                            $query2->whereHas('central',function($query3) use($localidad){
                                $query3->whereHas('localidad',function($query4)  use($localidad){
                                  return $query4->where('localidad_id',$localidad->id);

                             });
                            });
                        })->get();

                        $bancob = App\Models\Models\bancob::whereDate('updated_at',date('Y-m-d'))->whereHas('ubicacion',function($query2) use($localidad){
                            $query2->whereHas('central',function($query3) use($localidad){
                                $query3->whereHas('localidad',function($query4)  use($localidad){
                                  return $query4->where('localidad_id',$localidad->id);

                             });
                            });
                        })->get();

                        $sumatoria = $motor->count()+$aa->count()+$rectificadores->count()+$ups1->count()+$bancob->count();
                    
                    ?>
                     @if($sumatoria != 0)
                  <tr align="center">
                    <td><?php echo $localidad->nombre; ?>  </td>
                    <td><?php echo $motor->count(); ?> </td>
                    <td><?php echo $aa->count(); ?> </td>
                    <td><?php echo $rectificadores->count(); ?> </td>
                    <td><?php echo $ups1->count(); ?> </td>
                    <td><?php echo $bancob->count(); ?> </td>

                  </tr>
                 @endif  
                <?php } ?>
                  
                </tbody>
              
              </table>
           </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('scriptsalerts')


<script>
  

   @if(Session::has('exito'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Ups Guardado con exito'
      })
      })
    });
  @endif
   @if(Session::has('actualizado'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Datos actualizados con exito'
      })
      })
    });
  @endif
  
</script>

  <script>
    function deleteItem(item){
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Desea eliminar este ups del intentario?',
      text: "Estas por quitar este equipo del Inventario!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, quitarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {


      if (result.value) {



            $.ajax({
                url: "/ups/"+item,
                type:"DELETE",
                   headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    

                    success:function(result, status, xhr){                    
                        
                        if (status == 'success') {

                          swalWithBootstrapButtons.fire(
                            'Quitado con Exito!',
                            'Se eliminó con exito',
                            'success'
                          )

                          $("#tdId_"+item).hide();
                         // txt.innerHTML = result ;



                        }
                    },
                    error:function(xhr, status, error){
                      

                      Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se eliminó el producto, vuelva a intentarlo mas tarde '+error,
                        footer: '<a href>Seguramente hubo un problema de conexión</a>'
                      })
                      

                    }
                });
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No se ha quitado el producto.',
          'error'
        )
      }
    })
  }
</script>


@endsection