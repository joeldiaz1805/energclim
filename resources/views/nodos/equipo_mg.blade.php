@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Octavo Proyecto de Servicio Universal de Telecomnicaciones - O.P.S.U.T. 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/opsut">O.P.S.U.T.</a></li>
              <li class="breadcrumb-item active">Datos de Nodo - Requerimientos- Nuevo Requerimiento</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content --> 
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
             
              <div class="card-body">
              
                    <div class="col-md-12">
                    <!-- general form elements disabled -->
                     <div class="card card-success">
                      <div class="card-header">
                        <h3 class="card-title">Nodo O.P.S.U.T. Codigo: {{$nodoopsut->codigo ?? 'N/C'}},   ID:  {{$nodoopsut->id}}, Requerimientos - Agregar Nuevo Requerimiento a Este Nodo</h3>
                      </div>
            
               <div class="card-body">

                      <!-- /.primera fila-->
                       



        <div class="row">




                <!-- /.card-body -->

                             <div class="col-sm-4">
                              <!-- text input -->
                               <div class="form-group">
                                <label>Nombre</label>
                                <dd>{{$nodoopsut->nombre}}</dd>
                               </div>
                             </div>

                           <div class="col-sm-4">
                            <div class="form-group">
                              <label>Codigo CONATEL</label>
                              <dd>{{$nodoopsut->codigo ?? 'sin code'}}</dd>
                            </div>
                           </div>

                           <div class="col-sm-4">
                              <!-- text input -->
                             <div class="form-group">
                               <label>Tipo de Nodo</label>
                               <dd>{{$nodoopsut->tipo}} </dd>
                             </div>
                           </div>
                        </div>

          <!-- segunda fila -->
                     
                         <div class="row">
                               <div  style="display: none" class="col-sm-4">
                              <!-- text input -->
                                <div class="form-group">
                                  <label>Direcion Fisica del Nodo</label>
                                     <dd>{{$nodoopsut->ubicacion ?? 'sin direccion'}}</dd>

                               </div>
                              </div>
                           
                               <div style="display: none" class="col-sm-4">
                                <!-- text input -->
                                  <div class="form-group">
                                    <label>Latitud UTM</label>
                                     <dd>{{$nodoopsut->latitud ?? '0.0'}}</dd>
                               
                                </div>
                              </div>
                               
                            <div style="display: none" class="col-sm-4">
                                  <!-- text input -->
                              <div class="form-group">
                                <label>Longitud UTM</label>
                               <dd>{{$nodoopsut->longitud ?? '0.0'}}</dd>
                              </div>
                            </div>
                         </div>

  

          <!-- tercera fila -->

                          <div class="row">
                            <div class="col-sm-4">
                              <!-- text input -->
                              <div class="form-group">
                                <label>Status del Nodo</label>
                                 
                                     <select disabled="disabled" name="status" class="form-control" value="">
                                          <option @if($nodoopsut->status == "OPERATIVO") selected @endif value="OPERATIVO">OPERATIVO</option>
                                          <option @if($nodoopsut->status == "APAGADO") selected @endif value="APAGADO">APAGADO</option>
                                          <option @if($nodoopsut->status == "BYPASEADO") selected @endif value="BYPASEADO">BYPASEADO</option>
                                          <option @if($nodoopsut->status == "DESINCORPORADO") selected @endif value="DESINCORPORADO">DESINCORPORADO</option>
                                          <option @if($nodoopsut->status == "ILUMINADO EN GESTOR") selected @endif value="ILUMINADO EN GESTOR">ILUMINADO EN GESTOR</option>
                                          <option @if($nodoopsut->status == "VANDALIZADO") selected @endif value="VANDALIZADO">VANDALIZADO</option>
                                      
                                 </select>
                              </div>
                            </div>

                              <div class="col-sm-4">
                              <!-- text input -->
                                <div class="form-group">
                                  <label>Estado</label>
                                 <dd>{{$nodoopsut->estados->nombre ?? 'Sin asignar'}}</dd>
                                </div>
                              </div>
                              
                               <div class="col-sm-4">
                              <!-- text input -->
                              <div class="form-group" style="display: none">
                                <label>Region</label>
                               <dd>{{$nodoopsut->regiones->nombre ?? ''}}</dd>
                              </div>
                            </div>
                          </div>


                   
                
         </div>

          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Registrar Nuevo Equipo al Nodo - Datos del Equipo </h3>
              </div>
            
              <form role="form" method="POST" id="formulario_equipos_nodos" name="formulario_tickets"  action="{{ url('/nodos')}}">
                        {{ csrf_field() }}

                        <input type="hidden" name="nodoopsut_id" value="{{$nodoopsut->id}}">
                <div class="card-body">
                   <div class="row">

                 <div class="col-md-4 text-center">
                  <div class="form-group">
                     <label>Equipo</label>
                    <select  class="form-control" name="equipo_id" id="equipo">
                      <option value="">Seleccione un Equipo</option>
                          <option value="1">MOTOGENERADOR</option>
                          <option value="2">RECTIFICADOR</option>
                          <option value="3">AIRE ACONDICIONADO</option>
                          <option value="9">BANCO BATERIA</option>
                    </select>
                   
                  </div>
                  </div>

                  <div class="col-md-4 text-center">
                      <div class="form-group">
                        <label>Status del equipo</label>
                          <select  class="form-control"name="status" id="detalleopsutpivot">
                               <option value="1">OPERATIVO</option>
                               <option value="2">INOPERATIVO</option>
                               <option value="3">DAÑADA</option>
                               <option value="4">HURTADO</option>
                               <option value="5">VANDALIZADO TOTAL</option>
                               <option value="6">VANDALIZADO, DESINCORPORADO</option>
                               <option value="7">VANDALIZADO SIN PLAN DE RECUPERACION</option>
                               <option value="8">VANDALIZADO TOTALMENTE</option>
                               <option value="9">BYPASSEADO</option>
                          </select>
                      </div>
                  </div>

                  <div class="col-md-4 text-center" id="kva" style="display: none;">
                    <div class="form-group">
                      <label>Kva:</label>
                      <input type="text" name="kva" id="kva_input" required disabled class="form-control" placeholder="Introduzca Kva">
                    </div>
                  </div>
                  <div class="col-md-4 text-center" id="tr" style="display: none;">
                    <div class="form-group">
                      <label>Tr:</label>
                      <input type="text" name="tr" required disabled id="tr_input" class="form-control" placeholder="Introduzca Toneladas">
                    </div>
                  </div>
                  <div class="col-md-4 text-center" id="amph" style="display: none;">
                    <div class="form-group">
                      <label>AmpH:</label>
                      <input type="text" name="amph" required disabled id="amph_input" class="form-control" placeholder="Introduzca AmpH">
                    </div>
                  </div>
                  <div class="col-md-4 text-center" id="watts" style="display: none;">
                    <div class="form-group">
                      <label>Watts:</label>
                      <input type="text" name="watts" required disabled id="watts_inputs" class="form-control" placeholder="Introduzca Watts">
                    </div>
                  </div>
             </div>


            <div class="row">
              <div class="col-md-12 text-center">
                <div class="form-group">
                  <label for="comment">Observaciones:</label>
                    <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Observaciones','Información complementaria sobre seguimientos e incidencias a destacar. Solo de la instalación del motogenerador', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a><!--End ShowHelp-->
                    <textarea class="form-control" name="observacion" rows="3" id="comment"></textarea>      
                </div>
              </div>
            </div>
          </div>
           <div class="card-footer" align="right">
             <button type="submit" class="btn btn-primary">Agregar al nodo</button>
           </div>
        </form>
      </div>
    </div>
     <!--CIERRA EL CSFR-->
  </div>
      
                 
    </form>
                  </div>
                </div>
            </div>
          </div>
        </div>
    </section>
</div>





@endsection



@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_requerimiento_actualizar") !!}
@endif


<script type="text/javascript">
 $(document).ready(function(){

$('select[id=region]').change(function () {
      console.log('hola')
    var region_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'estado/' + region_id, function (cidades) {
        $('select[id=estado]').empty();

        if(cidades.length != 0){
            $('select[id=estado]').append('<option value=""> Selecciona un estado</option>');
            $.each(cidades, function (key, value) {
                $('select[id=estado]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });

    });




    $('select[id=equipo]').change(function () {
      console.log('equipo, buscando.... espera.... ya casi....')
    var equipo_id = $(this).val();
    //MG
    if (equipo_id== 1){
      $('#kva_input').prop('disabled', false);
      $('#kva').show();
      //amph
      $('#amph_input').prop('disabled', true);
      $('#amph').hide();
      //tr
      $('#tr_input').prop('disabled', true);
      $('#tr').hide()
      //tr
      $('#watts_inputs').prop('disabled', true);
      $('#watts').hide();
      }
      //BB
    if(equipo_id== 9){
      $('#amph_input').prop('disabled', false);
      $('#amph').show();

      //kva
      $('#kva_input').prop('disabled', true);
      $('#kva').hide();
      //tr
      $('#tr_input').prop('disabled', true);
      $('#tr').hide();
      //watts
      $('#watts_inputs').prop('disabled', true);
      $('#watts').hide();
    }
     //RECt
    if( equipo_id == 2 ){
      $('#watts_inputs').prop('disabled', false);
      $('#watts').show();

      $('#amph_input').prop('disabled', true);
      $('#amph').hide();


      //kva
      $('#kva_input').prop('disabled', true);
      $('#kva').hide();
      //tr
      $('#tr_input').prop('disabled', true);
      $('#tr').hide();

    }



    //AA
    if(equipo_id== 3  ){
      //tr
      $('#tr_input').prop('disabled', false);
      $('#tr').show();
      //amp
      $('#amph_input').prop('disabled', true);
      $('#amph').hide();

      //kva
      $('#kva_input').prop('disabled', true);
      $('#kva').hide();
      //watts
      $('#watts_inputs').prop('disabled', true);
      $('#watts').hide();
    }
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    // $.get(APP_URL+'invequipo/' + equipo_id+'/'+{{$nodoopsut->id}}, function (equipo) {
    //     $('select[id=detalleopsutpivot]').empty();

    //     if(equipo.length != 0){
         
    //         $('select[id=detalleopsutpivot]').append('<option value=""> Selec</option>');
    //         console.log('whops... si hay algo en la consulta...')
    //         $.each(equipo, function (key, value) {

    //             $('select[id=detalleopsutpivot]').append('<option value=' + value.id + '>'+ value.id + '</option>');
            
    //         });
    //       }
    //     else{
    //       console.log('whops... no, no hay...')
    //     }
    //     });
    });
 }); 




$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection