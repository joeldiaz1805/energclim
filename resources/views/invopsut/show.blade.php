@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Octavo Proyecto de Servicio Universal de Telecomnicaciones - O.P.S.U.T. 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/opsut">O.P.S.U.T.</a></li>
              <li class="breadcrumb-item active">Inventario Asociado al Nodo  {{$nodoopsut->codigo}}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content --> 
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
             
              <div class="card-body">
      <form method="POST" id="inventario_asociado_nodo" name="requerimiento"  action="#" enctype="multipart/form-data">
        <input type="hidden" name="_method" value="PUT">
                        {{ csrf_field() }}
                    
                   
                    <div class="col-md-12">
                    <!-- general form elements disabled -->
                     <div class="card card-success">
                      <div class="card-header">
                        <h3 class="card-title">Datos del Nodo O.P.S.U.T.</h3>
                      </div>
            
               <div class="card-body">

                      <!-- /.primera fila-->
                       



        <div class="row">
                             <div class="col-sm-4">
                              <!-- text input -->
                               <div class="form-group">
                                <label>Nombre</label>
                                <dd>{{$nodoopsut->nombre}}</dd>
                               </div>
                             </div>

                           <div class="col-sm-4">
                            <div class="form-group">
                              <label>Codigo CONATEL</label>
                              <dd>{{$nodoopsut->codigo}}</dd>
                            </div>
                           </div>

                           <div class="col-sm-4">
                              <!-- text input -->
                             <div class="form-group">
                               <label>Tipo de Nodo</label>
                               <dd>{{$nodoopsut->tipo}}</dd>
                             </div>
                           </div>
                        </div>

          <!-- segunda fila -->
                     
                         <div class="row">
                               <div class="col-sm-4">
                              <!-- text input -->
                                <div class="form-group">
                                  <label>Direcion Fisica del Nodo</label>
                                     <dd>{{$nodoopsut->ubicacion}}</dd>

                               </div>
                              </div>
                           
                               <div class="col-sm-4">
                                <!-- text input -->
                                  <div class="form-group">
                                    <label>Latitud UTM</label>
                                     <dd>{{$nodoopsut->latitud}}</dd>
                               
                                </div>
                              </div>
                               
                            <div class="col-sm-4">
                                  <!-- text input -->
                              <div class="form-group">
                                <label>Longitud</label>
                               <dd>{{$nodoopsut->longitud}}</dd>
                              </div>
                            </div>
                         </div>
  

          <!-- tercera fila -->

                          <div class="row">
                            <div class="col-sm-4">
                              <!-- text input -->
                              <div class="form-group">
                                <label>Status del Nodo</label>
                                 
                                     <select name="status" class="form-control" value="">
                                            <option @if($nodoopsut->stat_nod== 1 ) selected @endif value="1" >ACTIVO</option1>
                                            <option @if($nodoopsut->stat_nod== 2 ) selected @endif value="2" >BYPASEADO</option>
                                            <option @if($nodoopsut->stat_nod== 3 ) selected @endif value="3" >RETIRADO</option>
                                 </select>
                              </div>
                            </div>

                              <div class="col-sm-4">
                              <!-- text input -->
                                <div class="form-group">
                                   <label>Estado</label>
                               <dd>{{$nodoopsut->estados->nombre}}</dd>
                                </div>
                              </div>
                              
                               <div class="col-sm-4">
                              <!-- text input -->
                              <div class="form-group">
                                <label>Region</label>
                               <dd>{{$nodoopsut->regiones->nombre}}</dd>
                              </div>
                            </div>
                          </div>


          <!-- cuarta fila -->
        <div class="row">
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Reaponsable</label>
                                 <dd>{{$nodoopsut->personal->nombre}} 
                                     {{$nodoopsut->personal->apellido}}</dd>
                                <dd>{{$nodoopsut->personal->cod_p00}}</dd>
                                <dd>{{$nodoopsut->personal->num_oficina}}</dd>
                                <dd>{{$nodoopsut->personal->num_personal}}</dd>
                               </div>
                               </div>

                       <div class="col-sm-4">
                        <div class="form-group">
                            <label>observacion</label>
                             <dd> <textarea style="height: 100px; width: 100%";>{{$nodoopsut->observacion}}</textarea></dd>
                            </div>      
                           </div>

                              <div class="col-sm-4">
                             <div class="form-group">

                                </div>
                            </div>
                    
               </div>
                 
                <!-- CIERRA EL CAR CAR SUCES  --> 
         </div>
<!--carga de datos del requerimiento del nodo-->
<div class="card-body">
              <table id="inventario1" class="table table-bordered table-striped">
                <thead>

                  <tr align="center">


                       <th>Codigo del Nodo</th>
                       <th>Equipo Asociado</th>
                       <th>Tipo</th>
                       <th>Observacion</th>
                       <th>Acciones</th>
             
                   </tr>
                </thead>
                <tbody>
               <!-- aca se empieza a cargar el row principal de requerimientos-->
                 @foreach($detalleopsutpivot as $det)
             

                 <tr align="center"  id="nod_{{$det->id}}">
                    <td>{{$det->nodoopsut->codigo}} </td>
                    <td>{{$det->tipoequipo}}</td>
                    <td>{{$det->equipo_id}}</td>
                    <td>{{$det->observacion}}</td>
                  
                    <td>
                     <a href="/opsut/{{$det->id}}/edit" class="btn btn-warning"><i class="far fa-edit"></i></a>
                      <a href="/opsut/{{$det->id}}" class="btn btn-success"><i class="far fa-eye"></i></a>
                      <a id="intt" @if($det->stat_req_nod==1) class="btn btn-success" onclick="deleteItem('{{$det->id}}')" @else class="btn btn-danger" onclick="activeItem('{{$det->id}}')" @endif class="btn btn-danger"><i id="ele"  class="fa fa-user-times"></i></a>
                      </td>
                  </tr>
                  @endforeach

                
                </tbody>
              </table>
           </div>
<!--fin de carga de datos del requermientos-->
<!--carga de datos de detalles equipos del nodo-->






       <!--fin de carga de datos de detalles equipos del nodo-->
        </div>
      
         <div class="card-footer" align="right">
           <button type="submit" class="btn btn-primary">Actualizar</button>
         </div>
                 
    </form>
                  </div>
                </div>
            </div>
            </div>
        </div>
    </section>
</div>





@endsection


@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#requerimiento") !!}
@endif



<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection