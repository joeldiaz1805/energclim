<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GGEC</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
  
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
<!-- SweetAlert2 -->
  <link rel="stylesheet" href="{{asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
  <!-- Toastr -->
  <link rel="stylesheet" href="{{asset('plugins/toastr/toastr.min.css')}}">

<style type="text/css">
  .help-block{
    color: red;
  }
</style>
 
</head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form role="form" method="POST" action="{{url('/reset_password')}}" id="formulario_login" name="formulario_login" >
               {{ csrf_field() }}
              <h1>Sistema de Control de articulos</h1>
              <div>
                <input type="text" class="form-control" name="email" placeholder="Email" required="" />
              </div>
             
              <div>
                <button class="btn btn-default submit" type="submit">Resetear Contraseña</button>
                
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <!-- <p class="change_link">¿Eres nuevo?
                  <a href="#signup" class="to_register"> Crear una cuenta </a>
                </p> -->

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-shopping-cart"></i> GGEC</h1>
                  <p>©2023 All Rights Reserved.</p>
                </div>
              </div>
            </form>
          </section>
        </div>

      
      </div>
    </div>

     
  </body>
</html>


