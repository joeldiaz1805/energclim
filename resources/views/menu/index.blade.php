@extends ('/layouts/index')

@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
             Menues
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Registro</a></li>
              <li class="breadcrumb-item active">Menues</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Registro de Menues</h3>
              <div align="right">
                  <a href="/menus/create" class="btn btn-success">Agregar un  Menù</a>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class=" display table table-bordered table-striped nowrap" style="width:100%">
                <thead>
                  <tr align="center">
                    <th>Acciòn</th>
                    <th>Nombre Menu</th>
                    <th>Nombre Sub-Menu</th>
                    <th>Roles Visibles</th>
                    <th>Icono</th>
                    <th>Fecha Creado</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($menus as $menu)
                  <tr align="center" id="tdId_{{$menu->id}}" >
                    <td>
                     <a href="/menus/{{encrypt($menu->id)}}/edit" class="btn btn-warning"><i class="far fa-edit"></i></a>
                     <a onclick="deleteItem('{{encrypt($menu->id)}}')" class="btn btn-danger"><i id="ele"  class="fa fa-user-times"></i></a>
                      </td>
                    
                    <td>{{$menu->nombre}} </td>
                    <td>@foreach($menu->submenu as $sub) {{$sub->nombre?? 'sin submenu' }} @endforeach </td>
                    <td>@foreach($menu->roles as $roles) {{$roles->denominacion?? 'sin rol' }} @endforeach </td>
                    <td><i class="fa {{$menu->icon}}"></i></td>
                    <td>{{$menu->updated_at}} </td>
                  
                    
                  </tr>
                  @endforeach
                </tbody>
              </table>
           </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('scriptsalerts')


<script>
 
  
</script>

  <script>
    function deleteItem(item){
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Desea eliminar esta prov ?',
      text: "Estas por quitar esta prov!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, quitarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {


      if (result.value) {



            $.ajax({
                url: "/menus/"+item,
                type:"DELETE",
                   headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    

                    success:function(result, status, xhr){                    
                        
                        if (status == 'success') {

                          swalWithBootstrapButtons.fire(
                            'Quitado con Exito!',
                            'Se eliminó con exito',
                            'success'
                          )

                          $("#tdId_"+item).hide();
                         // txt.innerHTML = result ;



                        }
                    },
                    error:function(xhr, status, error){
                      

                      Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se eliminó la prov, vuelva a intentarlo mas tarde '+error,
                        footer: '<a href>Seguramente hubo un problema de conexión</a>'
                      })
                      

                    }
                });
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No se ha quitado el Estado.',
          'error'
        )
      }
    })
  }
</script>


@endsection