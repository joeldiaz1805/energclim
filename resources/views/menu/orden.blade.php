@extends ('/layouts/index')

@section ('content')
  <link rel="stylesheet" href="{{asset('order_select/chosen.min.css')}}">

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>
            Menu 
          </h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Registro</a></li>
            <li class="breadcrumb-item active">Menu</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12 col-sm-12">
          <div class="card card-primary card-tabs">
            <div class="card-body">
              <form method="POST" id="formulario_usuarios" enctype="multipart/form-data" name="formulario_usuarios"  action="{{ url('/orden_post')}}">
                  {{ csrf_field() }}
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Orden de salida de los Menus</label>
                      <select multiple="" class="form-control"  id="Productos" name="orden[]" multiple data-live-search="true"> >
                        @foreach($menu as $men)
                          <option value="{{$men->id}}">{{$men->nombre}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
                <div class="card-footer" align="right">
                  <button  id="submit" class="btn btn-primary">Guardar</button>
                </div>
              </form>
            </div>
	        </div>
	      </div>
      </div>
    </div>
  </section>
</div>



@endsection
@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_usuarios") !!}
@endif

<script src="{{asset('order_select/chosen.jquery.min.js')}}"></script>



<script type="text/javascript">
  $(document).ready(function () {

    $("#Productos").chosen();
  $("#submit").on("click", function(e) {

  // evitamos que se envíe el formulario
  e.preventDefault();

  // obtenemos los parámetros y los ordenamos
  var serializedForm = $("form").serializeArray();
  var orderedValues = ChosenOrder.getSelectionOrder( $("#Productos") );
  
  console.log("ANTES DE ORDENAR SE ENVIARÍA:");
  console.log(JSON.stringify(serializedForm));
  
  // reordenamos el formulario serializado para que se ajuste al ordenado
  var x = 0;
  var y = 0;
  while (x < serializedForm.length) {
    if (serializedForm[x].name == "Productos[]") {
      serializedForm[x].value = orderedValues[y];
      y++;
    }
    x++;
  }
  
  console.log("DESPUÉS DE ORDENAR SE ENVÍA:");
  console.log(JSON.stringify(serializedForm));

  // enviamos el formulario usando AJAX
  $.ajax({
    url: "procesar.php",
    data: serializedForm
  });
});





  }); 
   


</script>
@endsection





