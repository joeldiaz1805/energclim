@extends ('/layouts/index')

@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Tickets energia -  Sismyc 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Monitoreo</a></li>
              <li class="breadcrumb-item active">Tickets Sismyc</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Registro de acciones</h3>
            </div>
            <div class="card-body">
              <table id="example1" class=" display table table-bordered table-striped nowrap" style="width:100%">
                <thead>
                  <tr align="center">
                    <th >Nro Ticket</th>
                    <th>Originador</th>
                    <th>Descripcion</th>
                    <th>Plataforma</th>
                    <th>Area Resolutoria</th>
                    <th>Responsable</th>
                    <th>Fecha de Creación</th>
                    <th>Fecha de Solicitud</th>
                    <th>Cliente</th>
                    <th>Solicitante</th>
                    <th>Impacto</th>
                    <th>Prioridad</th>
                    <th>Estatus</th>
                    <th>Comite</th>
                    <th>Region</th>
                    <th>Estado</th>
                    <th>Localidad</th>
                   
                    <th>Gerencia Solicitante</th>
                    <th>Supervisor</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($re as $reporte)
                  <tr align="center" >
                    @php $descripcion = explode('_',$reporte->descripcion_reporte); @endphp
                    <td>{{$reporte->numero_ticket}} </td>
                    <td>{{$reporte->cor_responsible->nombre_responsable}} </td>
                    <td>{{$reporte->descripcion_reporte}} </td>
                    <td style="color:red;">Campo no Enviado</td>
                    <td>{{$reporte->resolutive_area->nombre_area ?? 'sin valor'}} </td>
                    <td style="color:red;">Campo no Enviado </td>
                    <?php setlocale(LC_TIME,"es_ES");?>
                    <td>{{strftime("%B",strtotime( $reporte->hora_diagnostico_cor))  }} </td>

                    <td>{{$reporte->hora_diagnostico_cor}} </td>
                    <td>{{$reporte->client->Nombre_cliente}} </td>
                    <td>{{$reporte->ticket_solicitor->Nombre_solicitante}} </td>
                    <td>{{$reporte->cdc_severity->Nombre_severidad}} </td>
                    <td>{{$reporte->cdc_priority->Nombre_prioridad}} </td>
                    <td>{{$reporte->cdc_status->Nombre_estado}} </td>
                    <td style="color:red;">Campo no Enviado </td>
                    <td style="color:red;">Campo no Enviado </td>
                    <td style="color:red;">Campo no Enviado </td>
                    <td style="color:red;">Campo no Enviado </td>

                  

                    <td>{{$reporte->ticket_group->nombre_grupo}} </td>

                    <td style="color:red;">Campo no Enviado </td>
  
                  </tr>
                  @endforeach
                </tbody>
              </table>
           </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('scriptsalerts')


<script>
   @if(Session::has('exito'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Estado Guardado con exito'
      })
      })
    });
  @endif
   @if(Session::has('actualizado'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Datos actualizados con exito'
      })
      })
    });
  @endif
  
</script>

  <script>
    function deleteItem(item){
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Desea eliminar esta Sala ?',
      text: "Estas por quitar esta Sala!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, quitarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {


      if (result.value) {



            $.ajax({
                url: "/acciones/"+item,
                type:"DELETE",
                   headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    

                    success:function(result, status, xhr){                    
                        
                        if (status == 'success') {

                          swalWithBootstrapButtons.fire(
                            'Quitado con Exito!',
                            'Se eliminó con exito',
                            'success'
                          )

                          $("#tdId_"+item).hide();
                         // txt.innerHTML = result ;



                        }
                    },
                    error:function(xhr, status, error){
                      

                      Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se eliminó la Sala, vuelva a intentarlo mas tarde '+error,
                        footer: '<a href>Seguramente hubo un problema de conexión</a>'
                      })
                      

                    }
                });
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No se ha quitado el Estado.',
          'error'
        )
      }
    })
  }
</script>


@endsection