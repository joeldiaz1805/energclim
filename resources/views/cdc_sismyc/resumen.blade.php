@extends ('/layouts/index')

@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Resumen diario
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Resumen</a></li>
              <li class="breadcrumb-item active">Tickets Ex</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
          <div class="col-md-12">
            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fa fa-cogs"></i>
                  Resumen
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <dl>
                  <strong>GGEC/GSC/COSEC VLP</strong>
                  <br><strong>Fecha @php echo (date("d").'/'.date("m").'/'.date('Y')); @endphp</strong>
                  <br><strong>Resumen de Actividades</strong><br><br>
                  @foreach($re as $reporte)
                  @if($reporte->cor_responsible->nombre_responsable=="RAFAEL PAEZ" || $reporte->cor_responsible->nombre_responsable=="AGUSTIN GAMARRA" || $reporte->cor_responsible->nombre_responsable=="LUIS CARRILLO" || $reporte->cor_responsible->nombre_responsable=="ALEXANDER ALVAREZ")
                    @php $value = explode('_',$reporte->descripcion_reporte); @endphp
                      <dd>@if($reporte->ticket_status_id == 1)✅ @else 🛠️ @endif *Caso:@php echo $value[0];@endphp {{$reporte->resolutive_area->nombre_area}} Ticket:{{$reporte->numero_ticket}}*</dd>
                      <dd>{{$reporte->comments[0]->comment}}</dd><br>
                    @endif
                  @endforeach
                  <br><br>
                  <strong>
                  Actividad Realizada por:
                  <br>Danny Hurtado
                  <br>Jose Alvarez
                  <br>Rafael Paez
                  <br>Luis Carrillo 
                  <br>Jose Matos
                  <br>Agustin Gamarra
                  </strong>
                </dl>
              </div>
            </div>
          </div>
        </div>
      </div>
  </section>
</div>
@endsection

<!--✅ reseulto  . 🛠️ en proceso-->