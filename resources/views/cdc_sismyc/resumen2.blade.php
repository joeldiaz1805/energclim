<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Resumen Diario </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
  
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
   <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">

  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="{{asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
  <!-- Toastr -->
  <link rel="stylesheet" href="{{asset('plugins/toastr/toastr.min.css')}}">
    <!-- Select2 -->
  <link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
 
</head>
<body  style="background-image: url('{{ asset('images/fondo.jpg') }}'); no-repeat center center fixed; background-size: 100% 100%;" class="hold-transition login-page " >
    
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
    <div style="margin-top: -150px">
	     <div class="banner">
	        <img src="{{asset('images/banner.png')}}" width="500" height="70">
	        <h2 style="color: black">Bienvenido al Portal Web Interactivo</h2>
	    </div>
    </div>
     
        
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
              
              	<div class="card-body">
	                <dl>
	                  <strong>*GGEC/GSC/COSEC VLP*</strong>
	                  <br><strong>*Fecha @php echo (date("d").'/'.date("m").'/'.date('Y').'*'); @endphp</strong>
	                  <br><strong>*Resumen de Actividades*</strong><br><br>
	                  @foreach($re as $reporte)
	                  	@if($reporte->cor_responsible->nombre_responsable=="JOSE MATOS" || $reporte->cor_responsible->nombre_responsable=="RAFAEL PAEZ" || $reporte->cor_responsible->nombre_responsable=="AGUSTIN GAMARRA" || $reporte->cor_responsible->nombre_responsable=="LUIS CARRILLO" || $reporte->cor_responsible->nombre_responsable=="ALEXANDER ALVAREZ")
	                    	@php $value = explode('_',$reporte->descripcion_reporte); @endphp
	                      	<dd>@if($reporte->ticket_status_id == 1)✅ @else 🛠️ @endif *Caso: @php echo $value[0];@endphp {!! str_replace('EX_','Edo-',$reporte->resolutive_area->nombre_area) !!} Sala: @php echo $value[4];@endphp Ticket: {{$reporte->numero_ticket}}*</dd>
	                      	<dd>{{$reporte->comments[0]->comment}}</dd><br>
	                    @endif
	                  @endforeach
	                  <br><br>
	                  <strong>
	                  Actividad Realizada por:
	                  <br>*Danny Hurtado*
	                  <br>*Jose Alvarez*
	                  <br>*Rafael Paez*
	                  <br>*Luis Carrillo*
	                  <br>*Jose Matos*
	                  <br>*Agustin Gamarra*
	                  </strong>
	                </dl>
              </div>
        </div>
    </div>
	

			    
      <!-- /.container-fluid -->
    
    <!-- /.content -->
  </div>
          
          
            

	
  
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('plugins/select2/js/select2.full.min.js')}}"></script>

<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- SweetAlert2 -->
<script src="{{asset('dist/js/clipboard.min.js')}}"></script>

  <script src="{{asset('plugins/sweetalert2/sweetalert2.min.js')}}"></script>
  <!-- Toastr -->
  <script src="{{asset('plugins/toastr/toastr.min.js')}}"></script>

<script src="{{asset('js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>


</body>
</html>


