
@extends ('/layouts/index')

@section ('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Escoja rango de Fecha a Consultar
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Sismyc</a></li>
              <li class="breadcrumb-item active">Rango de fecha</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
     
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
              
              <div class="card-body">
                 <form method="POST" id="formulario_usuarios" enctype="multipart/form-data" name="formulario_usuarios"  action="{{ url('/graficas')}}">
                    {{ csrf_field() }}
                    <div class="col-md-12">
                    <div class="card card-success">
                      <div class="card-header">
                        <h3 class="card-title">Rango a Consultar</h3>
                        <div align="right" style="display: none">
                            <a href="/tickets_ex/create" class="btn btn-primary">Resumen</a>
                        </div>
                        <div class="card-footer" align="right">
                        <button type="submit"  id="crear"class="btn btn-primary">Consultar</button>
                      </div>
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body">
                        <div class="row">
                         <div class="col-md-6" data-select2-id="30">
                            <div class="form-group" data-select2-id="29">
                              <label>Meses</label>
                              <select class="form-control select2bs4 select2-hidden-accessible" name="meses" style="width: 100%;" data-select2-id="17" tabindex="-1" aria-hidden="true">
                                <option data-select2-id="46" value="julio">Julio 2022 </option>
                                <option data-select2-id="45" value="agosto">Agosto 2022</option>
                                <option data-select2-id="44" value="septiembre">Septiembre 2022</option>
                                <option data-select2-id="43" value="octubre">Octubre 2022</option>
                                <option data-select2-id="42" value="noviembre">Noviembre 2022</option>
                                <option data-select2-id="41" value="diciembre">Diciembre 2022</option>
                                <option data-select2-id="35" value="enero">Enero 2023</option>
                                <option data-select2-id="36" value="febrero">Febrero 2023</option>
                                <option data-select2-id="37" value="marzo">Marzo 2023</option>
                                <option data-select2-id="38" value="abril">Abril 2023</option>
                                <option data-select2-id="39" value="mayo">Mayo 2023</option>
                                <option data-select2-id="40" value="Y-06-01,Y-m-d">Junio 2023</option>
                              </div>
                              </div>
                        </div>
                      </div>
                      </div>
                       
                      </form>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </section>
</div>


@endsection
@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_usuarios") !!}
@endif

<script type="text/javascript">
 
 function calcular(){

    var finicio =moment(document.getElementById('fechaInicio').value);
    var ffin =moment(document.getElementById('fechaFin').value);
    var dias = ffin.diff(finicio,'days');
    console.log(ffin.diff(finicio,'days'),'dias');

  
/*

     if(dias > 31){
        toastr.error('El Rango de fecha supera los 31 dias permitidos');
        $('#crear').prop('disabled',true);
      }else{
        
        $('#crear').prop('disabled',false);
      }

*/


 }




  
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection





@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_usuarios") !!}
@endif

<script type="text/javascript">
 
 function calcular(){

    var finicio =moment(document.getElementById('fechaInicio').value);
    var ffin =moment(document.getElementById('fechaFin').value);
    var dias = ffin.diff(finicio,'days');
    console.log(ffin.diff(finicio,'days'),'dias');

  
/*

     if(dias > 31){
        toastr.error('El Rango de fecha supera los 31 dias permitidos');
        $('#crear').prop('disabled',true);
      }else{
        
        $('#crear').prop('disabled',false);
      }

*/


 }




  
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection




