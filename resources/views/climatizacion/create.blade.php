@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Salas Climatizadas
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inventario</a></li>
              <li class="breadcrumb-item active">Salas</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <div class="row ">
	    <div class="col s12 m12 l12 ">
	      @if ($errors->any())
	        <div class="alerta-errores">
	          <ul>
	            @foreach ($errors->all() as $error)
	              <li>{{ $error }}</li>
	            @endforeach
	          </ul>
	        </div>
	      @endif
	    </div>
	  </div>
    <section class="content">
      	<div class="container-fluid">
	        <div class="row">
    	    	<div class="col-12 col-sm-12">
            		<div class="card card-primary card-tabs">
		              	<div class="card-body">
              	 			<form method="POST" id="formulario_aa" name="formulario_aa"  action="{{ url('/climatizacion')}}">
              	 				{{ csrf_field() }}
              	 				<input type="hidden" name="user_id" value="{{Auth::user()->id}}">
              	 				
			              	 	<div class="col-md-12" @if($parametros != null) style="display: none" @endif>
						            <div class="card card-success">
							            <div class="card-header">
							                <h3 class="card-title">Ubicación y valores</h3>
							            </div>
						              	<div class="card-body">
						                	<div class="card-header">
								                <h3 class="card-title">Ubicación</h3>
								            </div>
								            <br>
						                  	<div class="row">
						                  		<div class="col-sm-4" @if(Auth::user()->region_id != null) style="display:none" @endif >
							                      	<div class="form-group">
							                        	<label>Región:</label>
							                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Región','Seleccione región donde está ubicado el AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        	<select  name="region_id" id="region" class="form-control">
							                        			@if(Auth::user()->region_id != null) 
							                        				<option value="{{Auth::user()->region_id}}">Seleccione una Región</option>
							                        			@endif
								                        		@foreach($regiones as $region)
									                        		<option value="{{$region->id}}">{{$region->nombre}}</option>
									                        	@endforeach
								                    		</select>
							                    	</div>
						                    	</div>
						                      	<div class="col-sm-4">
							                      	<div class="form-group">
								                        <label>Estado:</label>
								                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Estado','Seleccione estado donde está ubicado el AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        <select name="estado_id" id="estado" class="form-control">
									                       	<option value="">Seleccione un estado</option>
								                        	@if(Auth::user()->region_id != null)
								                        	 	@foreach($estados as $edo)
								                        	 		<option value="{{$edo->id}}">{{$edo->nombre}}</option>
								                        	 	@endforeach
								                        	@endif
								                    	</select>
							                      	</div>
						                    	</div>
						                   
						                    	<div class="col-sm-4">
							                      	<div class="form-group">
							                        	<label>Central:</label>
							                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Central','Seleccione central donde está ubicado el AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        	<select name="localidad_id" id="central" class="form-control">
										                       		<option value="">Seleccione una central</option>
									                    	</select>
								                    </div>
						                      	</div>
							                </div>
						                 	<div class="row">
							                 	<div class="col-sm-4">
							                      	<div class="form-group">
							                        	<label>Piso:</label>
							                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Piso','Introduzca piso donde se encuentra el AA. Solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                        	<select name="piso" id="box_ModeloRectificador" class="form-control">
							                        		<option>Seleccione ubicación piso</option>
								                        	
								                        		@foreach($pisos as $piso)
									                        		<option value="{{$piso->nombre}}">{{$piso->nombre}}</option>
									                        	@endforeach
														</select>
								                    </div>
							                    </div>
							                    <div class="col-sm-4">
							                      	<div class="form-group">
							                        	<label>Sala o espacio físico:</label>
							                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Sala','Seleccione sala donde está alojado el AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                        	<select name="sala" id="box_ModeloRectificador" class="form-control">
							                        		<option>Seleccione una sala</option>
							                        			@foreach($salas as $sala)
									                        		<option value="{{$sala->nombre}}">{{$sala->nombre}}</option>
									                        	@endforeach
														</select>
							                      	</div>
							                   	</div>

							                   	<div class="col-sm-4">
							                      	<div class="form-group">
							                        	<label>Grados C° en la Sala:</label>
							                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Sala','Seleccione sala donde está alojado el AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        				<input type="text" class="form-control" value="{{old('grados')}}" required name="grados" placeholder="Ej: 18.5">
							                      	</div>
							                   	</div>
						                 	</div>
						                 	<div class="row">
						                 		<div class="col-sm-4">
						                 			<label>Porcentaje de Sala Climatizada:</label>
						                 			<input name="porcentaje" type="text" class="knob" data-thickness="0.2" 
							                        data-anglearc="250" data-angleoffset="-125" value="1" 
							                        data-width="120" data-height="120" data-fgcolor="#00c0ef" 
							                        style="width: 60px; height: 36px; position: absolute; 
							                         margin-top: 40px; margin-left: -92px; 
							                        border: 0px; background: none; font: bold 24px Arial; 
							                        text-align: center; color: rgb(0, 192, 239); padding: 0px; appearance: none;">
							                        
						                 		</div>
						                 		<div class="col-sm-8">
							                 		<div class="form-group">
														<label for="comment">Observaciones:</label>
														<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Observaciones','Información complementaria sobre seguimientos e incidencias a destacar. Solo de la instalación del AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
														<textarea class="form-control" rows="3" name="observacion" id="comment"></textarea>			
													</div>
							                 	</div>
						                 	</div>
					                    </div>
				                    </div>
			                  	</div>
					         	<div class="card-footer" align="right">
					              	<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
					            </div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>


@endsection
@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_aa") !!}
@endif



<script type="text/javascript">
    var tipo_compresor = document.getElementById('tipo_compresor');
    //var tipo = document.getElementById('tipo');

  $('#UMA').click(function(){
     var valor = $(this).val();
      if (valor==2){
       	$('#tipo_compresor').prop('disabled', true);
       	$('#compresor_row').hide();
        tipo_compresor.value="no aplica"
        //tipo.append('<input id="input_tipo" name="tipo_compresor" value="no aplica" >');

      }else{
        $('#tipo_compresor').prop('disabled', false);
       	$('#compresor_row').show();
        //$('#region').prop('disabled', true);
        tipo_compresor.value=""
      }
  });

  function falla(){
  	var falla = document.getElementById('input_falla')
  	var range = document.getElementById('range')
  	var opera = document.getElementById('operatividad')
  	console.log(falla.value)

  	range.value == falla.value
  	if(falla.value < 1){
  		opera.value == "Inoperativo"
  	}else if(falla.value>99){
  		opera.value == "Operativo"
  	}

  }

 $("#no_posee").click(function(){
	var no_posee_gen = document.getElementById('no_posee');
	var serial_gen = document.getElementById('serial_gen');
	if(no_posee_gen.checked){
        	//$('#serial_gen').hide();
        	serial_gen.value="no posee"
        	$('#serial_gen').prop('readonly', true);
 		}
 	if(!no_posee_gen.checked){
        	//$('#serial_gen').hide();
        	serial_gen.value=""
        	$('#serial_gen').prop('readonly', false);
 		}

 	});

 $('select[id=operatividad]').change(function(){
 	var valor = $(this).val();
 	//console.log(valor)
 	if (valor == "Falla"){
 		$('#falla').show();
		$('#input_falla').prop('disabled',false);
 	}else{
		$('#falla').hide();
		$('#input_falla').prop('disabled',true);

 	}



 });



$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
  $(document).ready(function(){



  	$('select[id=region]').change(function () {
    	console.log('hola')
    var region_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'estado/' + region_id, function (cidades) {
        $('select[id=estado]').empty();

        if(cidades.length != 0){
            $('select[id=estado]').append('<option value=""> Selecciona un estado</option>');
            $.each(cidades, function (key, value) {
                $('select[id=estado]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });

    });


	$('select[id=estado]').change(function () {
    	//console.log('hola')
    var estado_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'central/' + estado_id, function (cidades) {
        $('select[id=central]').empty();

        if(cidades.length != 0){
            $('select[id=central]').append('<option value=""> Selecciona un central</option>');
            $.each(cidades, function (key, value) {
                $('select[id=central]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });

    });


	});
});
</script>
@endsection


