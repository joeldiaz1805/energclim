<style type="text/css">
  .nav_sub_menu{
    margin-left: 15%;
  }
</style>
 <aside class="main-sidebar sidebar-light-primary elevation-4">
    <a href="/home" class="brand-link">
      <img  src="{{asset('/img/Cantv_logo.png')}}" style="height: 60%; width: 45%;margin-left: 25%"  class="brand-image"
           style="opacity: .8">
           <br>
    </a>
    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('/img/Cantv_logo.png')}}" class="brand-image " >
        </div>
        <div class="info">
          <a href="usuarios/{{encrypt(Auth::user()->id)}}/edit" class="d-block">{{Auth::user()->name}} {{Auth::user()->last_name}}</a>

        </div>
      </div>
          
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">


      
          <li class="nav-item">
            <a  href="/home" class="nav-link @if(Request::is('home')) text-primary @endif">

              <i  class="fas fa-home nav-icon" ></i>
              <p>
               

                Inicio
              </p>
            </a>


          </li>
          <li class="nav-item">
            <a href="/actividades" class="nav-link">
              <i class="fas fa-poll-h nav-icon"></i>
              <p>Actividades Diarias</p>
            </a>
          </li>
          @if(Auth::user()->rol_id != 1 || Auth::user()->rol_id != 4 || Auth::user()->rol_id != 5 || Auth::user()->rol_id != 6 || Auth::user()->rol_id != 3) 
           <li class="nav-item" >
            <a href="/consultar_ticket_ex" class="nav-link">
              <i class="fas fa-poll-h nav-icon"></i>
              <p>Reporte tickets Ex</p>
            </a>
          </li>
          @endif

           <li class="nav-item">
            <a href="/con_graficas" class="nav-link">
              <i class="fas fa-poll-h nav-icon"></i>
              <p>Reporte de Fallas</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="/control-cambios-ex" class="nav-link">
              <i class="fas fa-poll-h nav-icon"></i>
              <p>Control de Cambio Ex</p>
            </a>
          </li>
            <li class="nav-item" >
            <a href="/disponibilidad-servicio" class="nav-link text-success">
              <i class="fas fa-poll-h nav-icon " ></i>
              <p>Disponibilidad de servicio</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="/masiva" class="nav-link">
              <i class="fas fa-map"></i>
              <p>Falla Masiva</p>
            </a>
          </li>
           
          
          @if(Auth::user()->rol_id != 6 && Auth::user()->rol_id != 8 && Auth::user()->rol_id != 9)
          <li id="inventario" class="nav-item has-treeview @if(Request::is('motogenerador') || Request::segment(1) == 'motogenerador' || Request::is('aire_acondicionado') || Request::segment(1) == 'aire_acondicionado' || Request::is('rectificador') || Request::segment(1) == 'rectificador' || Request::is('ups') || Request::segment(1) == 'ups' || Request::is('opsut') || Request::segment(1) == 'opsut' || Request::is('nodos') || Request::segment(1) == 'nodos' || Request::is('reqopsut') || Request::segment(1) == 'reqopsut' || Request::segment(1) == 'nodos') menu-open @endif">

            <a href="#" class="nav-link">
              <i  class="fas fa-folder-open nav-icon"></i>
              <p>
                Levantamiento de Planta
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
           
            <ul class="nav nav-treeview">
              
              <li class="nav-item">
                <li class="nav-item">
                  <a href="#" class="nav-link">
                    <i class="fas fa-book nav-icon"></i>
                    <p>MotoGenerador
                      <i class="right fas fa-angle-left"></i>
                    </p>
                  </a>
                  <ul class="nav nav-treeview nav_sub_menu">
                    <li   class="nav-item">
                      <a href="/motogenerador" class="nav-link @if(Request::is('motogenerador')  || Request::segment(1) == 'motogenerador') text-primary @endif">
                        <p>Equipos</p>
                        <i class="fas fa-cogs nav-icon"></i>
                      </a>
                    </li>
                  </ul>
                  <ul class="nav nav-treeview nav_sub_menu">
                    <li class="nav-item">
                      <a href="/nivel_combustible" class="nav-link">
                        <p>Suministros</p>
                        <i class="fas fa-gas-pump"></i>
                      </a>
                    </li>
                   
                  </ul>
                </li>
              </li>


              <li class="nav-item">
                <li class="nav-item">
                  <a href="#" class="nav-link">
                    <i class="fas fa-thermometer-quarter"></i>
                    <p>Climatizacion
                      <i class="right fas fa-angle-left"></i>
                    </p>
                  </a>
                  <ul class="nav nav-treeview nav_sub_menu">
                    <li class="nav-item">
                      <a  href="/aire_acondicionado" class="nav-link @if(Request::is('aire_acondicionado')  || Request::segment(1) == 'aire_acondicionado') text-primary @endif">
                        <p>Equipos</p>
                        <i class="fas fa-cubes nav-icon"></i>
                      </a>
                    </li>
                  </ul>
                  <ul class="nav nav-treeview nav_sub_menu">
                    <li class="nav-item">
                      <a  href="/climatizacion" class="nav-link">
                        <p>Salas</p>
                        <i class="fas fa-thermometer-quarter"></i>
                      </a>
                    </li>
                   
                  </ul>
                </li>
              </li>
              <li class="nav-item">
                <a href="/cuadro_fuerza" class="nav-link @if(Request::is('cuadro_fuerza')  || Request::segment(1) == 'cuadro_fuerza') text-primary @endif">
                  <i class="fas fa-charging-station nav-icon"></i>
                  <p>Cuadro de Fuerza</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/bancob" class="nav-link @if(Request::is('bancob') || Request::segment(1) == 'bancob') text-primary @endif">
                 <i class="fas fa-car-battery"></i>
                  <p>Banco Bateria</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/ups" class="nav-link @if(Request::is('ups') || Request::segment(1) == 'ups') text-primary @endif">
                  <i class="fas fa-battery-full nav-icon"></i>
                  <p>Ups</p>
                </a>
              </li>
            </ul>
          
        <ul class="nav nav-treeview">
              <li  class="nav-item  @if(Request::is('opsut') || Request::is('nodos') || Request::is('reqopsut') || Request::segment(1) == 'nodos') menu-open @endif">
                <a href="#" class="nav-link">
                  <p>Nodos OPSUT
                  <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
              <li style="display: none" class="nav-item">
                <a href="/opsut" class="nav-link  @if(Request::is('opsut')) text-primary @endif">
                  <i class="fas fa-chart-bar nav-icon"></i>
                  <p>Estatus</p>
                </a>
              </li>
              <li  class="nav-item">

                <a href="/nodos" class="nav-link  @if(Request::is('nodos')) text-primary @endif">
                  <i class="fas fa-list-alt nav-icon"></i>
                  <p>Inventario</p>
                </a>
              </li>
               <li  class="nav-item">

                <a href="/resumen_diario" class="nav-link  @if(Request::is('resumen_diario')) text-primary @endif">
                  <i class="fas fa-list-alt nav-icon"></i>
                  <p>Resumen Diario</p>
                </a>
              </li>
              <li  class="nav-item">

                <a href="/equipos_actualizados" class="nav-link  @if(Request::is('equipos_actualizados')) text-primary @endif">
                  <i class="fas fa-list-alt nav-icon"></i>
                  <p>Equipos Actualizados</p>
                </a>
              </li>
              <li  style="display: none" class="nav-item">
                <a href="/equipos_nodos" class="nav-link">
                  <i class="fas fa-wrench nav-icon"></i>
                  <p>Equipos Nodos</p>
                </a>
              </li>
              <li style="display: none" class="nav-item">
                <a href="/reqopsut" class="nav-link @if(Request::is('reqopsut')) text-primary @endif">
                  <i class="fas fa-clipboard-list nav-icon"></i>
                  <p>Requerimientos</p>
                </a>
              </li>
             <li style="display: none" class="nav-item">
                <a href="#" class="nav-link">
                  <i class="fas fa-sync-alt nav-icon"></i>
                  <p>Actualizaciones</p>
                </a>
              </li>
              
            </ul>
          </li>
        </li>
            </ul>
            <ul  class="nav nav-treeview">
              <li style="display: none" class="nav-item">
                <a href="#" class="nav-link">
                  <p>Nodos NGN
                  <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
              </li>
            </ul>
          </li>
          @endif

            @if(Auth::user()->rol_id != 4 && Auth::user()->rol_id != 5 && Auth::user()->rol_id != 6 && Auth::user()->rol_id != 7 && Auth::user()->rol_id != 8 && Auth::user()->rol_id != 9)
          <li class="nav-item has-treeview  @if(Request::is('tickets') || Request::is('tickets/create') || Request::segment(1) == 'tickets') menu-open @endif">

            <a href="#" class="nav-link">
              <i class="fas fa-exclamation-triangle nav-icon"></i>
              <p>
                 Factibilidad
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">

              <li class="nav-item  @if(Request::is('tickets') || Request::is('tickets/create') || Request::segment(2) != 'create') menu-open @endif">

                <a href="/tickets" class="nav-link">
                  <i class="fas fa-angle-left right"></i>
                  <p>Registro de Solicitudes</p>
                </a>
                 <ul class="nav nav-treeview">
                  <li class="nav-item">

                    <a href="/tickets/create" class="nav-link @if(Request::is('tickets/create') ) text-primary @endif">

                      <i class="fas fa-paste"></i>
                      <p>Solicitud de Planificacion</p>
                    </a>
                  </li>
                 
                  <li class="nav-item">

                    <a href="/tickets" class="nav-link @if(Request::is('tickets') || Request::segment(1) == 'tickets' && Request::segment(2) != 'create' ) text-primary @endif">

                      <i class="fas fa-list-alt"></i>
                      <p>Planificaciones</p>
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <li class="nav-item  @if(Request::is('registro_actividades')) menu-open @endif">
            <a href="/registro_actividades" class="nav-link">
              <i class="fas fa-calendar-check nav-icon"></i>
              <p>
                Actividades por Región
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/registro_actividades" class="nav-link @if(Request::is('registro_actividades') || Request::segment(1) == 'registro_actividades') text-primary @endif">
                  <i class="fas fa-wrench"></i>
                  <p>Registro</p>
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="#" class="nav-link @if(Request::is('registro_actividades') || Request::segment(1) == 'registro_actividades') text-primary @endif">
                  <i class="fas fa-wrench"></i>
                  <p>Carga de Reportes</p>
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="#" class="nav-link @if(Request::is('registro_actividades') || Request::segment(1) == 'registro_actividades') text-primary @endif">
                  <i class="fas fa-wrench"></i>
                  <p>Comparación de Data</p>
                </a>
              </li>
            </ul>
             <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/cuadrillas" class="nav-link @if(Request::is('registro_actividades') || Request::segment(1) == 'registro_actividades') text-primary @endif">
                  <i class="fas fa-wrench"></i>
                  <p>Cuadrillas Ex</p>
                </a>
              </li>
            </ul>
          </li>

          @endif

          @if(Auth::user()->rol_id > 4 && Auth::user()->rol_id != 9 && Auth::user()->rol_id != 6 && Auth::user()->rol_id != 7 && Auth::user()->rol_id != 8)

          <li class="nav-item has-treeview">
            <li class="nav-item  @if(Request::is('registro_actividades') || Request::is('registro_actividades')) menu-open @endif">
            <a href="/registro_actividades" class="nav-link">
              <i class="fas fa-calendar-check nav-icon"></i>
              <p>
                CSCG
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/registro_actividades" class="nav-link @if(Request::is('registro_actividades') || Request::segment(1) == 'registro_actividades') text-primary @endif">
                  <i class="fas fa-wrench"></i>
                  <p>Registro de Actividades</p>
                </a>
              </li>
            </ul>
          </li>

          @endif  


         
            <li class="nav-item  @if(Request::is('registro_actividades') || Request::is('registro_actividades')) menu-open @endif">
            <a href="/registro_actividades" class="nav-link">
            
              <i class="fas fa-file"></i>
              <p>
                Generador de Reportes Ex
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/tablero_actividad" class="nav-link @if(Request::is('tablero_actividad') || Request::segment(1) == 'tablero_actividad') text-primary @endif">
                  <i class="fas fa-car-battery"></i>
                  <p>Planilla Banco Bateria</p>
                </a>
              </li>
            </ul>

             
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/planillas_ups" class="nav-link @if(Request::is('masiva') || Request::segment(1) == 'tablero_actividad') text-primary @endif">
                  <i class="fas fa-car-battery"></i>
                  <p>Planilla de Ups</p>
                </a>
              </li>
            </ul>
          </li>


          <li class="nav-item has-treeview  @if(Request::is('usuarios') || Request::is('personal') || Request::is('format')) text-primary  menu-open @endif">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Administración
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
           
            <ul class="nav nav-treeview nav_sub_menu">
              @if(Auth::user()->rol_id == 1)
              <li class="nav-item">
                <a  href="#" class="nav-link">
                  <i class="fas fa-book nav-icon"></i>
                  <p>Generador Sismyc
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav-treeview nav_sub_menu" >
                  <li class="nav-item">
                    <a href="/acciones" class="nav-link">
                      <i class="fas fa-random nav-icon"></i>
                      <p>Acciones</p>
                    </a>
                  </li>
                  <li class="nav-item ">
                    <a  href="/salas" class="nav-link">
                      <i class="fas fa-users nav-icon"></i>
                      <p>Salas</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a  href="/pisos" class="nav-link">
                      <i class="fas fa-industry nav-icon"></i>
                      <p>Pisos</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/equipos" class="nav-link">
                      <i class="fas fa-plug nav-icon"></i>
                      <p>Equipos</p>
                    </a>
                  </li>

                  <li class="nav-item">
                    <a href="/fallas_modulo" class="nav-link">
                      <i class="fas fa-plug nav-icon"></i>
                      <p>Fallas por servicio (Ex/AA)</p>
                    </a>
                  </li>
                   <li class="nav-item">
                    <a href="/Componentes" class="nav-link">
                      <i class="fas fa-plug nav-icon"></i>
                      <p>Componentes de equipos</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/marcas" class="nav-link">
                      <i class="fas fa-poll-h nav-icon"></i>
                      <p>Marcas</p>
                    </a>
                  </li>
                 

                </ul>
              </li>
              @endif
             
          @if (Auth::user()->rol_id ==1)
              <li class="nav-item">
                <a href="/usuarios" class="nav-link  @if(Request::is('usuarios')) text-primary @endif">
                  <i class="fas fa-tty"></i>
                  <p>Usuarios</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="/personal" class="nav-link @if(Request::is('personal')) text-primary @endif">
                  <i class="fas fa-user"></i>
                  <p>Personal</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="/format" class="nav-link @if(Request::is('format')) text-primary @endif">
                  <i class="fas fa-archive"></i>
                  <p>Formatos y/o Reportes</p>
                </a>
              </li>
              

           @endif
           @if(Auth::user()->rol_id != 1 )
              <li class="nav-item">
                <a href="/usuarios" class="nav-link  @if(Request::is('usuarios')) text-primary @endif"> 
                <a href="mod_usuario/{{encrypt(Auth::user()->id)}}" class="nav-link">
                 <i class="fas fa-user"></i>
                  <p> Cambiar contraseña</p>
                </a>
              </li>
           @endif
             
              @if(Auth::user()->rol_id == 6 || Auth::user()->rol_id == 1 || Auth::user()->rol_id == 5)
              <li class="nav-item">
                <a href="/localidades" class="nav-link @if(Request::is('localidades')) text-primary @endif">
                  <i class="fas fa-building"></i>
                  <p>Localidades</p>
                </a>
              </li>
              @endif

              @if(Auth::user()->id== 18 || Auth::user()->id == 8)
              <li class="nav-item">
                <a href="/municipios" class="nav-link @if(Request::is('municipios')) text-primary @endif">
                  <i class="fas fa-building"></i>
                  <p>Municipios</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/parroquias" class="nav-link @if(Request::is('parroquias')) text-primary @endif">
                  <i class="fas fa-building"></i>
                  <p>Parroquias</p>
                </a>
              </li>
              @endif
              
              

             
            </ul>
          </li>

          <li class="nav-item has-treeview">
         @if(Auth::user()->rol_id == 1 || Auth::user()->rol_id == 8)
          
          <li class="nav-item  @if(Request::is('registro_actividades')) menu-open @endif">
            <a href="/registro_actividades" class="nav-link">
              <i class="fas fa-search-dollar"></i>
              <p>
                Administracion de Gastos
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/proveedores" class="nav-link @if(Request::is('registro_actividades') || Request::segment(1) == 'registro_actividades') text-primary @endif">
                  <i class="fas fa-address-card"></i>
                  <p>Registro de Proveedores</p>
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/contratos" class="nav-link @if(Request::is('registro_actividades') || Request::segment(1) == 'registro_actividades') text-primary @endif">
                  <i class="fas fa-file-signature"></i>
                  <p>Registro de Contratos</p>
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/facturas" class="nav-link @if(Request::is('registro_actividades') || Request::segment(1) == 'registro_actividades') text-primary @endif">
                  <i class="fas fa-file-invoice-dollar"></i>
                  <p>Registro de Facturas</p>
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/consolidado" class="nav-link @if(Request::is('consolidado') || Request::segment(1) == 'consolidado') text-primary @endif">
                  <i class="fas fa-file-invoice-dollar"></i>
                  <p>Consolidado</p>
                </a>
              </li>
            </ul>
          </li>
              @endif

          
          <li class="nav-item">
            <a href="/logout" class="nav-link">
              <i class="nav-icon far fa-circle text-info"></i>
              <p>Cerrar sesion</p>
            </a>
          </li>
        </ul>
      </nav>
      
    </div>
    
  </aside>
