<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GGEC</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
  
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
   <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">

  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="{{asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
  <!-- Toastr -->
  <link rel="stylesheet" href="{{asset('plugins/toastr/toastr.min.css')}}">
    <!-- Select2 -->
  <link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
 
</head>
<body  style="background-image: url('{{ asset('images/fondo.jpg') }}'); no-repeat center center fixed; background-size: 100% 100%;" class="hold-transition login-page " >
    
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
    <div style="margin-top: -150px">
	     <div class="banner">
	        <img src="{{asset('images/banner.png')}}" width="500" height="70">
	        <h2 style="color: black">Bienvenido al Portal Web Interactivo</h2>
	    </div>
    </div>
     
        
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
              
              <div class="card-body">
              	 <form method="POST" id="formulario_tickets" name="formulario_tickets"  action="{{ url('/tickets')}}">
                        {{ csrf_field() }}
                
				       
                     <div class="col-md-12">
			            <div class="card card-primary">
			              <div class="card-header">
			                <h3 class="card-title" id="titulo">Mantenimiento</h3>
			              </div>
			              <!-- /.card-header -->
			              <div class="card-body">

			              	<div class="row">
			              		<div class="col-sm-5">
				                	<label>Tipo de Actividad:</label>
				                  	<div class="col-sm-12">
				                      <!-- text input -->
				                      	<div class="form-group">
				                        	
				                        	<div class="form-check" required>
					                          <input class="form-check-input" onblur="generar()" id="checkbox1" type="radio" required value="Mantenimiento Preventivo" name="tipo_mantenimiento">
					                          <label class="form-check-label">Mantenimiento Preventivo</label><br>
					                          <input class="form-check-input" onblur="generar()" id="checkbox2" type="radio" required value="Mantenimiento Correctivo"  name="tipo_mantenimiento">
					                          <label class="form-check-label">Mantenimiento Correctivo</label><br>
					                          <input class="form-check-input" onblur="generar()" id="checkbox3" type="radio" required value="Inspeccion"  name="tipo_mantenimiento">
					                          <label class="form-check-label">Inspecciòn</label>
					                        </div>
					                    </div>
				                    </div>
				                 </div>
				                 <div class="col-sm-4">
				                	<label>Tipo de Servicio:</label>
				                  	<div class="col-sm-4">
				                      <!-- text input -->
				                      	<div class="form-group">
				                        	
				                        	<div class="form-check" required>
					                          <input class="form-check-input" onblur="generar()" type="radio" id="tipo_servicio" value="Ex" name="tipo_servicio">
					                          <label class="form-check-label">Ex</label><br>
					                          <input class="form-check-input" onblur="generar()" type="radio" id="tipo_servicio2" value="A/A" name="tipo_servicio">
					                          <label class="form-check-label">A/A</label>
					                        </div>
					                    </div>
				                    </div>
				                 </div>
				                 
				                 <!-- <div class="col-sm-4">
			                      	<div class="form-group">
			                        	<label>Buscar equipo Serial:</label>
				                        	<input type="text" class="form-control" required name="telefono" >
				                    </div>
			                      </div> -->

			              	</div>
							<div class="row">
			                  	<div class="col-sm-4">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Región:</label>
				                        	<select  name="central[region_id]" id="region" class="form-control">
				                        		<option value="">Seleccione una Region</option>
				                        		@foreach($regiones as $region)
					                        		<option value="{{$region->id}}">{{$region->nombre}}</option>
					                        	@endforeach
					                    	</select>
				                    	</div>
				                </div>
		                    	<div class="col-sm-4">
	                      <!-- text input -->
			                      <div class="form-group">
			                        <label>Estado:</label>
			                        <select name="central[estado_id]" id="estado" class="form-control">
				                       		<option value="">Seleccione un Estado</option>
			                    	</select>
			                      </div>
			                    </div>
		                    	<div class="col-sm-4">
	                    
			                      	<div class="form-group">
			                        	<label>Central:</label>
				                        	<select  name="central_id" id="central" class="form-control">
						                       		<option value="">Seleccione una Central</option>
					                    	</select>
				                    </div>
			                      </div>
			                </div>

			                <div class="row">

			                    <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Tipo de Equipo:</label>
				                        <select name="equipo_id" id="equipo" onblur="generar()" class="form-control">
				                        	<option value="">Seleccione un equipo</option>
				                        	@foreach($equipos as $equipo)
				                        		<option value="{{$equipo->denominacion}}">{{$equipo->denominacion}}</option>
			                            	@endforeach
										</select>
				                      </div>
				                    </div>
			                	<div class="col-sm-4" style="display: none">
			                        <div class="form-group">
			                        	<label>Equipos:</label>
			                        	<select id="equipo_en_central" name="box_ModeloRectificador" class="form-control select2bs4">
											<option value="">Seleccione el equipo</option>
										</select>
											
			                        </div>
			                    </div>

			                	 <div class="col-sm-4">
			                      <!-- text input -->
			                      <div class="form-group">
			                        <label>Componente de Equipo:</label>
			                        <select name="componente_id" id="componente" onblur="generar()" class="form-control">
			                            <option value=""></option>
									</select>
			                      </div>
			                    </div>
			                    <div class="col-sm-4">
			                      <!-- text input -->
			                      <div class="form-group">
			                        <label>Tipo de Actividad:</label>
			                        <select name="tipo_actividad" onblur="generar()" id="mantenimiento" class="form-control">
		                            	<option value="">Seleccione una actividad</option>
									</select>
			                      </div>
			                    </div>
			                   </div>

			                    <div class="row">

			                    <div class="col-sm-4" id="Correctivo" style="display: none;">
			                      <!-- text input -->
			                      <div class="form-group">
			                        <label>Tipo de Falla:</label>
			                        <select disabled name="tipo_falla" onblur="generar()" id="fallas" class="form-control">
		                            	<option value="">Seleccione una Falla</option>
									</select>
			                      </div>
			                    </div>


			                </div>
			                	
			                <div class="row">
			                	<div class="col-sm-4">
			                        <div class="form-group">
			                        	<label>Sala:</label>
			                        	<select name="sala" id="box_ModeloRectificador" name="box_ModeloRectificador" class="form-control">
											<option value="">Seleccione una Sala</option>
											@foreach($salas as $sala)
			                            		<option value="{{$sala->nombre}}">{{$sala->nombre}}</option>
			                            	@endforeach
										</select>
			                        </div>
			                    </div>

			                    
		                    	<div class="col-sm-4">
		                      		<div class="form-group">
		                        		<label>Piso:</label>
				                        	<select id="piso" onchange="generar()" name="box_ModeloRectificador" class="form-control select2 ">
												<option value="">Seleccione un piso</option>
												@foreach($pisos as $piso)
				                            		<option value="{{$piso->nombre}}">{{$piso->nombre}}</option>
				                            	@endforeach
											</select>
			                    	</div>
	                      		</div>
			                    <div class="col-sm-4">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Reporta/Responsable</label>
				                        	<!--input type="text" class="form-control" required name="responsable_id" -->
				                        	<select  name="responsable_id" onblur="generar()" id="personal" class="form-control">
				                        		
					                        	<option  value="">Quien Informa</option>
					                        	
					                    	</select>
				                    </div>
			                    </div>
			                </div>

			                <div class="row" style="display: none">
			                    <div class="col-sm-4">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Teléfono Adicional:</label>
			                        		<div class="input-group">
								                    <div class="input-group-prepend">
								                      <span class="input-group-text"><i class="fas fa-phone"></i></span>
								                    </div>
								                    <input type="text" name="telefono" required  class="form-control" data-inputmask='"mask": "99999999999"' data-mask>
								                  </div>
								            </div>
			                      </div>
		                   
			                    <div class="col-sm-4">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Cargo:</label>
				                        	<input type="text" class="form-control" required name="cargo" >
				                    </div>
			                      </div>
			                	
			                    <div class="col-sm-12">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Correo:</label>
				                        	<input type="text" class="form-control" required name="correo" >
				                    </div>
			                      </div>



			                </div>
			                <div class="row" style="display: none">
			                    <div class="col-sm-12">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Correo Supervisor:</label>
				                        	<input type="text" class="form-control" required name="correo_supervisor" >
				                    </div>
			                      </div>
			                </div>
			                <div class="row">
			                	
			                 <div class="col-sm-4" style="display: none;">

			                 		<input type="hidden" name="status" value="En Proceso">
			                      	<div class="form-group">
			                        	<label>Estatus:</label>
				                        	<select  class="form-control">
				                        		<option></option> 
				                            	<option value="En Proceso">En Proceso</option>
					                        	<option  value="Resuelto">Resuelto</option>
					                        	<option  value="Asignado">Asignado</option>
					                        	<option  value="Cancelado">Cancelado</option>
					                        	<option  value="Pendiente por">Pendiente por</option>
					                        	<option  value="Cerrado">Cerrado</option>
					                    	</select>
				                    </div>
				                </div>
			             </div>
		                    
			                <div class="row">
			                    <div class="col-sm-4">
			                      	<div class="form-group">
			                        	<label>Fecha de Inicio de Actividad: </label>
				                      	<input type="date" onblur="generar()"  class="form-control" id="fecha_inicio" required name="fecha_inicio" value="" >
				                    </div>
			                    </div>
	                    		<div class="col-sm-4">
	                    			<div class="form-group"> 
	                    				<label>Hora de inicio de Actividad: </label>
				                   	 	<div class="input-group date" id="timepicker" data-target-input="nearest">
	   				                       	<input onblur="generar()" type="text" name="hora_inicio" id="hora_inicio" class="form-control datetimepicker-input" data-target="#timepicker">
						                      <div class="input-group-append" data-target="#timepicker" data-toggle="datetimepicker">
						                          <div class="input-group-text"><i class="far fa-clock"></i></div>
						                      </div>
					                       
					                   	</div>
				                   	</div>
	                    		</div>
		                      	<div class="col-sm-4">
		                      		<div class="form-group">
		                        		<label>Fecha de Fin:</label>
		                        		<input type="date" readonly="true" class="form-control" required name="fecha_fin_falla" >
			                    	</div>
		                      	</div>

				                    
			                </div>

			                <div class="row">
			                	<div class="col-sm-4">
			                		<a onclick="generar()" class="btn btn-info">Generar comentario de apertura</a>
			                	</div>
			                   	<div class="col-sm-8">
			                      	<div  class="form-group">
			                        	<label>Fecha de Creacion de ticket: </label>
			                        	
				                      	<span>{{$ldate}}</span>
				                    </div>
			                    </div>


			                  </div>

			                  <div class="row">
			                  	
			                    <div class="col-sm-12">
			                      <!-- text input -->
				                     <div class="form-group">
										<label for="comment">Observaciones:</label>
										<textarea readonly class="form-control"  name="actividad" rows="3" id="comment"></textarea>			
									</div>
			                    </div>

			                  </div>
								
			            	</div>
			            </div>
			        </div>
	                <div class="card-footer" align="right" style="display: none">
		                <button type="submit" class="btn btn-primary">Guardar</button>
		            </div>
                </form>
			</div>
        </div>
    </div>
	

			    
      <!-- /.container-fluid -->
    
    <!-- /.content -->
  </div>
          
          
            

	
  
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('plugins/select2/js/select2.full.min.js')}}"></script>

<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- SweetAlert2 -->
<script src="{{asset('dist/js/clipboard.min.js')}}"></script>

  <script src="{{asset('plugins/sweetalert2/sweetalert2.min.js')}}"></script>
  <!-- Toastr -->
  <script src="{{asset('plugins/toastr/toastr.min.js')}}"></script>

<script src="{{asset('js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>



<script type="text/javascript">
var clipboard = new Clipboard('.btn');
$('.select2').select2();

	
	$(document).ready(function(){
        $("#checkbox1").click(function(){
	        $('#Correctivo').hide();
	       	$('#fallas').prop('disabled','disabled');

	       	var APP_URL = {!!json_encode(url('/'))!!}+'/';
			    $.get(APP_URL+'mantenimiento/'+1, function (cidades) {
			        $('select[id=mantenimiento]').empty();
	                    if(cidades.length != 0){
			              $('select[id=mantenimiento]').append('<option value=""> Selecciona una actividad </option>');
			              $.each(cidades, function (key, value) {
			                  $('select[id=mantenimiento]').append('<option value=' + value.id + '>'+ value.descripcion + '</option>');
			              });
			          }

			      });
   		 });
        $("#checkbox2").click(function(){
        	$('#fallas').prop('disabled', false);
	        $('#Correctivo').show();
   		 });
});
	$(document).ready(function(){
        $("#checkbox3").click(function(){
	        $('#Correctivo').hide();
	       	$('#fallas').prop('disabled','disabled');

	       	var APP_URL = {!!json_encode(url('/'))!!}+'/';
			    $.get(APP_URL+'mantenimiento/'+1, function (cidades) {
			        $('select[id=mantenimiento]').empty();
	                    if(cidades.length != 0){
			              $('select[id=mantenimiento]').append('<option value=""> Selecciona una actividad </option>');
			              $.each(cidades, function (key, value) {
			                  $('select[id=mantenimiento]').append('<option value=' + value.id + '>'+ value.descripcion + '</option>');
			              });
			          }

			      });
   		 });
        $("#checkbox2").click(function(){
        	$('#fallas').prop('disabled', false);
	        $('#Correctivo').show();
   		 });
});

function generar(){
	var rt;
	var mnt;
	var fll = " -";
	var equipo1 = document.getElementById('checkbox1');
	var equipo2 = document.getElementById('checkbox2');
	var equipo3 = document.getElementById('checkbox3');
	var fallas = document.getElementById('fallas');
	if (equipo1.checked){
		rt = equipo1.value
	}if (equipo2.checked){
		fll =' que se encuentra con falla: '+fallas.options[fallas.selectedIndex].text
		rt = equipo2.value
	}if (equipo3.checked){
		rt = equipo3.value
	}
	var mantenimiento = document.getElementById('tipo_servicio');
	var mantenimiento2 = document.getElementById('tipo_servicio2');

	if (mantenimiento.checked){
		mnt = mantenimiento.value
	}if (mantenimiento2.checked){
		mnt = mantenimiento2.value
	}
	var personal = document.getElementById('personal');
	var tipo_actividad = document.getElementById('mantenimiento');
	
	var box_ModeloRectificador = document.getElementById('box_ModeloRectificador');

	var equipo = document.getElementById('equipo');
	var componente = document.getElementById('componente');
	var fecha_inicio = document.getElementById('fecha_inicio').value;
	var hora_inicio = document.getElementById('hora_inicio').value;
	var piso = document.getElementById('piso').value;
	
	document.getElementById('comment').value='HIA: '
	+hora_inicio+' '+'Informa :'
	+personal.options[personal.selectedIndex].text+' que realizara: '
	+rt+ ', a un equipo de:  '
	+mnt+ ' de tipo:'
	+equipo.options[equipo.selectedIndex].text +' en sala :'
	+box_ModeloRectificador.options[box_ModeloRectificador.selectedIndex].text +' piso: '
	+piso+' al componente: '
	+componente.options[componente.selectedIndex].text
	+fll + ' y realizarà: '
	+tipo_actividad.options[tipo_actividad.selectedIndex].text
	



}




  	$(document).ready(function(){
        $("#tipo_servicio").click(function(){
    		
		    var tipo_servicio = 1;
		    
		    var APP_URL = {!!json_encode(url('/'))!!}+'/';
		    $.get(APP_URL+'tipo_servicio/' + tipo_servicio, function (cidades) {
		        $('select[id=equipo]').empty();

		        if(cidades.length != 0){
		            $('select[id=equipo]').append('<option value=""> Selecciona un tipo equipo </option>');
		            $.each(cidades, function (key, value) {
		                $('select[id=equipo]').append('<option value=' + value.id + '>'+ value.descripcion + '</option>');
		            });
		        }

		        });

		    });
          	
        $("#tipo_servicio2").click(function(){
        		 var tipo_servicio = 2;
		    
		    var APP_URL = {!!json_encode(url('/'))!!}+'/';
		    $.get(APP_URL+'tipo_servicio/' + tipo_servicio, function (cidades) {
		        $('select[id=equipo]').empty();

		        if(cidades.length != 0){
		            $('select[id=equipo]').append('<option value=""> Selecciona un tipo equipo </option>');
		            $.each(cidades, function (key, value) {
		                $('select[id=equipo]').append('<option value=' + value.id + '>'+ value.descripcion + '</option>');
		            });
		        }

		        });
         
   		 });


   		

});
  $(document).ready(function(){


  	$('select[id=region]').change(function () {
    	console.log('hola')
    var region_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'estado/' + region_id, function (cidades) {
        $('select[id=estado]').empty();

        if(cidades.length != 0){
            $('select[id=estado]').append('<option value=""> Selecciona un estado</option>');
            $.each(cidades, function (key, value) {
                $('select[id=estado]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });

    $.get(APP_URL+'pers/' + region_id, function (cidades) {
        $('select[id=personal]').empty();

        if(cidades.length != 0){
            $('select[id=personal]').append('<option value=""> Quien informa</option>');
            $.each(cidades, function (key, value) {
                $('select[id=personal]').append('<option value=' + value.nombre + '>'+ value.cargo +' '+value.nombre +' '+value.apellido+' '+value.num_personal +'</option>');
            });
        }

        });
    


    });


	$('select[id=estado]').change(function () {
    	//console.log('hola')
    var estado_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'central/' + estado_id, function (cidades) {
        $('select[id=central]').empty();

        if(cidades.length != 0){
            $('select[id=central]').append('<option value=""> Selecciona un central</option>');
            $.each(cidades, function (key, value) {
                $('select[id=central]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });

    $.get(APP_URL+'pers/' + estado_id, function (cidades) {
        $('select[id=personal]').empty();
        console.log(cidades)

        if(cidades.length != 0){
            $('select[id=personal]').append('<option value=""> Quien informa</option>');
            $.each(cidades, function (key, value) {
                $('select[id=personal]').append('<option value=' + value.nombre + '>'+ value.cargo +' '+'de la GGEC '+cidades[0].estado.nombre+' '+' '+value.nombre +' '+value.apellido+' '+value.num_personal +'</option>');
            });
        }

        });

    });


    

    $('select[id=equipo]').change(function () {
      //console.log("estoy aqui papa");
      var equipo_id = $(this).val();
      
      var APP_URL = {!!json_encode(url('/'))!!}+'/';
      $.get(APP_URL+'componentes/' + equipo_id, function (cidades) {
          $('select[id=componente]').empty();

          if(cidades.length != 0){
              $('select[id=componente]').append('<option value=""> Selecciona un Componente </option>');
              $.each(cidades, function (key, value) {
                  $('select[id=componente]').append('<option value=' + value.id + '>'+ value.descripcion + '</option>');
              });
          }

      });
    });



    $('select[id=componente]').change(function () {
      var falla_id = $(this).val();
      var APP_URL = {!!json_encode(url('/'))!!}+'/';
      $.get(APP_URL+'fallas/' + falla_id, function (cidades) {
          $('select[id=fallas]').empty();

          if(cidades.length != 0){
              $('select[id=fallas]').append('<option value=""> Selecciona una Falla </option>');
              $.each(cidades, function (key, value) {
                  $('select[id=fallas]').append('<option value=' + value.id + '>'+ value.descripcion + '</option>');
              });
          }

      });
    });

    $('select[id=fallas]').change(function () {
      var rt;
      var falla_id = $(this).val();
      var equipo1 = document.getElementById('checkbox1');
	  var equipo2 = document.getElementById('checkbox2');
	  if (equipo1.checked){
		rt = 1
		}if (equipo2.checked){
			rt = 2
		}
      
      var APP_URL = {!!json_encode(url('/'))!!}+'/';
      $.get(APP_URL+'mantenimiento/' + falla_id+ '/'+rt, function (cidades) {
          $('select[id=mantenimiento]').empty();

          if(cidades.length != 0){
              $('select[id=mantenimiento]').append('<option value=""> Selecciona una actividad </option>');
              $.each(cidades, function (key, value) {
                  $('select[id=mantenimiento]').append('<option value=' + value.id + '>'+ value.descripcion + '</option>');
              });
          }

      });
    });











     $('select[id=equipo]').change(function () {
      var equipo_en_central = $(this).val();
      var central = document.getElementById('central').value;
      // var select =  $('select[id="equipo"] option:selected').text();
      // console.log(select);
      var APP_URL = {!!json_encode(url('/'))!!}+'/';
      $.get(APP_URL+'equipos_centrales/' + equipo_en_central, function (cidades) {
      	//console.log(cidades[0].motor);
          $('select[id=equipo_en_central]').empty();

          if(cidades.length != 0){
              $('select[id=equipo_en_central]').append('<option value=""> Selecciona un equipo </option>');

              
              	if (equipo_en_central == 1){
              		///////////////MOTOGENERADOR
	              	$.each(cidades, function (key, value) {
	                	  $('select[id=equipo_en_central]').append('<option value=' + value.id + '>'+ value.motor.marca +' '+ value.motor.serial + '</option>');
	              	});
              	}else if(equipo_en_central == 2){
              		///////// RECTIFICADORES
              		$.each(cidades, function (key, value) {
	                	  $('select[id=equipo_en_central]').append('<option value=' + value.id + '>'+ value.rectificador.marca +' '+ value.rectificador.serial + '</option>');
	              	});
              	}else if(equipo_en_central == 3){
              		$.each(cidades, function (key, value) {
	                	  $('select[id=equipo_en_central]').append('<option value=' + value.id + '>'+ value.marca +' '+ value.serial + '</option>');
	              	});
              	}else if(equipo_en_central == 7){
              		/*UPS BANCO DE BATERIA*/
              		$.each(cidades, function (key, value) {
	                	  $('select[id=equipo_en_central]').append('<option value=' + value.id + '>'+ value.ups.marca +' '+ value.ups.serial + '</option>');
	              	});
              	}



          }else{
          	$('select[id=equipo_en_central]').append('<option disabled value=""> No hay equipos registrados </option>');

          }

      });
    });


  });






$(document).ready(function () {

	
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});

</script>
</body>
</html>


