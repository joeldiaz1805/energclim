<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title id="title">Portal Web GGEC - {{Request::segment(1)}}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('css/ionicons.min.css')}}">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{asset('plugins/jqvmap/jqvmap.min.css')}}">

    <!-- Select2 -->
  <link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">



  <link rel="stylesheet" href="{{asset('plugins/bootstrap-select/bootstrap-select.css')}}">



  <link rel="stylesheet" href="{{asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">

<!-- SweetAlert2 -->
  <link rel="stylesheet" href="{{asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
  <!-- Toastr -->
  <link rel="stylesheet" href="{{asset('plugins/toastr/toastr.min.css')}}">

  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('plugins/summernote/summernote-bs4.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

  <link rel="stylesheet" href="{{asset('css/leaftlejs.css')}}" >

  <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="{{asset('plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css')}}">

  <link rel="stylesheet" href="{{asset('plugins/datatables-buttons/css/buttons.dataTables.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/datatables-fixedheader/css/fixedHeader.bootstrap4.min.css')}}">


  <link rel="apple-touch-icon"  href="{{asset('/img/fav/apple-touch-icon.png')}}">
  <link rel="icon" type="image/png"  href="{{asset('/img/fav/favicon-32x32.png')}}">
  <link rel="icon" type="image/png"  href="{{asset('/img/fav/favicon-16x16.png')}}">
  <link rel="manifest" href="{{asset('/img/fav/site.webmanifest')}}">
 

    <style type="text/css">
      .help-block{
        color: red
      }
    
      .alerta-errores{
        color: red
      }
      thead input {
        width: 100%;
    }

    </style>
   <div id="estilos_middleware"></div>

</head>
<body class="hold-transition sidebar-mini layout-fixed " >
<div class="wrapper">

@include('layouts/head')
  <!-- Navbar -->
@include('layouts/nav')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
 @include('layouts/side')
 
<div id="help_popupTopLeft" style="position: absolute;width: 175px;text-align: left;z-index: 1001;display:none">
  <div style="padding:2px;filter:shadow(color:gray,strength:7, direction:135);" id="help_popup_inner"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <th width="6px" height="5" class="m-tl"><img src="{{asset('/img/m-tl.gif')}}"></th>
    <th width="500px" bgcolor="#ff6600"></th>
    <th width="6" height="5" valign="top" class="m-tr"><img src="{{asset('/img/m-tr.gif')}}"></th>
  </tr>
  <tr>
    <th colspan="3" bgcolor="#ff6600" style="border-left:1px solid #ff6600; padding-left:3px; padding-right:3px"><div id="help_popupTopLeftTitle" class="globe_header" align="center"></div></th>
    </tr>
  <tr>
    <th colspan="3" bgcolor="#FFFFFF" style="border-left:1px solid #ff6600; padding-left:3px; border-right:1px solid #ff6600; padding-right:3px; padding-bottom:3px; padding-top:3px"><div style="margin-top:3; margin-bottom:3;" id="help_popupTopLeftText" class="globe_cont"></div></th>
    </tr>
  <tr>
    <th colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tbody><tr>
        <th align="right" valign="top" scope="col"><img src="{{asset('/img/help_box_bottomrightCIZ.gif')}}" width="10" height="6"></th>
        <th scope="col" width="100%" style="background-image:url({{asset('/img/help_box_bottomC.gif')}})"></th>
        <th width="35" height="31" align="left" valign="top" scope="col"><img src="{{asset('/img/help_box_bottomleftCIZ.gif')}}" width="35" height="31"></th>
      </tr>
    </tbody></table></th>
    </tr>
  </tbody></table>
  </div>
  </div>


  <div id="help_popupTopRight" style="position: absolute;width: 175px;text-align: left;z-index: 1001;display:none;">
    <div style="padding:2px;filter:shadow(color:gray,strength:7, direction:135);" id="help_popup_inner2"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody><tr>
      <th width="6px" height="5" class="m-tl"><img src="{{asset('/img/m-tl.gif')}}"></th>
      <th width="500px" bgcolor="#ff6600"></th>
      <th width="6" height="5" valign="top" class="m-tr"><img src="{{asset('/img/m-tr.gif')}}"></th>
    </tr>
    <tr>
      <th colspan="3" bgcolor="#ff6600" style="border-left:1px solid #ff6600; padding-left:3px; padding-right:3px"><div id="help_popupTopRightTitle" class="globe_header" align="center"></div></th>
      </tr>
    <tr>
      <th colspan="3" bgcolor="#FFFFFF" style="border-left:1px solid #ff6600; padding-left:3px; border-right:1px solid #ff6600; padding-right:3px; padding-bottom:3px; padding-top:3px"><div style="margin-top:3; margin-bottom:3;" id="help_popupTopRightText" class="globe_cont"></div></th>
      </tr>
    <tr>
      <th colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody><tr>
          <th width="35" height="31" align="left" valign="top" scope="col"><img src="{{asset('/img/help_box_bottomleftC.gif')}}" width="35" height="31"></th>
          <th scope="col" width="100%" style="background-image:url({{asset('/img/help_box_bottomC.gif')}})"></th>
          <th align="right" valign="top" scope="col"><img src="{{asset('/img/help_box_bottomrightC.gif')}}" width="10" height="6"></th>
        </tr>
      </tbody></table></th>
      </tr>
  </tbody></table>
  </div>
  </div>

  <div id="help_popupBottomRight" style="position: absolute;width: 175px;text-align: left;z-index: 1001;display:none">
    <div style="padding:2px;filter:shadow(color:gray,strength:7, direction:135);" id="help_popup_inner"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody><tr>
      <th colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody><tr>
          <th align="right" valign="top" scope="col"><img src="{{asset('/img/help_box_bottomleftCIZ_up.gif')}}" width="35" height="31"></th>
          <th scope="col" width="100%" style="background-image:url({{asset('/img/help_box_bottomC_up.gif')}})">&nbsp;</th>
          <th align="left" valign="top" scope="col"><img src="{{asset('/img/help_box_bottomrightC_up.gif')}}" width="10" height="31"></th>
        </tr>
      </tbody></table></th>
    </tr>
    <tr>
      <th colspan="3" bgcolor="#ff6600" style="border-left:1px solid #ff6600; padding-left:3px; padding-right:3px"><div id="help_popupBottomRightTitle" class="globe_header" align="center"></div></th>
      </tr>
    <tr>
      <th colspan="3" bgcolor="#FFFFFF" style="border-left:1px solid #ff6600; padding-left:3px; border-right:1px solid #ff6600; padding-right:3px; padding-bottom:3px; padding-top:3px"><div style="margin-top:3; margin-bottom:3;" id="help_popupBottomRightText" class="globe_cont"></div></th>
      </tr>
    <tr>
      <th colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody><tr>
          <th align="right" valign="top" scope="col"><img src="{{asset('/img/help_box_bottomrightCIZ.gif')}}" width="10" height="6"></th>
          <th scope="col" width="100%" style="background-image:url({{asset('/img/help_box_bottomC.gif')}})"></th>
          <th align="left" valign="top" scope="col"><img src="{{asset('/img/help_box_bottomrightC.gif')}}" width="10" height="6"></th>
        </tr>
      </tbody></table></th>
      </tr>
  </tbody></table>
  </div>
  </div>
  <div id="help_popupBottomLeft" style="position: absolute;width: 175px;text-align: left;z-index: 1001;display:none">
    <div style="padding:2px;filter:shadow(color:gray,strength:7, direction:135);" id="help_popup_inner"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody><tr>
      <th colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody><tr>
          <th align="right" valign="top" scope="col"><img src="{{asset('/img/help_box_bottomrightCIZ_up.gif')}}" width="10" height="31"></th>
          <th scope="col" width="100%" style="background-image:url({{asset('/img/help_box_bottomC_up.gif')}})">&nbsp;</th>
          <th align="left" valign="top" scope="col"><img src="{{asset('/img/help_box_bottomleftC_up.gif')}}" width="35" height="31"></th>
        </tr>
      </tbody></table></th>
    </tr>
    <tr>
      <th colspan="3" bgcolor="#ff6600" style="border-left:1px solid #ff6600; padding-left:3px; padding-right:3px"><div id="help_popupBottomLeftTitle" class="globe_header" align="center"></div></th>
      </tr>
    <tr>
      <th colspan="3" bgcolor="#FFFFFF" style="border-left:1px solid #ff6600; padding-left:3px; border-right:1px solid #ff6600; padding-right:3px; padding-bottom:3px; padding-top:3px"><div style="margin-top:3; margin-bottom:3;" id="help_popupBottomLeftText" class="globe_cont"></div></th>
      </tr>
    <tr>
      <th colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody><tr>
          <th align="right" valign="top" scope="col"><img src="{{asset('/img/help_box_bottomrightCIZ.gif')}}" width="10" height="6"></th>
          <th scope="col" width="100%" style="background-image:url({{asset('/img/help_box_bottomC.gif')}})"></th>
          <th align="left" valign="top" scope="col"><img src="{{asset('/img/help_box_bottomrightC.gif')}}" width="10" height="6"></th>
        </tr>
      </tbody></table></th>
      </tr>
  </tbody></table>
  </div>
  </div>
 

    @yield('content')


  @include('layouts/footer')


  <!-- Content Wrapper. Contains page content -->
  
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


<!-- jQuery UI 1.11.4 -->

  <!-- jQuery -->
<script src="{{asset('plugins/jquery/jquery.js')}}"></script>
<script src="{{asset('plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<script>
    $.widget.bridge('uibutton', $.ui.button)
  </script> 
  <script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('plugins/chart.js/Chart.min.js')}}"></script> 
  <script src="{{asset('plugins/sparklines/sparkline.js')}}"></script>
   <script src="{{asset('plugins/jquery-knob/jquery.knob.min.js')}}"></script>
   <!--
   <script src="{{asset('plugins/jqvmap/jquery.vmap.min.js')}}"></script> 
   <script src="{{asset('plugins/jqvmap/maps/jquery.vmap.germany.js')}}"></script> 
   <script src="{{asset('js/pages/vmap.js')}}"></script>


 
   <!-- mapa ve-mill clases--> 
<!--<script type="text/javascript" src="{{asset('js/pages/jquery.min.js')}}"></script>
 -->
  <link rel="stylesheet" type="text/css" href="{{asset('js/pages/jquery-jvectormap-2.0.5.css')}}">
  <script type="text/javascript" src="{{asset('js/pages/jquery-jvectormap-2.0.5.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/pages/jquery-jvectormap-map.js')}}"></script>


  <script type="text/javascript" src="{{asset('plugins/bootstrap-select/bootstrap-select.min.js')}}"></script>


  <!-- InputMask --> 
  <script src="{{asset('plugins/moment/moment.min.js')}}"></script>
  <script src="{{asset('plugins/inputmask/min/jquery.inputmask.bundle.min.js')}}"></script>
  <script src="{{asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- jquery-validation -->
<script src="{{asset('plugins/summernote/summernote-bs4.min.js')}}"></script>
<script src="{{asset('plugins/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('plugins/jquery-validation/additional-methods.min.js')}}"></script>
<!-- Bootstrap 4 -->

<!-- SweetAlert2 -->
  <script src="{{asset('plugins/sweetalert2/sweetalert2.min.js')}}"></script>
  <!-- Toastr -->
  <script src="{{asset('plugins/toastr/toastr.min.js')}}"></script>

<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.js')}}"></script>
<!-- AdminLTE dashboard demo pagina de graficos (This is only for demo purposes) -->
<!--<script src="{{asset('dist/js/pages/dashboard.js')}}"></script>-->
<script src="{{asset('js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
@error('superUser')
  <script>
      toastr.error(' epa... No tienes el privilegio para este modulo');
   </script>
  @enderror

 



<!-- 
-->
<!-- ChartJS -->
<!-- Sparkline -->
<!-- JQVMap -->
<!-- jQuery Knob Chart -->
<!-- daterangepicker -->
<!-- Tempusdominus Bootstrap 4 -->
<!-- Summernote -->
<!-- overlayScrollbars -->
<script src="{{asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
  
  <!-- Select2 -->
<script src="{{asset('plugins/select2/js/select2.full.min.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>

<script src="{{asset('js/vector/mapdata.js')}}"></script>
<script src="{{asset('js/vector/countrymap.js')}}"></script>



<!-- BUTTONS DE DATATABLE -->
<script src="{{asset('plugins/datatables-buttons/js/dataTables.buttons.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/datatables-buttons/js/buttons1.html5.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/datatables-buttons/js/jszip.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/datatables-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>

<script type="text/javascript" src="{{asset('plugins/datatables-buttons/js/pdfmake.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/datatables-buttons/js/vfs_fonts.js')}}"></script>

<script src="{{asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/mercJs/pt.bubble-min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/mercJs/pt.loadImages.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/leaftlejs.js')}}"></script>

<!-- Bootstrap4 Duallistbox -->
<script src="{{asset('/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js')}}"></script>
<!--Iconos-->
 <script type="text/javascript" src="{{ asset('js/icos.js')}}"></script>


<script>
 @if(Session::has('superUser'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'error',
        title: 'no no. No tienes el privilegio para este modulo'
      })
      })
    });
  @endif

  
  @if(Session::has('permision'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'error',
        title: 'No tienes Permisos'
      })
      })
    });
  @endif

$(document).ready(function() {
  
  // Setup - add a text input to each footer cell
    $('#example1 thead tr').clone(true).appendTo( '#example1 thead' );
    $('#example1 thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Filtrar '+title+'" />' );
 
        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    } );
      $('.js-example-basic-multiple').select2();
      var fecha =new  Date();
    var table = $("#example1").DataTable({
        "orderCellsTop": true,
        "fixedHeader": true,
        "responsive": false,
        "autoWidth": true,
        "bJQueryUI": true,
        'deferRender':    true,
        'scrollY':        500,
        'scrollX':        true,
        'scrollCollapse': true,
        'scroller':       true,
       
        dom: 'Bfrtip',
          buttons: [
             
               {
              extend: 'excelHtml5',
              messageTop:fecha.getFullYear()+'-'+(fecha.getMonth()+1)+'-'+fecha.getDate(),
              exportOptions: {
                stripHtml: true, /* Aquí indicamos que no se eliminen las imágenes */
                 columns: [':visible:not(.not-export-col):not(.hidden)'],
                 format:{
                  body: function (data, row, column, node ) {
                          return $(node).is("div") ?
                                $(node).find('input:text').val():
                                data;
                      }
                 }
              },
              footer:true
          }
          ],
      });



     new $.fn.dataTable.FixedHeader( table );

});


  $(function () {



var table = $("#example2").DataTable({
        "orderCellsTop": true,
        "fixedHeader": true,
        "responsive": false,
        "autoWidth": true,
        "bJQueryUI": true,
        'deferRender':    true,
        'scrollY':        500,
        'scrollX':        false,
        'scrollCollapse': true,
        'scroller':       true,
       
        dom: 'Bfrtip',
          buttons: [
             
               {
              extend: 'excelHtml5',
              exportOptions: {
                stripHtml: true, /* Aquí indicamos que no se eliminen las imágenes */
                 columns: [':visible:not(.not-export-col):not(.hidden)'],
                 format:{
                  body: function (data, row, column, node ) {
                          return $(node).is("div") ?
                                $(node).find('input:text').val():
                                data;
                      }
                 }
              },
              footer:true
          }
          ],
      });

/*
     
    $("#example2").DataTable({
        "responsive": true,
        "autoWidth": false,
        dom: 'Bfrtip',
          buttons: [
              {
                  extend: 'pdfHtml5',
                  orientation: 'landscape',
                  pageSize: 'LEGAL'
              },
               {
              extend: 'excelHtml5',
              customize: function( xlsx ) {
                  var sheet = xlsx.xl.worksheets['Reporte_inventario.xml'];
   
                  $('row c[r^="C"]', sheet).attr( 's', '2' );
              }
          }
          ],
      });*/
  });
</script>

@yield('scriptsalerts')


<script> 


   

  @if(Session::has('success'))
      toastr.success('{!! Session::get('success')!!}')
   @endif
   @if(Session::has('info'))
      toastr.info('{!! Session::get('info')!!}')
    @endif
    @if(Session::has('error'))
      toastr.error('{!! Session::get('error')!!}')
    @endif
   @if(Session::has('warning'))
      toastr.warning('{!! Session::get('warning')!!}')
    @endif
  
</script>
@yield('vs')




<script>
	 


  //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
 
    $(function () {
                $('#datetimepicker1').datetimepicker();
            });

  $(function () {
   

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'L'
    });
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )


    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    //$('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

    

  })
</script>

</body>
</html>
