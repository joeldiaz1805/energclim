<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GGEC</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
  
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
<!-- SweetAlert2 -->
  <link rel="stylesheet" href="{{asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
  <!-- Toastr -->
  <link rel="stylesheet" href="{{asset('plugins/toastr/toastr.min.css')}}">


 
</head>
<body  style="background-image: url('{{ asset('images/fondo_mejorado.jpg') }}'); no-repeat center center fixed; background-size: 100% 100%;" class="hold-transition login-page">
     <div style="margin-top: -150px">
     <div class="banner">
        <img src="{{asset('images/banner(1).png')}}" width="500" height="70">
        <h2 style="color: black">Bienvenido al Portal Web Interactivo</h2>
    </div>
    </div>
<div class="login-box">
  <div class="login-logo">
    <a href="../../index2.html" style="color: black"><b>Energía y Climatización</b></a>
  </div>

  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Inicia Sesion en el portal</p>

      <form action="{{url('/')}}" method="post">
          {{ csrf_field() }}
            @if(Session::has('error_message'))
              <div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h5><i class="icon fas fa-ban"></i> Error!</h5>
                    {{ Session::get('error_message') }}
                
                </div>
            @endif
        <div class="input-group mb-3">
          <input type="text" class="form-control" name="email" placeholder="Email o Alias">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" id="password" class="form-control" name="password" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              
                <a href="#" id="show"> 
                  
                <span id="show" class="fas fa-eye"></span>
                </a>
              
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember">
              <label for="remember">
                Recordar
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Entrar</button>
          </div>
          <!-- /.col -->
        </div>
          <div class="row">
            <div class="col-md-12">
              
            <p class="mb-1">
              <a href="/password_reset">¿Olvido su Contraseña?</a>
            </p>
            </div>
            
          </div>
      </form>

    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<div id="myModal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
   
  </div>
</div>

<!-- /.login-box -->

<!-- jQuery -->
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- SweetAlert2 -->
  <script src="{{asset('plugins/sweetalert2/sweetalert2.min.js')}}"></script>
  <!-- Toastr -->
  <script src="{{asset('plugins/toastr/toastr.min.js')}}"></script>


<script type="text/javascript">

  $('#show').click(function(){
    var pass = document.getElementById('password')
    var atr = pass.getAttribute('type');

    console.log(atr)
     if (atr == 'password'){
      $('#password').prop('type','text')
    }
   else
    {
      $('#password').prop('type','password')
    }
   
  });


  @if(Session::has('Inactivo'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'error',
        title: 'Te encuentras Inhabilitado para iniciar sesion, comunicate con un Administrador'
      })
      })
    });
  @endif
  @if(Session::has('permision'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'error',
        title: 'No tienes Permisos'
      })
      })
    });
  @endif

   @if(Session::has('PwErr'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'error',
        title: 'Demasiados intentos fallidos, comunicate con un Administrador'
      })
      })
    });
  @endif
toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-bottom-center",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "800",
  "hideDuration": "1000",
  "timeOut": "12000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

  @if(Session::has('success'))
      toastr.success('{!! Session::get('success')!!}')
   @endif
   @if(Session::has('info'))
      toastr.info('{!! Session::get('info')!!}')
    @endif
    @if(Session::has('error'))
      toastr.error('{!! Session::get('error')!!}')
    @endif
   @if(Session::has('warning'))
      toastr.warning('{!! Session::get('warning')!!}')
    @endif
  

</script>
</body>
</html>