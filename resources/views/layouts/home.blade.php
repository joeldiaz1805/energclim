@extends ('/layouts/index')
@section ('content')

<style type="text/css">
    #map_zoom {
        display: none;
      }
</style>
<div class="content-wrapper">


    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Gerencia General de Energía y Climatización</h1>
            
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Principal</a></li>
              <li class="breadcrumb-item active">Gerencia General de Energía y Climatización</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          @if(Auth::user()->rol_id != 4)
            <div class="col-lg-3 col-6" >
              <!-- small box -->
              <div class="small-box bg-gradient-light text-dark">
                <div class="inner">
                  <h3>{{$totalEquipos}}</h3>

                  <p>Equipos Registrados</p>
                </div>
                <div class="icon">
                  <i class="fas fa-database" style="color: lightblue;"></i>
                </div>
                <a href="/inventario_region" id="openMenu" class="small-box-footer text-dark">Más información <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            @endif

          

          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-gradient-light text-dark">
              <div class="inner">
                <h3>{{$prevCount}}</h3>

                <p>Tickets por Planificacion </p>
              </div>
              <div class="icon">
                <i class="fas fa-edit" style="color: lightblue"></i>
              </div>
              <a href="/preventivos" class="small-box-footer text-dark">Más información<i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-gradient-light text-dark">
              <div class="inner">
                <h3>{{$corrCount}}</h3>

                <p>Tickets por Proyecto</p>
              </div>
              <div class="icon">
                <i class="fas fa-tools" style="color: lightblue"></i>
              </div>
              <a href="/correctivos" class="small-box-footer text-dark">Más información <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-gradient-light text-dark">
              <div class="inner">
                <h3>{{$insp}}</h3>

                <p>Ticket por inspeccion</p>
              </div>
              <div class="icon">
                <i class="fas fa-cog" style="color: lightblue"></i>
              </div>
              <a href="/inspeccion" class="small-box-footer text-dark">Más información <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>


          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-gradient-light text-dark">
              <div class="inner">
                <h3>{{$motoG}}</h3>

                <p>MotoG Registrados</p>
              </div>
              <div class="icon">
                <i class="fas fa-cogs" style="color: gray;"></i>
              </div>
              <a href="/motogenerador" id="openMenu" class="small-box-footer text-dark">Más información <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>

          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-gradient-light text-dark">
              <div class="inner">
                <h3>{{$AireA}}</h3>

                <p>Climatizaciòn Registrados</p>
              </div>
              <div class="icon">
                <i class="fas fa-thermometer-quarter" style="color: lightblue;"></i>
              </div>
              <a href="/aire_acondicionado" id="openMenu" class="small-box-footer text-dark">Más información <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>

          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-gradient-light text-dark">
              <div class="inner">
                <h3>{{$cuadroF}}</h3>

                <p>Cuadro de Fuerza Registrados</p>
              </div>
              <div class="icon">
                <i class="fas fa-charging-station" style="color: orange"></i>
              </div>
              <a href="/cuadro_fuerza" id="openMenu" class="small-box-footer text-dark">Más información <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>

          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-gradient-light text-dark">
              <div class="inner">
                <h3>{{$bancob}}</h3>

                <p>Banco Bateria</p>
              </div>
              <div class="icon">
                <i class="fas fa-car-battery"style="color: green;"></i>
                <!--<i class="fas fa-battery-full" style="color: green;"></i>-->
              </div>
              <a href="/bancob" id="openMenu" class="small-box-footer text-dark">Más información <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-7 connectedSortable">


            <!-- Map card -->
            <div class="card bg-gradient-primary">
              <div class="card-header border-0">
                <h7 class="card-title">
                  <i class="fas fa-map-marker-alt mr-1"></i>
                  Nivel de Combustible Nacional
                </h7>
                <!-- card tools -->
                <div class="card-tools">
                  <button type="button"
                          class="btn btn-primary btn-sm daterange"
                          data-toggle="tooltip"
                          title="Date range">
                    <i class="far fa-calendar-alt"></i>
                  </button>
                  <button type="button"
                          class="btn btn-primary btn-sm"
                          data-card-widget="collapse"
                          data-toggle="tooltip"
                          title="Collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
                <!-- /.card-tools -->
              </div>
             
              <div class="card-body">
                <div class="row" >
                  <div class="col-12 text-left" style="margin-top: -7%; margin-left: 7%;">
                    <h7> Faltantes:</h7>
                    <h7 class="text-red">{{$ltrsFaltantes}} L </h7>-
                    <h7>{{$porcentajeFaltante}}%</h7>
                  </div>
                </div>
                <!-- /.row -->
                <div id="map" style="height: 100%; width: 100%;"></div>
                <!--div id="map"></div-->
              </div>
              <!-- /.card-body-->
             
            </div>
            <!-- /.card -->
            <div class="card card-danger" style="display:none">
              <div class="card-header">
                <h3 class="card-title">Nivel de Combustible en Regiones</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <a href="/niveles_combustible_region">
                <div class="card-body">
                  <canvas id="donutChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                </div>
              </a>
            </div>

            <!-- Custom tabs (Charts with tabs)-->
           
            <!-- /.card -->

            <!-- DIRECT CHAT -->
          
            <!--/.direct-chat -->
             <div class="card" style="display:none">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-chart-pie mr-1"></i>
                 Tickets
               
                </h3>
                
                  <div class="card-tools">
                   <ul class="nav nav-pills ml-auto">
                    <li class="nav-item">
                      <a class="nav-link active" href="#revenue-chart" data-toggle="tab">Tickets Mensuales</a>
                    </li>

                    <li class="nav-item">
                      <a class="nav-link" href="#sales-chart" data-toggle="tab">Control de Produccion</a>
                    </li>
                  </ul>
                </div>
           
             
               
                </div>
              <!-- /.card-header -->
           
  <!-- graficos relacion de tikets -->
              <div class="card-body">
                <div class="tab-content p-0">
                  <!-- Morris chart - Sales -->

<!-- /.card-body ondas -->
                  <div class="chart tab-pane active" id="revenue-chart"
                       style="position: relative; height: 300px;">
                      <canvas id="revenue-chart-canvas" height="300" style="height: 300px;"></canvas>                         
                   </div>
<!-- /.card-body  tortas -->

                  


                </div>
              </div><!-- /.card-body -->
            </div>

            <!-- TO DO List -->
            <div class="card" style="display:none">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="ion ion-clipboard mr-1"></i>
                  To Do List
                </h3>

                <div class="card-tools">
                  <ul class="pagination pagination-sm">
                    <li class="page-item"><a href="#" class="page-link">&laquo;</a></li>
                    <li class="page-item"><a href="#" class="page-link">1</a></li>
                    <li class="page-item"><a href="#" class="page-link">2</a></li>
                    <li class="page-item"><a href="#" class="page-link">3</a></li>
                    <li class="page-item"><a href="#" class="page-link">&raquo;</a></li>
                  </ul>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <ul class="todo-list" data-widget="todo-list">
                  <li>
                    <!-- drag handle -->
                    <span class="handle">
                      <i class="fas fa-ellipsis-v"></i>
                      <i class="fas fa-ellipsis-v"></i>
                    </span>
                    <!-- checkbox -->
                    <div  class="icheck-primary d-inline ml-2">
                      <input type="checkbox" value="" name="todo1" id="todoCheck1">
                      <label for="todoCheck1"></label>
                    </div>
                    <!-- todo text -->
                    <span class="text">Design a nice theme</span>
                    <!-- Emphasis label -->
                    <small class="badge badge-danger"><i class="far fa-clock"></i> 2 mins</small>
                    <!-- General tools such as edit or delete-->
                    <div class="tools">
                      <i class="fas fa-edit"></i>
                      <i class="fas fa-trash-o"></i>
                    </div>
                  </li>
                  <li>
                    <span class="handle">
                      <i class="fas fa-ellipsis-v"></i>
                      <i class="fas fa-ellipsis-v"></i>
                    </span>
                    <div  class="icheck-primary d-inline ml-2">
                      <input type="checkbox" value="" name="todo2" id="todoCheck2" checked>
                      <label for="todoCheck2"></label>
                    </div>
                    <span class="text">Make the theme responsive</span>
                    <small class="badge badge-info"><i class="far fa-clock"></i> 4 hours</small>
                    <div class="tools">
                      <i class="fas fa-edit"></i>
                      <i class="fas fa-trash-o"></i>
                    </div>
                  </li>
                  <li>
                    <span class="handle">
                      <i class="fas fa-ellipsis-v"></i>
                      <i class="fas fa-ellipsis-v"></i>
                    </span>
                    <div  class="icheck-primary d-inline ml-2">
                      <input type="checkbox" value="" name="todo3" id="todoCheck3">
                      <label for="todoCheck3"></label>
                    </div>
                    <span class="text">Let theme shine like a star</span>
                    <small class="badge badge-warning"><i class="far fa-clock"></i> 1 day</small>
                    <div class="tools">
                      <i class="fas fa-edit"></i>
                      <i class="fas fa-trash-o"></i>
                    </div>
                  </li>
                  <li>
                    <span class="handle">
                      <i class="fas fa-ellipsis-v"></i>
                      <i class="fas fa-ellipsis-v"></i>
                    </span>
                    <div  class="icheck-primary d-inline ml-2">
                      <input type="checkbox" value="" name="todo4" id="todoCheck4">
                      <label for="todoCheck4"></label>
                    </div>
                    <span class="text">Let theme shine like a star</span>
                    <small class="badge badge-success"><i class="far fa-clock"></i> 3 days</small>
                    <div class="tools">
                      <i class="fas fa-edit"></i>
                      <i class="fas fa-trash-o"></i>
                    </div>
                  </li>
                  <li>
                    <span class="handle">
                      <i class="fas fa-ellipsis-v"></i>
                      <i class="fas fa-ellipsis-v"></i>
                    </span>
                    <div  class="icheck-primary d-inline ml-2">
                      <input type="checkbox" value="" name="todo5" id="todoCheck5">
                      <label for="todoCheck5"></label>
                    </div>
                    <span class="text">Check your messages and notifications</span>
                    <small class="badge badge-primary"><i class="far fa-clock"></i> 1 week</small>
                    <div class="tools">
                      <i class="fas fa-edit"></i>
                      <i class="fas fa-trash-o"></i>
                    </div>
                  </li>
                  <li>
                    <span class="handle">
                      <i class="fas fa-ellipsis-v"></i>
                      <i class="fas fa-ellipsis-v"></i>
                    </span>
                    <div  class="icheck-primary d-inline ml-2">
                      <input type="checkbox" value="" name="todo6" id="todoCheck6">
                      <label for="todoCheck6"></label>
                    </div>
                    <span class="text">Let theme shine like a star</span>
                    <small class="badge badge-secondary"><i class="far fa-clock"></i> 1 month</small>
                    <div class="tools">
                      <i class="fas fa-edit"></i>
                      <i class="fas fa-trash-o"></i>
                    </div>
                  </li>
                </ul>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <button type="button" class="btn btn-info float-right"><i class="fas fa-plus"></i> Add item</button>
              </div>
            </div>
            <!-- /.card -->
          </section>
          <!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

  <div class="card card-primary">
              <div class="card-header">
              <h3 class="card-title">Registro de Afectaciones del Mes "JUNIO - {{date("Y-m-d")}}"</h3>
              
            </div>
            
                <div class="card-body">
                  
                  <table id="" class=" display table table-bordered table-striped nowrap" style="width:100%">
                    <thead>
                      <tr align="center">
                        <th class="not-export-col">Descripcion</th>
                        <th>Cantidad</th>
                        <th>% </th>
                      </tr>
                    </thead>
                    <tbody>
                            
                      @foreach($result as $valores)
                      <tr align="center" >
                        <td>{{$valores['falla']}} </td>
                        <td>{{$valores['items']}} </td>
                        <td>{{round(($valores['items']* 100)/$contdor)}} %</td>
                           
                      </tr>
                      @endforeach
                    </tbody>
                    <tfoot align="center">
                  <tr>
                    <th>SubTotal:</th>
                    <th>{{$contdor}}</th>
                    <th>100%</th>
                  </tr>
                </tfoot>
                  </table>
                </div>
              

              <div class="card-body">
                <div class="d-flex">
                  <p class="d-flex flex-column">
                  </p>
                  <p class="ml-auto d-flex flex-column text-right">
                    
                    <span class="text-muted">Registros del ultimo mes</span>
                  </p>
                </div>
                <!-- /.d-flex -->

                <div class="position-relative mb-4">
                  <canvas id="sales-chart" height="200"></canvas>
                </div>

                <div class="d-flex flex-row justify-content-end">
                  <span class="mr-2">
                    <i class="fas fa-square text-primary"></i> Fallas Resueltas Registradas
                  </span>
                </div>
              </div>



              <div class="card-body">
                  
                  <table id="" class=" display table table-bordered table-striped nowrap" style="width:100%">
                    <thead>
                      <tr align="center">
                        <th class="not-export-col">Regiones</th>
                        <th>Afectacion Voz</th>
                        <th>Afectacion Aba</th>
                        <th>% </th>
                      </tr>
                    </thead>
                    <tbody>
                            
                      @foreach($result2 as $valores)
                      <tr align="center" >
                        <td>{{$valores['region']}} </td>
                        <td>{{$valores['impacto_voz']}} </td>
                        <td>{{$valores['impacto_aba']}} </td>
                        <td>{{round((($valores['impacto_voz'] +$valores['impacto_aba']) * 100)/$contdor_afectacion)}} %</td>
                           
                      </tr>
                      @endforeach
                    </tbody>
                    <tfoot align="center">
                        <tr>
                          <th>SubTotal:</th>
                          <th>{{$contdor_voz}}</th>
                          <th>{{$contdor_aba}}</th>
                          
                          <th>100%</th>
                        </tr>
                </tfoot>
                  </table>
                </div>



<div class="card-body">
  
              <div class="d-flex">
                  <p class="d-flex flex-column">
                  </p>
                  <p class="ml-auto d-flex flex-column text-right">
                    
                    <span class="text-muted">Afectaciones Vinculadas</span>
                  </p>
                </div>
                <!-- /.d-flex -->

                <div class="position-relative mb-4">
                  <canvas id="sales-chart2" height="200"></canvas>
                </div>

                <div class="d-flex flex-row justify-content-end">
                  <span class="mr-2">
                    <i class="fas fa-square text-primary"></i> Afectaciòn Voz
                    <i class="fas fa-square" style="color:#6a0a0a;"></i> Afectaciòn Aba
                  </span>
                </div>
              </div>
           
              <!-- /.card-footer-->
            </div>
           
          
       
            <!-- /.card -->

            <!-- Calendar -->
            <div class="card bg-gradient-success" style="display:none;">
              <div class="card-header border-0">

                <h3 class="card-title">
                  <i class="far fa-calendar-alt"></i>
                  Calendar
                </h3>
                <!-- tools card -->
                <div class="card-tools">
                  <!-- button with a dropdown -->
                  <div class="btn-group">
                    <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" data-offset="-52">
                      <i class="fas fa-bars"></i></button>
                    <div class="dropdown-menu" role="menu">
                      <a href="#" class="dropdown-item">Add new event</a>
                      <a href="#" class="dropdown-item">Clear events</a>
                      <div class="dropdown-divider"></div>
                      <a href="#" class="dropdown-item">View calendar</a>
                    </div>
                  </div>
                  <button type="button" class="btn btn-success btn-sm" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-success btn-sm" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
                <!-- /. tools -->
              </div>
              <!-- /.card-header -->
              <div class="card-body pt-0">
                <!--The calendar -->
                <div id="calendar" style="width: 100%"></div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </section>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>

      <input type="hidden" name="datos"  id="datos" value="{{$var}}">
  <input type="hidden" name="datos"  id="datos2" value="{{$var2}}">
  </div>
  @endsection

@section('vs')


<script>
  
  //-------------
    //- DONUT CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var donutChartCanvas = $('#donutChart').get(0).getContext('2d')
    var donutData        = {
      labels: [
          'Niveles de Combustible',
         
      ],
      datasets: [
        {
          data: [3000],
          backgroundColor : ['#f56954'],
        }
      ]
    }
    var donutOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    new Chart(donutChartCanvas, {
      type: 'doughnut',
      data: donutData,
      options: donutOptions
    })

 
  
  var salesChartData = {
    labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    datasets: [
      {

        label               : 'Tickets En Proceso',
        backgroundColor     : 'tansparent',
        borderColor         : '#f56954',
        pointBackgroundColor: '#f56954',
        pointColor          : '#f56954',
        pointStrokeColor    : '#f56954',
        pointHighlightFill  : '#f56954',
        pointHighlightStroke: '#f56954',
        data                : [{{$act['Enero']}},
                              {{$act['Febrero']}},
                              {{$act['Marzo']}},
                              {{$act['Abril']}},
                              {{$act['Mayo']}},
                              {{$act['Junio']}},
                              {{$act['Julio']}},
                              {{$act['Agosto']}},
                              {{$act['Septiembre']}},
                              {{$act['Octubre']}},
                              {{$act['Noviembre']}},
                              {{$act['Diciembre']}}
                              ],
        fill                : false
      },
      {
        label               : 'Tickets Resuelto',
        backgroundColor     : 'tansparent',
        borderColor         : '#00a65a',
        pointBackgroundColor: '#00a65a',
        pointColor          : '#00a65a',
        pointStrokeColor    : '#00a65a',
        pointHighlightFill  : '#00a65a',
        pointHighlightStroke: '#00a65a',
        data                : [{{$arrres['Enero']}},
                              {{$arrres['Febrero']}},
                              {{$arrres['Marzo']}},
                              {{$arrres['Abril']}},
                              {{$arrres['Mayo']}},
                              {{$arrres['Junio']}},
                              {{$arrres['Julio']}},
                              {{$arrres['Agosto']}},
                              {{$arrres['Septiembre']}},
                              {{$arrres['Octubre']}},
                              {{$arrres['Noviembre']}},
                              {{$arrres['Diciembre']}}
                               ],
        fill                : false
      },
            
    ]
  }

 var pieChartCanvas = $('#sales-chart-canvas').get(0).getContext('2d')

  var pieData        = {
    labels: [
        'Tickets en Proceso', 
        'Tickets Resueltos',
        
    ],
    datasets: [
      {
        data: [{{$actcount}}, {{$rescount}}],
        backgroundColor : ['#f56954', '#00a65a']
      }
    ]
  }
  var pieOptions = {
    legend: {
      display: true
    },
    maintainAspectRatio : false,
    responsive : true,
  }
  //Create pie or douhnut chart
  // You can switch between pie and douhnut using the method below.
  var pieChart = new Chart(pieChartCanvas, {
    type: 'doughnut',
    data: pieData,
    options: pieOptions      
  });




</script>

<script src="{{asset('dist/js/pages/dashboard3.js')}}"></script>

@endsection