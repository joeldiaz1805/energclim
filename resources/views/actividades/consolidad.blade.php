@extends ('/layouts/index')

@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Consolidado
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Facturas </a></li>
              <li class="breadcrumb-item active">Consolidado</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Facturas Pendientes por Pagar Ejercicio fiscal 2021</h3>
              
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class=" display nowrap  table table-bordered table-striped" style="width:100%">
                <thead>

                  <tr align="center">
                    <th>Proveedores</th>
                    <th>Monto Total Facturas Pagadas</th>
                    <th>Monto Total Facturas Pendiente por Pagar</th>
                    
                  </tr>
                </thead>
                <tbody>
                  <?php
                    foreach($proveedores as $proveedor){  
                      $Pagadas = App\Models\Models\facturas::wherestatus(1)->whereHas('contrato', function ($query) use($proveedor) {
                              return $query->where('proveedor_id',$proveedor->id);
                      })->get();

                      $Pendientes = App\Models\Models\facturas::wherestatus(2)->whereHas('contrato', function ($query) use($proveedor) {
                              return $query->where('proveedor_id',$proveedor->id);
                      })->get();
                    ?>
                  

                  <tr align="center"  id="tdId_{{$proveedor->id}}">
                    <td><?php echo $proveedor->nombre; ?></td>
                    <td><?php echo $Pagadas->sum('monto_factura') ; ?> </td>
                    <td><?php echo $Pendientes->sum('monto_factura') ;?> </td>
                  </tr>
            
                <?php }?>
                </tbody>
              </table>
           </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('scriptsalerts')


<script>

  
</script>

  <script>

    function deleteItem(item){
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'Â¿Desea Inhabilitar este usuario de la lista?',
      text: "Estas por Inhabilitar un usuario!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, Inhabilitarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {

      if (result.value) {



            $.ajax({
                url: "/facturas/"+item,
                type:"DELETE",
                   headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    

                    success:function(result, status, xhr){                    
                        
                        if (status == 'success') {

                          swalWithBootstrapButtons.fire(
                            'Inhabilitado con Exito!',
                            'Se Inhabilito con exito',
                            'success'
                          )

                          //$("#tdId_"+item).hide();
                          document.getElementById("intt").className="btn btn-danger";
                          //intt.style(color:green);
                           document.getElementById("act_"+item).innerHTML = "Inativo";
                          //$("#act_"+item).innerHTML = 'Inativo' ;



                        }
                    },
                    error:function(xhr, status, error){
                      

                      Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se Inhabilito ningun usuario '+error,
                        footer: '<a href>Seguramente hubo un problema de conexiÃ³n</a>'
                      })
                      

                    }
                });
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No se ha quitado el producto.',
          'error'
        )
      }
    })
  }
  function activeItem(item){
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'Â¿Desea Habilitar este usuario de la lista?',
      text: "Estas por Habilitar un usuario!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, Habilitarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {

      if (result.value) {



            $.ajax({
                url: "/Factura/"+item,
                type:"DELETE",
                   headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    

                    success:function(result, status, xhr){                    
                        
                        if (status == 'success') {

                          swalWithBootstrapButtons.fire(
                            'Habilitado con Exito!',
                            'Se HabilitÃ² con exito',
                            'success'
                          )

                          //$("#tdId_"+item).hide();
                          document.getElementById("intt").className="btn btn-success";
                          var h= document.getElementById("act_"+item);
                          h.innerHTML = "Activo";

                          //$("#act_"+item).innerHTML = 'Activo' ;



                        }
                    },
                    error:function(xhr, status, error){
                      

                      Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se Habilitado ningun usuario '+error,
                        footer: '<a href>Seguramente hubo un problema de conexiÃ³n</a>'
                      })
                      

                    }
                });
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No se ha Habilitado el usuario.',
          'error'
        )
      }
    })
  }
</script>


@endsection
