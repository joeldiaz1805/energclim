@extends ('/layouts/index')

@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Actividades
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Actividades </a></li>
              <li class="breadcrumb-item active">lista</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Vista Preliminar de Registros de Actividades</h3>
              <div align="right">
                  <a href="/actividades/create" class="btn btn-success">Agregar Actividad</a>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class=" display nowrap  table table-bordered table-striped" style="width:100%">
                <thead>

                  <tr align="center">
                    <th class="not-export-col">Acciones</th>
                    <th>Fecha de Inicio</th>
                    <th>Fecha de Fin</th>
                    <th>Localidad</th>
                    <th>Actividad</th>
                    <th>Descripción</th>
                    <th>Responsable</th>
                    <th>Prioridad</th>
                    <th>Status</th>
                    <th>Departamento y/o Gerencia</th>
                    <th>Ultima Actualizacion</th>
                    <th>Usuario</th>
                  </tr>
                </thead>
                <tbody>
                 @foreach($actividades as $actividad)
                  <tr align="center"  id="tdId_{{$actividad->id}}" @if($actividad->status == 2) style="background-color:yellow;color: black" @elseif($actividad->status == 1) style="background-color:green; color: black" @elseif($actividad->status == 3) style="background-color:orange; color: black" @else style="background-color:red; color: black;"  @endif>
                    <td>
                     <a href="/actividades/{{encrypt($actividad->id)}}/edit" class="btn btn-warning"><i class="far fa-edit"></i></a>
                      <!--a href="/actividads/{{encrypt( $actividad->id)}}" class="btn btn-success"><i class="far fa-eye"></i></a-->
                     <a onclick="deleteItem('{{encrypt($actividad->id)}}')" class="btn btn-danger"><i id="ele"  class="fa fa-user-times"></i></a>
                      
                      </td>
                    <td>{{$actividad->fecha_inicio}}</td>
                    <td>{{$actividad->fecha_fin}}</td>
                    <td>{{$actividad->localidad->nombre}}</td>
                    <td>{{$actividad->actividad}}</td>
                    <td>{{$actividad->descripcion}}</td>
                    <td>@foreach($actividad->responsables as $respons)
                      {{$respons->nombre }} {{$respons->apellido }}-
                      @endforeach
                    </td>
                    <td>@if($actividad->prioridad == 1 ) Baja @elseif($actividad->prioridad == 2) Media @else Alta @endif</td>
                    <td>@if($actividad->status == 2) Por Realizar  @elseif($actividad->status == 1) Realizada @elseif($actividad->status == 3) En Proceso @else Cancelada @endif</td>
                    <td>{{$actividad->departamento}}</td>
                    <td>{{$actividad->updated_at}}</td>
                    <td>{{$actividad->usuario->name ?? 'nobody'}} </td>
                  </tr>
                  @endforeach
                
                </tbody>
              </table>
           </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('scriptsalerts')


<script>

  
</script>

  <script>

    function deleteItem(item){
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'Â¿Desea Inhabilitar este usuario de la lista?',
      text: "Estas por Inhabilitar un usuario!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, Inhabilitarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {

      if (result.value) {



            $.ajax({
                url: "/actividades/"+item,
                type:"DELETE",
                   headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    

                    success:function(result, status, xhr){                    
                        
                        if (status == 'success') {

                          swalWithBootstrapButtons.fire(
                            'Eliminada Actividad con Exito!',
                            'Se Eliminò con exito',
                            'success'
                          )

                          $("#tdId_"+item).hide();
                        }
                    },
                    error:function(xhr, status, error){
                      

                      Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se Inhabilito ningun usuario '+error,
                        footer: '<a href>Seguramente hubo un problema de conexiÃ³n</a>'
                      })
                      

                    }
                });
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No se ha quitado el producto.',
          'error'
        )
      }
    })
  }
  function activeItem(item){
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'Â¿Desea Habilitar este usuario de la lista?',
      text: "Estas por Habilitar un usuario!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, Habilitarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {

      if (result.value) {



            $.ajax({
                url: "/Actividades/"+item,
                type:"DELETE",
                   headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    

                    success:function(result, status, xhr){                    
                        
                        if (status == 'success') {

                          swalWithBootstrapButtons.fire(
                            'Habilitado con Exito!',
                            'Se HabilitÃ² con exito',
                            'success'
                          )

                          //$("#tdId_"+item).hide();
                          document.getElementById("intt").className="btn btn-success";
                          var h= document.getElementById("act_"+item);
                          h.innerHTML = "Activo";

                          //$("#act_"+item).innerHTML = 'Activo' ;



                        }
                    },
                    error:function(xhr, status, error){
                      

                      Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se Habilitado ningun usuario '+error,
                        footer: '<a href>Seguramente hubo un problema de conexiÃ³n</a>'
                      })
                      

                    }
                });
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No se ha Habilitado el usuario.',
          'error'
        )
      }
    })
  }
</script>


@endsection
