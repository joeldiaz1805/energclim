@extends ('/layouts/index')

@section ('content')
<div class="content-wrapper">
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>
            Registrar Nueva Actividad
          </h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/Actividads">Actividad</a></li>
            <li class="breadcrumb-item active">Crear </li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <div class="row ">
      <div class="col s12 m12 l12 ">
        @if ($errors->any())
          <div class="alerta-errores">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif
      </div>
    </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12 col-sm-12">
          <div class="card card-primary card-tabs">
            <div class="card-body">
              <form method="POST" id="formulario_Actividad" name="formulario_Actividad"  action="{{ url('/actividades')}}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" value="{{Auth::user()->id}}" name="user_id">
                <div class="col-md-12">
		              <div class="card card-success">
				              <div class="card-header">
				                <h3 class="card-title">Nueva Actividad</h3>
                     </div>
			              <div class="card-body">
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="form-group">
                            <label>Región:</label>
                              <select  name="region_id" id="region" class="form-control">
                                <option>Seleccione una Región</option>
                                @foreach($regiones as $region)
                                  <option value="{{$region->id}}">{{$region->nombre}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Estado:</label>
                              <select name="estado_id" id="estado" class="form-control">
                                <option value="">Seleccione un estado</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Central:</label>
                                <select name="localidad_id" id="central" class="form-control">
                                  <option value="">Seleccione una Localidad</option>
                                </select>
                            </div>
                          </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="form-group">
                            <label>Departamentos:</label>
                            <select name="departamento" class="form-control">
                              <option value="">Seleccione una Gerencia</option>
                              <option value="Coordinación Normas, Análisis y Diseño">Coordinación Normas, Análisis y Diseño</option>
                              <option value="Coordinación Seguimiento y Control de Gestión">Coordinación Seguimiento y Control de Gestión</option>
                              <option value="Coordinación Seguimiento y Control de Gastos">Coordinación Seguimiento y Control de Gastos</option>
                              <option value="Coordinación Seguimiento y Control de Inversión">Coordinación Seguimiento y Control de Inversión</option>
                              <option value="Cosec">Cosec</option>
                              <option value="Gerencia General">Gerencia General</option>
                              <option value="Gerencia de Seguimiento y Control">Gerencia de Seguimiento y Control</option>
                              <option value="Gcia. Cost. Energía y Climatización">Gcia. Cost. Energía y Climatización</option>
                              <option value="Gcia. Oper. Mtto Reg. Capital">Gcia. Oper. Mtto Reg. Capital</option>
                              <option value="Gcia. Oper. Mtto Reg. Centro Oriente">Gcia. Oper. Mtto Reg. Central</option>
                              <option value="Gcia. Oper. Mtto Reg. Guayana">Gcia. Oper. Mtto Reg. Guayana</option>
                              <option value="Gcia. Oper. Mtto Reg. Centro Oriente">Gcia. Oper. Mtto Reg. Centro Oriente</option>
                              <option value="Gcia. Oper. Mtto Reg. Centro Occidente">Gcia. Oper. Mtto Reg. Centro Occidente</option>
                              <option value="Gcia. Oper. Mtto Reg. Occidente">Gcia. Oper. Mtto Reg. Occidente</option>
                              <option value="Gcia. Oper. Mtto Reg. Los Andes">Gcia. Oper. Mtto Reg. Los Andes</option>
                              <option value="Gcia. de Proyectos e Ingeniería">Gcia. de Proyectos e Ingeniería</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="form-group">
                            <label>Fecha Inicio Actividad</label>
                            <input type="date" name="fecha_inicio" id="fechaInicio" required class="form-control" placeholder="Fecha inicio de Actividad">
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <label>Fecha fin Actividad</label> 
                            <input type="date" name="fecha_fin" id="fechaFin" onblur="calcular()" required class="form-control" placeholder="Fecha fin de Actividad">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="form-group">
                            <label>Actividad</label>
                            <input type="text" name="actividad" required class="form-control" placeholder="Actividad">
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <label>Prioridad</label>
                          <select name="prioridad" class="form-control">
                            <option value="1">Baja</option>
                            <option value="2">Media</option>
                            <option value="3">Alta</option>
                          </select>
                        </div>
                        <div class="col-sm-4">
                          <div class="form-group">
                            <label>Status de la Actividad</label>
                              <select name="status" id="status" class="form-control">
                                <option value="">Status</option>
                                    <option value="1">Realizada</option>
                                    <option value="2">Por Realizar</option>
                                    <option value="3">En Proceso</option>
                                    <option value="4">Cancelada</option>
                              </select>
                           </div>
                         </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div align="right">
                            <a href="javascript:void(0);" id="add_button" class="btn btn-warning" title="Otro responsable"><i class="fas fa-plus"></i></a>
                            <a href="/personal/create" class="btn btn-primary"> <i class="fa fa-plush"></i>Registrar</a>
                          </div>
                          <div class="card-header">
                            <h3 class="card-title">Responsables</h3>
                          </div>

                        </div>
                      </div>
                      <div class="row" id="field_wrapper">
                        <div class="col-sm-8">
                          <div class="form-group">
                            <label>Responsable</label>
                              <select name="respon[]" id="responsable" class="form-control select2">
                                @foreach($responsables as $personal)
                                  <option value="{{$personal->id}}">{{$personal->nombre }} {{$personal->apellido}}</option>
                                @endforeach
                              </select>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="comment">Describe la actividad a realizar:</label>
                            <textarea class="form-control" rows="3" name="descripcion" id="comment"></textarea>      
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
     			      </div>
                <div class="card-footer" align="right">
          		    <button type="submit"  id="boton_crear"  class="btn btn-primary">Crear</button>
  			        </div>
              </form>
            </div>
	        </div>
	      </div>
      </div>
    </div>
	</section>
</div>
@endsection


@section('vs')
  @if(isset($validator))
     {!! $validator->selector("#formulario_Actividad") !!}
  @endif



<script type="text/javascript">
   var addButton = $('#add_button'); //Add button selector
    var wrapper = $('#field_wrapper'); //Input field wrapper
    var fieldHTML = '<div class="col-sm-8"><div class="form-group"><label>Responsable</label><select name="respon[]" id="responsable" class="form-control select2"><a href="/personal">registrar</a>@foreach($responsables as $personal)<option value="{{$personal->id}}">{{$personal->nombre }} {{$personal->apellido}}</option>@endforeach</select><a href="javascript:void(0);" id="remove_button" class="remove_button" title="Remove field"><i class="fas fa-trash" style="color:red;"></i></a</div></div>'; //New input field html 
    $(addButton).click(function(){ //Once add button is clicked
        
            $(wrapper).append(fieldHTML); // Add field html
        
    });
    $(wrapper).on('click', '#remove_button', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });




   function calcular(){

    var finicio =moment(document.getElementById('fechaInicio').value);
    var ffin =moment(document.getElementById('fechaFin').value);
    var dias = ffin.diff(finicio,'days');
    console.log(ffin.diff(finicio,'days'),'dias');

  


     if(dias < 0){
        toastr.error('Fecha Fin de Actividad debe ser igual o Mayor que fecha Inicio');
        $('#crearboton_crear').prop('disabled',true);
      }else{
        
        $('#boton_crear').prop('disabled',false);
      }




 }

  function buscar(){
    var codigo = document.getElementById('cod_proveedor').value
    var nombre = document.getElementById('nombre_proveedor')
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    console.log(codigo)

    $.get(APP_URL+'Actividads_buscar_proveedor/'+codigo, function (cidades) {
          console.log(cidades);
         if (cidades == false){
            toastr.error('Codigo de Proveedor no encontrado');
            nombre.value = "No encontrado";
            $('select[id=nro_contrato]').empty();

         }else{
            nombre.value = cidades.nombre;
            $('select[id=nro_contrato]').empty();
                if(cidades.contratos.length != 0){
                    $('select[id=nro_contrato]').append('<option value=""> Selecciona un Contrato</option>');
                    $.each(cidades.contratos, function (key, value) {
                        $('select[id=nro_contrato]').append('<option value=' + value.id + '>'+ value.nro_contrato + '</option>');
                    });
                }

         }
         // $('#monto_Actividad').prop('disabled',false);

        });

    };

  $('select[id=nro_contrato]').change(function () {
    var nro_contrato = $(this).val();
    var monto = document.getElementById('monto_Actividad').value
    var sumatoria = document.getElementById('sumatoria');
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'Actividads_suma/'+nro_contrato, function (cidades) {
          console.log(cidades)
          sumatoria.value = cidades;
          $('#monto_Actividad').prop('disabled',false);

        });

    });
$(document).ready(function(){



  


  $('select[id=region]').change(function () {
    var region_id = $(this).val();
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'estado/' + region_id, function (cidades) {
      console.log('hole')
        $('select[id=estado]').empty();
        if(cidades.length != 0){
            $('select[id=estado]').append('<option value=""> Selecciona un estado</option>');
            $.each(cidades, function (key, value) {
                $('select[id=estado]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });

    });


  $('select[id=estado]').change(function () {
      //console.log('hola')
    var estado_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'central/' + estado_id, function (cidades) {
        $('select[id=central]').empty();

        if(cidades.length != 0){
            $('select[id=central]').append('<option value=""> Selecciona un central</option>');
            $.each(cidades, function (key, value) {
                $('select[id=central]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });

    });


  });
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
});



  
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });

</script>
@endsection