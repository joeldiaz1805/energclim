@extends ('/layouts/index')

@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Inventario total por Regiòn 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Monitoreo</a></li>
              <li class="breadcrumb-item active">Inventario</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Registro de Inventario</h3>
              <!-- <div align="right">
                  <a href="/tickets/create" class="btn btn-success">Nuevo Ticket</a>
              </div> -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class=" display table table-bordered table-striped nowrap" style="width:100%">
                <thead>
                  <tr align="center">
                    <th>Region</th>
                    <th>MG</th>
                    <th>A/A</th>
                    <th>CUADRO DE FUERZA</th>
                    <th>UPS</th>
                    <th>BANCO BATERIAS</th>
                    
                    
                  </tr>
                </thead>
                <tbody>
                
                  <?php
                  foreach($regiones as $region){
                        
                        $motor = App\Models\Models\motogeneradorPivot::whereHas('motor', function ($query) use($region) {
                          $query->whereHas('ubicacion',function($query2) use($region){
                              $query2->whereHas('central',function($query3) use($region){
                                  return $query3->where('region_id',$region->id);

                                });
                              });
                           })->get();

                         $aa = App\Models\Models\AireA::whereHas('ubicacion',function($query2) use($region){
                            $query2->whereHas('central',function($query3) use($region){
                                return $query3->where('region_id',$region->id);

                            });
                      
                         })->get();



                         $ups1 = App\Models\Models\ups_bancob::whereHas('ubicacion',function($query2)  use($region){
                            $query2->whereHas('central',function($query3)  use($region){
                                return $query3->where('region_id',$region->id);
                            });
                        })->get();

                         $rectificadores = App\Models\Models\rect_bcobb::whereHas('ubicacion',function($query2) use($region){
                            $query2->whereHas('central',function($query3) use($region){
                                return $query3->where('region_id',$region->id);
                            });
                        })->get();

                        $bancob = App\Models\Models\bancob::whereHas('ubicacion',function($query2) use($region){
                            $query2->whereHas('central',function($query3) use($region){
                                return $query3->where('region_id',$region->id);
                            });
                        })->get();


                    
                    ?>
                  <tr align="center" >
                    <td><?php echo $region->nombre; ?>  </td>
                    <td><?php echo $motor->count(); ?> </td>
                    <td><?php echo $aa->count(); ?> </td>
                    <td><?php echo $rectificadores->count(); ?> </td>
                    <td><?php echo $ups1->count(); ?> </td>
                    <td><?php echo $bancob->count(); ?> </td>

                  </tr>
                <?php } ?>
                  
                </tbody>
                <tfoot align="center">
                  <tr>
                    <th>SubTotal:</th>
                    <th>{{$mg->count()}}</th>
                    <th>{{$aire->count()}}</th>
                    <th>{{$rect->count()}}</th>
                    <th>{{$ups->count()}}</th>
                    <th>{{$bb->count()}}</th>
                  </tr>
                </tfoot>
              </table>
           </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('scriptsalerts')


<script>
  

   @if(Session::has('exito'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Ups Guardado con exito'
      })
      })
    });
  @endif
   @if(Session::has('actualizado'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Datos actualizados con exito'
      })
      })
    });
  @endif
  
</script>

  <script>
    function deleteItem(item){
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Desea eliminar este ups del intentario?',
      text: "Estas por quitar este equipo del Inventario!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, quitarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {


      if (result.value) {



            $.ajax({
                url: "/ups/"+item,
                type:"DELETE",
                   headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    

                    success:function(result, status, xhr){                    
                        
                        if (status == 'success') {

                          swalWithBootstrapButtons.fire(
                            'Quitado con Exito!',
                            'Se eliminó con exito',
                            'success'
                          )

                          $("#tdId_"+item).hide();
                         // txt.innerHTML = result ;



                        }
                    },
                    error:function(xhr, status, error){
                      

                      Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se eliminó el producto, vuelva a intentarlo mas tarde '+error,
                        footer: '<a href>Seguramente hubo un problema de conexión</a>'
                      })
                      

                    }
                });
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No se ha quitado el producto.',
          'error'
        )
      }
    })
  }
</script>


@endsection