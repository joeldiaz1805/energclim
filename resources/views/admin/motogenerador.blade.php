@extends ('/layouts/index')

@section ('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Motogenerador 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inventario</a></li>
              <li class="breadcrumb-item active">Motogenerador</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <div class="row ">
	    <div class="col s12 m12 l12 ">
	        @if ($errors->any())
	            <div class="alerta-errores">
	                <ul>
	                    @foreach ($errors->all() as $error)
	                        <li>{{ $error }}</li>
	                    @endforeach
	                </ul>
	            </div>
	        @endif
	    </div>
	</div>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
     
        <div class="row">
          <div class="col-12 col-sm-12">
          	<div class="card card-primary card-tabs">

              <div class="card-body">
                
				    <form method="POST" id="formulario_mg" name="formulario_mg"  action="{{ url('/motogenerador')}}">
                        {{ csrf_field() }}
              	 				<input type="hidden" name="motor[user_id]" value="{{Auth::user()->id}}">


                    @if($parametros != null)
                    <?php

                    	$localidad = App\Models\Models\localidades::where('id',decrypt($parametros['url']))->first();
                    	# code...
                    
                    ?>
                    <input type="hidden" name="parametro" value="{{$parametros['url']}}">
  	 				<input type="hidden" name="central[estado_id]" value="{{$localidad->estado_id}}">
  	 				<input type="hidden" name="central[region_id]" value="{{$localidad->estados->region_id}}">
  	 				<input type="hidden" name="central[localidad_id]" value="{{decrypt($parametros['url'])}}">

  	 				@endif
                        <!--Inicio sección Ubicación y Responsable-->

                        <div class="col-md-12" @if($parametros != null) style="display: none" @endif>
			            <div class="card card-success">
			              	<div class="card-header">
			                	<h3 class="card-title">Ubicación y Responsable</h3>
			              	</div>
			              <!-- /.card-header -->
			              	<div class="card-body">
			               
			                	<div class="card-header">
					                <h3 class="card-title">Ubicación</h3>
					            </div><br>
			                  	<div class="row" @if($parametros != null) style="display: none" @endif >
			                  		<div class="col-sm-4">
			                      <!-- text input -->
			                      	<div class="form-group">

			                        	<label>Región:</label>
			                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Región','Seleccione región donde está ubicado el equipo', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        	<select  @if($parametros != null) disabled @endif name="central[region_id]" id="region"  required class="form-control">
				                        		<option> Seleccione una Región</option>
				                        		@foreach($regiones as $region)
					                        		<option value="{{$region->id}}">{{$region->nombre}}</option>
					                        	@endforeach
					                    	</select>
				                    </div>
			                      </div>
			                      <div class="col-sm-4">
			                      <!-- text input -->
				                      <div class="form-group">

				                        <label>Estado:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Estado','Seleccione estado donde está ubicado el equipo', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <select  @if($parametros != null) disabled @endif name="central[estado_id]" id="estado" required class="form-control">
			                            	<option value="">Selecciona un estado</option>
												
						                       
				                    	</select>
				                      </div>
			                    	</div>
			                  		<div class="col-sm-4">
			                      <!-- text input -->
				                      	<div class="form-group">

				                        	<label>Central:</label>
				                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Central','Seleccione central donde está ubicado el equipo', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
					                        	<select   @if($parametros != null) disabled @endif name="central[localidad_id]" id="central" required  class="form-control">

					                            	<option  value="">Selecciona una central</option>
												
						                    	</select>
					                    </div>
			                    	</div>
			                 </div>
			                 <div class="row">
			                 	<div class="col-sm-4">
			                      <!-- text input -->
			                      <div class="form-group">

			                        <label>Piso:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Piso','Introduzca piso donde se encuentra el equipo. Solo números sin letras', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        <select name="ubicacion[piso]" id="box_ModeloRectificador" class="form-control">
					                        		<option>Seleccione ubicación piso</option>
						                        	<option value="AZOTEA">AZOTEA</option>
													<option value="PH">PH</option>
													<option value="MZ">MZ</option>
													<option value="PB">PB</option>
													<option value="ST">ST</option>
													<option value="S1">S1</option>
													<option value="S2">S2</option>
													<option value="S3">S3</option>
													<option value="Unico piso">Unico piso</option>
													<option value="P1">P1</option>
													<option value="P2">P2</option>
													<option value="P3">P3</option>
													<option value="P4">P4</option>
													<option value="P5">P5</option>
													<option value="P6">P6</option>
													<option value="P7">P7</option>
													<option value="P8">P8</option>
													<option value="P9">P9</option>
													<option value="P10">P10</option>
													<option value="P11">P11</option>
													<option value="P12">P12</option>
													<option value="P13">P13</option>
													<option value="P14">P14</option>
													<option value="P15">P15</option>
													<option value="P16">P16</option>
													<option value="P17">P17</option>
													<option value="P18">P18</option>
													<option value="P19">P19</option>
													<option value="P20">P20</option>
													<option value="P21">P21</option>
													<option value="TERRAZA">TERRAZA</option>
												</select>

			                      </div>
			                    </div>

			                    <div class="col-sm-4">
			                      <!-- text input -->
			                      <div class="form-group">

			                        <label>Sala o espacio físico:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Piso','Indique sala o espacio físico donde se encuentra el equipo', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        <select name="ubicacion[sala]" id="box_ModeloRectificador" class="form-control">
					                        		<option>Seleccione una sala</option>
					                        		<option value="ABA">ABA</option>
													<option value="ADSL">ADSL</option>
													<option value="BANCO DE BATERIA">BANCO DE BATERIA</option>
													<option value="CENTRO DE DISTRIBUCION">CENTRO DE DISTRIBUCION</option>
													<option value="CX">CX</option>
													<option value="DATA CENTER">DATA CENTER</option>
													<option value="DIGITAL">DIGITAL</option>
													<option value="DP">DP</option>
													<option value="DSLAM">DSLAM</option>
													<option value="DX">DX</option>
													<option value="FURGON">FURGON</option>
													<option value="LIBRE">LIBRE</option>
													<option value="MG">MG</option>
													<option value="OAC">OAC</option>
													<option value="OFICINA ADMINISTRATIVA">OFICINA ADMINISTRATIVA</option>
													<option value="OPSUT">OPSUT</option>
													<option value="OUTDOOR">OUTDOOR</option>
													<option value="PCM">PCM</option>
													<option value="PSTN">PSTN</option>
													<option value="RECTIFICADORES">RECTIFICADORES</option>
													<option value="SSP">SSP</option>
													<option value="TX">TX</option>
													<option value="TX INTERNACIONAL">TX INTERNACIONAL</option>
													<option value="UMA">UMA</option>
													<option value="UNICA">UNICA</option>
													<option value="UPS">UPS</option>
													<option value="VARIAS SALAS">VARIAS SALAS</option>
												</select>
			                      </div>
			                    </div>

			                    <div class="col-sm-4">
			                      <!-- text input -->
			                        <div class="form-group">

			                        	<label>Estructura:</label>
			                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Estructura','Donde está alojado el equipo', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        	<select name="ubicacion[estructura]" required class="form-control">
				                        		<option>Seleccione tipo de estructura</option>
				                            	<option value="Fija">CENTRAL CRITICA</option>
					                        	<option  value="CENTRAL FIJA">CENTRAL FIJA</option>
					                        	<option  value="URL">URL</option>
					                        	<option  value="NODO INDOOR">NODO INDOOR</option>
					                        	<option  value="NODO OUTDOOR">NODO OUTDOOR</option>
					                        	<option  value="GTI">GTI</option>
					                        	<option  value="OPSUT">OPSUT</option>
					                        	<option  value="SUPER AULA">SUPER AULA</option>
					                        	<option  value="OAC">OAC</option>
					                        	<option  value="DLC">DLC</option>
					                        	<option  value="NO CANTV">NO CANTV</option>
					                    	</select>
				                    </div>
			                 	</div>
			                 	<div class="col-sm-4">
			                      <!-- text input -->
			                        <div class="form-group">

			                        	<label>Criticidad del Espacio:</label>
			                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Criticidad','Relacionado al espacio y condiciones ambientales donde está alojado el equipo', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        	<select name="ubicacion[criticidad_esp]" required class="form-control">
				                            	<option>Seleccione criticidad del espacio</option>
												<option value="Crítica">Crítica</option>
												<option value="Óptima">Óptima</option>
					                    	</select>
				                    </div>
			                 	</div>
			                 </div>
								<div class="card-header">
					               <h3 class="card-title">Responsable Atención Motogenerador</h3>
					            </div><br>
			                <div class="row" style="display: none" >
			                	<div class="col-sm-6">
			                		<div class="form-group d-line" required>
			                        	<label>Responsable ya regitrados:</label>
			                        	si
			                          	<input  type="radio" id="respo" value="true" name="respo">
			                           	no
			                          	<input  type="radio" id="respo1" value="false" name="respo">
			                          
			                          <br>
			                		</div>
			                	</div>
			                </div>
			                <div class="row" id="responsable_id" style="display: none" >
			                	<div class="col-sm-12">
				                	<div class="form-group" >
			                        	<label>Responsable: (ya regitrados)</label>
				                        	<select name="responsable_id[responsable_id]" required class="form-control">
				                            	@foreach($responsables as $pers)
				                            		<option value="{{$pers->id}}">{{$pers->cargo}}-{{$pers->nombre}}{{$pers->apellido}}</option>
												@endforeach
				                        		
					                    	</select>
				                    </div>
			                	</div>
			                </div>


			                <div id="personal">
			                	
			               
			                <div class="row" >
			                    
			                     <div class="col-sm-4">
			                      <!-- text input -->
			                      <div class="form-group">

			                        <label>Nombres:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Nombres','Nombre persona encargada y responsable del equipo', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        <input type="text" class="form-control"  required name="responsable[nombre]" placeholder="Ej: Pedro...">

			                      </div>
			                    </div>
			                    <div class="col-sm-4">
			                      <!-- text input -->
			                      <div class="form-group">
			                        <label>Apellidos:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Apellidos','Apellido persona encargada y responsable del equipo', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        <input type="text" class="form-control" required name="responsable[apellido]" placeholder="Ej: Tortoza...">

			                      </div>
			                    </div>
			                     <div class="col-sm-4">
			                      <!-- text input -->
			                      <div class="form-group">

			                        <label>Cédula:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cédula','Número de cédula', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        <input type="text" class="form-control"  required name="responsable[cedula]" placeholder="Introduzca cédula">

			                      </div>
			                    </div>
			                </div>
			                  
			                  	<div class="row">

			                  		<div class="col-sm-4">
			                      <!-- text input -->
				                      <div class="form-group">

				                        <label>P00:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','P00','Número interno que identifica al trabajador de la empresa. Ej. 112233', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" class="form-control"  required name="responsable[cod_p00]" placeholder="Introduzca P00">

				                      </div>
				                    </div>
			                  		<div class="col-sm-4">
			                      
				                      <div class="form-group">

				                        <label>Número de Oficina:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Número de Oficina','Número de Telf.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <div class="input-group">
						                    <div class="input-group-prepend">
						                      <span class="input-group-text"><i class="fas fa-phone"></i></span>
						                    </div>
						                    <input type="text" name="responsable[num_oficina]"  required  class="form-control" data-inputmask='"mask": "99999999999"' data-mask>
						                  </div>

				                      </div>
				                    </div>

				                     <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">

				                        <label>Número Personal:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Número Personal','Número de Telf.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                      <div class="input-group">
					                    <div class="input-group-prepend">
					                      <span class="input-group-text"><i class="fas fa-phone"></i></span>
					                    </div>
					                    <input type="text" name="responsable[num_personal]" required  class="form-control" data-inputmask='"mask": "99999999999"' data-mask>
					                  </div>
				                    </div>
				                      </div>
			                  		
			                  	</div>

			                  	<div class="row">
			                  		<div class="col-sm-4">
						                      <!-- text input -->
						                      <div class="form-group">

						                        <label>Cargo:</label>
						                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cargo','Denominación del cargo de la persona encargada del equipo.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
						                        <input type="text" class="form-control"  required name="responsable[cargo]" placeholder="Introduzca cargo">

						                      </div>
						                    </div>
			                  		<div class="col-sm-8">
			                      <!-- text input -->
			                      		<div class="form-group">

			                        		<label>Correo:</label>
			                        		<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Correo','Dirección de correo electrónico.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        		<input type="email" name="responsable[correo]"  required class="form-control" placeholder="Introduzca correo corporativo/personal">
			                        		
			                      		</div>
			                    	</div>
				              	</div>
							</div>
						</div>
					</div>
				</div>
			<!--Fin seccion Ubicacion y responsable-->
                     	<div class="col-md-12">
				            <!-- general form elements disabled -->
				            <div class="card card-success">
				              <div class="card-header">
				                <h3 class="card-title">Motor</h3>
				              </div>
				              <!-- /.card-header -->
				              <div class="card-body">
				                  <div class="row">
				                    <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">

				                        <label>Marca:</label>

				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Marca','Seleccionar fabricante del motogenerador', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>

				                        <select id="motor[marca]" required name="motor[marca]" class="form-control">
			                            	<option>Seleccione una marca</option>
											@foreach($marcas as $mar)
												<option value="{{$mar->descripcion}}">{{$mar->descripcion}}</option>
											@endforeach
										
				                    	</select>
				                      </div>
				                    </div>
				                    <div class="col-sm-4">
				                      <!-- text input -->
				                      	<div class="form-group">

				                        	<label>Modelo:</label>
				                        		<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Modelo Motor','Caracteres y números que lo identifican Ej. MT202011112525', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>

				                        		<input type="text" name="motor[modelo]" required class="form-control" placeholder="Introduzca modelo">
				                        		
					                    </div>
				                      </div>
				                   
									<div class="col-sm-4">
				                      <div class="form-group">

				                        	<label>Estado de Arranque:</label>

				                        		<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Estado de Arranque','Seleccione ¿cómo inicia el equipo?', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>

					                        	<select name="motor[estado_arranque]" required class="form-control">

					                            	<option>Seleccione modalidad de arranque</option>
													<option value="Automático">Automático</option>
													<option value="Manual">Manual</option>
													<option value="Inoperativo">Inoperativo</option>
						                    	</select>
					                    </div>
				                  </div>
				                 </div>

				                  <div class="row">
				                     <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">

				                        <label>Serial:</label>
				                        <input type="checkbox" name="no_posee" id="no_posee_motor">No Posee

				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Serial','Secuencia de números y/o caracteres', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>

				                        <input type="text" name="motor[serial]" id="serial_motor" required class="form-control" placeholder="Introduzca serial">

				                      </div>
				                    </div>
				                     <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">

				                        <label>Inventario Cantv:</label>

				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Inventario Cantv','Código interno de inventario en Cantv. Ej. 223344', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a> 

				                        <input type="text" name="motor[inventario_cant]" required class="form-control" placeholder="Introduzca Inventario CANTV">

				                      </div>
				                    </div>
				                     <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">

				                        <label>Capacidad tanque de aceite:</label>

				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Capacidad tanque de aceite','en litros, solo números sin letras', 300 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>

				                        <input type="text" name="motor[cap_tanq_aceite]" value="{{old('motor[cap_tanq_aceite]')}}" required class="form-control" placeholder="Introduzca capacidad tanque aceite">

				                      </div>
				                    </div>
				                  	</div>

				                  	<div class="row">
					                    <div class="col-sm-4">
					                      <!-- text input -->
					                      <div class="form-group">

					                        <label>Capacidad tanque de refrigerante:</label>

					                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Capacidad tanque de refrigerante','en litros, solo números sin letras', 300 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>

					                        <input type="text"  name="motor[cap_tanq_refrig]" required class="form-control" placeholder="Introduzca capacidad tanque refrigerante">

					                      </div>
					                    </div>
					                     <div class="col-sm-4">
					                      <!-- text input -->
					                      <div class="form-group">

					                        <label>Combustible Tanque Principal:</label>

					                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Combustible Tanque Principal','en litros, solo números sin letras', 300 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>

					                        <input type="text" name="motor[comb_tanq_princ]" required class="form-control" placeholder="Introduzca capacidad TPC">

					                      </div>
					                    </div>
					                     <div class="col-sm-4">
					                      <!-- text input -->
					                      <div class="form-group">


					                        <label>Cantidad de Filtros:</label>

					                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cantidad de filtros','solo números sin letras', 300 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>

					                        <input type="text" name="motor[cantidad_filtro]" required class="form-control" placeholder="Introduzca cantidad de filtros">

					                      </div>
					                    </div>
				                  	</div>



				                    <div class="row">
				                    	<div class="col-sm-4">
					                        <div class="form-group">

					                        	<label>Cantidad Baterias Arranque:</label>

					                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cantidad Baterias Arranque','Seleccione cantidad de baterías', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>

						                        	<select name="motor[cant_baterias]" required class="form-control">
						                        		<option value="0">Seleccione cantidad de baterías</option>
						                            	<option value="1">1</option>
							                        	<option value="2">2</option>
							                        	<option value="3">3</option>
							                        	<option value="4">4</option>
							                        	<option value="5">5</option>
							                    	</select>
						                    </div>
						                    <div class="col-sm-12">
						                      <div class="form-group">

						                        <label>Amperaje en batería de arranque:</label>

						                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Amperaje en batería de arranque','Introduzca un número. Ej. 20, 30,100, etc. , solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>

						                        <input type="text" name="motor[amp_bateria]" required class="form-control" placeholder="Introduzca amperaje en bateria(s) de arranque separadas por ',">

						                      </div>
						                    </div>
					                    </div>

					                    <div class="col-sm-4">
						                    <div class="form-group">
						                    	<label>Cargador de baterías:</label>

						                    	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cargador de baterías','¿Posee cargador de baterías?', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>

						                        <div class="form-check">
						                          <input class="form-check-input" type="radio" name="motor[carg_bat]" value="Si">
						                          <label class="form-check-label">Si</label>
						                        </div>
						                        <div class="form-check">
						                          <input class="form-check-input" type="radio" name="motor[carg_bat]" value="No">
						                          <label class="form-check-label">No</label>
						                        </div>
					                      	</div>
					                      	<div class="col-sm-12">
						                      <div class="form-group">

						                        <label>Cantidad:</label>

						                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cantidad','¿Cuántos cargadores posee?, solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>

						                        <input type="text" name="motor[cant_bat_arranq]" required class="form-control" placeholder="Introduzca cantidad de cargadores">

						                      </div>
						                    </div>
					                     </div>
					                     <div class="col-sm-4">
					                      <!-- text input -->
					                      <div class="form-group">


					                        <label>Modelo Filtros:</label>

					                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Modelo de Filtros','solo números sin letras', 300 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>

					                        <input type="text" name="motor[modelo_filtro]" required class="form-control" placeholder="Introduzca modelos de filtros">

					                      </div>
					                    </div>
					                    
					                 </div>
					                 <div class="row">
					                 	<div class="col-sm-12">
						                 	<div class="card-header">
						                		<h3 class="card-title">Bateria de Control </h3><h3 class="card-title" style="color: red">*Mantenga estos campos en blanco si No posee Bateria de Control*</h3>
							              		<div align="right">
							              			<input type="button" class="btn btn-success " value="+" id="add_field" >
							              		</div>
						              		</div>
					                 		
					                 	</div>
					                 	
					                 </div>
					                 <div  id="listas">
					                 	<div id="cpp">
							                <div class="row" id="cpp">
						                    	<div class="col-sm-4">
							                        <div class="form-group">
					    		                    	<label>Cantidad Baterias Control:</label>
					            		            	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cantidad Baterias Arranque','Seleccione cantidad de baterías', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        	<select name="bateriaControl[]" required class="form-control">
								                        		<option></option>
								                            	<option value="1">1</option>
									                        	<option value="2">2</option>
									                        	<option value="3">3</option>
									                        	<option value="4">4</option>
									                        	<option value="5">5</option>
									                    	</select>
						                    		</div>
						               			</div>
							                    <div class="col-sm-4">
							                      	<div class="form-group">
							                        	<label>Amperaje en batería de Control:</label>
							                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Amperaje en batería de arranque','Introduzca un número. Ej. 20, 30,100, etc. , solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                        	<input type="text" name="bateriaControl[]" required class="form-control" placeholder="Introduzca amperaje en bateria(s) de arranque separadas por ',">
							                      </div>
							                    </div>
							                    <div class="col-sm-4">
								                    <div class="form-group">
								                    	<label>Voltaje:</label>
							                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Amperaje en batería de arranque','Introduzca un número. Ej. 20, 30,100, etc. , solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                       <div class="form-check">
								                          <input class="form-check-input" type="radio" value="12v" name="bateriaControl[]">
								                          <label class="form-check-label">12v</label>
								                        </div>
								                        <div class="form-check">
								                          <input class="form-check-input" type="radio" name="bateriaControl[]" value="24v">
								                          <label class="form-check-label">24v</label>
								                        </div>
							                      	</div>
							                     </div>
					                 		</div>
					                 	</div>
					                 </div>
									<!--
				                 	 <div class="card-footer" align="right">
					                  <button type="submit" class="btn btn-primary">Guardar</button>
					                </div>-->
					              
				              </div>
				              <!-- /.card-body -->
				            </div>
				        </div>
                  	<div class="col-md-12">
			            <div class="card card-success">
			              <div class="card-header">
			                <h3 class="card-title">Generador</h3>
			              </div>
			              <!-- /.card-header -->
			              <div class="card-body">
			                
			                  <div class="row">
			                    <div class="col-sm-4">
			                      <!-- text input -->
			                      <div class="form-group">

			                        <label>Marca:</label>

			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Marca Generador','Seleccionar fabricante del generador', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>

			                        <select name="generador[marca]" required class="form-control">
		                            	<option>Seleccione una marca</option>
										<option value="AEG">AEG</option>
										<option value="CATERPILLAR">CATERPILLAR</option>
										<option value="DESCONOCIDO">DESCONOCIDO</option>
										<option value="DYNAF">DYNAF</option>
										<option value="LEROY SOMER">LEROY SOMER</option>
										<option value="MARATHON">MARATHON</option>
										<option value="HAWKER SIIDI">HAWKER SIIDI</option>
										<option value="GENERALING SET">GENERALING SET</option>
										<option value="LISTER">LISTER</option>
										<option value="SDMO">SDMO</option>
										<option value="POWER PLANT">POWER PLANT</option>
										<option value="MARATÒN">MARATÒN</option>
										<option value="DOOSAN">DOOSAN</option>
										<option value="ECC">ECC</option>
										<option value="KHD">KHD</option>
										<option value="KOHLER">KOHLER</option>
										<option value="ALTERNATEUS">ALTERNATEUS</option>
										<option value="MARELLI">MARELLI</option>
										<option value="MECC ALTER">MECC ALTER</option>
										<option value="NEWAGE">NEWAGE</option>
										<option value="OLYMPIAN">OLYMPIAN</option>
										<option value="ONAN">ONAN</option>
										<option value="PETBOW">PETBOW</option>
										<option value="SINCRO">SINCRO</option>
										<option value="STAGE">STAGE</option>
										<option value="STAMFORD">STAMFORD</option>
										<option value="STARLIGHT">STARLIGHT</option>
										<option value="WUXI NEWAGE">WUXI NEWAGE</option>
			                    	</select>
			                      </div>
			                    </div>
			                    <div class="col-sm-4">
			                      <!-- text input -->
			                      	<div class="form-group">

			                        	<label>Modelo:</label>

			                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Modelo Generador','Caracteres y números que lo identifican Ej. GE202011112525', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>

			                        		<input type="text" name="generador[modelo]" required class="form-control" placeholder="Introduzca modelo">
				                    </div>
			                      </div>
			                   
								<div class="col-sm-4">
			                        <div class="form-group">

			                        	<label>Serial:</label>
				                        <input type="checkbox" name="no_posee" id="no_posee_gen">No Posee
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Serial Generador','Secuencia de números y/o caracteres', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        	<input type="text" required class="form-control" id="serial_gen" name="generador[serial]" placeholder="Introduzca serial">

			                      	</div>
		                        </div>
			                 </div>

			                <div class="row">
			                    
			                     <div class="col-sm-4">
			                      <!-- text input -->
			                      <div class="form-group">

			                        <label>Inventario Cantv:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Inventario Cantv Generador','Código interno de inventario en Cantv. Ej. 223344', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        <input type="text" class="form-control" required name="generador[inventario_cant]" placeholder="Introduzca inventario CANTV">

			                      </div>
			                    </div>

			                     <div class="col-sm-4">
			                      <!-- text input -->
			                      <div class="form-group">

			                        <label>Capacidad KVA:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Capacidad KVA','kilovoltiamperio. Solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        <input type="number" class="form-control" required name="generador[cap_kva]" placeholder="Introduzca capacidad KVA">

			                      </div>
			                    </div>

			                    


			                  	</div>
						<div class="row">
								 <div class="col-sm-4">
						                    <div class="form-group">
						                    	<label>Tensión:</label>
						                    	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Tension','Marque el voltaje del equipo', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
						                        <div class="form-check">
						                          <input class="form-check-input" type="radio" value="208V" name="motor[fases]">
						                          <label class="form-check-label">208V</label>
						                        </div>
						                        <div class="form-check">
						                          <input class="form-check-input" type="radio" name="motor[fases]" value="416V">
						                          <label class="form-check-label">416V</label>
						                        </div>
						                        <div class="form-check">
						                          <input class="form-check-input" type="radio" name="motor[fases]" value="480V" >
						                          <label class="form-check-label">480V</label>
						                        </div>
					                      	</div>
					                     </div>
 							<div class="col-sm-4">
					                <div class="form-group">
					                  	<label>Tablero de Transferencia:</label>
					                  	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Tablero de Transferencia','¿Posee tablero de transferencia?', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                	    <div class="form-check">
				                          <input class="form-check-input" type="radio"  name="generador[tablero]"  value="Si">
				                          <label class="form-check-label">Si</label>
				                        </div>
				                        <div class="form-check">
				                          <input class="form-check-input" type="radio"  name="generador[tablero]" value="No" checked>
				                          <label class="form-check-label">No</label>
				                        </div>
				                    </div>
				                </div>
						</div>
			                  
			              </div>
			              <!-- /.card-body -->
			            </div>
			        </div>
			        <div class="col-md-12">
			            <div class="card card-success">
			              <div class="card-header">
			                <h3 class="card-title">Instalación Motogenerador</h3>
			              </div>
			              <!-- /.card-header -->
			              <div class="card-body">
			                <form role="form">
			                  <div class="row">
			                    <div class="col-sm-4">
			                      <!-- text input -->
			                      <div class="form-group">
			                        <label>Operatividad:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Operatividad','Seleccione el estado en que se encuentra el equipo', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a><!--End ShowHelp-->
			                        <select name="motor[operatividad]" id="operatividad" class="form-control">
		                            	<option>Indique operatividad del equipo</option>
																	<option value="Operativo">Operativo</option>
																	<option value="Inoperativo">Inoperativo</option>
																	<option value="Optimo">Optimo</option>
																	<option value="Critico">Critico</option>
																	<option value="Vandalizado">Vandalizado</option>
																	<option value="Deficiente">Deficiente</option>

									</select>
			                      </div>
			                    </div>
			                    <div class="col-sm-4">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Fecha de Instalación:</label>
			                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Fecha de Instalación','Introduzca fecha', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        	<!--End ShowHelp-->
				                        	<input id="box_ModeloRectificador" name="motor[fecha_instalacion]" class="form-control" type="date">
				                    </div>
			                      </div>
			                   
			                      <div class="col-sm-4">
			                      <!-- text input -->
			                      <div class="form-group">
			                        <label>Criticidad del Equipo:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Criticidad del Equipo','Selecione criticidad. Por Ej. para su atención inmediata o posterior', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a><!--End ShowHelp-->
			                        <select name="motor[criticidad]" class="form-control">
		                            	<option>Indique critidad del equipo</option>
										<option value="Alta">Alta</option>
										<option value="Media">Media</option>
										<option value="Baja">Baja</option>
									</select>
			                      </div>
			                    </div>
			                 </div>
			                 <div class="row" id="falla" style="display: none">
			                 	<div class="col-sm-4">
		                 			<label>Porcentaje Operatividad:</label>
		                 			<div class="form-group">
				                        <input name="motor[porc_falla]" onblur="falla()" id="input_falla" disabled type="text" value="">
			                 		</div>
			                 	</div>
			                 </div>
			                 <div class="row">
			                  	<div class="col-sm-4">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>¿Equipo en Garantía?</label>
			                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Garantía','¿Tiene o está vigente la garantía?', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a><!--End ShowHelp-->
				                        	<div class="form-check">
						                          <input class="form-check-input" type="radio" value="si" name="motor[garantia]">
						                          <label class="form-check-label">Si</label>
						                        </div>
						                        <div class="form-check">
						                          <input class="form-check-input" type="radio" value="no" name="motor[garantia]">
						                          <label class="form-check-label">No</label>
						                        </div>				                    
						                    </div>
			                    </div>
			                </div>

			                 <div class="form-group">
								<label for="comment">Observaciones:</label>
								<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Observaciones','Información complementaria sobre seguimientos e incidencias a destacar. Solo de la instalación del motogenerador', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a><!--End ShowHelp-->
								<textarea class="form-control" name="motor[observaciones]" rows="3" id="comment"></textarea>			
							</div>
			                  	<!--
			                 	 <div class="card-footer" align="right">
				                  <button type="submit" class="btn btn-primary">Guardar</button>
				                </div>-->
			                 	 <div class="card-footer" align="right">
				                  <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
				                </div>
				                </form>
				            
			              </div>
			              <!-- /.card-body -->
			            </div>
                    	
			              </div>
			              <!-- /.card-body -->
			            
			        </div>
                  </div>
                </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

@endsection
@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_mg") !!}
@endif



<script type="text/javascript">

	var campos_max  = 1;   //max de 10 campos
	var cp = document.getElementById('cpp').innerHTML
        var x = 0;
        $('#add_field').click (function(e) {
        	var btn = document.getElementById('add_field')
                e.preventDefault();     //prevenir novos clicks
                if (x < campos_max) {
                        $('#listas').append(
            			'<div class="row" id="cpp">\
						  	<div class="col-sm-4">\
							    <div class="form-group">\
					    		    <label>Cantidad Baterias Control:</label>\
					            	<a href="#" onmouseover="getMouseXY( event ); showHelp( "help_popup","Cantidad Baterias Arranque","Seleccione cantidad de baterías", 200 );" onmouseout="hideHelp("help_popup"); " onclick="return false;"><img align="absmiddle" src="http://localhost:8000/img/signo.gif" class="ico_err"></a>\
		                        	<select name="bateriaControl2[]" required="" class="form-control">\
		                        		<option></option>\
		                            	<option value="1">1</option>\
			                        	<option value="2">2</option>\
			                        	<option value="3">3</option>\
			                        	<option value="4">4</option>\
			                        	<option value="5">5</option>\
			                    	</select>\
						        </div>\
						    </div>\
							<div class="col-sm-4">\
								<div class="form-group">\
							        <label>Amperaje en batería de Control:</label>\
							        <a href="#" onmouseover="getMouseXY( event ); showHelp( "help_popup","Amperaje en batería de arranque","Introduzca un número. Ej. 20, 30,100, etc. , solo números sin letras", 200 );" onmouseout="hideHelp("help_popup"); " onclick="return false;"><img align="absmiddle" src="http://localhost:8000/img/signo.gif" class="ico_err"></a>\
							            <input type="text" name="bateriaControl2[]" required="" class="form-control" placeholder="Introduzca amperaje en bateria(s) de arranque separadas por ",">\
							    </div>\
							    </div>\
							    <div class="col-sm-4">\
				                    <div class="form-group">\
				                    	<label>Voltaje:</label>\
				                    	<a href="#"  onmouseover="getMouseXY( event ); showHelp( "help_popup","Tension","Marque el voltaje del equipo", 100 );" onmouseout="hideHelp("help_popup"); " onclick="return false;"><img align="absmiddle" src="{{asset("/img/signo.gif")}}" class="ico_err"></a>\
				                        <div class="form-check">\
				                          <input class="form-check-input" type="radio" value="12v" name="bateriaControl2[]">\
				                          <label class="form-check-label">12v</label>\
				                        </div>\
				                        <div class="form-check">\
				                          <input class="form-check-input" type="radio" name="bateriaControl2[]" value="24v">\
				                          <label class="form-check-label">24v</label>\
				                        </div>\
			                      	</div>\
			                     </div>\
					   </div>'
            				);
                        x++;
                }
                $('#add_field').hide();

        });
        // Remover o div anterior
        $('#listas').on("click",".remover_campo",function(e) {
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
        });

function falla(){
  	var falla = document.getElementById('input_falla')
  	var range = document.getElementById('range')
  	var opera = document.getElementById('operatividad')
  	console.log(falla.value)

  	range.value == falla.value
  	if(falla.value < 1){
  		opera.value == "Inoperativo"
  	}else if(falla.value>99){
  		opera.value == "Operativo"
  	}

  }
 $('select[id=operatividad]').change(function(){
 	var valor = $(this).val();
 	//console.log(valor)
 	if (valor == "Deficiente"){
 		$('#falla').show();
		$('#input_falla').prop('disabled',false);
 	}else{
		$('#falla').hide();
		$('#input_falla').prop('disabled',true);

 	}



 });




 $("#no_posee_motor").click(function(){
	var no_posee_motor = document.getElementById('no_posee_motor');
	var serial_motor = document.getElementById('serial_motor');
	if(no_posee_motor.checked){
        	//$('#serial_motor').hide();
        	serial_motor.value="no posee"
        	$('#serial_motor').prop('readonly', true);
 		}
 	if(!no_posee_motor.checked){
        	//$('#serial_motor').hide();
        	serial_motor.value=""
        	$('#serial_motor').prop('readonly', false);
 		}

 	});
 $("#no_posee_gen").click(function(){
	var no_posee_gen = document.getElementById('no_posee_gen');
	var serial_gen = document.getElementById('serial_gen');
	if(no_posee_gen.checked){
        	//$('#serial_gen').hide();
        	serial_gen.value="no posee"
        	$('#serial_gen').prop('readonly', true);
 		}
 	if(!no_posee_gen.checked){
        	//$('#serial_gen').hide();
        	serial_gen.value=""
        	$('#serial_gen').prop('readonly', false);
 		}

 	});


  $("#respo1").click(function(){
 		var a = document.getElementById('respo1');
 		if(a.checked){
        	$('#personal').show();
        	$('#responsable_id').hide();
 		}
     
	});
	 $("#respo").click(function(){
 		var a = document.getElementById('respo');
 		if(a.checked){
        	$('#personal').hide();
        	$('#responsable_id').show();
        	$('#responsable_nombre').prop('disabled', false);
        	$('#responsable_apellido').prop('disabled', false);
        	$('#responsable_cedula').prop('disabled', false);
        	$('#responsable_num_oficina').prop('disabled', false);
        	$('#responsable_cargo').prop('disabled', false);
        	$('#responsable_correo').prop('disabled', false);
 		}
	});




$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });	
  
   $(document).ready(function(){



  	$('select[id=region]').change(function () {
    	console.log('hola')
    var region_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'estado/' + region_id, function (cidades) {
        $('select[id=estado]').empty();

        if(cidades.length != 0){
            $('select[id=estado]').append('<option value=""> Selecciona un estado</option>');
            $.each(cidades, function (key, value) {
                $('select[id=estado]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });

    });


	$('select[id=estado]').change(function () {
    	//console.log('hola')
    var estado_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'central/' + estado_id, function (cidades) {
        $('select[id=central]').empty();

        if(cidades.length != 0){
            $('select[id=central]').append('<option value=""> Selecciona un central</option>');
            $.each(cidades, function (key, value) {
                $('select[id=central]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });

    });


	});
});
</script>
@endsection
