@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Motorgenerador 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inventario</a></li>
              <li class="breadcrumb-item active">Motorgenerador</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <div class="row ">
	    <div class="col s12 m12 l12 ">
	      @if ($errors->any())
	        <div class="alerta-errores">
	          <ul>
	            @foreach ($errors->all() as $error)
	              <li>{{ $error }}</li>
	              @endforeach
	          </ul>
	        </div>
	      @endif
	    </div>
		</div>
    <!-- Main content -->
    <section class="content">
	    <div class="container-fluid">
	      <div class="row">
    		  <div class="col-12 col-sm-12">
        		<div class="card card-primary card-tabs">
         			<div class="card-body">
				  			<form method="POST" id="formulario_mg" name="formulario_mg" action="/motogenerador/{{encrypt($motogenerador->id)}}">
									<input type="hidden" name="_method" value="PUT">
              	 	<input type="hidden" name="motor[user_id]" value="{{Auth::user()->id}}">
		                  @if($parametros != null)
			                  <input type="hidden" name="parametro" value="{{$parametros['url']}}">
						  	 			@endif
               	
			              <!-- /.card-body -->
			              		{{csrf_field()}}
	               	<div class="col-md-12">
						        <div class="card card-success">
			        		   	<div class="card-header">
			            			<h3 class="card-title">Ubicación y Responsable</h3>
			            		</div>
						      
						         	<div class="card-body">
						           	<div class="card-header">
								          <h3 class="card-title">Ubicación</h3>
					    		      </div>
					         			<br>
			                  
			                  <div class="row">
                					<div class="col-sm-4">
		                      	<div class="form-group">
				                    	<label>Región:</label>
                				    	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Región','Seleccione región donde está ubicado el equipo', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                      				<select id="region_id" name="central[region_id]" class="form-control">
		                        		@foreach($regiones as $region)
			                        		<option @if($region->id == $motogenerador->motor->ubicacion->central->localidad->estados->region->id) selected @endif value="{{$region->id}}">{{$region->nombre}}</option>
			                        	@endforeach
			                    		</select>
                    				</div>
			                    </div>
						               
	                      	<div class="col-sm-4">
		                      	<div class="form-group">
		                        	<label>Estado:</label>
		                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Estado','Seleccione estado donde está ubicado el equipo', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
		                        	<select id="estado_id" name="central[estado_id]" class="form-control">
	                            		<option value=""></option>
																		@foreach($estados as $estado)
				                       				<option @if($estado->id == $motogenerador->motor->ubicacion->central->estados->id) selected @endif value="{{$estado->id}}">{{$estado->nombre}}</option>
				                        		@endforeach
		                    			</select>
		                      	</div>
	                    		</div>
		                  		
		                  		<div class="col-sm-4">
		                      	<div class="form-group">
		                        	<label>Central:</label>
		                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Central','Seleccione central donde está ubicado el equipo', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
		                        	<select id="central_id" name="central[localidad_id]"  class="form-control">
																	@foreach($localidades as $central)
				                       		<option @if($central->id == $motogenerador->motor->ubicacion->central->localidad->id) selected @endif value="{{$central->id}}">{{$central->nombre}}</option>
				                        @endforeach
			                    		</select>
			                    	</div>
		                    	</div>
               					</div>
			                 				
			                 	<div class="row">
							            <div class="col-sm-4">
							              <div class="form-group">
			                        <label>Piso:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Piso','Introduzca piso donde se encuentra el equipo. Solo números sin letras', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        <select name="ubicacion[piso]" id="box_ModeloRectificador" class="form-control">
		                        		<option>Seleccione ubicación piso</option>
			                        	<option @if($motogenerador->motor->ubicacion->piso=="AZOTEA") selected @endif value="AZOTEA">AZOTEA</option>
																<option @if($motogenerador->motor->ubicacion->piso=="PH") selected @endif value="PH">PH</option>
																<option @if($motogenerador->motor->ubicacion->piso=="MZ") selected @endif value="MZ">MZ</option>
																<option @if($motogenerador->motor->ubicacion->piso=="PB") selected @endif value="PB">PB</option>
																<option @if($motogenerador->motor->ubicacion->piso=="ST") selected @endif value="ST">ST</option>
																<option @if($motogenerador->motor->ubicacion->piso=="S1") selected @endif value="S1">S1</option>
																<option @if($motogenerador->motor->ubicacion->piso=="S2") selected @endif value="S2">S2</option>
																<option @if($motogenerador->motor->ubicacion->piso=="S3") selected @endif value="S3">S3</option>
																<option @if($motogenerador->motor->ubicacion->piso=="Unicopiso") selected @endif value="Unico piso">Unico piso</option>
																<option @if($motogenerador->motor->ubicacion->piso=="P1") selected @endif value="P1">P1</option>
																<option @if($motogenerador->motor->ubicacion->piso=="P2") selected @endif value="P2">P2</option>
																<option @if($motogenerador->motor->ubicacion->piso=="P3") selected @endif value="P3">P3</option>
																<option @if($motogenerador->motor->ubicacion->piso=="P4") selected @endif value="P4">P4</option>
																<option @if($motogenerador->motor->ubicacion->piso=="P5") selected @endif value="P5">P5</option>
																<option @if($motogenerador->motor->ubicacion->piso=="P6") selected @endif value="P6">P6</option>
																<option @if($motogenerador->motor->ubicacion->piso=="P7") selected @endif value="P7">P7</option>
																<option @if($motogenerador->motor->ubicacion->piso=="P8") selected @endif value="P8">P8</option>
																<option @if($motogenerador->motor->ubicacion->piso=="P9") selected @endif value="P9">P9</option>
																<option @if($motogenerador->motor->ubicacion->piso=="P10") selected @endif value="P10">P10</option>
																<option @if($motogenerador->motor->ubicacion->piso=="P11") selected @endif value="P11">P11</option>
																<option @if($motogenerador->motor->ubicacion->piso=="P12") selected @endif value="P12">P12</option>
																<option @if($motogenerador->motor->ubicacion->piso=="P13") selected @endif value="P13">P13</option>
																<option @if($motogenerador->motor->ubicacion->piso=="P14") selected @endif value="P14">P14</option>
																<option @if($motogenerador->motor->ubicacion->piso=="P15") selected @endif value="P15">P15</option>
																<option @if($motogenerador->motor->ubicacion->piso=="P16") selected @endif value="P16">P16</option>
																<option @if($motogenerador->motor->ubicacion->piso=="P17") selected @endif value="P17">P17</option>
																<option @if($motogenerador->motor->ubicacion->piso=="P18") selected @endif value="P18">P18</option>
																<option @if($motogenerador->motor->ubicacion->piso=="P19") selected @endif value="P19">P19</option>
																<option @if($motogenerador->motor->ubicacion->piso=="P20") selected @endif value="P20">P20</option>
																<option @if($motogenerador->motor->ubicacion->piso=="P21") selected @endif value="P21">P21</option>
																<option @if($motogenerador->motor->ubicacion->piso=="TERRAZA") selected @endif value="TERRAZA">TERRAZA</option>
															</select>
							              </div>
							            </div>
							            
							            <div class="col-sm-4">
		                      	<div class="form-group">
			                        <label>Sala o espacio físico:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Piso','Indique sala o espacio físico donde se encuentra el equipo', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        <select name="ubicacion[sala]" id="box_ModeloRectificador" class="form-control">
		                        		<option>Seleccione una sala</option>
		                        		<option @if($motogenerador->motor->ubicacion->sala=="ABA") selected @endif value="ABA">ABA</option>
																<option @if($motogenerador->motor->ubicacion->sala=="ADSL") selected @endif value="ADSL">ADSL</option>
																<option @if($motogenerador->motor->ubicacion->sala=="BANCO DE BATERIA") selected @endif value="BANCO DE BATERIA">BANCO DE BATERIA</option>
																<option @if($motogenerador->motor->ubicacion->sala=="CENTRO DE DISTRIBUCION") selected @endif value="CENTRO DE DISTRIBUCION">CENTRO DE DISTRIBUCION</option>
																<option @if($motogenerador->motor->ubicacion->sala=="CX") selected @endif value="CX">CX</option>
																<option @if($motogenerador->motor->ubicacion->sala=="DATA CENTER") selected @endif value="DATA CENTER">DATA CENTER</option>
																<option @if($motogenerador->motor->ubicacion->sala=="DIGITAL") selected @endif value="DIGITAL">DIGITAL</option>
																<option @if($motogenerador->motor->ubicacion->sala=="DP") selected @endif value="DP">DP</option>
																<option @if($motogenerador->motor->ubicacion->sala=="DSLAM") selected @endif value="DSLAM">DSLAM</option>
																<option @if($motogenerador->motor->ubicacion->sala=="DX") selected @endif value="DX">DX</option>
																<option @if($motogenerador->motor->ubicacion->sala=="FURGON") selected @endif value="FURGON">FURGON</option>
																<option @if($motogenerador->motor->ubicacion->sala=="LIBRE") selected @endif value="LIBRE">LIBRE</option>
																<option @if($motogenerador->motor->ubicacion->sala=="MG") selected @endif value="MG">MG</option>
																<option @if($motogenerador->motor->ubicacion->sala=="OAC") selected @endif value="OAC">OAC</option>
																<option @if($motogenerador->motor->ubicacion->sala=="OFICINA ADMINISTRATIVA") selected @endif value="OFICINA ADMINISTRATIVA">OFICINA ADMINISTRATIVA</option>
																<option @if($motogenerador->motor->ubicacion->sala=="OPSUT") selected @endif value="OPSUT">OPSUT</option>
																<option @if($motogenerador->motor->ubicacion->sala=="OUTDOOR") selected @endif value="OUTDOOR">OUTDOOR</option>
																<option @if($motogenerador->motor->ubicacion->sala=="PCM") selected @endif value="PCM">PCM</option>
																<option @if($motogenerador->motor->ubicacion->sala=="PSTN") selected @endif value="PSTN">PSTN</option>
																<option @if($motogenerador->motor->ubicacion->sala=="RECTIFICADORES") selected @endif value="RECTIFICADORES">RECTIFICADORES</option>
																<option @if($motogenerador->motor->ubicacion->sala=="SSP") selected @endif value="SSP">SSP</option>
																<option @if($motogenerador->motor->ubicacion->sala=="TX") selected @endif value="TX">TX</option>
																<option @if($motogenerador->motor->ubicacion->sala=="TX INTERNACIONAL") selected @endif value="TX INTERNACIONAL">TX INTERNACIONAL</option>
																<option @if($motogenerador->motor->ubicacion->sala=="UMA") selected @endif value="UMA">UMA</option>
																<option @if($motogenerador->motor->ubicacion->sala=="UNICA") selected @endif value="UNICA">UNICA</option>
																<option @if($motogenerador->motor->ubicacion->sala=="UPS") selected @endif value="UPS">UPS</option>
																<option @if($motogenerador->motor->ubicacion->sala=="VARIAS SALAS") selected @endif value="VARIAS SALAS">VARIAS SALAS</option>
															</select>
							              </div>
							            </div>
							            
							            <div class="col-sm-4">
		                        <div class="form-group">
		                        	<label>Estructura:</label>
		                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Estructura','Donde está alojado el equipo', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
		                        	<select name="ubicacion[estructura]"   class="form-control">
																<option @if($motogenerador->motor->ubicacion->estructura=="Fija") selected @endif  value="Fija">Fija</option>
																<option @if($motogenerador->motor->ubicacion->estructura=="Móvil") selected @endif value="Móvil">Móvil</option>
																<option @if($motogenerador->motor->ubicacion->estructura=="URL") selected @endif value="URL">URL</option>
								              </select>
								            </div>
							            </div>
							            
				                 	<div class="col-sm-4">
		                        <div class="form-group">
		                        	<label>Criticidad del Espacio:</label>
		                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Criticidad','Relacionado al espacio y condiciones ambientales donde está alojado el equipo', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
		                        	<select name="ubicacion[criticidad_esp]" class="form-control">
	                            	<option value=""></option>
																<option  @if($motogenerador->motor->ubicacion->criticidad_esp=="Crítica") selected @endif value="Crítica">Crítica</option>
																<option  @if($motogenerador->motor->ubicacion->criticidad_esp=="Óptima") selected @endif value="Óptima">Óptima</option>
			                    		</select>
			                    	</div>
				                 	</div>
                 				</div>

												<div class="card-header">
								          <h3 class="card-title">Responsable Motogenerador</h3>
								        </div>
								        <br>
							          
							          <div class="row">
			                    <div class="col-sm-4">
			                      <div class="form-group">
			                        <label>Nombres:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Nombres','Nombre persona encargada y responsable del equipo', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        <input type="text" class="form-control" name="responsable[nombre]"  value="{{$motogenerador->motor->ubicacion->responsable->nombre}}" placeholder="Enter ...">
			                      </div>
			                    </div>
			                    <div class="col-sm-4">
			                      <!-- text input -->
			                      <div class="form-group">
			                        <label>Apellidos:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Apellidos','Apellido persona encargada y responsable del equipo', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        <input type="text" class="form-control" name="responsable[apellido]" value="{{$motogenerador->motor->ubicacion->responsable->apellido}}" placeholder="Enter ...">
			                      </div>
			                    </div>
			                    <div class="col-sm-4">
			                      <!-- text input -->
			                      <div class="form-group">
			                        <label>Cédula:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cédula','Número de cédula', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        <input type="text" class="form-control" name="responsable[cedula]" value="{{$motogenerador->motor->ubicacion->responsable->cedula}}"placeholder="Enter ...">
			                      </div>
			                    </div>
              					</div>
			                  <!---->
			                  <div class="row">
			                  	<div class="col-sm-4">
			                     	<div class="form-group">
				                      <label>P00:</label>
				                      <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','P00','Número interno que identifica al trabajador de la empresa. Ej. 112233', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                      <input type="text" class="form-control" name="responsable[cod_p00]" value="{{$motogenerador->motor->ubicacion->responsable->cod_p00}}" placeholder="Enter ...">
			                     	</div>
				                  </div>
			                  
			                  	<div class="col-sm-4">
	                      		<div class="form-group">
			                        <label>Número de Oficina:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Número de Oficina','Número de Telf.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        <div class="input-group">
						                    <div class="input-group-prepend">
						                      <span class="input-group-text"><i class="fas fa-phone"></i></span>
						                    </div>
					                    	<input type="text" name="responsable[num_oficina]"  value="{{$motogenerador->motor->ubicacion->responsable->num_oficina}}"  class="form-control" data-inputmask='"mask": "99999999999"' data-mask>
					                  	</div>
			                      </div>
				                  </div>

			                    <div class="col-sm-4">
		                      	<div class="form-group">
		                        	<label>Número Personal:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Número Personal','Número de Telf.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                      	<div class="input-group">
					                    	<div class="input-group-prepend">
					                      		<span class="input-group-text"><i class="fas fa-phone"></i></span>
					                    	</div>
				                    		<input type="text" name="responsable[num_personal]"   class="form-control" value="{{$motogenerador->motor->ubicacion->responsable->num_personal}}" data-inputmask='"mask": "99999999999"' data-mask>
				                  		</div>
		                      	</div>
			                    </div>
						            </div>
		                  	
		                  	<div class="row">
		                  		<div class="col-sm-4">
				                    <div class="form-group">
				                        <label>Cargo:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cargo','Denominación del cargo de la persona encargada del equipo.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" class="form-control"  name="responsable[cargo]" value="{{$motogenerador->motor->ubicacion->responsable->cargo}}" placeholder="Introduzca cargo">
				                    </div>
			                    </div>
		                  		<div class="col-sm-8">
	                      		<div class="form-group">
	                        		<label>Correo:</label>
	                        		<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Correo','Dirección de correo electrónico.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
	                        		<input type="text" class="form-control" name="responsable[correo]" value="{{$motogenerador->motor->ubicacion->responsable->correo}}"  placeholder="Enter ...">
	                      		</div>
	                      	</div>
		                    </div>
						          </div>
			              </div>
			          	</div>

		              <div class="col-md-12">
				            <div class="card card-success">
				              <div class="card-header">
				                <h3 class="card-title">Motor</h3>
				              </div>
					              
			              	<div class="card-body">
		                  	<div class="row">
			                    <div class="col-sm-4">
			                      <div class="form-group">
			                        <label>Marca:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Marca','Seleccionar fabricante del motogenerador', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        <select name="motor[marca]" value="{{$motogenerador->motor->marca}}" class="form-control">
		                            	<option>Seleccione una marca</option>
		                            	@foreach($marcas as $mar)
																	<option @if($mar->descripcion == $motogenerador->motor->marca) selected @endif value="{{$mar->descripcion}}">{{$mar->descripcion}}</option>
																@endforeach
										
			                    	</select>
			                      </div>
			                    </div>
			                   
			                    <div class="col-sm-4">
		                      	<div class="form-group">
		                        	<label>Modelo:</label>
		                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Modelo Motor','Caracteres y números que lo identifican Ej. MT202011112525', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
	                            <input type="text" name="motor[modelo]"  class="form-control" value="{{$motogenerador->motor->modelo}}" placeholder="Introduzca modelo">
				                    </div>
			                    </div>
			                   
													<div class="col-sm-4">
			                      <div class="form-group">
			                        <label>Estado Arranque:</label>
		                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Estado de Arranque','Seleccione ¿cómo inicia el equipo?', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        	<select name="motor[estado_arranque]" class="form-control">
																	<option @if($motogenerador->motor->estado_arranque=="Automático") selected @endif value="Automático">Automático</option>
																	<option @if($motogenerador->motor->estado_arranque=="Manual") selected @endif value="Manual">Manual</option>
																	<option @if($motogenerador->motor->estado_arranque=="Inoperativo") selected @endif value="Inoperativo">Inoperativo</option>
				                    	</select>
				                    </div>
			                  	</div>
			                 	</div>

		                  	<div class="row">
			                    <div class="col-sm-4">
				                    <div class="form-group">
			                       	<label>Serial:</label>
				                      <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Serial','Secuencia de números y/o caracteres', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" name="motor[serial]" value="{{$motogenerador->motor->serial}}" class="form-control" placeholder="Enter ...">
			                        </div>
			                    </div>
			                    
			                    <div class="col-sm-4">
		                      	<div class="form-group">
			                        <label>Inventario Cantv:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Inventario Cantv','Código interno de inventario en Cantv. Ej. 223344', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a> 
				                        <input type="text" name="motor[inventario_cant]" value="{{$motogenerador->motor->inventario_cant}}" class="form-control" placeholder="Enter ...">
			                      </div>
			                    </div>
			                    
			                    <div class="col-sm-4">
			                      <div class="form-group">
			                       	<label>Capacidad Tanque Aceite:</label>
			                       	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Capacidad tanque de aceite','en litros, solo números sin letras', 300 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                       	<input type="text" name="motor[cap_tanq_aceite]" value="{{$motogenerador->motor->cap_tanq_aceite}}" class="form-control" placeholder="Enter ...">
			                      </div>
			                    </div>
		                  	</div>

			                  	<div class="row">
				                    <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Capacidad Tanque Refrigerante:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Capacidad tanque de refrigerante','en litros, solo números sin letras', 300 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text"  name="motor[cap_tanq_refrig]" value="{{$motogenerador->motor->cap_tanq_refrig}}" class="form-control" placeholder="Enter ...">
				                      </div>
				                    </div>
				                     <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Combustible Tanque Principal :</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Combustible Tanque Principal','en litros, solo números sin letras', 300 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" name="motor[comb_tanq_princ]" value="{{$motogenerador->motor->comb_tanq_princ}}" class="form-control" placeholder="Enter ...">
				                      </div>
				                    </div>
				                     <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">


				                        <label>Cantidad de Filtros:</label>

				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cantidad de filtros','solo números sin letras', 300 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>

				                        <input type="text" name="motor[cantidad_filtro]" value="{{$motogenerador->motor->cantidad_filtro}}"  class="form-control" placeholder="Introduzca capacidad TDC">

				                      </div>
				                    </div>
			                  	</div>



			                    <div class="row">
			                    	<div class="col-sm-4">
				                        <div class="form-group">
				                        	<label>Cantidad Baterias Arranque:</label>
				                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cantidad Baterias Arranque','Seleccione cantidad de baterías', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
					                        	<select name="motor[cant_baterias]" class="form-control">
					                            	<option @if($motogenerador->motor->cant_baterias==1) selected @endif value="1">1</option>
						                        	<option @if($motogenerador->motor->cant_baterias==2) selected @endif value="2">2</option>
						                        	<option @if($motogenerador->motor->cant_baterias==3) selected @endif value="3">3</option>
						                        	<option @if($motogenerador->motor->cant_baterias==4) selected @endif value="4">4</option>
						                        	<option @if($motogenerador->motor->cant_baterias==5) selected @endif value="5">5</option>
						                    	</select>
					                    </div>
					                    <div class="col-sm-12">
					                      <div class="form-group">
					                        <label>Amperaje Batería Arranque:</label>
					                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Amperaje en batería de arranque','Introduzca un número. Ej. 20, 30,100, etc. , solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
					                        <input type="text" name="motor[amp_bateria]" value="{{$motogenerador->motor->amp_bateria}}" class="form-control" placeholder="Enter ...">
					                      </div>
					                    </div>
				                    </div>

				                    <div class="col-sm-4">
					                    <div class="form-group">
					                    	<label>Cargador de baterías:</label>
					                    	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cargador de baterías','¿Posee cargador de baterías?', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
					                        <div class="form-check">
					                          <input class="form-check-input" @if($motogenerador->motor->carg_bat=='Si') checked @endif type="radio" name="motor[carg_bat]" value="Si">
					                          <label class="form-check-label">Si</label>
					                        </div>
					                        <div class="form-check">
					                          <input class="form-check-input" type="radio" @if($motogenerador->motor->carg_bat=='No') checked @endif name="motor[carg_bat]" value="No">
					                          <label class="form-check-label">No</label>
					                        </div>
				                      	</div>
				                      	<div class="col-sm-12">
					                      <div class="form-group">
					                        <label>Cantidad:</label>
					                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cantidad','¿Cuántos cargadores posee?, solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
					                        <input type="text" name="motor[cant_bat_arranq]" value="{{$motogenerador->motor->cant_bat_arranq}}" class="form-control" placeholder="Enter ...">
					                      </div>
					                    </div>
				                    </div>
				                    <div class="col-sm-4">
				                      <!-- text input -->
				                      	<div class="form-group">
					                        <label>Modelo Filtros:</label>
					                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Modelo de Filtros','solo números sin letras', 300 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
					                        <input type="text" name="motor[modelo_filtro]" value="{{$motogenerador->motor->modelo_filtro}}"  class="form-control" placeholder="Introduzca capacidad TDC">
				                      	</div>
				                    </div>
				                	</div>
				                <div class="row">
				                 	<div class="col-sm-12">
					                 	<div class="card-header">
					                		<h3 class="card-title">Bateria de Control</h3>
					              		</div>
				                 		
				                 	</div>
				                 	
				                 </div>


				                 <div  id="listas">
				                 	@foreach($motogenerador->motor->batteryctrl as $batctrl)
				                 	<div id="cpp">
						                <div class="row" id="cpp">
					                    	<div class="col-sm-4">
						                        <div class="form-group">
				    		                    	<label>Cantidad Baterias Control:</label>
				            		            	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cantidad Baterias Arranque','Seleccione cantidad de baterías', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                        	<select name="bateriaControl{{$batctrl->id}}[cantidad]"  class="form-control">
							                        		<option></option>
							                            	<option @if($batctrl->cantidad==1) selected @endif value="1">1</option>
								                        	<option @if($batctrl->cantidad==2) selected @endif value="2">2</option>
								                        	<option @if($batctrl->cantidad==3) selected @endif value="3">3</option>
								                        	<option @if($batctrl->cantidad==4) selected @endif value="4">4</option>
								                        	<option @if($batctrl->cantidad==5) selected @endif value="5">5</option>
								                    	</select>
					                    		</div>
					               			</div>
						                    <div class="col-sm-4">
						                      	<div class="form-group">
						                        	<label>Amperaje en batería de Control:</label>
						                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Amperaje en batería de arranque','Introduzca un número. Ej. 20, 30,100, etc. , solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
						                        	<input type="text" name="bateriaControl{{$batctrl->id}}[amperaje]" value="{{$batctrl->amperaje}}"  class="form-control" placeholder="Introduzca amperaje en bateria(s) de arranque separadas por ',">
						                      </div>
						                    </div>
						                    <div class="col-sm-4">
							                    <div class="form-group">
							                    	<label>Voltaje:</label>
						                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Amperaje en batería de arranque','Introduzca un número. Ej. 20, 30,100, etc. , solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                       <div class="form-check">
							                          <input class="form-check-input"  @if($batctrl->voltaje=="12v") checked @endif type="radio" value="12v" name="bateriaControl{{$batctrl->id}}[voltaje]">
							                          <label class="form-check-label">12v</label>
							                        </div>
							                        <div class="form-check">
							                          <input class="form-check-input"  @if($batctrl->voltaje=="24v") checked @endif type="radio" name="bateriaControl{{$batctrl->id}}[voltaje]" value="24v">
							                          <label class="form-check-label">24v</label>
							                        </div>
						                      	</div>
						                     </div>
				                 		</div>
				                 	</div>
				                 	@endforeach
				                 </div>
			              	</div>
						             
						        </div>
						      </div>
			            
			            <div class="col-md-12">
				            <div class="card card-success">
					            <div class="card-header">
					               <h3 class="card-title">Generador</h3>
					            </div>
						          
						          <div class="card-body">
		                  	<div class="row">
			                    <div class="col-sm-4">
		                      	<div class="form-group">
		                        	<label>Marca:</label>
		                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Marca Generador','Seleccionar fabricante del generador', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        <select name="generador[marca]" class="form-control">
																<option @if($motogenerador->generador->marca=="AEG") selected @endif value="AEG">AEG</option>
																<option @if($motogenerador->generador->marca=="CATERPILLAR") selected @endif value="CATERPILLAR">CATERPILLAR</option>
																<option @if($motogenerador->generador->marca=="DESCONOCIDO") selected @endif  value="DESCONOCIDO">DESCONOCIDO</option>
																<option @if($motogenerador->generador->marca=="DYNAF") selected @endif  value="DYNAF">DYNAF</option>
																<option @if($motogenerador->generador->marca=="LEROY SOMER") selected @endif  value="LEROY SOMER">LEROY SOMER</option>
																<option @if($motogenerador->generador->marca=="MARATHON") selected @endif value="MARATHON">MARATHON</option>
																<option @if($motogenerador->generador->marca=="HAWKER SIIDI") selected @endif value="HAWKER SIIDI">HAWKER SIIDI</option>
																<option @if($motogenerador->generador->marca=="GENERALING SET") selected @endif value="GENERALING SET">GENERALING SET</option>
																<option @if($motogenerador->generador->marca=="LISTER") selected @endif value="LISTER">LISTER</option>
																<option @if($motogenerador->generador->marca=="POWER PLANT") selected @endif value="POWER PLANT">POWER PLANT</option>
																<option @if($motogenerador->generador->marca=="MARATÒN") selected @endif value="MARATÒN">MARATÒN</option>
																<option @if($motogenerador->generador->marca=="ECC") selected @endif value="ECC">ECC</option>
																<option @if($motogenerador->generador->marca=="KHD") selected @endif value="KHD">KHD</option>
																<option @if($motogenerador->generador->marca=="KOHLER") selected @endif value="KOHLER">KOHLER</option>
																<option @if($motogenerador->generador->marca=="ALTERNATEUS") selected @endif value="ALTERNATEUS">ALTERNATEUS</option>
																<option @if($motogenerador->generador->marca=="MARELLI") selected @endif  value="MARELLI">MARELLI</option>
																<option @if($motogenerador->generador->marca=="MECC ALTER") selected @endif  value="MECC ALTER">MECC ALTER</option>
																<option @if($motogenerador->generador->marca=="NEWAGE") selected @endif value="NEWAGE">NEWAGE</option>
																<option @if($motogenerador->generador->marca=="OLYMPIAN") selected @endif value="OLYMPIAN">OLYMPIAN</option>
																<option @if($motogenerador->generador->marca=="ONAN") selected @endif value="ONAN">ONAN</option>
																<option @if($motogenerador->generador->marca=="PETBOW") selected @endif value="PETBOW">PETBOW</option>
																<option @if($motogenerador->generador->marca=="SINCRO") selected @endif value="SINCRO">SINCRO</option>
																<option @if($motogenerador->generador->marca=="STAGE") selected @endif value="STAGE">STAGE</option>
																<option @if($motogenerador->generador->marca=="STAMFORD") selected @endif value="STAMFORD">STAMFORD</option>
																<option @if($motogenerador->generador->marca=="STARLIGHT") selected @endif value="STARLIGHT">STARLIGHT</option>
																<option @if($motogenerador->generador->marca=="WUXI NEWAGE") selected @endif value="WUXI NEWAGE">WUXI NEWAGE</option>
								              </select>
							              </div>
							            </div>
			                    
			                    <div class="col-sm-4">
		                      	<div class="form-group">
		                        	<label>Modelo:</label>
		                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Modelo Generador','Caracteres y números que lo identifican Ej. GE202011112525', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
		                        	<input type="text" name="generador[modelo]"  class="form-control" value="{{$motogenerador->generador->modelo}}" placeholder="Introduzca modelo">
				                    </div>
			                    </div>
							                   
													<div class="col-sm-4">
		                        <div class="form-group">
		                        	<label>Serial:</label>
		                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Serial Generador','Secuencia de números y/o caracteres', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
		                        	<input type="text" class="form-control" value="{{$motogenerador->generador->serial}}" name="generador[serial]" placeholder="Enter ...">
		                      	</div>
	                        </div>
						            </div>
				               
				                <div class="row">
			                    <div class="col-sm-4">
		                      	<div class="form-group">
			                        <label>Inventario Cantv:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Inventario Cantv Generador','Código interno de inventario en Cantv. Ej. 223344', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        <input type="text" class="form-control" value="{{$motogenerador->generador->inventario_cant}}" name="generador[inventario_cant]" placeholder="Enter ...">
				                    </div>
			                    </div>

			                    <div class="col-sm-4">
		                      	<div class="form-group">
			                        <label>Capacidad KVA:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Capacidad KVA','kilovoltiamperio. Solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        <input type="text" class="form-control" value="{{$motogenerador->generador->cap_kva}}" name="generador[cap_kva]" placeholder="Enter ...">
		                      	</div>
			                    </div>
					                
					                <div class="col-sm-4">
				                    <div class="form-group">
				                    	<label>Tensión:</label>
				                    	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Tension','Marque el voltaje del equipo', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <div class="form-check">
				                          <input class="form-check-input" type="radio" name="motor[fases]" @if($motogenerador->motor->fases=="208V") checked @endif value="208V">
				                          <label class="form-check-label">208V</label>
				                        </div>
				                        <div class="form-check">
				                          <input class="form-check-input" type="radio" name="motor[fases]" @if($motogenerador->motor->fases=="416V") checked @endif value="416V" >
				                          <label class="form-check-label">416V</label>
				                        </div>
				                        <div class="form-check">
				                          <input class="form-check-input" type="radio" name="motor[fases]" @if($motogenerador->motor->fases=="480V") checked @endif value="480V" >
				                          <label class="form-check-label">480V</label>
				                        </div>
						                </div>
						            	</div>
						            
						            	<div class="col-sm-4">
						                <div class="form-group">
					                  	<label>Tablero de Transferencia:</label>
					                  	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Tablero de Transferencia','¿Posee tablero de transferencia?', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                	    <div class="form-check">
			                          <input class="form-check-input" type="radio"  name="generador[tablero]"  @if($motogenerador->generador->tablero=="Si") checked @endif value="Si">
			                          <label class="form-check-label">Si</label>
			                        </div>
			                        <div class="form-check">
			                          <input class="form-check-input" type="radio"  name="generador[tablero]" @if($motogenerador->generador->tablero=="No") checked @endif value="No">
			                          <label class="form-check-label">No</label>
			                        </div>
				                    </div>
					                </div>
								        </div>
								              <!-- /.card-body -->
								      </div>
								    </div>
									</div>
								
									<div class="col-md-12">
				            <div class="card card-success">
				              <div class="card-header">
				                <h3 class="card-title">Instalación Motogenerador</h3>
				              </div>
				              <!-- /.card-header -->
				              <div class="card-body">
			                  <div class="row">
			                    <div class="col-sm-4">
			                      <div class="form-group">
			                        <label>Operatividad:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Operatividad','Seleccione el estado en que se encuentra el equipo', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a><!--End ShowHelp-->
			                        <select name="motor[operatividad]" id="operatividad" class="form-control">
	                            	<option @if($motogenerador->motor->operatividad=="Operativo") selected @endif value="Operativo">Operativo</option>
	                            	<option @if($motogenerador->motor->operatividad=="Inoperativo") selected @endif value="Inoperativo">Inoperativo</option>
	                            	<option @if($motogenerador->motor->operatividad=="Optimo") selected @endif value="Optimo">Optimo</option>
                                <option @if($motogenerador->motor->operatividad=="Critico") selected @endif value="Critico">Critico</option>
                                <option @if($motogenerador->motor->operatividad=="Vandalizado") selected @endif value="Vandalizado">Vandalizado</option>
                                <option @if($motogenerador->motor->operatividad=="Deficiente") selected @endif value="Deficiente">Deficiente</option>
															</select>
			                      </div>
			                    </div>
			                    
			                    <div class="col-sm-4">
			                      	<div class="form-group">
			                        	<label>Fecha de Instalación:</label>
			                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Fecha de Instalación','Introduzca fecha', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input id="box_ModeloRectificador" name="motor[fecha_instalacion]" value="{{$motogenerador->motor->fecha_instalacion}}" class="form-control" type="date">
				                    	</div>
			                    </div>
			                   
			                    <div class="col-sm-4">
			                      <div class="form-group">
			                        <label>Criticidad del Equipo:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Criticidad del Equipo','Selecione criticidad. Por Ej. para su atención inmediata o posterior', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        <select name="motor[criticidad]" class="form-control">
		                            	<option>Indique critidad del equipo</option>
																	<option @if($motogenerador->motor->criticidad=="Alta") selected @endif value="Alta">Alta</option>
                              		<option @if($motogenerador->motor->criticidad=="Media") selected @endif value="Media">Media</option>
                              		<option @if($motogenerador->motor->criticidad=="Baja") selected @endif value="Baja">Baja</option>
																</select>
			                      </div>
			                    </div>
			                 	</div>

			                	<div class="row" id="falla" @if($motogenerador->motor->operatividad != "Deficiente") style="display: none" @endif>
	                        <div class="col-sm-4">
	                          <label>Porcentaje Operatividad:</label>
	                          <div class="form-group">
	                                <input name="motor[porc_falla]" onblur="falla()" id="input_falla" @if($motogenerador->motor->operatividad != "Deficiente") disabled @endif type="text" value="{{$motogenerador->motor->porc_falla}}">
	                          </div>
	                        </div>
	                    	</div>

			                  <div class="row">
			                  	<div class="col-sm-4">
		                      	<div class="form-group">
		                        	<label>¿Equipo en Garantía?</label>
		                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Garantía','¿Tiene o está vigente la garantía?', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a><!--End ShowHelp-->
		                        	<div class="form-check">
			                          <input class="form-check-input" type="radio" @if($motogenerador->motor->garantia == 'si') checked @endif value="si" name="motor[garantia]">
			                          <label class="form-check-label">Si</label>
			                        </div>
			                        <div class="form-check">
			                          <input class="form-check-input" type="radio" @if($motogenerador->motor->garantia == 'no') checked @endif value="no" name="motor[garantia]">
			                          <label class="form-check-label">No</label>
			                        </div>				                    
				                    </div>
			                    </div>
			                	</div>

			                 	<div class="form-group">
													<label for="comment">Observaciones:</label>
													<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Observaciones','Información complementaria sobre seguimientos e incidencias a destacar. Solo de la instalación del motogenerador', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
													<textarea class="form-control" name="motor[observaciones]" rows="3" id="comment">{{$motogenerador->motor->observaciones}}</textarea>			
												</div>
				                  
			                 	<div class="card-footer" align="right">
				                  <button type="submit" class="btn btn-success">
				                  	<i class="fas fa-sync-alt nav-icon"></i> 
				                  	Actualizar
				                  </button>
				                </div>
						        
				              </div>
				              <!-- /.card-body -->
				            </div>
				          </div>
			        </form>
			       </div>
			     </div>
			   </div>
			 </div>
			</div>
		</section>
	</div>
@endsection
@section('vs')
	@if(isset($validator))
    	{!! $validator->selector("#formulario_mg") !!}
	@endif






<script type="text/javascript">
$(document).ready(function () {


  	$('select[id=region_id]').change(function () {
    	console.log('hola')
    var region_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'estado/' + region_id, function (cidades) {
        $('select[id=estado_id]').empty();

        if(cidades.length != 0){
            $('select[id=estado_id]').append('<option value=""> Selecciona un estado</option>');
            $.each(cidades, function (key, value) {
                $('select[id=estado_id]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });

    });


	$('select[id=estado_id]').change(function () {
    	//console.log('hola')
    var estado_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'central/' + estado_id, function (cidades) {
        $('select[id=central_id]').empty();

        if(cidades.length != 0){
            $('select[id=central_id]').append('<option value=""> Selecciona un central</option>');
            $.each(cidades, function (key, value) {
                $('select[id=central_id]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });

    });

function falla(){
    var falla = document.getElementById('input_falla')
    var range = document.getElementById('range')
    var opera = document.getElementById('operatividad')
    console.log(falla.value)

    range.value == falla.value
    if(falla.value < 1){
      opera.value == "Inoperativo"
    }else if(falla.value>99){
      opera.value == "Optimo"
    }

  }
$('select[id=operatividad]').change(function(){
  var valor = $(this).val();
  //console.log(valor)
  if (valor == "Deficiente"){
    $('#falla').show();
    $('#input_falla').prop('disabled',false);
  }else{
    $('#falla').hide();
    $('#input_falla').prop('disabled',true);

  }



 });





  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
 
});
</script>
@endsection


