@extends ('/layouts/index')

@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Motorgenerador 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inventario</a></li>
              <li class="breadcrumb-item active">Motorgenerador</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
          <div class="col-md-6">
            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fa fa-cogs"></i>
                  Motor
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <dl>
                  <dt>Marca</dt>
                  <dd>{{$motogenerador->motor->marca}}</dd>
                  <dt>Modelo</dt>
                  <dd>{{$motogenerador->motor->modelo}}</dd>
                  <dt>Serial</dt>
                  <dd>{{$motogenerador->motor->serial}}</dd>
                  <dt>Capacidad de tanque de aceite</dt>
                  <dd>{{$motogenerador->motor->cap_tanq_aceite}}</dd>
                  <dt>Capacidad de tanque de refrigeración</dt>
                  <dd>{{$motogenerador->motor->cap_tanq_refrig}}</dd>
                  <dt>Comb de tanque de principal</dt>
                  <dd>{{$motogenerador->motor->comb_tanq_princ}}</dd>
                  <dt>Comb de tanque de diario</dt>
                  <dd>{{$motogenerador->motor->comb_tanq_diario}}</dd>
                  <dt>Cantidad Baterias de Arranque</dt>
                  <dd>{{$motogenerador->motor->cant_bat_arranq}}</dd>
                  <dt>Cantidad Baterias </dt>
                  <dd>{{$motogenerador->motor->cant_baterias}}</dd>
                  <dt>Fases</dt>
                  <dd>{{$motogenerador->motor->fases}}</dd>
                  <dt>Cargador de Bateria</dt>
                  <dd>@if($motogenerador->motor->carg_bat == 1) Si @else No  @endif</dd>
                   <dt>Inventario Cantv</dt>
                  <dd>{{$motogenerador->motor->inventario_cant}}</dd>
                </dl>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <div class="row">
              <div class="col-md-12">
                <div class="card card-primary">
                  <div class="card-header">
                    <h3 class="card-title">
                      <i class="fa fa-map"></i>
                          Ubicación
                    </h3>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <dl>
                      <dt>Central/Localidad</dt>
                      <dd>{{$motogenerador->motor->ubicacion->central->localidad->nombre}}</dd>
                      <dt>Estado</dt>
                      <dd>{{$motogenerador->motor->ubicacion->central->localidad->estados->nombre}} </dd>
                      <dt>Regiòn</dt>
                      <dd>{{$motogenerador->motor->ubicacion->central->region->nombre}} </dd>
                      <dt>Piso</dt>
                      <dd>{{$motogenerador->motor->ubicacion->piso}} </dd>
                      <dt>Estructura</dt>
                      <dd>{{$motogenerador->motor->ubicacion->estructura}}</dd>
                      <dt>Sala</dt>
                      <dd>{{$motogenerador->motor->ubicacion->sala}} </dd>
                      <dt>Criticidad de espacio</dt>
                      <dd>{{$motogenerador->motor->ubicacion->criticidad_esp}} </dd>
                     
                    </dl>
                  </div>
                  <!-- /.card-body -->
                </div>
              </div>
            </div>



          </div>
          <!-- ./col -->
          <div class="col-md-6">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fa fa-bolt"></i>
                      Generador
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <dl>
                  <dt>Marca</dt>
                  <dd>{{$motogenerador->generador->marca}}</dd>
                  <dt>Modelo</dt>
                  <dd>{{$motogenerador->generador->modelo}}</dd>
                  <dt>Serial</dt>
                  <dd>{{$motogenerador->generador->serial}}</dd>
                  <dt>Inventario Cantv</dt>
                  <dd>{{$motogenerador->generador->inventario_cant}}</dd>
                  <dt>Capacidad kva</dt>
                  <dd>{{$motogenerador->generador->cap_kva}}</dd>
                  <dt>Tablero</dt>
                  <dd> @if($motogenerador->generador->tablero==1) Si @else No @endif 
                    </dd>
                </dl>
              </div>
              <!-- /.card-body -->
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="card card-primary">
                  <div class="card-header">
                    <h3 class="card-title">
                      <i class="fa fa-user"></i>
                          Responsable
                    </h3>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <dl>
                      <dt>Nombre y Apellido</dt>
                      <dd>{{$motogenerador->motor->ubicacion->responsable->nombre}} / 
                     {{$motogenerador->motor->ubicacion->responsable->apellido}}</dd>
                      <dt>Cédula</dt>
                      <dd>{{$motogenerador->motor->ubicacion->responsable->cedula}}</dd>
                      <dt>Cargo</dt>
                      <dd>{{$motogenerador->motor->ubicacion->responsable->cargo}}</dd>
                      <dt>Numero de Oficina</dt>
                      <dd>{{$motogenerador->motor->ubicacion->responsable->num_oficina}}</dd>
                      <dt>Numero de Personal</dt>
                      <dd>{{$motogenerador->motor->ubicacion->responsable->num_personal}}</dd>
                      <dt>Numero de Poo</dt>
                      <dd>{{$motogenerador->motor->ubicacion->responsable->cod_p00}}</dd>
                      <dt>Correo</dt>
                      <dd>{{$motogenerador->motor->ubicacion->responsable->correo}}</dd>
                    </dl>
                  </div>
                  <!-- /.card-body -->
                </div>
              </div>
              
            </div>
          </div>
          <!-- ./col -->
       
          </div>
        
      </div>
    </div>
  </section>
</div>
@endsection