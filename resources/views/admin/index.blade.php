@extends ('/layouts/index')

@section('content')

<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Motogenerador 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inventario</a></li>
              <li class="breadcrumb-item active">Motogenerador</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Vista Preliminar de Registros</h3>
              <div align="right">
                  <a href="/motogenerador/create" class="btn btn-success">Nuevo Registro</a>
              </div>
            </div>
            <!-- /.card-header -->

            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr align="center">
                    <th class="not-export-col">Acciones</th>
                    <th>Región</th>
                    <th>Estado</th>
                    <th>Central</th>
                    <th>Estructura</th>
                    <th>Sala</th>
                    <th>Piso</th>
                    <th>Responsable</th>
                    <th>Marca Motor</th>
                    <th>Modelo Motor</th>
                    <th>Serial Motor</th>
                    <th>Inventario CANTV Motor</th>
                    <th>Estado de Arranque</th>
                    <th>Capacidad Tanque Aceite (ltrs)</th>
                    <th>Capacidad Tanque de Refrigerante (ltrs)</th>
                    <th>Capacidad Tanque Principal (ltrs)</th>
                    <!--<th>Capacidad Tanque Diario (ltrs)</th>-->
                    <th>Cantidad Baterías de Arranque</th>
                    <th>Amperaje Baterías de Arranque</th>
                    <th>Cargador de Baterías</th>
                    <th>Cantidad de filtros</th>
                    <th>Modelo de Filtros</th>
                    <th>Baterias de control</th>
                    <th>Operatividad</th>
                    <th>Porcentaje Falla</th>
                    <th>Fases</th>
                    <th>Marca Generador</th>
                    <th>Modelo Generador</th>
                    <th>Serial Generador</th>
                    <th>Inventario CANTV Generador</th>
                    <th>Capacidad (KVA)</th>
                    <th>Tablero de Transferencia</th>
                    <th>Fecha Actualización</th>
                    <th>Usuario</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($motor as $equipos)
                  <tr align="center" id="tdId_{{$equipos->id}}">
                    <td>
                      <a href="/motogenerador/{{encrypt($equipos->id)}}/edit" class="btn btn-warning"><i class="far fa-edit"></i></a>
                      <a href="/motogenerador/{{encrypt($equipos->id)}}" class="btn btn-success"><i class="far fa-eye"></i></a>
                      <a onclick="deleteItem('{{$equipos->id}}')" class="btn btn-danger"><i class="fa fa-times"></i></a>
                    </td>
                    <td>{{$equipos->motor->ubicacion->central->localidad->estados->region->nombre ?? 'inexistente'}}</td>
                    <td>{{$equipos->motor->ubicacion->central->localidad->estados->nombre ?? 'inexistente'}}</td>
                    <td>{{$equipos->motor->ubicacion->central->localidad->nombre ?? 'inexistente'}}</td>
                    <td>{{$equipos->motor->ubicacion->central->localidad->tipo ?? 'S/T'}}</td>
                    <td>{{$equipos->motor->ubicacion->sala}}</td>
                    <td>{{$equipos->motor->ubicacion->piso}}</td>
                    <td>
                      {{$equipos->motor->ubicacion->responsable->nombre}}_{{$equipos->motor->ubicacion->responsable->apellido}}
                    </td>
                    <td>{{$equipos->motor->marca}}</td>
                    <td>{{$equipos->motor->modelo}}</td>
                    <td>{{$equipos->motor->serial}}</td>
                    <td>{{$equipos->motor->inventario_cant}}</td>
                    <td>{{$equipos->motor->estado_arranque}}</td>
                    <td>{{$equipos->motor->cap_tanq_aceite}}</td>
                    <td>{{$equipos->motor->cap_tanq_refrig}}</td>
                    <td>{{$equipos->motor->comb_tanq_princ}}</td>
                    <!--<td>{{$equipos->motor->comb_tanq_diario}}</td>-->
                    <td>{{$equipos->motor->cant_baterias}}</td>
                    <td>{{$equipos->motor->amp_bateria}}</td>
                    <td>{{$equipos->motor->carg_bat}}</td>
                    <td>{{$equipos->motor->cantidad_filtro}}</td>
                    <td>{{$equipos->motor->modelo_filtro}}</td>
                    
                    <td>
                      @if($equipos->motor->batteryctrl != null)
                        @foreach($equipos->motor->batteryctrl as $baterias)
                        
                          
                          Cantidad:{{$baterias->cantidad}} 
                          Amp:{{$baterias->amperaje}} amp
                          Voltaje{{$baterias->voltaje}}
                            
                        @endforeach
                      @else 
                        No Posee
                      @endif

                    </td>
                    <td>{{$equipos->motor->operatividad}}</td>
                    <td>
                        @if($equipos->motor->operatividad =='Falla' && $equipos->motor->porc_falla)
                          {{$equipos->motor->porc_falla}}%
                      @else
                        @if($equipos->motor->operatividad == "Inoperativo" ||  $equipos->motor->operatividad == "Vandalizado" ) 0%  @else  100%  @endif  
                       
                      @endif

                    </td>



                    <td>{{$equipos->motor->fases}}</td>
                    <td>{{$equipos->generador->marca}}</td>
                    <td>{{$equipos->generador->modelo}}</td>
                    <td>{{$equipos->generador->serial}}</td>
                    <td>{{$equipos->generador->inventario_cant}}</td>
                    <td>{{$equipos->generador->cap_kva}}</td>
                    <td>{{$equipos->generador->tablero}}</td>
                    <td>{{$equipos->motor->updated_at}}</td>
                    <td>{{$equipos->motor->usuario->name ?? 'Sin Cambios'}}</td>
                  </tr>
                    @endforeach
                </tbody>
              </table>
           </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('scriptsalerts')


<script>
   @if(Session::has('exito'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Motogenerador Guardado con éxito'
      })
      })
    });
  @endif
   @if(Session::has('actualizado'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Datos actualizados con exito'
      })
      })
    });
  @endif
  
</script>

  <script>
    function deleteItem(item){
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Desea eliminar este motogenerador del intentario?',
      text: "Estas por quitar este equipo del Inventario!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, quitarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {

      var txt = document.getElementById("totalCar");
      console.log(item)

      if (result.value) {



            $.ajax({
                url: "/motogenerador/"+item,
                type:"DELETE",
                   headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    

                    success:function(result, status, xhr){                    
                        
                        if (status == 'success') {

                          swalWithBootstrapButtons.fire(
                            'Quitado con Exito!',
                            'Se eliminó con exito',
                            'success'
                          )

                          $("#tdId_"+item).hide();
                         // txt.innerHTML = result ;



                        }
                    },
                    error:function(xhr, status, error){
                      

                      Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se eliminó el producto, vuelva a intentarlo mas tarde '+error,
                        footer: '<a href>Seguramente hubo un problema de conexión</a>'
                      })
                      

                    }
                });
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No se ha quitado el producto.',
          'error'
        )
      }
    })
  }
</script>


@endsection
