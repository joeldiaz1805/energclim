@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Editar Contrato
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/contrato">Contrato</a></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content --> 
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
             
              <div class="card-body">
                <form method="POST" id="formulario_contrato" name="formulario_contrato"   action="/contratos/{{encrypt($contratos->id)}}" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <input type="hidden" value="{{Auth::user()->id}}" name="user_id">
                  <input type="hidden" value="PUT" name="_method">
                  <div class="col-md-12">
                    <div class="card card-success">
                      <div class="card-header">
                        <h3 class="card-title">Actualizar</h3>
                       </div>
                      <div class="card-body">
                        <div class="row">
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Desde</label>
                                <input type="date" name="desde" value="{{$contratos->desde}}"  class="form-control" maxlength="49" placeholder="Nombres">
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Hasta</label>
                                <input type="date" name="hasta"  value="{{$contratos->hasta}}" required class="form-control" maxlength="49" placeholder="Apellidos">
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Proveedor</label>
                                <select name="proveedor_id" id="estado" class="form-control">
                                  <option value="">Seleccione un Proveedor</option>
                                    @foreach($proveedores as $pro)
                                      <option @if($contratos->proveedor_id == $pro->id) selected @endif value="{{$pro->id}}">{{$pro->nombre}}</option>
                                    @endforeach
                                </select>
                             </div>
                           </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Monto asignado al Contrato</label>
                              <input type="text" name="monto_asignado" value="{{$contratos->monto_asignado}}" required class="form-control" maxlength="20" placeholder="Cod - P-00"> 
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Numero de Contrato</label>
                              <input type="text" name="nro_contrato" value="{{$contratos->nro_contrato}}" required class="form-control" maxlength="20" placeholder="0000000"> 
                            </div>
                          </div>
                        </div>
                      </div>
                <!-- CIERRA EL CAR CAR SUCES  --> 
         </div>
         <!--CIERRA EL CSFR-->
        </div>
      
         <div class="card-footer" align="right">
           <button type="submit" class="btn btn-primary">Actualizar</button>
         </div>
                     

    </form>
                  </div>
                </div>
            </div>
            </div>
        </div>
    </section>
</div>


@endsection


@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_contrato") !!}
@endif



<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });

$('select[id=region]').change(function () {
      console.log('hola')
    var region_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'estado/' + region_id, function (cidades) {
        $('select[id=estado]').empty();

        if(cidades.length != 0){
            $('select[id=estado]').append('<option value=""> Selecciona un estado</option>');
            $.each(cidades, function (key, value) {
                $('select[id=estado]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });

    });




  
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection