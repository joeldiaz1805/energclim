@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Octavo Proyecto de Servicio Universal de Telecomnicaciones - O.P.S.U.T.
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/opsut">O.P.S.U.T.</a></li>
              <li class="breadcrumb-item active">Requerimientos de Nodo</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content --> 
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
             
              <div class="card-body">
      <form method="POST" id="formulario_nodo" name="formulario_index"  action="#" enctype="multipart/form-data">
        <input type="hidden" name="_method" value="PUT">
                        {{ csrf_field() }}
                    
                   
                    <div class="col-md-12">
                    <!-- general form elements disabled -->
                     <div class="card card-success">
                      <div class="card-header">
                        <h3 class="card-title"> lista de Requerimientos para Nodos O.P.S.U.T.</h3>
                      </div>
            
          

<div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>

                  <tr align="center">
                       <th>Codigo de Requerimiento</th>
                       <th>Requerimiento</th>
                       <th>Status Requerimiento</th>
                       <th>Observacion</th>
                       <th>ID del nodo</th>
                   </tr>
                </thead>
                <tbody>
               <!-- aca se empieza a cargar el row principal de personal-->
                 @foreach($requeopsut as $reque)
             
                 <tr align="center"  id="nod_{{$reque->id}}">
                    <td>{{$reque->codigo}} </td>
                    <td>{{$reque->requerimiento}}</td>
                    <td>{{$reque->stat_req_nod}}</td>
                    <td>{{$reque->observacion}}</td>
                    <td>{{$reque->nodoopsut_id}}</td>
                    <td>
                     <a href="/opsut/{{$reque->id}}/edit" class="btn btn-warning"><i class="far fa-edit"></i></a>
                      <a href="#" class="btn btn-success"><i class="far fa-eye"></i></a>
                      <a id="intt" @if($reque->stat_req_nod==1) class="btn btn-success" onclick="deleteItem('{{$reque->id}}')" @else class="btn btn-danger" onclick="activeItem('{{$reque->id}}')" @endif class="btn btn-danger"><i id="ele"  class="fa fa-user-times"></i></a>
                      </td>
                  </tr>
                  @endforeach

                
                </tbody>
              </table>
           </div>



         <!--CIERRA EL CSFR-->
        </div>
      
         <div class="card-footer" align="right">
           <button type="submit" class="btn btn-primary">Actualizar</button>
         </div>
                 
    </form>
                  </div>
                </div>
            </div>
            </div>
        </div>
    </section>
</div>





@endsection

@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_requerimientoasall") !!}
@endif



<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection