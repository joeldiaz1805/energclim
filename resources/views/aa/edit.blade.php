@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Aire Acondicionado 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inventario</a></li>
              <li class="breadcrumb-item active">Aire Acondicionado</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
     
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
              
              <div class="card-body">
                 <form method="POST" id="formulario_aa" name="formulario_aa"  action="/aire_acondicionado/{{encrypt($aire->id)}}">
                  <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="aireA[user_id]" value="{{Auth::user()->id}}">
                     @if($parametros != null)
                           
                            <input type="hidden" name="parametro" value="{{$parametros['url']}}">
                    
                    @endif
                        {{ csrf_field() }}

                        <div class="col-md-12">
                  <div class="card card-success">
                    <div class="card-header">
                      <h3 class="card-title">Ubicación y Responsable</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                      <div class="card-header">
                        <h3 class="card-title">Ubicación</h3>
                      </div>
                      <br>
                      <div class="row">
                        <div class="col-sm-4">
                          <!-- text input -->
                            <div class="form-group">
                              <label>Región:</label>
                              <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Región','Seleccione región donde está ubicado el AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                <select  name="central[region_id]" class="form-control">
                                  @foreach($regiones as $region)
                                    <option @if($region->id == $aire->ubicacion->central->region->id) selected @endif value="{{$region->id}}">{{$region->nombre}}</option>
                                  @endforeach
                              </select>
                          </div>
                        </div>
                        
                       
                          <div class="col-sm-4">
                          <!-- text input -->
                          <div class="form-group">
                            <label>Estado:</label>
                            <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Estado','Seleccione estado donde está ubicado el AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                            <select name="central[estado_id]" class="form-control">
                              @foreach($estados as $estado)
                                <option @if($estado->id == $aire->ubicacion->central->estados->id) selected @endif value="{{$estado->id}}">{{$estado->nombre}}</option>
                              @endforeach
                          </select>
                          </div>
                        </div>
                       
                        <div class="col-sm-4">
                          <!-- text input -->
                            <div class="form-group">
                              <label>Central:</label>
                              <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Central','Seleccione central donde está ubicado el AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                <select name="central[localidad_id]" class="form-control">
                                  @foreach($localidades as $central)
                                    <option @if($central->id == $aire->ubicacion->central->localidad->id) selected @endif value="{{$central->id}}">{{$central->nombre}}</option>
                                  @endforeach
                              </select>
                          </div>
                          </div>
              
                     </div>
                     <div class="row">

                      <div class="col-sm-4">
                          <!-- text input -->
                            <div class="form-group">
                              <label>Piso:</label>
                              <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Piso','Introduzca piso donde se encuentra el AA. Solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                <select name="ubicacion[piso]" id="box_ModeloRectificador" class="form-control" required>
                                    <option>Seleccione ubicación piso</option>
                                    <option @if($aire->ubicacion->piso=="AZOTEA") selected @endif value="AZOTEA">AZOTEA</option>
                                    <option @if($aire->ubicacion->piso=="PH") selected @endif value="PH">PH</option>
                                    <option @if($aire->ubicacion->piso=="MZ") selected @endif value="MZ">MZ</option>
                                    <option @if($aire->ubicacion->piso=="PB") selected @endif value="PB">PB</option>
                                    <option @if($aire->ubicacion->piso=="ST") selected @endif value="ST">ST</option>
                                    <option @if($aire->ubicacion->piso=="S1") selected @endif value="S1">S1</option>
                                    <option @if($aire->ubicacion->piso=="S2") selected @endif value="S2">S2</option>
                                    <option @if($aire->ubicacion->piso=="S3") selected @endif value="S3">S3</option>
                                    <option @if($aire->ubicacion->piso=="Unico piso") selected @endif value="Unico piso">Unico piso</option>
                                    <option @if($aire->ubicacion->piso=="P1") selected @endif value="P1">P1</option>
                                    <option @if($aire->ubicacion->piso=="P2") selected @endif value="P2">P2</option>
                                    <option @if($aire->ubicacion->piso=="P3") selected @endif value="P3">P3</option>
                                    <option @if($aire->ubicacion->piso=="P4") selected @endif value="P4">P4</option>
                                    <option @if($aire->ubicacion->piso=="P5") selected @endif value="P5">P5</option>
                                    <option @if($aire->ubicacion->piso=="P6") selected @endif value="P6">P6</option>
                                    <option @if($aire->ubicacion->piso=="P7") selected @endif value="P7">P7</option>
                                    <option @if($aire->ubicacion->piso=="P8") selected @endif value="P8">P8</option>
                                    <option @if($aire->ubicacion->piso=="P9") selected @endif value="P9">P9</option>
                                    <option @if($aire->ubicacion->piso=="P10") selected @endif value="P10">P10</option>
                                    <option @if($aire->ubicacion->piso=="P11") selected @endif value="P11">P11</option>
                                    <option @if($aire->ubicacion->piso=="P12") selected @endif value="P12">P12</option>
                                    <option @if($aire->ubicacion->piso=="P13") selected @endif value="P13">P13</option>
                                    <option @if($aire->ubicacion->piso=="P14") selected @endif value="P14">P14</option>
                                    <option @if($aire->ubicacion->piso=="P15") selected @endif value="P15">P15</option>
                                    <option @if($aire->ubicacion->piso=="P16") selected @endif value="P16">P16</option>
                                    <option @if($aire->ubicacion->piso=="P17") selected @endif value="P17">P17</option>
                                    <option @if($aire->ubicacion->piso=="P18") selected @endif value="P18">P18</option>
                                    <option @if($aire->ubicacion->piso=="P19") selected @endif value="P19">P19</option>
                                    <option @if($aire->ubicacion->piso=="P20") selected @endif value="P20">P20</option>
                                    <option @if($aire->ubicacion->piso=="P21") selected @endif value="P21">P21</option>
                                    <option @if($aire->ubicacion->piso=="TERRAZA") selected @endif value="TERRAZA">TERRAZA</option>
                                </select>
                          </div>
                      </div>

                        <div class="col-sm-4">
                          <div class="form-group">
                            <label>Sala o espacio físico:</label>
                            <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Sala','Seleccione sala donde está alojado el AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                            <select name="ubicacion[sala]" id="box_ModeloRectificador" class="form-control">
                           
                                    <option>Seleccione una sala</option>
                                    <option @if($aire->ubicacion->sala=="ABA") selected @endif value="ABA">ABA</option>
                                    <option @if($aire->ubicacion->sala=="ADSL") selected @endif value="ADSL">ADSL</option>
                                    <option @if($aire->ubicacion->sala=="BANCO DE BATERIA") selected @endif value="BANCO DE BATERIA">BANCO DE BATERIA</option>
                                    <option @if($aire->ubicacion->sala=="CENTRO DE DISTRIBUCION") selected @endif value="CENTRO DE DISTRIBUCION">CENTRO DE DISTRIBUCION</option>
                                    <option @if($aire->ubicacion->sala=="CX") selected @endif value="CX">CX</option>
                                    <option @if($aire->ubicacion->sala=="DATA CENTER") selected @endif value="DATA CENTER">DATA CENTER</option>
                                    <option @if($aire->ubicacion->sala=="DIGITAL") selected @endif value="DIGITAL">DIGITAL</option>
                                    <option @if($aire->ubicacion->sala=="DP") selected @endif value="DP">DP</option>
                                    <option @if($aire->ubicacion->sala=="DSLAM") selected @endif value="DSLAM">DSLAM</option>
                                    <option @if($aire->ubicacion->sala=="DX") selected @endif value="DX">DX</option>
                                    <option @if($aire->ubicacion->sala=="FURGON") selected @endif value="FURGON">FURGON</option>
                                    <option @if($aire->ubicacion->sala=="LIBRE") selected @endif value="LIBRE">LIBRE</option>
                                    <option @if($aire->ubicacion->sala=="MG") selected @endif value="MG">MG</option>
                                    <option @if($aire->ubicacion->sala=="OAC") selected @endif value="OAC">OAC</option>
                                    <option @if($aire->ubicacion->sala=="OFICINA ADMINISTRATIVA") selected @endif value="OFICINA ADMINISTRATIVA">OFICINA ADMINISTRATIVA</option>
                                    <option @if($aire->ubicacion->sala=="OPSUT") selected @endif value="OPSUT">OPSUT</option>
                                    <option @if($aire->ubicacion->sala=="OUTDOOR") selected @endif value="OUTDOOR">OUTDOOR</option>
                                    <option @if($aire->ubicacion->sala=="PCM") selected @endif value="PCM">PCM</option>
                                    <option @if($aire->ubicacion->sala=="PSTN") selected @endif value="PSTN">PSTN</option>
                                    <option @if($aire->ubicacion->sala=="RECTIFICADORES") selected @endif value="RECTIFICADORES">RECTIFICADORES</option>
                                    <option @if($aire->ubicacion->sala=="SSP") selected @endif value="SSP">SSP</option>
                                    <option @if($aire->ubicacion->sala=="TX") selected @endif value="TX">TX</option>
                                    <option @if($aire->ubicacion->sala=="TX INTERNACIONAL") selected @endif value="TX INTERNACIONAL">TX INTERNACIONAL</option>
                                    <option @if($aire->ubicacion->sala=="UMA") selected @endif value="UMA">UMA</option>
                                    <option @if($aire->ubicacion->sala=="UNICA") selected @endif value="UNICA">UNICA</option>
                                    <option @if($aire->ubicacion->sala=="UPS") selected @endif value="UPS">UPS</option>
                                    <option @if($aire->ubicacion->sala=="VARIAS SALAS") selected @endif value="VARIAS SALAS">VARIAS SALAS</option>
                            </select>
                          </div>
                        </div>

                        <div class="col-sm-4">
                          <!-- text input -->
                            <div class="form-group">
                              <label>Estructura:</label>
                              <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Estructura','Seleccione donde está alojado el AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                <select name="ubicacion[estructura]" class="form-control">
                                  <option>Seleccione tipo de estructura</option>
                                  <option @if($aire->ubicacion->estructura=="CENTRAL CRITICA") selected @endif value="Fija">CENTRAL CRITICA</option>
                                  <option @if($aire->ubicacion->estructura=="CENTRAL FIJA") selected @endif value="CENTRAL FIJA">CENTRAL FIJA</option>
                                  <option @if($aire->ubicacion->estructura=="URL") selected @endif value="URL">URL</option>
                                  <option @if($aire->ubicacion->estructura=="NODO INDOOR") selected @endif value="NODO INDOOR">NODO INDOOR</option>
                                  <option @if($aire->ubicacion->estructura=="NODO OUTDOOR") selected @endif value="NODO OUTDOOR">NODO OUTDOOR</option>
                                  <option @if($aire->ubicacion->estructura=="GTI") selected @endif value="GTI">GTI</option>
                                  <option @if($aire->ubicacion->estructura=="OPSUT") selected @endif value="OPSUT">OPSUT</option>
                                  <option @if($aire->ubicacion->estructura=="SUPER AULA") selected @endif value="SUPER AULA">SUPER AULA</option>
                                  <option @if($aire->ubicacion->estructura=="OAC") selected @endif value="OAC">OAC</option>
                                  <option @if($aire->ubicacion->estructura=="DLC") selected @endif value="DLC">DLC</option>
                                  <option @if($aire->ubicacion->estructura=="NO CANTV") selected @endif value="NO CANTV">NO CANTV</option>
                              </select>
                          </div>
                        </div>
                     </div>

                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label>Criticidad del Espacio:</label>
                            <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Criticidad del Espacio','Introduzca Criticidad del Espacio', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                            <select name="ubicacion[criticidad_esp]" required class="form-control">
                              <option>Seleccione criticidad del espacio</option>
                              <option @if($aire->ubicacion->criticidad_esp=="Crítica") selected @endif value="Crítica">Crítica</option>
                              <option @if($aire->ubicacion->criticidad_esp=="Óptima") selected @endif value="Óptima">Óptima</option>
                            </select>
                        </div>
                      </div>
                    </div>

                    <div class="card-header">
                       <h3 class="card-title">Responsable Equipo A/A</h3>
                    </div>
                    <br>
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label>Nombres:</label>
                          <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Nombres','Nombre persona encargada y responsable del AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                          <input type="text" class="form-control" value="{{$aire->ubicacion->responsable->nombre}}" required name="responsable[nombre]" >
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label>Apellidos:</label>
                          <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Apellidos','Apellido persona encargada y responsable del AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                          <input type="text" class="form-control" value="{{$aire->ubicacion->responsable->apellido}}" required name="responsable[apellido]" >
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label>Cédula:</label>
                          <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cédula','Número de cédula', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                          <input type="text" class="form-control" required value="{{$aire->ubicacion->responsable->cedula}}" name="responsable[cedula]" >
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label>P00:</label>
                          <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','P00','Número interno que identifica al trabajador de la empresa. Ej. 112233', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                          <input type="text" class="form-control" required value="{{$aire->ubicacion->responsable->cod_p00}}" name="responsable[cod_p00]" >
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label>Número de Oficina:</label>
                            <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Número de Oficina','Número de Telf.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                            <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-phone"></i></span>
                              </div>
                              <input type="text" name="responsable[num_oficina]" value="{{$aire->ubicacion->responsable->num_oficina}}" required  class="form-control" data-inputmask='"mask": "99999999999"' data-mask>
                            </div>
                          </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label>Número Personal:</label>
                          <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Número Personal','Número de Telf.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fas fa-phone"></i></span>
                            </div>
                            <input type="text" name="responsable[num_personal]" value="{{$aire->ubicacion->responsable->num_personal}}" required  class="form-control" data-inputmask='"mask": "99999999999"' data-mask>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label>Cargo:</label>
                            <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cargo','Denominación del cargo de la persona encargada del AA.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                            <input type="text" class="form-control" required name="responsable[cargo]" value="{{$aire->ubicacion->responsable->cargo}}" placeholder="Introduzca cargo">
                          </div>
                        </div>
                        <div class="col-sm-8">
                          <div class="form-group">
                            <label>Correo:</label>
                            <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Correo','Dirección de correo electrónico. Ej. empleado@cantv.com.ve', 300 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                            <input type="email" name="responsable[correo]" value="{{$aire->ubicacion->responsable->correo}}" required class="form-control" >
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  </div>              
                    <div class="col-md-12">
                  
                      <div class="card card-success">
                        <div class="card-header">
                          <h3 class="card-title">Aire Acondicionado</h3>
                        </div>
                      
                        <div class="card-body">
                          <div class="row">
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Marca:</label>
                                <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Marca','Seleccionar fabricante del aire acondicionado', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                  <select name="aireA[marca]" class="form-control">
                                    @foreach($marcas as $mar)
                                        <option @if($mar->descripcion == $aire->marca) selected @endif value="{{$mar->descripcion}}">{{$mar->descripcion}}</option>
                                    @endforeach


                                  </select>
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Modelo:</label>
                                <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Modelo AA','Caracteres y números que lo identifican Ej. AA202011112525', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                  <input type="text" name="aireA[modelo]" value="{{$aire->modelo}}"  class="form-control" placeholder="Introduzca modelo">
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="form-group">
                                  <label>Serial:</label>
                                  <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Serial','Secuencia de números y/o caracteres', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                    <input type="text" name="aireA[serial]"  value="{{$aire->serial}}"   class="form-control" placeholder="Introduzca serial">
                                </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Inventario Cantv:</label>
                                <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Inventario Cantv','Código interno de inventario en Cantv. Ej. 223344', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                <input type="text" class="form-control" name="aireA[inventario_cant]"  value="{{$aire->inventario_cant}}"    placeholder="Introduzca Inventario Cantv">
                              </div>
                            </div>
                          </div>

                          <div class="card-header">
                            <h3 class="card-title">Composición:</h3>
                          </div>
                          <br>
                          <div class="row">
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Tipo de Equipo:</label>
                                <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Tipo de Equipo','Seleccione el tipo de equipo', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                <select id="UMA" name="aireA[tipo_equipo]"  class="form-control">
                                    <option @if($aire->tipo_equipo==1) selected @endif value="1">Precisión</option>
                                    <option @if($aire->tipo_equipo==2) selected @endif value="2">UMA</option>
                                    <option @if($aire->tipo_equipo==3) selected @endif value="3">SPLIT</option>
                                    <option @if($aire->tipo_equipo==4) selected @endif  value="4">Compacto</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Tipo de Correa:</label>
                                <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Tipo de Correa','Indique tipo de correa que utiliza el equipo', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                <input name="aireA[tipo_correa]" value="{{$aire->tipo_correa}}" type="text" class="form-control" placeholder="Ej: B52">
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Número de Correa:</label>
                                <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Número de Correa','Solo números sin letras. Ej. 01, 10, 20, etc.', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                <input name="aireA[nro_correa]" value="{{$aire->nro_correa}}" type="text" class="form-control" placeholder="Ej: 02">
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Cantidad de Correas:</label>
                                <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cantidad de Correas','Solo números sin letras. Ej. 01, 10, 20, etc.', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                <input name="aireA[cant_correa]" value="{{$aire->cant_correa}}" type="text" class="form-control" placeholder="Ej: B52">
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Número de Circuitos:</label>
                                <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Número de Circuitos','Seleccione el número de circuitos', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                <select name="aireA[nro_circuitos]" class="form-control">
                                  <option @if($aire->num_circuitos=="Monofásico") selected @endif value="Monofásico">Monofásico</option>
                                  <option @if($aire->num_circuitos=="Bifásico") selected @endif  value="Bifásico">Bifásico</option>
                                  <option @if($aire->num_circuitos=="Trifásico") selected @endif  value="Trifásico">Trifásico</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-sm-4" id="compresor_row">
                              <div class="form-group">
                                <label>Tipo de Compresor:</label>
                                <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Tipo de Compresor','Seleccione el tipo de compresor', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                <select id="tipo_compresor"  name="aireA[tipo_compresor]" class="form-control">
                                  <option @if($aire->tipo_compresor=="Hermético") selected @endif value="Hermético">Hermético</option>
                                  <option @if($aire->tipo_compresor=="Semi-Hermético") selected @endif value="Semi-Hermético">Semi-Hermético</option>
                                  <option @if($aire->tipo_compresor=="Garrapata") selected @endif value="Garrapata">Garrapata</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Toneladas de refrigeración instaladas:</label>
                                <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Toneladas de Refrigeración Intaladas','En toneladas, solo números sin letras. Ej. 1, 10, 20, etc.', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                <input name="aireA[ton_refrig_instalada]" value="{{$aire->ton_refrig_instalada}}" type="text" class="form-control" placeholder="Ej: 20">
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Toneladas de refrigeración operativas:</label>
                                <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Toneladas de Refrigeración Operativas','En toneladas, solo números sin letras. Ej. 1, 10, 20, etc.', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                <input name="aireA[ton_refrig_operativa]" value="{{$aire->ton_refrig_operativa}}" type="text" class="form-control" placeholder="Ej: 20">
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Toneladas de refrigeración faltantes:</label>
                                <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Toneladas de Refrigeración Faltantes','En toneladas, solo números sin letras. Ej. 1, 10, 20, etc.', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                <input name="aireA[ton_refrig_faltante]"  type="text" value="{{$aire->ton_refrig_faltante}}"  class="form-control" placeholder="Ej: 20">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Tipo de Refrigerante:</label>
                                <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Tipo de Refrigerante','Seleccione el tipo de refrigerante.', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                  <select name="aireA[tipo_refrigerante]" class="form-control">
                                    <option @if($aire->tipo_refrigerante=="R22") selected @endif value="R22">R22</option>
                                    <option @if($aire->tipo_refrigerante=="134A") selected @endif value="134A">134A</option>
                                    <option @if($aire->tipo_refrigerante=="410A") selected @endif value="410A">410A</option>
                                    <option @if($aire->tipo_refrigerante=="407C") selected @endif value="407C">407C</option>
                                  </select>
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Voltaje de Operación:</label>
                                <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Voltaje de Operación','Marque el voltaje de operación.', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                <div class="form-check">
                                  <input class="form-check-input" type="radio" @if($aire->volt_operacion=="208/230/3/60") checked @endif value="208/230/3/60" name="aireA[volt_operacion]">
                                  <label class="form-check-label">208/230/3/60</label>
                                </div>
                                <div class="form-check">
                                  <input class="form-check-input" type="radio" @if($aire->volt_operacion=="460/3/60") checked @endif value="460/3/60" name="aireA[volt_operacion]">
                                  <label class="form-check-label">460/3/60</label>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Tablero de Control:</label>
                                <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Tablero de Control','Indique si tiene tablero de control', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                  <div class="form-check">
                                    <input class="form-check-input" @if($aire->tablero_control=="Si") checked @endif type="radio" value="Si" name="aireA[tablero_control]">
                                    <label class="form-check-label">Si</label>
                                  </div>
                                  <div class="form-check">
                                    <input class="form-check-input" @if($aire->tablero_control=="No") checked @endif type="radio" value="No" name="aireA[tablero_control]">
                                    <label class="form-check-label">No</label>
                                  </div>                                
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-12">
                      <div class="card card-success">
                        <div class="card-header">
                          <h3 class="card-title">Instalación Equipo de A/A</h3>
                        </div>
                        <div class="card-body">
                          <div class="row">
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Operatividad:</label>
                                <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Operatividad','Seleccione el estado en que se encuentra el equipo', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                <select name="aireA[operatividad]" id="operatividad" class="form-control">
                                  <option @if($aire->operatividad=="Optimo") selected @endif value="Optimo">Optimo</option>
                                  <option @if($aire->operatividad=="No Operativo") selected @endif value="No Operativo">No Operativo</option>
                                  <option @if($aire->operatividad=="Critico") selected @endif value="Critico">Critico</option>
                                  <option @if($aire->operatividad=="Deficiente") selected @endif value="Deficiente">Deficiente</option>
                                  <option @if($aire->operatividad=="Vandalizado") selected @endif value="Vandalizado">Vandalizado</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Fecha de Instalación:</label>
                                <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Fecha de Instalación','Introduzca fecha', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                <input name="aireA[fecha_instalacion]" value="{{$aire->fecha_instalacion}}" id="box_ModeloRectificador" class="form-control" type="date">
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Criticidad del Equipo:</label>
                                <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Criticidad del Equipo','Seleccione criticidad. Por Ej. para su atención inmediata o posterior', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                <select  name="aireA[criticidad]"class="form-control">
                                  <option @if($aire->criticidad=="Alta") selected @endif value="Alta">Alta</option>
                                  <option @if($aire->criticidad=="Media") selected @endif value="Media">Media</option>
                                  <option @if($aire->criticidad=="Baja") selected @endif value="Baja">Baja</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          
                          <div class="row" id="falla" @if($aire->operatividad == "Deficiente") style="display: none" @endif>
                            <div class="col-sm-4">
                              <label>Porcentaje Operatividad:</label>
                              <div class="form-group">
                                    <input name="aireA[porc_falla]" onblur="falla()" id="input_falla" disabled type="text" value="{{$aire->porc_falla}}">
                              </div>
                            </div>
                           </div>
                          


                          <div class="row">
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>¿Equipo en Garantía?</label>
                                <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Garantía','¿Tiene o está vigente la garantía?', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                <div class="form-check">
                                  <input class="form-check-input" @if($aire->garantia=="si") checked @endif type="radio" value="si" name="aireA[garantia]">
                                  <label class="form-check-label">Si</label>
                                </div>
                                <div class="form-check">
                                  <input class="form-check-input" type="radio" @if($aire->garantia=="no") checked @endif value="no" name="aireA[garantia]">
                                  <label class="form-check-label">No</label>
                                </div>                            
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="comment">Observaciones:</label>
                            <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Observaciones','Información complementaria sobre seguimientos e incidencias a destacar. Solo de la instalación del AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                            <textarea class="form-control" rows="3" name="aireA[observaciones]" id="comment">{{$aire->observaciones}}</textarea>      
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="card-footer" align="right">
                      <button type="submit" class="btn btn-success"><i class="fas fa-sync-alt nav-icon"></i> Actualizar</button>
                    </div>
                  </div>
                </div>
              </div> 
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</div>
  
@endsection
@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_aa") !!}
@endif



<script type="text/javascript">
   var tipo_compresor = document.getElementById('tipo_compresor');
    //var tipo = document.getElementById('tipo');

  $('#UMA').click(function(){
     var valor = $(this).val();
      if (valor==2){
        $('#tipo_compresor').prop('disabled', true);
        $('#compresor_row').hide();
        tipo_compresor.value="no aplica"
        //tipo.append('<input id="input_tipo" name="tipo_compresor" value="no aplica" >');

      }else{
        $('#tipo_compresor').prop('disabled', false);
        $('#compresor_row').show();
        //$('#region').prop('disabled', true);
        tipo_compresor.value=""
      }
  });
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });

 function falla(){
    var falla = document.getElementById('input_falla')
    var range = document.getElementById('range')
    var opera = document.getElementById('operatividad')
    console.log(falla.value)

    range.value == falla.value
    if(falla.value < 1){
      opera.value == "Critico"
    }else if(falla.value>99){
      opera.value == "Optimo"
    }

  }
$('select[id=operatividad]').change(function(){
  var valor = $(this).val();
  //console.log(valor)
  if (valor == "Deficiente"){
    $('#falla').show();
    $('#input_falla').prop('disabled',false);
  }else{
    $('#falla').hide();
    $('#input_falla').prop('disabled',true);

  }



 });




  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection


