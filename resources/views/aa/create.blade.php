@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Aire Acondicionado 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inventario</a></li>
              <li class="breadcrumb-item active">Aire Acondicionado</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <div class="row ">
	    <div class="col s12 m12 l12 ">
	      @if ($errors->any())
	        <div class="alerta-errores">
	          <ul>
	            @foreach ($errors->all() as $error)
	              <li>{{ $error }}</li>
	            @endforeach
	          </ul>
	        </div>
	      @endif
	    </div>
	  </div>


    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
     
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
              
              <div class="card-body">
              	 <form method="POST" id="formulario_aa" name="formulario_aa"  action="{{ url('/aire_acondicionado')}}">

              	 	{{ csrf_field() }}
                        <input type="hidden" name="aireA[user_id]" value="{{Auth::user()->id}}">
              	 	@if($parametros != null)
                    <?php

                    	$localidad = App\Models\Models\localidades::where('id',decrypt($parametros['url']))->first();
                    	# code...
                    
                    ?>
                    <input type="hidden" name="parametro" value="{{$parametros['url']}}">
  	 				<input type="hidden" name="central[estado_id]" value="{{$localidad->estado_id}}">
  	 				<input type="hidden" name="central[region_id]" value="{{$localidad->estados->region_id}}">
  	 				<input type="hidden" name="central[localidad_id]" value="{{decrypt($parametros['url'])}}">

  	 				@endif
              	<!--Inicio sección Ubicación y Responsable-->
              	 	<div class="col-md-12"  @if($parametros != null) style="display: none" @endif>
			            <div class="card card-success">
			              <div class="card-header">
			                <h3 class="card-title">Ubicación y Responsable</h3>
			              </div>
			              <!-- /.card-header -->
			              <div class="card-body">
			                	<div class="card-header">
					                <h3 class="card-title">Ubicación</h3>
					              </div>
					              <br>
			                  <div class="row" @if($parametros != null) style="display: none" @endif>
			                  	<div class="col-sm-4">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Región:</label>
			                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Región','Seleccione región donde está ubicado el AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        	<select @if($parametros != null) disabled @endif  name="central[region_id]" id="region" class="form-control">
				                        		<option>Seleccione una Región</option>
				                        		@foreach($regiones as $region)
					                        		<option value="{{$region->id}}">{{$region->nombre}}</option>
					                        	@endforeach
					                    	</select>
				                    </div>
			                    </div>
			                    
			                   
			                      <div class="col-sm-4">
			                      <!-- text input -->
			                      <div class="form-group">
			                        <label>Estado:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Estado','Seleccione estado donde está ubicado el AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        <select @if($parametros != null) disabled @endif name="central[estado_id]" id="estado" class="form-control">
			                        	
				                       		<option value="">Seleccione un estado</option>
				                        
			                    	</select>
			                      </div>
			                    </div>
			                   
			                    <div class="col-sm-4">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Central:</label>
			                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Central','Seleccione central donde está ubicado el AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        	<select @if($parametros != null) disabled @endif name="central[localidad_id]" id="central" class="form-control">
				                        		
						                       		<option value="">Seleccione una central</option>
						                        
					                    	</select>
				                    </div>
			                      </div>
								
			                 </div>
			                 <div class="row">

			                 	<div class="col-sm-4">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Piso:</label>
			                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Piso','Introduzca piso donde se encuentra el AA. Solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        	<select name="ubicacion[piso]" id="box_ModeloRectificador" class="form-control">
					                        		<option>Seleccione ubicación piso</option>
						                        	<option value="AZOTEA">AZOTEA</option>
													<option value="PH">PH</option>
													<option value="MZ">MZ</option>
													<option value="PB">PB</option>
													<option value="ST">ST</option>
													<option value="S1">S1</option>
													<option value="S2">S2</option>
													<option value="S3">S3</option>
													<option value="Unico piso">Unico piso</option>
													<option value="P1">P1</option>
													<option value="P2">P2</option>
													<option value="P3">P3</option>
													<option value="P4">P4</option>
													<option value="P5">P5</option>
													<option value="P6">P6</option>
													<option value="P7">P7</option>
													<option value="P8">P8</option>
													<option value="P9">P9</option>
													<option value="P10">P10</option>
													<option value="P11">P11</option>
													<option value="P12">P12</option>
													<option value="P13">P13</option>
													<option value="P14">P14</option>
													<option value="P15">P15</option>
													<option value="P16">P16</option>
													<option value="P17">P17</option>
													<option value="P18">P18</option>
													<option value="P19">P19</option>
													<option value="P20">P20</option>
													<option value="P21">P21</option>
													<option value="TERRAZA">TERRAZA</option>
												</select>
				                    </div>
			                      </div>

			                      <div class="col-sm-4">
			                      <!-- text input -->
			                      <div class="form-group">
			                        <label>Sala o espacio físico:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Sala','Seleccione sala donde está alojado el AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        <select name="ubicacion[sala]" id="box_ModeloRectificador" class="form-control">
					                        		<option>Seleccione una sala</option>
					                        		<option value="ABA">ABA</option>
													<option value="ADSL">ADSL</option>
													<option value="BANCO DE BATERIA">BANCO DE BATERIA</option>
													<option value="CENTRO DE DISTRIBUCION">CENTRO DE DISTRIBUCION</option>
													<option value="CX">CX</option>
													<option value="DATA CENTER">DATA CENTER</option>
													<option value="DIGITAL">DIGITAL</option>
													<option value="DP">DP</option>
													<option value="DSLAM">DSLAM</option>
													<option value="DX">DX</option>
													<option value="FURGON">FURGON</option>
													<option value="LIBRE">LIBRE</option>
													<option value="MG">MG</option>
													<option value="OAC">OAC</option>
													<option value="OFICINA ADMINISTRATIVA">OFICINA ADMINISTRATIVA</option>
													<option value="OPSUT">OPSUT</option>
													<option value="OUTDOOR">OUTDOOR</option>
													<option value="PCM">PCM</option>
													<option value="PSTN">PSTN</option>
													<option value="RECTIFICADORES">RECTIFICADORES</option>
													<option value="SSP">SSP</option>
													<option value="TX">TX</option>
													<option value="TX INTERNACIONAL">TX INTERNACIONAL</option>
													<option value="UMA">UMA</option>
													<option value="UNICA">UNICA</option>
													<option value="UPS">UPS</option>
													<option value="VARIAS SALAS">VARIAS SALAS</option>
												</select>
			                      </div>
			                    </div>

			                  	<div class="col-sm-4">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Estructura:</label>
			                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Estructura','Seleccione donde está alojado el AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        	<select name="ubicacion[estructura]" required class="form-control">
				                        		<option>Seleccione tipo de estructura</option>
				                            	<option value="Fija">CENTRAL CRITICA</option>
					                        	<option  value="CENTRAL FIJA">CENTRAL FIJA</option>
					                        	<option  value="URL">URL</option>
					                        	<option  value="NODO INDOOR">NODO INDOOR</option>
					                        	<option  value="NODO OUTDOOR">NODO OUTDOOR</option>
					                        	<option  value="GTI">GTI</option>
					                        	<option  value="OPSUT">OPSUT</option>
					                        	<option  value="SUPER AULA">SUPER AULA</option>
					                        	<option  value="OAC">OAC</option>
					                        	<option  value="DLC">DLC</option>
					                        	<option  value="NO CANTV">NO CANTV</option>
					                    	</select>
				                    </div>
			                    </div>
			                 </div>

			                 <div class="row">
											<div class="col-sm-4">
						                      
						                      	<div class="form-group">
						                        	<label>Criticidad del Espacio:</label>
						                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Criticidad del Espacio','Introduzca Criticidad del Espacio', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                        	<select name="ubicacion[criticidad_esp]" required class="form-control">
				                            				<option>Seleccione criticidad del espacio</option>
															<option value="Crítica">Crítica</option>
															<option value="Óptima">Óptima</option>
					                    				</select>
							                    </div>
						                      </div>
										</div>

								<div class="card-header">
					               <h3 class="card-title">Responsable Equipo A/A</h3>
					            </div>
					            <br>
				                <div class="row">
				                     <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">

				                        <label>Nombres:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Nombres','Nombre persona encargada y responsable del AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" class="form-control" required name="responsable[nombre]" placeholder="Ej: Pedro...">

				                      </div>
				                    </div>
				                    <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Apellidos:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Apellidos','Apellido persona encargada y responsable del AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" class="form-control" required name="responsable[apellido]" placeholder="Ej: Tortoza...">

				                      </div>
				                    </div>
				                     <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">

				                        <label>Cédula:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cédula','Número de cédula', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" class="form-control" required name="responsable[cedula]" placeholder="Introduzca cédula">

				                      </div>
				                    </div>
				                </div>
			                  
			                  	<div class="row">

			                  		<div class="col-sm-4">
			                      <!-- text input -->
				                      <div class="form-group">

				                        <label>P00:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','P00','Número interno que identifica al trabajador de la empresa. Ej. 112233', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" class="form-control" required name="responsable[cod_p00]" placeholder="Introduzca P00">

				                      </div>
				                    </div>
			                  		<div class="col-sm-4">
			                      <!-- text input -->
				                      <div class="form-group">

				                        <label>Número de Oficina:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Número de Oficina','Número de Telf.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <div class="input-group">
						                    <div class="input-group-prepend">
						                      <span class="input-group-text"><i class="fas fa-phone"></i></span>
						                    </div>
						                    <input type="text" name="responsable[num_oficina]" required  class="form-control" data-inputmask='"mask": "99999999999"' data-mask>
						                  </div>

				                      </div>
				                    </div>

				                     <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">

				                        <label>Número Personal:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Número Personal','Número de Telf.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
					                      <div class="input-group">
						                    <div class="input-group-prepend">
						                      <span class="input-group-text"><i class="fas fa-phone"></i></span>
						                    </div>
						                    <input type="text" name="responsable[num_personal]" required  class="form-control" data-inputmask='"mask": "99999999999"' data-mask>
						                  </div>
					                    </div>
				                      </div>
			                  		
			                  	</div>

			                  	<div class="row">
			                  		<div class="col-sm-4">
						                      <!-- text input -->
						                      <div class="form-group">

						                        <label>Cargo:</label>
						                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cargo','Denominación del cargo de la persona encargada del AA.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
						                        <input type="text" class="form-control" required name="responsable[cargo]" placeholder="Introduzca cargo">

						                      </div>
						                    </div>
			                  		<div class="col-sm-8">
			                      <!-- text input -->
			                      		<div class="form-group">

			                        		<label>Correo:</label>
			                        		<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Correo','Dirección de correo electrónico. Ej. empleado@cantv.com.ve', 300 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        		<input type="email" name="responsable[correo]" required class="form-control" placeholder="Introduzca correo corporativo/personal">
			                        		
			                      		</div>
			                    	</div>
			                  	</div>
			                  </div>
			              </div>
			          </div>
                <!--Fin sección Ubicación y Responsable-->
                     	<div class="col-md-12">
				            <!-- general form elements disabled -->
				            <div class="card card-success">
				              <div class="card-header">
				                <h3 class="card-title">Aire Acondicionado</h3>
				              </div>
				              <!-- /.card-header -->
				              <div class="card-body">
				                  <div class="row">
				                    <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Marca:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Marca','Seleccionar fabricante del aire acondicionado', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <select name="aireA[marca]" class="form-control">
			                            	<option value="">Seleccione marca</option>
											@foreach($marcas as $mar)
												<option value="{{$mar->descripcion}}">{{$mar->descripcion}}</option>
											@endforeach
						            	</select>
				                      </div>
				                    </div>
				                    <div class="col-sm-4">
				                      <!-- text input -->
				                      	<div class="form-group">
				                        	<label>Modelo:</label>
				                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Modelo AA','Caracteres y números que lo identifican Ej. AA202011112525', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
					                        	<input type="text" name="aireA[modelo]"  class="form-control" placeholder="Introduzca modelo">
					                    </div>
				                      </div>
				                   
									<div class="col-sm-4">
				                      <div class="form-group">
				                        	<label>Serial:</label>
				                        		<input type="checkbox" name="no_posee" id="no_posee">No Posee
				                        		<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Serial','Secuencia de números y/o caracteres', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
					                        	<input type="text" name="aireA[serial]" id="serial_gen" class="form-control" placeholder="Introduzca serial">
					                    </div>
				                  </div>
				                 </div>

				                <div class="row">
				                     <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Inventario Cantv:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Inventario Cantv','Código interno de inventario en Cantv. Ej. 223344', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" class="form-control" name="aireA[inventario_cant]"  placeholder="Introduzca Inventario CANTV">
				                      </div>
				                    </div>
				           		</div>

				           		<div class="card-header">
			                		<h3 class="card-title">Composición:</h3>
			              		</div>
			              		<br>

				                <div class="row">
					                <div class="col-sm-4">
					                      <!-- text input -->
					                    <div class="form-group">
					                       <label>Tipo de Equipo:</label>
					                       <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Tipo de Equipo','Seleccione el tipo de equipo', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
					                        <select id="UMA" name="aireA[tipo_equipo]"  class="form-control">
			                            		<option value="">Seleccione el tipo de equipo</option>
												<option value="1">Precisión</option>
												<option value="2">UMA</option>
												<option value="3">SPLIT</option>
												<option value="4">Compacto</option>
						            		</select>
						            	</div>
					                </div>
					                <div class="col-sm-4">
					                    <div class="form-group">
					                        <label>Tipo de Correa:</label>
					                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Tipo de Correa','Indique tipo de correa que utiliza el equipo', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
						                        <input name="aireA[tipo_correa]" type="text" class="form-control" placeholder="Ej: B52">
						                </div>
						            </div>
					                <div class="col-sm-4">
					                    <div class="form-group">
					                        <label>Número de Correa:</label>
					                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Número de Correa','Solo números sin letras. Ej. 01, 10, 20, etc.', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
						                        <input name="aireA[nro_correa]" type="text" class="form-control" placeholder="Ej: 20">
						                </div>
						            </div>
						            <div class="col-sm-4">
					                    <div class="form-group">
					                        <label>Cantidad de Correas:</label>
					                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cantidad de Correas','Solo números sin letras. Ej. 01, 10, 20, etc.', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
						                        <input name="aireA[cant_correa]" type="text" class="form-control" placeholder="Ej: 02">
						                </div>
						            </div>
					                <div class="col-sm-4">
					                      <!-- text input -->
					                    <div class="form-group">
					                       <label>Número de Circuitos:</label>
					                       <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Número de Circuitos','Seleccione el número de circuitos', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
					                        <select name="aireA[nro_circuitos]" class="form-control">
					                        	<option value="">Seleccione estándar</option>
			                            		<option value="Monofásico">Monofásico</option>
												<option value="Bifásico">Bifásico</option>
												<option value="Trifásico">Trifásico</option>
											</select>
					                    </div>
					                </div>
					                <div id="tipo"></div>
					                <div class="col-sm-4" id="compresor_row">
					                      <!-- text input -->
					                    <div class="form-group">
					                       <label>Tipo de Compresor:</label>
					                       <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Tipo de Compresor','Seleccione el tipo de compresor', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
					                        <select id="tipo_compresor"  name="aireA[tipo_compresor]" class="form-control">
					                        	<option value="">Seleccione tipo de compresor</option>
			                            		<option value="Hermético">Hermético</option>
												<option value="Semi-Hermético">Semi-Hermético</option>
												<option value="Garrapata">Garrapata</option>
											</select>
					                    </div>
					                </div>
				                  	</div>
				                  	<div class="row">
				                  		<div class="col-sm-4">
					                        <div class="form-group">
					                        	<label>Toneladas de refrigeración instaladas:</label>
					                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Toneladas de Refrigeración Instaladas','En toneladas, solo números sin letras. Ej. 1, 10, 20, etc.', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
						                        <input name="aireA[ton_refrig_instalada]" type="text" class="form-control" placeholder="Ej: 20">
						                    </div>
						                </div>
				                  		<div class="col-sm-4">
					                        <div class="form-group">
					                        	<label>Toneladas de refrigeración operativas:</label>
					                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Toneladas de Refrigeración Operativas','En toneladas, solo números sin letras. Ej. 1, 10, 20, etc.', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
						                        <input name="aireA[ton_refrig_operativa]" type="text" class="form-control" placeholder="Ej: 20">
						                    </div>
						                </div>
						                <div class="col-sm-4">
					                      <!-- text input -->
					                      <div class="form-group">
					                        <label>Toneladas de refrigeración faltantes:</label>
					                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Toneladas de Refrigeración Faltantes','En toneladas, solo números sin letras. Ej. 1, 10, 20, etc.', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
					                        	<input name="aireA[ton_refrig_faltante]"  type="text" class="form-control" placeholder="Ej: 20">
					                      </div>
					                    </div>
				                  	</div>
				                    <div class="row">
				                    	<div class="col-sm-4">
					                        <div class="form-group">
					                        	<label>Tipo de Refrigerante:</label>
					                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Tipo de Refrigerante','Seleccione el tipo de refrigerante.', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
						                        	<select name="aireA[tipo_refrigerante]" class="form-control">
						                        		<option>Seleccione tipo de refrigerante</option>
						                            	<option value="R22">R22</option>
							                        	<option value="134A">134A</option>
							                        	<option value="410A">410A</option>
							                        	<option value="407C">407C</option>
							                        </select>
						                    </div>
						                </div>
					                    <div class="col-sm-4">
						                    <div class="form-group">
						                    	<label>Voltaje de Operación:</label>
						                    	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Voltaje de Operación','Marque el voltaje de operación.', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
						                        <div class="form-check">
						                          <input class="form-check-input" type="radio" value="208/230/3/60" name="aireA[volt_operacion]">
						                          <label class="form-check-label">208/230/3/60</label>
						                        </div>
						                        <div class="form-check">
						                          <input class="form-check-input" type="radio" value="460/3/60" name="aireA[volt_operacion]">
						                          <label class="form-check-label">460/3/60</label>
						                        </div>
					                      	</div>
					                     </div>
					                     <div class="col-sm-4">
						                    <div class="form-group">
						                    	<label>¿Posee Tablero de Control?</label>
						                    	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Tablero de Control','Indique si tiene tablero de control', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
						                        <div class="form-check">
						                          <input class="form-check-input" type="radio" value="si" name="aireA[tablero_control]">
						                          <label class="form-check-label">Si</label>
						                        </div>
						                        <div class="form-check">
						                          <input class="form-check-input" type="radio" value="no" name="aireA[tablero_control]">
						                          <label class="form-check-label">No</label>
						                        </div>				                        
					                      	</div>
					                     </div>
					                 </div>
									<!--
				                 	 <div class="card-footer" align="right">
					                  <button type="submit" class="btn btn-primary">Guardar</button>
					                </div>-->
				              </div>
				              <!-- /.card-body -->
				            </div>
				        </div>

                  	<div class="col-md-12">
			            <div class="card card-success">
			              <div class="card-header">
			                <h3 class="card-title">Instalación Equipo de A/A</h3>
			              </div>
			              <!-- /.card-header -->
			              <div class="card-body">
			                <form role="form">
			                  <div class="row">
			                    <div class="col-sm-4">
			                      <!-- text input -->
			                      <div class="form-group">
			                        <label>Operatividad:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Operatividad','Seleccione el estado en que se encuentra el equipo', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        <select name="aireA[operatividad]" id="operatividad" class="form-control">
		                            	<option value="">Indique operatividad del equipo</option>
										<option value="Optimo">Optimo</option>
										<option value="Critico">Critico</option>
										<option value="No Operativo">No Operativo</option>
										<option value="Deficiente">Deficiente</option>
										<option value="Vandalizado">Vandalizado</option>
									</select>
			                      </div>
			                    </div>
			                    <div class="col-sm-4">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Fecha de Instalación:</label>
			                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Fecha de Instalación','Introduzca fecha', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        	<input name="aireA[fecha_instalacion]" id="box_ModeloRectificador" class="form-control" type="date">
				                    </div>
			                      </div>
			                   
			                      <div class="col-sm-4">
			                      <!-- text input -->
			                      <div class="form-group">
			                        <label>Criticidad del Equipo:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Criticidad del Equipo','Seleccione criticidad. Por Ej. para su atención inmediata o posterior', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        <select  name="aireA[criticidad]"class="form-control">
		                            	<option value="">Indique criticidad del equipo</option>
										<option value="Alta">Alta</option>
										<option value="Media">Media</option>
										<option value="Baja">Baja</option>
									</select>
			                      </div>
			                    </div>
			                 </div>
			                 <div class="row" id="falla" style="display: none">
			                 	<div class="col-sm-4">
		                 			<label>Porcentaje Operatividad:</label>
		                 			<div class="form-group">
				                        <input name="aireA[porc_falla]" onblur="falla()" id="input_falla" disabled type="text" value="">
			                 		</div>
			                 	</div>
			                 </div>
			                 <div class="row">
			                  	<div class="col-sm-4">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>¿Equipo en Garantía?</label>
			                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Garantía','¿Tiene o está vigente la garantía?', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        	<div class="form-check">
						                          <input class="form-check-input" type="radio" value="si" name="aireA[garantia]">
						                          <label class="form-check-label">Si</label>
						                        </div>
						                        <div class="form-check">
						                          <input class="form-check-input" type="radio" value="no" name="aireA[garantia]">
						                          <label class="form-check-label">No</label>
						                        </div>				                    
						                    </div>
			                    </div>
			                </div>

			                 <div class="form-group">
								<label for="comment">Observaciones:</label>
								<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Observaciones','Información complementaria sobre seguimientos e incidencias a destacar. Solo de la instalación del AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								<textarea class="form-control" rows="3" name="aireA[observaciones]" id="comment"></textarea>			
							</div>
			                  	<!--
			                 	 <div class="card-footer" align="right">
				                  <button type="submit" class="btn btn-primary">Guardar</button>
				                </div>-->
				            </form>
			              </div>
			              <!-- /.card-body -->
			            </div>
                    	
			                 	 <div class="card-footer" align="right">
				                  <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
				                </div>
				                </form>
			              </div>
			              <!-- /.card-body -->
			            
			        </div>
                  </div>
                </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

@endsection
@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_aa") !!}
@endif



<script type="text/javascript">
    var tipo_compresor = document.getElementById('tipo_compresor');
    //var tipo = document.getElementById('tipo');

  $('#UMA').click(function(){
     var valor = $(this).val();
      if (valor==2){
       	$('#tipo_compresor').prop('disabled', true);
       	$('#compresor_row').hide();
        tipo_compresor.value="no aplica"
        //tipo.append('<input id="input_tipo" name="tipo_compresor" value="no aplica" >');

      }else{
        $('#tipo_compresor').prop('disabled', false);
       	$('#compresor_row').show();
        //$('#region').prop('disabled', true);
        tipo_compresor.value=""
      }
  });

  function falla(){
  	var falla = document.getElementById('input_falla')
  	var range = document.getElementById('range')
  	var opera = document.getElementById('operatividad')
  	console.log(falla.value)

  	range.value == falla.value
  	if(falla.value < 1){
  		opera.value == "Critico"
  	}else if(falla.value>99){
  		opera.value == "Optimo"
  	}

  }

 $("#no_posee").click(function(){
	var no_posee_gen = document.getElementById('no_posee');
	var serial_gen = document.getElementById('serial_gen');
	if(no_posee_gen.checked){
        	//$('#serial_gen').hide();
        	serial_gen.value="no posee"
        	$('#serial_gen').prop('readonly', true);
 		}
 	if(!no_posee_gen.checked){
        	//$('#serial_gen').hide();
        	serial_gen.value=""
        	$('#serial_gen').prop('readonly', false);
 		}

 	});

 $('select[id=operatividad]').change(function(){
 	var valor = $(this).val();
 	//console.log(valor)
 	if (valor == "Deficiente"){
 		$('#falla').show();
		$('#input_falla').prop('disabled',false);
 	}else{
		$('#falla').hide();
		$('#input_falla').prop('disabled',true);

 	}



 });



$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
  $(document).ready(function(){



  	$('select[id=region]').change(function () {
    	console.log('hola')
    var region_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'estado/' + region_id, function (cidades) {
        $('select[id=estado]').empty();

        if(cidades.length != 0){
            $('select[id=estado]').append('<option value=""> Selecciona un estado</option>');
            $.each(cidades, function (key, value) {
                $('select[id=estado]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });

    });


	$('select[id=estado]').change(function () {
    	//console.log('hola')
    var estado_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'central/' + estado_id, function (cidades) {
        $('select[id=central]').empty();

        if(cidades.length != 0){
            $('select[id=central]').append('<option value=""> Selecciona un central</option>');
            $.each(cidades, function (key, value) {
                $('select[id=central]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });

    });


	});
});
</script>
@endsection


