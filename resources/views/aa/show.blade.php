@extends ('/layouts/index')

@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Aire Acondicionado 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inventario</a></li>
              <li class="breadcrumb-item active">Aire Acondicionado</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
          <div class="col-md-6">
            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fa fa-cogs"></i>
                  <h3>Aire Acondicionado</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <dl>
                  <dt>Marca</dt>
                  <dd>{{$aire->marca}}</dd>
                  <dt>Modelo</dt>
                  <dd>{{$aire->modelo}}</dd>
                  <dt>Serial</dt>
                  <dd>{{$aire->serial}}</dd>
                  <dt>Tipo de equipo</dt>
                  <dd>{{$aire->tipo_equipo}}</dd>
                  <dt>Tipo de Correa</dt>
                  <dd>{{$aire->tipo_correa}}</dd>
                  <dt>Número de Correa</dt>
                  <dd>{{$aire->nro_correa}}</dd>
                  <dt>Cantidad de Correas</dt>
                  <dd>{{$aire->cant_correa}}</dd>
                  <dt>Numero de Circuito</dt>
                  <dd>{{$aire->num_circuito}}</dd>
                  <dt>Tipo de Compresor</dt>
                  <dd>{{$aire->tipo_compresor}}</dd>
                  <dt>Tipo de Refrigerante</dt>
                  <dd>{{$aire->tipo_refrigerante}}</dd>
                  <dt>Toneladas de Refrigeración Instaladas</dt>
                  <dd>{{$aire->tipo_refrigerante}}</dd>
                  <dt>Toneladas de refrigeración operativas</dt>
                  <dd>{{$aire->ton_refrig_operativa}}</dd>
                  <dt>Toneladas de refrigeración faltantes</dt>
                  <dd>{{$aire->ton_refrig_faltante}}</dd>
                  <dt>Voltaje de Operación</dt>
                  <dd>{{$aire->volt_operacion}}</dd>
                  <dt>Tablero de Control</dt>
                  <dd>{{$aire->tablero_control}}</dd>
                   <dt>Inventario Cantv</dt>
                  <dd>{{$aire->inventario_cant}}</dd>
                  <dt>Operatividad</dt>
                  <dd>{{$aire->operatividad}}</dd>
                  <dt>Fecha de Instalacion</dt>
                  <dd>{{$aire->fecha_instalacion}}</dd>
                  <dt>Criticidad del Equipo</dt>
                  <dd>{{$aire->criticidad}}</dd>
                  <dt>Observaciones/dt>
                  <dd>{{$aire->observaciones}}</dd>
                </dl>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            


          </div>
          <!-- ./col -->
          <div class="col-md-6">
           
            <div class="row">
              <div class="col-md-12">
                <div class="card card-primary">
                  <div class="card-header">
                    <h3 class="card-title">
                      <i class="fa fa-user"></i>
                          Responsable
                    </h3>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <dl>
                      <dt>Nombre y Apellido</dt>
                      <dd>{{$aire->ubicacion->responsable->nombre}} / 
                     {{$aire->ubicacion->responsable->apellido}}</dd>
                      <dt>Cédula</dt>
                      <dd>{{$aire->ubicacion->responsable->cedula}}</dd>
                      <dt>Cargo</dt>
                      <dd>{{$aire->ubicacion->responsable->cargo}}</dd>
                      <dt>Numero de Oficina</dt>
                      <dd>{{$aire->ubicacion->responsable->num_oficina}}</dd>
                      <dt>Numero de Personal</dt>
                      <dd>{{$aire->ubicacion->responsable->num_personal}}</dd>
                      <dt>Numero de Poo</dt>
                      <dd>{{$aire->ubicacion->responsable->cod_p00}}</dd>
                      <dt>Correo</dt>
                      <dd>{{$aire->ubicacion->responsable->correo}}</dd>
                    </dl>
                  </div>
                  <!-- /.card-body -->
                </div>
              </div>
              
            </div>

            <div class="row">
              <div class="col-md-12">
                <div class="card card-primary">
                  <div class="card-header">
                    <h3 class="card-title">
                      <i class="fa fa-map"></i>
                          Ubicación
                    </h3>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <dl>
                      <dt>Central/Localidad</dt>
                      <dd>{{$aire->ubicacion->central->localidad->nombre}}</dd>
                      <dt>Estado</dt>
                      <dd>{{$aire->ubicacion->central->estados->nombre}} </dd>
                      <dt>Regiòn</dt>
                      <dd>{{$aire->ubicacion->central->region->nombre}} </dd>
                      <dt>Piso</dt>
                      <dd>{{$aire->ubicacion->piso}} </dd>
                      <dt>Estructura</dt>
                      <dd>{{$aire->ubicacion->estructura}}</dd>
                      <dt>Sala</dt>
                      <dd>{{$aire->ubicacion->sala}} </dd>
                      <dt>Criticidad de espacio</dt>
                      <dd>{{$aire->ubicacion->criticidad_esp}} </dd>
                     
                    </dl>
                  </div>
                  <!-- /.card-body -->
                </div>
              </div>
            </div>



          </div>
          <!-- ./col -->
       
          </div>
        
      </div>
    </div>
  </section>
</div>
@endsection