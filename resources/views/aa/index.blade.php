@extends ('/layouts/index')

@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Aire Acondicionado 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inventario</a></li>
              <li class="breadcrumb-item active">Aire Acondicionado</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Vista Preliminar de Registros</h3>
              <div align="right">
                  <a href="/aire_acondicionado/create" class="btn btn-success">Nuevo Registro</a>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class=" display nowrap table table-bordered table-striped" style="width:100%">
                <thead>
                  <tr align="center">
                    <th class="not-export-col">Acciones</th>
                    <th>Región</th>
                    <th>Estado</th>
                    <th>Central</th>
                    <th>Estructura</th>
                    <th>Sala</th>
                    <th>Piso</th>
                    <th>Responsable</th>
                    <th>Marca</th>
                    <th>Modelo</th>
                    <th>Serial</th>
                    <th>Inventario CANTV</th>
                    <th>Tipo de Equipo</th>
                    <th>Tipo de Correa</th>
                    <th>Número de Correa</th>
                    <th>Cantidad de Correas</th>
                    <th>Número de Circuitos</th>
                    <th>Tipo de Compresor</th>
                    <th>Toneladas de refrigeración Instaladas</th>
                    <th>Toneladas de refrigeración Operativas</th>
                    <th>Toneladas de refrigeración Faltantes</th>
                    <th>Tipo de Refrigerante</th>
                    <th>Voltaje de Operación</th>
                    <th>Tablero de Control</th>
                    <th>Operatividad</th>
                    <th>Porcentaje Operativo</th>
                    <th>Fecha de Instalación</th>
                    <th>Criticidad del Equipo</th>
                    <th>Garantía</th>
                    <th>Observaciones</th>
                    <th>Fecha Attualización</th>
                    <th>Usuario</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($aa as $aire)
                  <tr align="center"  id="tdId_{{$aire->id}}">
                    <td>
                      <a href="/aire_acondicionado/{{encrypt($aire->id)}}/edit" class="btn btn-warning"><i class="far fa-edit"></i></a>
                      <a href="/aire_acondicionado/{{encrypt($aire->id)}}" class="btn btn-success"><i class="far fa-eye"></i></a>
                      <a onclick="deleteItem('{{$aire->id}}')" class="btn btn-danger"><i class="fa fa-times"></i></a>
                    </td>
                    <td>{{$aire->ubicacion->central->region->nombre}}</td>
                    <td>{{$aire->ubicacion->central->localidad->estados->nombre?? 'Inexistente'}}</td>
                    <td>{{$aire->ubicacion->central->localidad->nombre?? 'Inexistente'}}</td>
                    <td>{{$aire->ubicacion->central->localidad->tipo ?? 'S/N'}}</td>
                    <td>{{$aire->ubicacion->sala}}</td>
                    <td>{{$aire->ubicacion->piso}}</td>
                    <td>
                      {{$aire->ubicacion->responsable->nombre}}/
                      {{$aire->ubicacion->responsable->apellido}}/
                      {{$aire->ubicacion->responsable->cod_p00}}/
                      {{$aire->ubicacion->responsable->num_personal}}/
                      {{$aire->ubicacion->responsable->correo}}
                    </td>
                    <td>{{$aire->marca}}</td>
                    <td>{{$aire->modelo}}</td>
                    <td>{{$aire->serial}}</td>
                    <td>{{$aire->inventario_cant}}</td>
                    <td>
                      @if($aire->tipo_equipo == 1)
                        Precisión
                      @endif
                      @if($aire->tipo_equipo == 2)
                      UMA
                      @endif
                      @if($aire->tipo_equipo == 3)
                        SPLIT
                      @endif
                      @if($aire->tipo_equipo == 4)
                        Compacto
                      @endif

                      </td>
                    <td>{{$aire->tipo_correa}}</td>
                    <td>{{$aire->nro_correa}}</td>
                    <td>{{$aire->cant_correa}}</td>
                    <td>{{$aire->nro_circuitos}}</td>
                    <td>{{$aire->tipo_compresor}}</td>
                    <td>{{$aire->ton_refrig_instalada}}</td>
                    <td>{{$aire->ton_refrig_operativa}}</td>
                    <td>{{$aire->ton_refrig_faltante}}</td>
                    <td>{{$aire->tipo_refrigerante}}</td>
                    <td>{{$aire->volt_operacion}}</td>
                    <td>{{$aire->tablero_control}}</td>
                    <td>{{$aire->operatividad}}</td>
                    <td class="grafic">
                      @if($aire->operatividad =='Falla' && $aire->porc_falla)
                        {{$aire->porc_falla}} %
                      @else
                       @if($aire->operatividad == "Inoperativo" ||  $aire->operatividad == "Vandalizado" ) 0 % @else  100%   @endif
                      @endif
                    </td>


                    <td>{{$aire->fecha_instalacion}}</td>
                    <td>{{$aire->criticidad}}</td>
                    <td>{{$aire->garantia}}</td>
                    <td>{{$aire->observaciones}}</td>
                    <td>{{$aire->created_at}}</td>
                    <td>{{$aire->usuario->name ?? 'Sin Cambios'}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
           </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('scriptsalerts')


<script>
   @if(Session::has('exito'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Aire Acondicionado Guardado con exito'
      })
      })
    });
  @endif
   @if(Session::has('actualizado'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Datos actualizados con exito'
      })
      })
    });
  @endif
  
</script>

  <script>
    function deleteItem(item){
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Desea eliminar este Aire Acondicionado del intentario?',
      text: "Estas por quitar este equipo del Inventario!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, quitarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {

      if (result.value) {



            $.ajax({
                url: "/aire_acondicionado/"+item,
                type:"DELETE",
                   headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    

                    success:function(result, status, xhr){                    
                        
                        if (status == 'success') {

                          swalWithBootstrapButtons.fire(
                            'Quitado con Exito!',
                            'Se eliminó con exito',
                            'success'
                          )

                          $("#tdId_"+item).hide();
                         // txt.innerHTML = result ;



                        }
                    },
                    error:function(xhr, status, error){
                      

                      Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se eliminó el producto, vuelva a intentarlo mas tarde '+error,
                        footer: '<a href>Seguramente hubo un problema de conexión</a>'
                      })
                      

                    }
                });
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No se ha quitado el producto.',
          'error'
        )
      }
    })
  }
</script>


@endsection