@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Estado 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Registro</a></li>
              <li class="breadcrumb-item active">Estado</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
     
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
              
              <div class="card-body">
              	 <form method="POST" id="formulario_usuarios" enctype="multipart/form-data" name="formulario_usuarios"  action="/estados/{{$estado->id}}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">

                     	<div class="col-md-12">
				            <!-- general form elements disabled -->
				            <div class="card card-success">
				              <div class="card-header">
				                <h3 class="card-title">Estado</h3>
				              </div>
				              <!-- /.card-header -->
				              <div class="card-body">
				              	 <div class="row">
				                    <div class="col-sm-6">
				                      <!-- text input -->
				                      	<div class="form-group">
                                  <label>Regiòn Asociada:</label>
                                    <select  name="region_id" id="region_id" class="form-control">
                                      <option>Seleccione una Regiòn</option>
                                        @foreach($region as $reg)
                                          <option @if($reg->id == $estado->region_id) selected @endif value="{{$reg->id}}">{{$reg->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
				                    </div>


                            <div class="col-sm-6">
                                <div class="form-group">
                                  <label>Nombre o Descripciòn</label>
                                    <input type="text" name="nombre" value="{{$estado->nombre}}"  class="form-control" placeholder="Nombre de Estado">
                              </div>
                             </div>

                            
				                   
									           

	                       </div>
				            </div>
				        </div>
		                 	 <div class="card-footer" align="right">
			                  <button type="submit" class="btn btn-primary">Crear</button>
			                </div>
			                </form>
                        </div>
		            </div>
		        </div>
            </div>
        </div>
  	</section>
</div>


@endsection
@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_usuarios") !!}
@endif



<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection





