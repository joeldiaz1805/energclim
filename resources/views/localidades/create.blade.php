@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Localidades 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Registro</a></li>
              <li class="breadcrumb-item active">Localidades</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
     
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
              
              <div class="card-body">
              	 <form method="POST" id="formulario_usuarios" enctype="multipart/form-data" name="formulario_usuarios"  action="{{ url('/localidades')}}">
                        {{ csrf_field() }}
                     	<div class="col-md-12">
				            <!-- general form elements disabled -->
				            <div class="card card-success">
				              <div class="card-header">
				                <h3 class="card-title">Localidad</h3>
				              </div>
				              <!-- /.card-header -->
				              <div class="card-body">
				              	 <div class="row">
				                    <div class="col-sm-6">
			                      	<div class="form-group">
                                <label>Estado Asociado:</label>
                                  <select  name="estado_id" id="estado_id" class="form-control">
                                    <option>Seleccione una Estado</option>
                                      @foreach($estados as $edo)
                                        <option value="{{$edo->id}}">{{$edo->nombre}}</option>
                                      @endforeach
                                  </select>
                              </div>
                            </div>
				                                              
                            <div class="col-sm-6">
                              <label>Municipio a asignar Parroquias:</label>
                              <div class="form-group">
                                <select name="municipio_id" id="municipio_id" class="form-control select2">
                                  <option>Seleccione un municipio</option>
                                  
                                </select>
                              </div>
                            </div>

                            <div style="display:none" class="col-sm-4">
                              <label>Parroquias a asignar:</label>
                              <div class="form-group">
                                <select disabled name="parroquia_id" id="parroquia_id" class="form-control select2">
                                  <option>Seleccione una Parroquia</option>
                                  
                                </select>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>Nombre o Descripciòn</label>
                                <input type="text" name="nombre"  class="form-control" placeholder="Nombre de Localidad">
                              </div>
                            </div>

                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>Tipo de Localidad</label>
                                <input type="text" name="tipo"  class="form-control" placeholder="Tipo de Localidad">
                              </div>
                            </div>

	                        </div>

                        <div class="card-header">
                          <h3 class="card-title">Supervisor encargado</h3>
                          <input type="checkbox" name="no_posee" id="no_posee">No Posee
                        </div>
                      <br>
                        <div class="row">
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Nombres:</label>
                              <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Nombres','Nombre persona encargada y coordinador del AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                              <input type="text" id="sname" class="form-control"  required name="supervisor[nombre]" placeholder="Ej: Pedro...">
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Apellidos:</label>
                              <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Apellidos','Apellido persona encargada y supervisor del AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                              <input type="text"id="slast"  class="form-control" required name="supervisor[apellido]" placeholder="Ej: Tortoza...">
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Cédula:</label>
                              <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cédula','Número de cédula', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                              <input type="text" id="sid" class="form-control" required name="supervisor[cedula]" placeholder="Introduzca cédula">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>P00:</label>
                              <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','P00','Número interno que identifica al trabajador de la empresa. Ej. 112233', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                              <input type="text" id="snum_office" class="form-control"  required name="supervisor[cod_p00]" placeholder="Introduzca P00">
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Número de Oficina:</label>
                              <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Número de Oficina','Número de Telf.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="fas fa-phone"></i></span>
                                </div>
                              <input type="text"id="snum_personal"  name="supervisor[num_oficina]"  required  class="form-control" data-inputmask='"mask": "99999999999"' data-mask>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="form-group">
                            <label>Número Personal:</label>
                            <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Número Personal','Número de Telf.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                            <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-phone"></i></span>
                              </div>
                              <input type="text" id="sp00" name="supervisor[num_personal]"  required  class="form-control" data-inputmask='"mask": "99999999999"' data-mask>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="form-group">
                            <label>Cargo:</label>
                            <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cargo','Denominación del cargo de la persona encargada del AA.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                            <input type="text" id="scargo" class="form-control" readonly value="Supervisor"  required name="supervisor[cargo]" placeholder="Introduzca cargo">
                          </div>
                        </div>
                        <div class="col-sm-8">
                          <div class="form-group">
                            <label>Correo:</label>
                            <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Correo','Dirección de correo electrónico. Ej. empleado@cantv.com.ve', 300 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                            <input type="email" id="scorreo" name="supervisor[correo]"  required class="form-control" placeholder="Introduzca correo corporativo/personal">
                          </div>
                        </div>
                      </div>
                      <div class="card-header">
                         <h3 class="card-title">Coordinador encargado</h3>
                      </div>
                      <br>
                        <div class="row">
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Nombres:</label>
                              <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Nombres','Nombre persona encargada y coordinador del AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                              <input type="text" id="cname" class="form-control" required name="coordinador[nombre]" placeholder="Ej: Pedro...">
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Apellidos:</label>
                              <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Apellidos','Apellido persona encargada y coordinador del AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                              <input type="text" id="clast" class="form-control" required name="coordinador[apellido]" placeholder="Ej: Tortoza...">
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Cédula:</label>
                              <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cédula','Número de cédula', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                              <input type="text" id="cid" class="form-control" required name="coordinador[cedula]" placeholder="Introduzca cédula">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>P00:</label>
                              <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','P00','Número interno que identifica al trabajador de la empresa. Ej. 112233', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                              <input type="text" id="c_numoffice" class="form-control" required name="coordinador[cod_p00]" placeholder="Introduzca P00">
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Número de Oficina:</label>
                              <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Número de Oficina','Número de Telf.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="fas fa-phone"></i></span>
                                </div>
                                <input type="text" id="c_numpersonal" name="coordinador[num_oficina]" required  class="form-control" data-inputmask='"mask": "99999999999"' data-mask>
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Número Personal:</label>
                              <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Número Personal','Número de Telf.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="fas fa-phone"></i></span>
                                </div>
                                <input type="text" id="cp00" name="coordinador[num_personal]" required  class="form-control" data-inputmask='"mask": "99999999999"' data-mask>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Cargo:</label>
                              <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cargo','Denominación del cargo de la persona encargada del AA.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                              <input type="text" id="c_cargo" class="form-control" readonly value="Coordinador" required name="coordinador[cargo]" placeholder="Introduzca cargo">
                            </div>
                          </div>
                          <div class="col-sm-8">
                            <div class="form-group">
                              <label>Correo:</label>
                              <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Correo','Dirección de correo electrónico. Ej. empleado@cantv.com.ve', 300 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                              <input type="email" id="c_correo" name="coordinador[correo]" required class="form-control" placeholder="Introduzca correo corporativo/personal">
                            </div>
                          </div>
                        </div>
				            </div>
	       			        </div>
		                 	 <div class="card-footer" align="right">
			                  <button type="submit"  class="btn btn-primary">Crear</button>
			                </div>
			                </form>
                        </div>
		            </div>
		        </div>
            </div>
        </div>
  	</section>
</div>


@endsection
@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_usuarios") !!}
@endif



<script type="text/javascript">
$(document).ready(function(){



    $('select[id=estado_id]').change(function () {
     console.log('hola')
    var estado_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'municipios_poredo/' + estado_id, function (cidades) {
        $('select[id=municipio_id]').empty();

        if(cidades.length != 0){
            $('select[id=municipio_id]').append('<option value=""> Selecciona un municipio</option>');
            $.each(cidades, function (key, value) {
                $('select[id=municipio_id]').append('<option value=' + value.id + '>'+ value.descripcion + '</option>');
            });
        }

        });

    });
      $('select[id=municipio_id]').change(function () {
      console.log('hola')
      var municipio_id = $(this).val();
      $('#parroquia_id').prop('disabled', false);

    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'parroquias_pormu/' + municipio_id, function (cidades) {
        $('select[id=parroquia_id]').empty();

        if(cidades.length != 0){
            $('select[id=parroquia_id]').append('<option value=""> Selecciona una parroquia</option>');
            $.each(cidades, function (key, value) {
                $('select[id=parroquia_id]').append('<option value=' + value.id + '>'+ value.descripcion + '</option>');
            });
        }

        });

    });
  });





$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
$("#no_posee").click(function(){
  var no_posee_gen = document.getElementById('no_posee');
  if(no_posee_gen.checked){
          $('#sname').prop('disabled', true);
          $('#slast').prop('disabled', true);
          $('#sid').prop('disabled', true);
          $('#snum_office').prop('disabled', true);
          $('#snum_personal').prop('disabled', true);
          $('#sp00').prop('disabled', true);
          $('#scargo').prop('disabled', true);
          $('#scorreo').prop('disabled', true);

          $('#cname').prop('disabled', true);
          $('#clast').prop('disabled', true);
          $('#cid').prop('disabled', true);
          $('#c_numoffice').prop('disabled', true);
          $('#c_numpersonal').prop('disabled', true);
          $('#cp00').prop('disabled', true);
          $('#c_cargo').prop('disabled', true);
          $('#c_correo').prop('disabled', true);
    }
  if(!no_posee_gen.checked){
          $('#sname').prop('disabled', false);
          $('#slast').prop('disabled', false);
          $('#sid').prop('disabled', false);
          $('#snum_office').prop('disabled', false);
          $('#snum_personal').prop('disabled', false);
          $('#sp00').prop('disabled', false);
          $('#scargo').prop('disabled', false);
          $('#scorreo').prop('disabled', false);

          $('#cname').prop('disabled', false);
          $('#clast').prop('disabled', false);
          $('#cid').prop('disabled', false);
          $('#c_numoffice').prop('disabled', false);
          $('#c_numpersonal').prop('disabled', false);
          $('#cp00').prop('disabled', false);
          $('#c_cargo').prop('disabled', false);
          $('#c_correo').prop('disabled', false);
    }

  });
</script>
@endsection





