@extends ('/layouts/index')

@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Localidades 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Monitoreo</a></li>
              <li class="breadcrumb-item active">Localidades</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Registro de Localidades</h3>
              <div align="right">
                  <a href="/localidades/create" class="btn btn-success">Agregar Localidad</a>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class=" display table table-bordered table-striped nowrap" style="width:100%">
                <thead>
                  <tr align="center">
                    <th class="not-export-col">Acciòn</th>
                    <th>Region</th>
                    <th>Estado</th>
                    <th>Municipio</th>
                    <th>Parroquia</th>
                    <th>Localidad</th>
                    <th>Latitud</th>
                    <th>Longitud</th>
                    <th>Tipo</th>
                    <th>Supervisor</th>
                    <th>Coordinador</th>
                    <th>Fecha Creaciòn</th>
                    <th>Fecha Alteración</th>
                    <th>Alterado Por:</th>
                    
                  </tr>
                </thead>
                <tbody>
                  @foreach($central as $local)
                  <tr align="center" id="tdId_{{$local->id}}" >
                    <td>
                      @if($local->estados)
                      <a href="/localidades/{{encrypt($local->id)}}/edit" class="btn btn-warning"><i class="far fa-edit"></i></a>
                        @if($local->estados->region_id != 6 || Auth::user()->email == "joeldiaz1805@gmail.com")
                        @endif
                      @endif
                      <!-- <a href="#" class="btn btn-success"><i class="far fa-eye"></i></a> -->
                      @if(Auth::user()->email == "joeldiaz1805@gmail.com" || Auth::user()->email == "zmejia01@cantv.com.ve")
                        <a onclick="deleteItem('{{$local->id}}')" class="btn btn-danger"><i id="ele"  class="fa fa-user-times"></i></a>
                      @endif
                      </td>
                    
                    <td>@if($local->estados)
                          @if($local->estados->region)
                            {{$local->estados->region->nombre}}
                          @else
                          -
                          @endif
                      @endif
                       </td>
                    <td>@if($local->estados)
                      {{$local->estados->nombre}}
                      @else
                      -
                      @endif </td>
                      <td>{{$local->municipio->descripcion ?? 'S/N'}}</td>
                      <td>{{$local->parroquia->descripcion ?? 'S/N'}}</td>
                    <td>{{$local->nombre}} </td>
                    <td>{{$local->latitud}} </td>
                    <td>{{$local->longitud}} </td>
                    <td>{{$local->tipo}} </td>
                    <td>{{$local->supervisor->nombre ?? 'Sin asignar'}} {{$local->supervisor->apellido ?? 'Sin asignar'}} </td>
                    <td>{{$local->coordinador->nombre ?? 'Sin asignar'}} {{$local->coordinador->apellido ?? 'Sin asignar'}} </td>
                    <td>{{$local->created_at}} </td>
                    <td>{{$local->updated_at}} </td>
                    <td>{{$local->usuario->name ?? 'Sin Cambios'}} </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
           </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('scriptsalerts')


<script>
   @if(Session::has('exito'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Localidad Guardada con exito'
      })
      })
    });
  @endif
   @if(Session::has('actualizado'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Datos actualizados con exito'
      })
      })
    });
  @endif
  
</script>

  <script>
    function deleteItem(item){
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Desea eliminar esta localidad ?',
      text: "Estas por quitar esta localidad!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, quitarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {


      if (result.value) {



            $.ajax({
                url: "/localidades/"+item,
                type:"DELETE",
                   headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    

                    success:function(result, status, xhr){                    
                        
                        if (status == 'success') {

                          swalWithBootstrapButtons.fire(
                            'Quitado con Exito!',
                            'Se eliminó con exito',
                            'success'
                          )

                          $("#tdId_"+item).hide();
                         // txt.innerHTML = result ;



                        }
                    },
                    error:function(xhr, status, error){
                      

                      Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se eliminó localidad, vuelva a intentarlo mas tarde '+error,
                        footer: '<a href>Seguramente hubo un problema de conexión</a>'
                      })
                      

                    }
                });
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No se ha quitado el producto.',
          'error'
        )
      }
    })
  }
</script>


@endsection