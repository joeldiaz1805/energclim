@extends ('/layouts/index')

@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Tickets energia -  Sismyc 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Monitoreo</a></li>
              <li class="breadcrumb-item active">Tickets Sismyc</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Registro de acciones</h3>
            </div>
            <div class="card-body">
              <table id="example1" class=" display table table-bordered table-striped nowrap" style="width:100%">
                <thead>
                  <tr align="center">
                    <th >Nro Ticket</th>
                    <th>Estatus</th>
                    <th>Origen</th>
                    <th>Originador</th>
                    <th>Prioridad</th>
                    <th>Especialidad</th>
                    <th>Area Asignada</th>
                    <th>Localidad</th>
                    <th>Tipo de Localidad</th>
                    <th>ACCION</th>
                    <th>EQUIPO</th>
                    <th>SALA</th>
                    <th>PISO</th>
                    <th>AULA</th>
                    <th>REPORTADO POR</th>
                    <th>TLF</th>
                    <th>INFORMACION ADICIONAL</th>
                    <th>SERVICIO</th>
                    <th>ESTADO</th>
                    <th>REGION</th>
                    <th>ACTIVIDAD</th>
                    <th>PROVEEDOR</th>
                    <th>MUNICIPIO</th>
                    <th>Falla</th>
                    <th>Plataforma</th>
                    <th>Hora de Inicio Falla</th>
                    <th>Hora de diagnostico</th>
                    <th>Tiempo de diagnostico</th>
                    <th>Hora contacto al Tecnico</th>
                    <th>Hora de llegada al sitio</th>
                    <th>Hora localizacion de la falla</th>
                    <th>Fecha fin de la falla</th>
                    <th>Hora restablecimiento de servicio</th>
                    <th>Duracion del evento en Horas</th>
                    <th>Cerrado por</th>
                    <th>Fecha de Creación</th>
                    <th>Impacto E1</th>
                    <th>Impacto abonado de voz</th>
                    <th>Impacto puerto ABA</th>
                    <th>Impacto Radio Base</th>
                    <th>Impacto circuitos</th>
                    <th>Impacto circuitos Movilnet</th>
                    <th>Elementos de red Afectados</th>
                    <th>Elementos de red Causo falla</th>
                    <th>Detalles de Incidente</th>
                    <th>Causa</th>
                    <th>Accion Tomada</th>
                    <th>Tramo</th>
                    <th>Fecha de Modificacion</th>
                    <th>Persona que Modificò</th>
                    <th>Comentario</th>
                    <th>Localidad (E-R)</th>
                    <th>Estado (E-R)</th>
                    <th>Region (E-R)</th>
                    <th>Tipo localidad  (E-R)</th>
                    <th>Numero de Evidencias</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($re as $reporte)
                  <tr align="center" >
                    @php $descripcion = explode('_',$reporte->descripcion_reporte); @endphp
                    <td>{{$reporte->numero_ticket}} </td>
                    <td>{{$reporte->ticket_status->nombre_estado}} </td>
                    <td>Telefono </td>
                    <td>{{$reporte->cor_responsible->nombre_responsable}} </td>
                  

                    <td>{{$reporte->ticket_severity->nombre_severidad}} </td>
                    <td>{{$reporte->ticket_group->nombre_grupo}} </td>
                    <td>{{$reporte->resolutive_area->nombre_area ?? 'sin valor'}} </td>
                    <td>{{ $descripcion[0] ?? 'valor null' }}</td>
                    <td>{{ $descripcion[1] ?? 'valor null' }}</td>
                    <td>{{ $descripcion[2] ?? 'valor null'}}</td>
                    <td>{{ $descripcion[3] ?? 'valor null'}}</td>
                    <td>{{ $descripcion[4] ?? 'valor null'}}</td>
                    <td>{{ $descripcion[5] ?? 'valor null'}}</td>
                    <td>{{ $descripcion[6] ?? 'valor null'}}</td>
                    <td>{{ $descripcion[7] ?? 'valor null'}}</td>
                    <td>{{ $descripcion[8] ?? 'valor null'}}</td>
                    <td>{{ $descripcion[9] ?? 'valor null'}}</td>
                    <td>{{ $descripcion[10] ?? 'valor null'}}</td>
                    <td>{{ $descripcion[11] ?? 'valor null'}}</td>
                    <td>{{ $descripcion[12] ?? 'valor null'}}</td>
                    <td>{{ $descripcion[13] ?? 'valor null'}}</td>
                    <td>{{ $descripcion[14] ?? 'valor null'}}</td>
                    <td>{{ $descripcion[15] ?? 'valor null'}}</td>
                    <td>{{$reporte->ticket_failure->nombre_falla ?? 'Not Data'}}</td>
                    <td>{{$reporte->ticket_platform->nombre_plataforma}} </td>
                    <td>{{$reporte->hora_inicio_ticket}} </td>
                    <td>{{$reporte->hora_diagnostico_cor}} </td>
                    <td>{{\Carbon\Carbon::parse($reporte->hora_inicio_ticket)->diffForHumans(\Carbon\Carbon::parse($reporte->hora_diagnostico_cor))}} </td>
                    <td>{{$reporte->hora_contacto_tecnico }} </td>
                    <td>{{ $reporte->hora_llegada_sitio }} </td>
                    <td>{{$reporte->hora_inicio_reparacion}} </td>
                    <td>{{$reporte->hora_final_evento}} </td>
                    <td>{{\Carbon\Carbon::parse($reporte->hora_restablecimiento_servicio)->format('d-m-Y h:i:s a')}} </td>
                    <td>{{\Carbon\Carbon::parse($reporte->hora_inicio_ticket)->diffForHumans(\Carbon\Carbon::parse($reporte->hora_final_evento))}} </td>
                    <td>{{$reporte->cor_responsible->nombre_responsable}}</td>
                    <td>{{$reporte->created_at}} </td>
                    <td>{{$reporte->impacto_e1 ?? 'N/A'}} </td>
                    <td>{{$reporte->impacto_voz ?? 'N/A'}} </td>
                    <td>{{$reporte->impacto_aba ?? 'N/A'}} </td>
                    <td>{{$reporte->impacto_radio_base ?? 'N/A'}} </td>
                    <td>{{$reporte->impacto_circuitos ?? 'N/A'}} </td>
                    <td>{{$reporte->impacto_circuitos_movilnet ?? 'N/A'}} </td>
                    <td>{{$reporte->network_elements[0]->nombre_elemento_red ?? 'N/A'}} </td>
                    <td>{{$reporte->elemento_red_causo_falla ?? 'N/A'}} </td>
                    <td>{{$reporte->incident_detail_id ?? 'N/A'}} </td>
                    <td>{{$reporte->causa ?? 'N/A'}} </td>
                    <td>{{$reporte->accion_tomada ?? 'N/A'}} </td>
                    <td>{{$reporte->tramo ?? 'N/A'}} </td>
                    <td>{{$reporte->updated_at}} </td>
                    <td>{{$reporte->last_comment->cor_responsible->nombre_responsable ?? 'S/C'}}</td>
                    <td>{{$reporte->last_comment->comment}}</td>
                    <td>{{$reporte->network_elements[0]->location->Nombre_localidad ?? 'N/A'}} </td>
                    <td>{{$reporte->network_elements[0]->location->state->Nombre_estado ?? 'N/A'}} </td>
                    <td>{{$reporte->network_elements[0]->location->state->region->Nombre_region ?? 'N/A'}} </td>
                    <td>{{$reporte->network_elements[0]->network_element_type->nombre_tipo_elemento_red ?? 'N/A'}} </td>
                    <td>{{$reporte->ticket_evidences_count}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
           </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('scriptsalerts')


<script>
   @if(Session::has('exito'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Estado Guardado con exito'
      })
      })
    });
  @endif
   @if(Session::has('actualizado'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Datos actualizados con exito'
      })
      })
    });
  @endif
  
</script>

  <script>
    function deleteItem(item){
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Desea eliminar esta Sala ?',
      text: "Estas por quitar esta Sala!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, quitarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {


      if (result.value) {



            $.ajax({
                url: "/acciones/"+item,
                type:"DELETE",
                   headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    

                    success:function(result, status, xhr){                    
                        
                        if (status == 'success') {

                          swalWithBootstrapButtons.fire(
                            'Quitado con Exito!',
                            'Se eliminó con exito',
                            'success'
                          )

                          $("#tdId_"+item).hide();
                         // txt.innerHTML = result ;



                        }
                    },
                    error:function(xhr, status, error){
                      

                      Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se eliminó la Sala, vuelva a intentarlo mas tarde '+error,
                        footer: '<a href>Seguramente hubo un problema de conexión</a>'
                      })
                      

                    }
                });
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No se ha quitado el Estado.',
          'error'
        )
      }
    })
  }
</script>


@endsection