@extends ('/layouts/index')

@section ('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Registro de ticket resueltos por "Disponibilidad de Servicio"
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Sismyc</a></li>
              <li class="breadcrumb-item active">graficas</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
     
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card">
              <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">Registro de Ticket por tipo de Falla</h3>
                
                </div>
              </div>
             
                <div class="card-body">
                  
                  <table id="example1" class=" display table table-bordered table-striped nowrap" style="width:100%">
                    <thead>
                      <tr align="center">
                        <th class="not-export-col">Descripcion</th>
                        <th>Cantidad</th>
                        <th>% </th>
                      </tr>
                    </thead>
                    <tbody>
                            
                      @foreach($result as $valores)
                      <tr align="center" >
                        <td>{{$valores['falla']}} </td>
                        <td>{{$valores['items']}} </td>
                        <td>{{round(($valores['items']* 100)/$contdor)}} %</td>
                           
                      </tr>
                      @endforeach
                    </tbody>
                    <tfoot align="center">
                  <tr>
                    <th>SubTotal:</th>
                    <th>{{$contdor}}</th>
                    <th>100%</th>
                  </tr>
                </tfoot>
                  </table>
                </div>
              <div class="card-body">
                <div class="d-flex">
                  <p class="d-flex flex-column">
                  </p>
                  <p class="ml-auto d-flex flex-column text-right">
                    
                    <span class="text-muted">Registros del ultimo mes</span>
                  </p>
                </div>
                <!-- /.d-flex -->

                <div class="position-relative mb-4">
                  <canvas id="sales-chart" height="200"></canvas>
                </div>

                <div class="d-flex flex-row justify-content-end">
                  <span class="mr-2">
                    <i class="fas fa-square text-primary"></i> Fallas Registradas
                  </span>
                </div>
              </div>
            </div>
            <!-- /.card -->

            <input type="hidden" name="datos"  id="datos" value="{{$var}}">
            </div>
        </div>
  	</section>
</div>


@endsection
@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_usuarios") !!}
@endif

<script type="text/javascript">
 function calcular(){

    var finicio =moment(document.getElementById('fechaInicio').value);
    var ffin =moment(document.getElementById('fechaFin').value);
    var dias = ffin.diff(finicio,'days');
    console.log(ffin.diff(finicio,'days'),'dias');

  
/*

     if(dias > 31){
        toastr.error('El Rango de fecha supera los 31 dias permitidos');
        $('#crear').prop('disabled',true);
      }else{
        
        $('#crear').prop('disabled',false);
      }

*/


 }




  
$(document).ready(function () {
  




    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas = $('#barChart').get(0).getContext('2d')
    var barChartData = $.extend(true, {}, areaChartData)
    var temp0 = areaChartData.datasets[0]
    var temp1 = areaChartData.datasets[1]
    barChartData.datasets[0] = temp1
    barChartData.datasets[1] = temp0

    var barChartOptions = {
      responsive              : true,
      maintainAspectRatio     : false,
      datasetFill             : false
    }

    new Chart(barChartCanvas, {
      type: 'bar',
      data: barChartData,
      options: barChartOptions
    })

    //---------------------
    //- STACKED BAR CHART -
    //---------------------
    var stackedBarChartCanvas = $('#stackedBarChart').get(0).getContext('2d')
    var stackedBarChartData = $.extend(true, {}, barChartData)

    var stackedBarChartOptions = {
      responsive              : true,
      maintainAspectRatio     : false,
      scales: {
        xAxes: [{
          stacked: true,
        }],
        yAxes: [{
          stacked: true
        }]
      }
    }

    new Chart(stackedBarChartCanvas, {
      type: 'bar',
      data: stackedBarChartData,
      options: stackedBarChartOptions
    })
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
<!-- jQuery -->
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE -->
<script src="{{asset('dist/js/adminlte.js')}}"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="{{asset('plugins/chart.js/Chart.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('dist/js/pages/dashboard3.js')}}"></script>
@endsection




