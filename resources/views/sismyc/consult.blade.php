@extends ('/layouts/index')

@section ('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Escoja rango de Fecha a Consultar
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Sismyc</a></li>
              <li class="breadcrumb-item active">Rango de fecha</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
     
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
              
              <div class="card-body">
              	 <form method="GET" id="formulario_usuarios" enctype="multipart/form-data" name="formulario_usuarios"  action="{{ url('/tickets_ex')}}">
                    {{ csrf_field() }}
                   	<div class="col-md-12">
				            <div class="card card-success">
				              <div class="card-header">
				                <h3 class="card-title">Rango a Consultar</h3>
                        <div align="right" style="display: none">
                            <a href="/tickets_ex/create" class="btn btn-primary">Resumen</a>
                        </div>
				              </div>
				              <!-- /.card-header -->
				              <div class="card-body">
                        <div class="row">
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label>Indique fecha inicio:</label>
                              <input type="date" name="fechaInicio" id="fechaInicio" class="form-control" placeholder="Indique fecha inicio:">

                              
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label>Indique fecha fin:</label>
                              <input type="date" name="fechaFin" id="fechaFin" onblur="calcular()"  class="form-control" placeholder="ndique fecha fin:">
                            </div>
                          </div>
                        </div>
  				            </div>
				              </div>
		                 	 <div class="card-footer" align="right">
			                  <button type="submit"  id="crear"class="btn btn-primary">Consultar</button>
			                </div>
			                </form>
                    </div>
		            </div>
		        </div>
            </div>
        </div>
  	</section>
</div>


@endsection
@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_usuarios") !!}
@endif

<script type="text/javascript">
 
 function calcular(){

    var finicio =moment(document.getElementById('fechaInicio').value);
    var ffin =moment(document.getElementById('fechaFin').value);
    var dias = ffin.diff(finicio,'days');
    console.log(ffin.diff(finicio,'days'),'dias');

  
/*

     if(dias > 31){
        toastr.error('El Rango de fecha supera los 31 dias permitidos');
        $('#crear').prop('disabled',true);
      }else{
        
        $('#crear').prop('disabled',false);
      }

*/


 }




  
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection




