@extends ('/layouts/index')

@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Ups - Banco de Baterías
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inventario</a></li>
              <li class="breadcrumb-item active">Ups & Banco Bateria</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
          <div class="col-md-6">
            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fa fa-cogs"></i>
                  Ups
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <dl>
                  <dt>Marca</dt>
                  <dd></dd>
                  <dd>*Caso {{$reporte->numero_ticket}} 
                  {{$reporte->ticket_status->nombre_estado}} 
                  {{$reporte->ticket_platform->nombre_plataforma}} 
                  {{$reporte->ticket_severity->nombre_severidad}} 
                  {{$reporte->cor_responsible->nombre_responsable}} 
                  {{$reporte->resolutive_area->nombre_area}} 
                  {{$reporte->descripcion_reporte}} 
                  {{$reporte->created_at}} </dd>
                </dl>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <div class="row">
              <div class="col-md-12">
                <div class="card card-primary">
                  <div class="card-header">
                    <h3 class="card-title">
                      <i class="fa fa-map"></i>
                          Ubicaciòn
                    </h3>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <dl>
                      <dt>Central/Localidad</dt>
                      <dd>{{$ups_bancob->ubicacion->central->localidad->nombre}}</dd>
                      <dt>Estado</dt>
                      <dd>{{$ups_bancob->ubicacion->central->estados->nombre}} </dd>
                      <dt>Regiòn</dt>
                      <dd>{{$ups_bancob->ubicacion->central->region->nombre}} </dd>
                      <dt>Piso</dt>
                      <dd>{{$ups_bancob->ubicacion->piso}} </dd>
                      <dt>Estructura</dt>
                      <dd>{{$ups_bancob->ubicacion->estructura}}</dd>
                      <dt>Sala</dt>
                      <dd>{{$ups_bancob->ubicacion->sala}} </dd>
                      <dt>Criticidad de espacio</dt>
                      <dd>{{$ups_bancob->ubicacion->criticidad_esp}} </dd>
                     
                    </dl>
                  </div>
                  <!-- /.card-body -->
                </div>
              </div>
            </div>



          </div>
          <!-- ./col -->
          <div class="col-md-6">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fa fa-bolt"></i>
                      Banco de Baterias
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <dl>
                  <dt>Marca</dt>
                  <dd>{{$ups_bancob->bancob->marca}}</dd>
                  <dt>Modelo</dt>
                  <dd>{{$ups_bancob->bancob->modelo}}</dd>
                  <dt>Serial</dt>
                  <dd>{{$ups_bancob->bancob->serial}}</dd>
                  <dt>Inventario Cantv</dt>
                  <dd>{{$ups_bancob->bancob->inventario_cant}}</dd>
                  <dt>Capacidad kva</dt>
                  <dd>{{$ups_bancob->bancob->cap_kva}}</dd>
                  <dt>Carga Total</dt>
                  <dd>{{$ups_bancob->bancob->carga_total_bb}}</dd>
                  <dt>Elementos operativos</dt>
                  <dd>{{$ups_bancob->bancob->elem_oper_bb}}</dd>
                  <dt>Elementos inoperativos</dt>
                  <dd>{{$ups_bancob->bancob->elem_inoper_bb}}</dd>
                  <dt>Amperaje de elementos</dt>
                  <dd>{{$ups_bancob->bancob->amp_elem}}</dd>
                  <dt>Autonomía de Respaldo</dt>
                  <dd>{{$ups_bancob->bancob->autonomia_respaldo}}</dd>
                  <dt>Tablero</dt>
                  <dd>{{$ups_bancob->bancob->tablero}}</dd>
                </dl>
              </div>
              <!-- /.card-body -->
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="card card-primary">
                  <div class="card-header">
                    <h3 class="card-title">
                      <i class="fa fa-user"></i>
                          Responsable
                    </h3>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <dl>
                      <dt>Nombre y Apellido</dt>
                      <dd>{{$ups_bancob->ubicacion->responsable->nombre}} / 
                     {{$ups_bancob->ubicacion->responsable->apellido}}</dd>
                      <dt>Cédula</dt>
                      <dd>{{$ups_bancob->ubicacion->responsable->cedula}}</dd>
                      <dt>Cargo</dt>
                      <dd>{{$ups_bancob->ubicacion->responsable->cargo}}</dd>
                      <dt>Numero de Oficina</dt>
                      <dd>{{$ups_bancob->ubicacion->responsable->num_oficina}}</dd>
                      <dt>Numero de Personal</dt>
                      <dd>{{$ups_bancob->ubicacion->responsable->num_personal}}</dd>
                      <dt>Numero de Poo</dt>
                      <dd>{{$ups_bancob->ubicacion->responsable->cod_p00}}</dd>
                      <dt>Correo</dt>
                      <dd>{{$ups_bancob->ubicacion->responsable->correo}}</dd>
                    </dl>
                  </div>
                  <!-- /.card-body -->
                </div>
              </div>
              
            </div>
          </div>
          <!-- ./col -->
       
          </div>
        
      </div>
    </div>
  </section>
</div>
@endsection