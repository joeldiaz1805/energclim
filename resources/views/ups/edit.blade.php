@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              UPS
              <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','UPS','Sistemas de alimentación ininterrumpida, en inglés Uninterruptible Power Supply, es un dispositivo que gracias a sus baterías y otros elementos almacenadores de energía, durante un apagón eléctrico proporciona energía eléctrica por un tiempo limitado.', 75 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a> 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inventario</a></li>
              <li class="breadcrumb-item active">Ups</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
     
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
              
              <div class="card-body">
              	 <form method="POST" id="formulario_ups" name="formulario_ups"  action="/ups/{{$ups_bancob->id}}">
                        {{ csrf_field() }}

                        <div class="col-md-12">
			            <div class="card card-success">
			              <div class="card-header">
			                <h3 class="card-title">Ubicación y Responsable</h3>
			              </div>
			              <!-- /.card-header -->
			              <div class="card-body">
			                	<div class="card-header">
					                <h3 class="card-title">Ubicación</h3>
					              </div>
					              <br>

							<div class="row">
			                  	<div class="col-sm-4">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Región:</label>
			                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Región','Seleccione región donde está ubicado el UPS', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        	<select  name="central[region_id]" class="form-control">
                        						@foreach($regiones as $region)
                        							<option @if($region->id == $ups_bancob->ubicacion->central->region->id) selected @endif value="{{$region->id}}">{{$region->nombre}}</option>
                                    			@endforeach
                      						</select>
				                    </div>
			                    </div>
			                      <div class="col-sm-4">
			                      <!-- text input -->
			                      <div class="form-group">
			                        <label>Estado:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Estado','Seleccione estado donde está ubicado el UPS', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        	<select name="central[estado_id]" class="form-control">
                          					@foreach($estados as $estado)
                            					<option @if($estado->id == $ups_bancob->ubicacion->central->estados->id) selected @endif value="{{$estado->id}}">{{$estado->nombre}}</option>
                          					@endforeach
                        				</select>
			                      </div>
			                    </div>
			                   
			                    <div class="col-sm-4">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Central:</label>
			                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Central','Seleccione central donde está ubicado el UPS', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>	
				                        <select name="central[localidad_id]" class="form-control">
                            				@foreach($localidades as $central)
                              				<option @if($central->id == $ups_bancob->ubicacion->central->localidad->id) selected @endif value="{{$central->id}}">{{$central->nombre}}</option>
                            				@endforeach
                          				</select>
				                    </div>
			                      </div>
								
			                 </div>
			                 <div class="row">


				                    <div class="col-sm-4">
				                      <!-- text input -->
				                      	<div class="form-group">
				                        	<label>Piso:</label>
				                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Piso','Introduzca piso donde se encuentra el UPS. Solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
					                        	<select name="ubicacion[piso]" id="box_ModeloRectificador" class="form-control">
                                      				<option>Seleccione ubicación piso</option>
                                      				<option @if($ups_bancob->ubicacion->piso=="AZOTEA") selected @endif value="AZOTEA">AZOTEA</option>
                                      				<option @if($ups_bancob->ubicacion->piso=="PH") selected @endif value="PH">PH</option>
                                      				<option @if($ups_bancob->ubicacion->piso=="MZ") selected @endif value="MZ">MZ</option>
                                      				<option @if($ups_bancob->ubicacion->piso=="PB") selected @endif value="PB">PB</option>
                                      				<option @if($ups_bancob->ubicacion->piso=="ST") selected @endif value="ST">ST</option>
                                      				<option @if($ups_bancob->ubicacion->piso=="S1") selected @endif value="S1">S1</option>
                                      				<option @if($ups_bancob->ubicacion->piso=="S2") selected @endif value="S2">S2</option>
                                      				<option @if($ups_bancob->ubicacion->piso=="S3") selected @endif value="S3">S3</option>
                                      				<option @if($ups_bancob->ubicacion->piso=="Unico piso") selected @endif value="Unico piso">Unico piso</option>
                                      				<option @if($ups_bancob->ubicacion->piso=="P1") selected @endif value="P1">P1</option>
                                      				<option @if($ups_bancob->ubicacion->piso=="P2") selected @endif value="P2">P2</option>
                                      				<option @if($ups_bancob->ubicacion->piso=="P3") selected @endif value="P3">P3</option>
                                      				<option @if($ups_bancob->ubicacion->piso=="P4") selected @endif value="P4">P4</option>
                                      				<option @if($ups_bancob->ubicacion->piso=="P5") selected @endif value="P5">P5</option>
                                      				<option @if($ups_bancob->ubicacion->piso=="P6") selected @endif value="P6">P6</option>
                                      				<option @if($ups_bancob->ubicacion->piso=="P7") selected @endif value="P7">P7</option>
                                      				<option @if($ups_bancob->ubicacion->piso=="P8") selected @endif value="P8">P8</option>
                                      				<option @if($ups_bancob->ubicacion->piso=="P9") selected @endif value="P9">P9</option>
                                      				<option @if($ups_bancob->ubicacion->piso=="P10") selected @endif value="P10">P10</option>
                                      				<option @if($ups_bancob->ubicacion->piso=="P11") selected @endif value="P11">P11</option>
                                      				<option @if($ups_bancob->ubicacion->piso=="P12") selected @endif value="P12">P12</option>
                                      				<option @if($ups_bancob->ubicacion->piso=="P13") selected @endif value="P13">P13</option>
                                      				<option @if($ups_bancob->ubicacion->piso=="P14") selected @endif value="P14">P14</option>
                                      				<option @if($ups_bancob->ubicacion->piso=="P15") selected @endif value="P15">P15</option>
                                      				<option @if($ups_bancob->ubicacion->piso=="P16") selected @endif value="P16">P16</option>
                                      				<option @if($ups_bancob->ubicacion->piso=="P17") selected @endif value="P17">P17</option>
                                      				<option @if($ups_bancob->ubicacion->piso=="P18") selected @endif value="P18">P18</option>
                                      				<option @if($ups_bancob->ubicacion->piso=="P19") selected @endif value="P19">P19</option>
                                      				<option @if($ups_bancob->ubicacion->piso=="P20") selected @endif value="P20">P20</option>
                                      				<option @if($ups_bancob->ubicacion->piso=="P21") selected @endif 				value="P21">P21</option>
                                      				<option @if($ups_bancob->ubicacion->piso=="TERRAZA") selected @endif value="TERRAZA">TERRAZA</option>
                                    			</select>
					                    </div>
				                      </div>

				                      <div class="col-sm-4">
			                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Sala o espacio físico:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Sala','Seleccione sala donde está alojado el UPS', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        	<select name="ubicacion[sala]" id="box_ModeloRectificador" class="form-control">
                                      			<option>Seleccione una sala</option>
                                      			<option @if($ups_bancob->ubicacion->sala=="ABA") selected @endif value="ABA">ABA</option>
                                      			<option @if($ups_bancob->ubicacion->sala=="ADSL") selected @endif value="ADSL">ADSL</option>
                                      			<option @if($ups_bancob->ubicacion->sala=="BANCO DE BATERIA") selected @endif value="BANCO DE BATERIA">BANCO DE BATERIA</option>
                                      			<option @if($ups_bancob->ubicacion->sala=="CENTRO DE DISTRIBUCION") selected @endif value="CENTRO DE DISTRIBUCION">CENTRO DE DISTRIBUCION</option>
                                      			<option @if($ups_bancob->ubicacion->sala=="CX") selected @endif value="CX">CX</option>
                                      			<option @if($ups_bancob->ubicacion->sala=="DATA CENTER") selected @endif value="DATA CENTER">DATA CENTER</option>
                                      			<option @if($ups_bancob->ubicacion->sala=="DIGITAL") selected @endif value="DIGITAL">DIGITAL</option>
                                      			<option @if($ups_bancob->ubicacion->sala=="DP") selected @endif value="DP">DP</option>
                                      			<option @if($ups_bancob->ubicacion->sala=="DSLAM") selected @endif value="DSLAM">DSLAM</option>
                                      			<option @if($ups_bancob->ubicacion->sala=="DX") selected @endif value="DX">DX</option>
                                      			<option @if($ups_bancob->ubicacion->sala=="FURGON") selected @endif value="FURGON">FURGON</option>
                                      			<option @if($ups_bancob->ubicacion->sala=="LIBRE") selected @endif value="LIBRE">LIBRE</option>
                                      			<option @if($ups_bancob->ubicacion->sala=="MG") selected @endif value="MG">MG</option>
                                      			<option @if($ups_bancob->ubicacion->sala=="OAC") selected @endif value="OAC">OAC</option>
                                      			<option @if($ups_bancob->ubicacion->sala=="OFICINA ADMINISTRATIVA") selected @endif value="OFICINA ADMINISTRATIVA">OFICINA ADMINISTRATIVA</option>
                                      			<option @if($ups_bancob->ubicacion->sala=="OPSUT") selected @endif value="OPSUT">OPSUT</option>
                                      			<option @if($ups_bancob->ubicacion->sala=="OUTDOOR") selected @endif value="OUTDOOR">OUTDOOR</option>
                                      			<option @if($ups_bancob->ubicacion->sala=="PCM") selected @endif value="PCM">PCM</option>
                                      			<option @if($ups_bancob->ubicacion->sala=="PSTN") selected @endif value="PSTN">PSTN</option>
                                      			<option @if($ups_bancob->ubicacion->sala=="RECTIFICADORES") selected @endif value="RECTIFICADORES">RECTIFICADORES</option>
                                      			<option @if($ups_bancob->ubicacion->sala=="SSP") selected @endif value="SSP">SSP</option>
                                      			<option @if($ups_bancob->ubicacion->sala=="TX") selected @endif value="TX">TX</option>
                                      			<option @if($ups_bancob->ubicacion->sala=="TX INTERNACIONAL") selected @endif value="TX INTERNACIONAL">TX INTERNACIONAL</option>
                                      			<option @if($ups_bancob->ubicacion->sala=="UMA") selected @endif value="UMA">UMA</option>
                                      			<option @if($ups_bancob->ubicacion->sala=="UNICA") selected @endif value="UNICA">UNICA</option>
                                      			<option @if($ups_bancob->ubicacion->sala=="UPS") selected @endif value="UPS">UPS</option>
                                      			<option @if($ups_bancob->ubicacion->sala=="VARIAS SALAS") selected @endif value="VARIAS SALAS">VARIAS SALAS</option>
                                  			</select>
				                      </div>
			                    	</div>

			                  	<div class="col-sm-4">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Estructura:</label>
			                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Estructura','Donde está alojado el UPS', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        	<select name="ubicacion[estructura]" class="form-control">
                                  				<option>Seleccione tipo de estructura</option>
                                  				<option @if($ups_bancob->ubicacion->estructura=="CENTRAL CRITICA") selected @endif value="Fija">CENTRAL CRITICA</option>
                                  				<option @if($ups_bancob->ubicacion->estructura=="CENTRAL FIJA") selected @endif value="CENTRAL FIJA">CENTRAL FIJA</option>
                                  				<option @if($ups_bancob->ubicacion->estructura=="URL") selected @endif value="URL">URL</option>
                                  				<option @if($ups_bancob->ubicacion->estructura=="NODO INDOOR") selected @endif value="NODO INDOOR">NODO INDOOR</option>
                                  				<option @if($ups_bancob->ubicacion->estructura=="NODO OUTDOOR") selected @endif value="NODO OUTDOOR">NODO OUTDOOR</option>
                                  				<option @if($ups_bancob->ubicacion->estructura=="GTI") selected @endif value="GTI">GTI</option>
                                  				<option @if($ups_bancob->ubicacion->estructura=="OPSUT") selected @endif value="OPSUT">OPSUT</option>
                                  				<option @if($ups_bancob->ubicacion->estructura=="SUPER AULA") selected @endif value="SUPER AULA">SUPER AULA</option>
                                  				<option @if($ups_bancob->ubicacion->estructura=="OAC") selected @endif value="OAC">OAC</option>
                                  				<option @if($ups_bancob->ubicacion->estructura=="DLC") selected @endif value="DLC">DLC</option>
                                  				<option @if($ups_bancob->ubicacion->estructura=="NO CANTV") selected @endif value="NO CANTV">NO CANTV</option>
                              				</select>
				                    </div>
			                    </div>
			                 </div>

							<div class="row">
								<div class="col-sm-4">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Criticidad del Espacio:</label>
			                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Criticidad del Espacio','Introduzca Criticidad del Espacio', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        	<select name="ubicacion[criticidad_esp]" required class="form-control">
                                      			<option>Seleccione criticidad del espacio</option>
                                      			<option @if($ups_bancob->ubicacion->criticidad_esp=="Crítica") selected @endif value="Crítica">Crítica</option>
                                      			<option @if($ups_bancob->ubicacion->criticidad_esp=="Óptima") selected @endif value="Óptima">Óptima</option>
                                			</select>
				                    </div>
			                      </div>
							</div>


								<div class="card-header">
					               <h3 class="card-title">Responsable UPS y BB</h3>
					            </div>
					            <br>
				                <div class="row">
				                     <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">

				                        <label>Nombres:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Nombres','Nombre persona encargada y responsable del UPS', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" class="form-control" value="{{$ups_bancob->ubicacion->responsable->nombre}}" required name="responsable[nombre]" >

				                      </div>
				                    </div>
				                    <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Apellidos:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Apellidos','Apellido persona encargada y responsable del UPS', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" class="form-control" value="{{$ups_bancob->ubicacion->responsable->apellido}}" required name="responsable[apellido]" >
				                      </div>
				                    </div>
				                     <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">

				                        <label>Cédula:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cédula','Número de cédula', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" class="form-control" value="{{$ups_bancob->ubicacion->responsable->cedula}}" required name="responsable[cedula]" >

				                      </div>
				                    </div>
				                </div>
			                  
			                  	<div class="row">

			                  		<div class="col-sm-4">
			                      <!-- text input -->
				                      <div class="form-group">

				                        <label>P00:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','P00','Número interno que identifica al trabajador de la empresa. Ej. 112233', 75 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" class="form-control" value="{{$ups_bancob->ubicacion->responsable->cod_p00}}" required name="responsable[cod_p00]" >

				                      </div>
				                    </div>
			                  		<div class="col-sm-4">
			                      <!-- text input -->
				                      <div class="form-group">

				                        <label>Número de Oficina:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Número de Oficina','Número de Telf.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <div class="input-group">
						                    <div class="input-group-prepend">
						                      <span class="input-group-text"><i class="fas fa-phone"></i></span>
						                    </div>
						                    <input type="text" name="responsable[num_oficina]" value="{{$ups_bancob->ubicacion->responsable->num_oficina}}" required  class="form-control" data-inputmask='"mask": "99999999999"' data-mask>
						                  </div>

				                      </div>
				                    </div>

				                     <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">

				                        <label>Número Personal:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Número Personal','Número de Telf.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
					                      <div class="input-group">
						                    <div class="input-group-prepend">
						                      <span class="input-group-text"><i class="fas fa-phone"></i></span>
						                    </div>
						                    <input type="text" name="responsable[num_personal]" value="{{$ups_bancob->ubicacion->responsable->num_personal}}" required  class="form-control" data-inputmask='"mask": "99999999999"' data-mask>
						                  </div>
					                    </div>
				                      </div>
			                  		
			                  	</div>

				                  	<div class="row">
				                  		<div class="col-sm-4">
						                      <!-- text input -->
						                      <div class="form-group">

						                        <label>Cargo:</label>
						                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cargo','Denominación del cargo de la persona encargada del UPS.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
						                        <input type="text" class="form-control" required name="responsable[cargo]" value="{{$ups_bancob->ubicacion->responsable->cargo}}" placeholder="Introduzca cargo">

						                      </div>
						                    </div>
				                  		<div class="col-sm-8">
				                      <!-- text input -->
				                      		<div class="form-group">

				                        		<label>Correo:</label>
				                        		<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Correo','Dirección de correo electrónico. Ej. empleado@cantv.com.ve', 300 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        		<input type="email" name="responsable[correo]" value="{{$ups_bancob->ubicacion->responsable->correo}}" required class="form-control">
				                        		
				                      		</div>
				                    	</div>
				                  	</div>
				                  </div>
				              </div>
				          </div>
                
                     	<div class="col-md-12">
				            <!-- general form elements disabled -->
				            <div class="card card-success">
				              <div class="card-header">
				                <h3 class="card-title">Ups</h3>
				              </div>
				              <!-- /.card-header -->
				              <div class="card-body">
				                  <div class="row">
				                    <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Marca:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Marca','Seleccionar fabricante del UPS', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <select name="ups[marca]" class="form-control">
                            				@foreach($marcas as $mar)
                                        <option @if($mar->descripcion == $motogenerador->motor->marca) selected @endif value="{{$mar->descripcion}}">{{$mar->descripcion}}</option>
                                    @endforeach  
                          				</select>
				                      </div>
				                    </div>
				                    <div class="col-sm-4">
				                      <!-- text input -->
				                      	<div class="form-group">
				                        	<label>Modelo:</label>
				                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Modelo UPS','Caracteres y números que lo identifican tal cual aparece en el equipo. Ej. UPS202011112525', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
					                        	<input type="text" name="ups[modelo]"  value="{{$ups_bancob->ups->modelo}}" class="form-control" placeholder="Introduzca modelo">
					                    </div>
				                     </div>
				                   
									<div class="col-sm-4">
				                      <div class="form-group">
				                        	<label>Serial:</label>
				                        		<input type="checkbox" name="no_posee" id="no_posee_ups">No Posee
				                        		<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Serial','Secuencia de números y/o caracteres', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
					                        	<input type="text" name="ups[serial]"  value="{{$ups_bancob->ups->serial}}" class="form-control" placeholder="Introduzca serial">
					                    </div>
				                  </div>
				                 </div>

				                <div class="row">
				                     <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Inventario Cantv:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Inventario Cantv','Código interno de inventario en Cantv. Ej. 223344', 150 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" class="form-control" name="ups[inventario_cant]"  value="{{$ups_bancob->ups->inventario_cant}}"  placeholder="Introduzca Inventario Cantv">
				                      </div>
				                    </div>

				                    <div class="col-sm-4">
			                      <!-- text input -->
			                      <div class="form-group">
			                        <label>Capacidad KVA:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Capacidad KVA','kilovoltiamperio. Solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        <input type="text" name = "ups[cap_kva]" value="{{$ups_bancob->ups->cap_kva}}" class="form-control" placeholder="Introduzca Capacidad KVA">
			                      </div>
			                    </div>
								<div class="col-sm-4">
			                      <!-- text input -->
			                      <div class="form-group">
			                        <label>Carga Conectada:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Carga Conectada','???', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        <input type="text" name = "ups[carga_conectada]" value="{{$ups_bancob->ups->carga_conectada}}" class="form-control" placeholder="Introduzca Carga Conectada">
			                      </div>
			                    </div>
			                    </div>
 								<div class="row">
				                     <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Tensión Salida:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Tensión','Introduzca el voltaje del equipo. Solo números sin letras', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" class="form-control" name="ups[tension_salida]" value="{{$ups_bancob->ups->tension_salida}}" placeholder="Introduzca Tensión Salida">
				                      </div>
				                    </div>   
				           		</div>
				           		
				              </div>
				              <!-- /.card-body -->
				            </div>
				        </div>

                  	<div class="col-md-12">
			            <div class="card card-success">
			              <div class="card-header">
			                <h3 class="card-title">Banco de Baterías</h3>
			              </div>
			              <!-- /.card-header -->
			              <div class="card-body">
			                <form role="form">
			                  <div class="row">


    							<div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Marca:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Marca','Seleccionar fabricante de la batería', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <select name="bco_baterias[marca]" class="form-control">
                                    		<option @if($ups_bancob->bancob->marca=="SUNLIGHT") selected @endif value="SUNLIGHT">SUNLIGHT</option>
                                    		<option @if($ups_bancob->bancob->marca=="EVOLUTION") selected @endif value="EVOLUTION">EVOLUTION</option>
                                    		<option @if($ups_bancob->bancob->marca=="CHARLES") selected @endif value="CHARLES">CHARLES</option>
                                    		<option @if($ups_bancob->bancob->marca=="SERVELEC") selected @endif value="SERVELEC">SERVELEC</option>
                                    		<option @if($ups_bancob->bancob->marca=="PRO TECH 24201") selected @endif value="PRO TECH 24201">PRO TECH 24201</option>
                                    		<option @if($ups_bancob->bancob->marca=="SPTLINE") selected @endif value="SPTLINE">SPTLINE</option>
                                    		<option @if($ups_bancob->bancob->marca=="MEP") selected @endif value="MEP">MEP</option>
                                    		<option @if($ups_bancob->bancob->marca=="TITAN") selected @endif value="TITAN">TITAN</option>
                                    		<option @if($ups_bancob->bancob->marca=="ATLANTIC POWER") selected @endif value="ATLANTIC POWER">ATLANTIC POWER</option>
                                    		<option @if($ups_bancob->bancob->marca=="OTRO") selected @endif value="OTRO">OTRO</option>
                                  		</select>
				                      </div>
				                    </div>
				                    <div class="col-sm-4">
				                      <!-- text input -->
				                      	<div class="form-group">
				                        	<label>Modelo:</label>
				                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Modelo Batería','Caracteres y números que lo identifican Ej. BA202011112525', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
					                        <input type="text" name="bco_baterias[modelo]"  value="{{$ups_bancob->bancob->modelo}}" class="form-control" placeholder="Introduzca Modelo">
					                    </div>
				                      </div>
				                   
									<div class="col-sm-4">
				                      <div class="form-group">
				                        	<label>Serial:</label>
				                        	<input type="checkbox" name="no_posee" id="no_posee_bb">No Posee
				                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Serial','de Batería. Secuencia de números y/o caracteres', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
					                        <input type="text" name="bco_baterias[serial]"  value="{{$ups_bancob->bancob->serial}}" class="form-control" placeholder="Introduzca Serial">
					                    </div>
				                  </div>
				                 </div>

				                <div class="row">
				                    <div class="col-sm-4">
				                      <!-- text input -->
					                    <div class="form-group">
					                    	<label>Inventario Cantv:</label>
					                    	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Inventario Cantv','Código interno de inventario en Cantv. Ej. 223344', 150 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
					                        <input type="text" class="form-control" name="bco_baterias[inventario_cant]" value="{{$ups_bancob->bancob->inventario_cant}}" placeholder="Introduzca Inventario Cantv">
					                    </div>
				                    </div>

				                    <div class="col-sm-4">
			                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Capacidad KVA:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Capacidad KVA','kilovoltiamperio. Solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" name = "bco_baterias[cap_kva]" value="{{$ups_bancob->bancob->cap_kva}}" class="form-control" placeholder="Introduzca Capacidad KVA">
				                      </div>
				                    </div>
									<div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Carga total en BB:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Carga total en Banco de Batería','Suma de todas las cargas de las baterías. kilovoltiamperio. Solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" name = "bco_baterias[carga_total_bb]"  value="{{$ups_bancob->bancob->carga_total_bb}}" class="form-control" placeholder="Introduzca Carga Total en BB">
				                      </div>
				                    </div>
								</div>
								<div class="row">
				                    <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Elementos Operativos en BB:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cantidad de Elementos Operativos','Solo números sin letras. Ej. 1, 10, 20, etc.', 150 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" class="form-control" name="bco_baterias[elem_oper_bb]"  value="{{$ups_bancob->bancob->elem_oper_bb}}" placeholder="Introduzca Elementos Operativos en BB">
				                      </div>
				                    </div>

					                <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Elementos Inoperativos en BB:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cantidad de Elementos Inoperativos','Solo números sin letras. Ej. 1, 10, 20, etc.', 150 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" name = "bco_baterias[elem_inoper_bb]"  value="{{$ups_bancob->bancob->elem_inoper_bb}}" class="form-control" placeholder="Introduzca Elementos Inoperativos en BB">
				                      </div>
				                    </div>
									<div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Amperaje de Elementos:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Amperaje de Elementos','Introduzca un número. Ej. 20, 30,100, etc. , solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" name = "bco_baterias[amp_elem]"  value="{{$ups_bancob->bancob->amp_elem}}" class="form-control" placeholder="Introduzca Amperaje de Elementos">
				                      </div>
				                    </div>
								
								</div>
								<div class="row">
									<div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Autonomía de Respaldo:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Autonomía de Respaldo','Introduzca un número. Ej. 20, 30,100, etc. , solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" class="form-control" name="bco_baterias[autonomia_respaldo]" value="{{$ups_bancob->bancob->autonomia_respaldo}}"  placeholder="Introduzca Autonomía de Respaldo">
				                      </div>
				                    </div>

				                    <div class="col-sm-4">
						                <div class="form-group">
						                  	<label>¿Posee Tablero de Control?:</label>
						                  	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Tablero de Control','¿Banco Baterías tiene tablero de control?', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
						                        <div class="form-check">
						                          <input class="form-check-input" @if($ups_bancob->bancob->tablero=="Si") checked @endif type="radio" value="si" name="bco_baterias[tablero]">
                                					<label class="form-check-label">Si</label>
                              					</div>
                              			<div class="form-check">
                                			<input class="form-check-input" @if($ups_bancob->bancob->tablero=="No") checked @endif type="radio" value="no" name="bco_baterias[tablero]">
                                			<label class="form-check-label">No</label>
						               	</div>				                        
					                   	</div>
					                </div>
								</div>
								</div>
			              	</div>
			              <!-- /.card-body -->
			            </div>
                  	


						<div class="col-md-12">
							 <div class="card card-success">
			                      <div class="card-header">
			                        <h3 class="card-title">Instalación UPS</h3>
			                      </div>
			                      <!-- /.card-header -->
			                      <div class="card-body">
             				 	<div class="row">
								
									<div class="col-sm-4">
					                      <!-- text input -->
					                      <div class="form-group">
					                        <label>Operatividad:</label>
					                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Operatividad','Seleccione el estado en que se encuentra el UPS', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
					                        <select name="ups_bcob[operatividad]" class="form-control">
                                      			<option @if($ups_bancob->operatividad=="Operativo") checked @endif value="Operativo">Operativo</option>
                                      			<option @if($ups_bancob->operatividad=="Inoperativo") checked @endif value="Inoperativo">Inoperativo</option>
                                      			<option @if($ups_bancob->operatividad=="Vandalizado") checked @endif value="Vandalizado">Vandalizado</option>
                                    		</select>
					                      </div>
					                    </div>
					                    <div class="col-sm-4">
					                      <!-- text input -->
					                      	<div class="form-group">
					                        	<label>Fecha de Instalación:</label>
					                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Fecha de Instalación','del UPS', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
						                        	<input name="ups_bcob[fecha_instalacion]" id="box_ModeloRectificador" value="{{$ups_bancob->fecha_instalacion}}" class="form-control" type="date">
						                    </div>
					                      </div>
					                   
					                      <div class="col-sm-4">
					                      <!-- text input -->
					                      <div class="form-group">
					                        <label>Criticidad del Equipo:</label>
					                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Criticidad del Equipo','Selecione criticidad UPS. Por Ej. para su atención inmediata o posterior', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
					                        <select  name="ups_bcob[criticidad]"class="form-control">
                                      			<option @if($ups_bancob->criticidad=="Alta") checked @endif value="Alta">Alta</option>
                                      			<option @if($ups_bancob->criticidad=="Media") checked @endif value="Media">Media</option>
                                      			<option @if($ups_bancob->criticidad=="Baja") checked @endif value="Baja">Baja</option>
                                    		</select>
					                      </div>
					                    </div>
					                 </div>
					                 <div class="row">
					                  	<div class="col-sm-4">
					                      <!-- text input -->
					                      	<div class="form-group">
					                        	<label>¿Equipo en Garantía?</label>
					                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Garantía','¿Tiene o está vigente la garantía del UPS?', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
						                        	<div class="form-check">
								                          <input class="form-check-input" type="radio" @if($ups_bancob->garantia=="Si") checked @endif value="Si" name="ups_bcob[garantia]">
                                          <label class="form-check-label">Si</label>
                                        </div>
                                        <div class="form-check">
                                          <input class="form-check-input" type="radio"  @if($ups_bancob->garantia=="No") checked @endif value="No" name="ups_bcob[garantia]">
                                          <label class="form-check-label">No</label>
								                        </div>				                    
								                    </div>
					                    </div>
					                </div>
					                 <div class="row">
					                 	<div class="col-md-12">
											<div class="form-group">
												<label for="comment">Observaciones</label>
												<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Observaciones','Información complementaria sobre seguimientos e incidencias a destacar. Solo de la instalación del UPS', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
												<textarea class="form-control" rows="3" name="ups_bcob[observaciones]" id="comment">{{$ups_bancob->observaciones}}</textarea>			
											</div>
					                 	</div>
					                 </div>

								</div>
									 
							</div>
						</div>
				                 	 <div class="card-footer" align="right">
                      					<button type="submit" class="btn btn-success"><i class="fas fa-sync-alt nav-icon"></i> Actualizar</button>
                      				</div>
				            </form>
			              </div>
			            </div>
			    	</div>
              	</div>
          	</div>
        </div>
      </div>
    </section>
</div>

@endsection
  @section('vs')
    @if(isset($validator))
      {!! $validator->selector("#formulario_ups") !!}
    @endif

<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection
