@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              UPS
              <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','UPS','Sistemas de alimentación ininterrumpida, en inglés Uninterruptible Power Supply, es un dispositivo que gracias a sus baterías y otros elementos almacenadores de energía, durante un apagón eléctrico proporciona energía eléctrica por un tiempo limitado.', 75 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a> 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inventario</a></li>
              <li class="breadcrumb-item active">Ups</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
     
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
              
              <div class="card-body">
              	 <form method="POST" id="formulario_ups" name="formulario_ups"  action="{{ url('/ups')}}">
                        {{ csrf_field() }}

                        <div class="col-md-12">
			            <div class="card card-success">
			              <div class="card-header">
			                <h3 class="card-title">Ubicación y Responsable</h3>
			              </div>
			              <!-- /.card-header -->
			              <div class="card-body">
			                	<div class="card-header">
					                <h3 class="card-title">Ubicación</h3>
					              </div>
					              <br>

							<div class="row">
			                  	<div class="col-sm-4">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Región:</label>
			                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Región','Seleccione región donde está ubicado el UPS', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        	<select  name="central[region_id]" id="region" class="form-control">
				                        		<option>Seleccione una Región</option>
				                        		@foreach($regiones as $region)
					                        		<option value="{{$region->id}}">{{$region->nombre}}</option>
					                        	@endforeach
					                    	</select>
				                    </div>
			                    </div>
			                      <div class="col-sm-4">
			                      <!-- text input -->
			                      <div class="form-group">
			                        <label>Estado:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Estado','Seleccione estado donde está ubicado el UPS', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        <select name="central[estado_id]" id="estado" class="form-control">
			                        	<option value="">Seleccione un estado</option>
			                    	</select>
			                      </div>
			                    </div>
			                   
			                    <div class="col-sm-4">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Central:</label>
			                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Central','Seleccione central donde está ubicado el UPS', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>	
				                        <select name="central[localidad_id]" id="central" class="form-control">
				                        		<option value="">Seleccione una central</option>
					                    	</select>
				                    </div>
			                      </div>
								
			                 </div>
			                 <div class="row">


				                    <div class="col-sm-4">
				                      <!-- text input -->
				                      	<div class="form-group">
				                        	<label>Piso:</label>
				                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Piso','Introduzca piso donde se encuentra el UPS. Solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
					                        	<select name="ubicacion[piso]" id="box_ModeloRectificador" class="form-control">
					                        		<option>Seleccione ubicación piso</option>
						                        	<option value="AZOTEA">AZOTEA</option>
													<option value="PH">PH</option>
													<option value="MZ">MZ</option>
													<option value="PB">PB</option>
													<option value="ST">ST</option>
													<option value="S1">S1</option>
													<option value="S2">S2</option>
													<option value="S3">S3</option>
													<option value="Unico piso">Unico piso</option>
													<option value="P1">P1</option>
													<option value="P2">P2</option>
													<option value="P3">P3</option>
													<option value="P4">P4</option>
													<option value="P5">P5</option>
													<option value="P6">P6</option>
													<option value="P7">P7</option>
													<option value="P8">P8</option>
													<option value="P9">P9</option>
													<option value="P10">P10</option>
													<option value="P11">P11</option>
													<option value="P12">P12</option>
													<option value="P13">P13</option>
													<option value="P14">P14</option>
													<option value="P15">P15</option>
													<option value="P16">P16</option>
													<option value="P17">P17</option>
													<option value="P18">P18</option>
													<option value="P19">P19</option>
													<option value="P20">P20</option>
													<option value="P21">P21</option>
													<option value="TERRAZA">TERRAZA</option>
												</select>
					                    </div>
				                      </div>

				                      <div class="col-sm-4">
			                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Sala o espacio físico:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Sala','Seleccione sala donde está alojado el UPS', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        	<select name="ubicacion[sala]" id="box_ModeloRectificador" class="form-control">
					                        		<option>Seleccione una sala</option>
					                        		<option value="ABA">ABA</option>
													<option value="ADSL">ADSL</option>
													<option value="BANCO DE BATERIA">BANCO DE BATERIA</option>
													<option value="CENTRO DE DISTRIBUCION">CENTRO DE DISTRIBUCION</option>
													<option value="CX">CX</option>
													<option value="DATA CENTER">DATA CENTER</option>
													<option value="DIGITAL">DIGITAL</option>
													<option value="DP">DP</option>
													<option value="DSLAM">DSLAM</option>
													<option value="DX">DX</option>
													<option value="FURGON">FURGON</option>
													<option value="LIBRE">LIBRE</option>
													<option value="MG">MG</option>
													<option value="OAC">OAC</option>
													<option value="OFICINA ADMINISTRATIVA">OFICINA ADMINISTRATIVA</option>
													<option value="OPSUT">OPSUT</option>
													<option value="OUTDOOR">OUTDOOR</option>
													<option value="PCM">PCM</option>
													<option value="PSTN">PSTN</option>
													<option value="RECTIFICADORES">RECTIFICADORES</option>
													<option value="SSP">SSP</option>
													<option value="TX">TX</option>
													<option value="TX INTERNACIONAL">TX INTERNACIONAL</option>
													<option value="UMA">UMA</option>
													<option value="UNICA">UNICA</option>
													<option value="UPS">UPS</option>
													<option value="VARIAS SALAS">VARIAS SALAS</option>
												</select>
				                      </div>
			                    	</div>

			                  	<div class="col-sm-4">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Estructura:</label>
			                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Estructura','Donde está alojado el UPS', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        	<select name="ubicacion[estructura]" required class="form-control">
				                        		<option>Seleccione tipo de estructura</option>
				                            	<option value="Fija">CENTRAL CRITICA</option>
					                        	<option  value="CENTRAL FIJA">CENTRAL FIJA</option>
					                        	<option  value="URL">URL</option>
					                        	<option  value="NODO INDOOR">NODO INDOOR</option>
					                        	<option  value="NODO OUTDOOR">NODO OUTDOOR</option>
					                        	<option  value="GTI">GTI</option>
					                        	<option  value="OPSUT">OPSUT</option>
					                        	<option  value="SUPER AULA">SUPER AULA</option>
					                        	<option  value="OAC">OAC</option>
					                        	<option  value="DLC">DLC</option>
					                        	<option  value="NO CANTV">NO CANTV</option>
					                    	</select>
				                    </div>
			                    </div>
			                 </div>

							<div class="row">
								<div class="col-sm-4">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Criticidad del Espacio:</label>
			                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Criticidad del Espacio','Introduzca Criticidad del Espacio', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        	<select name="ubicacion[criticidad_esp]" required class="form-control">
				                            	<option>Seleccione criticidad del espacio</option>
												<option value="Crítica">Crítica</option>
												<option value="Óptima">Óptima</option>
					                    	</select>
				                    </div>
			                      </div>
							</div>


								<div class="card-header">
					               <h3 class="card-title">Responsable UPS y BB</h3>
					            </div>
					            <br>
				                <div class="row">
				                     <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">

				                        <label>Nombres:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Nombres','Nombre persona encargada y responsable del UPS', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" class="form-control" required name="responsable[nombre]" placeholder="Ej: Pedro">

				                      </div>
				                    </div>
				                    <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Apellidos:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Apellidos','Apellido persona encargada y responsable del UPS', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" class="form-control" required name="responsable[apellido]" placeholder="Tortoza">

				                      </div>
				                    </div>
				                     <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">

				                        <label>Cédula:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cédula','Número de cédula', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" class="form-control" required name="responsable[cedula]" placeholder="Introduzca número de cédula">

				                      </div>
				                    </div>
				                </div>
			                  
			                  	<div class="row">

			                  		<div class="col-sm-4">
			                      <!-- text input -->
				                      <div class="form-group">

				                        <label>P00:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','P00','Número interno que identifica al trabajador de la empresa. Ej. 112233', 75 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" class="form-control" required name="responsable[cod_p00]" placeholder="Introduzca P00">

				                      </div>
				                    </div>
			                  		<div class="col-sm-4">
			                      <!-- text input -->
				                      <div class="form-group">

				                        <label>Número de Oficina:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Número de Oficina','Número de Telf.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <div class="input-group">
						                    <div class="input-group-prepend">
						                      <span class="input-group-text"><i class="fas fa-phone"></i></span>
						                    </div>
						                    <input type="text" name="responsable[num_oficina]" required  class="form-control" data-inputmask='"mask": "99999999999"' data-mask>
						                  </div>

				                      </div>
				                    </div>

				                     <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">

				                        <label>Número Personal:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Número Personal','Número de Telf.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
					                      <div class="input-group">
						                    <div class="input-group-prepend">
						                      <span class="input-group-text"><i class="fas fa-phone"></i></span>
						                    </div>
						                    <input type="text" name="responsable[num_personal]" required  class="form-control" data-inputmask='"mask": "99999999999"' data-mask>
						                  </div>
					                    </div>
				                      </div>
			                  		
			                  	</div>

				                  	<div class="row">
				                  		<div class="col-sm-4">
						                      <!-- text input -->
						                      <div class="form-group">

						                        <label>Cargo:</label>
						                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cargo','Denominación del cargo de la persona encargada del UPS.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
						                        <input type="text" class="form-control" required name="responsable[cargo]" placeholder="Introduzca cargo">

						                      </div>
						                    </div>
				                  		<div class="col-sm-8">
				                      <!-- text input -->
				                      		<div class="form-group">

				                        		<label>Correo:</label>
				                        		<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Correo','Dirección de correo electrónico. Ej. empleado@cantv.com.ve', 300 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        		<input type="email" name="responsable[correo]" required class="form-control" placeholder="Introduzca correo corporativo/personal">
				                        		
				                      		</div>
				                    	</div>
				                  	</div>
				                  </div>
				              </div>
				          </div>
                
                     	<div class="col-md-12">
				            <!-- general form elements disabled -->
				            <div class="card card-success">
				              <div class="card-header">
				                <h3 class="card-title">Ups</h3>
				              </div>
				              <!-- /.card-header -->
				              <div class="card-body">
				                  <div class="row">
				                    <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Marca:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Marca','Seleccionar fabricante del UPS', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <select name="ups[marca]" class="form-control">
			                            <option value="">Seleccione marca</option>
											@foreach($marcas as $mar)
												<option value="{{$mar->descripcion}}">{{$mar->descripcion}}</option>
											@endforeach							
						            	</select>
				                      </div>
				                    </div>
				                    <div class="col-sm-4">
				                      <!-- text input -->
				                      	<div class="form-group">
				                        	<label>Modelo:</label>
				                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Modelo UPS','Caracteres y números que lo identifican tal cual aparece en el equipo. Ej. UPS202011112525', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
					                        	<input type="text" name="ups[modelo]"  class="form-control" placeholder="Introduzca modelo">
					                    </div>
				                     </div>
				                   
									<div class="col-sm-4">
				                      <div class="form-group">
				                        	<label>Serial:</label>
				                        		<input type="checkbox" name="no_posee" id="no_posee_ups">No Posee
				                        		<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Serial','Secuencia de números y/o caracteres', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
					                        	<input type="text" name="ups[serial]" id="serial_ups" class="form-control" placeholder="Introduzca serial">
					                    </div>
				                  </div>
				                 </div>

				                <div class="row">
				                     <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Inventario Cantv:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Inventario Cantv','Código interno de inventario en Cantv. Ej. 223344', 150 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" class="form-control" name="ups[inventario_cant]"  placeholder="Introduzca Inventario Cantv">
				                      </div>
				                    </div>

				                    <div class="col-sm-4">
			                      <!-- text input -->
			                      <div class="form-group">
			                        <label>Capacidad KVA:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Capacidad KVA','kilovoltiamperio. Solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        <input type="text" name = "ups[cap_kva]" class="form-control" placeholder="Introduzca Capacidad KVA">
			                      </div>
			                    </div>
								<div class="col-sm-4">
			                      <!-- text input -->
			                      <div class="form-group">
			                        <label>Carga Conectada:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Carga Conectada','???', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        <input type="text" name = "ups[carga_conectada]" class="form-control" placeholder="Introduzca Carga Conectada">
			                      </div>
			                    </div>
			                    </div>
 								<div class="row">
				                     <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Tensión Salida:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Tensión','Introduzca el voltaje del equipo. Solo números sin letras', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" class="form-control" name="ups[tension_salida]"  placeholder="Introduzca Tensión Salida">
				                      </div>
				                    </div>   
				           		</div>
				           		
				              </div>
				              <!-- /.card-body -->
				            </div>
				        </div>

                  	<div class="col-md-12">
			            <div class="card card-success">
			              <div class="card-header">
			                <h3 class="card-title">Banco de Baterías</h3>
			              </div>
			              <!-- /.card-header -->
			              <div class="card-body">
			                <form role="form">
			                  <div class="row">


    							<div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Marca:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Marca','Seleccionar fabricante de la batería', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <select name="bco_baterias[marca]" class="form-control">
			                            <option value="">Seleccione marca</option>
										<option value="SUNLIGHT">SUNLIGHT</option>
										<option value="EVOLUTION">EVOLUTION</option>
										<option value="CHARLES">CHARLES</option>
										<option value="SERVELEC">SERVELEC</option>
										<option value="PRO TECH 24201">PRO TECH 24201</option>
										<option value="SPTLINE">SPTLINE</option>
										<option value="MEP">MEP</option>
										<option value="TITAN">TITAN</option>
										<option value="ATLANTIC POWER">ATLANTIC POWER</option>
										<option value="OTRO">OTRO</option>										
						            	</select>
				                      </div>
				                    </div>
				                    <div class="col-sm-4">
				                      <!-- text input -->
				                      	<div class="form-group">
				                        	<label>Modelo:</label>
				                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Modelo Batería','Caracteres y números que lo identifican Ej. BA202011112525', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
					                        <input type="text" name="bco_baterias[modelo]"  class="form-control" placeholder="Introduzca Modelo">
					                    </div>
				                      </div>
				                   
									<div class="col-sm-4">
				                      <div class="form-group">
				                        	<label>Serial:</label>
				                        	<input type="checkbox" name="no_posee" id="no_posee_bb">No Posee
				                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Serial','de Batería. Secuencia de números y/o caracteres', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
					                        <input type="text" name="bco_baterias[serial]" id="serial_bb" class="form-control" placeholder="Introduzca Serial">
					                    </div>
				                  </div>
				                 </div>

				                <div class="row">
				                    <div class="col-sm-4">
				                      <!-- text input -->
					                    <div class="form-group">
					                    	<label>Inventario Cantv:</label>
					                    	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Inventario Cantv','Código interno de inventario en Cantv. Ej. 223344', 150 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
					                        <input type="text" class="form-control" name="bco_baterias[inventario_cant]"  placeholder="Introduzca Inventario Cantv">
					                    </div>
				                    </div>

				                    <div class="col-sm-4">
			                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Capacidad KVA:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Capacidad KVA','kilovoltiamperio. Solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" name = "bco_baterias[cap_kva]" class="form-control" placeholder="Introduzca Capacidad KVA">
				                      </div>
				                    </div>
									<div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Carga total en BB:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Carga total en Banco de Batería','Suma de todas las cargas de las baterías. kilovoltiamperio. Solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" name = "bco_baterias[carga_total_bb]" class="form-control" placeholder="Introduzca Carga Total en BB">
				                      </div>
				                    </div>
								</div>
								<div class="row">
				                    <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Elementos Operativos en BB:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cantidad de Elementos Operativos','Solo números sin letras. Ej. 1, 10, 20, etc.', 150 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" class="form-control" name="bco_baterias[elem_oper_bb]"  placeholder="Introduzca Elementos Operativos en BB">
				                      </div>
				                    </div>

					                <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Elementos Inoperativos en BB:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cantidad de Elementos Inoperativos','Solo números sin letras. Ej. 1, 10, 20, etc.', 150 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" name = "bco_baterias[elem_inoper_bb]" class="form-control" placeholder="Introduzca Elementos Inoperativos en BB">
				                      </div>
				                    </div>
									<div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Amperaje de Elementos:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Amperaje de Elementos','Introduzca un número. Ej. 20, 30,100, etc. , solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" name = "bco_baterias[amp_elem]" class="form-control" placeholder="Introduzca Amperaje de Elementos">
				                      </div>
				                    </div>
								
								</div>
								<div class="row">
									<div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Autonomía de Respaldo:</label>
				                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Autonomía de Respaldo','Introduzca un número. Ej. 20, 30,100, etc. , solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
				                        <input type="text" class="form-control" name="bco_baterias[autonomia_respaldo]"  placeholder="Introduzca Autonomía de Respaldo">
				                      </div>
				                    </div>

				                    <div class="col-sm-4">
						                <div class="form-group">
						                  	<label>¿Posee Tablero de Control?:</label>
						                  	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Tablero de Control','¿Banco Baterías tiene tablero de control?', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
						                        <div class="form-check">
						                          <input class="form-check-input" type="radio" value="si" name="bco_baterias[tablero]">
						                          <label class="form-check-label">Si</label>
						                        </div>
						                        <div class="form-check">
						                          <input class="form-check-input" type="radio" value="no" name="bco_baterias[tablero]">
						                          <label class="form-check-label">No</label>
						                    	</div>				                        
					                   	</div>
					                </div>
								</div>
								</div>
			              	</div>
			              <!-- /.card-body -->
			            </div>
                  	


						<div class="col-md-12">
							 <div class="card card-success">
			                      <div class="card-header">
			                        <h3 class="card-title">Instalación UPS</h3>
			                      </div>
			                      <!-- /.card-header -->
			                      <div class="card-body">
             				 	<div class="row">
								
									<div class="col-sm-4">
					                      <!-- text input -->
					                      <div class="form-group">
					                        <label>Operatividad:</label>
					                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Operatividad','Seleccione el estado en que se encuentra el UPS', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
					                        <select name="ups_bcob[operatividad]" class="form-control">
				                            	<option value="">Indique operatividad del equipo</option>
												<option value="Operativo">Operativo</option>
												<option value="Inoperativo">Inoperativo</option>
												<option value="Vandalizado">Vandalizado</option>
											</select>
					                      </div>
					                    </div>
					                    <div class="col-sm-4">
					                      <!-- text input -->
					                      	<div class="form-group">
					                        	<label>Fecha de Instalación:</label>
					                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Fecha de Instalación','del UPS', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
						                        	<input name="ups_bcob[fecha_instalacion]" id="box_ModeloRectificador" class="form-control" type="date">
						                    </div>
					                      </div>
					                   
					                      <div class="col-sm-4">
					                      <!-- text input -->
					                      <div class="form-group">
					                        <label>Criticidad del Equipo:</label>
					                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Criticidad del Equipo','Selecione criticidad UPS. Por Ej. para su atención inmediata o posterior', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
					                        <select  name="ups_bcob[criticidad]"class="form-control">
				                            	<option value="">Indique criticidad del equipo</option>
												<option value="Alta">Alta</option>
												<option value="Media">Media</option>
												<option value="Baja">Baja</option>
											</select>
					                      </div>
					                    </div>
					                 </div>
					                 <div class="row">
					                  	<div class="col-sm-4">
					                      <!-- text input -->
					                      	<div class="form-group">
					                        	<label>¿Equipo en Garantía?</label>
					                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Garantía','¿Tiene o está vigente la garantía del UPS?', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
						                        	<div class="form-check">
								                          <input class="form-check-input" type="radio" value="si" name="ups_bcob[garantia]">
								                          <label class="form-check-label">Si</label>
								                        </div>
								                        <div class="form-check">
								                          <input class="form-check-input" type="radio" value="no" name="ups_bcob[garantia]">
								                          <label class="form-check-label">No</label>
								                        </div>				                    
								                    </div>
					                    </div>
					                </div>
					                 <div class="row">
					                 	<div class="col-md-12">
											<div class="form-group">
												<label for="comment">Observaciones</label>
												<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Observaciones','Información complementaria sobre seguimientos e incidencias a destacar. Solo de la instalación del UPS', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
												<textarea class="form-control" rows="3" name="ups_bcob[observaciones]" id="comment"></textarea>			
											</div>
					                 	</div>
					                 </div>

								</div>
									 
							</div>
						</div>
				                 	 <div class="card-footer" align="right">
					                  <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
					                </div>
				                </form>
			              </div>
			              <!-- /.card-body -->
			            </div>
			        </div>
              </div>
              <!-- /.card -->
          </div>
          
        </div>

      
      
       
        <!-- /.card -->
       
        <!-- /.card -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

@endsection
@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_ups") !!}
@endif



<script type="text/javascript">
 $("#no_posee_ups").click(function(){
	var no_posee_ups = document.getElementById('no_posee_ups');
	var serial_ups = document.getElementById('serial_ups');
	if(no_posee_ups.checked){
        	//$('#serial_gen').hide();
        	serial_ups.value="no posee"
        	$('#serial_ups').prop('readonly', true);
 		}
 	if(!no_posee_ups.checked){
        	//$('#serial_gen').hide();
        	serial_ups.value=""
        	$('#serial_ups').prop('readonly', false);
 		}

 	});



 	 $("#no_posee_bb").click(function(){
	var no_posee_bb = document.getElementById('no_posee_bb');
	var serial_bb = document.getElementById('serial_bb');
	if(no_posee_bb.checked){
        	//$('#serial_gen').hide();
        	serial_bb.value="no posee"
        	$('#serial_bb').prop('readonly', true);
 		}
 	if(!no_posee_bb.checked){
        	//$('#serial_gen').hide();
        	serial_bb.value=""
        	$('#serial_bb').prop('readonly', false);
 		}

 	});





$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
  $(document).ready(function(){



  	$('select[id=region]').change(function () {
    	console.log('hola')
    var region_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'estado/' + region_id, function (cidades) {
        $('select[id=estado]').empty();

        if(cidades.length != 0){
            $('select[id=estado]').append('<option value=""> Selecciona un estado</option>');
            $.each(cidades, function (key, value) {
                $('select[id=estado]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });

    });


	$('select[id=estado]').change(function () {
    	//console.log('hola')
    var estado_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'central/' + estado_id, function (cidades) {
        $('select[id=central]').empty();

        if(cidades.length != 0){
            $('select[id=central]').append('<option value=""> Selecciona un central</option>');
            $.each(cidades, function (key, value) {
                $('select[id=central]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });

    });


	});
});
</script>
@endsection

