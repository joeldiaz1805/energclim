@extends ('/layouts/index')

@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              UPS 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">UPS</a></li>
              <li class="breadcrumb-item active">UPS</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Vista Preliminar de Registros</h3>
              <div align="right">
                  <a href="/ups/create" class="btn btn-success">Nuevo Registro</a>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class=" display nowrap  table table-bordered table-striped" style="width:100%">
                <thead>
                  <tr align="center">
                    <th>Acciones</th>
                    <th>Región</th>
                    <th>Estado</th>
                    <th>Central</th>
                    <th>Estructura</th>
                    <th>Sala</th>
                    <th>Piso</th>
                    <th>Responsable</th>
                    <th>Marca UPS</th>
                    <th>Modelo UPS</th>
                    <th>Serial UPS</th>
                    <th>Inventario CANTV UPS</th>
                    <th>Capacidad KVA</th>
                    <th>Carga Conectada</th>
                    <th>Tensión de Salida</th>
                    <th>Marca Banco de Baterías</th>
                    <th>Modelo Banco de Baterías</th>
                    <th>Serial Banco de Baterías</th>
                    <th>Inventario CANTV BB</th>
                    <th>Capacidad KVA</th>
                    <th>Carga Total en BB</th>
                    <th>Elementos Operativos</th>
                    <th>Elementos Inoperativos</th>
                    <th>Amperaje de Elementos</th>
                    <th>Autonomía de respaldo (hrs)</th>
                    <th>Tablero de Control</th>
                    <th>Operatividad</th>
                    <th>Fecha de Instalación</th>
                    <th>Criticidad del Equipo</th>
                    <th>Garantía</th>
                    <th>Observaciones</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($ups as $equipo)
                  
                  <tr align="center" id="tdId_{{$equipo->id}}">
                    <td>
                      <a href="/ups/{{$equipo->id}}/edit" class="btn btn-warning"><i class="far fa-edit"></i></a>
                      <a href="/ups/{{$equipo->id}}" class="btn btn-success"><i class="far fa-eye"></i></a>
                      <a onclick="deleteItem('{{$equipo->id}}')" class="btn btn-danger"><i class="fa fa-times"></i></a>
                    </td>
                    <td>{{$equipo->ubicacion->central->region->nombre}}</td>
                    <td>{{$equipo->ubicacion->central->estados->nombre}}</td>
                    <td>{{$equipo->ubicacion->central->localidad->nombre}}</td>
                    <td>{{$equipo->ubicacion->estructura}}</td>
                    <td>{{$equipo->ubicacion->sala}}</td>
                    <td>{{$equipo->ubicacion->piso}}</td>
                    <td>
                      {{$equipo->ubicacion->responsable->nombre}}/
                      {{$equipo->ubicacion->responsable->apellido}}/
                      {{$equipo->ubicacion->responsable->cod_p00}}/
                      {{$equipo->ubicacion->responsable->num_personal}}/
                      {{$equipo->ubicacion->responsable->correo}}
                    </td>  
                    <td>{{$equipo->ups->marca}}</td>
                    <td>{{$equipo->ups->modelo}}</td>
                    <td>{{$equipo->ups->serial}}</td>
                    <td>{{$equipo->ups->inventario_cant}}</td>
                    <td>{{$equipo->ups->cap_kva}}</td>
                    <td>{{$equipo->ups->carga_conectada}}</td>
                    <td>{{$equipo->ups->tension_salida}}</td>
                    <td>{{$equipo->bancob->marca ?? 'sin marca'}}</td>
                    <td>{{$equipo->bancob->modelo ?? 'sin modelo'}}</td>
                    <td>{{$equipo->bancob->serial ?? 'sin serial'}}</td>
                    <td>{{$equipo->bancob->inventario_cant ?? 'sin inv'}}</td>
                    <td>{{$equipo->bancob->cap_kva ?? ' sin valores'}}</td>
                    <td>{{$equipo->bancob->carga_total_bb  ?? ' sin valores'}}</td>
                    <td>{{$equipo->bancob->elem_oper_bb  ?? ' sin valores'}}</td>
                    <td>{{$equipo->bancob->elem_inoper_bb  ?? ' sin valores'}}</td>
                    <td>{{$equipo->bancob->amp_elem  ?? ' sin valores'}}</td>
                    <td>{{$equipo->bancob->autonomia_respaldo  ?? ' sin valores'}}</td>
                    <td>{{$equipo->bancob->tablero  ?? ' sin valores'}}</td>
                    <td>{{$equipo->operatividad}}</td>
                    <td>{{$equipo->fecha_instalacion}}</td>
                    <td>{{$equipo->criticidad}}</td>
                    <td>{{$equipo->garantia}}</td>
                    <td>{{$equipo->observaciones}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
           </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('scriptsalerts')


<script>
   @if(Session::has('exito'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Ups Guardado con exito'
      })
      })
    });
  @endif
   @if(Session::has('actualizado'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Datos actualizados con exito'
      })
      })
    });
  @endif
  
</script>

  <script>
    function deleteItem(item){
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Desea eliminar este ups del intentario??',
      text: "Estas por quitar este equipo del Inventario!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, quitarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {


      if (result.value) {



            $.ajax({
                url: "/ups/"+item,
                type:"DELETE",
                   headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    

                    success:function(result, status, xhr){                    
                        
                        if (status == 'success') {

                          swalWithBootstrapButtons.fire(
                            'Quitado con Exito!',
                            'Se eliminó con exito',
                            'success'
                          )

                          $("#tdId_"+item).hide();
                         // txt.innerHTML = result ;



                        }
                    },
                    error:function(xhr, status, error){
                      

                      Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se eliminó el producto, vuelva a intentarlo mas tarde '+error,
                        footer: '<a href>Seguramente hubo un problema de conexión</a>'
                      })
                      

                    }
                });
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No se ha quitado el producto.',
          'error'
        )
      }
    })
  }
</script>


@endsection
