@extends ('/layouts/index')

@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Fallas 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Monitoreo</a></li>
              <li class="breadcrumb-item active">Fallas</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Registro de Fallas</h3>
              <div align="right">
                  <a href="/fallas_modulo/create" class="btn btn-success">Agregar Fallas</a>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class=" display table table-bordered table-striped nowrap" style="width:100%">
                <thead>
                  <tr align="center">
                    <th class="not-export-col">Acciòn</th>
                    <th>Fallas</th>
                    <th>Equipo asociado</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($fallas as $falla)
                  <tr align="center" id="tdId_{{$falla->id}}" >
                    <td>
                     <a href="/fallas_modulo/{{encrypt($falla->id)}}/edit" class="btn btn-warning"><i class="far fa-edit"></i></a>
                     <a onclick="deleteItem('{{$falla->id}}')" class="btn btn-danger"><i id="ele"  class="fa fa-user-times"></i></a>
                      </td>
                    
                    <td>{{$falla->descripcion}} </td>
                    <td>{{$falla->tipo == 1 ? 'Ex' : ($falla->tipo == 2 ? 'A/A' : $falla->tipo) }}</td>
                    
                  </tr>
                  @endforeach
                </tbody>
              </table>
           </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('scriptsalerts')


<script>
   @if(Session::has('exito'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Estado Guardado con exito'
      })
      })
    });
  @endif
   @if(Session::has('actualizado'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Datos actualizados con exito'
      })
      })
    });
  @endif
  
</script>

  <script>
    function deleteItem(item){
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Desea eliminar este equipo ?',
      text: "Estas por quitar este equio!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, quitarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {


      if (result.value) {



            $.ajax({
                url: "/Fallas/"+item,
                type:"DELETE",
                   headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    

                    success:function(result, status, xhr){                    
                        
                        if (status == 'success') {

                          swalWithBootstrapButtons.fire(
                            'Quitado con Exito!',
                            'Se eliminó con exito',
                            'success'
                          )

                          $("#tdId_"+item).hide();
                         // txt.innerHTML = result ;



                        }
                    },
                    error:function(xhr, status, error){
                      

                      Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se eliminó el equipo, vuelva a intentarlo mas tarde '+error,
                        footer: '<a href>Seguramente hubo un problema de conexión</a>'
                      })
                      

                    }
                });
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No se ha quitado el Estado.',
          'error'
        )
      }
    })
  }
</script>


@endsection