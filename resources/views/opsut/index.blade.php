@extends ('/layouts/index')
@section ('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"> Octavo Proyecto de Servicio Universal de Telecomnicaciones - O.P.S.U.T.</h1>
            
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Principal</a></li>
              <li class="breadcrumb-item active">O.P.S.U.T.</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          
          <section class="col-lg-12 connectedSortable">

            <div class="card ">
              <div class="card-header border-0 bg-gradient-primary">
                <h3 class="card-title ">
                  <i class="fas fa-map-marker-alt mr-1"></i>
                  Mapa de Venezuela / Nodos O.P.S.U.T.
                </h3>
               
              </div>
              <div class="card-body">
                <div id="mapid" style="width: 100%; height: 600px;"></div>
              </div>
                         
            </div>
       
          </section>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  </div>
  @endsection

@section('scriptsalerts')

<script>
  var mymap = L.map('mapid').setView([10.102150, -68,2238], 6);
  L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    minZoom: 0,
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
  }).addTo(mymap);
  L.marker([10.102150, -68,2238]).addTo(mymap).bindPopup("<b>Hello world!</b><br />I am a popup.");
  L.marker([10.102150, -64,421763]).addTo(mymap).bindPopup("<b>Las Lajitas</b><br />Nodo Opsut");
</script>
@endsection

