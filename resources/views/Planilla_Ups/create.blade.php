@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Planilla Ups
              <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Rectificador','Un Rectificador es un dispositivo electrónico que permite convertir la corriente alterna en corriente continua.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a> 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inventario</a></li>
              <li class="breadcrumb-item active">Planilla Ups</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <div class="row ">
	    <div class="col s12 m12 l12 ">
	      @if ($errors->any())
	      <div class="alerta-errores">
	        <ul>
	          @foreach ($errors->all() as $error)
	          <li>{{ $error }}</li>
	      	  @endforeach
	        </ul>
	      </div>
	      @endif
	    </div>
	  </div>

    <!-- Main content -->
    <section class="content">
     	<div class="container-fluid">
	      <div class="row">
       		<div class="col-12 col-sm-12">
         		<div class="card card-primary card-tabs">
         			<div class="card-body">
	         			<form method="POST" id="formulario_rect" name="formulario_rect"  action="{{url('/tablero_actividad')}}">
	              	{{ csrf_field() }}
	              	<input type="hidden" name="usuario_id" value="{{Auth::user()->id}}">
	                <div class="col-md-12" >
						        <div class="card card-success">
			              	<div class="card-header">
			                	<h3 class="card-title">Especifique la actividad</h3>
			              	</div>
							              <!-- /.card-header -->
			              	<div class="card-body">
			              		<div class="row" >
							           	<div class="col-sm-4">
						               	<div class="form-group">
						                 	<label>Región:</label>
						                 	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Región','Seleccione región donde está ubicado el AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
						                 	<select id="region" class="form-control">
						                  	<option>Seleccione una Región</option>
							                  @foreach($regiones as $region)
								                <option value="{{$region->id}}">{{$region->nombre}}</option>
								                @endforeach
	 							              </select>
								            </div>
							            </div>

		                      <div class="col-sm-4">
			                      <div class="form-group">
			                        <label>Estado:</label>
			                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Estado','Seleccione estado donde está ubicado el AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
			                        <select  id="estado" class="form-control">
			                       		<option value="">Seleccione un estado</option>
 				                    	</select>
			                      </div>
			                    </div>

			                    <div class="col-sm-4">
						                <div class="form-group">
						                 	<label>Central:</label>
						                 	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Central','Seleccione central donde está ubicado el AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							               	<select name="localidad_id" id="central" class="form-control select2">
								             		<option value="">Seleccione una central</option>
									           	</select>
								            </div>
						              </div>
							          </div>
		
						                 		<div class="row">
						                 			<div class="col-sm-4">
							                 			<div class="form-group">
							                        <label>Nº Ticket:</label>
						                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Serial','de Batería. Secuencia de números y/o caracteres', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        <input type="text" id="ticket" name="ticket_num"  class="form-control" placeholder="Introduzca Nùmero de ticket Sismyc">
										                </div>
						                 			</div>
								               		<div class="col-sm-4">
							                     	<div class="form-group">
							                       	<label>Realizado por:</label>
							                       	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Estructura','Tipo de infraestrcutura tecnológica donde está alojado el rectificador', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                       	<select name="responsable_id" required class="form-control select2">
					                        			@foreach($personal as $super)
					                        				<option value="{{$super->id}}">{{$super->nombre}}</option>
					                        			@endforeach
						                    			</select>
									                  </div>
								                	</div>
							                    <div class="col-sm-4">
							                      	<div class="form-group">
							                        	<label>Supervision:</label>
							                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Estructura','Tipo de infraestrcutura tecnológica donde está alojado el rectificador', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                        	<select name="supervisor_id" required class="form-control select2">
							                        		@foreach($personal as $super)
					                        					<option value="{{$super->id}}">{{$super->nombre}}</option>
					                        				@endforeach
						                    			</select>
								                    </div>
								                </div>
					                 		</div>

					                 		<div class="row">
					                 			<div class="col-sm-4">
							                      	<div class="form-group">
							                        	<label>Fecha Emision:</label>
							                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Sala','Seleccione sala donde está alojado el rectificador', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        <input class="form-control" type="date" name="fecha_emision">	
							                      	</div>
						                    	</div>
						                    	<div class="col-sm-4">
							                      	<div class="form-group">
							                        	<label>Fecha Ejecucion:</label>
							                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Sala','Seleccione sala donde está alojado el rectificador', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        <input class="form-control" type="date" name="fecha_ejecucion">	
							                        	
							                      	</div>
						                    	</div>
						                    	<div style="display: none;" class="col-sm-4">
						                 				<div class="form-group">
								                        <label>Instalaciòn:</label>
							                        	<input type="checkbox" name="instalacion">
							                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Serial','de Batería. Secuencia de números y/o caracteres', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        <input type="text" id="instalacion" name="instalacion"  class="form-control" placeholder="Introduzca Nùmero de ticket Sismyc">
									                	</div>
						                    	</div>
						                 			
						                 			<div class="col-sm-4">
								                      	<div class="form-group">
								                        	<label>Sala o espacio físico:</label>
								                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Sala','Seleccione sala donde está alojado el rectificador', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
									                        	<select name="sala" id="box_ModeloRectificador" class="form-control">
									                        		<option value="">Seleccione una Sala</option>
																								@foreach($salas as $sala)
									                            		<option value="{{$sala->nombre}}">{{$sala->nombre}}</option>
									                            	@endforeach
																						</select>
								                      	</div>
							                    	</div>
								            </div>
													</div>
												</div>
											</div>
				             	
						        <div class="col-md-12">
				            	<div class="card card-success">
						            <div class="card-header">
					                <h3 class="card-title">Planilla Ups</h3>
						            </div>
						            <div class="card-body">
			                  	<div class="row">
														<div class="col-sm-4">
				                      <div class="form-group">
				                        <label>Marca:</label>
					                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Marca','Seleccionar fabricante de la batería', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
						                        <select name="bco_baterias[marca]" class="form-control">
				                            	<option value=""></option>
																			@foreach($marcas as $mar)
																				<option value="{{$mar->descripcion}}">{{$mar->descripcion}}</option>
																			@endforeach				
											            	</select>
						                      </div>
						                    </div>
						                    <div class="col-sm-4">
					                      	<div class="form-group">
					                        	<label>Modelo:</label>
						                       	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Modelo Batería','Caracteres y números que lo identifican Ej. BA202011112525', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
						                        <input type="text" name="bco_baterias[modelo]"  class="form-control" placeholder="Introduzca Modelo">
							                    </div>
						                    </div>
															<div class="col-sm-4">
								               	<div class="form-group">
								                 	<label>S/N Ups:</label><br>
								                 	<input type="checkbox" name="bco_baterias[tipo]" id="tipo" value="1">Abierto
								                        	<input type="checkbox" name="bco_baterias[tipo]" id="tipo" value="2">Sellado
									                    </div>
								                  	</div>
								                 </div>

								                <div class="row">

								                    <div class="col-sm-4">
							                      <!-- text input -->
								                      	<div class="form-group">
								                        	<label>Voltaje AC Entrada Ups:</label>
								                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Capacidad KVA','kilovoltiamperio. Solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        	<input type="text" name = "bco_baterias[cap_ah]" class="form-control" placeholder="Introduzca Capacidad Amp/AmpH">
								                      	</div>
								                    </div>
													<div class="col-sm-4">
								                      <!-- text input -->
								                      	<div class="form-group">
								                        	<label>Voltaje AC Salida Ups:</label>
								                       	 	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Carga total en Banco de Batería','Suma de todas las cargas de las baterías. kilovoltiamperio. Solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        	<input type="text" name = "bco_baterias[v_total]" class="form-control" placeholder="Introduzca Carga de la Localidad">
								                      	</div>
								                    </div>
								                    <div class="col-sm-4">
								                      <!-- text input -->
								                      	<div class="form-group">
								                        	<label>% Carga Ups:</label>
								                       	 	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Carga total en Banco de Batería','Suma de todas las cargas de las baterías. kilovoltiamperio. Solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        	<input type="text" name = "bco_baterias[v_carga]" class="form-control" placeholder="Introduzca Carga de la Localidad">
								                      	</div>
								                    </div>

												</div>
												<div class="row">
								                   
									                <div class="col-sm-4">
								                      <!-- text input -->
								                      	<div class="form-group">
									                        <label>Numero de Inventario</label>
									                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cantidad de Elementos Criticos','Solo números sin letras. Ej. 1, 10, 20, etc.', 150 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
									                        <input type="text" name = "bco_baterias[celdas]" class="form-control" placeholder="Introduzca Numero de Celdas Dañadas">
								                      	</div>
								                    </div>

								                    <div class="col-sm-4">
								                      <!-- text input -->
								                      	<div class="form-group">
									                        <label>Fecha Instalaciòn</label>
									                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cantidad de Elementos Criticos','Solo números sin letras. Ej. 1, 10, 20, etc.', 150 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
									                        <input type="date" name = "bco_baterias[fecha_instalacion]" class="form-control" placeholder="Introduzca Numero de Celdas Dañadas">
								                      	</div>
								                    </div>
								                 
								                    <div class="col-sm-4">
								                      <!-- text input -->
									                    <div class="form-group">
									                    	<label>Status Ups:</label>
									                    	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Inventario Cantv','Código interno de inventario en Cantv. Ej. 223344', 150 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
									                        <input type="text" class="form-control" name="bco_baterias[num_inv]"  placeholder="Introduzca Inventario Cantv">
									                    </div>
								                    </div>
												</div>
							           		
										</div>
					        		</div>
												<div id="banbo_bateria"></div>
					        	</div>

					        	 <div class="col-md-12">
				            	<div class="card card-success">
						            <div class="card-header">
					                <h3 class="card-title">Cuestionario Sistema ininterrumpido de potencia</h3>
						            </div>
						            <div class="card-body">

													<table  class=" display nowrap  table table-bordered table-striped" style="width:100%;">
						                <thead>
						                  <tr align="center" >
						                    
						                   
						                    <th>Tipo Actividad</th>
						                    <th>Pregunta</th>
						                    <th>Supervision</th>
						                    <th>Personal Cosec</th>
						                  </tr>
						                </thead>
					              	  <tbody>
															@foreach($cuestionario as $question)
			                  			<tr align="center">
                     						<td>
									                 	<input type="checkbox" name="bco_baterias[tipo]" id="tipo" value="1" style="margin-right: 2px;">T
									                  <input type="checkbox" name="bco_baterias[tipo]" id="tipo" value="2">C
                     						</td>
                     						<td style="font-size:55%; ">{{$question->descripcion}}</td>
                     						<td >
									                 	<input type="checkbox" name="bco_baterias[tipo]" id="tipo" value="1">Bueno <br>
									                  <input type="checkbox" name="bco_baterias[tipo]" id="tipo" value="2">Malo
                     						</td>
                     						<td>
                     							<select id="personal_{{$question->id}}" class="form-control select2">
				                        	
					                        		@foreach($personal as $pers)
						                        		<option value="{{$pers->id}}">{{$pers->nombre}}</option>
						                        	@endforeach
							                    	</select>
                     							
                     						</td>
                     					</tr>
														@endforeach					
                     				</tbody>
                     			</table>
								             
											
										</div>
					        		</div>
												<div id="banbo_bateria"></div>
					        	</div>
				              
				              	<div class="card-footer" align="right">
									<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
								</div>
							</form>
						</div>
            		</div>
        		</div>
    		</div>
		</div>
	</section>
</div>

@endsection


@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_rect") !!}
@endif



<script type="text/javascript">

$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });






  	$('select[id=region]').change(function () {
    	console.log('hola')
    var region_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'estado/' + region_id, function (cidades) {
        $('select[id=estado]').empty();

        if(cidades.length != 0){
            $('select[id=estado]').append('<option value=""> Selecciona un estado</option>');
            $.each(cidades, function (key, value) {
                $('select[id=estado]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });

    });


	$('select[id=estado]').change(function () {
    	//console.log('hola')
    var estado_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'central/' + estado_id, function (cidades) {
        $('select[id=central]').empty();

        if(cidades.length != 0){
            $('select[id=central]').append('<option value=""> Selecciona un central</option>');
            $.each(cidades, function (key, value) {
                $('select[id=central]').append('<option value=' + value.id + '>'+ value.nombre +' - '+ value.tipo + '</option>');
            });
        }

        });

    });



});

    function deleteItem(item){
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Desea eliminar este banco de bateria del intentario?',
      text: "Estas por quitar este equipo del Inventario!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, quitarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {


      if (result.value) {



            $.ajax({
                url: "/bancob/"+item,
                type:"DELETE",
                   headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    

                    success:function(result, status, xhr){                    
                        
                        if (status == 'success') {

                          swalWithBootstrapButtons.fire(
                            'Quitado con Exito!',
                            'Se eliminó con exito',
                            'success'
                          )

                          $("#tdId_"+item).hide();
                         // txt.innerHTML = result ;



                        }
                    },
                    error:function(xhr, status, error){
                      

                      Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se eliminó el producto, vuelva a intentarlo mas tarde '+error,
                        footer: '<a href>Seguramente hubo un problema de conexión</a>'
                      })
                      

                    }
                });
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No se ha quitado el producto.',
          'error'
        )
      }
    })
  }
</script>
@endsection
