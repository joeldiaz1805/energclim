@extends ('/layouts/index')

@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Facturas
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Facturas </a></li>
              <li class="breadcrumb-item active">lista</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Vista Preliminar de Registros de Facturas</h3>
              <div align="right">
                  <a href="/facturas/create" class="btn btn-success">Agregar factura</a>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class=" display nowrap  table table-bordered table-striped" style="width:100%">
                <thead>

                  <tr align="center">
                    <th class="not-export-col">Acciones</th>
                    <th>Gerencia</th>
                    <th>Codigo Proveedor</th>
                    <th>Nombre Proveedor</th>
                    <th>N° de Pedido</th>
                    <th>Status</th>
                    <th>Modo de Pago</th>
                    <th>Fecha de Recepción</th>
                    <th>Fecha de Pago</th>
                    <th>N° de Factura</th>
                    <th>Monto Bs</th>
                    <th>Monto USD</th>
                    <th>Tasa Cambio</th>
                    <th>Prioridad</th>
                    <th>Descripción</th>
                    <th>Justificación</th>
                    <th>Ultima Actualizacion</th>
                    <th>Usuario</th>
                  </tr>
                </thead>
                <tbody>
                 @foreach($facturas as $factura)
                  <tr align="center"  id="tdId_{{$factura->id}}">
                    <td>
                     <a href="/facturas/{{encrypt($factura->id)}}/edit" class="btn btn-warning"><i class="far fa-edit"></i></a>
                      <!--a href="/facturas/{{encrypt( $factura->id)}}" class="btn btn-success"><i class="far fa-eye"></i></a-->
                     <a onclick="deleteItem('{{encrypt($factura->id)}}')" class="btn btn-danger"><i id="ele"  class="fa fa-user-times"></i></a>
                      
                      </td>
                    <td>{{$factura->gerencia}}</td>
                    <td>{{$factura->contrato->proveedor->cod_proveedor}}</td>
                    <td>{{$factura->contrato->proveedor->nombre_proveedor}}</td>
                    <td>{{$factura->n_pedido}}</td>
                    <td>@if($factura->status == '1') Pagado @else  Pendiente @endif</td>
                    <td>{{$factura->monto_factura}}</td>
                    <td>{{$factura->fecha}}</td>
                    <td>{{$factura->fecha_pago}}</td>
                    <td>{{$factura->nro_factura}}</td>
                    <td>{!! number_format($factura->monto_factura,2) !!}@if($factura->modo_pago == 1) $ @else Bsf @endif </td>
                    <td>{!! number_format($factura->monto_factura,2) !!}@if($factura->modo_pago == 1) $ @else Bsf @endif </td>
                    <td> - </td>


                    <td>@if($factura->prioridad == 1 ) Baja @elseif($factura->prioridad == 2) Media @else Alta @endif</td>
                    <td>{{$factura->trabajo_realizado}}</td>
                    <td> - </td>
                    <td>{{$factura->updated_at}}</td>
                    <td>{{$factura->usuario->name ?? 'nobody'}} </td>
                  </tr>
                  @endforeach
                
                </tbody>
              </table>
           </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('scriptsalerts')


<script>

  
</script>

  <script>

    function deleteItem(item){
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'Â¿Desea Inhabilitar este usuario de la lista?',
      text: "Estas por Inhabilitar un usuario!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, Inhabilitarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {

      if (result.value) {



            $.ajax({
                url: "/facturas/"+item,
                type:"DELETE",
                   headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    

                    success:function(result, status, xhr){                    
                        
                        if (status == 'success') {

                          swalWithBootstrapButtons.fire(
                            'Inhabilitado con Exito!',
                            'Se Inhabilito con exito',
                            'success'
                          )

                          //$("#tdId_"+item).hide();
                          document.getElementById("intt").className="btn btn-danger";
                          //intt.style(color:green);
                           document.getElementById("act_"+item).innerHTML = "Inativo";
                          //$("#act_"+item).innerHTML = 'Inativo' ;



                        }
                    },
                    error:function(xhr, status, error){
                      

                      Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se Inhabilito ningun usuario '+error,
                        footer: '<a href>Seguramente hubo un problema de conexiÃ³n</a>'
                      })
                      

                    }
                });
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No se ha quitado el producto.',
          'error'
        )
      }
    })
  }
  function activeItem(item){
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'Â¿Desea Habilitar este usuario de la lista?',
      text: "Estas por Habilitar un usuario!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, Habilitarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {

      if (result.value) {



            $.ajax({
                url: "/Factura/"+item,
                type:"DELETE",
                   headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    

                    success:function(result, status, xhr){                    
                        
                        if (status == 'success') {

                          swalWithBootstrapButtons.fire(
                            'Habilitado con Exito!',
                            'Se HabilitÃ² con exito',
                            'success'
                          )

                          //$("#tdId_"+item).hide();
                          document.getElementById("intt").className="btn btn-success";
                          var h= document.getElementById("act_"+item);
                          h.innerHTML = "Activo";

                          //$("#act_"+item).innerHTML = 'Activo' ;



                        }
                    },
                    error:function(xhr, status, error){
                      

                      Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se Habilitado ningun usuario '+error,
                        footer: '<a href>Seguramente hubo un problema de conexiÃ³n</a>'
                      })
                      

                    }
                });
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No se ha Habilitado el usuario.',
          'error'
        )
      }
    })
  }
</script>


@endsection
