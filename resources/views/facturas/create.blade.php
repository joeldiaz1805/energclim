@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Registrar Nueva Factura
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/facturas">Factura</a></li>
              <li class="breadcrumb-item active">Crear </li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
             
              <div class="card-body">
                <form method="POST" id="formulario_Factura" name="formulario_Factura"  action="{{ url('/facturas')}}" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <input type="hidden" value="{{Auth::user()->id}}" name="user_id">
                  <div class="col-md-12">
			              <div class="card card-success">
 				              <div class="card-header">
 				                <h3 class="card-title">Nuevo Factura</h3>
                       </div>
				              <div class="card-body">
                        




                        <div class="row">
                          <div class="col-sm-4">
                            <!-- text input -->
                              <div class="form-group">
                                <label>Región:</label>
                                <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Región','Seleccione región donde está ubicado el AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                  <select  name="region_id" id="region" class="form-control">
                                    <option>Seleccione una Región</option>
                                    @foreach($regiones as $region)
                                      <option value="{{$region->id}}">{{$region->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                          </div>
                          
                         
                            <div class="col-sm-4">
                            <!-- text input -->
                            <div class="form-group">
                              <label>Estado:</label>
                              <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Estado','Seleccione estado donde está ubicado el AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                              <select name="estado_id" id="estado" class="form-control">
                                
                                  <option value="">Seleccione un estado</option>
                                
                            </select>
                            </div>
                          </div>
                         
                          <div class="col-sm-4">
                            <!-- text input -->
                              <div class="form-group">
                                <label>Central:</label>
                                <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Central','Seleccione central donde está ubicado el AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                  <select name="localidad_id" id="central" class="form-control">
                                    
                                      <option value="">Seleccione una Localidad</option>
                                    
                                </select>
                            </div>
                            </div>
                
                       </div>
                       <div class="row">
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Gerencia:</label>
                              <select name="gerencia" class="form-control">
                                <option value="">Seleccione una Gerencia</option>
                                <option value="Coordinación Normas, Análisis y Diseño">Coordinación Normas, Análisis y Diseño</option>
                                <option value="Coordinación Seguimiento y Control de Gestión">Coordinación Seguimiento y Control de Gestión</option>
                                <option value="Coordinación Seguimiento y Control de Gastos">Coordinación Seguimiento y Control de Gastos</option>
                                <option value="Coordinación Seguimiento y Control de Inversión">Coordinación Seguimiento y Control de Inversión</option>
                                <option value="Cosec">Cosec</option>
                                <option value="Gerencia General">Gerencia General</option>
                                <option value="Gerencia de Seguimiento y Control">Gerencia de Seguimiento y Control</option>
                                <option value="Gcia. Cost. Energía y Climatización">Gcia. Cost. Energía y Climatización</option>
                                <option value="Gcia. Oper. Mtto Reg. Capital">Gcia. Oper. Mtto Reg. Capital</option>
                                <option value="Gcia. Oper. Mtto Reg. Centro Oriente">Gcia. Oper. Mtto Reg. Central</option>
                                <option value="Gcia. Oper. Mtto Reg. Guayana">Gcia. Oper. Mtto Reg. Guayana</option>
                                <option value="Gcia. Oper. Mtto Reg. Centro Oriente">Gcia. Oper. Mtto Reg. Centro Oriente</option>
                                <option value="Gcia. Oper. Mtto Reg. Centro Occidente">Gcia. Oper. Mtto Reg. Centro Occidente</option>
                                <option value="Gcia. Oper. Mtto Reg. Occidente">Gcia. Oper. Mtto Reg. Occidente</option>
                                <option value="Gcia. Oper. Mtto Reg. Los Andes">Gcia. Oper. Mtto Reg. Los Andes</option>
                                <option value="Gcia. de Proyectos e Ingeniería">Gcia. de Proyectos e Ingeniería</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Número de Pedido</label>
                              <input type="text" name="n_pedido" required class="form-control" placeholder="Número de Pedido">
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <label>Modo Pago</label>
                            <select name="modo_pago" class="form-control">
                              <option value="1">VES</option>
                              <option value="2">USD</option>
                            </select>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Codigo de Proveedor: <a onclick="buscar()" style="color: blue"><i class="fa fa-search"></i></a></label>
                              <input type="text" name="cod_proveedor" id="cod_proveedor" required class="form-control" placeholder="Codigo de Proveedor:">
                              
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Nombre de Proveedor</label>
                              <input type="text" disabled id="nombre_proveedor" class="form-control" placeholder="Nombre de Proveedor">
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <label>Prioridad</label>
                            <select name="prioridad" class="form-control">
                              <option value="1">Baja</option>
                              <option value="2">Media</option>
                              <option value="3">Alta</option>
                            </select>
                          </div>
                        </div>

                        <div class="row">
		                      <div class="col-sm-4">
	 	                        <div class="form-group">
  	                          <label>Fecha de Recepción</label>
					                      <input type="date" name="fecha"  class="form-control" maxlength="49" placeholder="Nombres">
					                  </div>
				                  </div>

  			                  <div class="col-sm-4">
				                    <div class="form-group">
				                      <label>Numero de Contrato</label>
                                <select name="contrato_id" id="nro_contrato" class="form-control select2">
                                  <option value="">Numero de Contrato</option>
<!--
                                    @foreach($contratos as $cont)
                                      <option value="{{$cont->id}}">{{$cont->nro_contrato}} - {{$cont->proveedor->nombre}}</option>
                                    @endforeach-->
                                </select>
				                     </div>
				                   </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Numero de Factura</label>
                                <input type="text" name="nro_factura"  class="form-control" maxlength="49" placeholder="Numero de Factura">
                            </div>
                          </div>
				                </div>
                        <div class="row">
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Status de la Factura</label>
                                <select name="status" id="status" class="form-control">
                                  <option value="">Status</option>
                                      <option value="1">Pagada</option>
                                      <option value="2">Pendiente por Pagar</option>
                                </select>
                             </div>
                           </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Monto Factura</label>
                              <input type="text" disabled onblur="monto_superado()" name="monto_factura" id="monto_factura" required class="form-control" placeholder="Monto Factura"> 
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Monto Disponible para la factura</label>
                              <input type="text" readonly name="sumatoria" id="sumatoria" required class="form-control" maxlength="20" placeholder="Monto Factura"> 
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Equipo a intervenir</label>
                              <select name="equipo_id"  class="form-control select2">
                                  <option value="">Seleccione un Equipo</option>
                                    @foreach($equipos as $equipo)
                                      <option value="{{$equipo->id}}">{{$equipo->descripcion}}</option>
                                    @endforeach
                                </select>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="comment">Trabajo Realizado:</label>
                              <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Observaciones','Trabajo Realizado', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                              <textarea class="form-control" rows="3" name="trabajo_realizado" id="comment"></textarea>      
                            </div>
                          </div>
                        </div>
                      </div>
                <!-- CIERRA EL CAR CAR SUCES  --> 
         </div>
         <!--CIERRA EL CSFR-->
				</div>
      
         <div class="card-footer" align="right">
			     <button type="submit" disabled id="boton_crear" class="btn btn-primary">Crear</button>
			   </div>
			               

    </form>
                  </div>
		            </div>
		        </div>
            </div>
        </div>
  	</section>
</div>


@endsection


@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_Factura") !!}
@endif



<script type="text/javascript">
  function monto_superado(){
    var monto = document.getElementById('monto_factura').value
    var disp = document.getElementById('sumatoria').value

    if (monto > disp){
        toastr.error('El monto supera el disponible.');
        $('#boton_crear').prop('disabled',true);
      }else{
        
        $('#boton_crear').prop('disabled',false);
      }

  }


  function buscar(){
    var codigo = document.getElementById('cod_proveedor').value
    var nombre = document.getElementById('nombre_proveedor')
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    console.log(codigo)

    $.get(APP_URL+'facturas_buscar_proveedor/'+codigo, function (cidades) {
          console.log(cidades);
         if (cidades == false){
            toastr.error('Codigo de Proveedor no encontrado');
            nombre.value = "No encontrado";
            $('select[id=nro_contrato]').empty();

         }else{
            nombre.value = cidades.nombre;
            $('select[id=nro_contrato]').empty();
                if(cidades.contratos.length != 0){
                    $('select[id=nro_contrato]').append('<option value=""> Selecciona un Contrato</option>');
                    $.each(cidades.contratos, function (key, value) {
                        $('select[id=nro_contrato]').append('<option value=' + value.id + '>'+ value.nro_contrato + '</option>');
                    });
                }

         }
         // $('#monto_factura').prop('disabled',false);

        });

    };

  $('select[id=nro_contrato]').change(function () {
    var nro_contrato = $(this).val();
    var monto = document.getElementById('monto_factura').value
    var sumatoria = document.getElementById('sumatoria');
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'facturas_suma/'+nro_contrato, function (cidades) {
          console.log(cidades)
          sumatoria.value = cidades;
          $('#monto_factura').prop('disabled',false);

        });

    });
$(document).ready(function(){



  


  $('select[id=region]').change(function () {
    var region_id = $(this).val();
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'estado/' + region_id, function (cidades) {
      console.log('hole')
        $('select[id=estado]').empty();
        if(cidades.length != 0){
            $('select[id=estado]').append('<option value=""> Selecciona un estado</option>');
            $.each(cidades, function (key, value) {
                $('select[id=estado]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });

    });


  $('select[id=estado]').change(function () {
      //console.log('hola')
    var estado_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'central/' + estado_id, function (cidades) {
        $('select[id=central]').empty();

        if(cidades.length != 0){
            $('select[id=central]').append('<option value=""> Selecciona un central</option>');
            $.each(cidades, function (key, value) {
                $('select[id=central]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });

    });


  });
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
});



  
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });

</script>
@endsection