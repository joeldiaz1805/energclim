<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GGEC</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
  
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
   <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">

  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="{{asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
  <!-- Toastr -->
  <link rel="stylesheet" href="{{asset('plugins/toastr/toastr.min.css')}}">
    <!-- Select2 -->
  <link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
 
</head>
<body  style="background-image: url('{{ asset('images/fondo.jpg') }}'); no-repeat center center fixed; background-size: 100% 100%;" class="hold-transition login-page " >
    
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
    <div style="margin-top: -150px">
	     <div class="banner">
	        <img src="{{asset('images/banner.png')}}" width="500" height="70">
	        <h2 style="color: black">Bienvenido al Portal Web Interactivo</h2>
	    </div>
    </div>
	
            <div class="card card-info" style="width: 90%; height: 80%">
              <div class="card-header">
                <h3 class="card-title">Generador de Texto Sismyc</h3>
              </div>
              <div class="card-body">

				
				        	<div class="row">
	              				<div class="col-sm-4">
			                		<label>Actividad:</label>
		                  			<div class="col-sm-12">
				                      	<div class="form-group">
				                        	<div class="form-check" required>
					                          <input class="form-check-input" onblur="generar()" id="checkbox1" type="radio" checked required value="RUTINARIA" name="tipo_mantenimiento">
					                          <label class="form-check-label">RUTINARIA</label><br>
					                          <input class="form-check-input" onblur="generar()" id="checkbox2" type="radio" required value="CNE"  name="tipo_mantenimiento">
					                          <label class="form-check-label">CNE</label><br>
					                          <input class="form-check-input" onblur="generar()" id="checkbox3" type="radio" required value="ATENCION DE FALLAS"  name="tipo_mantenimiento">
					                          <label class="form-check-label">ATENCION DE FALLAS</label><br>
					                          <input class="form-check-input" onblur="generar()" id="checkbox4" type="radio" required value="GESTION"  name="tipo_mantenimiento">
					                          <label class="form-check-label">GESTION</label><br> 
					                        </div>
			                    		</div>
		                    		</div>
		                 		</div>
		                 		<div class="col-sm-4">
			                		<label>Contratista:</label>
				                  	<div class="col-sm-12">
				                     
				                      	<div class="form-group">
				                        	<select id="provedores" onchange="generar()" class="form-control select2 ">
						                       	<option value="">Seleccione un provedor</option>
						                       	@foreach($contratistas as $contratos)
						                       		<option @if($contratos->nombre == "CANTV") selected @endif value="{{$contratos->nombre}}_">{{$contratos->nombre}}</option>
						                       	@endforeach
						                       	
				                    		</select>
					                    </div>
				                    </div>
			                 	</div>
			                 	<div class="col-sm-4" style="display: none">
			                		<label>Tipo de Servicio:</label>
				                  	<div class="col-sm-4">
				                     
				                      	<div class="form-group">
				                        	
				                        	<div class="form-check" required>
					                          <input class="form-check-input" onblur="generar()" type="radio" id="tipo_servicio" value="Ex" name="tipo_servicio">
					                          <label class="form-check-label">Ex</label><br>
					                          <input class="form-check-input" onblur="generar()" type="radio" id="tipo_servicio2" value="A/A" name="tipo_servicio">
					                          <label class="form-check-label">A/A</label>
					                        </div>
					                    </div>
				                    </div>
			                 	</div>
			                 	



		              		</div>
							<div class="row">
		                  		<div class="col-sm-12">
		                      		<div class="form-group">
		                        		<label>Localidad:</label>
			                        	<select  id="centrales" onchange="generar()" class="form-control select2 ">
			                        		<option value="">Seleccione una Central</option>
			                        		@foreach($localidades as $centrales)
			                        			@if($centrales->estados)
				                        			<option value="{{$centrales->nombre}}-{{$centrales->estados->nombre}}-{{$centrales->municipio->descripcion ?? 'S/M'}}-{{$centrales->estados->region->nombre}}-{{$centrales->tipo}}">{{$centrales->nombre}} - {{$centrales->municipio->descripcion ?? 'S/M'}} - {{$centrales->estados->nombre}} - {{$centrales->estados->region->nombre}} - {{$centrales->tipo}}</option>
				                        		@endif
				                        	@endforeach
				                    	</select>
			                    	</div>
			                	</div>
		                	</div>

		                	<div class="row">
		                    	<div class="col-sm-2" >
			                      	<div class="form-group">
			                      		<label>Regiòn:</label>
					                	<input type="text" class="form-control" readonly id="region" name="">
			                    	</div>
		                      	</div>
		                      	<div class="col-sm-2">
		                      		<div class="form-group">
			                      		<label>Estado:</label>

					                	<input type="text" class="form-control" readonly id="esta2" name="">
		                      		</div>
		                      	</div>

		                      	<div class="col-sm-2">
		                      		<div class="form-group">
			                      		<label>Municipio:</label>

					                	<input type="text" class="form-control" readonly id="municipio" name="">
		                      		</div>
		                      	</div>
		                      	<div class="col-sm-4">
		                      		<div class="form-group">
			                      		<label>Localidad:</label>
					                    <input type="text"  class="form-control" readonly id="central2" name="">
		                      		</div>
		                      	</div>
		                      	<div class="col-sm-2">
		                      		<div class="form-group">
			                      		<label>Tipo:</label>
					                    <input type="text" onblur="generar()" class="form-control" readonly id="tipo" name="">
		                      		</div>
		                      	</div>
		                		
		                	</div>

		                	<div class="row">

		                    	<div class="col-sm-4">
			                     
			                    	<div class="form-group">
			                        	<label>Equipo:</label>
				                        <select name="equipo_id" id="equipo" onchange="generar()" class="form-control select2">
				                        	<option value="">Seleccione un equipo</option>


			                            	@foreach($equipos as $eq)
			                            		<option value="{{$eq->descripcion}}">{{$eq->descripcion}}</option>
			                            	@endforeach
			                            	
										</select>

			                      	</div>
			                    </div>
			                    <div class="col-sm-4" id="Correctivo" >
			                    
			                      	<div class="form-group">
				                        <label>Acciòn:</label>
				                        <select name="tipo_falla" onchange="generar()" id="fallas" class="form-control select2">
			                            	


			                            	<option value="">Seleccione una Acciòn</option>
			                            	@foreach($acciones as $accion)
			                            		<option value="{{$accion->nombre}}">{{$accion->nombre}}</option>
			                            	@endforeach
										</select>
				                    </div>
				                    <div class="form-group">
				                    	<input type="hidden" id="accion1" class="form-control" >
			                        	
				                    	<div  id="input_accion" style="display: none">
				                    		<input type="text" onblur="generar()" id="accion2" class="form-control" >
				                    	</div>
				                    </div>
			                    </div>
			                  
			                	
			                	<div class="col-sm-4">
			                        <div class="form-group">
			                        	<label>Sala:</label>
			                        	<select name="sala" onchange="generar()" id="box_ModeloRectificador" name="box_ModeloRectificador" class="form-control select2">
											<option value="">Seleccione una Sala</option>
											@foreach($salas as $sala)
			                            		<option value="{{$sala->nombre}}">{{$sala->nombre}}</option>
			                            	@endforeach
										</select>
			                        </div>
		                    	</div>
		                    	<div class="col-sm-4">
		                      		<div class="form-group">
		                        		<label>Piso:</label>
				                        	<select id="piso" onchange="generar()" name="box_ModeloRectificador" class="form-control select2 ">
												<option value="">Seleccione un piso</option>
												@foreach($pisos as $piso)
				                            		<option value="{{$piso->nombre}}">{{$piso->nombre}}</option>
				                            	@endforeach
											</select>
			                    	</div>
	                      		</div>
		                      	<div class="col-sm-4">
			                      	<div class="form-group">
			                        	<label>Origen:</label>
				                        	<input type="text" onblur="generar()" id="personal" name="" placeholder="Reporta" class="form-control">
				                    </div>
			                    </div>
			                    <div class="col-sm-4">
			                      	<div class="form-group">
			                        	<label>Teléfono:</label>
		                        		<div class="input-group">
						                    <div class="input-group-prepend">
						                      	<span class="input-group-text"><i class="fas fa-phone"></i></span>
						                    </div>
						                    <input type="text" onblur="generar()" id="telefono" name="telefono" required  class="form-control" data-inputmask='"mask": "99999999999"' data-mask>
						                </div>
								    </div>
			                    </div>
		                    </div>
		                    <div class="row">
		                    	<div class="col-sm-4">
			                      	<div class="form-group">
			                        	<label>Aula:</label>
				                        	<input type="text" onblur="generar()" id="aula" name="" placeholder="aula" class="form-control">
				                    </div>
			                    </div>
		                    </div>

			               
		                  	<div class="row">
			                    <div class="col-sm-12">
					                    <div class="form-group">
										<label for="comment">Informaciòn Adicional:</label>
										<textarea class="form-control"  onblur="generar()" name="info" rows="3" id="info"></textarea>			
									</div>
			                    </div>
		                  	</div>

		                	<div class="row">
			                	<div class="col-sm-4" style="display: none">
			                		<a onclick="generar()" class="btn btn-info">Generar comentario de apertura</a>
			                	</div>
		                    </div>

		                  		
			                  		
		                  		
		                  	<div class="row">

			                    <div class="col-sm-12">
				                     <div class="form-group">
										<label >Texto Generado:
											<button class="btn btn-success" data-clipboard-target="#comment">
											  <i class="fa fa-clipboard"></i> Copiar
											</button>
										</label>
										<textarea  readonly class="form-control"  name="actividad" rows="3" id="comment"></textarea>			
									</div>
			                    </div>
			                </div>
              </div>
              
            </div>
          
          
            

	
  
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('plugins/select2/js/select2.full.min.js')}}"></script>

<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- SweetAlert2 -->
<script src="{{asset('dist/js/clipboard.min.js')}}"></script>

  <script src="{{asset('plugins/sweetalert2/sweetalert2.min.js')}}"></script>
  <!-- Toastr -->
  <script src="{{asset('plugins/toastr/toastr.min.js')}}"></script>

<script src="{{asset('js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>



<script type="text/javascript">
var clipboard = new Clipboard('.btn');
$('.select2').select2();

function generar(){
	var rt;
	var mnt;
	var fll = " -";
	var equipo1 = document.getElementById('checkbox1');
	var equipo2 = document.getElementById('checkbox2');
	var equipo3 = document.getElementById('checkbox3');
	var equipo4 = document.getElementById('checkbox4');
	var provedores = document.getElementById('provedores');
	var fallas = document.getElementById('fallas');
	var central = document.getElementById('central2');
	var estados = document.getElementById('esta2');
	var municipio = document.getElementById('municipio');
	var region = document.getElementById('region');
	var aula = document.getElementById('aula');
	var tipo = document.getElementById('tipo');
	

	if (equipo1.checked){
		rt = equipo1.value
	}if (equipo2.checked){
		fll =' que se encuentra con falla: '+fallas.options[fallas.selectedIndex].text
		rt = equipo2.value
	}if (equipo3.checked){
		rt = equipo3.value
	}if (equipo4.checked){
		rt = equipo4.value
	}
	var mantenimiento = document.getElementById('tipo_servicio');
	var mantenimiento2 = document.getElementById('tipo_servicio2');

	var personal = document.getElementById('personal');
	var tipo_actividad = document.getElementById('mantenimiento');
	
	var box_ModeloRectificador = document.getElementById('box_ModeloRectificador');

	var equipo = document.getElementById('equipo');
	var telefono = document.getElementById('telefono');
	var info = document.getElementById('info');
	var componente = document.getElementById('componente');

	var piso = document.getElementById('piso');
	var accion2 = document.getElementById('accion2');
	var accion1 = document.getElementById('accion1');
	if (equipo.value == 'EXTRACTOR' || equipo.value == 'VENTILADOR' || equipo.value == 'AIRE ACONDICIONADO' || equipo.value == 'CHILLER' || equipo.value == 'UMA' || equipo.value == 'VARIOS(A/A)' ){
		mnt=  'A/A'
	}else{
		mnt = "EX"
	}

	if(fallas.options[fallas.selectedIndex].text == "OTROS(ESPECIFIQUE)"){
		accion1.value = "OTROS:";
		$('#input_accion').show();
	}else{
		$('#input_accion').hide()
		accion1.value = ""
		accion2.value = fallas.options[fallas.selectedIndex].text
	}
	
	document.getElementById('comment').value= central2.value+'_'
	+tipo.value.toUpperCase()+'_'
	+accion1.value+accion2.value.toUpperCase()+'_'
	+equipo.options[equipo.selectedIndex].text+'_'
	+box_ModeloRectificador.options[box_ModeloRectificador.selectedIndex].text+'_'
	+piso.options[piso.selectedIndex].text+'_'
	+aula.value+'_'
	+personal.value.toUpperCase()+'_'
	+telefono.value+'_'
	+info.value.toUpperCase()+'_'
	+mnt+'_'
	+estados.value+'_'
	+region.value+'_'
	+rt+'_'
	+provedores.value
	+municipio.value+'_'

}


  $(document).ready(function(){


  	$('select[id=centrales]').change(function () {
    var central = $(this).val();
    var valores = central.split('-')
    //console.log('hola',valores)
  	
  	document.getElementById('tipo').value = valores[4]
  	document.getElementById('central2').value = valores[0]
  	document.getElementById('esta2').value = valores[1]
  	document.getElementById('municipio').value = valores[2]
  	document.getElementById('region').value =valores[3]
  	//console.log(cidades[0].tipo)
  	if (valores[3]==""){
  		$('#tipo').attr("readonly",false)
  	}else{
  		$('#tipo').attr("readonly",true)

  	}
  	generar();
    
    
    // var APP_URL = {!!json_encode(url('/'))!!}+'/';
    // $.get(APP_URL+'centrales/' + central, function (cidades) {
    //  //$('select[id=estado]').empty();
    //  	console.log(cidades[0].nombre, cidades[0].estados.nombre, cidades[0].estados.region.nombre)
     	



    //     });

    });
  });


</script>
</body>
</html>


