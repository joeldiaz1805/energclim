<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GGEC</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
  
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
   <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">

  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="{{asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
  <!-- Toastr -->
  <link rel="stylesheet" href="{{asset('plugins/toastr/toastr.min.css')}}">
    <!-- Select2 -->
  <link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
 
</head>
<body  style="background-image: url('{{ asset('images/fondo.jpg') }}'); no-repeat center center fixed; background-size: 100% 100%;" class="hold-transition login-page " >
    
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
    <div style="margin-top: -150px">
	     <div class="banner">
	        <!--img src="{{asset('images/banner.png')}}" width="500" height="70"-->
	        <!--h2 style="color: black">Bienvenido al Portal Web Interactivo</h2-->
	    </div>
    </div>
	
            <div class="card" style="width: 90%; height: 80%;">
              <div class="card-header" style="background-color: #16365c !important; ">
              	<img src="{{asset('images/cosec.png')}}" style="width: 12%; height: 12%;" align="left">
              
                <h3 class="card-title" style="color:white; margin-left: 27%; margin-top: 2%; font-size: 160%;">GGEC/GSC/COSEC</h3><br>
                <h3 class="card-title" style="color:white; margin-left: 12%; font-size: 160%;">GERENCIA GENERAL ENERGIA Y CLIMATIZACION</h3>
              	<img src="{{asset('images/cantv.png')}}" style="width: 20%; height: 20%;margin-top: -12%; margin-bottom:-12%;" align="right">
              </div>
              <div class="card-body" style="background-color:#d3c8c8">
			        	<div class="row">
           				<div class="col-sm-3">
                		<label>Actividad:</label>
                 			<div class="col-sm-12">
                      	<div class="form-group">
                        	<div class="form-check" required>
		                          <input class="form-check-input" onblur="generar()" onclick="actividad()" id="checkbox2" type="radio" required value="ATENCION DE FALLAS"  name="tipo_mantenimiento">
		                          <label class="form-check-label">ATENCION DE FALLAS</label><br>
	                          	<input class="form-check-input" onblur="generar()" onclick="actividad()" id="checkbox3" type="radio" required value="GESTION"  name="tipo_mantenimiento">
		                          <label class="form-check-label">GESTION</label><br> 
		                          <input class="form-check-input" onblur="generar()" onclick="actividad()" id="checkbox1" type="radio"  required value="PROGRAMADO" name="tipo_mantenimiento">
		                          <label class="form-check-label">PROGRAMADO</label><br>
		                          <input class="form-check-input" onblur="generar()" onclick="actividad()" id="checkbox4" type="radio"  required value="NO PROGRAMADO" name="tipo_mantenimiento">
		                          <label class="form-check-label">NO PROGRAMADO</label><br>
		                          
		                        </div>
	                    		</div>
                    		</div>
	                 		</div>
			                 	
		                 	<div class="col-sm-3">
		                		<label>Especialidad:</label>
		                  	<div class="col-sm-4">
	                      	<div class="form-group">
	                        	<div class="form-check" required>
		                          <input class="form-check-input" onblur="generar()" type="radio" onclick="servicio()"  id="servicio2" value="2" name="tipo_servicio">
		                          <label class="form-check-label">CLIMATIZACIÓN</label><br>
		                          <input class="form-check-input" onblur="generar()" type="radio" onclick="servicio()"  id="servicio1" value="1" name="tipo_servicio">
		                          <label class="form-check-label">ENERGIA</label><br>
		                        </div>
			                    </div>
		                    </div>
		                 	</div>
		                 	
		                 	<div class="col-sm-3">
			                	<label>Usuario:</label>
				               	<div class="col-sm-4">
	                      	<div class="form-group">
	                        	<div class="form-check" required>
		                          <input class="form-check-input" onblur="generar()" checked type="radio" onclick="servicio()"  id="usuario1" value="1" name="usuario">
		                          <label class="form-check-label">CANTV</label><br>
		                          <input class="form-check-input" onblur="generar()" type="radio" onclick="servicio()"  id="usuario2" value="2" name="usuario">
		                          <label class="form-check-label">CNE</label><br>
		                        </div>
					                </div>
			                  </div>
		                 	</div>
			                 	
	                 		<div class="col-sm-3">
		                		<label>Contratista:</label>
		                  	<div class="col-sm-12">
	                      	<div class="form-group">
	                        	<select id="provedores" onchange="generar()" class="form-control select2bs4 ">
			                       	<option value="">Seleccione un provedor</option>
			                       	@foreach($contratistas as $contratos)
			                       		<option @if($contratos->nombre == "CANTV") selected @endif value="{{$contratos->nombre}}_">{{$contratos->nombre}}</option>
			                       	@endforeach
	                    			</select>
		                    	</div>
		                    </div>
		                 	</div>

	              		</div>

	              		<div class="row">
	              			<div class="col-sm-4">
                    		<div class="form-group">
                      		<label>Estados:</label>
                      		<div class="select2-purple">
	                        	<select  id="estados" onchange="generar()" class="form-control select2bs4"  data-placeholder="Seleccine un estado" style="width: 100%;">
	                        		<option value="">Seleccione una Central</option>
		                        		@foreach($estados as $estado)
		                        			@if($estado)
			                        			<option value="{{$estado->id}}" >{{$estado->nombre}} </option>
			                        		@endif
			                        	@endforeach
		                    		</select>
                      			
                      		</div>
	                    	</div>
		                	</div>
	              		</div>

										<div class="row">
                  		<div class="col-sm-12">
                    		<div class="form-group">
                      		<label>Buscar:</label> <i class="fa fa-search"></i>
                        	<select  id="centrales" onchange="generar()" class="form-control select2bs4">
                        		<option value="">Seleccione una Localidad</option>
                        		
	                    	</select>
                    	</div>
		                	</div>
	                	</div>

		                <div class="row">
		                 	<div class="col-sm-2" >
			                    	
                    	<div class="form-group">
			                  <label>Región:</label>
			                	<input type="text" class="form-control" readonly id="region" name="">
	                    </div>
		                </div>
                  	
                  	<div class="col-sm-2">
                  		<div class="form-group">
                    		<label>Estado:</label>
			                	<input type="text" class="form-control" readonly id="esta2" name="">
                   		</div>
                   	</div>
		                
		                <div class="col-sm-2">
                  		<div class="form-group">
                    		<label>Municipio:</label>
			                	<input type="text" class="form-control" readonly id="municipio" name="">
                   		</div>
                   	</div>
                  	
                  	<div class="col-sm-4">
                  		<div class="form-group">
                    		<label>Localidad:</label>
	                    <input type="text"  class="form-control" readonly id="central2" name="">
                  		</div>
                  	</div>
                  	
                  	<div class="col-sm-2">
                  		<div class="form-group">
                    		<label>Tipo:</label>
	                    <input type="text" onblur="generar()" class="form-control" readonly id="tipo" name="">
                  		</div>
                  	</div>
		                		
		              </div>

                	<div class="row">
                  	<div class="col-sm-4">
                    	<div class="form-group">
                        <label>Equipo:</label>
	                      <select name="equipo_id" id="equipo" onchange="generar()" class="form-control select2bs4">
	                       	<option value="">Seleccione un equipo</option>
												</select>
                    	</div>
                    </div>


                    <div class="col-sm-4" id="div_tipo_falla">
                    	<div class="form-group">
                        <label>Fallas:</label>
	                      <select name="falla" id="tipo_falla" onchange="generar()" class="form-control select2bs4">
	                       	<option value="">Seleccione un falla</option>
												</select>
                    	</div>
                    </div>

                    <div class="col-sm-4" id="Correctivo" >
                    	<div class="form-group">
                        <label>Acción/Condición:</label>
	                        <select name="tipo_falla" onchange="generar()" id="fallas" class="form-control select2bs4">
                            <option value="">Seleccione una Acción</option>
													</select>
	                    </div>
                    </div>


                    <div class="col-sm-4" id="div_tipo_fluido" style="display:none;">
                    	<div class="form-group">
                        <label>Cantidad de litros:</label>
	                      <input type="text" onblur="generar()" id="tipo_fluido"  placeholder="LTRS" class="form-control" name="tipo_fluido">
                    	</div>
                    </div>

                    <div class="form-group">
	                   	<input type="hidden" id="accion1" class="form-control" >
	                    	<div  id="input_accion" style="display: none">
	                    		<input type="text" onblur="generar()" id="accion2" class="form-control" >
	                    	</div>
	                  </div>
		              </div>
			                  
	                <div class="row">	
	                	<div class="col-sm-4">
	                    <div class="form-group">
	                     	<label>Sala:</label>
                       	<select name="sala" onchange="generar()" id="box_ModeloRectificador" name="box_ModeloRectificador" class="form-control select2bs4">
													<option value="">Seleccione una Sala</option>
														@foreach($salas as $sala)
                            	<option value="{{$sala->nombre}}">{{$sala->nombre}}</option>
                            @endforeach
												</select>
	                    </div>
                    </div>
                    
                    <div class="col-sm-4">
                     	<div class="form-group">
                       	<label>Piso:</label>
		                    <select id="piso" onchange="generar()" name="box_ModeloRectificador" class="form-control select2bs4 ">
													<option value="">Seleccione un piso</option>
													@foreach($pisos as $piso)
		                     		<option value="{{$piso->nombre}}">{{$piso->nombre}}</option>
		                     	@endforeach
												</select>
	                   	</div>
                    </div>
		                      
                    <div class="col-sm-4">
	                   	<div class="form-group">
	                     	<label>Origen:</label>
		                   	<input type="text" onblur="generar()" id="personal" name="" placeholder="Reporta" class="form-control">
		                 	</div>
	                  </div>
	                </div>
			                
	                <div class="row">    
	                  <div class="col-sm-4">
	                    <div class="form-group">
	                     	<label>Teléfono:</label>
                       	<div class="input-group">
				                  <div class="input-group-prepend">
				                   	<span class="input-group-text"><i class="fas fa-phone"></i></span>
				                  </div>
				                  <input type="text" onblur="generar()" id="telefono" name="telefono" required  class="form-control" data-inputmask='"mask": "99999999999"' data-mask>
				               	</div>
						    			</div>
	                  </div>
	                  <div class="col-sm-4">
                      <div class="form-group">
                       	<label>Aula:</label>
	                     	<input type="text" onblur="generar()" id="aula" name="" placeholder="aula" class="form-control">
	                    </div>
                    </div>
                  </div>
		                 			               
                	<div class="row">
                    <div class="col-sm-12">
		                  <div class="form-group">
												<label for="comment">Información Adicional:</label>
												<textarea class="form-control"  onblur="generar()" name="info" rows="3" id="info"></textarea>			
											</div>
			              </div>
		              </div>

                	<div class="row">
	                	<div class="col-sm-4" style="display: none">
	                		<a onclick="generar()" class="btn btn-info">Generar comentario de apertura</a>
	                	</div>
                  </div>
                	
                	<div class="row">
                    <div class="col-sm-12">
	                    <div class="form-group">
												<label >Texto Generado:
												<button class="btn btn-success" data-clipboard-target="#comment">
											  	<i class="fa fa-clipboard"></i> Copiar
												</button>
												</label>
												<textarea  readonly class="form-control"  name="actividad" rows="3" id="comment"></textarea>			
											</div>
			              </div>
			            </div>
              	</div>
	            </div>
	
  
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('plugins/select2/js/select2.full.min.js')}}"></script>

<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- SweetAlert2 -->
<script src="{{asset('dist/js/clipboard.min.js')}}"></script>

  <script src="{{asset('plugins/sweetalert2/sweetalert2.min.js')}}"></script>
  <!-- Toastr -->
  <script src="{{asset('plugins/toastr/toastr.min.js')}}"></script>

<script src="{{asset('js/pages/dashboard.js')}}"></script>



<script type="text/javascript">
var clipboard = new Clipboard('.btn');
$('.select2').select2();
	var mnt;


function generar(){
	var rt;
	var fll = " -";
	var equipo1 = document.getElementById('checkbox1');
	var equipo2 = document.getElementById('checkbox2');
	var equipo3 = document.getElementById('checkbox3');
	var equipo4 = document.getElementById('checkbox4');
	
	var provedores = document.getElementById('provedores');
	var fallas = document.getElementById('fallas');
	var tipo_falla = document.getElementById('tipo_falla');
	var central = document.getElementById('central2');
	var estados = document.getElementById('esta2');
	var municipio = document.getElementById('municipio');
	var region = document.getElementById('region');
	var aula = document.getElementById('aula');
	var tipo = document.getElementById('tipo');
	var usuario1 = document.getElementById('usuario1');
	var usuario2 = document.getElementById('usuario2');
	var usuario3 = document.getElementById('usuario3');
	var tipo_fluido = document.getElementById('tipo_fluido');
	var ltrs='';
	var fallas_options;

	if (equipo1.checked){
		rt = equipo1.value
	}if (equipo2.checked){
		fll =tipo_falla.options[tipo_falla.selectedIndex].text
		rt = equipo2.value
	}if (equipo3.checked){
		rt = equipo3.value
	}
	if (equipo4.checked){
		rt = equipo4.value
	}
	var mantenimiento = document.getElementById('tipo_servicio');
	var mantenimiento2 = document.getElementById('tipo_servicio2');

	var personal = document.getElementById('personal');
	var tipo_actividad = document.getElementById('mantenimiento');
	
	var box_ModeloRectificador = document.getElementById('box_ModeloRectificador');

	var equipo = document.getElementById('equipo');
	var telefono = document.getElementById('telefono');
	var info = document.getElementById('info');
	var componente = document.getElementById('componente');

	var piso = document.getElementById('piso');
	var accion2 = document.getElementById('accion2');
	var accion1 = document.getElementById('accion1');

	
	if(fallas.options[fallas.selectedIndex].text == "OTROS(ESPECIFIQUE)" || fallas.options[fallas.selectedIndex].text == ""){
		accion1.value = "OTROS:";
		$('#input_accion').show();
	}else{
		$('#input_accion').hide()
		accion1.value = ""
		accion2.value = fallas.options[fallas.selectedIndex].text
	}

				
	if(fallas.options[fallas.selectedIndex].text == "MOTOR EN SERVICIO" || fallas.options[fallas.selectedIndex].text == "MANTENIMIENTO CORRECTIVO" || fallas.options[fallas.selectedIndex].text == "CORTE PROGRAMADO" || fallas.options[fallas.selectedIndex].text == "RED PUBLICA" ){
		
					$('#div_tipo_falla').show()
					fallas_options=tipo_falla.options[tipo_falla.selectedIndex].text;

	}
	

	if($('#div_tipo_falla').is(':visible')){

		fallas_options=tipo_falla.options[tipo_falla.selectedIndex].text+'_';
		
	}else{
		fallas_options=''

	}
	
	
	document.getElementById('comment').value= central2.value+'_'
	+tipo.value.toUpperCase()+'_'
	+equipo.options[equipo.selectedIndex].text+'_'
	+fallas_options
	+accion1.value+accion2.value.toUpperCase()+'_'
	+document.getElementById('tipo_fluido').value.toUpperCase()+'_'
	+box_ModeloRectificador.options[box_ModeloRectificador.selectedIndex].text+'_'
	+piso.options[piso.selectedIndex].text+'_'
	+aula.value.toUpperCase()+'_'
	+personal.value.toUpperCase()+'_'
	+telefono.value+'_'
	+info.value.toUpperCase()+'_'
	+mnt+'_'
	+estados.value+'_'
	+region.value+'_'
	+rt+'_'
	+provedores.value
	+municipio.value+'_'


}


		function actividad() {
			var rutinaria = document.getElementById('checkbox1');
			var atencion = document.getElementById('checkbox2');
			var gestion = document.getElementById('checkbox3');
			var no_rutinaria = document.getElementById('checkbox4');
			
			var actividad;

			if (rutinaria.checked){
				actividad= 1
				$('#div_tipo_falla').show()
			}tipo_falla
			if (atencion.checked){
				actividad =2
				$('#div_tipo_falla').show()
			}
			if (gestion.checked){
				actividad=3
				$('#div_tipo_falla').hide()
			}
			if (no_rutinaria.checked){
				actividad=4
				$('#div_tipo_falla').show()
			}
			
	 
		    var APP_URL = {!!json_encode(url('/'))!!}+'/';
		    $.get(APP_URL+'tipo_actividad/' + actividad, function (cidades) {
		        $('select[id=fallas]').empty();

		        if(cidades.length != 0){
		            $('select[id=fallas]').append('<option value=""> Selecciona un tipo accion </option>');
		            $.each(cidades, function (key, value) {
		                $('select[id=fallas]').append('<option value=' + value.tipo_fluido+ '>'+ value.nombre + '</option>');
		            });
		        }

		        });
		}

		function servicio() {
			var servicio1 = document.getElementById('servicio1');
			var servicio2 = document.getElementById('servicio2');
			
		
			var servicio;
			if (servicio1.checked){
				servicio= 1
				mnt="ENERGIA"
			}
			if (servicio2.checked){
				servicio =2
				mnt="CLIMATIZACION"
			}
		
		
		    
		    var APP_URL = {!!json_encode(url('/'))!!}+'/';
		    $.get(APP_URL+'tipo_servicio/' + servicio, function (cidades) {
		        $('select[id=equipo]').empty();

		        if(cidades.length != 0){
		            $('select[id=equipo]').append('<option value=""> Selecciona un tipo equipo </option>');
		            $.each(cidades, function (key, value) {
		                $('select[id=equipo]').append('<option value=' + value.id + '>'+ value.descripcion + '</option>');
		            });
		        }

		        });


		          $.get(APP_URL+'fallas/' + servicio, function (cidades) {
		        $('select[id=tipo_falla]').empty();

		        if(cidades.length != 0){
		            $('select[id=tipo_falla]').append('<option value=""> Selecciona un tipo de falla </option>');
		            $.each(cidades, function (key, value) {
		                $('select[id=tipo_falla]').append('<option value=' + value.id + '>'+ value.descripcion + '</option>');
		            });
		        }

		        });
		}

  $(document).ready(function(){

  	 $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
  	 $('#fallas').change(function(){
  	 		var val = $(this).val();
  	 		if (val==1){
				$('#div_tipo_falla').hide()
  	 			
  	 			$('#div_tipo_fluido').show();
  	 			
  	 			
  	 		}else{

  	 			$('#div_tipo_fluido').hide();
  	 			$('#div_tipo_falla').show()
  	 			document.getElementById('tipo_fluido').value = '';
  	 		}
  	 });


  	$('select[id=estados]').change(function () {
      //console.log("estoy aqui papa");
      var estados = $(this).val();
      
      var APP_URL = {!!json_encode(url('/'))!!}+'/';
      $.get(APP_URL+'central/' + estados, function (cidades) {
          $('select[id=centrales]').empty();

          if(cidades.length != 0){
              $('select[id=centrales]').append('<option value=""> Selecciona una Localidad </option>');
              $.each(cidades, function (key, value) {

                  $('select[id=centrales]').append('<option value="' +value.nombre+'-'+ value.estados.nombre+'-'+ value.municipio.descripcion+'-'+ value.estados.region.nombre+'-'+ value.tipo+'">'+ value.nombre +'-'+ value.tipo+ '</option>');
              });
          }

      });
    });



  	$('select[id=centrales]').change(function () {
    var central = $(this).val();
    var valores = central.split('-')
    //console.log('hola',valores)
  	
  	document.getElementById('tipo').value = valores[4]
  	document.getElementById('central2').value = valores[0]
  	document.getElementById('esta2').value = valores[1]
  	document.getElementById('municipio').value = valores[2]
  	document.getElementById('region').value =valores[3]
  	//console.log(cidades[0].tipo)
  	if (valores[3]==""){
  		$('#tipo').attr("readonly",false)
  	}else{
  		$('#tipo').attr("readonly",true)

  	}
  	generar();
  

    });
  });


</script>
</body>
</html>


