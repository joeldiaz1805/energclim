@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Registro de Solicitud y Seguimiento de Proyectos y Factibilidad 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Factibilidad</a></li>
              <li class="breadcrumb-item active">Planificacion</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
     
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
              
              <div class="card-body">
              	 <form method="POST" id="formulario_tickets" name="formulario_tickets"  action="{{ url('/tickets')}}">
                        {{ csrf_field() }}
                
				        <input type="hidden" readonly class="form-control" value="{{Auth::user()->id}}" required name="usuario_id" >
                     <div class="col-md-12">
			            <div class="card card-primary">
			              <div class="card-header">
			                <h3 class="card-title" id="titulo">Planificacion</h3>
			              </div>
			              <!-- /.card-header -->
			              <div class="card-body">

			              	<div class="row">
			              		<div class="col-sm-5">
				                	<label>Tipo de Actividad:</label>
				                  	<div class="col-sm-12">
				                      <!-- text input -->
				                      	<div class="form-group">
				                        	
				                        	<div class="form-check" required>
					                          <input class="form-check-input" onblur="generar()" id="checkbox1" type="radio" required value="Planificacion" name="tipo_mantenimiento">
					                          <label class="form-check-label">Planificaciòn</label><br>
					                          <input class="form-check-input" onblur="generar()" id="checkbox2" type="radio" required value="Proyecto"  name="tipo_mantenimiento">
					                          <label class="form-check-label">Proyecto</label><br>
					                          <input style="display: none;" class="form-check-input" onblur="generar()" id="checkbox3" type="radio" required value="Inspeccion"  name="tipo_mantenimiento">
					                          <label style="display: none;" class="form-check-label">Inspecciòn</label><br>
					                        </div>
					                    </div>
				                    </div>
				                 </div>
				                 <div class="col-sm-4">
				                	<label>Tipo de Servicio:</label>
				                  	<div class="col-sm-4">
				                      <!-- text input -->
				                      	<div class="form-group">
				                        	
				                        	<div class="form-check" required>
					                          <input class="form-check-input" onblur="generar()" type="radio" id="tipo_servicio" value="Ex" name="tipo_servicio">
					                          <label class="form-check-label">Ex</label><br>
					                          <input class="form-check-input" onblur="generar()" type="radio" id="tipo_servicio2" value="A/A" name="tipo_servicio">
					                          <label class="form-check-label">A/A</label>
					                          <p>
					                          <input class="form-check-input" onblur="generar()" type="radio" id="tipo_servicio3" value="Ex-A/A" name="tipo_servicio">
					                          <label class="form-check-label">Ex-A/A</label>
					                          	
					                          </p>

					                        </div>
					                    </div>
				                    </div>
				                 </div>
				                 
				                 		<!-- <div class="col-sm-4">
			                      	<div class="form-group">
			                        	<label>Buscar equipo Serial:</label>
				                        	<input type="text" class="form-control" required name="telefono" >
				                    		</div>
			                      	</div> 
			                    	-->

			              	</div>
							<div class="row">
			                  	<div class="col-sm-4">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Región:</label>
				                        	<select  name="central[region_id]" id="region" class="form-control">
				                        		<option value="">Seleccione una Region</option>
				                        		@foreach($regiones as $region)
					                        		<option value="{{$region->id}}">{{$region->nombre}}</option>
					                        	@endforeach
					                    	</select>
				                    	</div>
				                </div>
		                    	<div class="col-sm-4">
	                      <!-- text input -->
			                      <div class="form-group">
			                        <label>Estado:</label>
			                        <select name="central[estado_id]" id="estado" class="form-control">
				                       		<option value="">Seleccione un Estado</option>
			                    	</select>
			                      </div>
			                    </div>
		                    	<div class="col-sm-4">
	                    
			                      	<div class="form-group">
			                        	<label>Central:</label>
				                        	<select  name="central_id" id="central" class="form-control select2">
						                       		<option value="">Seleccione una Central</option>
					                    	</select>
				                    </div>
			                      </div>
			                </div>

			                <div class="row">

			                    <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Tipo de Equipo:</label>
				                        <select name="equipo_id" id="equipo" onblur="generar()" class="form-control">
				                        	<option value="">Seleccione un equipo</option>
			                            	
										</select>
				                      </div>
				                    </div>
			                	<div class="col-sm-4">
			                        <div class="form-group">
			                        	<label>Equipos:</label>
			                        	<select id="equipo_en_central" disabled name="box_ModeloRectificador" class="form-control select2bs4">
											<option value="">Seleccione el equipo</option>
										</select>
											
			                        </div>
			                    </div>

			                	 <div class="col-sm-4">
			                      <!-- text input -->
			                      <div class="form-group">
			                        <label>Componente de Equipo:</label>
			                        <select name="componente_id" id="componente" onblur="generar()" class="form-control">
			                            <option value=""></option>
									</select>
			                      </div>
			                    </div>
			                   </div>

			                    <div class="row">

			                    <div class="col-sm-4" id="Correctivo" style="display: none;">
			                      <!-- text input -->
			                      <div class="form-group">
			                        <label>Tipo de Falla:</label>
			                        <select disabled name="tipo_falla" onblur="generar()" id="fallas" class="form-control">
		                            	<option value="">Seleccione una Falla</option>
									</select>
			                      </div>
			                    </div>

			                    <div class="col-sm-4">
			                      <!-- text input -->
			                      <div class="form-group">
			                        <label>Tipo de Actividad:</label>
			                        <select name="tipo_actividad" onblur="generar()" id="mantenimiento" class="form-control">
		                            	<option value="">Seleccione una actividad</option>
									</select>
			                      </div>
			                    </div>

			                </div>
			                	
			                <div class="row">
			                	<div class="col-sm-4">
			                        <div class="form-group">
			                        	<label>Sala:</label>
			                        	<select name="sala" id="box_ModeloRectificador" name="box_ModeloRectificador" class="form-control">
											<option value=""></option>
											<option value="Transmisión">Transmisión</option>
											<option value="PCM">PCM</option>
											<option value="Conmutación">Conmutación</option>
											<option value="Datos">Datos</option>
											<option value="PSTN">PSTN</option>
											<option value="Oficina">Oficina</option>
											<option value="OAC">OAC</option>
											<option value="Centro de Datos">Centro de Datos</option>
											<option value="DSLAM">DSLAM</option>
										</select>
			                        </div>
			                    </div>

			                    
		                    	<div class="col-sm-4">
			                      	<div class="form-group">
			                        	<label>Piso:</label>
				                        <input type="text" class="form-control" id="piso" required name="piso" >
				                    </div>
		                      	</div>
			                    <div class="col-sm-4">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Reporta/Responsable</label>
				                        	<!--input type="text" class="form-control" required name="responsable_id" -->
				                        	<select  name="responsable_id" id="personal" class="form-control select2">
				                        		@foreach($responsables as $resp)
					                        		<option onblur="generar()" value="{{$resp->id}}">{{$resp->cargo}} {{$resp->cod_p00}} {{$resp->nombre}} {{$resp->apellido}}</option>
					                        	@endforeach
					                    	</select>
				                    </div>
			                    </div>
			                </div>

			                <div class="row">
			                    <div class="col-sm-4">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Teléfono Adicional:</label>
			                        		<div class="input-group">
								                    <div class="input-group-prepend">
								                      <span class="input-group-text"><i class="fas fa-phone"></i></span>
								                    </div>
								                    <input type="text" name="telefono" required  class="form-control" data-inputmask='"mask": "99999999999"' data-mask>
								                  </div>
								            </div>
			                      </div>
		                   
			                    <div class="col-sm-4">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Cargo:</label>
				                        	<input type="text" class="form-control" required name="cargo" >
				                    </div>
			                      </div>
			                	
			                    <div class="col-sm-12">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Correo:</label>
				                        	<input type="text" class="form-control" required name="correo" >
				                    </div>
			                      </div>



			                </div>
			                <div class="row">
			                    <div class="col-sm-12">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Correo Supervisor:</label>
				                        	<input type="text" class="form-control" required name="correo_supervisor" >
				                    </div>
			                      </div>
			                </div>
			                <div class="row">
			                	
			                 <div class="col-sm-4" style="display: none;">

			                 		<input type="hidden" name="status" value="En Proceso">
			                      	<div class="form-group">
			                        	<label>Estatus:</label>
				                        	<select  class="form-control">
				                        		<option></option> 
				                            	<option value="En Proceso">En Proceso</option>
					                        	<option  value="Resuelto">Resuelto</option>
					                        	<option  value="Asignado">Asignado</option>
					                        	<option  value="Cancelado">Cancelado</option>
					                        	<option  value="Pendiente por">Pendiente por</option>
					                        	<option  value="Cerrado">Cerrado</option>
					                    	</select>
				                    </div>
				                </div>
			             </div>
		                    
			                <div class="row">
			                    <div class="col-sm-4">
			                      	<div class="form-group">
			                        	<label>Fecha de Inicio de Actividad: </label>
				                      	<input type="date" onblur="generar()"  class="form-control" id="fecha_inicio" required name="fecha_inicio" value="" >
				                    </div>
			                    </div>
	                    		<div class="col-sm-4">
	                    			<div class="form-group"> 
	                    				<label>Hora de inicio de Actividad: </label>
				                   	 	<div class="input-group date" id="timepicker" data-target-input="nearest">
	   				                       	<input onblur="generar()" type="text" name="hora_inicio" id="hora_inicio" class="form-control datetimepicker-input" data-target="#timepicker">
						                      <div class="input-group-append" data-target="#timepicker" data-toggle="datetimepicker">
						                          <div class="input-group-text"><i class="far fa-clock"></i></div>
						                      </div>
					                       
					                   	</div>
				                   	</div>
	                    		</div>
		                      	<div class="col-sm-4">
		                      		<div class="form-group">
		                        		<label>Fecha de Fin:</label>
		                        		<input type="date" readonly="true" class="form-control" required name="fecha_fin_falla" >
			                    	</div>
		                      	</div>

				                    
			                </div>

			                <div class="row">
			                	<div class="col-sm-4">
			                		<a onclick="generar()" class="btn btn-info">Generar comentario de apertura</a>
			                	</div>
			                   	<div class="col-sm-8">
			                      	<div  class="form-group">
			                        	<label>Fecha de Creacion de ticket: </label>
			                        	
				                      	<span>{{$ldate}}</span>
				                    </div>
			                    </div>


			                  </div>

			                  <div class="row">
			                  	
			                    <div class="col-sm-12">
			                      <!-- text input -->
				                     <div class="form-group">
										<label for="comment">Observaciones:</label>
										<textarea readonly class="form-control"  name="actividad" rows="3" id="comment"></textarea>			
									</div>
			                    </div>

			                  </div>
								
			            	</div>
			            </div>
			        </div>
	                <div class="card-footer" align="right">
		                <button type="submit" class="btn btn-primary">Guardar</button>
		            </div>
                </form>
			</div>
        </div>
    </div>
	</div>
	</div>
			    
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

@endsection
@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_tickets") !!}
@endif



<script type="text/javascript">
	
	$(document).ready(function(){
        $("#checkbox1").click(function(){
	        $('#Correctivo').hide();
	       	$('#fallas').prop('disabled','disabled');

	       	var APP_URL = {!!json_encode(url('/'))!!}+'/';
			    $.get(APP_URL+'mantenimiento/'+1, function (cidades) {
			        $('select[id=mantenimiento]').empty();
	                    if(cidades.length != 0){
			              $('select[id=mantenimiento]').append('<option value=""> Selecciona una actividad </option>');
			              $.each(cidades, function (key, value) {
			                  $('select[id=mantenimiento]').append('<option value=' + value.id + '>'+ value.descripcion + '</option>');
			              });
			          }

			      });
   		 });
        $("#checkbox2").click(function(){
        	$('#fallas').prop('disabled', false);
	        $('#Correctivo').show();
   		 });
});
	$(document).ready(function(){
        $("#checkbox3").click(function(){
	        $('#Correctivo').hide();
	       	$('#fallas').prop('disabled','disabled');

	       	var APP_URL = {!!json_encode(url('/'))!!}+'/';
			    $.get(APP_URL+'mantenimiento/'+1, function (cidades) {
			        $('select[id=mantenimiento]').empty();
	                    if(cidades.length != 0){
			              $('select[id=mantenimiento]').append('<option value=""> Selecciona una actividad </option>');
			              $.each(cidades, function (key, value) {
			                  $('select[id=mantenimiento]').append('<option value=' + value.id + '>'+ value.descripcion + '</option>');
			              });
			          }

			      });
   		 });
        $("#checkbox2").click(function(){
        	$('#fallas').prop('disabled', false);
	        $('#Correctivo').show();
   		 });
});

function generar(){
	var rt;
	var mnt;
	var fll = " -";
	var equipo1 = document.getElementById('checkbox1');
	var equipo2 = document.getElementById('checkbox2');
	var equipo3 = document.getElementById('checkbox3');
	var fallas = document.getElementById('fallas');
	if (equipo1.checked){
		rt = equipo1.value
	}if (equipo2.checked){
		fll =' que se encuentra con falla: '+fallas.options[fallas.selectedIndex].text
		rt = equipo2.value
	}if (equipo3.checked){
		rt = equipo3.value
	}
	var mantenimiento = document.getElementById('tipo_servicio');
	var mantenimiento2 = document.getElementById('tipo_servicio2');
	var mantenimiento3 = document.getElementById('tipo_servicio3');

	if (mantenimiento.checked){
		mnt = mantenimiento.value
	}if (mantenimiento2.checked){
		mnt = mantenimiento2.value
	}
	if (mantenimiento3.checked){
		mnt = mantenimiento3.value;	
    $('select[id=mantenimiento]').empty();
    $('select[id=mantenimiento]').append('<option value="66"> VARIAS ACTIVIDADES</option>');

	}
	var personal = document.getElementById('personal');
	var tipo_actividad = document.getElementById('mantenimiento');
	
	var box_ModeloRectificador = document.getElementById('box_ModeloRectificador');

	var equipo = document.getElementById('equipo');
	var componente = document.getElementById('componente');
	var fecha_inicio = document.getElementById('fecha_inicio').value;
	var hora_inicio = document.getElementById('hora_inicio').value;
	var piso = document.getElementById('piso').value;
	
	document.getElementById('comment').value='Reporta :'
	+personal.options[personal.selectedIndex].text+' que realizara una actividad de tipo: '
	+rt+ ', de tipo: '
	+mnt+ ' a un equipo de: '
	+equipo.options[equipo.selectedIndex].text +' en sala :'
	+box_ModeloRectificador.options[box_ModeloRectificador.selectedIndex].text +' piso: '
	+piso+' al componente: '
	+componente.options[componente.selectedIndex].text
	+fll + ' y realizarà: '
	+tipo_actividad.options[tipo_actividad.selectedIndex].text+ '. Fecha de inicio de actividad '
	+fecha_inicio+' hora de la actividad '
	+hora_inicio



}




  	$(document).ready(function(){
        $("#tipo_servicio").click(function(){
    		
		    var tipo_servicio = 1;
		    
		    var APP_URL = {!!json_encode(url('/'))!!}+'/';
		    $.get(APP_URL+'tipo_servicio/' + tipo_servicio, function (cidades) {
		        $('select[id=equipo]').empty();

		        if(cidades.length != 0){
		            $('select[id=equipo]').append('<option value=""> Selecciona un tipo equipo </option>');
		            $.each(cidades, function (key, value) {
		                $('select[id=equipo]').append('<option value=' + value.id + '>'+ value.descripcion + '</option>');
		            });
		        }

		        });

		    });
          	
        $("#tipo_servicio2").click(function(){
        		 var tipo_servicio = 2;
		    
		    var APP_URL = {!!json_encode(url('/'))!!}+'/';
		    $.get(APP_URL+'tipo_servicio/' + tipo_servicio, function (cidades) {
		        $('select[id=equipo]').empty();

		        if(cidades.length != 0){
		            $('select[id=equipo]').append('<option value=""> Selecciona un tipo equipo </option>');
		            $.each(cidades, function (key, value) {
		                $('select[id=equipo]').append('<option value=' + value.id + '>'+ value.descripcion + '</option>');
		            });
		        }

		        });
         
   		 });

   		 $("#tipo_servicio3").click(function(){
        		 var tipo_servicio = 3;
		    
		    var APP_URL = {!!json_encode(url('/'))!!}+'/';
		    $.get(APP_URL+'tipo_servicio/' + tipo_servicio, function (cidades) {
		        $('select[id=equipo]').empty();

		        if(cidades.length != 0){
		            $('select[id=equipo]').append('<option value=""> Selecciona un tipo equipo </option>');
		            $.each(cidades, function (key, value) {
		                $('select[id=equipo]').append('<option value=' + value.id + '>'+ value.descripcion + '</option>');
		            });
		        }

		        });
         
   		 });



       /* $("#tipo_servicio3").click(function(){
        		 var tipo_servicio = 2;
		        $('select[id=equipo]').empty();
		        $('select[id=componente]').empty();

		        $('select[id=equipo]').append('<option value=""> VARIOS EQUIPOS</option>');
		        $('select[id=componente]').append('<option value=""> VARIOS COMPONENTES </option>');
   		 });*/
   		

});
  $(document).ready(function(){


  	$('select[id=region]').change(function () {
    	console.log('hola')
    var region_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'estado/' + region_id, function (cidades) {
        $('select[id=estado]').empty();

        if(cidades.length != 0){
            $('select[id=estado]').append('<option value=""> Selecciona un estado</option>');
            $.each(cidades, function (key, value) {
                $('select[id=estado]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });

    });


	$('select[id=estado]').change(function () {
    	//console.log('hola')
    var estado_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'central/' + estado_id, function (cidades) {
        $('select[id=central]').empty();

        if(cidades.length != 0){
            $('select[id=central]').append('<option value=""> Selecciona un central</option>');
            $.each(cidades, function (key, value) {
                $('select[id=central]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });

    });


    

    $('select[id=equipo]').change(function () {
      //console.log("estoy aqui papa");
      var equipo_id = $(this).val();
      
      var APP_URL = {!!json_encode(url('/'))!!}+'/';
      $.get(APP_URL+'componentes/' + equipo_id, function (cidades) {
          $('select[id=componente]').empty();

          if(cidades.length != 0){
              $('select[id=componente]').append('<option value=""> Selecciona un Componente </option>');
              $.each(cidades, function (key, value) {
                  $('select[id=componente]').append('<option value=' + value.id + '>'+ value.descripcion + '</option>');
              });
          }

      });
    });



    $('select[id=componente]').change(function () {
      var falla_id = $(this).val();
      var APP_URL = {!!json_encode(url('/'))!!}+'/';
      $.get(APP_URL+'fallas/' + falla_id, function (cidades) {
          $('select[id=fallas]').empty();

          if(cidades.length != 0){
              $('select[id=fallas]').append('<option value=""> Selecciona una Falla </option>');
              $.each(cidades, function (key, value) {
                  $('select[id=fallas]').append('<option value=' + value.id + '>'+ value.descripcion + '</option>');
              });
          }

      });
    });

    $('select[id=fallas]').change(function () {
      var rt;
      var falla_id = $(this).val();
      var equipo1 = document.getElementById('checkbox1');
	  var equipo2 = document.getElementById('checkbox2');
	  if (equipo1.checked){
		rt = 1
		}if (equipo2.checked){
			rt = 2
		}
      
      var APP_URL = {!!json_encode(url('/'))!!}+'/';
      $.get(APP_URL+'mantenimiento/' + falla_id+ '/'+rt, function (cidades) {
          $('select[id=mantenimiento]').empty();

          if(cidades.length != 0){
              $('select[id=mantenimiento]').append('<option value=""> Selecciona una actividad </option>');
              $.each(cidades, function (key, value) {
                  $('select[id=mantenimiento]').append('<option value=' + value.id + '>'+ value.descripcion + '</option>');
              });
          }

      });
    });











     $('select[id=equipo]').change(function () {
      var equipo_en_central = $(this).val();
      var central = document.getElementById('central').value;
      // var select =  $('select[id="equipo"] option:selected').text();
      // console.log(select);
      var APP_URL = {!!json_encode(url('/'))!!}+'/';
      $.get(APP_URL+'equipos_centrales/' + equipo_en_central, function (cidades) {
      	//console.log(cidades[0].motor);
          $('select[id=equipo_en_central]').empty();

          if(cidades.length != 0){
              $('select[id=equipo_en_central]').append('<option value=""> Selecciona un equipo </option>');

              
              	if (equipo_en_central == 1){
              		///////////////MOTOGENERADOR
	              	$.each(cidades, function (key, value) {
	                	  $('select[id=equipo_en_central]').append('<option value=' + value.id + '>'+ value.motor.marca +' '+ value.motor.serial + '</option>');
	              	});
              	}else if(equipo_en_central == 2){
              		///////// RECTIFICADORES
              		$.each(cidades, function (key, value) {
	                	  $('select[id=equipo_en_central]').append('<option value=' + value.id + '>'+ value.rectificador.marca +' '+ value.rectificador.serial + '</option>');
	              	});
              	}else if(equipo_en_central == 3){
              		$.each(cidades, function (key, value) {
	                	  $('select[id=equipo_en_central]').append('<option value=' + value.id + '>'+ value.marca +' '+ value.serial + '</option>');
	              	});
              	}else if(equipo_en_central == 7){
              		/*UPS BANCO DE BATERIA*/
              		$.each(cidades, function (key, value) {
	                	  $('select[id=equipo_en_central]').append('<option value=' + value.id + '>'+ value.ups.marca +' '+ value.ups.serial + '</option>');
	              	});
              	}



          }else{
          	$('select[id=equipo_en_central]').append('<option disabled value=""> No hay equipos registrados </option>');

          }

      });
    });


  });






$(document).ready(function () {

	
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection





