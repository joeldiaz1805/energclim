º@extends ('/layouts/index')

@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Bitacora de ticket
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Registro </a></li>
              <li class="breadcrumb-item active">ticket</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fa fa-cogs"></i>
                  ticket
                   
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
            
                <div class="row">
                  <div class="col-md-6">
                    <dl>
                      <dt>Número de ticket</dt>
                      <dd>{{$ticket[0]->tikect->numero}}</dd>
                      
                      <dt>Ubicaciòn de operaciòn</dt>
                      <dd>Region: {{$ticket[0]->tikect->central->estados->region->nombre}}</dd>
                       <dd>Estado: {{$ticket[0]->tikect->central->estados->nombre}}</dd>
                       <dd>Central: {{$ticket[0]->tikect->central->nombre}}</dd>
                      
                      <dt>Sala</dt>
                      <dd>{{$ticket[0]->tikect->sala}}</dd>

                      <dt>Piso</dt>
                      <dd>{{$ticket[0]->tikect->piso}}</dd>

                      <dt>Equipo</dt>
                      <dd>{{$ticket[0]->tikect->equipo->descripcion}}</dd>

                      <dt>Componente</dt>
                      <dd>{{$ticket[0]->tikect->componente->descripcion}}</dd>

                      <dt>Tipo de Equipo</dt>
                      <dd>{{$ticket[0]->tikect->tipo_servicio}}</dd>
                      
                      <dt>Falla(si correctivo)</dt>
                      @if($ticket[0]->tikect->falla)
                        <dd>{{$ticket[0]->tikect->falla->descripcion}}</dd>
                      @endif
                      <dt>Actividad a realizar</dt>
                      <dd>{{$ticket[0]->tikect->actividad->descripcion}}</dd>

                      <dt>Ticket por:</dt>
                      <dd>{{$ticket[0]->tikect->tipo_mantenimiento}}</dd>
                    </dl>
                      
                  </div>
                  <!-- /.card-body -->
            
                <!-- /.card -->
                
                  
                <div class="col-md-6">
                 
                      <!-- /.card-header -->
                      
                        <dl>
                          <dt>Responsable</dt>
                          <dd>{{$ticket[0]->tikect->responsable->nombre}} {{$ticket[0]->tikect->responsable->apellido}} </dd>

                          <dt>Telefono personal del responsable</dt>
                          <dd>{{$ticket[0]->tikect->responsable->num_personal}}</dd>
                         
                          <dt>Telefono oficina del responsable</dt>
                          <dd>{{$ticket[0]->tikect->responsable->num_oficina}}</dd>
                         
                          <dt>Telefono Adicional</dt>
                          <dd>{{$ticket[0]->tikect->telefono}}</dd>
                         
                          <dt>Cargo</dt>
                          <dd>{{$ticket[0]->tikect->responsable->cargo}}</dd>

                          <dt>Correo</dt>
                          <dd>{{$ticket[0]->tikect->correo}}</dd>

                          <dt>Correo Supervisor</dt>
                          <dd>{{$ticket[0]->tikect->correo_supervisor}}</dd>

                          <dt>Estatus</dt>
                          <dd>{{$ticket[0]->tikect->status}}</dd>

                          <dt>Fecha inicio</dt>
                          <dd>{{$ticket[0]->tikect->fecha_inicio}}</dd>

                          <dt>Hora inicio de actividad</dt>
                          <dd>{{$ticket[0]->tikect->hora_inicio}}</dd>

                          <dt>Fecha fin falla</dt>
                          <dd>{{$ticket[0]->tikect->fecha_fin_falla}}</dd>
                        </dl>
                    
                      <!-- /.card-body -->
                    </div>
                  </div>

                  @foreach($ticket as $comentario)
                  @php
                    $val = sizeof($ticket) -1;
                   
                  @endphp
                  <div class="row">
                    <div class="col-md-12">
                      
                       @if($ticket[0]->tikect->status == "En Proceso" )
                        <div class="callout callout-warning">
                        @else
                        <div @if($ticket[$val]->id == $comentario->id) class="callout callout-danger" @endif class="callout callout-success">
                        @endif
                        <div class="card-header">
                          <div class="card-title" align="right">
                            <h5><i class="fa fa-user"></i> {{$comentario->usuario->name}} {{$comentario->usuario->last_name}} / {{$comentario->created_at}}
                            </h5>
                          </div>
                        </div>
                        

                        <p><i class="fa fa-bullhorn"></i> {{$comentario->descripcion}}</p>
                        @if($comentario->multimedia)
                          <p><a href="{{url('download',$comentario->multimedia->archivo)}}" class="btn btn-success"><i class="fa fa-download"></i> Descargar</a></p>
                        @endif
                           
                       
                      </div>
                    </div>
                  </div>
                  @endforeach
                  @if($ticket[0]->tikect->status == "Cerrado")
                   <div class="row">
                    <div class="col-md-12">
                      
                      <div class="callout callout-info">
                      

                       
                        <div class="card-header">
                          <div class="card-title">
                            <h5>Documentos De Evidencia de Cierre - {{$comentario->created_at}}</h5>
                          </div>
                        </div>
                        <br>

                        <p><a href="{{url('download',$ticket[0]->tikect->multimedia->archivo)}}" class="btn btn-success"><i class="fa fa-download"></i> Descargar</a></p>
                        
                           
                       
                      </div>
                    </div>
                  </div>
                  @endif
                  @if($ticket[0]->tikect->status != "Cerrado")
                  <div class="row" id="documentacion">
                     <div class="col-md-12">
                        <div class="card-footer" align="right">
                            <button type="button" onclick="evidenciar()" class="btn btn-danger">Cerrar Ticket</button>
                            <button type="button" id="doc" onclick="documentar()" class="btn btn-primary">Documentar</button>
                        </div>
                    
                      </div>
                      <div class="col-md-12" id="formulario" style="display: none">
                        <form method="POST" enctype="multipart/form-data" id="formulario_tickets" name="formulario_tickets"  action="{{ url('/tickets/documentar')}}">
                            {{ csrf_field() }}
                          <input type="hidden" readonly class="form-control" value="{{Auth::user()->id}}" required name="usuario_id" >
                          <input type="hidden" readonly class="form-control" value="{{$ticket[0]->tikect->id}}" required name="ticket_id" >
                              <div class="form-group">
                                <label for="comment">Documentacion:</label>
                                <textarea class="form-control" name="descripcion" rows="3" id="comment"></textarea>
                              </div>
                              <div class="form-group" id="archivo2">
                                  <label for="comment">Evidencia al seguimiento:</label>
                                  <input  type="file" name="archivo2">
                              </div>
                              <div class="form-group" id="archivo" style="display: none;">
                                  <label for="comment">Evidencia:</label>
                                  <input type="file" name="archivo">
                              </div>

                               <div class="card-footer" align="right">
                                  <button type="submit" class="btn btn-primary">Guardar</button>
                              </div>
                        </form>
                      </div>
                  </div>
                  @endif
            </div>
        </div>
    </section>
</div>
@endsection
@section('vs')
  @if(isset($validator))
     {!! $validator->selector("#formulario_tickets") !!}
@endif 



  <script type="text/javascript">

    @if(Session::has('Documentado'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Ticket Documentado con Exito'
      })
      })
    });
  @endif
    function documentar() {
       
        var x = document.getElementById("formulario");
        if (x.style.display === "none") {
            x.style.display = "block";
            document.getElementById("doc").innerHTML="Ocultar"
        } else {
            x.style.display = "none";
            document.getElementById("doc").innerHTML="Documentar"
        }
}

function evidenciar(){
 
  var y = document.getElementById("archivo")
  var z = document.getElementById("archivo2")
        if ( y.style.display === "none") {
            y.style.display = "block";
            z.style.display = "none";
        }else{
          y.style.display = "none";
        }
    }



  </script>
@endsection