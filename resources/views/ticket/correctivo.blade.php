@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Registro de Tickets 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Mantenimiento</a></li>
              <li class="breadcrumb-item active">Correctivo</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
     
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
              
              <div class="card-body">
              	 <form method="POST" id="formulario_ups" name="formulario_ups"  action="{{ url('/tickets')}}">
                        {{ csrf_field() }}
                
                     <div class="col-md-12">
			            <div class="card card-primary">
			              <div class="card-header">
			                <h3 class="card-title">Mantenimiento Correctivo</h3>
			              </div>
			              <!-- /.card-header -->
			              <div class="card-body">

			              	<div class="row">
			              		<div class="col-sm-4">
				                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Numero Ticket:</label>
				                        	<input type="hidden" readonly class="form-control" required name="numero" >
				                    </div>
			                      </div>
			              	</div>




							<div class="row">
			                  	<div class="col-sm-4">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Región:</label>
				                        	<select  name="central[region_id]" class="form-control">
				                        		@foreach($regiones as $region)
					                        		<option value="{{$region->id}}">{{$region->nombre}}</option>
					                        	@endforeach
					                    	</select>
				                    	</div>
				                </div>
		                    	<div class="col-sm-4">
	                      <!-- text input -->
			                      <div class="form-group">
			                        <label>Estado:</label>
			                        <select name="central[estado_id]" class="form-control">
			                        	@foreach($estados as $estado)
				                       		<option value="{{$estado->id}}">{{$estado->nombre}}</option>
				                        @endforeach
			                    	</select>
			                      </div>
			                    </div>
		                    	<div class="col-sm-4">
	                    
			                      	<div class="form-group">
			                        	<label>Central:</label>
				                        	<select name="central[localidad_id]" class="form-control">
				                        		@foreach($localidades as $central)
						                       		<option value="{{$central->id}}">{{$central->nombre}}</option>
						                        @endforeach
					                    	</select>
				                    </div>
			                      </div>
			                </div>

			                <div class="row">
			                	<div class="col-sm-4">
			                        <div class="form-group">
			                        	<label>Sala:</label>
			                        	<select name="ubicacion[sala]" id="box_ModeloRectificador" name="box_ModeloRectificador" class="form-control">
											<option value=""></option>
											<option value="Transmisión">Transmisión</option>
											<option value="PCM">PCM</option>
											<option value="Conmutación">Conmutación</option>
											<option value="Datos">Datos</option>
											<option value="PSTN">PSTN</option>
											<option value="Oficina">Oficina</option>
											<option value="OAC">OAC</option>
											<option value="Centro de Datos">Centro de Datos</option>
											<option value="DSLAM">DSLAM</option>
										</select>
			                        </div>
			                    </div>
		                    	<div class="col-sm-4">
			                      	<div class="form-group">
			                        	<label>Piso:</label>
				                        <input type="text" class="form-control" required name="ubicacion[piso]" >
				                    </div>
		                      	</div>
			                </div>

			                <div class="row">
			                	
			                    <div class="col-sm-4">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Responsable:</label>
				                        	<select  name="central[region_id]" class="form-control">
				                        		@foreach($regiones as $region)
					                        		<option value="{{$region->id}}">{{$region->nombre}}</option>
					                        	@endforeach
					                    	</select>
				                    </div>
			                    </div>
			                    <div class="col-sm-4">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Teléfono:</label>
				                        	<input type="text" class="form-control" required name="cargo" >
				                    </div>
			                      </div>
		                   
			                    <div class="col-sm-4">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Cargo:</label>
				                        	<input type="text" class="form-control" required name="cargo" >
				                    </div>
			                      </div>
			                </div>
			                <div class="row">
			                    <div class="col-sm-12">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Correo:</label>
				                        	<input type="text" class="form-control" required name="cargo" >
				                    </div>
			                      </div>
			                </div>
			                <div class="row">
			                    <div class="col-sm-12">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	<label>Correo Supervisor:</label>
				                        	<input type="text" class="form-control" required name="cargo" >
				                    </div>
			                      </div>
			                </div>
			                <div class="row">
			                	<div class="col-sm-4">
			                	<label>Tipo de Servicio:</label>
			                  	<div class="col-sm-4">
			                      <!-- text input -->
			                      	<div class="form-group">
			                        	
			                        	<div class="form-check" required>
				                          <input class="form-check-input" type="radio" value="si" name="">
				                          <label class="form-check-label">Ex</label><br>
				                          <input class="form-check-input" type="radio" value="no" name="">
				                          <label class="form-check-label">A/A</label>
				                        </div>
				                    </div>
			                    </div>
			                 </div>
			             </div>
		                    
			                <div class="row">
			                    <div class="col-sm-4">
			                      	<div class="form-group">
			                        	<label>Fecha de Creación:</label>
				                      	<input type="date" class="form-control" required name="cargo" >
				                    </div>
			                    </div>
		                    	<div class="col-sm-4">
			                      	<div class="form-group">
			                        	<label>Estatus:</label>
				                        	<select name="ubicacion[estructura]" class="form-control">
				                        		<option></option>
				                            	<option value="Fija">Fija</option>
					                        	<option  value="Movil">Móvil</option>
					                        	<option  value="URL">URL</option>
					                    	</select>
				                    </div>
				                </div>
			                    <div class="col-sm-4">
			                      	<div class="form-group">
			                        	<label>Fecha de Fin:</label>
			                        	<input type="date" class="form-control" required name="cargo" >
				                    </div>
			                      </div>
			                </div>
			                <div class="row">
			                    <div class="col-sm-4">
			                      <!-- text input -->
			                      <div class="form-group">
			                        <label>Tipo de Equipo:</label>
			                        <select name="rect_bcobb[operatividad]" class="form-control">
		                            	@foreach($localidades as $central)
						                    <option value="{{$central->id}}">{{$central->nombre}}</option>
						                @endforeach
									</select>
			                      </div>
			                    </div>

			                    <div class="col-sm-4">
			                      <!-- text input -->
			                      <div class="form-group">
			                        <label>Componente de Equipo:</label>
			                        <select name="rect_bcobb[operatividad]" class="form-control">
		                            	<option value=""></option>
										<option value="Operativo">Operativo</option>
										<option value="Inoperativo">Inoperativo</option>
										<option value="Vandalizado">Vandalizado</option>
									</select>
			                      </div>
			                    </div>

			                    <div class="col-sm-4">
			                      <!-- text input -->
			                      <div class="form-group">
			                        <label>Tipo de Falla:</label>
			                        <select name="rect_bcobb[operatividad]" class="form-control">
		                            	<option value=""></option>
										<option value="Operativo">Operativo</option>
										<option value="Inoperativo">Inoperativo</option>
										<option value="Vandalizado">Vandalizado</option>
									</select>
			                      </div>
			                    </div>
			                  </div>

			                  <div class="row">
			                  	<div class="col-sm-6">
			                      <!-- text input -->
				                     <div class="form-group">
										<label for="comment">Actividad:</label>
										<textarea class="form-control" name="actividad" rows="3" id="comment"></textarea>			
									</div>
			                    </div>
			                    <div class="col-sm-6">
			                      <!-- text input -->
				                     <div class="form-group">
										<label for="comment">Observaciones:</label>
										<textarea class="form-control" name="actividad" rows="3" id="comment"></textarea>			
									</div>
			                    </div>

			                  </div>
								
			            	</div>
			            </div>
			        </div>
	                <div class="card-footer" align="right">
		                <button type="submit" class="btn btn-primary">Guardar</button>
		            </div>
                </form>
			</div>
        </div>
    </div>
	</div>
	</div>
			    
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

@endsection
@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_ups") !!}
@endif



<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection





