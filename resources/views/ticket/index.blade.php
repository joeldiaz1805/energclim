@extends ('/layouts/index')

@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Ticket 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Monitoreo</a></li>
              <li class="breadcrumb-item active">Ticket</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Registro de Ticket</h3>
              <!-- <div align="right">
                  <a href="/tickets/create" class="btn btn-success">Nuevo Ticket</a>
              </div> -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class=" display table table-bordered table-striped nowrap" style="width:100%">
                <thead>
                  <tr align="center">
                    <th>Acciones</th>
                    <th>Localidad</th>
                    <th>Tipo de Actividad</th>
                    <th>Equipo</th>
                    <th>Tipo</th>
                    <th>Creado por</th>
                    <th>Ùltimo Comentario</th>
                    <th>Status</th>
                    <th>Ticket Nº</th>
                    <th>Ultima Actualización</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($ticket as $tik)
                  <tr align="center" id="tdId_{{$tik->id}}">
                    <td>
                      <!-- <a href="/tickets/{{$tik->id}}/edit" class="btn btn-warning"><i class="far fa-edit"></i></a> -->
                      <a href="/tickets/{{$tik->id}}" class="btn btn-success"><i class="far fa-eye"></i></a>
                      <!-- <a onclick="deleteItem('{{$tik->id}}')" class="btn btn-danger"><i class="fa fa-times"></i></a> -->
                    </td>
                    <td>{{$tik->central->nombre}} </td>
                    <td>{{$tik->tipo_mantenimiento}} </td>
                    <td>{{$tik->equipo->descripcion}} </td>
                    <td>{{$tik->tipo_servicio}}</td>
                    <td>{{$tik->responsable->nombre}} {{$tik->responsable->apellido}} {{$tik->responsable->num_personal}}</td>
                    <td >{{$tik->comentarios->last()->descripcion}}</td>
                    <td>{{$tik->status}} </td>
                    <td>{{$tik->numero}}</td>
                    <td>{{$tik->updated_at}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
           </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('scriptsalerts')


<script>
   @if(Session::has('exito'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Ups Guardado con exito'
      })
      })
    });
  @endif
   @if(Session::has('actualizado'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Datos actualizados con exito'
      })
      })
    });
  @endif
  
</script>

  <script>
    function deleteItem(item){
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Desea eliminar este ups del intentario?',
      text: "Estas por quitar este equipo del Inventario!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, quitarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {


      if (result.value) {



            $.ajax({
                url: "/ups/"+item,
                type:"DELETE",
                   headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    

                    success:function(result, status, xhr){                    
                        
                        if (status == 'success') {

                          swalWithBootstrapButtons.fire(
                            'Quitado con Exito!',
                            'Se eliminó con exito',
                            'success'
                          )

                          $("#tdId_"+item).hide();
                         // txt.innerHTML = result ;



                        }
                    },
                    error:function(xhr, status, error){
                      

                      Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se eliminó el producto, vuelva a intentarlo mas tarde '+error,
                        footer: '<a href>Seguramente hubo un problema de conexión</a>'
                      })
                      

                    }
                });
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No se ha quitado el producto.',
          'error'
        )
      }
    })
  }
</script>


@endsection