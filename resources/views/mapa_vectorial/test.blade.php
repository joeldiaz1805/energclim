<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Country Map Example</title>
    <script src="{{asset('js/vector/mapdata.js')}}"></script>
    <script src="{{asset('js/vector/countrymap.js')}}"></script>
    <script type="text/javascript">
      
      function region(id){
        
         window.open(id.stringify(), "_self");  
      }

    </script>
 
  </head>
  <body>
  <h1>HTML5/Javascript Country Map</h1>
    <div id="map" style="width:120%;"></div>
    
    
    
</html>
      
    

