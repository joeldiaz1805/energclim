<style type="text/css">
  table{
    border-collapse: collapse;
  }

td, th {
  border: black 1px solid;
}
</style>
<table style="width :100%">
  <tbody>
   
    <tr align="center" >
      <td rowspan="4" colspan="3"> <img style="width:100px;height:50px;" src="{{asset('img/cantv_planilla.png')}}"></td>
      <td rowspan="4" colspan="3"> <h7> PLANILLA MANTENIMIENTO UPS </h7></td>
      <td>P4 Nº INC{{$data->ticket_num}}</td>
      <td colspan="7">Realizado por: <h7> {{$data->responsable->nombre.' '.$data->responsable->apellido}} </h7></td>
    </tr>
    <tr>
      <td rowspan="3">Supervision: <br>  {{$data->supervisor->nombre.' '.$data->supervisor->apellido}} </td>
      <td colspan="7" rowspan="1" align="center">Fechas:</td>
    </tr>

    <tr>
      <td colspan="3" align="center">Emision</td>
      <td colspan="4" align="center">Ejecusion</td>
    </tr>
    
    <tr>
      <td colspan="3">{{$data->fecha_emision}}</td>
      <td colspan="4">{{$data->fecha_ejecucion}}</td>
    </tr>
    
    <tr>
      <td colspan="4">Instalacion: <h7> {{$data->localidad->nombre.' '. $data->localidad->tipo}} </h7> </td>
      <td colspan="3">Sala : <h7> {{$data->sala}} </h7></td>
      <td colspan="7">Sistema UPS <h7>  </h7>	</td>
    </tr>
   
   	<tr>
     	<td  colspan="2" style="font-size:13px">TIPO ACTIVIDAD</td>
     
     	<td  colspan="5" style="font-size:14px" align="center">SISTEMA ININTERRUMPIDO DE POTENCIA UPS: <h7> </h7></td>
     	<td  colspan="2" style="font-size:7px; word-wrap: break-word;">SUPERVICION  </td>
     	<td  colspan="5" style="font-size:10px">OBSERVACION:  </td>
   </tr>

   <tr>
     <td style="font-size:12px">T</td>
     <td style="font-size:12px">C</td>

     <td  colspan="2">UBIC: </td>
     <td  colspan="3">CODIG</td>
     <td  colspan="1">B </td>
     <td  colspan="1">M </td>
     <td colspan="1"></td>
     <td  colspan="4" style="font-size: 10px;">cosec (persona de contacto)</td>
   </tr>


   <tr>
	   	<td ></td>
	   	<td></td>
	   	<td colspan="5">1.- Verificación de las alarmas locales y remotas, antes y después de ejecuatr el mantenimiento.</td>
	   	<td></td>
	   	<td></td>
	   	<td>b_m_</td>
	   	<td valign="top" align="left" colspan="4"></td>
   </tr>


    <tr>
	   	<td ></td>
	   	<td></td>
	   	<td colspan="5">2.- Limpieza exterior del gabinete.</td>
	   	<td></td>
	   	<td></td>
	   	<td>b_m_</td>
	   	<td valign="top" align="left" colspan="4"></td>
   </tr>
   <tr>
	   	<td ></td>
	   	<td></td>
	   	<td colspan="5">3.-Limpieza de las rendijas de ventilación y sustitución del filtro de aire de ser necesario.</td>
	   	<td></td>
	   	<td></td>
	   	<td>b_m_</td>
	   	<td valign="top" align="left" colspan="4"></td>
   </tr>
   <tr>
	   	<td ></td>
	   	<td></td>
	   	<td colspan="5">4.- Limpieza externa del gabinete de bypass si aplica.</td>
	   	<td></td>
	   	<td></td>
	   	<td>b_m_</td>
	   	<td valign="top" align="left" colspan="4"></td>
   </tr>
    <tr>
	   	<td ></td>
	   	<td></td>
	   	<td colspan="5">5.- Revisión de las condiciones de las tarjetas, fusibles, piezas del rectificación e inversión y bypass.</td>
	   	<td></td>
	   	<td></td>
	   	<td>b_m_</td>
	   	<td valign="top" align="left" colspan="4"></td>
   </tr>
    <tr>
	   	<td ></td>
	   	<td></td>
	   	<td colspan="5">7.- Realización de inspección general del cuarto de baterías incluyendo condiciones de temperatura y ventilación. Solo para baterías abiertas.</td>
	   	<td></td>
	   	<td></td>
	   	<td>b_m_</td>
	   	<td valign="top" align="left" colspan="4"></td>
   </tr>
    <tr>
	   	<td ></td>
	   	<td></td>
	   	<td colspan="5">8.- Ejecución del mantenimiento de baterías, aplicando guia de usuario correspondiente, según el tipo de baterías</td>
	   	<td></td>
	   	<td></td>
	   	<td>b_m_</td>
	   	<td valign="top" align="left" colspan="4"></td>
   </tr>
    <tr>
	   	<td ></td>
	   	<td></td>
	   	<td colspan="5">9. Ejecución del descargue histórico de operación del equipo </td>
	   	<td></td>
	   	<td></td>
	   	<td>b_m_</td>
	   	<td valign="top" align="left" colspan="4"></td>
   </tr>
    <tr>
	   	<td ></td>
	   	<td></td>
	   	<td colspan="5">10.- Verificación de sincronización con la red del UPS.</td>
	   	<td></td>
	   	<td></td>
	   	<td>b_m_</td>
	   	<td valign="top" align="left" colspan="4"></td>
   </tr>
    <tr>
	   	<td ></td>
	   	<td></td>
	   	<td colspan="5">11.- Verificación del correcto funcionamiento de la pantalla, comandos y leds.</td>
	   	<td></td>
	   	<td></td>
	   	<td>b_m_</td>
	   	<td valign="top" align="left" colspan="4"></td>
   </tr>
    <tr>
	   	<td ></td>
	   	<td></td>
	   	<td colspan="5">12.-Realización de ajuste de  hora y fecha del equipo de ser necesario.</td>
	   	<td></td>
	   	<td></td>
	   	<td>b_m_</td>
	   	<td valign="top" align="left" colspan="4"></td>
   </tr>
    <tr>
	   	<td ></td>
	   	<td></td>
	   	<td colspan="5">13.-Verificación por pantalla de la medición de voltaje de baterías si aplica.</td>
	   	<td></td>
	   	<td></td>
	   	<td>b_m_</td>
	   	<td valign="top" align="left" colspan="4"></td>
   </tr>
    <tr>
	   	<td ></td>
	   	<td></td>
	   	<td colspan="5">14. Medición y registro de los parámetros eléctricos en la entrada y la salida del equipo, tales como corrientes, voltajes, potencias y factor de potencia.</td>
	   	<td></td>
	   	<td></td>
	   	<td>b_m_</td>
	   	<td valign="top" align="left" colspan="4"></td>
   </tr>
    <tr>
	   	<td ></td>
	   	<td></td>
	   	<td colspan="5">15.-Realización de ajuste del  torque en los tornillos que integran el (los) banco (s) de baterías, si aplican.</td>
	   	<td></td>
	   	<td></td>
	   	<td>b_m_</td>
	   	<td valign="top" align="left" colspan="4"></td>
   </tr>
    <tr>
   		<td colspan="14"></td>
    </tr>
    <tr>
   		<td colspan="14" style="background-color: gray;" align="center"> <strong> DATOS UPS </strong> </td>
    </tr>
    <tr>
	   	<td colspan="3" style="font-size:13px;font-weight: 100;">MARCA UPS:</td>
	   	<td colspan="3"></td>
	   	<td colspan="1"style="font-size:13px;font-weight: 100;"> VAC ENTRADA UPS:</td>
	   	<td colspan="7"></td>
    </tr>
    <tr>
	   	<td colspan="3"style="font-size:13px;font-weight: 100;">MODELO UPS:</td>
	   	<td colspan="3"></td>
	   	<td colspan="1"style="font-size:13px;font-weight: 100;"> VAC SALIDA UPS:</td>
	   	<td colspan="7"></td>
    </tr>
    <tr>
	   	<td colspan="3"style="font-size:13px;font-weight: 100;">S/N UPS:</td>
	   	<td colspan="3"></td>
	   	<td colspan="1"style="font-size:13px;font-weight: 100;"> % CARGA UPS:</td>
	   	<td colspan="7"></td>
	 <tr>
	   	<td colspan="3" style="font-size:13px;font-weight: 100;">Nº INVENTARIO UPS:</td>
	   	<td colspan="3"></td>
	   	<td colspan="1"style="font-size:13px;font-weight: 100;"> ESTATUS BATERIA UPS:</td>
	   	<td colspan="7"></td>
   </tr>

   <tr>
   	<td colspan="14" align="left">OBSERVACIONES <br><br><br><br> </td>
   </tr>
  

  </tbody>
</table>

