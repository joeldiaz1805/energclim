<style type="text/css">
  table{
    border-collapse: collapse;
  }

td, th {
  border: black 1px solid;
}
</style>
<table style="width :100%">
  <tbody>
   
    <tr align="center" >
      <td rowspan="4"> <img style="width:50px;height:30px;" src="{{asset('img/cantv_planilla.png')}}"></td>
      <td rowspan="4" colspan="5"> <h7> PLANILLA MANTENIMIENTO BANCO DE BATERIAS </h7></td>
      <td>P4 Nº INC{{$data->ticket_num}}</td>
      <td colspan="3">Realizado por: <h7> {{$data->responsable->nombre.' '.$data->responsable->apellido}} </h7></td>
    </tr>
    <tr>
      <td rowspan="3">Supervision:<h7>  {{$data->supervisor->nombre.' '.$data->supervisor->apellido}} </h7></td>
      <td colspan="3" align="center">Fechas:</td>
    </tr>

    <tr>
      <td colspan="2" align="center">Emision</td>
      <td  align="center">Ejecusion</td>
    </tr>
    
    <tr>
      <td colspan="2">{{$data->fecha_emision}}</td>
      <td>{{$data->fecha_ejecucion}}</td>
    </tr>
    
    <tr>
      <td colspan="4">Instalacion: <h7> {{$data->localidad->nombre.' '. $data->localidad->tipo}} </h7> </td>
      <td colspan="2">Sala : <h7> {{$data->sala}} </h7></td>
      <td colspan="4">Sistema Dc <h7> {{$data->sistema_dc}} </h7><</td>
    </tr>
   <tr >
     <td rowspan="2">Bco 1</td>
     <td rowspan="2">Marca: <h7> {{$data->planilla_bb[0]->marca ?? N/A}} </h7></td>
     <td rowspan="2">Modelo: <h7> {{$data->planilla_bb[0]->modelo ?? N/A}} </h7></td>
     <td rowspan="2">Tipo</td>
     <td rowspan="1" colspan="1">A : @if($data->planilla_bb[0]->tipo == 1) <h7>X</h7> @endif</td>
     <td rowspan="2" colspan="3">Cap.(AC): <h7> {{$data->planilla_bb[0]->cap_ah ?? N/A}}</td>
     <td rowspan="2">#Celdas: <h7> {{$data->planilla_bb[0]->celdas ?? N/A}}</td>
     <td rowspan="2">Fecha Inst: <h7> {{$data->planilla_bb[0]->fecha_instalacion ?? N/A}}</td>
   </tr>
  <tr>
   <td colspan="1" >S : @if($data->planilla_bb[0]->tipo == 2) <h7>X</h7> @endif</td>
   
  </tr>

   <tr >
     <td rowspan="2">Bco 2</td>
     <td rowspan="2">Marca<h7> {{$data->planilla_bb[1]->marca ?? N/A}}</td>
     <td rowspan="2">Modelo<h7> {{$data->planilla_bb[1]->modelo ?? N/A}}</td>
     <td rowspan="2">Tipo</td>
     <td rowspan="1" colspan="1">A: @if($data->planilla_bb[1]->tipo == 1) <h7>X</h7> @endif</td>
     


     <td rowspan="2" colspan="3">Cap.(AC)<h7> {{$data->planilla_bb[1]->cap_ah ?? N/A}}</td>
     <td rowspan="2">#Celdas<h7> {{$data->planilla_bb[1]->celdas ?? N/A}}</td>
     <td rowspan="2">Fecha Inst<h7> {{$data->planilla_bb[1]->fecha_instalacion ?? N/A}}</td>
   </tr>
  <tr>
   <td colspan="1" >S: @if($data->planilla_bb[1]->tipo == 2) <h7>X</h7> @endif</td>
   
  </tr>
  <tr>
    <td rowspan="3" colspan="1">Banco 1</td>
    <td colspan="4">V(Total) Flot : {{$data->planilla_bb[0]->v_total ?? 'N/A'}}</td>
    <td rowspan="3" colspan="1">Banco 2</td>
    <td colspan="4">V(Total) Flot {{$data->planilla_bb[1]->v_total ?? 'N/A'}}</td>
  </tr>
  <tr>
    <td colspan="4">V(Total) Carga: {{$data->planilla_bb[0]->v_carga ?? 'N/A'}}</td>
    <td colspan ="4" >V(Total) Carga: {{$data->planilla_bb[1]->v_carga ?? 'N/A'}}</td>

  </tr>
  <tr>
    <td colspan="4">Nº Inv Ex: {{$data->planilla_bb[0]->num_inv ?? 'N/A'}}</td>
    <td colspan="4">Nº Inv Ex : {{$data->planilla_bb[1]->num_inv ?? 'N/A'}}</td>
  </tr>
  <tr>
    <td style="background-color: lightgray;">Nº</td>
    <td style="background-color: lightgray;">Densidad</td>
    <td style="background-color: lightgray;">Temp</td>
    <td style="background-color: lightgray;">Tension</td>
    <td style="background-color: lightgray;">observaciones</td>

    <td style="background-color: lightgray;">Nº</td>
    <td style="background-color: lightgray;">Densidad</td>
    <td style="background-color: lightgray;">Temp</td>
    <td style="background-color: lightgray;">Tension</td>
    <td style="background-color: lightgray;">observaciones</td>
  </tr>
  <tr>
    <td style="background-color: lightgray;">1</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[0]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[0]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[0]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[0]->observaciones ?? 'S/V'}}</td>
    <td style="background-color: lightgray;">1</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[0]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[0]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[0]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[0]->observaciones ?? 'S/V'}}</td>
  </tr>

  <tr>
    <td style="background-color: lightgray;">2</td>
     <td>{{$data->planilla_bb[0]->celdas_bb[1]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[1]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[1]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[1]->observaciones ?? 'S/V'}}</td>
    <td style="background-color: lightgray;">2</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[1]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[1]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[1]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[1]->observaciones ?? 'S/V'}}</td>
  </tr>
  <tr>
    <td style="background-color: lightgray;">3</td>
     <td>{{$data->planilla_bb[0]->celdas_bb[2]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[2]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[2]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[2]->observaciones ?? 'S/V'}}</td>
    <td style="background-color: lightgray;">3</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[2]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[2]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[2]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[2]->observaciones ?? 'S/V'}}</td>
  </tr>
  <tr>
    <td style="background-color: lightgray;">4</td>
     <td>{{$data->planilla_bb[0]->celdas_bb[3]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[3]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[3]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[3]->observaciones ?? 'S/V'}}</td>
    <td style="background-color: lightgray;">4</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[3]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[3]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[3]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[3]->observaciones ?? 'S/V'}}</td>
  </tr>
  <tr>
    <td style="background-color: lightgray;">5</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[4]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[4]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[4]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[4]->observaciones ?? 'S/V'}}</td>
    <td style="background-color: lightgray;">5</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[4]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[4]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[4]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[4]->observaciones ?? 'S/V'}}</td>
  </tr>
  <tr>
    <td style="background-color: lightgray;">6</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[5]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[5]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[5]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[5]->observaciones ?? 'S/V'}}</td>
    <td style="background-color: lightgray;">6</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[5]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[5]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[5]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[5]->observaciones ?? 'S/V'}}</td>
  </tr>
  <tr>
    <td style="background-color: lightgray;">7</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[6]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[6]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[6]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[6]->observaciones ?? 'S/V'}}</td>
    <td style="background-color: lightgray;">7</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[6]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[6]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[6]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[6]->observaciones ?? 'S/V'}}</td>
  </tr>
  <tr>
    <td style="background-color: lightgray;">8</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[7]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[7]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[7]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[7]->observaciones ?? 'S/V'}}</td>
    <td style="background-color: lightgray;">8</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[7]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[7]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[7]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[7]->observaciones ?? 'S/V'}}</td>
  </tr>
  <tr>
    <td style="background-color: lightgray;">9</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[8]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[8]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[8]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[8]->observaciones ?? 'S/V'}}</td>
    <td style="background-color: lightgray;">9</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[8]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[8]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[8]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[8]->observaciones ?? 'S/V'}}</td>
  </tr>
  <tr>
    <td style="background-color: lightgray;">10</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[9]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[9]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[9]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[9]->observaciones ?? 'S/V'}}</td>
    <td style="background-color: lightgray;">10</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[9]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[9]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[9]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[9]->observaciones ?? 'S/V'}}</td>
  </tr>
  <tr>
    <td style="background-color: lightgray;">11</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[10]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[10]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[10]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[10]->observaciones ?? 'S/V'}}</td>
    <td style="background-color: lightgray;">11</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[10]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[10]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[10]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[10]->observaciones ?? 'S/V'}}</td>
  </tr>
  <tr>
    <td style="background-color: lightgray;">12</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[11]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[11]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[11]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[11]->observaciones ?? 'S/V'}}</td>
    <td style="background-color: lightgray;">12</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[11]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[11]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[11]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[11]->observaciones ?? 'S/V'}}</td>
  </tr>
  <tr>
    <td style="background-color: lightgray;">13</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[12]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[12]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[12]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[12]->observaciones ?? 'S/V'}}</td>
    <td style="background-color: lightgray;">13</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[12]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[12]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[12]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[12]->observaciones ?? 'S/V'}}</td>
  </tr>
  <tr>
    <td style="background-color: lightgray;">14</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[13]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[13]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[13]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[13]->observaciones ?? 'S/V'}}</td>
    <td style="background-color: lightgray;">14</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[13]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[13]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[13]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[13]->observaciones ?? 'S/V'}}</td>
  </tr>
  <tr>
    <td style="background-color: lightgray;">15</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[14]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[14]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[14]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[14]->observaciones ?? 'S/V'}}</td>
    <td style="background-color: lightgray;">15</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[14]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[14]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[14]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[14]->observaciones ?? 'S/V'}}</td>
  </tr>
  <tr>
    <td style="background-color: lightgray;">16</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[15]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[15]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[15]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[15]->observaciones ?? 'S/V'}}</td>
    <td style="background-color: lightgray;">16</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[15]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[15]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[15]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[15]->observaciones ?? 'S/V'}}</td>
  </tr>
  <tr>
    <td style="background-color: lightgray;">17</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[16]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[16]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[16]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[16]->observaciones ?? 'S/V'}}</td>
    <td style="background-color: lightgray;">17</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[16]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[16]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[16]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[16]->observaciones ?? 'S/V'}}</td>
  </tr>
  <tr>
    <td style="background-color: lightgray;">18</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[17]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[17]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[17]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[17]->observaciones ?? 'S/V'}}</td>
    <td style="background-color: lightgray;">18</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[17]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[17]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[17]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[17]->observaciones ?? 'S/V'}}</td>
  </tr>
  <tr>
    <td style="background-color: lightgray;">19</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[18]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[18]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[18]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[18]->observaciones ?? 'S/V'}}</td>
    <td style="background-color: lightgray;">19</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[18]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[18]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[18]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[18]->observaciones ?? 'S/V'}}</td>
  </tr>
  <tr>
    <td style="background-color: lightgray;">20</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[19]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[19]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[19]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[19]->observaciones ?? 'S/V'}}</td>
    <td style="background-color: lightgray;">20</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[19]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[19]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[19]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[19]->observaciones ?? 'S/V'}}</td>
  </tr>
  <tr>
    <td style="background-color: lightgray;">21</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[20]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[20]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[20]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[20]->observaciones ?? 'S/V'}}</td>
    <td style="background-color: lightgray;">21</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[20]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[20]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[20]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[20]->observaciones ?? 'S/V'}}</td>
  </tr>
  <tr>
    <td style="background-color: lightgray;">22</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[21]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[21]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[21]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[21]->observaciones ?? 'S/V'}}</td>
    <td style="background-color: lightgray;">22</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[21]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[21]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[21]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[21]->observaciones ?? 'S/V'}}</td>
  </tr>
  <tr>
    <td style="background-color: lightgray;" >23</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[22]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[22]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[22]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[22]->observaciones ?? 'S/V'}}</td>
    <td  style="background-color: lightgray;" >23</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[22]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[22]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[22]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[22]->observaciones ?? 'S/V'}}</td>
  </tr>
  <tr>
    <td style="background-color: lightgray;">24</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[23]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[23]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[23]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[23]->observaciones ?? 'S/V'}}</td>
    <td style="background-color: lightgray;">24</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[23]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[23]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[23]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[23]->observaciones ?? 'S/V'}}</td>
  </tr>
  <tr>
    <td style="background-color: lightgray;">25</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[24]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[24]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[24]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[24]->observaciones ?? 'S/V'}}</td>
    <td style="background-color: lightgray;">25</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[24]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[24]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[24]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[24]->observaciones ?? 'S/V'}}</td>
  </tr>
  <tr>
    <td style="background-color: lightgray;">26</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[25]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[25]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[25]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[0]->celdas_bb[25]->observaciones ?? 'S/V'}}</td>
    <td style="background-color: lightgray;">26</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[25]->densidad ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[25]->temperatura ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[25]->tension ?? 'S/V'}}</td>
    <td>{{$data->planilla_bb[1]->celdas_bb[25]->observaciones ?? 'S/V'}}</td>
  </tr>
 <tr>
    <td colspan="10" align="left">Observaciones
      <br>
      <br>
      <br>


    </td>
  </tr>
  

  </tbody>
</table>

