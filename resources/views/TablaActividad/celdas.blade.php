@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Banco de Bateria
              <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Rectificador','Un Rectificador es un dispositivo electrónico que permite convertir la corriente alterna en corriente continua.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a> 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inventario</a></li>
              <li class="breadcrumb-item active">Banco de Bateria</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
      <div class="row ">
	    <div class="col s12 m12 l12 ">
	      @if ($errors->any())
	        <div class="alerta-errores">
	          <ul>
	            @foreach ($errors->all() as $error)
	              <li>{{ $error }}</li>
	            @endforeach
	          </ul>
	        </div>
	      @endif
	    </div>
	  </div>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
	      <div class="row">
         	<div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
            	<div class="card-body">
	          		<form method="POST" id="formulario_rect" name="formulario_rect"  action="{{url('/tablero_actividad/celdas')}}">
	                {{ csrf_field() }}
	                <input type="hidden" name="usuario_id" value="{{Auth::user()->id}}">
	                <input type="hidden" name="planilla_bb_id" value="{{$tablero_id}}">
					        <div class="col-md-12">
			            	<div class="card card-success">
					            <div class="card-header">
				              <ul class="nav nav-tabs" id="custom-content-above-tab" role="tablist">
												<li class="nav-item">
													<a class="nav-link active " id="custom-content-above-home-tab" data-toggle="pill" href="#custom-content-above-home" role="tab" aria-controls="custom-content-above-home" aria-selected="true">Banco B Nº 1</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" id="custom-content-above-profile-tab" data-toggle="pill" href="#custom-content-above-profile" role="tab" aria-controls="custom-content-above-profile" aria-selected="false">Banco B Nº 2</a>
												</li>
									
											</ul>
				  		          </div>
						            <div class="card-body">
													<div class="row">
														<div class="tab-content" id="custom-content-above-tabContent">
															<div class="tab-pane fade show active" id="custom-content-above-home" role="tabpanel" aria-labelledby="custom-content-above-home-tab">
																<br>

                 									<div align="right" id="boton">
						                          <a href="javascript:void(0);" id="add_button" class="btn btn-warning" title="Add field"><i class="fas fa-plus"></i></a>
            					
            					            </div>

							                	<div class="card-header">
									                <h3 class="card-title">Celda : 1</h3>
									            </div><br>
							                      
																<div class="row">
								                  <div class="col-sm-3">
								                   	<div class="form-group">
								                     	<label>Densidad:</label>
								                       	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','densidad Batería','Caracteres y números que lo identifican Ej. BA202011112525', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
									                      <input type="text" name="bco_baterias[celda1][densidad]"  class="form-control" placeholder="Introduzca densidad">
									                  </div>
								                  </div>
																	
																	<div class="col-sm-3">
								                   	<div class="form-group">
								                     	<label>Temperatura:</label><br>
								                     	<input type="text" name="bco_baterias[celda1][temperatura]"  class="form-control" placeholder="Introduzca densidad">
									                  </div>
								                  </div>
								             
							                    <div class="col-sm-3">
								                    <div class="form-group">
								                     	<label>Tension</label>
								                    	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Capacidad KVA','kilovoltiamperio. Solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                     	<input type="text" name = "bco_baterias[celda1][tension]" class="form-control" placeholder="Introduzca Capacidad Amp/AmpH">
								                    </div>
								                  </div>

									                <div class="col-sm-3">
						                      	<div class="form-group">
							                        <label>Observaciones</label>
							                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cantidad de Elementos Criticos','Solo números sin letras. Ej. 1, 10, 20, etc.', 150 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                        <input type="text" name = "bco_baterias[celda1][observaciones]" class="form-control" placeholder="Introduzca Numero de observaciones Dañadas">
						                      	</div>
							                    </div>
																</div>

																<div id="banbo_bateria"></div>

																


															</div>
															
															<div class="tab-pane fade" id="custom-content-above-profile" role="tabpanel" aria-labelledby="custom-content-above-profile-tab">
																	<br>

                 									<div align="right" id="boton">
						                          <a href="javascript:void(0);" id="add_button1" class="btn btn-warning" title="Add field"><i class="fas fa-plus"></i></a>
            					
            					            </div>

																<div class="card-header">
									                <h3 class="card-title">Celda : 1</h3>
									            	</div><br>

															<div class="row">
							                      
								                  <div class="col-sm-3">
								                   	<div class="form-group">
								                     	<label>Densidad:</label>
								                       	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','densidad Batería','Caracteres y números que lo identifican Ej. BA202011112525', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
									                      <input type="text" name="bco_baterias2[celda1][densidad]"  class="form-control" placeholder="Introduzca densidad">
									                  </div>
								                  </div>
																	
																	<div class="col-sm-3">
								                   	<div class="form-group">
								                     	<label>Temperatura:</label><br>
								                     	<input type="text" name="bco_baterias2[celda1][temperatura]"  class="form-control" placeholder="Introduzca densidad">
									                  </div>
								                  </div>
								             
							                    <div class="col-sm-3">
								                    <div class="form-group">
								                     	<label>Tension</label>
								                    	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Capacidad KVA','kilovoltiamperio. Solo números sin letras', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                     	<input type="text" name = "bco_baterias2[celda1][tension]" class="form-control" placeholder="Introduzca Capacidad Amp/AmpH">
								                    </div>
								                  </div>

									                <div class="col-sm-3">
						                      	<div class="form-group">
							                        <label>Observaciones</label>
							                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Cantidad de Elementos Criticos','Solo números sin letras. Ej. 1, 10, 20, etc.', 150 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                        <input type="text" name = "bco_baterias2[celda1][observaciones]" class="form-control" placeholder="Introduzca Numero de observaciones Dañadas">
						                      	</div>
							                    </div>
																</div>

																<div id="banbo_bateria1"></div>
															


															</div>
															
															
														</div>
													</div>

			                  	
													
													</div>


												</div>
					        		</div>
											
											<div id="banbo_bateria"></div>
			        			</div>
		              	<div class="card-footer" align="right">
											<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
										</div>
									</form>
								</div>
            	</div>
        		</div>
    			</div>
				</div>
			</section>
		</div>
@endsection


@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_rect") !!}
@endif

<script type="text/javascript">


  var addButton = $('#add_button'); //Add button selector
    var wrapper = $('#banbo_bateria'); //Input field wrapper
    var cont=2;
      $(addButton).click(function(){ //Once add button is clicked
	    var fieldHTML =
	    						'<div class="card-header">'+
										                '<h3 class="card-title">Celda : '+cont+'</h3>'+
										            '</div><br>'+
	    						 ' <div class="row">'+
				                 ' <div class="col-sm-3">'+
				                   	'<div class="form-group">'+
				                     	'<label>Densidad:</label>'+
									'	<input type="text" name="bco_baterias[celda'+cont+'][densidad]"  class="form-control" placeholder="Introduzca densidad">'+
					        '         </div>'+
				                  '</div>'+
													'<div class="col-sm-3">'+
				                   	'<div class="form-group">'+
				                     	'<label>Temperatura:</label><br>'+
				                     	'<input type="text" name="bco_baterias[celda'+cont+'][temperatura]"  class="form-control" placeholder="Introduzca densidad">'+
					                  '</div>'+
				                  '</div>'+
				             
			                    '<div class="col-sm-3">'+
				                    '<div class="form-group">'+
				                     	'<label>Tension</label>'+
				                     	'<input type="text" name = "bco_baterias[celda'+cont+'][tension]" class="form-control" placeholder="Introduzca Capacidad Amp/AmpH">'+
				                    '</div>'+
				                  '</div>'+

					                '<div class="col-sm-3">'+
		                      	'<div class="form-group">'+
			                        '<label>Observaciones</label>'+
			                        '<input type="text" name = "bco_baterias[celda'+cont+'][observaciones]" class="form-control" placeholder="Introduzca Numero de observaciones Dañadas">'+
		                      	'</div>'+
			                    '</div>'+
												'</div>';



    	cont = cont +1;
	    $(wrapper).append(fieldHTML); // Add field html
        	//$('#boton').prop('hidden', true);
        
    });

    $(wrapper).on('click', '#remove_button', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });





  var addButton1 = $('#add_button1'); //Add button selector
    var wrapper1 = $('#banbo_bateria1'); //Input field wrapper
    var cont1 = 2;
    $(addButton1).click(function(){ //Once add button is clicked
    var fieldHTML1 = 
    									'<div class="card-header">'+
									                '<h3 class="card-title">Celda : '+cont1+'</h3>'+
									            '</div><br>'+
    									' <div class="row">'+
			                 ' <div class="col-sm-3">'+
			                   	'<div class="form-group">'+
			                     	'<label>Densidad:</label>'+
								'	<input type="text" name="bco_baterias2[celda'+cont1+'][densidad]"  class="form-control" placeholder="Introduzca densidad">'+
				        '          </div>'+
			                  '</div>'+
												'<div class="col-sm-3">'+
			                   	'<div class="form-group">'+
			                     	'<label>Temperatura:</label><br>'+
			                     	'<input type="text" name="bco_baterias2[celda'+cont1+'][temperatura]"  class="form-control" placeholder="Introduzca densidad">'+
				                  '</div>'+
			                  '</div>'+
			             
		                    '<div class="col-sm-3">'+
			                    '<div class="form-group">'+
			                     	'<label>Tension</label>'+
			                     	'<input type="text" name = "bco_baterias2[celda'+cont1+'][tension]" class="form-control" placeholder="Introduzca Capacidad Amp/AmpH">'+
			                    '</div>'+
			                  '</div>'+

				                '<div class="col-sm-3">'+
	                      	'<div class="form-group">'+
		                        '<label>Observaciones</label>'+
		                        '<input type="text" name = "bco_baterias2[celda'+cont1+'][observaciones]" class="form-control" placeholder="Introduzca Numero de observaciones Dañadas">'+
	                      	'</div>'+
		                    '</div>'+
											'</div>';

		
    	cont1 = cont1 +1;

	    $(wrapper1).append(fieldHTML1); // Add field html
        	//$('#boton').prop('hidden', true);
        
    });

    $(wrapper1).on('click', '#remove_button', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });










$("#no_posee_rect").click(function(){
	var no_posee_rect = document.getElementById('no_posee_rect');
	var serial_rect = document.getElementById('serial_rect');
	if(no_posee_rect.checked){
        	//$('#serial_gen').hide();
        	serial_rect.value="no posee"
        	$('#serial_rect').prop('readonly', true);
 		}
 	if(!no_posee_rect.checked){
        	//$('#serial_gen').hide();
        	serial_rect.value=""
        	$('#serial_rect').prop('readonly', false);
 		}

 	});



 	 $("#no_posee_bb").click(function(){
	var no_posee_bb = document.getElementById('no_posee_bb');
	var serial_bb = document.getElementById('serial_bb');
	if(no_posee_bb.checked){
        	//$('#serial_gen').hide();
        	serial_bb.value="no posee"
        	$('#serial_bb').prop('readonly', true);
 		}
 	if(!no_posee_bb.checked){
        	//$('#serial_gen').hide();
        	serial_bb.value=""
        	$('#serial_bb').prop('readonly', false);
 		}

 	});

 function falla(){
  	var falla = document.getElementById('input_falla')
  	var range = document.getElementById('range')
  	var opera = document.getElementById('operatividad')
  	console.log(falla.value)

  	range.value == falla.value
  	if(falla.value < 1){
  		opera.value == "Critico"
  	}else if(falla.value>99){
  		opera.value == "Optimo"
  	}

  }

 $('select[id=operatividad]').change(function(){
 	var valor = $(this).val();
 	//console.log(valor)
 	if (valor == "Deficiente"){
 		$('#falla').show();
		$('#input_falla').prop('disabled',false);
 	}else{
		$('#falla').hide();
		$('#input_falla').prop('disabled',true);

 	}



 });

$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });






  	$('select[id=region]').change(function () {
    	console.log('hola')
    var region_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'estado/' + region_id, function (cidades) {
        $('select[id=estado]').empty();

        if(cidades.length != 0){
            $('select[id=estado]').append('<option value=""> Selecciona un estado</option>');
            $.each(cidades, function (key, value) {
                $('select[id=estado]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });

    });


	$('select[id=estado]').change(function () {
    	//console.log('hola')
    var estado_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'central/' + estado_id, function (cidades) {
        $('select[id=central]').empty();

        if(cidades.length != 0){
            $('select[id=central]').append('<option value=""> Selecciona un central</option>');
            $.each(cidades, function (key, value) {
                $('select[id=central]').append('<option value=' + value.id + '>'+ value.nombre +' - '+ value.tipo + '</option>');
            });
        }

        });

    });



});
</script>
@endsection
