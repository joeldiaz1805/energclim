@extends ('/layouts/index')

@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Reporte Banco Bateria 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Reportes</a></li>
              <li class="breadcrumb-item active">Reporte Banco Bateria</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Vista Preliminar de Reportes</h3>
              <div align="right">
               
                  <a href="/tablero_actividad/create" class="btn btn-success">Nuevo Reporte</a>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class=" display nowrap  table table-bordered table-striped" style="width:100%">
                <thead>
                  <tr align="center" >
                    <th class="not-export-col">Acciones</th>
                    <th>Sala</th>
                    <th>Instalacion</th>
                    <th>Responsable</th>
                    <th>Supervisor</th>
                    <th>Fecha Emision</th>
                    <th>Fecha Ejecucion</th>
                    <th>Sistema Dc</th>
                    <th>Observaciones</th>
                    <th>Fecha Actualización</th>
                    <th>Usuario</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($tactividad as $act)
                    <tr align="center" id="tdId_{{$act->id}}">
                      <td>
                     <a href="/tablero_actividad/celdas/{{encrypt($act->id)}}" class="btn btn-warning" title="Agregar Celdas"><i class="fa fa-car-battery"></i></a>
                     <a style="display: none" onclick="deleteItem('{{encrypt($act->id)}}')" title="Descargar reporte" class="btn btn-danger"><i id="ele"  class="fa fa-user-times"></i></a>
                     <a href="verpdf/{{$act->id}}" class="btn btn-info"><i class="fa fa-file-pdf"></i> </a>

                      </td>
                      <td>{{$act->sala}}</td>
                      <td>{{$act->localidad->nombre.' '.$act->localidad->tipo }}</td>
                      <td>{{$act->responsable->nombre}}</td>
                      <td>{{$act->supervisor->nombre}}</td>
                      <td>{{$act->fecha_emision}}</td>
                      <td>{{$act->fecha_ejecucion}}</td>
                      <td>{{$act->sistema_dc}}</td>
                      <td>N/A</td>
                      <td>{{$act->updated_at}}</td>
                      <td>{{$act->usuario->name ?? 'NA'}}</td>
                    </tr>
                    @endforeach
                 
                </tbody>
              </table>
           </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('scriptsalerts')


<script>
   @if(Session::has('exito'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Banco Bateria Guardado con exito'
      })
      })
    });
  @endif
   @if(Session::has('actualizado'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Datos actualizados con exito'
      })
      })
    });
  @endif
  
</script>

  <script>
    function deleteItem(item){
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Desea eliminar este banco de bateria del intentario?',
      text: "Estas por quitar este equipo del Inventario!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, quitarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {


      if (result.value) {



            $.ajax({
                url: "/bancob/"+item,
                type:"DELETE",
                   headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    

                    success:function(result, status, xhr){                    
                        
                        if (status == 'success') {

                          swalWithBootstrapButtons.fire(
                            'Quitado con Exito!',
                            'Se eliminó con exito',
                            'success'
                          )

                          $("#tdId_"+item).hide();
                         // txt.innerHTML = result ;



                        }
                    },
                    error:function(xhr, status, error){
                      

                      Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se eliminó el producto, vuelva a intentarlo mas tarde '+error,
                        footer: '<a href>Seguramente hubo un problema de conexión</a>'
                      })
                      

                    }
                });
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No se ha quitado el producto.',
          'error'
        )
      }
    })
  }
</script>


@endsection