@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Usuario 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Registro</a></li>
              <li class="breadcrumb-item active">Usuario</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
     
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
              
              <div class="card-body">
              	 <form method="POST" id="formulario_usuarios" name="formulario_usuarios"  action="{{ url('/usuarios')}}">
                        {{ csrf_field() }}
                    <input type="hidden" name="active" value="1">
                     	<div class="col-md-12">
				            <!-- general form elements disabled -->
				            <div class="card card-success">
				              <div class="card-header">
				                <h3 class="card-title">Usuario</h3>
				              </div>
				              <!-- /.card-header -->
				              <div class="card-body">
				              	 <div class="row">
				                    <div class="col-sm-4">
				                      <!-- text input -->
				                      	<div class="form-group">
				                        	<label>Nombres</label>
					                        	<input type="text" name="name"  class="form-control" placeholder="Nombres">
					                    </div>
				                     </div>
				                   
									<div class="col-sm-4">
				                      <div class="form-group">
				                        	<label>Apellidos</label>
					                        	<input type="text" name="last_name"  class="form-control" placeholder="Apellidos">
					                    </div>
				                  	</div>

				                   <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Email</label>
				                        <input type="text" class="form-control" name="email"  placeholder="Email">
				                      </div>
				                    </div>
				                  </div>

				                  <div class="row">
				                    <div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Rol o Privilegio</label>
				                        <select name="rol_id" class="form-control">
			                            	@foreach($roles as $rol)
			                            		<option value="{{$rol->id}}">{{$rol->denominacion}}</option>
											@endforeach
						            	</select>
				                      </div>
				                    </div>

					                    <div class="col-sm-4">
				                      <!-- text input -->
					                      <div class="form-group">
					                        <label>Password</label>
					                        <input type="password" name = "password" class="form-control" placeholder="Password">
					                      </div>
					                    </div>
									<div class="col-sm-4">
				                      <!-- text input -->
				                      <div class="form-group">
				                        <label>Confirme Password</label>
				                        <input type="password" name="confirm_password" class="form-control" placeholder="Confirmar Password">
				                      </div>
				                    </div>
				                    </div>
	                            </div>
				            </div>
				        </div>
		                 	 <div class="card-footer" align="right">
			                  <button type="submit" class="btn btn-primary">Crear</button>
			                </div>
			                </form>
                        </div>
		            </div>
		        </div>
            </div>
        </div>
  	</section>
</div>


@endsection
@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_usuarios") !!}
@endif



<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection





