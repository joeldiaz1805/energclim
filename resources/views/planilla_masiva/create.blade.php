@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Reporte Masiva
              <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Rectificador','Un Rectificador es un dispositivo electrónico que permite convertir la corriente alterna en corriente continua.', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a> 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inventario</a></li>
              <li class="breadcrumb-item active">Reporte Masiva</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
      <div class="row ">
	    <div class="col s12 m12 l12 ">
	      @if ($errors->any())
	        <div class="alerta-errores">
	          <ul>
	            @foreach ($errors->all() as $error)
	              <li>{{ $error }}</li>
	            @endforeach
	          </ul>
	        </div>
	      @endif
	    </div>
	  </div>

    <!-- Main content -->
    <section class="content">
      	<div class="container-fluid">
	        <div class="row">
         		<div class="col-12 col-sm-12">
            		<div class="card card-primary card-tabs">
              			<div class="card-body">
	              			<form method="POST" id="formulario_rect" name="formulario_rect"  action="{{url('/tablero_actividad')}}">
	                        	{{ csrf_field() }}
	                        	<input type="hidden" name="usuario_id" value="{{Auth::user()->id}}">
	                        	
				                <div class="col-md-12" >
							        <div class="card card-success">
						              	<div class="card-header">
						                	<h3 class="card-title">Especifique la actividad</h3>
						              	</div>
							              <!-- /.card-header -->
						              	<div class="card-body">
						              		 <div class="row" >
							                  	<div class="col-sm-12">
						                      	<div class="form-group">
						                        	<label>Región:</label>
						                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Región','Seleccione región donde está ubicado el AA', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
						                        	<select id="region" multiple class="form-control">
						                        		
							                        		@foreach($regiones as $region)
								                        		<option value="{{$region->id}}">{{$region->nombre}}</option>
								                        	@endforeach
	 							                    	</select>
								                    </div>
							                    </div>
							                   

							                 </div>
							                 <div id="banbo_bateria" class="row"></div>
		
						                 		<div class="row">
						                 			<div class="col-sm-4">
							                 			<div class="form-group">
							                        <label>Nº Ticket:</label>
						                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Serial','de Batería. Secuencia de números y/o caracteres', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        <input type="text" id="ticket" name="ticket_num"  class="form-control" placeholder="Introduzca Nùmero de ticket Sismyc">
										                </div>
						                 			</div>
								               		<div class="col-sm-4">
							                     	<div class="form-group">
							                       	<label>Realizado por:</label>
							                       	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Estructura','Tipo de infraestrcutura tecnológica donde está alojado el rectificador', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                       	<select name="responsable_id" required class="form-control select2">
					                        			@foreach($personal as $super)
					                        				<option value="{{$super->id}}">{{$super->nombre}}</option>
					                        			@endforeach
						                    			</select>
									                  </div>
								                	</div>
							                    <div class="col-sm-4">
							                      	<div class="form-group">
							                        	<label>Supervision:</label>
							                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Estructura','Tipo de infraestrcutura tecnológica donde está alojado el rectificador', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                        	<select name="supervisor_id" required class="form-control select2">
							                        		@foreach($personal as $super)
					                        					<option value="{{$super->id}}">{{$super->nombre}}</option>
					                        				@endforeach
						                    			</select>
								                    </div>
								                </div>
					                 		</div>

					                 		<div class="row">
					                 			<div class="col-sm-4">
							                      	<div class="form-group">
							                        	<label>Fecha Emision:</label>
							                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Sala','Seleccione sala donde está alojado el rectificador', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        <input class="form-control" type="date" name="fecha_emision">	
							                      	</div>
						                    	</div>
						                    	<div class="col-sm-4">
							                      	<div class="form-group">
							                        	<label>Fecha Ejecucion:</label>
							                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Sala','Seleccione sala donde está alojado el rectificador', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        <input class="form-control" type="date" name="fecha_ejecucion">	
							                        	
							                      	</div>
						                    	</div>
						                    	<div style="display: none;" class="col-sm-4">
						                 				<div class="form-group">
								                        <label>Instalaciòn:</label>
							                        	<input type="checkbox" name="instalacion">
							                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Serial','de Batería. Secuencia de números y/o caracteres', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        <input type="text" id="instalacion" name="instalacion"  class="form-control" placeholder="Introduzca Nùmero de ticket Sismyc">
									                	</div>
						                    	</div>
						                 			
						                 			<div class="col-sm-4">
								                      	<div class="form-group">
								                        	<label>Sala o espacio físico:</label>
								                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Sala','Seleccione sala donde está alojado el rectificador', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
									                        	<select name="sala" id="box_ModeloRectificador" class="form-control">
									                        		<option value="">Seleccione una Sala</option>
																								@foreach($salas as $sala)
									                            		<option value="{{$sala->nombre}}">{{$sala->nombre}}</option>
									                            	@endforeach
																						</select>
								                      	</div>
							                    	</div>
								            </div>
								                
								            <div class="row">

							                  	<div class="col-sm-4">
							                      <!-- text input -->
							                      	<div class="form-group">
							                        	<label>Sistema DC:</label>
							                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Estructura','Tipo de infraestrcutura tecnológica donde está alojado el rectificador', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
								                        <input type="text" id="sistema_dc" name="sistema_dc"  class="form-control" placeholder="Introduzca Nùmero de ticket Sismyc">
								                        
								                    </div>
							                    </div>
					                 		</div>
										</div>
									</div>
								</div>
				             	
						        
				              	<div class="card-footer" align="right">
									<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
								</div>
							</form>
						</div>
            		</div>
        		</div>
    		</div>
		</div>
	</section>
</div>

@endsection


@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_rect") !!}
@endif



<script type="text/javascript">


  var addButton = $('#add_button'); //Add button selector
    var wrapper = $('#banbo_bateria'); //Input field wrapper
    var fieldHTML = 
		    '<div class="card card-success">'+
				'<div class="card-header">'+
					'<h3 class="card-title">Reporte Ups Nº 2</h3>'+
					//'<div align="right">'+
		    		//	'<a href="javascript:void(0);" id="remove_button" class="remove_button" title="Remove field"><i class="fas fa-trash" style="color:red;"></i></a>'+
					//'</div>'+
				'</div>'+
			
				'<div class="card-body">'+
	    			'<div class="row">'+
						'<div class="col-sm-4">'+
		                  '<div class="form-group">'+
		                    '<label>Marca:</label>'+
		                    '<select name="bco_bateria2[marca]" class="form-control">'+
		                    	'<option value=""></option>'+
								'+@foreach($marcas as $mar)'+
									'<option value="{{$mar->descripcion}}">{{$mar->descripcion}}</option>'+
								'@endforeach'+
			            	'</select>'+
		                  '</div>'+
		                '</div>'+
		                '<div class="col-sm-4">'+
		                  	'<div class="form-group">'+
		                    	'<label>Modelo:</label>'+
		                        '<input type="text" name="bco_bateria2[modelo]"  class="form-control" placeholder="Introduzca Modelo">'+
		                   ' </div>'+
		               ' </div>'+
						'<div class="col-sm-4">'+
		                  	'<div class="form-group">'+
		                    	'<label>Tipo:</label><br>'+
		                    	'<input type="checkbox" name="bco_bateria2[tipo]" id="tipo" value="1">Abierto'+
		                    	'<input type="checkbox" name="bco_bateria2[tipo]" id="bco_bateria2[tipo]" value="2">Sellado'+
		                    '</div>'+
		              	'</div>'+
	             	'</div>'+

	            	'<div class="row">'+

		                '<div class="col-sm-4">'+
		                  	'<div class="form-group">'+
		                    	'<label>Capacidad Amp/AmpH:</label>'+
		                    	'<input type="text" name = "bco_bateria2[cap_ah]" class="form-control" placeholder="Introduzca Capacidad Amp/AmpH">'+
		                  	'</div>'+
		               ' </div>'+

		                '<div class="col-sm-4">'+
		                  '<!-- text input -->'+
		                  	'<div class="form-group">'+
		                        '<label>Numero de Celdas</label>'+
		                       ' <input type="text" name = "bco_bateria2[celdas]" class="form-control" placeholder="Introduzca Numero de Celdas Dañadas">'+
		                  	'</div>'+
		                '</div>'+

		                '<div class="col-sm-4">'+
		                  '<!-- text input -->'+
		                  	'<div class="form-group">'+
		                        '<label>Fecha Instalaciòn</label>'+
		                        '<input type="date" name = "bco_bateria2[fecha_instalacion]" class="form-control" placeholder="Introduzca Numero de Celdas Dañadas">'+
		                  	'</div>'+
		               ' </div>'+
					'</div>'+
					'<div class="row">'+
	               
						'<div class="col-sm-4">'+
		                  '<!-- text input -->'+
		                  	'<div class="form-group">'+
		                    	'<label>Voltaje total(FLOT):</label>'+
		                    	'<input type="text" name = "bco_bateria2[v_total]" class="form-control" placeholder="Introduzca Carga de la Localidad">'+
		                  	'</div>'+
		                '</div>'+
		                '<div class="col-sm-4">'+
		                  '<!-- text input -->'+
		                  	'<div class="form-group">'+
		                    	'<label>Voltaje total(CARGA):</label>'+
		                    	'<input type="text" name = "bco_bateria2[v_carga]" class="form-control" placeholder="Introduzca Carga de la Localidad">'+
		                  	'</div>'+
		                '</div>'+
	             
		                '<div class="col-sm-4">'+
		                  '<!-- text input -->'+
		                    '<div class="form-group">'+
		                    	'<label>Inventario Cantv:</label>'+
		                        '<input type="text" class="form-control" name="bco_bateria2[num_inv]"  placeholder="Introduzca Inventario Cantv">'+
		                   ' </div>'+
		                '</div>'+
					'</div>'+
				'</div>'+
			'</div>'
    		

    $(addButton).click(function(){ //Once add button is clicked
		
	    $(wrapper).append(fieldHTML); // Add field html
        	$('#boton').prop('hidden', true);
        
    });

    $(wrapper).on('click', '#remove_button', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });









$("#no_posee_rect").click(function(){
	var no_posee_rect = document.getElementById('no_posee_rect');
	var serial_rect = document.getElementById('serial_rect');
	if(no_posee_rect.checked){
        	//$('#serial_gen').hide();
        	serial_rect.value="no posee"
        	$('#serial_rect').prop('readonly', true);
 		}
 	if(!no_posee_rect.checked){
        	//$('#serial_gen').hide();
        	serial_rect.value=""
        	$('#serial_rect').prop('readonly', false);
 		}

 	});



 	 $("#no_posee_bb").click(function(){
	var no_posee_bb = document.getElementById('no_posee_bb');
	var serial_bb = document.getElementById('serial_bb');
	if(no_posee_bb.checked){
        	//$('#serial_gen').hide();
        	serial_bb.value="no posee"
        	$('#serial_bb').prop('readonly', true);
 		}
 	if(!no_posee_bb.checked){
        	//$('#serial_gen').hide();
        	serial_bb.value=""
        	$('#serial_bb').prop('readonly', false);
 		}

 	});

 function falla(){
  	var falla = document.getElementById('input_falla')
  	var range = document.getElementById('range')
  	var opera = document.getElementById('operatividad')
  	console.log(falla.value)

  	range.value == falla.value
  	if(falla.value < 1){
  		opera.value == "Critico"
  	}else if(falla.value>99){
  		opera.value == "Optimo"
  	}

  }

 $('select[id=operatividad]').change(function(){
 	var valor = $(this).val();
 	//console.log(valor)
 	if (valor == "Deficiente"){
 		$('#falla').show();
		$('#input_falla').prop('disabled',false);
 	}else{
		$('#falla').hide();
		$('#input_falla').prop('disabled',true);

 	}



 });

$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });






  	$('select[id=region]').change(function () {
    	console.log('hola')
    var region_id = $(this).val();
    var regiones = region_id.toString().split(',');
    var wrapper = $('#banbo_bateria'); //Input field wrapper
   
   // console.log(regiones, regiones.length);
   
   
				 $(wrapper).empty();
				$.each(regiones, function (key, value){
				  var fieldHTML ='' ;
				console.log(fieldHTML);
					var APP_URL = {!!json_encode(url('/'))!!}+'/';
				if (value==1) {fieldHTML ="Andes"}else if(value==2){fieldHTML="Capital"}else if(value==3){fieldHTML="Central"}else if(value==4){fieldHTML="Centro Occidente"}else if(value==5){fieldHTML="Guayana"}else if(value==6){fieldHTML="Occidente"}else{fieldHTML="Oriente"}
				   $.get(APP_URL+'estado/' + value, function (cidades) {
				    if(cidades.length != 0){
	            $(wrapper).append(
	            	
		            	'<div class="col-md-4">'+
					          '<div class="form-group">'+
				            '<label>Estados de Region :'+fieldHTML+'</label>'+
				            '<select  id="estado_'+value+'" multiple class="form-control">'+
				             
			            	'</select>'+
				          '</div>'+
				        '</div>'
			        
            	)		


            	$.each(cidades, function (key1, value2){
            		console.log(value)
					       $('select[id=estado_'+value+']').append('<option value=' + value2.id + '>'+ value2.nombre + '</option>');

			            })		          
				        
				        }
				      });
					});
			


    });


	$('select[id=estado]').change(function () {
    	//console.log('hola')
    var estado_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'central/' + estado_id, function (cidades) {
        $('select[id=central]').empty();

        if(cidades.length != 0){
            $('select[id=central]').append('<option value=""> Selecciona un central</option>');
            $.each(cidades, function (key, value) {
                $('select[id=central]').append('<option value=' + value.id + '>'+ value.nombre +' - '+ value.tipo + '</option>');
            });
        }

        });

    });



});
</script>
@endsection
