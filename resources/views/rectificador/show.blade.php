@extends ('/layouts/index')

@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Rectificador 
              <small>Información</small>
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inventario</a></li>
              <li class="breadcrumb-item active">Rectificador - Banco de Batería</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
          <div class="col-md-6">
            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fa fa-cogs"></i>
                  Rectificador
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <dl>
                  <dt>Marca</dt>
                  <dd>{{$rectificador->rectificador->marca}}</dd>
                  <dt>Modelo</dt>
                  <dd>{{$rectificador->rectificador->modelo}}</dd>
                  <dt>Serial</dt>
                  <dd>{{$rectificador->rectificador->serial}}</dd>
                  <dt>Inventario Cantv</dt>
                  <dd>{{$rectificador->rectificador->inventario_cant}}</dd>
                  <dt>Número de fases</dt>
                  <dd>{{$rectificador->rectificador->num_fases}}</dd>
                  <dt>Cantidad de Elementos Operativos</dt>
                  <dd>{{$rectificador->rectificador->cant_oper}}</dd>
                  <dt>Cantidad de Elementos Inoperativos</dt>
                  <dd>{{$rectificador->rectificador->cant_inoper}}</dd>
                 
                  <dt>Consumo Actual en Amperios</dt>
                  <dd>{{$rectificador->rectificador->consumo_amp}}</dd>
                  <dt>Carga Total del Sistema en Amperios</dt>
                  <dd>{{$rectificador->rectificador->carga_total_amp}}</dd>
                  <dt>Voltaje de Operación</dt>
                  <dd>{{$rectificador->rectificador->volt_operacion}}</dd>
                  <dt>¿Posee Tablero de Control?</dt>
                  <dd>{{$rectificador->rectificador->tablero}}</dd>
                </dl>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <div class="row">
              <div class="col-md-12">
                <div class="card card-primary">
                  <div class="card-header">
                    <h3 class="card-title">
                      <i class="fa fa-map"></i>
                          Ubicación
                    </h3>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <dl>
                      <dt>Región</dt>
                      <dd>{{$rectificador->ubicacion->central->region->nombre}}</dd>
                      <dt>Estado</dt>
                      <dd>{{$rectificador->ubicacion->central->estados->nombre}} </dd>
                      <dt>Central / Localidad</dt>
                      <dd>{{$rectificador->ubicacion->central->localidad->nombre}} </dd>
                      <dt>Piso</dt>
                      <dd>{{$rectificador->ubicacion->piso}} </dd>
                      <dt>Estructura</dt>
                      <dd>{{$rectificador->ubicacion->estructura}}</dd>
                      <dt>Sala</dt>
                      <dd>{{$rectificador->ubicacion->sala}} </dd>
                      <dt>Criticidad de espacio</dt>
                      <dd>{{$rectificador->ubicacion->criticidad_rectificador}} </dd>
                     
                    </dl>
                  </div>
                  <!-- /.card-body -->
                </div>
              </div>
            </div>         
          </div>

          <div class="col-md-6">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fa fa-bolt"></i>
                      Banco de Baterías
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <dl>
                  <dt>Marca</dt>
                  <dd>{{$rectificador->bancob->marca}}</dd>
                  <dt>Modelo</dt>
                  <dd>{{$rectificador->bancob->modelo}}</dd>
                  <dt>Serial</dt>
                  <dd>{{$rectificador->bancob->serial}}</dd>
                  <dt>Inventario Cantv</dt>
                  <dd>{{$rectificador->bancob->inventario_cant}}</dd>
                  <dt>Capacidad kva</dt>
                  <dd>{{$rectificador->bancob->cap_kva}}</dd>
                  <dt>Carga Total</dt>
                  <dd>{{$rectificador->bancob->carga_total_bb}}</dd>
                  <dt>Elementos operativos</dt>
                  <dd>{{$rectificador->bancob->elem_oper_bb}}</dd>
                  <dt>Elementos inoperativos</dt>
                  <dd>{{$rectificador->bancob->elem_inoper_bb}}</dd>
                  <dt>Amperaje de elementos</dt>
                  <dd>{{$rectificador->bancob->amp_elem}}</dd>
                  <dt>Autonomía de Respaldo</dt>
                  <dd>{{$rectificador->bancob->autonomia_respaldo}}</dd>
                  <dt>¿Posee Tablero de Control?</dt>
                  <dd>{{$rectificador->bancob->tablero}}</dd>
                </dl>
              </div>
              <!-- /.card-body -->
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="card card-primary">
                  <div class="card-header">
                    <h3 class="card-title">
                      <i class="fa fa-user"></i>
                          Responsable
                    </h3>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <dl>
                      <dt>Nombre y Apellido</dt>
                      <dd>{{$rectificador->ubicacion->responsable->nombre}} / 
                     {{$rectificador->ubicacion->responsable->apellido}}</dd>
                      <dt>Cédula</dt>
                      <dd>{{$rectificador->ubicacion->responsable->cedula}}</dd>
                      <dt>Cargo</dt>
                      <dd>{{$rectificador->ubicacion->responsable->cargo}}</dd>
                      <dt>Numero de Oficina</dt>
                      <dd>{{$rectificador->ubicacion->responsable->num_oficina}}</dd>
                      <dt>Numero de Personal</dt>
                      <dd>{{$rectificador->ubicacion->responsable->num_personal}}</dd>
                      <dt>Numero de Poo</dt>
                      <dd>{{$rectificador->ubicacion->responsable->cod_p00}}</dd>
                      <dt>Correo</dt>
                      <dd>{{$rectificador->ubicacion->responsable->correo}}</dd>
                    </dl>
                  </div>
                  <!-- /.card-body -->
                </div>
              </div>
              
            </div>
          </div>  
        </div>
      </div>
    </section>
</div>
@endsection