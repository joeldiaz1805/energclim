@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Rectificador 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inventario</a></li>
              <li class="breadcrumb-item active">Rectificador</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
              <div class="card-body">
                <form method="POST" id="formulario_rectificador" name="formulario_rectificador"  action="/cuadro_fuerza/{{encrypt($rect->id)}}">
                        {{ csrf_field() }}
                  <input type="hidden" name="_method" value="PUT">
                  <input type="hidden" name="rectificador[user_id]" value="{{Auth::user()->id}}">
                  @if($parametros != null)
                    <input type="hidden" name="parametro" value="{{$parametros['url']}}">
                  @endif

                  <div class="col-md-12">
                    <div class="card card-success">
                      <div class="card-header">
                        <h3 class="card-title">Ubicación y Responsable</h3>
                      </div>
                      <div class="card-body">
                        <div class="card-header">
                          <h3 class="card-title">Ubicación</h3>
                        </div>
                        <br>
                        <div class="row">
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Región:</label>
                              <select name="central[region_id]" class="form-control">
                                @foreach($regiones as $region)
                                  <option @if($region->id == $rect->ubicacion->central->region->id) selected @endif value="{{$region->id}}">{{$region->nombre}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Estado:</label>
                              <select name="central[estado_id]" class="form-control">
                                @foreach($estados as $estado)
                                  <option @if($estado->id == $rect->ubicacion->central->estados->id) selected @endif value="{{$estado->id}}">{{$estado->nombre}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Central:</label>
                                <select name="central[localidad_id]" class="form-control">
                                  @foreach($localidades as $central)
                                    <option @if($central->id == $rect->ubicacion->central->localidad->id) selected @endif value="{{$central->id}}">{{$central->nombre}}</option>
                                  @endforeach
                                </select>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Estructura:</label>
                                <select name="ubicacion[estructura]" class="form-control">
                                  <option @if($rect->ubicacion->estructura="Fija") selected @endif value="Fija">Fija</option>
                                  <option @if($rect->ubicacion->estructura="Movil") selected @endif value="Movil">Móvil</option>
                                  <option @if($rect->ubicacion->estructura="URL") selected @endif value="URL">URL</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Sala:</label>
                              <select name="ubicacion[sala]" id="box_Modelorect" name="box_Modelorect" class="form-control">
                                <option @if($rect->ubicacion->sala="Transmisión") selected @endif value="Transmisión">Transmisión</option>
                                <option @if($rect->ubicacion->sala="PCM") selected @endif value="PCM">PCM</option>
                                <option @if($rect->ubicacion->sala="Conmutación") selected @endif value="Conmutación">Conmutación</option>
                                <option @if($rect->ubicacion->sala="Datos") selected @endif value="Datos">Datos</option>
                                <option @if($rect->ubicacion->sala="PSTN") selected @endif value="PSTN">PSTN</option>
                                <option @if($rect->ubicacion->sala="Oficina") selected @endif value="Oficina">Oficina</option>
                                <option @if($rect->ubicacion->sala="OAC") selected @endif value="OAC">OAC</option>
                                <option @if($rect->ubicacion->sala="Centro de Datos") selected @endif value="Centro de Datos">Centro de Datos</option>
                                <option @if($rect->ubicacion->sala="DSLAM") selected @endif value="DSLAM">DSLAM</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Piso:</label>
                              <input class="form-control" value="{{$rect->ubicacion->piso}}" required name="ubicacion[piso]">
                            </div>
                          </div>
                        </div>
                        <div class="card-header">
                           <h3 class="card-title">Responsable Cuadro de Fuerza</h3>
                        </div>
                        <br>
                        <div class="row">
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Nombres</label>
                                <input type="text" class="form-control" value="{{$rect->ubicacion->responsable->nombre}}" required name="responsable[nombre]" >
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Apellidos</label>
                                <input type="text" class="form-control" value="{{$rect->ubicacion->responsable->apellido}}" required name="responsable[apellido]" >
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Cèdula</label>
                                <input type="text" class="form-control" required value="{{$rect->ubicacion->responsable->cedula}}" name="responsable[cedula]" >
                              </div>
                            </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>P00</label>
                                <input type="text" class="form-control" required value="{{$rect->ubicacion->responsable->cod_p00}}" name="responsable[cod_p00]" >
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Nº de Oficina</label>
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="fas fa-phone"></i></span>
                                </div>
                                <input type="text" name="responsable[num_oficina]" value="{{$rect->ubicacion->responsable->num_oficina}}" required  class="form-control" data-inputmask='"mask": "99999999999"' data-mask>
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Nº Personal</label>
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="fas fa-phone"></i></span>
                                </div>
                                  <input type="text" name="responsable[num_personal]" value="{{$rect->ubicacion->responsable->num_personal}}" required  class="form-control" data-inputmask='"mask": "99999999999"' data-mask>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Cargo:</label>
                              <input type="text" class="form-control" required name="responsable[cargo]" value="{{$rect->ubicacion->responsable->cargo}}" placeholder="Introduzca cargo">
                            </div>
                          </div>
                          <div class="col-sm-8">
                            <div class="form-group">
                              <label>Correo</label>
                              <input type="email" name="responsable[correo]" value="{{$rect->ubicacion->responsable->correo}}" required class="form-control" >
                            </div>
                          </div>
                        </div>
                        <div class="card-header">
                          <h3 class="card-title">Supervisor Cuadro de Fuerza</h3>
                        </div>
                        <br>
                        <div class="row">
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Nombres</label>
                                <input type="text" class="form-control" value="{{$rect->ubicacion->supervisor->nombre}}" required name="supervisor[nombre]" >
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Apellidos</label>
                                <input type="text" class="form-control" value="{{$rect->ubicacion->supervisor->apellido}}" required name="supervisor[apellido]" >
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Cèdula</label>
                                <input type="text" class="form-control" required value="{{$rect->ubicacion->supervisor->cedula}}" name="supervisor[cedula]" >
                              </div>
                            </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>P00</label>
                                <input type="text" class="form-control" required value="{{$rect->ubicacion->supervisor->cod_p00}}" name="supervisor[cod_p00]" >
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Nº de Oficina</label>
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="fas fa-phone"></i></span>
                                </div>
                                <input type="text" name="supervisor[num_oficina]" value="{{$rect->ubicacion->supervisor->num_oficina}}" required  class="form-control" data-inputmask='"mask": "99999999999"' data-mask>
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Nº Personal</label>
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="fas fa-phone"></i></span>
                                </div>
                                  <input type="text" name="supervisor[num_personal]" value="{{$rect->ubicacion->supervisor->num_personal}}" required  class="form-control" data-inputmask='"mask": "99999999999"' data-mask>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Cargo:</label>
                              <input type="text" class="form-control" required name="supervisor[cargo]" value="{{$rect->ubicacion->supervisor->cargo}}" placeholder="Introduzca cargo">
                            </div>
                          </div>
                          <div class="col-sm-8">
                            <div class="form-group">
                              <label>Correo</label>
                              <input type="email" name="supervisor[correo]" value="{{$rect->ubicacion->supervisor->correo}}" required class="form-control" >
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="card card-success">
                      <div class="card-header">
                        <h3 class="card-title">Rectificador</h3>
                      </div>
                      <div class="card-body">
                        <div class="row">
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Marca:</label>
                              <select name="rectificador[marca]" class="form-control">
                                   @foreach($marcas as $mar)
                                        <option @if($mar->descripcion == $rect->rectificador->marca) selected @endif value="{{$mar->descripcion}}">{{$mar->descripcion}}</option>
                                    @endforeach      
                                </select>
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <!-- text input -->
                                <div class="form-group">
                                  <label>Modelo:</label>
                                    <input type="text" name="rectificador[modelo]" value="{{$rect->rectificador->modelo}}" class="form-control" placeholder="Introduzca modelo">
                              </div>
                              </div>
                           
                          <div class="col-sm-4">
                              <div class="form-group">
                                  <label>Serial:</label>
                                    <input type="text" name="rectificador[serial]" value="{{$rect->rectificador->serial}}" class="form-control" placeholder="Introduzca serial">
                              </div>
                          </div>
                         </div>

                        <div class="row">
                             <div class="col-sm-4">
                              <!-- text input -->
                              <div class="form-group">
                                <label>Inventario Cantv:</label>
                                <input type="text" name="rectificador[inventario_cant]" value="{{$rect->rectificador->inventario_cant}}" class="form-control" placeholder="Introduzca Inventario Cantv">
                              </div>
                            </div>
                      </div>

                      <div class="card-header">
                          <h3 class="card-title">Composición:</h3>
                        </div>
                        <br>

                            <div class="row">
                              
                               <div class="col-sm-4">
                                <!-- text input -->
                                <div class="form-group">
                                  <label>Cantidad de Elementos Operativos:</label>
                                  <input type="text" name="rectificador[cant_oper]" value="{{$rect->rectificador->cant_oper}}" class="form-control" placeholder="Ej: 20">
                                </div>
                              </div>
                               <div class="col-sm-4">
                                <!-- text input -->
                                <div class="form-group">
                                  <label>Cantidad de Elementos Inoperativos:</label>
                                  <input type="text" name="rectificador[cant_inoper]" value="{{$rect->rectificador->cant_inoper}}" class="form-control" placeholder="Ej: 20">
                                </div>
                              </div>
                              <div class="col-sm-4">
                                <!-- text input -->
                                <div class="form-group">
                                  <label>Cantidad Total de Elementos:</label>
                                  <input type="text" name="rectificador[cant_total]" value="{{$rect->rectificador->cant_total}}" class="form-control" placeholder="Ej: 20">
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-sm-4">
                                  <div class="form-group">
                                    <label>Consumo Actual en Amperios:</label>
                                    <input type="text" name="rectificador[consumo_amp]" value="{{$rect->rectificador->consumo_amp}}" class="form-control" placeholder="Ej: 20">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <!-- text input -->
                                <div class="form-group">
                                  <label>Carga Total del Sistema en Amperios:</label>
                                    <input type="text" name="rectificador[carga_total_amp]" value="{{$rect->rectificador->carga_total_amp}}" class="form-control" placeholder="Ej: 20">
                                </div>
                              </div>
                              <div class="col-sm-4">
                                <!-- text input -->
                                <div class="form-group">
                                  <label>Voltaje de Operación:</label>
                                  <div class="form-check">
                                      <input class="form-check-input" type="radio" @if($rect->rectificador->volt_operacion=="100-240 VAC") checked @endif value="100-240 VAC" name="rectificador[volt_operacion]">
                                      <label class="form-check-label">100-240 VAC</label>
                                    </div>
                                    <div class="form-check">
                                      <input class="form-check-input" type="radio" @if($rect->rectificador->volt_operacion=="120-208 VAC") checked @endif value="120-208 VAC" name="rectificador[volt_operacion]">
                                      <label class="form-check-label">120-208 VAC</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" @if($rect->rectificador->volt_operacion=="416 VAC") checked @endif name="rectificador[volt_operacion]" value="416 VAC" type="radio" >
                                        <label class="form-check-label">416 VAC</label>
                                    </div>
                                    <div class="form-check">
                                      <input class="form-check-input" type="radio" @if($rect->rectificador->volt_operacion=="480 VAC") checked @endif value="480 VAC" name="rectificador[volt_operacion]">
                                      <label class="form-check-label">480 VAC</label>
                                    </div>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-sm-4">
                                <div class="form-group">
                                  <label>¿Posee Tablero de Control?</label>
                                    <div class="form-check">
                                      <input class="form-check-input" type="radio" @if($rect->rectificador->tablero=="si") checked @endif value="si" name="rectificador[tablero]">
                                      <label class="form-check-label">Si</label>
                                    </div>
                                    <div class="form-check">
                                      <input class="form-check-input" type="radio" @if($rect->rectificador->tablero=="no") checked @endif value="no" name="rectificador[tablero]">
                                      <label class="form-check-label">No</label>
                                    </div>                                
                                  </div>
                               </div>
                               <div class="col-sm-4">
                                <!-- text input -->
                              <div class="form-group">
                                  <label>Número de fases:</label>
                                  <select name="rectificador[num_fases]" class="form-control">
                                      <option @if($rect->rectificador->num_fases=="Monofásico")selected @endif
                                      value="Monofásico">Monofásico</option>
                                      <option @if($rect->rectificador->num_fases=="Bifásico")selected @endif
                                      value="Bifásico">Bifásico</option>
                                      <option @if($rect->rectificador->num_fases=="Trifásico")selected @endif
                                      value="Trifásico">Trifásico</option>
                                  </select>
                              </div>
                              </div>
                        </div>
                      </div>
                 
                      </div>
                     
                    </div>
             
                

                    <div class="col-md-12">
                  <div class="card card-success">
                    <div class="card-header">
                      <h3 class="card-title">Instalación Cuadro de Fuerza</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                          <div class="col-sm-4">
                            <!-- text input -->
                            <div class="form-group">
                              <label>Operatividad:</label>
                              <select name="rect_bcobb[operatividad]" id="operatividad" class="form-control">
                                  <option @if($rect->operatividad=="Optimo")selected @endif value="Optimo">Optimo</option>
                                  <option @if($rect->operatividad=="Critico")selected @endif value="Critico">Critico</option>
                                  <option @if($rect->operatividad=="Vandalizado")selected @endif value="Vandalizado">Vandalizado</option>
                                  <option @if($rect->operatividad=="Deficiente")selected @endif value="Deficiente">Deficiente</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <!-- text input -->
                              <div class="form-group">
                                <label>Fecha de Instalación:</label>
                                  <input name="rect_bcobb[fecha_instalacion]" value="{{$rect->fecha_instalacion}}" id="box_Modelorect" class="form-control" type="date">
                            </div>
                            </div>
                         
                            <div class="col-sm-4">
                            <!-- text input -->
                            <div class="form-group">
                              <label>Criticidad del Equipo:</label>
                              <select name="rect_bcobb[criticidad]" class="form-control">
                                  <option @if($rect->criticidad=="Alta")selected @endif
                                  value="Alta">Alta</option>
                                  <option @if($rect->criticidad=="Media")selected @endif
                                  value="Media">Media</option>
                                  <option @if($rect->criticidad=="Baja")selected @endif
                                  value="Baja">Baja</option>
                              </select>
                            </div>
                          </div>
                       </div>

                       <div class="row" id="falla" @if($rect->operatividad != "Falla") style="display: none" @endif>
                            <div class="col-sm-4">
                              <label>Porcentaje Operatividad:</label>
                              <div class="form-group">
                                    <input name="rect_bcobb[porc_falla]" onblur="falla()" id="input_falla" disabled type="text" value="{{$rect->porc_falla}}">
                              </div>
                            </div>
                           </div>
                       <div class="row">
                          <div class="col-sm-4">
                            <!-- text input -->
                              <div class="form-group">
                                <label>¿Equipo en Garantía?</label>
                                  <div class="form-check">
                                      <input class="form-check-input" type="radio" @if($rect->garantia== "si") checked @endif value="si" name="rect_bcobb[garantia]">
                                      <label class="form-check-label">Si</label>
                                    </div>
                                    <div class="form-check">
                                      <input class="form-check-input" type="radio" @if($rect->garantia== "no") checked @endif value="no" name="rect_bcobb[garantia]">
                                      <label class="form-check-label">No</label>
                                    </div>                            
                                </div>
                          </div>
                      </div>

                      <div class="form-group">
                        <label for="comment">Observaciones:</label>
                          <textarea class="form-control" rows="3" name="rect_bcobb[observaciones]" id="comment">{{$rect->observaciones}}</textarea>      
                      </div>
             
                    </div>
                    <!-- /.card-body -->
                  </div>
                    


                      <div class="card-footer" align="right">
                        <button type="submit" class="btn btn-primary">Actualizar</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection

@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_rectificador") !!}
@endif



<script type="text/javascript">
$(document).ready(function () {
 function falla(){
    var falla = document.getElementById('input_falla')
    var range = document.getElementById('range')
    var opera = document.getElementById('operatividad')
    console.log(falla.value)

    range.value == falla.value
    if(falla.value < 1){
      opera.value == "Critico"
    }else if(falla.value>99){
      opera.value == "Optimo"
    }

  }

 $('select[id=operatividad]').change(function(){
  var valor = $(this).val();
  //console.log(valor)
  if (valor == "Deficiente"){
    $('#falla').show();
    $('#input_falla').prop('disabled',false);
  }else{
    $('#falla').hide();
    $('#input_falla').prop('disabled',true);

  }



 });



  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection


