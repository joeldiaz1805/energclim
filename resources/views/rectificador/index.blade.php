@extends ('/layouts/index')

@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Cuadro de Fuerza 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inventario</a></li>
              <li class="breadcrumb-item active">Cuadro de Fuerza</li>
            </ol>
          </div>
        </div>
      </div>
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Vista Preliminar de Registros</h3>
              <div align="right">
                  <a href="/cuadro_fuerza/create" class="btn btn-success">Nuevo Registro</a>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class=" display nowrap  table table-bordered table-striped" style="width:100%">
                <thead>
                  <tr align="center" >
                    <th class="not-export-col">Acciones</th>
                    <th>Región</th>
                    <th>Estado</th>
                    <th>Central</th>
                    <th>Estructura</th>
                    <th>Sala</th>
                    <th>Piso</th>
                    <th>Responsable</th>
                    <th>Marca Cuadro de Fuerza</th>
                    <th>Modelo Cuadro de Fuerza</th>
                    <th>Serial Cuadro de Fuerza</th>
                    <th>Inventario CANTV Cuadro de Fuerza</th>
                    <th>Número de Fases</th>
                    <th>Cantidad de Elementos Operativos</th>
                    <th>Cantidad de Elementos Inoperativos</th>
                    <th>Consumo Actual (Amp/h)</th>
                    <th>Carga Total del Sistema (Amp/h)</th>
                    <th>Tensión de Operación</th>
                    <th>Tablero de Control</th>
                   
                    <th>Operatividad</th>
                    <th>Porcentaje Operativo</th>
                    <th>Fecha de Instalación</th>
                    <th>Criticidad del Equipo</th>
                    <th>Garantía</th>
                    <th>Observaciones</th>
                    <th>Fecha Actualización</th>
                    <th>Usuario</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($rectificadores as $rect)
                  <tr align="center" id="tdId_{{$rect->id}}">
                    <td>
                      <a href="/cuadro_fuerza/{{encrypt($rect->id)}}/edit" class="btn btn-warning"><i class="far fa-edit"></i></a>
                      <a href="/cuadro_fuerza/{{encrypt($rect->id)}}" class="btn btn-success"><i class="far fa-eye"></i></a>
                      <a  onclick="deleteItem('{{$rect->id}}')"  class="btn btn-danger"><i class="fa fa-times"></i></a>
                    </td>
                    <td>{{$rect->ubicacion->central->region->nombre ?? 'Inexistente'}}</td>
                    <td>{{$rect->ubicacion->central->estados->nombre ?? 'Inexistente'}}</td>
                    <td>{{$rect->ubicacion->central->localidad->nombre ?? 'Inexistente'}}</td>
                    <td>{{$rect->ubicacion->central->localidad->tipo ?? 'S/T'}}</td>
                    <td>{{$rect->ubicacion->sala}}</td>
                    <td>{{$rect->ubicacion->piso}}</td>
                    <td>
                      {{$rect->ubicacion->responsable->nombre}}/
                      {{$rect->ubicacion->responsable->apellido}}/
                      {{$rect->ubicacion->responsable->cod_p00}}/
                      {{$rect->ubicacion->responsable->num_personal}}/
                      {{$rect->ubicacion->responsable->correo}}
                    </td>
                    <td>{{$rect->rectificador->marca}}</td>
                    <td>{{$rect->rectificador->modelo}}</td>
                    <td>{{$rect->rectificador->serial}}</td>
                    <td>{{$rect->rectificador->inventario_cant}}</td>
                    <td>{{$rect->rectificador->num_fases}}</td>
                    <td>{{$rect->rectificador->cant_oper}}</td>
                    <td>{{$rect->rectificador->cant_inoper}}</td>
                    <td>{{$rect->rectificador->consumo_amp}}</td>
                    <td>{{$rect->rectificador->carga_total_amp}}</td>
                    <td>{{$rect->rectificador->volt_operacion}}</td>
                    <td>{{$rect->rectificador->tablero}}</td>
               
                    <td>{{$rect->operatividad}}</td>
                    <td>
                         @if($rect->operatividad =='Falla' && $rect->porc_falla)

                        {{$rect->porc_falla}}%

                        
                      @else
                        @if($rect->operatividad == "Inoperativo" ||  $rect->operatividad == "Vandalizado" )
                          0% 
                        @else
                          100%
                        @endif 
                      @endif

                    </td>




                    <td>{{$rect->fecha_instalacion}}</td>
                    <td>{{$rect->criticidad}}</td>
                    <td>{{$rect->garantia}}</td>
                    <td>{{$rect->observaciones}}</td>
                    <td>{{$rect->updated_at}}</td>
                    <td>{{$rect->rectificador->usuario->name ?? 'Sin Cambios'}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
           </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('scriptsalerts')


<script>
   @if(Session::has('exito'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'rectificador Guardado con exito'
      })
      })
    });
  @endif
   @if(Session::has('actualizado'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Datos actualizados con exito'
      })
      })
    });
  @endif
  
</script>

  <script>
    function deleteItem(item){
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Desea eliminar este rectificador del intentario?',
      text: "Estas por quitar este equipo del Inventario!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, quitarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {


      if (result.value) {



            $.ajax({
                url: "/cuadro_fuerza/"+item,
                type:"DELETE",
                   headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    

                    success:function(result, status, xhr){                    
                        
                        if (status == 'success') {

                          swalWithBootstrapButtons.fire(
                            'Quitado con Exito!',
                            'Se eliminó con exito',
                            'success'
                          )

                          $("#tdId_"+item).hide();
                         // txt.innerHTML = result ;



                        }
                    },
                    error:function(xhr, status, error){
                      

                      Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se eliminó el producto, vuelva a intentarlo mas tarde '+error,
                        footer: '<a href>Seguramente hubo un problema de conexión</a>'
                      })
                      

                    }
                });
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No se ha quitado el producto.',
          'error'
        )
      }
    })
  }
</script>


@endsection