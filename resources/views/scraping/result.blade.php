<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GGEC</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
  
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
   <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">

  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="{{asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
  <!-- Toastr -->
  <link rel="stylesheet" href="{{asset('plugins/toastr/toastr.min.css')}}">
    <!-- Select2 -->
  <link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
 
</head>
<body  style="background-image: url('{{ asset('images/fondo.jpg') }}'); no-repeat center center fixed; background-size: 100% 100%;" class="hold-transition login-page " >
    
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
    <div style="margin-top: -150px">
	     <div class="banner">
	        <img src="{{asset('images/banner.png')}}" width="500" height="70">
	        <h2 style="color: black">Bienvenido al Portal Web Interactivo</h2>
	    </div>
    </div>
	
            <div class="card card-info" style="width: 90%; height: 80%">
              <div class="card-header">
                <h3 class="card-title">Consulta tu telefono fijo y tu plan de servicio</h3>
              	<div align="right"> <a href="/consulta_bossprov"><i class="fa fa-search"></i> </a></div>
              </div>
              <div class="card-body">

				<section class="content">
			      <div class="container-fluid">
			     
			        <div class="row">
			          <div class="col-12 col-sm-12">
			            <div class="card card-primary card-tabs">
			              
			              <div class="card-body">
			              	 <form method="POST" id="formulario_usuarios" enctype="multipart/form-data" name="formulario_usuarios"  action="{{ url('/consulta_bossprov')}}">
			                    {{ csrf_field() }}
			                   	<div class="col-md-12">
							            <div class="card card-warning">
							              <div class="card-header">
			                        		
							                <h3 class="card-title">Resultado de la consulta al Numero : {{$area}} {{$numero}} @if( strpos($pr[26],"Cortado_por_pago") ) <h5 style="margin-left: 2px; color: red;display:inline;"> Cortado Por pago</h5> @else <h5 style="margin-left: 2px; color: green;display:inline;">  Liberado y Navegando</h5> @endif  </h3>
							              
							              </div>

							              <!-- /.card-header -->
							              <div class="card-body">
			                       
							              	<div class="row">
			                       		 <div class="col-sm-12">
			                            <div class="form-group">
			                              <label>Estado de la Linea:</label>
			                              <table id="example1" class=" display table table-bordered table-striped nowrap" style="width:100%">
			                              	<tr>
			                              		<th>{{$th[0]->nodeValue}}:</th>
			                              		<td @if($td[0]->nodeValue == "UP") style="color:green" @else style="color:red" @endif>{{$td[0]->nodeValue}}</td>
			                              	</tr>
			                              	<tr>
			                              		<th>{{$th[1]->nodeValue}}:</th>
			                              		<td @if($td[1]->nodeValue == "UP") style="color:green" @else style="color:red" @endif>{{$td[1]->nodeValue}}</td>
			                              	</tr>
			                              	<tr>
			                              		<th >{{$th[2]->nodeValue}}:</th>
			                              		<td>{{$td[2]->nodeValue}}</td>
			                              	</tr>
			                              	<tr>
			                              		<th>{{$th[3]->nodeValue ?? '-'}}:</th>
			                              		<td>{{$td[3]->nodeValue ?? '-'}}</td>
			                              	</tr>
			                              		
			                              </table>
			                            </div>
			                          </div>
			                       	</div>
			                        <div class="row">
			                          <div class="col-sm-6">
			                          	<table id="example1" class=" display table table-bordered table-striped nowrap" style="width:100%">
			                          		<label>{{$pd[0][0]}}</label>
		                          			<tr>
		                          				<th>{{$pd[0][1]}}</th>
		                          				<td>{{$pd[0][2]}}</td>
		                          			</tr>
			                          		<tr>
			                          			<th>{{$pd[1][0]}}</th>
			                          			<td>{{$pd[1][1]}}</td>
			                          		</tr>
			                          		<tr>
			                          			<th>{{$pd[2][0]}}</th>
			                          			<td>{{$pd[2][1]}}</td>
			                          		</tr>
			                          		<tr>
			                          			<th>{{$pd[3][0]}}</th>
			                          			<td>{{$pd[3][1]}}</td>
			                          		</tr>
			                          		<tr>
			                          			<th>{{$pd[4][0]}}</th>
			                          			<td>{{$pd[4][1]}}</td>
			                          		</tr>
			                          		<tr>
			                          			<th>{{$pd[5][0]}}</th>
			                          			<td>{{$pd[5][1]}}</td>
			                          		</tr>
			                          		<tr>
			                          			<th>{{$pd[5][0]}}</th>
			                          			<td>{{$pd[5][1]}}</td>
			                          		</tr>
			                          		<tr>
			                          			<th>{{$pd[6][0]}}</th>
			                          			<td>{{$pd[6][1]}}</td>
			                          		</tr>
			                          		<tr>
			                          			<th>{{$pd[7][0]}}</th>
			                          			<td>{{$pd[7][1]}}</td>
			                          		</tr>
			                          		<tr>
			                          			<th>{{$pd[8][0]}}</th>
			                          			<td>{{$pd[8][1]}}</td>
			                          		</tr>
			                          		<tr>
			                          			<th>{{$pd[9][0]}}</th>
			                          			<td>{{$pd[9][1]}}</td>
			                          		</tr>
			                          		<tr>
			                          			<th>{{$pd[10][0]}}</th>
			                          			<td>{{$pd[10][1]}}</td>
			                          		</tr>
			                          		<tr>
			                          			<th>{{$pd[11][0]}}</th>
			                          			<td>{{$pd[11][1]}}</td>
			                          		</tr>
			                          	

			                          	</table>
			                           
			                          </div>
			                          <div class="col-sm-6">
			                          	<div class="form-group">
			                          		<table id="example1" class=" display table table-bordered table-striped nowrap" style="width:100%">
			                          		<label>-</label>	
			                          		<tr>
			                          			<th>{{$pd[12][0]}}</th>
			                          			<td>{{$pd[12][1]}}</td>
			                          		</tr>
			                          		<tr>
			                          			<th>{{$pd[13][0]}}</th>
			                          			<td>{{$pd[13][1]}}</td>
			                          		</tr>
			                          		<tr>
			                          			<th>{{$pd[14][0]}}</th>
			                          			<td>{{$pd[14][1]}}</td>
			                          		</tr>
			                          		<tr>
			                          			<th>{{$pd[15][0]}}</th>
			                          			<td>{{$pd[15][1]}}</td>
			                          		</tr>
			                          		<tr>
			                          			<th>{{$pd[16][0]}}</th>
			                          			<td>{{$pd[16][1]}}</td>
			                          		</tr>
			                          		<tr>
			                          			<th>{{$pd[17][0]}}</th>
			                          			<td>{{$pd[17][1]}}</td>
			                          		</tr>
			                          		<tr>
			                          			<th>{{$pd[18][0]}}</th>
			                          			<td>{{$pd[18][1]}}</td>
			                          		</tr>
			                          		<tr>
			                          			<th>{{$pd[19][0]}}</th>
			                          			<td>{{$pd[19][1]}}</td>
			                          		</tr>
			                          		<tr>
			                          			<th>{{$pd[20][0]}}</th>
			                          			<td>{{$pd[20][1]}}</td>
			                          		</tr>
			                          		<tr>
			                          			<th>{{$pd[21][0]}}</th>
			                          			<td>{{$pd[21][1]}}</td>
			                          		</tr>
			                          		
			                          		<tr>
			                          			<th>Datos Plan Aba</th>
			                          			<td>{{$pr[22]}}{{$pr[23]}}<br>{{$pr[24]}},{{$pr[25]}}<br>{{$pr[26]}},{{$pr[27]}},{{$pr[28]}},{{$pr[29]}}</td>
			                          		</tr>
			                          		</table>
			                          	</div>
			                          </div>

			                         
			                        </div>
			                       	
			  				            </div>
							              </div>
					                 	
						                </form>
			                    </div>
					            </div>
					        </div>
			            </div>
			        </div>
			  	</section>
              </div>
              
            </div>
          
          
            

	
  
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('plugins/select2/js/select2.full.min.js')}}"></script>

<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- SweetAlert2 -->
<script src="{{asset('dist/js/clipboard.min.js')}}"></script>

  <script src="{{asset('plugins/sweetalert2/sweetalert2.min.js')}}"></script>
  <!-- Toastr -->
  <script src="{{asset('plugins/toastr/toastr.min.js')}}"></script>

<script src="{{asset('js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>



<script type="text/javascript">
var clipboard = new Clipboard('.btn');
$('.select2').select2();

function generar(){
	var rt;
	var mnt;
	var fll = " -";
	var equipo1 = document.getElementById('checkbox1');
	var equipo2 = document.getElementById('checkbox2');
	var equipo3 = document.getElementById('checkbox3');
	var equipo4 = document.getElementById('checkbox4');
	var provedores = document.getElementById('provedores');
	var fallas = document.getElementById('fallas');
	var central = document.getElementById('central2');
	var estados = document.getElementById('esta2');
	var region = document.getElementById('region');
	var aula = document.getElementById('aula');
	var tipo = document.getElementById('tipo');
	

	if (equipo1.checked){
		rt = equipo1.value
	}if (equipo2.checked){
		fll =' que se encuentra con falla: '+fallas.options[fallas.selectedIndex].text
		rt = equipo2.value
	}if (equipo3.checked){
		rt = equipo3.value
	}if (equipo4.checked){
		rt = equipo4.value
	}
	var mantenimiento = document.getElementById('tipo_servicio');
	var mantenimiento2 = document.getElementById('tipo_servicio2');

	var personal = document.getElementById('personal');
	var tipo_actividad = document.getElementById('mantenimiento');
	
	var box_ModeloRectificador = document.getElementById('box_ModeloRectificador');

	var equipo = document.getElementById('equipo');
	var telefono = document.getElementById('telefono');
	var info = document.getElementById('info');
	var componente = document.getElementById('componente');

	var piso = document.getElementById('piso');
	var accion2 = document.getElementById('accion2');
	var accion1 = document.getElementById('accion1');
	if (equipo.value == 'EXTRACTOR' || equipo.value == 'AIRE ACONDICIONADO' || equipo.value == 'CHILLER' || equipo.value == 'UMA' || equipo.value == 'VARIOS(A/A)' ){
		mnt=  'A/A'
	}else{
		mnt = "EX"
	}

	if(fallas.options[fallas.selectedIndex].text == "OTROS(ESPECIFIQUE)"){
		accion1.value = "OTROS:";
		$('#input_accion').show();
	}else{
		$('#input_accion').hide()
		accion1.value = ""
		accion2.value = fallas.options[fallas.selectedIndex].text
	}
	
	document.getElementById('comment').value= central2.value+'_'
	+tipo.value.toUpperCase()+'_'
	+accion1.value+accion2.value.toUpperCase()+'_'
	+equipo.options[equipo.selectedIndex].text+'_'
	+box_ModeloRectificador.options[box_ModeloRectificador.selectedIndex].text+'_'
	+piso.options[piso.selectedIndex].text+'_'
	+aula.value+'_'
	+personal.value.toUpperCase()+'_'
	+telefono.value+'_'
	+info.value.toUpperCase()+'_'
	+mnt+'_'
	+estados.value+'_'
	+region.value+'_'
	+rt+'_'
	+provedores.value

}


  $(document).ready(function(){


  	$('select[id=centrales]').change(function () {
    var central = $(this).val();
    var valores = central.split('-')
    //console.log('hola',valores)
  	
  	document.getElementById('tipo').value = valores[3]
  	document.getElementById('central2').value = valores[0]
  	document.getElementById('esta2').value = valores[1]
  	document.getElementById('region').value =valores[2]
  	//console.log(cidades[0].tipo)
  	if (valores[3]==""){
  		$('#tipo').attr("readonly",false)
  	}else{
  		$('#tipo').attr("readonly",true)

  	}
  	generar();
    
    
    // var APP_URL = {!!json_encode(url('/'))!!}+'/';
    // $.get(APP_URL+'centrales/' + central, function (cidades) {
    //  //$('select[id=estado]').empty();
    //  	console.log(cidades[0].nombre, cidades[0].estados.nombre, cidades[0].estados.region.nombre)
     	



    //     });

    });
  });


</script>
</body>
</html>


