<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GGEC</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">


  <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">

    <!-- Select2 -->
  <link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
 
</head>
<body  style="background-image: url('{{ asset('images/fondo.jpg') }}'); no-repeat center center fixed; background-size: 100% 100%;" class="hold-transition login-page " >
    
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
    <div style="margin-top: -150px">
	     <div class="banner">
	        <img src="{{asset('images/banner.png')}}" width="500" height="70">
	        <h2 style="color: black">Bienvenido al Portal Web Interactivo</h2>
	    </div>
    </div>
	
            <div class="card card-info" style="width: 90%; height: 80%">
              <div class="card-header">
                <h3 class="card-title">Consulta tu telefono fijo y tu plan de servicio</h3>
              </div>
              <div class="card-body">

				<section class="content">
			      <div class="container-fluid">
			     
			        <div class="row">
			          <div class="col-12 col-sm-12">
			            <div class="card card-primary card-tabs">
			              
			              <div class="card-body">
			              	 <form method="POST" id="formulario_usuarios" enctype="multipart/form-data" name="formulario_usuarios"  action="{{ url('/consulta_bossprov')}}">
			                    {{ csrf_field() }}
			                   	<div class="col-md-12">
							            <div class="card card-warning">
							              <div class="card-header">
							                <h3 class="card-title">Numero a Contultar</h3>
			                        
							              </div>
							              <!-- /.card-header -->
							              <div class="card-body">
			                        <div class="row">
			                          <div class="col-sm-6">
			                            <div class="form-group">
			                              <label>Codigo de àrea:</label>
			                              <select name="area" class="form-control select2 ">
			                              	 <option value="212">212</option>
			                              	 <option value="234">234</option>
			                              	 <option value="235">235</option>
			                              	 <option value="237">237</option>
			                              	 <option value="238">238</option>
			                              	 <option value="239">239</option>
			                              	 <option value="240">240</option>
			                              	 <option value="241">241</option>
			                              	 <option value="242">242</option>
			                              	 <option value="243">243</option>
			                              	 <option value="244">244</option>
			                              	 <option value="245">245</option>
			                              	 <option value="246">246</option>
			                              	 <option value="247">247</option>
			                              	 <option value="248">248</option>
			                              	 <option value="249">249</option>
			                              	 <option value="251">251</option>
			                              	 <option value="252">252</option>
			                              	 <option value="253">253</option>
			                              	 <option value="254">254</option>
			                              	 <option value="255">255</option>
			                              	 <option value="256">256</option>
			                              	 <option value="257">257</option>
			                              	 <option value="258">258</option>
			                              	 <option value="259">259</option>
			                              	 <option value="261">261</option>
			                              	 <option value="262">262</option>
			                              	 <option value="263">263</option>
			                              	 <option value="264">264</option>
			                              	 <option value="265">265</option>
			                              	 <option value="266">266</option>
			                              	 <option value="267">267</option>
			                              	 <option value="268">268</option>
			                              	 <option value="269">269</option>
			                              	 <option value="271">271</option>
			                              	 <option value="272">272</option>
			                              	 <option value="273">273</option>
			                              	 <option value="274">274</option>
			                              	 <option value="275">275</option>
			                              	 <option value="276">276</option>
			                              	 <option value="277">277</option>
			                              	 <option value="278">278</option>
			                              	 <option value="279">279</option>
			                              	 <option value="281">281</option>
			                              	 <option value="282">282</option>
			                              	 <option value="283">283</option>
			                              	 <option value="284">284</option>
			                              	 <option value="285">285</option>
			                              	 <option value="286">286</option>
			                              	 <option value="287">287</option>
			                              	 <option value="288">288</option>
			                              	 <option value="289">289</option>
			                              	 <option value="291">291</option>
			                              	 <option value="292">292</option>
			                              	 <option value="293">293</option>
			                              	 <option value="294">294</option>
			                              	 <option value="295">295</option>
			                              	 <option value="296">296</option>
			                              </select>
			                            </div>
			                          </div>
			                          <div class="col-sm-6">
			                            <div class="form-group">
			                              <label>Numero:</label>
			                              <input type="text" name="numero" id="fechaFin"  class="form-control" placeholder="Numero de fijo">
			                            </div>
			                          </div>
			                        </div>
			  				            </div>
							              </div>
					                 	 <div class="card-footer" align="right">
						                  <button type="submit"  id="crear"class="btn btn-primary">Consultar</button>
						                </div>
						                </form>
			                    </div>
					            </div>
					        </div>
			            </div>
			        </div>
			  	</section>
              </div>
              
            </div>
          
          
            

	
  
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/select2/js/select2.full.min.js')}}"></script>

<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- SweetAlert2 -->
<script src="{{asset('dist/js/clipboard.min.js')}}"></script>



<script type="text/javascript">
var clipboard = new Clipboard('.btn');
$('.select2').select2();

</script>
</body>
</html>


