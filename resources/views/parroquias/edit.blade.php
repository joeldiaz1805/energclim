@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Parroquias 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Registro</a></li>
              <li class="breadcrumb-item active">Parroquias</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
     
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
              
              <div class="card-body">
              	 <form method="POST" id="formulario_usuarios" enctype="multipart/form-data" name="formulario_usuarios"  action="/parroquias/{{encrypt($parroquia->id)}}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="user_id" value="{{Auth::user()->id}}">

                     	<div class="col-md-12">
				            <!-- general form elements disabled -->
				            <div class="card card-success">
				              <div class="card-header">
				                <h3 class="card-title">Parroquia</h3>
				              </div>
				              <!-- /.card-header -->
				              <div class="card-body">
                         <div class="row">
                          <div class="col-sm-12">
                            <label>Busque un Estado:</label>
                            <div class="form-group">
                              <select id="estado_id" class="form-control select2">
                                @foreach($estados as $edo)
                                  <option value="{{$edo->id}}" @if($edo->id == $parroquia->municipio->estado_id) selected @endif >{{$edo->nombre}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                        </div>
				              	 <div class="row">
				                    <div class="col-sm-6">
				                      <!-- text input -->
				                      	<div class="form-group">
                                  <label>Municipio Asignado:</label>
                                    <select  name="estado_id" id="estado_id" class="form-control">
                                      <option>Seleccione un Municipio</option>
                                        @foreach($municipios as $mun)
                                          <option @if($mun->id == $parroquia->municipio_id) selected @endif value="{{$mun->id}}">{{$mun->descripcion}}</option>
                                        @endforeach
                                    </select>
                                </div>
				                    </div>


                            <div class="col-sm-6">
                                <div class="form-group">
                                  <label>Nombre o Descripciòn</label>
                                    <input type="text" name="descripcion" value="{{$parroquia->descripcion}}"  class="form-control" placeholder="Nombre de Parroquias">
                              </div>
                             </div>

                            
				                   
									           

	                       </div>
				            </div>
				        </div>
		                 	 <div class="card-footer" align="right">
			                  <button type="submit" class="btn btn-primary">Actualizar</button>
			                </div>
			                </form>
                        </div>
		            </div>
		        </div>
            </div>
        </div>
  	</section>
</div>


@endsection
@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_usuarios") !!}
@endif



<script type="text/javascript">
   $(document).ready(function(){



    $('select[id=estado_id]').change(function () {
     console.log('hola')
    var estado_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'municipios_poredo/' + estado_id, function (cidades) {
        $('select[id=municipio_id]').empty();

        if(cidades.length != 0){
            $('select[id=municipio_id]').append('<option value=""> Selecciona un municipio</option>');
            $.each(cidades, function (key, value) {
                $('select[id=municipio_id]').append('<option value=' + value.id + '>'+ value.descripcion + '</option>');
            });
        }

        });

    });
  });

$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection





