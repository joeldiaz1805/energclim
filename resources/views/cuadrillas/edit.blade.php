@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Proveedores 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Registro</a></li>
              <li class="breadcrumb-item active">Proveedores</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
     
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
              
              <div class="card-body">
                 <form method="POST" id="formulario_usuarios" enctype="multipart/form-data" name="formulario_usuarios"  action="/cuadrillas/{{encrypt($cuadrilla->id)}}">
                    {{ csrf_field() }}
                    <input type="hidden" value="{{Auth::user()->id}}" name="user_id">
                      <input type="hidden" value="PUT" name="_method">

                       <div class="col-md-12">
                    <div class="card card-success">
                      <div class="card-header">
                        <h3 class="card-title">Cuadrilla</h3>
                        
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body">
                        <div class="row" >

                          <div class="col-sm-12">
                            <div class="form-group">
                              <label>Descripciòn de la Cuadrilla</label>
                              <input type="text" name="nombre"  class="form-control" value="{{$cuadrilla->nombre}}" placeholder="Nombre Cuadrilla">
                            </div>
                          </div>
                          </div>
                        <div class="row" id="field_wrapper">
                          <div class="col-sm-12">
                            <div class="form-group">
                              <label>Asignar Personal</label>
                                <a href="javascript:void(0);" id="add_button" class="btn btn-warning" title="Add field"><i class="fas fa-plus"></i></a>
                                <br>
                                <br>

                                 <div class="row" style="margin-left: 2%">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      @foreach($cuadrilla->cuadrante as $per)
                                        <input checked type="checkbox" class="form-check-input" value="{{$per->personal->id}}" name="personal[]" ><label class="form-check-label" >{{$per->personal->nombre}} {{$per->personal->apellido}}</label>
                                        <br>
                                      @endforeach
                                    </div>
                                  </div>
                                </div>
                                 <select name="personal[]" class="form-control">
                                  @foreach($personal as $per)
                                    <option @if($per->id == $cuadrilla->cuadrante[0]->personal->id) selected @endif value="{{$per->id}}">{{$per->nombre}}</option>
                                  @endforeach
                                </select>
                            </div>
                          </div>
                         </div>
                    </div>
                      </div>
                       <div class="card-footer" align="right">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                      </div>
                      </form>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </section>
</div>


@endsection
@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#formulario_usuarios") !!}
@endif



<script type="text/javascript">

   
    var addButton = $('#add_button'); //Add button selector
    var wrapper = $('#field_wrapper'); //Input field wrapper
    var fieldHTML = 
    '<div class="col-sm-12">'+
                            '<div class="form-group">'+
                              '<label>Asignar Personal</label>'+
                                ' <select name="personal[]" class="form-control">'+
                                  '@foreach($personal as $per)'+
                                    '<option value="{{$per->id}}">{{$per->nombre}}</option>'+
                                   '@endforeach'+
                                '</select>'+
                                '<a href="javascript:void(0);" id="remove_button" class="remove_button" title="Remove field"><i class="fas fa-trash" style="color:red;"></i></a'+
                            '</div>'+
                          '</div>'

    $(addButton).click(function(){ //Once add button is clicked
        
            $(wrapper).append(fieldHTML); // Add field html
        
    });
    $(wrapper).on('click', '#remove_button', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });


$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection





