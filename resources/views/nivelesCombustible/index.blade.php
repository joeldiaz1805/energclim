@extends ('/layouts/index')

@section('content')

<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Niveles de Combustible 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inventario</a></li>
              <li class="breadcrumb-item active">Niveles de Combustible</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Vista Preliminar de Registros</h3>
              <div align="right">
                  <a href="/motogenerador/create" class="btn btn-success">Nuevo Registro</a>
              </div>
            </div>
            <!-- /.card-header -->

            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr align="center">
                    <th class="not-export-col">Acciones</th>
                    <th>Región</th>
                    <th>Estado</th>
                    <th>Localidad</th>
                    <th>Tipo Localidad</th>
                    <th>Operatividad</th>
                    <th>Capacidad kva</th>
                    <th>Capacidad Tanque Principal (ltrs)</th>
                    <th>Cantidad Actual Comb</th>
                    <th>Porcentaje de tanque (%)</th>
                    <th>Capacidad Tanque Aceite (ltrs)</th>
                    <th>Cantidad Actual Aceite</th>
                    <th>Porcentaje de tanque acite (%)</th>
                    <th>Cantidad Faltante Ltrs</th>
                    <th>Consumo Ltrs/Hora</th>
                    <th>Consumo Ltrs/Dia</th>
                    <th>Autonomia en Dias</th>
                    <th>Autonomia en Horas</th>
                    <th>Autonomia en Dias al 100%</th>
                    <th>Autonomia en Horas al 100%</th>
                    <th>Fecha Actualización</th>
                    <th>Último Usuario</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($motor as $equipos)
                  <tr align="center" id="tdId_{{$equipos->id}}">
                    <td>
                      <a href="/nivel_combustible/{{encrypt($equipos->id)}}/edit" class="btn btn-warning"><i class="far fa-edit"></i></a>
                    </td>
                    <td>{{$equipos->motor->ubicacion->central->localidad->estados->region->nombre ?? 'inex'}}</td>
                    <td>{{$equipos->motor->ubicacion->central->localidad->estados->nombre?? 'Estado inexistente' }}</td>
                    <td>{{$equipos->motor->ubicacion->central->localidad->nombre?? 'Localidad inexistente' }}</td>
                    <td>{{$equipos->motor->ubicacion->central->localidad->tipo?? 'Tipo no disponible'}}</td>
                    <td>{{$equipos->motor->operatividad}}</td>
                    <td>{{$equipos->generador->cap_kva}}</td>
                    <td>{{$equipos->motor->comb_tanq_princ}}</td>
                    <td>{{$equipos->motor->cant_actual}}</td>
                    <td>
                       @php try { echo round(($equipos->motor->cant_actual*100) / $equipos->motor->comb_tanq_princ).'%'; }catch(Exception $e){ echo(0).'%';} @endphp
                    </td>
                    <td>{{$equipos->motor->cap_tanq_aceite}} </td>
                    <td>{{$equipos->motor->aceite_actual}} </td>
                    <td> @php try { echo round(($equipos->motor->aceite_actual*100) / $equipos->motor->cap_tanq_aceite).'%'; }catch(Exception $e){ echo(0).'%';} @endphp</td>


                    <td>@php try { echo intval($equipos->motor->comb_tanq_princ) -intval($equipos->motor->cant_actual); 
                        $ltrsDia = $equipos->motor->ltrs_hora * 24;
                         }catch(Exception $e){ echo(0).'%';}
                        @endphp</td>



                    <td>{{$equipos->motor->ltrs_hora}}</td>
                    <td>{{$ltrsDia}}</td>
                    <td>@php try { echo number_format($equipos->motor->cant_actual  / $ltrsDia, 2,'.',''); } catch(Exception $e){ echo 'Incalculable';} @endphp</td>
                    <td>@php try { echo number_format($equipos->motor->cant_actual  / $equipos->motor->ltrs_hora,'2','.',''); } catch(Exception $e) {echo 'Incalculable';} @endphp</td>
                    <td>@php try { echo number_format($equipos->motor->comb_tanq_princ / $ltrsDia,'2','.',''); }catch (Exception $e) {echo "incalculable";} @endphp</td>
                    <td>@php try { echo number_format($equipos->motor->comb_tanq_princ / $equipos->motor->ltrs_hora,'2','.',''); } catch (Exception $e) {echo "incalculable";} @endphp</td>
                    <td>{{$equipos->motor->updated_at}}</td>
                    <td>{{$equipos->motor->usuario->name ?? 'Sin Cambios'}}</td>

                  </tr>
                    @endforeach
                </tbody>
              </table>
           </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('scriptsalerts')


<script>
   @if(Session::has('exito'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Motogenerador Guardado con éxito'
      })
      })
    });
  @endif
   @if(Session::has('actualizado'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Datos actualizados con exito'
      })
      })
    });
  @endif
  
</script>

  <script>
    function deleteItem(item){
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Desea eliminar este motogenerador del intentario?',
      text: "Estas por quitar este equipo del Inventario!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, quitarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {

      var txt = document.getElementById("totalCar");
      console.log(item)

      if (result.value) {



            $.ajax({
                url: "/motogenerador/"+item,
                type:"DELETE",
                   headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    

                    success:function(result, status, xhr){                    
                        
                        if (status == 'success') {

                          swalWithBootstrapButtons.fire(
                            'Quitado con Exito!',
                            'Se eliminó con exito',
                            'success'
                          )

                          $("#tdId_"+item).hide();
                         // txt.innerHTML = result ;



                        }
                    },
                    error:function(xhr, status, error){
                      

                      Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No se eliminó el producto, vuelva a intentarlo mas tarde '+error,
                        footer: '<a href>Seguramente hubo un problema de conexión</a>'
                      })
                      

                    }
                });
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'No se ha quitado el producto.',
          'error'
        )
      }
    })
  }
</script>


@endsection
