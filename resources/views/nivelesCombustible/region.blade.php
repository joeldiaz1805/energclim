@extends ('/layouts/index')

@section('content')
                 

<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-9">
            <h1>
              Niveles de Combustible - Region  {{$motor[0]->motor->ubicacion->central->localidad->estados->region->nombre}}
            </h1>
          </div>
          <div class="col-sm-3">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inventario</a></li>
              <li class="breadcrumb-item active">Niveles de Combustible</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Vista Preliminar de Registros</h3>
              <div align="right">
                
                  <h2 ><a href="{{url()->previous()}}"> <i class="fa fa-arrow-left"></i></a> Litros faltantes: <strong style="color:red;">{{$faltante}}</strong></h2>
              </div>
            </div>
            <!-- /.card-header -->

            <div class="card-body">
              <table id="example4" class="table table-bordered table-striped">
                <thead>
                  <tr align="center">
                    
                    <th>Estado</th>
                    <th>Localidad</th>
                    <th>Tipo Localidad</th>
                    <th>Operatividad</th>
                    <th>Capacidad Tanque Principal (ltrs)</th>
                    <th>Cantidad Actual Comb</th>
                    <th>Litros Faltantes</th>
                    <th>Porcentaje de tanque (%)</th>
                    <th>Fecha Actualización</th>
                    <th>Usuario</th>
                   
                  </tr>
                </thead>
                <tbody>
                  @foreach($motor as $equipos)
                  <tr align="center" id="tdId_{{$equipos->id}}">
                    
                    <td>{{$equipos->motor->ubicacion->central->localidad->estados->nombre?? 'Estado inexistente' }}</td><td>{{$equipos->motor->ubicacion->central->localidad->nombre?? 'Localidad inexistente' }}</td>
                    <td>{{$equipos->motor->ubicacion->central->localidad->tipo?? 'Tipo no disponible'}}</td>
                    <td>{{$equipos->motor->operatividad}}</td>
                    <td>{{$equipos->motor->comb_tanq_princ}}</td>
                    <td>{{$equipos->motor->cant_actual}}</td>
                    <td>{{$equipos->motor->comb_tanq_princ - $equipos->motor->cant_actual}}</td>
                    <td>
                       @php try { echo round(($equipos->motor->cant_actual*100) / $equipos->motor->comb_tanq_princ).'%'; }catch(Exception $e){ echo(0).'%';} @endphp
                    </td>
                    <td>{{$equipos->motor->updated_at}}</td>
                    <td>{{$equipos->motor->usuario->name ?? 'Sin Cambios'}}</td>
                   
                  
                  </tr>
                    @endforeach
                </tbody>
              </table>
           </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection

@section('scriptsalerts')


<script>
   @if(Session::has('exito'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Motogenerador Guardado con éxito'
      })
      })
    });
  @endif

   @if(Session::has('actualizado'))
     $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
      $('document').ready(function () {
        Toast.fire({
        icon: 'success',
        title: 'Datos actualizados con exito'
      })
      })
    });
  @endif
  

$(document).ready(function() {
  
  // Setup - add a text input to each footer cell
    $('#example4 thead tr').clone(true).appendTo( '#example4 thead' );
    $('#example4 thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Filtrar '+title+'" />' );
 
        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    });
      $('.js-example-basic-multiple').select2();
      var fecha =new  Date(); 
    var table = $("#example4").DataTable({
        "orderCellsTop": true,
        "fixedHeader": true,
        "responsive": false,
        "autoWidth": true,
        "bJQueryUI": true,
        'deferRender':    true,
        'scrollY':        500,
        'scrollX':        true,
        'scrollCollapse': true,
        'scroller':       true,
        order: [[6, 'asc']],
       
        dom: 'Bfrtip',
          buttons: [
             
               {
              extend: 'excelHtml5',
              title:'Portal Web GGEC - Combustible Region '+ `{!!$motor[0]->motor->ubicacion->central->localidad->estados->region->nombre!!}`,
              messageTop:fecha.getFullYear()+'-'+(fecha.getMonth()+1)+'-'+fecha.getDate()+'-'+fecha.getHours()+':'+fecha.getMinutes()+':'+fecha.getSeconds(),
              exportOptions: {
                stripHtml: true, /* Aquí indicamos que no se eliminen las imágenes */
                 columns: [':visible:not(.not-export-col):not(.hidden)'],
                 format:{
                  body: function (data, row, column, node ) {
                          return $(node).is("div") ?
                                $(node).find('input:text').val():
                                data;
                      }
                 }
              },
              footer:true
          }
          ],
      });



     new $.fn.dataTable.FixedHeader( table );

});



</script>

  


@endsection
