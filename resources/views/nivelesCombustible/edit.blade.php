@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Nivel de Combustible en MotoGenerador 
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inventario</a></li>
              <li class="breadcrumb-item active">Nivel de Combustible</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <div class="row ">
	    <div class="col s12 m12 l12 ">
	        @if ($errors->any())
	            <div class="alerta-errores">
	                <ul>
	                    @foreach ($errors->all() as $error)
	                        <li>{{ $error }}</li>
	                    @endforeach
	                </ul>
	            </div>
	        @endif
	    </div>
	</div>
    <!-- Main content -->
    <section class="content">
	    <div class="container-fluid">
	        <div class="row">
    		    <div class="col-12 col-sm-12">
          			<div class="card card-primary card-tabs">
              			<div class="card-body">
				  			<form method="POST" id="formulario_mg"  action="/nivel_combustible/{{encrypt($motogenerador->id)}}">
								<input type="hidden" name="_method" value="PUT">
                      <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                     @if($parametros != null)
                      <input type="hidden" name="state" value="{{$parametros['state']}}">
                      @endif
                      			{{csrf_field()}}
			                  	
								<div class="col-md-12">
						            <div class="card card-success">
							            <div class="card-header">
							                <h3 class="card-title">Niveles de Combustible</h3>
							            </div>
						              	<div class="card-body">
	  					                  <div class="row">
						                    	<div class="col-sm-6">
							                      	<div class="form-group">
						                        		<label>Cantidad de Combustible Actual:</label>
						                        		<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Operatividad','Seleccione el estado en que se encuentra el equipo', 100 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a><!--End ShowHelp-->
							                        	<input id="box_ModeloRectificador" name="cant_actual" value="{{$motogenerador->motor->cant_actual}}" class="form-control" type="text">
						                      		</div>
						                    	</div>
						                    	<div class="col-sm-6">
							                      	<div class="form-group">
							                        	<label>Consumo de Ltrs/Hora</label>
							                        	<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Fecha de Instalación','Introduzca fecha', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
							                        	<input id="box_ModeloRectificador" name="ltrs_hora" value="{{$motogenerador->motor->ltrs_hora}}" class="form-control" type="text">
								                    </div>
							                    </div>
							                    <div class="col-sm-4" style="display: none">
						                      		<div class="form-group">
						                        		<label>Consumo de Ltrs/Dia</label>
						                        		<a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Criticidad del Equipo','Selecione criticidad. Por Ej. para su atención inmediata o posterior', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a><!--End ShowHelp-->
							                        	<input disabled id="box_ModeloRectificador" name="ltrs_dia" value="{{$motogenerador->motor->ltrs_dia}}" class="form-control" type="text">
								                        
						                      		</div>
						                    	</div>
						                 	</div>
                              <div class="row">
                                  <div class="col-sm-6">
                                      <div class="form-group">
                                        <label>Capacidad de tanque de aceite</label>
                                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Fecha de Instalación','Introduzca fecha', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a>
                                        <input id="box_ModeloRectificador" name="cap_tanq_aceite" value="{{$motogenerador->motor->cap_tanq_aceite}}" class="form-control" type="text">
                                    </div>
                                  </div>
                                  <div class="col-sm-6" >
                                      <div class="form-group">
                                        <label>Nivel de aceite actual</label>
                                        <a href="#"  onmouseover="getMouseXY( event ); showHelp( 'help_popup','Criticidad del Equipo','Selecione criticidad. Por Ej. para su atención inmediata o posterior', 200 );" onmouseout="hideHelp('help_popup'); " onclick="return false;"><img align="absmiddle" src="{{asset('/img/signo.gif')}}" class="ico_err"></a><!--End ShowHelp-->
                                        <input id="box_ModeloRectificador" name="aceite_actual" value="{{$motogenerador->motor->aceite_actual}}" class="form-control" type="text">
                                        
                                      </div>
                                  </div>
                                
                              </div>
						                  	
						                 	<div class="card-footer" align="right">
							                  	<button type="submit" class="btn btn-success"><i class="fas fa-sync-alt nav-icon"></i> Actualizar</button>
							                </div>
									    </div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>



@endsection
@section('vs')
	@if(isset($validator))
    	{!! $validator->selector("#formulario_mg") !!}
	@endif






<script type="text/javascript">
$(document).ready(function () {

	 $(document).ready(function(){



  	$('select[id=region_id]').change(function () {
    	console.log('hola')
    var region_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'estado/' + region_id, function (cidades) {
        $('select[id=estado_id]').empty();

        if(cidades.length != 0){
            $('select[id=estado_id]').append('<option value=""> Selecciona un estado</option>');
            $.each(cidades, function (key, value) {
                $('select[id=estado_id]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });

    });


	$('select[id=estado_id]').change(function () {
    	//console.log('hola')
    var estado_id = $(this).val();
    
    var APP_URL = {!!json_encode(url('/'))!!}+'/';
    $.get(APP_URL+'central/' + estado_id, function (cidades) {
        $('select[id=central_id]').empty();

        if(cidades.length != 0){
            $('select[id=central_id]').append('<option value=""> Selecciona un central</option>');
            $.each(cidades, function (key, value) {
                $('select[id=central_id]').append('<option value=' + value.id + '>'+ value.nombre + '</option>');
            });
        }

        });

    });


	});


function falla(){
    var falla = document.getElementById('input_falla')
    var range = document.getElementById('range')
    var opera = document.getElementById('operatividad')
    console.log(falla.value)

    range.value == falla.value
    if(falla.value < 1){
      opera.value == "Inoperativo"
    }else if(falla.value>99){
      opera.value == "Operativo"
    }

  }
$('select[id=operatividad]').change(function(){
  var valor = $(this).val();
  //console.log(valor)
  if (valor == "Falla"){
    $('#falla').show();
    $('#input_falla').prop('disabled',false);
  }else{
    $('#falla').hide();
    $('#input_falla').prop('disabled',true);

  }



 });





  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection


