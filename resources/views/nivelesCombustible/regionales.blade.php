@extends ('/layouts/index')

@section ('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-8">
            <h1>
             Niveles de Combustibles Promediados por Región
            </h1>
          </div>
          <div class="col-sm-4">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">Niveles de Combustible</li>
            </ol>
          </div>
        </div>
        <div class="row mb-2">
          <div class="col-sm-12">
            <i class="nav-icon fas fa-tachometer-alt" style="color:#fd0000;margin-left: 1%;" ></i>Critico < 25%
            <i class="nav-icon fas fa-tachometer-alt" style="color:#f57a0a; margin-left: 1%;"></i>Alerta < 50% 
            <i class="nav-icon fas fa-tachometer-alt" style="color:#e7d20d;margin-left: 1%;"></i>Bajo  <75% 
            <i class="nav-icon fas fa-tachometer-alt" style="color:#0af52c;margin-left: 1%;"></i>Óptimo <= 100%
          </div>
          
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content --> 
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
             
              <div class="card-body">
              <form method="POST" id="formulario_nodo_all" name="requerimiento"  action="#" enctype="multipart/form-data">
                <input type="hidden" name="_method" value="PUT">
                        {{ csrf_field() }}
                    
                   
                    <div class="col-md-12">
                    <!-- general form elements disabled -->
                     <div class="card card-primary">
                      <div class="card-header">
                        <h3 class="card-title"> Datos de Las Regiones </h3>
                        
                      </div>
            
               <div class="card-body">

                      <!-- /.primera fila-->
                       

                        <div class="row">
                          <div class="col-md-6 text-center">
                            <div class="card ">
                              <div class="card-header" style="background-color:#00c0ef;">
                                <h3 class="card-title">Los LLanos</h3>

                                <div class="card-tools">
                                 
                                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                  </button>
                                </div>
                              </div>
                              <div class="card-body">
                                 
                                <dd> <input type="text" class="knob" data-readonly="true" value="{{$llanos}}" data-min="0" data-max="100" data-width="90" data-height="90" data-angleArc="250" data-angleOffset="-125"
                                  @if($llanos == 0)data-fgColor="#d2d6de" data-bgColor="#d2d6de" @elseif($llanos < 25 ) data-fgColor="#fd0000" data-bgColor="#d2d6de" @elseif($llanos < 50) data-fgColor="#F57A0A" data-bgColor="#d2d6de" @elseif($llanos < 75) data-fgColor="#E7D20D" data-bgColor="#d2d6de" @else data-fgColor="#0AF52C" data-bgColor="#d2d6de" @endif> </dd>
                                 
                                  <dd class="text-black">OPERATIBILIDAD = {{$llanos}} %</dd>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6 text-center" >
                            <div class="card ">
                              <div class="card-header" style="background-color:#f39c12;">
                                <h3 class="card-title">Capital</h3>
                                <div class="card-tools">
                                 
                                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                  </button>
                                </div>
                              </div>
                              <div class="card-body">
                           
                                  <dd> <input type="text" class="knob" data-readonly="true" value="{{$capital}}" data-min="0" data-max="100" data-width="90" data-height="90" data-angleArc="250" data-angleOffset="-125"
                                  @if ($capital == 0)data-fgColor="#d2d6de" data-bgColor="#d2d6de" @elseif($capital < 25 ) data-fgColor="#fd0000" data-bgColor="#d2d6de" @elseif($capital < 50) data-fgColor="#F57A0A" data-bgColor="#d2d6de" @elseif($capital < 75) data-fgColor="#E7D20D" data-bgColor="#d2d6de" @else data-fgColor="#0AF52C" data-bgColor="#d2d6de"@endif> </dd>
                                  <dd class="text-black">OPERATIBILIDAD = {{$capital}} %</dd>
                              </div>
                            </div>
                          </div>
                       </div>
<!-- SEXTA fila -->
      <div class="row">
        <div class="col-md-6 text-center">
          <div class="card ">
            <div class="card-header" style="background-color:#f56954;">
              <h3 class="card-title">Oriente</h3>
                <div class="card-tools">
                  
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">
             
            
              <dd><input type="text" class="knob" data-readonly="true" value="{{$oriente}}" data-min="0" data-max="100" data-width="90" data-height="90" data-angleArc="250" data-angleOffset="-125" @if ($oriente == 0)data-fgColor="#fd0000" data-bgColor="#fd0000" @elseif($oriente < 25 ) data-fgColor="#fd0000" data-bgColor="#d2d6de" @elseif($oriente < 50) data-fgColor="#F57A0A" data-bgColor="#d2d6de" @elseif($oriente < 75) data-fgColor="#E7D20D" data-bgColor="#d2d6de" @else data-fgColor="#0AF52C" data-bgColor="#d2d6de" @endif></dd>
              
              <dd class="text-black">OPERATIBILIDAD = {{$oriente}} %</dd>
            </div>
          </div>
        </div>
          


            <div class="col-md-6 text-center">
            <div class="card card-success">
              <div class="card-header" style="background-color:#00a65a;">
                <h3 class="card-title">Occidente</h3>

                <div class="card-tools">
                
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                </div>
                <!-- /.card-tools -->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                   
                       
                  <dd><input type="text" class="knob" data-readonly="true" value="{{$occidente}}" data-min="0" data-max="100" data-width="90" data-height="90" data-angleArc="250" data-angleOffset="-125"
                         @if ($occidente == 0)data-fgColor="#d2d6de" data-bgColor="#d2d6de" @elseif($occidente < 25 ) data-fgColor="#fd0000" data-bgColor="#d2d6de" @elseif($occidente < 50) data-fgColor="#F57A0A" data-bgColor="#d2d6de" @elseif($occidente < 75) data-fgColor="#E7D20D" data-bgColor="#d2d6de" @else data-fgColor="#0AF52C" data-bgColor="#d2d6de" @endif></dd>
                   
                                         <dd class="text-black">OPERATIBILIDAD = {{$occidente}} %</dd>
              
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>


                       

         </div>

         <div class="row">
        <div class="col-md-6 text-center">
          <div class="card ">
            <div class="card-header" style="background-color:#3c8dbc;">
              <h3 class="card-title">Andes</h3>
                <div class="card-tools">
                  
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">
             
            
              <dd><input type="text" class="knob" data-readonly="true" value="{{$andes}}"  data-min="0" data-max="100" data-width="90" data-height="90" data-angleArc="250" data-angleOffset="-125" @if ($andes == 0)data-fgColor="#d2d6de" data-bgColor="#d2d6de" @elseif($andes < 25 ) data-fgColor="#fd0000" data-bgColor="#d2d6de" @elseif($andes < 50) data-fgColor="#F57A0A" data-bgColor="#d2d6de" @elseif($andes < 75) data-fgColor="#E7D20D" data-bgColor="#d2d6de" @else data-fgColor="#0AF52C" data-bgColor="#d2d6de" @endif></dd>
              
              <dd class="text-black">OPERATIBILIDAD = {{$andes}} %</dd>
            </div>
          </div>
        </div>

        <div class="col-md-6 text-center">
          <div class="card ">
            <div class="card-header" style="background-color:#d2d6de;">
              <h3 class="card-title">Guayana</h3>
                <div class="card-tools">
                  
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">
             
            
              <dd><input type="text" class="knob" data-readonly="true" value="{{$guayana}}"  data-min="0" data-max="100" data-width="90" data-height="90" data-angleArc="250" data-angleOffset="-125" @if ($guayana == 0)data-fgColor="#d2d6de" data-bgColor="#d2d6de" @elseif($guayana < 25 ) data-fgColor="#fd0000" data-bgColor="#d2d6de" @elseif($guayana < 50) data-fgColor="#F57A0A" data-bgColor="#d2d6de" @elseif($guayana < 75) data-fgColor="#E7D20D" data-bgColor="#d2d6de" @else data-fgColor="#0AF52C" data-bgColor="#d2d6de" @endif></dd>
              
              <dd class="text-black">OPERATIBILIDAD = {{$guayana}} %</dd>
            </div>
          </div>
        </div>

         </div> 
         <div class="row">
        <div class="col-md-6 text-center">
          <div class="card ">
            <div class="card-header" style="background-color:#f39c12;">
              <h3 class="card-title">Centro</h3>
                <div class="card-tools">
                  
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">
             
            
              <dd><input type="text" class="knob" data-readonly="true" value="{{$centro}}"  data-min="0" data-max="100" data-width="90" data-height="90" data-angleArc="250" data-angleOffset="-125" @if ($centro == 0)data-fgColor="#d2d6de" data-bgColor="#d2d6de" @elseif($centro < 25 ) data-fgColor="#fd0000" data-bgColor="#d2d6de" @elseif($centro < 50) data-fgColor="#F57A0A" data-bgColor="#d2d6de" @elseif($centro < 75) data-fgColor="#E7D20D" data-bgColor="#d2d6de" @else data-fgColor="#0AF52C" data-bgColor="#d2d6de" @endif></dd>
              
              <dd class="text-black">OPERATIBILIDAD = {{$centro}} %</dd>
            </div>
          </div>
        </div>

         </div>

         
                 
    </form>
                  </div>
                </div>
            </div>
            </div>
        </div>
    </section>
</div>





@endsection

<!-- detalleopsutpivotaire -->
@section('vs')
 @if(isset($validator))
     {!! $validator->selector("#requerimiento") !!}
@endif



<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
@endsection